﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">Debug,F;Read_Write_Log,F;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Document" Type="Folder">
			<Item Name="87540-DS102.pdf" Type="Document" URL="../Document/87540-DS102.pdf"/>
			<Item Name="Chip Function.xlsx" Type="Document" URL="../Document/Chip Function.xlsx"/>
			<Item Name="Line Side Function.xlsx" Type="Document" URL="../Document/Line Side Function.xlsx"/>
			<Item Name="System Side Function.xlsx" Type="Document" URL="../Document/System Side Function.xlsx"/>
		</Item>
		<Item Name="Libraries" Type="Folder">
			<Item Name="Broadcom DSP" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Broadcom DSP.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp">
					<Item Name="Palette" Type="Folder">
						<Item Name="Broadcom DSP.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Broadcom DSP.mnu"/>
						<Item Name="Chip Info.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Chip Info.mnu"/>
						<Item Name="Chip Function.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Chip Function.mnu"/>
						<Item Name="Chip Function AVS.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Chip Function AVS.mnu"/>
						<Item Name="Chip Function Low Power.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Chip Function Low Power.mnu"/>
						<Item Name="Chip Function Reset.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Chip Function Reset.mnu"/>
						<Item Name="Chip Function Phy Power.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Chip Function Phy Power.mnu"/>
						<Item Name="RSFEC Pattern Gen.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/RSFEC Pattern Gen.mnu"/>
						<Item Name="RSFEC Pattern Gen Line Side.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/RSFEC Pattern Gen Line Side.mnu"/>
						<Item Name="RSFEC Pattern Gen System Side.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/RSFEC Pattern Gen System Side.mnu"/>
						<Item Name="Loopback.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Loopback.mnu"/>
						<Item Name="Tx Info.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/Tx Info.mnu"/>
						<Item Name="PRBS Gen.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/PRBS Gen.mnu"/>
						<Item Name="SNR.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Palette/SNR.mnu"/>
					</Item>
					<Item Name="1D Array to String__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/1D Array to String__ogtk.vi"/>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Broadcom DSP.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/Broadcom DSP.lvclass"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="End of Line Constant (bug fix).vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/End of Line Constant (bug fix).vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get File Extension.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
					<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="MGI String Pattern Match Exists.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/user.lib/_MGI/String/MGI String Pattern Match Exists.vi"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="Recursive File List.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/String to 1D Array__ogtk.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/Broadcom DSP.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="8740x.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp">
					<Item Name="8740x.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/8740x.lvclass"/>
					<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
					<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get Cluster Element by Name__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element by Name__ogtk.vi"/>
					<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
					<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
					<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
					<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
					<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
					<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
					<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
					<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
					<Item Name="GPComparison.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/vi.lib/GPower/Comparison/GPComparison.lvlib"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="Number To Enum.vim" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/vi.lib/Utility/Number To Enum.vim"/>
					<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
					<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
					<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/String to 1D Array__ogtk.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
					<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
					<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
					<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Broadcom DSP/8740x.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				</Item>
			</Item>
			<Item Name="Plugins" Type="Folder">
				<Item Name="Optical Product.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp">
					<Item Name="Palette" Type="Folder">
						<Item Name="Include mnu" Type="Folder">
							<Item Name="Product Calibration Page Assert &amp; Deassert.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Assert &amp; Deassert.mnu"/>
							<Item Name="Product Calibration Page Offset.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Offset.mnu"/>
							<Item Name="Product Calibration Page Slope.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Slope.mnu"/>
							<Item Name="Product Calibration Page Function.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page Function.mnu"/>
							<Item Name="Product Calibration Page 2 Point.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page 2 Point.mnu"/>
							<Item Name="Product Calibration Page.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Calibration Page.mnu"/>
							<Item Name="Product Communication.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Communication.mnu"/>
							<Item Name="Product ID Info Page.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product ID Info Page.mnu"/>
							<Item Name="Product Information.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Information.mnu"/>
							<Item Name="Product Information QSFP-DD.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Information QSFP-DD.mnu"/>
							<Item Name="Product Interrupt Flag.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag.mnu"/>
							<Item Name="Product Monitor.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Monitor.mnu"/>
							<Item Name="Product Page Function.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Page Function.mnu"/>
							<Item Name="Product Threshold.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Threshold.mnu"/>
							<Item Name="Product Class.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Class.mnu"/>
							<Item Name="Product Control Function QSFP.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function QSFP.mnu"/>
							<Item Name="Product Control Function QSFP-DD.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function QSFP-DD.mnu"/>
							<Item Name="Product Control Function SFP.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function SFP.mnu"/>
							<Item Name="Product Control Function.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function.mnu"/>
							<Item Name="Product Interrupt Flag QSFP-DD.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag QSFP-DD.mnu"/>
							<Item Name="Product Interrupt Flag QSFP.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag QSFP.mnu"/>
							<Item Name="Product Interrupt Flag SFP.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Interrupt Flag SFP.mnu"/>
							<Item Name="Product MSA Optional Page.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product MSA Optional Page.mnu"/>
							<Item Name="Product Lookup Table Page.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Lookup Table Page.mnu"/>
							<Item Name="Product Control Function PPG&amp;Loopback Control  QSFP-DD.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product Control Function PPG&amp;Loopback Control  QSFP-DD.mnu"/>
						</Item>
						<Item Name="Product.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Product.mnu"/>
					</Item>
					<Item Name="Optical Product" Type="Folder">
						<Item Name="Optical Product.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Optical Product.lvclass"/>
					</Item>
					<Item Name="MSA" Type="Folder">
						<Item Name="QSFP-DD" Type="Folder">
							<Item Name="QSFP-DD.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP-DD/QSFP-DD.lvclass"/>
						</Item>
						<Item Name="QSFP56 CMIS" Type="Folder">
							<Item Name="QSFP56 CMIS.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP56 CMIS/QSFP56 CMIS.lvclass"/>
						</Item>
						<Item Name="QSFP112 CMIS" Type="Folder">
							<Item Name="QSFP112 CMIS.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP112 CMIS/QSFP112 CMIS.lvclass"/>
						</Item>
						<Item Name="QSFP56 SFF8636" Type="Folder">
							<Item Name="QSFP56 SFF8636.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP56 SFF8636/QSFP56 SFF8636.lvclass"/>
						</Item>
						<Item Name="QSFP28" Type="Folder">
							<Item Name="QSFP28.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP28/QSFP28.lvclass"/>
						</Item>
						<Item Name="QSFP10" Type="Folder">
							<Item Name="QSFP10.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QSFP10/QSFP10.lvclass"/>
						</Item>
						<Item Name="SFP-DD" Type="Folder">
							<Item Name="SFP-DD.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP-DD/SFP-DD.lvclass"/>
						</Item>
						<Item Name="SFP56" Type="Folder">
							<Item Name="SFP56.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP56/SFP56.lvclass"/>
						</Item>
						<Item Name="SFP28" Type="Folder">
							<Item Name="SFP28.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP28/SFP28.lvclass"/>
						</Item>
						<Item Name="SFP28-EFM8BB1" Type="Folder">
							<Item Name="SFP28-EFM8BB1.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP28-EFM8BB1/SFP28-EFM8BB1.lvclass"/>
						</Item>
						<Item Name="SFP10" Type="Folder">
							<Item Name="SFP10.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/SFP10/SFP10.lvclass"/>
						</Item>
						<Item Name="OSFP" Type="Folder">
							<Item Name="OSFP.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/OSFP/OSFP.lvclass"/>
						</Item>
						<Item Name="COBO" Type="Folder">
							<Item Name="COBO.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/COBO/COBO.lvclass"/>
						</Item>
						<Item Name="QM Products" Type="Folder">
							<Item Name="QM SFP28" Type="Folder">
								<Item Name="QM SFP28.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/QM SFP28/QM SFP28.lvclass"/>
							</Item>
						</Item>
					</Item>
					<Item Name="Tx Device" Type="Folder">
						<Item Name="Tx Device.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Tx Device/Tx Device.lvclass"/>
					</Item>
					<Item Name="Rx Device" Type="Folder">
						<Item Name="Rx Device.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Rx Device/Rx Device.lvclass"/>
					</Item>
					<Item Name="PSM4 &amp; CWMD4 Device" Type="Folder">
						<Item Name="24025" Type="Folder">
							<Item Name="24025.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/24025/24025.lvclass"/>
						</Item>
					</Item>
					<Item Name="AOC TRx Device" Type="Folder">
						<Item Name="37045" Type="Folder">
							<Item Name="37045.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37045/37045.lvclass"/>
						</Item>
						<Item Name="37145" Type="Folder">
							<Item Name="37145.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37145/37145.lvclass"/>
						</Item>
						<Item Name="37345" Type="Folder">
							<Item Name="37345.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37345/37345.lvclass"/>
						</Item>
						<Item Name="37645" Type="Folder">
							<Item Name="37645.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37645/37645.lvclass"/>
						</Item>
						<Item Name="37044" Type="Folder">
							<Item Name="37044.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37044/37044.lvclass"/>
						</Item>
						<Item Name="37144" Type="Folder">
							<Item Name="37144.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37144/37144.lvclass"/>
						</Item>
						<Item Name="37344" Type="Folder">
							<Item Name="37344.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37344/37344.lvclass"/>
						</Item>
						<Item Name="37644" Type="Folder">
							<Item Name="37644.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/37644/37644.lvclass"/>
						</Item>
						<Item Name="RT146" Type="Folder">
							<Item Name="RT146.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/RT146/RT146.lvclass"/>
						</Item>
						<Item Name="RT145" Type="Folder">
							<Item Name="RT145.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/RT145/RT145.lvclass"/>
						</Item>
						<Item Name="UX2291" Type="Folder">
							<Item Name="UX2291.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/UX2291/UX2291.lvclass"/>
						</Item>
						<Item Name="UX2091" Type="Folder">
							<Item Name="UX2091.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Device/UX2091/UX2091.lvclass"/>
						</Item>
					</Item>
					<Item Name="Public" Type="Folder">
						<Item Name="Scan Product.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Product.vi"/>
						<Item Name="Scan Product By String.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Product By String.vi"/>
						<Item Name="Scan Product By Identifier.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Product By Identifier.vi"/>
						<Item Name="Replace USB-I2C Class.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Replace USB-I2C Class.vi"/>
						<Item Name="Get Product Index.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Product Index.vi"/>
						<Item Name="Get Product Class.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Product Class.vi"/>
						<Item Name="Get Product Channel.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Product Channel.vi"/>
						<Item Name="Get Slave Address.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Slave Address.vi"/>
						<Item Name="Replace Slave Address.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Replace Slave Address.vi"/>
						<Item Name="Scan Outsourcing Product.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Scan Outsourcing Product.vi"/>
						<Item Name="Get Outsourcing Product Class.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Public/Get Outsourcing Product Class.vi"/>
					</Item>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="Lookup Table.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Lookup Table/Lookup Table.lvlib"/>
					<Item Name="GPArray.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/GPower/Array/GPArray.lvlib"/>
					<Item Name="GPNumeric.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/GPower/Numeric/GPNumeric.lvlib"/>
					<Item Name="GPString.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/GPower/String/GPString.lvlib"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
					<Item Name="Space Constant.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="Number To Enum.vim" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/Number To Enum.vim"/>
					<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
					<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
					<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
					<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
					<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
					<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
					<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
					<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
					<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
					<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
					<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="LVOOP Is Same Or Descendant Class__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Is Same Or Descendant Class__ogtk.vi"/>
					<Item Name="Get LV Class Name.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
					<Item Name="Channel DBL-Cluster.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Control/Channel DBL-Cluster.ctl"/>
					<Item Name="Channel U16-Cluster.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Control/Channel U16-Cluster.ctl"/>
					<Item Name="Function-Enum.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugins/Optical Product.lvlibp/Optical Product/Control/Function-Enum.ctl"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Tester" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Line Side" Type="Folder">
				<Item Name="Polarity" Type="Folder">
					<Item Name="Test Line Side Get TRx Polarity.vi" Type="VI" URL="../Tester/Test Line Side Get TRx Polarity.vi"/>
					<Item Name="Test Line Side Set Rx Polarity.vi" Type="VI" URL="../Tester/Test Line Side Set Rx Polarity.vi"/>
					<Item Name="Test Line Side Set Tx Polarity.vi" Type="VI" URL="../Tester/Test Line Side Set Tx Polarity.vi"/>
				</Item>
				<Item Name="Loopback" Type="Folder">
					<Item Name="Test Line Side Get Digital Loopback.vi" Type="VI" URL="../Tester/Test Line Side Get Digital Loopback.vi"/>
					<Item Name="Test Line Side Set Digital Loopback.vi" Type="VI" URL="../Tester/Test Line Side Set Digital Loopback.vi"/>
					<Item Name="Test Line Side Set Remote Loopback.vi" Type="VI" URL="../Tester/Test Line Side Set Remote Loopback.vi"/>
					<Item Name="Test Line Side Get Remote Loopback.vi" Type="VI" URL="../Tester/Test Line Side Get Remote Loopback.vi"/>
				</Item>
				<Item Name="Squelch" Type="Folder">
					<Item Name="Test Line Side Get Rx Squelch.vi" Type="VI" URL="../Tester/Test Line Side Get Rx Squelch.vi"/>
					<Item Name="Test Line Side Set Rx Squelch.vi" Type="VI" URL="../Tester/Test Line Side Set Rx Squelch.vi"/>
					<Item Name="Test Line Side Get Tx Squelch.vi" Type="VI" URL="../Tester/Test Line Side Get Tx Squelch.vi"/>
					<Item Name="Test Line Side Set Tx Squelch.vi" Type="VI" URL="../Tester/Test Line Side Set Tx Squelch.vi"/>
				</Item>
				<Item Name="Tx Info" Type="Folder">
					<Item Name="Test Line Side Get Tx Info.vi" Type="VI" URL="../Tester/Test Line Side Get Tx Info.vi"/>
					<Item Name="Test Line Side Set Tx Info.vi" Type="VI" URL="../Tester/Test Line Side Set Tx Info.vi"/>
				</Item>
				<Item Name="PRBS Gen" Type="Folder">
					<Item Name="Test Line Side PRBS Gen Enable.vi" Type="VI" URL="../Tester/Test Line Side PRBS Gen Enable.vi"/>
					<Item Name="Test Line Side PRBS Gen Disable.vi" Type="VI" URL="../Tester/Test Line Side PRBS Gen Disable.vi"/>
					<Item Name="Test Line Side PRBS Gen Get Status.vi" Type="VI" URL="../Tester/Test Line Side PRBS Gen Get Status.vi"/>
					<Item Name="Test Line Side PRBS Enable Checker.vi" Type="VI" URL="../Tester/Test Line Side PRBS Enable Checker.vi"/>
					<Item Name="Test Line Side PRBS Disable Checker.vi" Type="VI" URL="../Tester/Test Line Side PRBS Disable Checker.vi"/>
					<Item Name="Test Line Side PRBS Clear.vi" Type="VI" URL="../Tester/Test Line Side PRBS Clear.vi"/>
					<Item Name="Test Line Side PRBS Get Lock State.vi" Type="VI" URL="../Tester/Test Line Side PRBS Get Lock State.vi"/>
					<Item Name="Test Line Side PRBS Gen Get Error Count.vi" Type="VI" URL="../Tester/Test Line Side PRBS Gen Get Error Count.vi"/>
				</Item>
				<Item Name="SSPRQ" Type="Folder">
					<Item Name="Test Line Side SSPRQ Control.vi" Type="VI" URL="../Tester/Test Line Side SSPRQ Control.vi"/>
					<Item Name="Test Line Side Get SSPRQ Status.vi" Type="VI" URL="../Tester/Test Line Side Get SSPRQ Status.vi"/>
					<Item Name="Test Line Side SSPRQ Clear.vi" Type="VI" URL="../Tester/Test Line Side SSPRQ Clear.vi"/>
					<Item Name="Test Line Side SSPRQ Enable Checker.vi" Type="VI" URL="../Tester/Test Line Side SSPRQ Enable Checker.vi"/>
					<Item Name="Test Line Side SSPRQ Disable Checker.vi" Type="VI" URL="../Tester/Test Line Side SSPRQ Disable Checker.vi"/>
					<Item Name="Test Line Side SSPRQ Get Error Count.vi" Type="VI" URL="../Tester/Test Line Side SSPRQ Get Error Count.vi"/>
				</Item>
				<Item Name="Rx Info" Type="Folder">
					<Item Name="Test Line Side Get Rx Info.vi" Type="VI" URL="../Tester/Test Line Side Get Rx Info.vi"/>
					<Item Name="Test Line Side Set Rx Info.vi" Type="VI" URL="../Tester/Test Line Side Set Rx Info.vi"/>
				</Item>
				<Item Name="Test Line Side Get CMIS SNR.vi" Type="VI" URL="../Tester/Test Line Side Get CMIS SNR.vi"/>
				<Item Name="Test Line Side Get SNR.vi" Type="VI" URL="../Tester/Test Line Side Get SNR.vi"/>
			</Item>
			<Item Name="System Side" Type="Folder">
				<Item Name="Polarity" Type="Folder">
					<Item Name="Test System Side Get TRx Polarity.vi" Type="VI" URL="../Tester/Test System Side Get TRx Polarity.vi"/>
					<Item Name="Test System Side Set Tx Polarity.vi" Type="VI" URL="../Tester/Test System Side Set Tx Polarity.vi"/>
					<Item Name="Test System Side Set Rx Polarity.vi" Type="VI" URL="../Tester/Test System Side Set Rx Polarity.vi"/>
				</Item>
				<Item Name="Loopback" Type="Folder">
					<Item Name="Test System Side Get Digital Loopback.vi" Type="VI" URL="../Tester/Test System Side Get Digital Loopback.vi"/>
					<Item Name="Test System Side Set Digital Loopback.vi" Type="VI" URL="../Tester/Test System Side Set Digital Loopback.vi"/>
					<Item Name="Test System Side Get Remote Loopback.vi" Type="VI" URL="../Tester/Test System Side Get Remote Loopback.vi"/>
					<Item Name="Test System Side Set Remote Loopback.vi" Type="VI" URL="../Tester/Test System Side Set Remote Loopback.vi"/>
				</Item>
				<Item Name="Squelch" Type="Folder">
					<Item Name="Test System Side Get Tx Squelch.vi" Type="VI" URL="../Tester/Test System Side Get Tx Squelch.vi"/>
					<Item Name="Test System Side Set Tx Squelch.vi" Type="VI" URL="../Tester/Test System Side Set Tx Squelch.vi"/>
					<Item Name="Test System Side Get Rx Squelch.vi" Type="VI" URL="../Tester/Test System Side Get Rx Squelch.vi"/>
					<Item Name="Test System Side Set Rx Squelch.vi" Type="VI" URL="../Tester/Test System Side Set Rx Squelch.vi"/>
				</Item>
				<Item Name="Tx Info" Type="Folder">
					<Item Name="Test System Side Get Tx Info.vi" Type="VI" URL="../Tester/Test System Side Get Tx Info.vi"/>
					<Item Name="Test System Side Set Tx Info.vi" Type="VI" URL="../Tester/Test System Side Set Tx Info.vi"/>
				</Item>
				<Item Name="PRBS Gen" Type="Folder">
					<Item Name="Test System Side PRBS Gen Enable.vi" Type="VI" URL="../Tester/Test System Side PRBS Gen Enable.vi"/>
					<Item Name="Test System Side PRBS Gen Disable.vi" Type="VI" URL="../Tester/Test System Side PRBS Gen Disable.vi"/>
					<Item Name="Test System Side PRBS Gen Get Status.vi" Type="VI" URL="../Tester/Test System Side PRBS Gen Get Status.vi"/>
					<Item Name="Test System Side PRBS Enable Checker.vi" Type="VI" URL="../Tester/Test System Side PRBS Enable Checker.vi"/>
					<Item Name="Test System Side PRBS Disable Checker.vi" Type="VI" URL="../Tester/Test System Side PRBS Disable Checker.vi"/>
					<Item Name="Test System Side PRBS Clear.vi" Type="VI" URL="../Tester/Test System Side PRBS Clear.vi"/>
					<Item Name="Test System Side PRBS Get Lock State.vi" Type="VI" URL="../Tester/Test System Side PRBS Get Lock State.vi"/>
					<Item Name="Test System Side PRBS Gen Get Error Count.vi" Type="VI" URL="../Tester/Test System Side PRBS Gen Get Error Count.vi"/>
				</Item>
				<Item Name="SSPRQ" Type="Folder">
					<Item Name="Test System Side SSPRQ Control.vi" Type="VI" URL="../Tester/Test System Side SSPRQ Control.vi"/>
					<Item Name="Test System Side Get SSPRQ Status.vi" Type="VI" URL="../Tester/Test System Side Get SSPRQ Status.vi"/>
					<Item Name="Test System Side SSPRQ Clear.vi" Type="VI" URL="../Tester/Test System Side SSPRQ Clear.vi"/>
					<Item Name="Test System Side SSPRQ Disable Checker.vi" Type="VI" URL="../Tester/Test System Side SSPRQ Disable Checker.vi"/>
				</Item>
				<Item Name="Rx Info" Type="Folder">
					<Item Name="Test System Side Get Rx Info.vi" Type="VI" URL="../Tester/Test System Side Get Rx Info.vi"/>
					<Item Name="Test System Side Set Rx Info.vi" Type="VI" URL="../Tester/Test System Side Set Rx Info.vi"/>
				</Item>
			</Item>
			<Item Name="File and Log" Type="Folder">
				<Item Name="Get Command From VBS File.vi" Type="VI" URL="../Tester/Get Command From VBS File.vi"/>
				<Item Name="Get Lane Read Write Command.vi" Type="VI" URL="../Tester/Get Lane Read Write Command.vi"/>
				<Item Name="4CH Read Write Log Convert.vi" Type="VI" URL="../Tester/4CH Read Write Log Convert.vi"/>
				<Item Name="Read Write Log to File.vi" Type="VI" URL="../Tester/Read Write Log to File.vi"/>
				<Item Name="DEC_EQUAL_TO_HEX.vi" Type="VI" URL="../Tester/DEC_EQUAL_TO_HEX.vi"/>
				<Item Name="Old SNR Convert.vi" Type="VI" URL="../Tester/Old SNR Convert.vi"/>
			</Item>
			<Item Name="ALB" Type="Folder">
				<Item Name="Test QSFP56 ALB PRBS Function.vi" Type="VI" URL="../Tester/Test QSFP56 ALB PRBS Function.vi"/>
			</Item>
			<Item Name="Test Get Info.vi" Type="VI" URL="../Tester/Test Get Info.vi"/>
			<Item Name="Test Get Chip Mode.vi" Type="VI" URL="../Tester/Test Get Chip Mode.vi"/>
			<Item Name="Test Set Chip Mode.vi" Type="VI" URL="../Tester/Test Set Chip Mode.vi"/>
			<Item Name="Test AVS Control.vi" Type="VI" URL="../Tester/Test AVS Control.vi"/>
			<Item Name="Verify Log.vi" Type="VI" URL="../Tester/Verify Log.vi"/>
			<Item Name="Test Get CRC32.vi" Type="VI" URL="../Tester/Test Get CRC32.vi"/>
			<Item Name="Test Reset.vi" Type="VI" URL="../Tester/Test Reset.vi"/>
			<Item Name="Test Set 0V75.vi" Type="VI" URL="../Tester/Test Set 0V75.vi"/>
			<Item Name="Test Set 0V90.vi" Type="VI" URL="../Tester/Test Set 0V90.vi"/>
			<Item Name="Test capi Get Status.vi" Type="VI" URL="../Tester/Test capi Get Status.vi"/>
		</Item>
		<Item Name="USB Communication" Type="Folder">
			<Item Name="USB-I2C" Type="Folder">
				<Item Name="USB-I2C.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp">
					<Item Name="Palette" Type="Folder">
						<Item Name="USB-I2C.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C.mnu"/>
						<Item Name="USB-I2C Example.mnu" Type="Document" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Example.mnu"/>
					</Item>
					<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
					<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
					<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
					<Item Name="Get File Extension.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
					<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
					<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
					<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
					<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
					<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
					<Item Name="MGI String Pattern Match Exists.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_MGI/String/MGI String Pattern Match Exists.vi"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Recursive File List.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
					<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
					<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
					<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
					<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
					<Item Name="USB-I2C Create.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Create/USB-I2C Create.lvclass"/>
					<Item Name="USB-I2C.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C/USB-I2C.lvclass"/>
					<Item Name="VI Tree.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/VI Tree.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
			</Item>
		</Item>
		<Item Name="8754x.lvlib" Type="Library" URL="../8754x.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="1D Array to String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/1D Array to String__ogtk.vi"/>
				<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/String to 1D Array__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="End of Line Constant (bug fix).vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/End of Line Constant (bug fix).vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Number To Enum.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Number To Enum.vim"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="8754x" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{D52F8670-D554-429B-B61A-8AA4A0EFD0B3}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">V1.10.3-20240718
1.Chip Mode Fix 4x26G change issue

V1.10.2-20230913
1.Chip mode 4x25G NRZ to 2x53G PAM4 Enable other FEC type

V1.10.1-20230321
1.Get capi_prbs_info_t fix get PRBS Gen Status bug, change to byte

V1.10.0-20220930
1.Chip Mode Add New Mode 4x25G NRZ to 4x25G PAM4、2x50G PAM4 to 2x50G PAM4 and 1x50G PAM4 to 1x50G PAM4

V1.9.0-20220915
1.Add capi_get_status
2.Both Side PRBS Get Lock State change use get cdr lock function

V1.8.1-20220907
1.Get Data to Write[] By Lane Config Type Line side enable parameters all change back to 0x79

V1.8.0-20220906
1.Support D00B, because Line side TXFIR add 12taps

V1.7.5-20220629
1.D00A Fix TXFIR control on both side

V1.7.4-20220628
1.8740x intl VIs change to dynamic

V1.7.3-20220121
1.Both Side PRBS Gen Get Error Count Fix bug for caller by 87580

V1.7.2-20220112
1.Both Side Prepare For Get Error Count change to share clone

V1.7.1-20220103
1.Fix CMIS SNR calc bug

V1.7.0-20211223
1.Line Side SNR Function Changed
2.Line Side Add CMIS SNR and LTP

V1.6.0-20211130
1.System Side Add Rx Info for Est ChLoss、Rx Mode、VGA and media type

V1.5.1-20211101
1.Both Side Fix Get Error Count bug when use whole lane

V1.5.0-20211014
1.Line Side Rx Info Add Gain Boost Control
2.capi_set_config read check change to dynamic

V1.4.13-20211005
1.Broadcom DSP Class Tx Info Add Tx Post4
2.Both Side TXFIR Fix Taps bug

V1.4.12-20211004
1.Fix Both Side Squelch Control bug

V1.4.11-20211001
1.Do Command Request By Core IP change to Dynamic

V1.4.10-20210831
1.Get Chip Mode Add 87541

V1.4.9-20210810
1.Remove Get Register Data, change it to use from 8754x
2.Line Side SNR Data change to put in top class

V1.4.8-20210805
1.Chip Info Add to verify 87541
2.Line Side Rx Info Add Peaking Filter

V1.4.7-20210804
1.Line Side Rx Info Add Power Mode Control Function

V1.4.6-20210720
1.System-Side PRBS GEN Get Type Fix Get PRBS13Q Display Bug

V1.4.5-20210624
1.Line-Side PRBS GEN Get Error Count and System-Side PRBS GEN Get Error Count fix get whole lane data bug for 8758x

V1.4.4-20210616
1.Get Lane Byte Data Fix bug when use Lane -1 with PHY1
2.System-Side PRBS GEN Get Lane Error Count、Line-Side and System Side PRBS GEN Get Lane Error Count Fix Lane Update Lock State for 87580
3.Line-Side Get CMIS Diagnostics change to dynamic
4.Line-Side Set SSPRQ Clear change to dynamic

V1.4.3-20210615
1.Get Lane Byte Data Modify for second die with 8758x

V1.4.2-20210429
1.Line Side Digital Loopback Add Set Status to Cluster of Class

V1.4.1-20210331
1.Get Temp Fix bug when temp under 0 C

V1.4.0-20210324
1.Add Mode 18 and 19
2.Fix Mode 10 bug
3.New FW Support Mode 17 on FEC ON State 

V1.3.2-20210219
1.Get Support FEC Term Type Fix Mode16 display bug

V1.3.1-20210205
1.Get Chip ID Add Auto Password Check for different MSA

V1.3.0-20210125
1.Add New Modes

V1.2.1-20201209
1.PRBS Get Error Count Add PRBS Clear when Start on first time

V1.2.0-20201208
1.Line Side Add SNR and SNR Level Function

V1.1.1-20201207
1.power_util_ana_reg_rdwr_access disable return check on for loop

V1.1.0-20201205
1.Add Internal Voltage Controls

V1.0.2-20201127
1.Disable Debug

V1.0.1-20201126
1.Set Chip Mode Disable Read Check

V1.0.0</Property>
				<Property Name="Bld_buildSpecName" Type="Str">8754x</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Libraries/Broadcom DSP</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{212E80BE-02A6-4989-ACBD-40EFED17AACE}</Property>
				<Property Name="Bld_version.build" Type="Int">85</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">10</Property>
				<Property Name="Bld_version.patch" Type="Int">3</Property>
				<Property Name="Destination[0].destName" Type="Str">8754x.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Libraries/Broadcom DSP/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Libraries/Broadcom DSP</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{73A38E15-258A-4BF8-8270-00683D8881A8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/8754x.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Luxshare-Tech</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Broadcom DSP - 8754x</Property>
				<Property Name="TgtF_internalName" Type="Str">8754x</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright (c) 2020-2024 Luxshare-Tech Corporation. All rights reserved</Property>
				<Property Name="TgtF_productName" Type="Str">8754x</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{1C3208AD-9BAF-4A40-B209-8184F588BDDE}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">8754x.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
