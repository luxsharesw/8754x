var structchip__bh__config =
[
    [ "baud_rate", "structchip__bh__config.html#afe28bbc7a6117d39b02514eaf94ded6e", null ],
    [ "modulation", "structchip__bh__config.html#a23c8f6486f56180237bdaf9ae06ae33f", null ],
    [ "osr", "structchip__bh__config.html#ad1dd28263808381a38e721fb95822e64", null ],
    [ "multiplier", "structchip__bh__config.html#af302133eac52b94f7d895c76322fdda5", null ],
    [ "vco_rate", "structchip__bh__config.html#aae29996f7814ccc268bd3f5cc4d1c6cf", null ],
    [ "lane_mask", "structchip__bh__config.html#a60eb571d4b1e8308541d7e4d0eb20046", null ]
];