var capi__def_8h =
[
    [ "tx_driver_t", "structtx__driver__t.html", "structtx__driver__t" ],
    [ "capi_polarity_info_t", "structcapi__polarity__info__t.html", "structcapi__polarity__info__t" ],
    [ "capi_lnktrn_802_3cd_cfg_t", "structcapi__lnktrn__802__3cd__cfg__t.html", "structcapi__lnktrn__802__3cd__cfg__t" ],
    [ "capi_lnktrn_cl72_cl93_cfg_t", "structcapi__lnktrn__cl72__cl93__cfg__t.html", "structcapi__lnktrn__cl72__cl93__cfg__t" ],
    [ "capi_lnktrn_status_t", "structcapi__lnktrn__status__t.html", "structcapi__lnktrn__status__t" ],
    [ "link_training_status_t", "structlink__training__status__t.html", "structlink__training__status__t" ],
    [ "lane_tx_info_t", "structlane__tx__info__t.html", "structlane__tx__info__t" ],
    [ "lane_rx_info_t", "structlane__rx__info__t.html", "structlane__rx__info__t" ],
    [ "lane_lnktrn_802_3cd_cfg_t", "structlane__lnktrn__802__3cd__cfg__t.html", "structlane__lnktrn__802__3cd__cfg__t" ],
    [ "lane_lnktrn_cl72_cl93_cfg_t", "structlane__lnktrn__cl72__cl93__cfg__t.html", "structlane__lnktrn__cl72__cl93__cfg__t" ],
    [ "lane_link_training_timer_t", "structlane__link__training__timer__t.html", "structlane__link__training__timer__t" ],
    [ "lane_lnktrn_info_t", "structlane__lnktrn__info__t.html", "structlane__lnktrn__info__t" ],
    [ "capi_lane_config_info_t", "structcapi__lane__config__info__t.html", "structcapi__lane__config__info__t" ],
    [ "capi_lane_ctrl_info_t", "structcapi__lane__ctrl__info__t.html", "structcapi__lane__ctrl__info__t" ],
    [ "capi_pmd_info_t", "structcapi__pmd__info__t.html", "structcapi__pmd__info__t" ],
    [ "capi_loopback_info_t", "structcapi__loopback__info__t.html", "structcapi__loopback__info__t" ],
    [ "capi_prbs_poly_t", "structcapi__prbs__poly__t.html", "structcapi__prbs__poly__t" ],
    [ "capi_prbs_cfg_t", "structcapi__prbs__cfg__t.html", "structcapi__prbs__cfg__t" ],
    [ "capi_prbs_ssprq_t", "structcapi__prbs__ssprq__t.html", "structcapi__prbs__ssprq__t" ],
    [ "capi_q_prbs_13_t", "structcapi__q__prbs__13__t.html", "structcapi__q__prbs__13__t" ],
    [ "capi_prbs_square_wave_t", "structcapi__prbs__square__wave__t.html", "structcapi__prbs__square__wave__t" ],
    [ "capi_prbs_shared_tx_ptrn_t", "structcapi__prbs__shared__tx__ptrn__t.html", "structcapi__prbs__shared__tx__ptrn__t" ],
    [ "capi_prbs_staircase_pat_t", "structcapi__prbs__staircase__pat__t.html", "structcapi__prbs__staircase__pat__t" ],
    [ "capi_prbs_info_t", "structcapi__prbs__info__t.html", "structcapi__prbs__info__t" ],
    [ "capi_prbs_err_inject_t", "structcapi__prbs__err__inject__t.html", "structcapi__prbs__err__inject__t" ],
    [ "prbs_ml_err_t", "structprbs__ml__err__t.html", "structprbs__ml__err__t" ],
    [ "capi_prbs_status_t", "structcapi__prbs__status__t.html", "structcapi__prbs__status__t" ],
    [ "capi_status_t", "structcapi__status__t.html", "structcapi__status__t" ],
    [ "capi_lpm_info_t", "structcapi__lpm__info__t.html", "structcapi__lpm__info__t" ],
    [ "capi_power_info_t", "structcapi__power__info__t.html", "structcapi__power__info__t" ],
    [ "capi_rptr_indep_lane_ctrl_info_t", "structcapi__rptr__indep__lane__ctrl__info__t.html", "structcapi__rptr__indep__lane__ctrl__info__t" ],
    [ "capi_line_pll_ctrl_info_t", "structcapi__line__pll__ctrl__info__t.html", "structcapi__line__pll__ctrl__info__t" ],
    [ "capi_chip_command_info_t", "structcapi__chip__command__info__t.html", "structcapi__chip__command__info__t" ],
    [ "eye_margin_estimate_t", "structeye__margin__estimate__t.html", "structeye__margin__estimate__t" ],
    [ "capi_diag_lane_status_t", "structcapi__diag__lane__status__t.html", "structcapi__diag__lane__status__t" ],
    [ "reset_t", "structreset__t.html", "structreset__t" ],
    [ "lpm_t", "structlpm__t.html", "structlpm__t" ],
    [ "txpi_override_t", "structtxpi__override__t.html", "structtxpi__override__t" ],
    [ "capi_lane_config_mod_t", "structcapi__lane__config__mod__t.html", "structcapi__lane__config__mod__t" ],
    [ "capi_config_info_t", "structcapi__config__info__t.html", "structcapi__config__info__t" ],
    [ "capi_chip_ref_clk_conf_reg_t", "unioncapi__chip__ref__clk__conf__reg__t.html", "unioncapi__chip__ref__clk__conf__reg__t" ],
    [ "capi_temp_status_info_t", "unioncapi__temp__status__info__t.html", "unioncapi__temp__status__info__t" ],
    [ "capi_lane_mode_reg_t", "unioncapi__lane__mode__reg__t.html", "unioncapi__lane__mode__reg__t" ],
    [ "capi_lane_command_reg_t", "unioncapi__lane__command__reg__t.html", "unioncapi__lane__command__reg__t" ],
    [ "capi_lane_status_reg_t", "unioncapi__lane__status__reg__t.html", "unioncapi__lane__status__reg__t" ],
    [ "chip_top_spi_program_command_t", "unionchip__top__spi__program__command__t.html", "unionchip__top__spi__program__command__t" ],
    [ "bh_lane_config_reg_t", "unionbh__lane__config__reg__t.html", "unionbh__lane__config__reg__t" ],
    [ "lw_mode_reg_t", "unionlw__mode__reg__t.html", "unionlw__mode__reg__t" ],
    [ "capi_load_phy_info_t", "structcapi__load__phy__info__t.html", "structcapi__load__phy__info__t" ],
    [ "capi_firmware_load_phy_list_t", "structcapi__firmware__load__phy__list__t.html", "structcapi__firmware__load__phy__list__t" ],
    [ "capi_status_info_t", "structcapi__status__info__t.html", "structcapi__status__info__t" ],
    [ "capi_image_info_t", "structcapi__image__info__t.html", "structcapi__image__info__t" ],
    [ "phy_static_config_t", "structphy__static__config__t.html", "structphy__static__config__t" ],
    [ "capi_download_info_t", "structcapi__download__info__t.html", "structcapi__download__info__t" ],
    [ "capi_broadcast_intf_t", "structcapi__broadcast__intf__t.html", "structcapi__broadcast__intf__t" ],
    [ "capi_reg_info_t", "structcapi__reg__info__t.html", "structcapi__reg__info__t" ],
    [ "capi_lane_status_t", "unioncapi__lane__status__t.html", "unioncapi__lane__status__t" ],
    [ "capi_hw_info_t", "structcapi__hw__info__t.html", "structcapi__hw__info__t" ],
    [ "capi_sw_info_t", "structcapi__sw__info__t.html", "structcapi__sw__info__t" ],
    [ "capi_chip_info_t", "structcapi__chip__info__t.html", "structcapi__chip__info__t" ],
    [ "capi_core_info_t", "structcapi__core__info__t.html", "structcapi__core__info__t" ],
    [ "capi_chip_status_info_t", "structcapi__chip__status__info__t.html", "structcapi__chip__status__info__t" ],
    [ "capi_avs_status_t", "structcapi__avs__status__t.html", "structcapi__avs__status__t" ],
    [ "capi_avs_voltage_info_t", "structcapi__avs__voltage__info__t.html", "structcapi__avs__voltage__info__t" ],
    [ "capi_other_voltage_config_info_t", "structcapi__other__voltage__config__info__t.html", "structcapi__other__voltage__config__info__t" ],
    [ "capi_voltage_status_info_t", "unioncapi__voltage__status__info__t.html", "unioncapi__voltage__status__info__t" ],
    [ "capi_avs_mode_config_info_t", "structcapi__avs__mode__config__info__t.html", "structcapi__avs__mode__config__info__t" ],
    [ "capi_i2c_info_t", "structcapi__i2c__info__t.html", "structcapi__i2c__info__t" ],
    [ "lane_txpi_override_t", "structlane__txpi__override__t.html", "structlane__txpi__override__t" ],
    [ "cw_chip_port_info_t", "structcw__chip__port__info__t.html", "structcw__chip__port__info__t" ],
    [ "cw_chip_port_info_for_mode_t", "structcw__chip__port__info__for__mode__t.html", "structcw__chip__port__info__for__mode__t" ],
    [ "capi_test_pattern_cfg_t", "structcapi__test__pattern__cfg__t.html", "structcapi__test__pattern__cfg__t" ],
    [ "capi_phy_power_t", "structcapi__phy__power__t.html", "structcapi__phy__power__t" ],
    [ "capi_fixed_voltage_config_info_t", "structcapi__fixed__voltage__config__info__t.html", "structcapi__fixed__voltage__config__info__t" ],
    [ "capi_chip_voltage_config_info_t", "structcapi__chip__voltage__config__info__t.html", "structcapi__chip__voltage__config__info__t" ],
    [ "capi_fec_type_t", "unioncapi__fec__type__t.html", "unioncapi__fec__type__t" ],
    [ "capi_fec_rx_err_cnt_t", "structcapi__fec__rx__err__cnt__t.html", "structcapi__fec__rx__err__cnt__t" ],
    [ "capi_fec_rx_status_t", "structcapi__fec__rx__status__t.html", "structcapi__fec__rx__status__t" ],
    [ "cw_kp4fec_err_cnt_t", "structcw__kp4fec__err__cnt__t.html", "structcw__kp4fec__err__cnt__t" ],
    [ "capi_fec_ber_t", "structcapi__fec__ber__t.html", "structcapi__fec__ber__t" ],
    [ "capi_fec_dump_status_t", "structcapi__fec__dump__status__t.html", "structcapi__fec__dump__status__t" ],
    [ "cw_kp4fec_ber_t", "structcw__kp4fec__ber__t.html", "structcw__kp4fec__ber__t" ],
    [ "capi_kp4fec_ber_state_t", "structcapi__kp4fec__ber__state__t.html", "structcapi__kp4fec__ber__state__t" ],
    [ "capi_rptr_fec_st_t", "structcapi__rptr__fec__st__t.html", "structcapi__rptr__fec__st__t" ],
    [ "lnktrn_status_t", "structlnktrn__status__t.html", "structlnktrn__status__t" ],
    [ "lane_status_t", "structlane__status__t.html", "structlane__status__t" ],
    [ "diag_status_t", "structdiag__status__t.html", "structdiag__status__t" ],
    [ "cw_status_t", "structcw__status__t.html", "structcw__status__t" ],
    [ "sticky_loss_t", "structsticky__loss__t.html", "structsticky__loss__t" ],
    [ "capi_lane_info_t", "structcapi__lane__info__t.html", "structcapi__lane__info__t" ],
    [ "capi_epdm_prbs_cfg_t", "structcapi__epdm__prbs__cfg__t.html", "structcapi__epdm__prbs__cfg__t" ],
    [ "capi_epdm_mode_cfg_set_info_t", "structcapi__epdm__mode__cfg__set__info__t.html", "structcapi__epdm__mode__cfg__set__info__t" ],
    [ "capi_epdm_archive_info_t", "structcapi__epdm__archive__info__t.html", "structcapi__epdm__archive__info__t" ],
    [ "fw_epdm_archive_storage_t", "structfw__epdm__archive__storage__t.html", "structfw__epdm__archive__storage__t" ],
    [ "capi_archive_info_t", "structcapi__archive__info__t.html", "structcapi__archive__info__t" ],
    [ "capi_cmis_info_t", "structcapi__cmis__info__t.html", "structcapi__cmis__info__t" ],
    [ "capi_shstgrm_info_t", "structcapi__shstgrm__info__t.html", "structcapi__shstgrm__info__t" ],
    [ "memory_info_t", "structmemory__info__t.html", "structmemory__info__t" ],
    [ "capi_cmis_snr_ltp_t", "structcapi__cmis__snr__ltp__t.html", "structcapi__cmis__snr__ltp__t" ],
    [ "memory_data_info_t", "structmemory__data__info__t.html", "structmemory__data__info__t" ],
    [ "dsp_mpi_cfg_info_t", "structdsp__mpi__cfg__info__t.html", "structdsp__mpi__cfg__info__t" ],
    [ "dsp_mpi_st_info_t", "structdsp__mpi__st__info__t.html", "structdsp__mpi__st__info__t" ],
    [ "dsp_mission_mpi_cfg_info_t", "structdsp__mission__mpi__cfg__info__t.html", "structdsp__mission__mpi__cfg__info__t" ],
    [ "serdes_diag_info_t", "structserdes__diag__info__t.html", "structserdes__diag__info__t" ],
    [ "capi_diag_command_request_info_t", "structcapi__diag__command__request__info__t.html", "structcapi__diag__command__request__info__t" ],
    [ "capi_command_info_t", "structcapi__command__info__t.html", "structcapi__command__info__t" ],
    [ "avs_default_info_t", "structavs__default__info__t.html", "structavs__default__info__t" ],
    [ "int_reg_default_info_t", "structint__reg__default__info__t.html", "structint__reg__default__info__t" ],
    [ "capi_chip_default_info_t", "structcapi__chip__default__info__t.html", "structcapi__chip__default__info__t" ],
    [ "ENABLE_FEC_PRBS", "capi__def_8h.html#a28e06b371c18872b86e28b0e179f0f57", null ],
    [ "SET_CONFIG", "capi__def_8h.html#a03c9510ad1cf49128f06fbc48bb5a51d", null ],
    [ "GET_CONFIG", "capi__def_8h.html#a43a953ce63e5f94de4e8e24355391d52", null ],
    [ "CAPI_VERSION", "capi__def_8h.html#a83faf2349d58e9eb0837e6cdf96ffed7", null ],
    [ "CAPI_MAX_PORTS", "capi__def_8h.html#ae6b5f922929f0e24c5d7c96282663ae8", null ],
    [ "CORE_READY_TIMER", "capi__def_8h.html#a3974997bd76efcf417a3c4ac990f7a12", null ],
    [ "FEC_FLN_NUM", "capi__def_8h.html#ab4d6289f338c9d453b33a2e092708798", null ],
    [ "PORT_CFG_LANE_RDY_TIMEOUT", "capi__def_8h.html#a4ed765dde47827a45a8b39c40aa05a89", null ],
    [ "QSFP_I2C_SLAVE_ADDRESS_DEFAULT", "capi__def_8h.html#aac5e7598d5b4ab7064597f2148459063", null ],
    [ "FAST_SPI_PROGRAM_WITH_BLOCK_WRITE", "capi__def_8h.html#aa7d0f27bd97961b1e5ecb98c71abd4e5", null ],
    [ "I2C_BLOCK_WRITE_SIZE", "capi__def_8h.html#a0be84ef12bceb06db6af1e20dbfeb2c8", null ],
    [ "CAPI_NUM_DFE_TAPS", "capi__def_8h.html#a68f0e2365ce298163c62e37b95f8162e", null ],
    [ "CAPI_RPC_CMD_FEC_STATISTICS", "capi__def_8h.html#a01d3be62eeea957eff174cce69026a31", null ],
    [ "ENABLE_FEC_ST", "capi__def_8h.html#a9a7c22cd7cc8b006391b0332c1e25c05", null ],
    [ "ENABLE_CMIS_INTF", "capi__def_8h.html#aca6f7248b3481b619a79f729b01ff4cf", null ],
    [ "KP4PRBS_ERR_INJECT", "capi__def_8h.html#a89f36266b7a8b277ef27d5180de30951", null ],
    [ "PURELY_CAPI_SET_VDDM", "capi__def_8h.html#adb6576fc961e0d0936d72fed2117a3c6", null ],
    [ "CHIP_VOLTAGE_SET_FLAG", "capi__def_8h.html#ae4c66b7d3b2cc09075bedb1f66208651", null ],
    [ "CHIP_VOLTAGE_GET_STATUS", "capi__def_8h.html#a32f2eda3c50d9973f44e981878be3454", null ],
    [ "MAX_PRTAD", "capi__def_8h.html#a1c832a712d2bc86c4bd5520c593e5321", null ],
    [ "MAX_LW_LANES", "capi__def_8h.html#a8b3125e80b9c246c8f735c1917b90850", null ],
    [ "MAX_BH_LANES", "capi__def_8h.html#a05e1ba27d5809415a3241c8f66b9e914", null ],
    [ "MAX_LANES", "capi__def_8h.html#a271c22683beda12335d9077cbbfe5732", null ],
    [ "MAX_PORT", "capi__def_8h.html#a401e1a60d6381236216b6a130a6685bd", null ],
    [ "__HSIP_FUNC_ERR", "capi__def_8h.html#a6ac1815a96a31e2e0d5b94e7d312c455", null ],
    [ "FEC_FLN_NUM", "capi__def_8h.html#ab4d6289f338c9d453b33a2e092708798", null ],
    [ "FEC_TOT_BITS_CORR_NUM", "capi__def_8h.html#a016d97df7a80a5644a1052bd00e92155", null ],
    [ "FEC_TOT_FRAMES_ERR_NUM", "capi__def_8h.html#a34675c72e95ccd67eae427f915b087ae", null ],
    [ "HW_MUTEX_INVALID", "capi__def_8h.html#aad15517b380fbdad5e090a7a89fadd64", null ],
    [ "capi_phy_info_t", "capi__def_8h.html#a6c0d038c73afd5756acbfb94a2fa3296", null ],
    [ "capi_lnktrn_info_t", "capi__def_8h.html#aa548f35a4148b16bce8fda6c189b1d02", null ],
    [ "capi_lane_cdr_mode_t", "capi__def_8h.html#a9386c5a0b5036ea00091c3cfe6db473b", null ],
    [ "capi_phy_side_t", "capi__def_8h.html#a6880328d9cdf4a76fd3c3fa218f4b4d9", [
      [ "PHY_HOST_SIDE", "capi__def_8h.html#a6880328d9cdf4a76fd3c3fa218f4b4d9a8f9f07173431a026acd0643eb6770939", null ],
      [ "PHY_MEDIA_SIDE", "capi__def_8h.html#a6880328d9cdf4a76fd3c3fa218f4b4d9ad38fca1e896e90d332e32edec49530b1", null ],
      [ "PHY_BOTH_SIDES", "capi__def_8h.html#a6880328d9cdf4a76fd3c3fa218f4b4d9aee91f5689dd82e0f6f6b4cffca5fe7dc", null ]
    ] ],
    [ "capi_enable_t", "capi__def_8h.html#a2634c6edc4221b9f479bde04f5eb10fb", [
      [ "CAPI_DISABLE", "capi__def_8h.html#a2634c6edc4221b9f479bde04f5eb10fba24d2becd86f202dacee64e4af36471dd", null ],
      [ "CAPI_ENABLE", "capi__def_8h.html#a2634c6edc4221b9f479bde04f5eb10fba08ed4ed049a7afa5b095c024bff8889d", null ]
    ] ],
    [ "capi_switch_t", "capi__def_8h.html#a0ff8bc50936086feff6db155d0efae9f", [
      [ "CAPI_SWITCH_OFF", "capi__def_8h.html#a0ff8bc50936086feff6db155d0efae9fa01b00978fd09cdd396a67e4d3e1d0f4a", null ],
      [ "CAPI_SWITCH_ON", "capi__def_8h.html#a0ff8bc50936086feff6db155d0efae9faf0c77a971fced74ebdec5196e24f9a86", null ],
      [ "CAPI_SWITCH_TOGGLE", "capi__def_8h.html#a0ff8bc50936086feff6db155d0efae9fa5d8d3fe2e2e686f80c140593a2f86719", null ]
    ] ],
    [ "capi_voltage_t", "capi__def_8h.html#a2cdb67b24c9e9b65724b4459570b1648", [
      [ "CAPI_ZERO", "capi__def_8h.html#a2cdb67b24c9e9b65724b4459570b1648af44b5a11865662fd524a547d7086f2da", null ],
      [ "CAPI_ZERO_P_SEVEN_FIVE_MLV", "capi__def_8h.html#a2cdb67b24c9e9b65724b4459570b1648a887ba79fb0ec6539d32c4a3534812d5a", null ],
      [ "CAPI_ZERO_P_NINE_MLV", "capi__def_8h.html#a2cdb67b24c9e9b65724b4459570b1648a6956345b6ef37e844be3fea8f1fb5466", null ],
      [ "CAPI_ONE_P_TWO_MLV", "capi__def_8h.html#a2cdb67b24c9e9b65724b4459570b1648a25f715aff7d4bbdcf6fc62067b4a915c", null ],
      [ "CAPI_DEFAULT", "capi__def_8h.html#a2cdb67b24c9e9b65724b4459570b1648a70520943c9e64f854158c5ceaa2a3fd5", null ]
    ] ],
    [ "capi_polarity_action_t", "capi__def_8h.html#ad1d7d75958b9d4f383b3075a2f85e937", [
      [ "POLARITY_DEFAULT_SETTING", "capi__def_8h.html#ad1d7d75958b9d4f383b3075a2f85e937ac2695a898bc2d16e05c5f42605572579", null ],
      [ "POLARITY_INVERT_DEFAULT_SETTING", "capi__def_8h.html#ad1d7d75958b9d4f383b3075a2f85e937a37134ad3f90195b914e7efe347234d75", null ],
      [ "POLARITY_SWAP_CURRENT_SETTING", "capi__def_8h.html#ad1d7d75958b9d4f383b3075a2f85e937afe5b724400393a84bf8f0e385f371266", null ]
    ] ],
    [ "capi_direction_t", "capi__def_8h.html#a5318deb316d9021645b0d3542ae19be0", [
      [ "DIR_INGRESS", "capi__def_8h.html#a5318deb316d9021645b0d3542ae19be0af46cd498821270c1fc3f21a2ebd9606d", null ],
      [ "DIR_EGRESS", "capi__def_8h.html#a5318deb316d9021645b0d3542ae19be0a022d4e4e332e1acce9f575ede2c96c97", null ],
      [ "DIR_BOTH", "capi__def_8h.html#a5318deb316d9021645b0d3542ae19be0a910ed574248f08b08257886986a34116", null ]
    ] ],
    [ "capi_line_lock_t", "capi__def_8h.html#a29f37eeabbb0802638005653362a43ed", [
      [ "CAPI_LINE_UNLOCKED", "capi__def_8h.html#a29f37eeabbb0802638005653362a43eda99933acdfa322219e0a2b5b70e87501f", null ],
      [ "CAPI_LINE_LOCKED", "capi__def_8h.html#a29f37eeabbb0802638005653362a43edaf958b3865f62e1ca70258339920aefa6", null ]
    ] ],
    [ "capi_cdr_lock_t", "capi__def_8h.html#a4ced93a503924ecb37339bcc644616cd", [
      [ "CAPI_CDR_UNLOCKED", "capi__def_8h.html#a4ced93a503924ecb37339bcc644616cdad9a18ce50f8feb7fb63ed4750a48e176", null ],
      [ "CAPI_CDR_LOCKED", "capi__def_8h.html#a4ced93a503924ecb37339bcc644616cda0795f3a16c8cd43dee642c8d71297f41", null ]
    ] ],
    [ "capi_los_t", "capi__def_8h.html#a910742473bc1653cfda6cf9e8c4eeca4", [
      [ "CAPI_NO_LOSE_OF_SIGNAL", "capi__def_8h.html#a910742473bc1653cfda6cf9e8c4eeca4a81616c66ec8dece7fb7b4a34495e1aa6", null ],
      [ "CAPI_LOSE_OF_SIGNAL", "capi__def_8h.html#a910742473bc1653cfda6cf9e8c4eeca4a81c562c1c5b981308e7cf539e6ffa8c2", null ]
    ] ],
    [ "capi_pll_lock_t", "capi__def_8h.html#ac5ba38ded506061bd1dd7951a5a97ba0", [
      [ "CAPI_PLL_OUT_OF_LOCK", "capi__def_8h.html#ac5ba38ded506061bd1dd7951a5a97ba0a41ab250acd7e0aa85746f33aaecb1e11", null ],
      [ "CAPI_PLL_LOCKED", "capi__def_8h.html#ac5ba38ded506061bd1dd7951a5a97ba0ab9041636ac52b2b11de187fdd95c1041", null ]
    ] ],
    [ "capi_lnktrn_type_t", "capi__def_8h.html#a130be97b99d7d7f4cb12e2b63ef6b93a", [
      [ "LNKTRN_CL72", "capi__def_8h.html#a130be97b99d7d7f4cb12e2b63ef6b93aa14e58c922f63459c2c72b32e53861ee3", null ],
      [ "LNKTRN_CL93", "capi__def_8h.html#a130be97b99d7d7f4cb12e2b63ef6b93aadfb72b30a5d9cb8393d79b9dad866e02", null ],
      [ "LNKTRN_802_3CD", "capi__def_8h.html#a130be97b99d7d7f4cb12e2b63ef6b93aad379645eeca0285655426e9e48fdf716", null ],
      [ "LNKTRN_TYPE_MAX", "capi__def_8h.html#a130be97b99d7d7f4cb12e2b63ef6b93aa9fc15b190b8042ef24a473dbb2eef2df", null ]
    ] ],
    [ "capi_lnktrn_frm_size_t", "capi__def_8h.html#a1e05496131c34f52e64df56fd62fdecd", [
      [ "LNKTRN_FRM_SIZE_4K", "capi__def_8h.html#a1e05496131c34f52e64df56fd62fdecda426b2756a010ee417b93a4f4dbde32e3", null ],
      [ "LNKTRN_FRM_SIZE_16K", "capi__def_8h.html#a1e05496131c34f52e64df56fd62fdecda7e799f54bdb939f2279e529c500ed575", null ],
      [ "LNKTRN_FRM_SIZE_MAX", "capi__def_8h.html#a1e05496131c34f52e64df56fd62fdecda58901cf7b81c01497beb5bc33192ad3e", null ]
    ] ],
    [ "capi_lnktrn_init_cond_t", "capi__def_8h.html#a58902441f8cd2fc469fb4852a70665b8", [
      [ "LNKTRN_INIT_COND_NO", "capi__def_8h.html#a58902441f8cd2fc469fb4852a70665b8af6f7824e25062336513fe4fb984bd500", null ],
      [ "LNKTRN_INIT_COND_NO_EQ", "capi__def_8h.html#a58902441f8cd2fc469fb4852a70665b8aaefc896e35847147fde0830be2721b4e", null ],
      [ "LNKTRN_INIT_COND_MAX", "capi__def_8h.html#a58902441f8cd2fc469fb4852a70665b8a9a4ca7e4fec823bdb2b2d0a26b834e08", null ]
    ] ],
    [ "capi_lnktrn_cl72_cl93_init_t", "capi__def_8h.html#a9e2531695d83fc65da381bdeeb562555", [
      [ "LNKTRN_CL_INIT_NORMAL", "capi__def_8h.html#a9e2531695d83fc65da381bdeeb562555a0bf8b0b6d00456bfe5332a0e80ad507f", null ],
      [ "LNKTRN_CL_INIT_EN", "capi__def_8h.html#a9e2531695d83fc65da381bdeeb562555acf5af1ed0b74887829d7db8fd9c8be0d", null ],
      [ "LNKTRN_CL_INIT_MAX", "capi__def_8h.html#a9e2531695d83fc65da381bdeeb562555ad99842911527d1039f2dda7fa6bf459d", null ]
    ] ],
    [ "capi_lnktrn_cl72_cl93_preset_t", "capi__def_8h.html#a0ce8b45d664cf23a445109c9a59df022", [
      [ "LNKTRN_CL_PRESET_NORMAL", "capi__def_8h.html#a0ce8b45d664cf23a445109c9a59df022afdc827003c8f99509b72d72dc4eee444", null ],
      [ "LNKTRN_CL_PRESET_EN", "capi__def_8h.html#a0ce8b45d664cf23a445109c9a59df022a3e15d96410eb023872facd98a62900af", null ],
      [ "LNKTRN_CL_PRESET_MAX", "capi__def_8h.html#a0ce8b45d664cf23a445109c9a59df022a1f7d611e7927f44a2cf7bcc05119bb50", null ]
    ] ],
    [ "capi_link_training_timer_t", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573f", [
      [ "CAPI_LINK_TRAIN_TIMER_DEFAULT", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa6458e3cfcaa44dfc23fe51e364ea3629", null ],
      [ "CAPI_LINK_TRAIN_TIMER_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fabc30bdb41c1fec8e0a1e6fd0a9aefad2", null ],
      [ "CAPI_LINK_TRAIN_TIMER_1S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa52b25cb6ec3a8a65f90d68e85f792f18", null ],
      [ "CAPI_LINK_TRAIN_TIMER_1S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fae31f320458c026a2e82c275bb74053d5", null ],
      [ "CAPI_LINK_TRAIN_TIMER_2S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fac0f34ec52f0a6317f5a886a8402e14ef", null ],
      [ "CAPI_LINK_TRAIN_TIMER_2S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa5844c2eabe347825a5ce7df9e4a71f93", null ],
      [ "CAPI_LINK_TRAIN_TIMER_3S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa20df152f7c458bf47da70df06de0fc5e", null ],
      [ "CAPI_LINK_TRAIN_TIMER_3S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573faca1e6dd7be92e4239d873c2abe1b909e", null ],
      [ "CAPI_LINK_TRAIN_TIMER_4S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fae8756cb56594ee61c012d53acdcedbc0", null ],
      [ "CAPI_LINK_TRAIN_TIMER_4S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fac04c1c41f694f8d907b6fa4c3f8c781f", null ],
      [ "CAPI_LINK_TRAIN_TIMER_5S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa5c590f550adc410b0a93a650f4adf9df", null ],
      [ "CAPI_LINK_TRAIN_TIMER_5S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa005a2b42be4a82f096bd731f99b7ba96", null ],
      [ "CAPI_LINK_TRAIN_TIMER_6S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa9bdea180d29dc08fc43132823a4d6dc8", null ],
      [ "CAPI_LINK_TRAIN_TIMER_6S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa2e0cfada1e541468ccfad38660f47071", null ],
      [ "CAPI_LINK_TRAIN_TIMER_7S", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa8e980d453d60887e7c8b6d15649346b3", null ],
      [ "CAPI_LINK_TRAIN_TIMER_7S_500MS", "capi__def_8h.html#a484a99a48043eb0cea9a07966efe573fa2ff4efc6fb5c389ae5d2aaf85a819a56", null ]
    ] ],
    [ "capi_lnktrn_err_t", "capi__def_8h.html#a9640847ac0c3531871e21b3dd3aea3e7", [
      [ "LNKTRN_ERR_NONE", "capi__def_8h.html#a9640847ac0c3531871e21b3dd3aea3e7ae3bfe4fb1c91abbd27ee5ce86e7fbd57", null ],
      [ "LNKTRN_ERR_FRM_LCK", "capi__def_8h.html#a9640847ac0c3531871e21b3dd3aea3e7a31571c03afabb0ee9703aed162c86c2a", null ],
      [ "LNKTRN_ERR_SNR", "capi__def_8h.html#a9640847ac0c3531871e21b3dd3aea3e7a1165204f1cb71f9a950d486ab52e457b", null ],
      [ "LNKTRN_ERR_TIMEOUT", "capi__def_8h.html#a9640847ac0c3531871e21b3dd3aea3e7a7e211b265cf2028fc92f7a017092727f", null ],
      [ "LNKTRN_ERR_MAX", "capi__def_8h.html#a9640847ac0c3531871e21b3dd3aea3e7a5df4ce697a442b7e03cb4b146584d4c2", null ]
    ] ],
    [ "capi_linktrn_done_t", "capi__def_8h.html#aca09405832461e420210a3ad2581875e", [
      [ "CAPI_LINKED_TRAINING_NOT_COMPLETED", "capi__def_8h.html#aca09405832461e420210a3ad2581875eabb54f12477da1e712a411b0b0182372f", null ],
      [ "CAPI_LINKED_TRAINING_SUCCESSFULLU_COMPLETED", "capi__def_8h.html#aca09405832461e420210a3ad2581875ea31213b44b7a322f5afed8e364ba45c07", null ]
    ] ],
    [ "capi_adc_sig_clipping_t", "capi__def_8h.html#ac1bb0cf74e5ee500c2b86a2d6a2c21be", [
      [ "CAPI_NO_ADC_SIGNAL_CLIPPING_DETECTED", "capi__def_8h.html#ac1bb0cf74e5ee500c2b86a2d6a2c21beab5b6a8ca9c1638335494d7f849ca715c", null ],
      [ "CAPI_ADC_SIGNAL_CLIPPING_DETECTED", "capi__def_8h.html#ac1bb0cf74e5ee500c2b86a2d6a2c21beab25bd2c704117ba76b0bbe6b105a2d18", null ]
    ] ],
    [ "capi_client_pll_unlock_sticky_t", "capi__def_8h.html#ac6ac8db4545dfe500f30739e11ebd2ca", [
      [ "CAPI_PLL_UNLOCK_STICKY_NOT_LOST_LOCK", "capi__def_8h.html#ac6ac8db4545dfe500f30739e11ebd2caa52ce7a81c3bf9cb9507e4953870fdcf0", null ],
      [ "CAPI_PLL_UNLOCK_STICKY_LOST_LOCK", "capi__def_8h.html#ac6ac8db4545dfe500f30739e11ebd2caa37c3f1eef998c203820700a5f9529d6c", null ]
    ] ],
    [ "capi_client_bh2bh_fifo_status_t", "capi__def_8h.html#a0c6a6cc5de2d0554e8f32a45fd18a224", [
      [ "CAPI_BH2BH_FIFO_STATUS_NO_COLLISION", "capi__def_8h.html#a0c6a6cc5de2d0554e8f32a45fd18a224a0b062d3ce716734f179da6b200e17697", null ],
      [ "CAPI_BH2BH_FIFO_STATUS_IN_COLLISION", "capi__def_8h.html#a0c6a6cc5de2d0554e8f32a45fd18a224aee7a237ca2309a6841d3b4c53edf2e3e", null ]
    ] ],
    [ "capi_bh_txfir_tap_enable_t", "capi__def_8h.html#aa75a9928e629d9397f421fc830f35975", [
      [ "CAPI_NRZ_LP_3TAP", "capi__def_8h.html#aa75a9928e629d9397f421fc830f35975a9acf0b3c70cf63e65e90e4f5a6ba1165", null ],
      [ "CAPI_NRZ_6TAP", "capi__def_8h.html#aa75a9928e629d9397f421fc830f35975a1532ec6d96523c125de7d26cbbcf5a62", null ],
      [ "CAPI_PAM4_LP_3TAP", "capi__def_8h.html#aa75a9928e629d9397f421fc830f35975a084b8849d4743354e08d888ec7a27a7d", null ],
      [ "CAPI_PAM4_6TAP", "capi__def_8h.html#aa75a9928e629d9397f421fc830f35975a7b863008884663a63165d7533dd86531", null ]
    ] ],
    [ "capi_lw_tx_precode_indep_t", "capi__def_8h.html#a93b6145af4fa793e788df27b3419fd07", [
      [ "CAPI_LW_INDEP_TX_PRECODE_DEFAULT", "capi__def_8h.html#a93b6145af4fa793e788df27b3419fd07ac77d16c8ea91068a6b0d40fca2389a8b", null ],
      [ "CAPI_LW_INDEP_TX_PRECODE_OFF", "capi__def_8h.html#a93b6145af4fa793e788df27b3419fd07a9d619657f3435684806b05186c9351ce", null ],
      [ "CAPI_LW_INDEP_TX_PRECODE_ON", "capi__def_8h.html#a93b6145af4fa793e788df27b3419fd07a1eebc5ea501bf5c848904d69c450ddef", null ]
    ] ],
    [ "capi_media_type_t", "capi__def_8h.html#aba7a5cd75af84a6d56c4696d7f7d32ce", [
      [ "CAPI_MEDIA_TYPE_OPTICAL", "capi__def_8h.html#aba7a5cd75af84a6d56c4696d7f7d32cea3f11a4675ff000a0e48155d25498cd1d", null ],
      [ "CAPI_MEDIA_TYPE_PCB_TRACE_BACK_PLANE", "capi__def_8h.html#aba7a5cd75af84a6d56c4696d7f7d32cea80208b997d17c15f4046441c4aa12883", null ],
      [ "CAPI_MEDIA_TYPE_COPPER_CABLE", "capi__def_8h.html#aba7a5cd75af84a6d56c4696d7f7d32ceab90846d2fbc58e3c46b27d2a7234245d", null ]
    ] ],
    [ "lane_cdr_mode_t", "capi__def_8h.html#a7bacfa31590fa347fe28f5d40739c16c", [
      [ "LANE_CDR_MODE_OSCDR_FORCE_ENABLE", "capi__def_8h.html#a7bacfa31590fa347fe28f5d40739c16ca7ff5552e0a87235ea3ba41001bb44109", null ],
      [ "LANE_CDR_MODE_OSCDR_FORCE_DISABLE", "capi__def_8h.html#a7bacfa31590fa347fe28f5d40739c16ca36831402c52a7c0c74418b47f7fedc0b", null ],
      [ "LANE_CDR_MODE_BRCDR_FORCE_ENABLE", "capi__def_8h.html#a7bacfa31590fa347fe28f5d40739c16ca96a279ea58bebbebc0bcaca35d8636ce", null ],
      [ "LANE_CDR_MODE_BRCDR_FORCE_DISABLE", "capi__def_8h.html#a7bacfa31590fa347fe28f5d40739c16ca2d47889c44215ddb4497f14500442db8", null ],
      [ "LANE_CDR_MODE_CDR_FORCE_DISABLE", "capi__def_8h.html#a7bacfa31590fa347fe28f5d40739c16caef26fc53fd071ce5c3cb46fd1c536693", null ]
    ] ],
    [ "lane_config_type_t", "capi__def_8h.html#aaf670f39db366fc7db8da322c7010945", [
      [ "LANE_CONFIG_TYPE_NONE", "capi__def_8h.html#aaf670f39db366fc7db8da322c7010945a2092c73b8c6fe95166a59832af9b6ee6", null ],
      [ "LANE_CONFIG_TYPE_LANE_RX_INFO", "capi__def_8h.html#aaf670f39db366fc7db8da322c7010945ac7af11c0820d43556894ccbc51c281aa", null ],
      [ "LANE_CONFIG_TYPE_LANE_TX_INFO", "capi__def_8h.html#aaf670f39db366fc7db8da322c7010945abed48c0408594f43acaa8a1fab36dd79", null ],
      [ "LANE_CONFIG_TYPE_LANE_LINK_TRAINING_INFO", "capi__def_8h.html#aaf670f39db366fc7db8da322c7010945af8cd2f5c44fd7da803a27db95ed0d978", null ]
    ] ],
    [ "capi_loopback_mode_t", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450", [
      [ "CAPI_GLOBAL_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a2e1a436f41ad6bd3383cc8efa7ffaa8f", null ],
      [ "CAPI_GLOBAL_PMD_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a89fafb4d72a7974000144c2b4ccde11b", null ],
      [ "CAPI_SYSTEM_GLOBAL_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450abac368c223e50c44bdee830849d03094", null ],
      [ "CAPI_SYSTEM_GLOBAL_PMD_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a7a72eca2081c37cca7938d8b63dfb363", null ],
      [ "CAPI_REMOTE_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a2b05aef4389d10713cb7fc2553064252", null ],
      [ "CAPI_REMOTE_PMD_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a33698f8370ebf263adff6e2318ab8203", null ],
      [ "CAPI_SYSTEM_REMOTE_PMD_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450adff4697a9fac50c7d0c9f2b3b78542c9", null ],
      [ "CAPI_GLOBAL_PCS_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a9a4d8ce5f197e0cc4a590f8fa8eb378e", null ],
      [ "CAPI_REMOTE_PCS_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a69a117df10b0c93cb523f4d46ec795a7", null ],
      [ "CAPI_SYSTEM_GLOBAL_PCS_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a0605632e9eaa30022ea676075c04d5c4", null ],
      [ "CAPI_SYSTEM_REMOTE_PCS_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450a2a17e9a38f0579dc49516250b4e3d680", null ],
      [ "CAPI_LAST_LOOPBACK_MODE", "capi__def_8h.html#a11ed0a7c8a65e6156f519d10f0769450ab7a34caa2b290da541c9573df6b089a6", null ]
    ] ],
    [ "capi_pattern_gen_mon_type_t", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdf", [
      [ "CAPI_PRBS_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa400e2dceb8af42e5198ecc1baa3863c3", null ],
      [ "CAPI_PRBS_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfaf614abfed0e5b142da04d0033ef4128b", null ],
      [ "CAPI_PRBS_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfab5f0d45e9ce4abf37524e1bf37d67d37", null ],
      [ "CAPI_PRBS_SSPRQ_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfaa980b90be8acd1f5180902889af9f003", null ],
      [ "CAPI_PRBS_SSPRQ_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfad33431f516eb6697530b8736c2576b28", null ],
      [ "CAPI_PRBS_SSPRQ_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa3907e7ef7ba8f2469c493594bd4adcb2", null ],
      [ "CAPI_PRBS_Q_PRBS_13_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfaaff8ec4a683004f4de2e2f5ee287ed85", null ],
      [ "CAPI_PRBS_Q_PRBS_13_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfacd516f56019749f1c564999083557de0", null ],
      [ "CAPI_PRBS_Q_PRBS_13_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa49ac80b35968d00a4ee653fdcf240963", null ],
      [ "CAPI_PRBS_SQUARE_WAVE_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa728ec57472794c12b707a92a5232b856", null ],
      [ "CAPI_PRBS_TX_LINEARITY_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa2109949b6aba33fc5c4cd33ac8713985", null ],
      [ "CAPI_PRBS_SHARED_TX_PATTERN_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfad74cb7efa928bdc08f5441a2092b85a4", null ],
      [ "CAPI_PRBS_FEC_INGRESS_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa0495825539cc6ac9986afb04f4b9df04", null ],
      [ "CAPI_PRBS_FEC_INGRESS_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa428a108e92e5055910e53c1a57de216f", null ],
      [ "CAPI_PRBS_FEC_INGRESS_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfaf6860727764eed080b6f11b1f77e25de", null ],
      [ "CAPI_PRBS_FEC_EGRESS_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfadbeadef8fe186b14d56172034d0b84a9", null ],
      [ "CAPI_PRBS_FEC_EGRESS_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa6e08ec23cc847823d04b154865f7e4f8", null ],
      [ "CAPI_PRBS_FEC_EGRESS_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa0b3097c9a840179f7def7266f968f345", null ],
      [ "CAPI_PRBS_KP4_HOST_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa65d64fa30520b7deaf0770a5c05f1771", null ],
      [ "CAPI_PRBS_KP4_HOST_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa5ae0c679531e322a178ad3855eb4633c", null ],
      [ "CAPI_PRBS_KP4_HOST_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfab8288b047959452912c7fe72fc64ce6a", null ],
      [ "CAPI_PRBS_KP4_MEDIA_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa2b7eb238f872d62124b48be58ebdd891", null ],
      [ "CAPI_PRBS_KP4_MEDIA_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa1ca5b295e01fdf23096849498f73548a", null ],
      [ "CAPI_PRBS_KP4_MEDIA_GEN_MON", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfade30197f9337e8bd4ff40b6cb8bdfed0", null ],
      [ "CAPI_PRBS_JP03B", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa4f41ab1dfaf3f1a40f44365c0a5febde", null ],
      [ "CAPI_PRBS_STAIRCASE_PATTERN", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa81d6ae5cd1ce8f69be1c265759d30646", null ],
      [ "CAPI_PRBS_CW_GENERATOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa1a98594737eda50587b829467bfac71e", null ],
      [ "CAPI_PRBS_CW_MONITOR", "capi__def_8h.html#a3130dbede4b732639d417f81ff9f3cdfa542767e6c5de8a94f45adceb21261d7a", null ]
    ] ],
    [ "capi_bh_prbs_poly_t", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90c", [
      [ "CAPI_BH_PRBS_POLY_7", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca18e37957cbd886fd02ae7de428d4105c", null ],
      [ "CAPI_BH_PRBS_POLY_9", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca19163759df9d3c7a4be9f759a6249eef", null ],
      [ "CAPI_BH_PRBS_POLY_11", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90cac9990ad7cd74aa74673947c335ddc791", null ],
      [ "CAPI_BH_PRBS_POLY_15", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90cacbad0721eab9176977f9f912cde379f4", null ],
      [ "CAPI_BH_PRBS_POLY_23", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca8965b2f2bed3df89e72f03443bb120b4", null ],
      [ "CAPI_BH_PRBS_POLY_31", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90cadea6163881c497e39e0f02a600d4af1c", null ],
      [ "CAPI_BH_PRBS_POLY_58", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90caf84dfc0ccca16932be030ff9f003d54d", null ],
      [ "CAPI_BH_PRBS_POLY_49", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca38522fe4b4299b3a673b4987350f9844", null ],
      [ "CAPI_BH_PRBS_POLY_10", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90cace404503fb2869f79bcf7d32839b6d6a", null ],
      [ "CAPI_BH_PRBS_POLY_20", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90cae8797576b62d1c0c08d45e8c19badc32", null ],
      [ "CAPI_BH_PRBS_POLY_13", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca5777cab4be30c3611754117cba8b8ac4", null ],
      [ "CAPI_BH_PRBS_USER_40_BIT_REPEAT", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca9bfefb7ae66184b06f65e46c10619333", null ],
      [ "CAPI_BH_PRBS_PRBS_AUTO_DETECT", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca59347b4f2576789c59e7fd67cdf75678", null ],
      [ "CAPI_BH_PCS_PRBS_7", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca3dc95d797f4aba2f69518d90c70390b3", null ],
      [ "CAPI_BH_PCS_PRBS_15", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90ca319e61b6253a7c097353e07b8b848b4c", null ],
      [ "CAPI_BH_PCS_PRBS_23", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90caa9250f635623cd80768e800837976ef4", null ],
      [ "CAPI_BH_PCS_PRBS_31", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90cadad2c212271b9d9528259979794b9d13", null ],
      [ "CAPI_BH_PRBS_PRBS_UNKNOWN", "capi__def_8h.html#a1c38829e8f0bb2a84d2697f12ebcf90caac13f4eb6f038457b7672c4ea5fd4ab5", null ]
    ] ],
    [ "capi_bh_prbs_checker_mode_t", "capi__def_8h.html#aa2ffa430f09ab557d092523f03f8d516", [
      [ "CAPI_BH_PRBS_SELF_SYNC_HYSTERESIS", "capi__def_8h.html#aa2ffa430f09ab557d092523f03f8d516ac8fdbed3719f493ae8cb31278e0896de", null ],
      [ "CAPI_BH_PRBS_INITIAL_SEED_HYSTERESIS", "capi__def_8h.html#aa2ffa430f09ab557d092523f03f8d516a8a941b1e955a9639335a37734fedb238", null ],
      [ "CAPI_BH_PRBS_INITIAL_SEED_NO_HYSTERESIS", "capi__def_8h.html#aa2ffa430f09ab557d092523f03f8d516a0fb9953ded7b58faba3be370e0351786", null ]
    ] ],
    [ "capi_fec_prbs_gen_poly_t", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815", [
      [ "CAPI_FEC_PRBS_GEN_POLY_7", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815adcf679b61f20b68bcff035e26309d51e", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_9", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815ac8c53b6cab5e3787be4153d90f2a58f7", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_10", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a3c24059d8fa7e122897900f850700403", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_11", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815abda0e0c4c0f0c4bd6d4d09f0ea214858", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_13", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a48e10bec3412c3e2eb30c4bb6def34e0", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_15", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815ac8173a1da9505524b627f9d3cb61aa11", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_20", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a682c23d66223c81c8706ac5591854a15", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_23", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a3cdc9673559801b57d0a2ec427afc2f6", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_31", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a5dd58038f266a84c5a56ff00ebcd5930", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_49", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a36f7edd5b7c89cf8b0274c7bd8cee0b3", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_58", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a2f98ef1b9e0430417d34239b5279bc5d", null ],
      [ "CAPI_FEC_PRBS_GEN_POLY_COUNT", "capi__def_8h.html#afba9bbd8def686a2c10719c281eda815a13d7c039c66042c7caf62245521f9fde", null ]
    ] ],
    [ "capi_prbs_poly_type_t", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138", [
      [ "CAPI_PRBS_POLY_7", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a0f3b3314f3a50ce03f24646ea54e0d51", null ],
      [ "CAPI_PRBS_POLY_9", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a251f418c5627f8fc49eefd9f2db554ff", null ],
      [ "CAPI_PRBS_POLY_10", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138aebe536fce3b13662c03eec78dfc23bf6", null ],
      [ "CAPI_PRBS_POLY_11", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138af5c10a6bfd703a3eceebe93320b7f542", null ],
      [ "CAPI_PRBS_POLY_13", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a9a62ee771d88294ef35e322e4dea1927", null ],
      [ "CAPI_PRBS_POLY_15", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a0354a9df59f565aa8ada2356c8681cf5", null ],
      [ "CAPI_PRBS_POLY_20", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a66996162fec9ca8fe45c4823bba0509b", null ],
      [ "CAPI_PRBS_POLY_23", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a343cdcbf0a2f6d9b4627862298bc4baf", null ],
      [ "CAPI_PRBS_POLY_31", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a3cfcb498114e609a9b93da5f22c5bf9f", null ],
      [ "CAPI_PRBS_POLY_49", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138afa9a2452a2b4beaa1e01f8735165b1ae", null ],
      [ "CAPI_PRBS_POLY_58", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138ad97ffff4cbcbda6a207bd50b81965a58", null ],
      [ "CAPI_PRBS_POLY_COUNT", "capi__def_8h.html#a5d49981d0db0c2b46cb0788cf5e8e138a4374d630b0a05c48e0eea62f0c16eee5", null ]
    ] ],
    [ "square_wave_pattern_t", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7d", [
      [ "ONE_1_CONSECUTIVE", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7da0c671cb91d79e626d573374f7e8f892d", null ],
      [ "ONE_2_CONSECUTIVE", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7da8a07c9b9fef8a43095e743639e1fe41a", null ],
      [ "ONE_4_CONSECUTIVE", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7dae49ca7c6d132d218b541cd59a1e14689", null ],
      [ "ONE_8_CONSECUTIVE", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7daa09b776b36f2958075e55410a2072057", null ],
      [ "ONE_16_CONSECUTIVE", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7da100a649d36937385de77af792377d577", null ],
      [ "SQR_WAVE_PATTEN_MAX", "capi__def_8h.html#a6889d05ec9d7ea89e6a95a6d3f041c7da114eddd976a195347622924a261db293", null ]
    ] ],
    [ "capi_prbs_err_inj_type_t", "capi__def_8h.html#aac400b013cee708cf381121d8d053ce5", [
      [ "PRBS_ERR_INJ_LSB", "capi__def_8h.html#aac400b013cee708cf381121d8d053ce5a36683cf76e118ea14da7b2c96a912da0", null ],
      [ "PRBS_ERR_INJ_MSB", "capi__def_8h.html#aac400b013cee708cf381121d8d053ce5af019f870491a1397d956e9f4db450252", null ]
    ] ],
    [ "capi_status_type_t", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293", [
      [ "GPR_LANE_CDR_LOCK_STATUS", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293a3b0ebadfcdb95dd9b780a553d6d96f84", null ],
      [ "GPR_LANE_LOL_LOS_STATUS", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293adda816389ba02781f30f2e940fdf0faf", null ],
      [ "GPR_LANE_SIGDET_STATUS", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293a43a0a711aaaa12a7950ef6ff86ce3b02", null ],
      [ "GPR_LANE_TX_SQUELCH_STATUS", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293a9392174ab4b0df747a074486c8a55ecc", null ],
      [ "GPR_LANE_CDR_RESTART_COUNTER", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293a977a6c946c6161a927b452d59bb0d3f0", null ],
      [ "GPR_LANE_RX_OUTPUT_STATUS", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293af85142719f926a9043107dabdc6d4595", null ],
      [ "GPR_LANE_RX_OUTPUT_LATCH_STATUS", "capi__def_8h.html#a0efcc2385932ecf2842cbe5510e70293a358490cea075be6f8f86b5a6fca9f4f0", null ]
    ] ],
    [ "phy_command_id_t", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004a", [
      [ "COMMAND_ID_ALL", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa27b8c3ee7cf18d6a17586473e8c1b596", null ],
      [ "COMMAND_ID_SET_TXPI_OVERRIDE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aac5daf28da35b2c6b35d2ea1811e31aaf", null ],
      [ "COMMAND_ID_SET_LOW_POWER_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aafbb107f9367f9b01e2a231329a5c7d05", null ],
      [ "COMMAND_ID_GET_LOW_POWER_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa4a49b928efccf383765556395043a99b", null ],
      [ "COMMAND_ID_SET_POLARITY", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa6b7a0ee5eeaa0cf5fff8aa64cf891b9d", null ],
      [ "COMMAND_ID_GET_POLARITY", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa621b7eb7c715dc5c3a1f21a48d46f0c4", null ],
      [ "COMMAND_ID_SET_LANE_CTRL_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aacf5880d6aa6c1b2ce895de84c2b09793", null ],
      [ "COMMAND_ID_GET_LANE_CTRL_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aae47b33c866f6b159c3f3f8758877f4b9", null ],
      [ "COMMAND_ID_SET_CONFIG_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaee7414113dbe286f1b09c69d45796fcb", null ],
      [ "COMMAND_ID_GET_CONFIG_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa83931ccb2b9ecb50513a864669e50c32", null ],
      [ "COMMAND_ID_SET_LOOPBACK_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaf65cd2f6cfad2865be8bbaf2a9da448a", null ],
      [ "COMMAND_ID_GET_LOOPBACK_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaa4f70796c9d35baf2d85b918464ac33c", null ],
      [ "COMMAND_ID_SET_PRBS_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa28deaac28f44bd887ea83d674f93f69e", null ],
      [ "COMMAND_ID_GET_PRBS_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa96864ba8d81008a7b9993591e330bf01", null ],
      [ "COMMAND_ID_CLEAR_PRBS_STATUS", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aadf9487c258667310f77e587378207a93", null ],
      [ "COMMAND_ID_GET_PRBS_STATUS", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaf59d29ada6fe822f020e0e8a21c81ff4", null ],
      [ "COMMAND_ID_INJ_PRBS_ERROR", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa9d1e8d3899adf3b5ce808a2e2465abb4", null ],
      [ "COMMAND_ID_DIAG_LANE_STATUS", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa68dfddd3f931da1826f3471cf1485297", null ],
      [ "COMMAND_ID_SET_LANE_CONFIG_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa35c4ebf66ec276e90f482f221ce0c9f1", null ],
      [ "COMMAND_ID_GET_LANE_CONFIG_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa0095977444317c9ee011267b95f04318", null ],
      [ "COMMAND_ID_GET_LANE_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaa49440716c5f8783cbac976623005f4d", null ],
      [ "COMMAND_ID_SET_ARCHIVE_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa7adab55acd00df793d0b7c7ccfafc2ab", null ],
      [ "COMMAND_ID_GET_ARCHIVE_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa0f71a8fcc1483c909b306f66fff7df35", null ],
      [ "COMMAND_ID_GET_CMIS_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aab507c5c9a5e9e754937ae4705a117381", null ],
      [ "COMMAND_ID_GET_SHIST_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaabd8b81dd58b6decd556638290d9b84b", null ],
      [ "COMMAND_ID_CLIENT_SIDE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa4da616ef3e88998a0b51fd85fcc459fc", null ],
      [ "COMMAND_ID_SET_SERDES_LANE_CDR_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa56a1cfdcf2d9a623b296504a91445e4f", null ],
      [ "COMMAND_ID_GET_SERDES_LANE_CDR_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa9100ba7a5658d984e4e460ec6b95b358", null ],
      [ "COMMAND_ID_LW_SIDE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aafe59a9132ec7e1b96fa9f1b2dfc1cd1d", null ],
      [ "COMMAND_ID_SET_DSP_POWER_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa67a438b9e63da85a2a6dc651b8a79025", null ],
      [ "COMMAND_ID_GET_DSP_POWER_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aafcccbe14051ecb0c02e77dfa082092af", null ],
      [ "COMMAND_ID_SET_LW_RCLK_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa700a3cf4107b061376da65b432d4ee21", null ],
      [ "COMMAND_ID_SET_LW_DSP_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa6a7a620894ca2820df45fee7c1ad213d", null ],
      [ "COMMAND_ID_GET_LW_DSP_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa9fc49a670c004b489c7d4170ecddc261", null ],
      [ "COMMAND_ID_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa0fcd7b9f9a9dd77ddc80b093ce8f1b81", null ],
      [ "COMMAND_ID_SET_TC_SE_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aafac63877c18845f57fa075e8d3d31668", null ],
      [ "COMMAND_ID_GET_TC_SE_MODE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa8ca76a08c3cbcb0f68a5e74ef76f9f9a", null ],
      [ "COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa3cdee1b763e7fc8e68cf942f5d02fc30", null ],
      [ "COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa44b5ad1647f0fbda604f3230ef540168", null ],
      [ "COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa9eaa930d7db1d770fd1ae4178e4ccee0", null ],
      [ "COMMAND_ID_DIAG_SET_MEDIA_MPI_CONFIG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa21f8001488de91e03565e8a788b5e9d6", null ],
      [ "COMMAND_ID_DIAG_GET_MEDIA_MPI_CONFIG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa50ad8f52182cf1d28d70bc7e6085e54d", null ],
      [ "COMMAND_ID_DIAG_GET_MEDIA_MPI_STATE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aae5ee2dc05fcb79d810b0c251748983ac", null ],
      [ "COMMAND_ID_DIAG_SET_MEDIA_MISSION_MPI_CONFIG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa6db8921f997ff5487cddc57338c3475e", null ],
      [ "COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_CONFIG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaf1414e717812be9e009ead50e92af6fe", null ],
      [ "COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_STATE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaf40420e4933ec0570411b7677d393267", null ],
      [ "COMMAND_ID_DIAG_GET_SERDES_DIAG_INFO", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aadbf5fcafb67469ce7941684b66a8ae96", null ],
      [ "COMMAND_ID_CW_SIDE", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aae4db10c03a405f1e11123b76f2493180", null ],
      [ "COMMAND_ID_SET_CW_RPTR_FEC_MON_CONFIG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa1ce75aab7306f57fc1b9cbf3bae42336", null ],
      [ "COMMAND_ID_SET_RPTR_INDEP_HDL", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaaf0f11e991e6486d1a1650af5d61207b", null ],
      [ "COMMAND_ID_GET_RPTR_INDEP_HDL", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa387d01d0784de70ebe65416a37f8b9ce", null ],
      [ "COMMAND_ID_SET_LINE_PLL_DDCC_CFG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaf595075823aa1da2cb59bd9f344f4e00", null ],
      [ "COMMAND_ID_GET_LINE_PLL_DDCC_CFG", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aa78981f469e7593ec5d87aff5800e0751", null ],
      [ "COMMAND_ID_LAST", "capi__def_8h.html#a42de8173b7c4b1e2294598ea93d0004aaf890ec8ae20ceb92ff56579bab87f37f", null ]
    ] ],
    [ "power_mode_t", "capi__def_8h.html#af578d276ef26dc941024eaad001ae207", [
      [ "POWER_MODE_DEFAULT", "capi__def_8h.html#af578d276ef26dc941024eaad001ae207aa26edef7a2af9605b931952aaa7bf71a", null ],
      [ "POWER_MODE_HIGH_POWER", "capi__def_8h.html#af578d276ef26dc941024eaad001ae207a0844a9def666e30ea6e0b8d00f041161", null ],
      [ "POWER_MODE_MEDIUM_POWER", "capi__def_8h.html#af578d276ef26dc941024eaad001ae207a72de42f9ea15c6f04e0a099938d1488e", null ],
      [ "POWER_MODE_LOW_POWER", "capi__def_8h.html#af578d276ef26dc941024eaad001ae207a6ae941d3551022229475f4ac6f75d9f1", null ]
    ] ],
    [ "capi_host_fec_type_t", "capi__def_8h.html#a449f109abe88293fa8d969ab5464e273", [
      [ "CAPI_HOST_FEC_TYPE_NA", "capi__def_8h.html#a449f109abe88293fa8d969ab5464e273ac0025a269db6e86669b23b543f32fe77", null ],
      [ "CAPI_HOST_FEC_TYPE_RS528", "capi__def_8h.html#a449f109abe88293fa8d969ab5464e273ac0434a7ca98d9f3bcd2ddeb05782ff94", null ],
      [ "CAPI_HOST_FEC_TYPE_RS544", "capi__def_8h.html#a449f109abe88293fa8d969ab5464e273abcbb8ec9ecfa6b81d73e0c573d48b0fd", null ],
      [ "CAPI_HOST_FEC_TYPE_PCS", "capi__def_8h.html#a449f109abe88293fa8d969ab5464e273ac5ad4cc14a3e17664b230fe19759fd65", null ],
      [ "CAPI_HOST_FEC_TYPE_MAX", "capi__def_8h.html#a449f109abe88293fa8d969ab5464e273a55d0458d58364621e9d5eb0e72a644d4", null ]
    ] ],
    [ "capi_line_fec_type_t", "capi__def_8h.html#ad471d9c0d902670cd6d11f6c5ba703b3", [
      [ "CAPI_LINE_FEC_TYPE_NA", "capi__def_8h.html#ad471d9c0d902670cd6d11f6c5ba703b3a13d57e2da1e82af8337901f69465c033", null ],
      [ "CAPI_LINE_FEC_TYPE_RS528", "capi__def_8h.html#ad471d9c0d902670cd6d11f6c5ba703b3a46e7feb19bf02efe52460a591b970f3b", null ],
      [ "CAPI_LINE_FEC_TYPE_RS544", "capi__def_8h.html#ad471d9c0d902670cd6d11f6c5ba703b3a4fdcc08b7514392ed11b1da8e3c2b0a2", null ],
      [ "CAPI_LINE_FEC_TYPE_PCS", "capi__def_8h.html#ad471d9c0d902670cd6d11f6c5ba703b3a18269993073a4defb4c45ec67f7937cd", null ],
      [ "CAPI_LINE_FEC_TYPE_MAX", "capi__def_8h.html#ad471d9c0d902670cd6d11f6c5ba703b3a0a02be1143e44aae077f2a05c9c4ab9e", null ]
    ] ],
    [ "capi_ref_clk_frq_mode_t", "capi__def_8h.html#ae2e0d62610b2034bcbe9e76f84b164d6", [
      [ "CAPI_REF_CLK_FRQ_156_25_MHZ_ETHERNET", "capi__def_8h.html#ae2e0d62610b2034bcbe9e76f84b164d6a5386bf4c649ef7aa647e16635279ad02", null ],
      [ "CAPI_REF_CLK_FRQ_625_MHZ_ETHERNET", "capi__def_8h.html#ae2e0d62610b2034bcbe9e76f84b164d6a73a0a1359699ac0cbb33712f6591a3fc", null ],
      [ "CAPI_REF_CLK_FRQ_166_15625_MHZ_ETHERNET", "capi__def_8h.html#ae2e0d62610b2034bcbe9e76f84b164d6af6b599920909eafa7d9a8b942190a622", null ],
      [ "CAPI_REF_CLK_FRQ_MAX", "capi__def_8h.html#ae2e0d62610b2034bcbe9e76f84b164d6a064f21b3c4ce84fb3c2be3ea9b95a724", null ]
    ] ],
    [ "capi_lane_mux_t", "capi__def_8h.html#a2760dc5b1f26d957c2c0464901fd7cb1", [
      [ "CAPI_LANE_MUX_BIT_MUX", "capi__def_8h.html#a2760dc5b1f26d957c2c0464901fd7cb1a19d929f5b42a2566c55e20a33391e470", null ],
      [ "CAPI_LANE_MUX_SYMBOL_MUX", "capi__def_8h.html#a2760dc5b1f26d957c2c0464901fd7cb1a18f560020c7da31e12eba8e2f29a09b3", null ],
      [ "CAPI_LANE_MUX_MAX", "capi__def_8h.html#a2760dc5b1f26d957c2c0464901fd7cb1a7b2e3cccd7cbaea6cac4887e064dd9ca", null ]
    ] ],
    [ "capi_cfg_dp_type_t", "capi__def_8h.html#a52395c86d202d6b13cd6b63190e21338", [
      [ "CAPI_REPEATER_PATH", "capi__def_8h.html#a52395c86d202d6b13cd6b63190e21338a91d3a81e2ea31ff2c902e5296e405a4a", null ],
      [ "CAPI_RETIMER_PATH", "capi__def_8h.html#a52395c86d202d6b13cd6b63190e21338a804b09440302e7e711cea12d99ebbe50", null ],
      [ "CAPI_PATH_MAX", "capi__def_8h.html#a52395c86d202d6b13cd6b63190e21338afff8b332c438c828ac8c1f64cc8b79a3", null ]
    ] ],
    [ "capi_lane_fec_term_type_t", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eaf", [
      [ "CAPI_LANE_FEC_TERM_BYPASS", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eafac52a264bb3d565aeaf75b5f33c620355", null ],
      [ "CAPI_LANE_FEC_DEC_FWD", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eafa09f0353d26c6fb5cd429f7ca06f4db35", null ],
      [ "CAPI_LANE_FEC_DEC_ENC", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eafa4b14a6335bd759f00f2253def9329f36", null ],
      [ "CAPI_LANE_PCS_XENC", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eafaf67db0a8bac5759c02b23c371f5ca34a", null ],
      [ "CAPI_LANE_FEC_DEC_XDEC_XENC_ENC", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eafa88d0fe7c2ae9cbadb3c3a87123e3967a", null ],
      [ "CAPI_LANE_FEC_TERM_MAX", "capi__def_8h.html#a2e7a3b839d1c09a161c763a906985eafab084c2cc0dc37da05805e8006cdc3edf", null ]
    ] ],
    [ "capi_lw_baud_rate_t", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89", [
      [ "CAPI_LW_BR_53_125", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89ad92544a97c37fc38a73b64c8d26e00e7", null ],
      [ "CAPI_LW_BR_51_5625", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89ab39cbe7693e7fe6a31a68aaf8c7c1361", null ],
      [ "CAPI_LW_BR_25_78125", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89a5fcf70ab9004ac538e403b8f66f409e1", null ],
      [ "CAPI_LW_BR_26_5625", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89a0cdc6310fd39806a271e1b25bd5f6cfc", null ],
      [ "CAPI_LW_BR_10_3125", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89a9dd98dfe1ee3b25d3d49a443db90422d", null ],
      [ "CAPI_LW_BR_20_625", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89abda898b7e74fb3ce340cfdaca6f69462", null ],
      [ "CAPI_LW_BR_1_25", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89af7e951fc14d91b7fec804113a01f2865", null ],
      [ "CAPI_LW_BR_MAX", "capi__def_8h.html#a3e733ec495e1a0fac6aca89e94de9e89af4bd9d93d845f3dabf8be693f7a2a3f1", null ]
    ] ],
    [ "capi_bh_baud_rate_t", "capi__def_8h.html#a5feef88482919575e8171a725ab703fb", [
      [ "CAPI_BH_BR_53_125", "capi__def_8h.html#a5feef88482919575e8171a725ab703fbad726496a685a7f17774743b8fd7872e6", null ],
      [ "CAPI_BH_BR_51_5625", "capi__def_8h.html#a5feef88482919575e8171a725ab703fba204fdc2b4d71fc36ab8f38f1bc3606cd", null ],
      [ "CAPI_BH_BR_25_78125", "capi__def_8h.html#a5feef88482919575e8171a725ab703fbad25976eb0fbf87bd9e81473d2676f8ee", null ],
      [ "CAPI_BH_BR_26_5625", "capi__def_8h.html#a5feef88482919575e8171a725ab703fba0f8c085a35e9f05a80dd73067d3a9f9f", null ],
      [ "CAPI_BH_BR_10_3125", "capi__def_8h.html#a5feef88482919575e8171a725ab703fbab041d80bc90d87de856953e4736d3599", null ],
      [ "CAPI_BH_BR_20_625", "capi__def_8h.html#a5feef88482919575e8171a725ab703fba888ff379ed20dc7587cabd8e131bb90c", null ],
      [ "CAPI_BH_BR_1_25", "capi__def_8h.html#a5feef88482919575e8171a725ab703fba86376451a95ba7a6f636f2e3d2fa8fef", null ],
      [ "CAPI_BH_BR_MAX", "capi__def_8h.html#a5feef88482919575e8171a725ab703fbaf195c67623eb7c3d6268f66208e3a4e0", null ]
    ] ],
    [ "capi_function_mode_t", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75a", [
      [ "CAPI_MODE_NONE", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aa397d4cc888f4bfd9d501de68e04836a0", null ],
      [ "CAPI_MODE_400G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aa027e617e006e912c9f7290ae47589653", null ],
      [ "CAPI_MODE_200G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aab0c3883a6c5dcb9cefe088d761ef8280", null ],
      [ "CAPI_MODE_100G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aad775b3e23a15c09562a0c68deb3c03b2", null ],
      [ "CAPI_MODE_50G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aad51cffa02bb4d708a005213a3aae9835", null ],
      [ "CAPI_MODE_25G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aa500f825a184f7812ed43df17e64be777", null ],
      [ "CAPI_MODE_10G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aa1c0799fe44bf416c02803cf5eda45d89", null ],
      [ "CAPI_MODE_1G", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aae08599620008f6011fefc292d20f24bf", null ],
      [ "CAPI_MODE_MAX", "capi__def_8h.html#aaef3ff1290d513ee52633fbf77e7c75aac8e27ea67d29522bb3c1c1ca580b9759", null ]
    ] ],
    [ "capi_modulation_t", "capi__def_8h.html#a04f84cb755d841a7b0df5b52f0066338", [
      [ "CAPI_MODULATION_NRZ", "capi__def_8h.html#a04f84cb755d841a7b0df5b52f0066338aee179cf2f67c33a4de3726a4130c607c", null ],
      [ "CAPI_MODULATION_PAM4", "capi__def_8h.html#a04f84cb755d841a7b0df5b52f0066338a98a046300c255a37c129f0478144995d", null ]
    ] ],
    [ "capi_port_config_status_t", "capi__def_8h.html#a9c316fba8c182ee71e91f2623bbf580b", [
      [ "CAPI_PORT_CONFIG_STATUS_NOT_CONFIGURED", "capi__def_8h.html#a9c316fba8c182ee71e91f2623bbf580ba60bd6c95a830b2244b6b44bd4163dc4d", null ],
      [ "CAPI_PORT_CONFIG_STATUS_IN_PROGRESS", "capi__def_8h.html#a9c316fba8c182ee71e91f2623bbf580ba35c5c5298cd1018dec01575e9e7828ad", null ],
      [ "CAPI_PORT_CONFIG_STATUS_SUCCESS", "capi__def_8h.html#a9c316fba8c182ee71e91f2623bbf580ba6290206a975c5f33d919641d9863b49a", null ],
      [ "CAPI_PORT_CONFIG_STATUS_FAILED", "capi__def_8h.html#a9c316fba8c182ee71e91f2623bbf580bab1b347282e6ad5ef2b212f2687e78bef", null ]
    ] ],
    [ "capi_port_power_down_status_t", "capi__def_8h.html#aca239a5c6a6adca1eceb6bfda9fc861e", [
      [ "CAPI_PORT_POWER_DOWN_STATUS_POWER_UP", "capi__def_8h.html#aca239a5c6a6adca1eceb6bfda9fc861eabd0ab805d9538309a2e1dd0f8b8112f3", null ],
      [ "CAPI_PORT_POWER_DOWN_STATUS_POWER_DOWN", "capi__def_8h.html#aca239a5c6a6adca1eceb6bfda9fc861ea8273b3f948c7c9a0926c5b469d7ec0aa", null ]
    ] ],
    [ "capi_lane_command_t", "capi__def_8h.html#abb6d463af10942879792a8670eb52ada", [
      [ "CAPI_LANE_CMD_SET_ALL_MODES", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa09b85c40e4d1d2b27af959f5447c276a", null ],
      [ "CAPI_LANE_CMD_SET_PER_PLL_MODES", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa117e211a8c8048eba72d0f4ff2319322", null ],
      [ "CAPI_LANE_CMD_SET_BLACKHAWK_MODE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa8c67f899a1eeb1aadaac90ad39ee89ed", null ],
      [ "CAPI_LANE_CMD_SET_LINE_MODE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa4c85dba693fb9b107d77a0eaa9061dfe", null ],
      [ "CAPI_LANE_CMD_ASSERT_RESET_BLACKHAWK_LANE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa2a834e748ef0837ab2d783f8576e5dc9", null ],
      [ "CAPI_LANE_CMD_RELEASE_RESET_BLACKHAWK_LANE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa21fa5bb7bc9fa6bd48c4260490817eb6", null ],
      [ "CAPI_LANE_CMD_ASSERT_RESET_LINE_LANE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaabd0cbad26e2a73ea79a8b0f63ad51f13", null ],
      [ "CAPI_LANE_CMD_RELEASE_RESET_LINE_LANE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa55375092db3a504c6e1f4976396622ef", null ],
      [ "CAPI_LANE_CMD_READ_STATUS_BLACKHAWK_LANE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaaf24032031224bd4fd689838c5ef59d54", null ],
      [ "CAPI_LANE_CMD_READ_STATUS_LINE_LANE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa32750b920983a6fd522845efec9c5c20", null ],
      [ "CAPI_LANE_CMD_SUSPEND_LINE_LANE_INGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa982ef8499f80d707ee7d9a1137df4e99", null ],
      [ "CAPI_LANE_CMD_RESUME_LINE_LANE_INGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa93c1f3403c8c7fb654473002dcf3badf", null ],
      [ "CAPI_LANE_CMD_SUSPEND_LINE_LANE_EGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa2a0098801776263384e52d92904f0c1b", null ],
      [ "CAPI_LANE_CMD_RESUME_LINE_LANE_EGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaad5326aa6a50749ac832ab8392954c79f", null ],
      [ "CAPI_LANE_CMD_SUSPEND_BH_LANE_INGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaaae58de90d0fac957f22e5b2f2ebc1948", null ],
      [ "CAPI_LANE_CMD_RESUME_BH_LANE_INGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa1383d3174ccd4aa5d045c996ab4c7329", null ],
      [ "CAPI_LANE_CMD_SUSPEND_BH_LANE_EGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa08b88ee2636368de9a792dc9efad2b8f", null ],
      [ "CAPI_LANE_CMD_RESUME_BH_LANE_EGRESS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa0c45435c9d6a6099e67646c0652baae4", null ],
      [ "CAPI_LANE_CMD_FORCE_LW_BH_CDR_LOCKED", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaaa16b3ea54a19b34cbc1ac369cd7d1d4c", null ],
      [ "CAPI_LANE_CMD_FORCE_LW_CDR_LOCKED", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa6ef66444f6cb7304caf2a66b6d8519bd", null ],
      [ "CAPI_LANE_CMD_FORCE_BH_CDR_LOCKED", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa20079d3b4fd9133a2cc19e801e387ccc", null ],
      [ "CAPI_LANE_CMD_UNFORCE_LW_BH_CDR_LOCKED", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa17879442f82765807394b97ff7b32bdb", null ],
      [ "CAPI_LANE_CMD_UNFORCE_LW_CDR_LOCKED", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaaebf4c40797c0cc5b5589e2c8a06e1207", null ],
      [ "CAPI_LANE_CMD_UNFORCE_BH_CDR_LOCKED", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa835d4ade329562617888baa1d6923b47", null ],
      [ "CAPI_LANE_CMD_GET_LW_STICKY_STATUS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa19c2cce1db9891fda4268911e918724f", null ],
      [ "CAPI_LANE_CMD_SET_PORTS_FUNCTION_MOKDE", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa4b02601ce12e5d0e6a07c364532eee77", null ],
      [ "CAPI_LANE_CMD_ADD_NEW_PROTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaacb8569753b20f17bd0e8e8ea5d73a432", null ],
      [ "CAPI_LANE_CMD_DROP_EXITING_PORTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa00cb371b95b4368144accf8e60994250", null ],
      [ "CAPI_LANE_CMD_DROP_ALL_EXITING_PORTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa4605c48284986ce21272bd9c3cefe3d5", null ],
      [ "CAPI_LANE_CMD_ASSERT_DATA_RESET_FOR_PORTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa8206de42e3a8e74fd8beec6c606b83f2", null ],
      [ "CAPI_LANE_CMD_DEASSERT_DATA_RESET_FORT_PORTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa1c350ab1e53ec63aa8bc5ae91248d2f4", null ],
      [ "CAPI_LANE_CMD_ASSERT_POWER_DOWN_FOR_PORTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa8ab367589dc4a51d9cd3b487693d1881", null ],
      [ "CAPI_LANE_CMD_DEASSERT_POWER_DOWN_FORT_PORTS", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa2aec90eb39a300a19fc755c22ca61323", null ],
      [ "CAPI_LANE_CMD_GET_PORT", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaace48e5cb7d4ae5d58962ee2fef151a34", null ],
      [ "CAPI_LANE_CMD_ADD_NEW_PROTS_LPM", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa1726995c707b4f1d1117a053581246ac", null ],
      [ "CAPI_LANE_CMD_ADD_NEW_PORTS_EX", "capi__def_8h.html#abb6d463af10942879792a8670eb52adaa62001f7163215261e0f3bc3784e0db49", null ]
    ] ],
    [ "capi_register_t", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243", [
      [ "CAPI_CHIP_REF_CLOCK_CONFIG_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243abd4a41e234c82c73b3abb5996df6b8b0", null ],
      [ "CAPI_CHIP_TEMP_STATUS_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243a5ba5292afb92f0fa6c16a3c07b89d9cc", null ],
      [ "CAPI_CHIP_VOLTAGE_STATUS_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243a238b5deae5839a8aac6a52446257cf25", null ],
      [ "CAPI_LANE_MODE_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243a8a10452f951edbc3e21d3030e000ed33", null ],
      [ "CAPI_LANE_COMMAND_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243a559893d51cddfd3a2fd20743d7b0c766", null ],
      [ "CAPI_LANE_STATUS_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243a20154820c67bce5ba882101194d58bbf", null ],
      [ "CAPI_CLIENT_LANE_MODE_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243a1b35500769bbc2f6e5ca66886ac073a1", null ],
      [ "CAPI_LINE_WRAPPER_MODE_REGISTER", "capi__def_8h.html#af6ef73cb10ed0a006b88fbcd3ddee243af60e7d1e338f5b636928a9007be20553", null ]
    ] ],
    [ "capi_ref_clk_valid_t", "capi__def_8h.html#a5cd377db565ef0661a78ccbe0348de28", [
      [ "CAPI_REF_CLOCK_NOT_VALID", "capi__def_8h.html#a5cd377db565ef0661a78ccbe0348de28a2227c0e2154742af27891d62205ec947", null ],
      [ "CAPI_REF_CLOCK_VALID", "capi__def_8h.html#a5cd377db565ef0661a78ccbe0348de28af902c1070db7d043820f3c3f222eaff7", null ]
    ] ],
    [ "capi_bh_loopback_alt_mode_t", "capi__def_8h.html#afddff7ed3c5814c0b716e0e1e7df1861", [
      [ "CAPI_BH_LOOPBACK_ALT_MODE_OFF", "capi__def_8h.html#afddff7ed3c5814c0b716e0e1e7df1861a2cc3f496a5bfc32038469deb6a0f7414", null ],
      [ "CAPI_BH_LOOPBACK_ALT_MODE_ON", "capi__def_8h.html#afddff7ed3c5814c0b716e0e1e7df1861aab72526a5d1ffb87c86989e879b3541c", null ]
    ] ],
    [ "capi_command_change_t", "capi__def_8h.html#adcf3aaa3f2919aa8180fc2d39e336f9d", [
      [ "CAPI_COMMAND_NOT_CHANGED", "capi__def_8h.html#adcf3aaa3f2919aa8180fc2d39e336f9da10cf33b426f74c7f0b976fd20da09bc8", null ],
      [ "CAPI_COMMAND_CHANGED", "capi__def_8h.html#adcf3aaa3f2919aa8180fc2d39e336f9da61f30d3c57e0cbfa7caa308d852806a1", null ]
    ] ],
    [ "capi_err_code_t", "capi__def_8h.html#a2138554eee51bd5ab2480d24cec03fbc", [
      [ "CAPI_NO_ERROR", "capi__def_8h.html#a2138554eee51bd5ab2480d24cec03fbca4ee27b76af0dd5c818c1ff5ed711176e", null ]
    ] ],
    [ "capi_command_accepted_t", "capi__def_8h.html#aa6966242f607e401d255778ee6796a2c", [
      [ "CAPI_COMMAND_NOT_ACCEPTED", "capi__def_8h.html#aa6966242f607e401d255778ee6796a2ca64528c5f2f2ca31dbe36b252678ec84f", null ],
      [ "CAPI_COMMAND_ACCEPTED", "capi__def_8h.html#aa6966242f607e401d255778ee6796a2ca60406bd01dba255590336a6ca130bff7", null ]
    ] ],
    [ "capi_bh_db_lost_t", "capi__def_8h.html#a42ddf004add1e8891a13777eede457ff", [
      [ "CAPI_BH_DB_LOST_0_9_VSR", "capi__def_8h.html#a42ddf004add1e8891a13777eede457ffa9659e96e411bb381399bfa44eb3ccfc1", null ],
      [ "CAPI_BH_DB_LOST_10_19_MR", "capi__def_8h.html#a42ddf004add1e8891a13777eede457ffa09920dd74e03f85c5b5b491007323314", null ],
      [ "CAPI_BH_DB_LOST_20_29_LR", "capi__def_8h.html#a42ddf004add1e8891a13777eede457ffad59ef43f592facabb487e7b639d70270", null ]
    ] ],
    [ "capi_bh_medium_t", "capi__def_8h.html#a7e97053edf734a4741ba4ba0d81c4429", [
      [ "CAPI_BH_MEDIUM_PCB", "capi__def_8h.html#a7e97053edf734a4741ba4ba0d81c4429a5fad02f280115ddc220417f4b18dfacb", null ],
      [ "CAPI_BH_MEDIUM_COPPER_WIRE", "capi__def_8h.html#a7e97053edf734a4741ba4ba0d81c4429adcd66f084f99857c04052b730041a982", null ],
      [ "CAPI_BH_MEDIUM_OPTICAL", "capi__def_8h.html#a7e97053edf734a4741ba4ba0d81c4429a84f2445c83e1b19a2ddf9571bf61eeb2", null ]
    ] ],
    [ "capi_bh_tx_t", "capi__def_8h.html#a5a89fb087c086c1cb211655c0e53ae29", [
      [ "CAPI_BH_tx_ENABLE", "capi__def_8h.html#a5a89fb087c086c1cb211655c0e53ae29aca8ba14a5462eabdde8f9fbb19789d3a", null ],
      [ "CAPI_BH_tx_DISABLE", "capi__def_8h.html#a5a89fb087c086c1cb211655c0e53ae29aa9d446d218951c1a9cf20582dfbfc007", null ]
    ] ],
    [ "capi_bh_rx_t", "capi__def_8h.html#a64bdfc697609f69b7c491ff247202f01", [
      [ "CAPI_BH_rx_ENABLE", "capi__def_8h.html#a64bdfc697609f69b7c491ff247202f01a0baa5e7e668efdc0909908965716e157", null ],
      [ "CAPI_BH_rx_DISABLE", "capi__def_8h.html#a64bdfc697609f69b7c491ff247202f01a697a1f9c95c01b1aabaa33c0e0bfa9d9", null ]
    ] ],
    [ "capi_lw_optical_mode_t", "capi__def_8h.html#afa2fde72fa6deec966fea404f7e96335", [
      [ "CAPI_LW_OPTICAL_MODE", "capi__def_8h.html#afa2fde72fa6deec966fea404f7e96335a2bf21f89226cafe21eb2290f18e3d197", null ],
      [ "CAPI_LW_COPPER_MODE", "capi__def_8h.html#afa2fde72fa6deec966fea404f7e96335ac71fd5b4611c6da391e47ddb70688f14", null ]
    ] ],
    [ "capi_lw_phd_mode_t", "capi__def_8h.html#a7de40108162a96f50fd06b50a12d6afc", [
      [ "CAPI_LW_PHD_MODE_PRE_CURSOR", "capi__def_8h.html#a7de40108162a96f50fd06b50a12d6afca7ecfe34dce3f648e1536ce48a684d9e6", null ],
      [ "CAPI_LW_PHD_MODE_MMSE", "capi__def_8h.html#a7de40108162a96f50fd06b50a12d6afcaf7ac4b7e02b4b4679e9fc3daf4133094", null ],
      [ "CAPI_LW_PHD_MODE_SYMMETRICAL", "capi__def_8h.html#a7de40108162a96f50fd06b50a12d6afcad404e319eb61aaf50fc2a43a9d2dd701", null ],
      [ "CAPI_LW_PHD_MODE_SYMETRICAL_W_PRECODER", "capi__def_8h.html#a7de40108162a96f50fd06b50a12d6afcabb926f1c68c54fb22d0a1027ba3db271", null ]
    ] ],
    [ "capi_lw_eqpp_t", "capi__def_8h.html#a678a61cd124bd06ff8f733f8deee50cb", [
      [ "CAPI_LW_EQPP_ENABLE", "capi__def_8h.html#a678a61cd124bd06ff8f733f8deee50cba502ae5101dd2868063bd933e84a54ff9", null ],
      [ "CAPI_LW_EQPP_DISABLE", "capi__def_8h.html#a678a61cd124bd06ff8f733f8deee50cba444a9d39162cfa99116179cb85dba22c", null ]
    ] ],
    [ "capi_lw_nldet_t", "capi__def_8h.html#a97ef9b8e58044092cca594333347a4b7", [
      [ "CAPI_LW_NLDET_ENABLE", "capi__def_8h.html#a97ef9b8e58044092cca594333347a4b7aa8f7b0a5d18794ca432a329a0c4accfb", null ],
      [ "CAPI_LW_NLDET_DISABLE", "capi__def_8h.html#a97ef9b8e58044092cca594333347a4b7a2216449dad9c74381a05360899e7f866", null ]
    ] ],
    [ "capi_lw_dfe_t", "capi__def_8h.html#a4dfbb595a1df795c23d7710d4b9e5ce7", [
      [ "CAPI_LW_DFE_ENABLE", "capi__def_8h.html#a4dfbb595a1df795c23d7710d4b9e5ce7aaa61d767952c2fb7af3f2b17ce046753", null ],
      [ "CAPI_LW_DFE_DISABLE", "capi__def_8h.html#a4dfbb595a1df795c23d7710d4b9e5ce7a2ff2bc655ab5e6e5bc6001bcf24b2047", null ]
    ] ],
    [ "capi_bh_slicer_mode_t", "capi__def_8h.html#a12e8d29adf5bdedd51f6b4a9afddfbda", [
      [ "CAPI_BH_SLICER_MODE_NS", "capi__def_8h.html#a12e8d29adf5bdedd51f6b4a9afddfbdaa90d24f618a7321651877d966836934f5", null ],
      [ "CAPI_BH_SLICER_MODE_ES", "capi__def_8h.html#a12e8d29adf5bdedd51f6b4a9afddfbdaa9d5a2b07dfd10054cdaec80233c6132f", null ]
    ] ],
    [ "capi_bh_precoder_t", "capi__def_8h.html#a13d0e43227dd53565581216d77d5ddd1", [
      [ "CAPI_BH_PRECODER_ENABLE", "capi__def_8h.html#a13d0e43227dd53565581216d77d5ddd1a105f810cba5c50b1917cff339a31304f", null ],
      [ "CAPI_BH_PRECODER_DISABLE", "capi__def_8h.html#a13d0e43227dd53565581216d77d5ddd1a2b1e16317ab4647de62893de95e394d0", null ]
    ] ],
    [ "capi_lw_extended_mode_t", "capi__def_8h.html#a29cad4b3a347d18ad416ec2ebbb8bc8d", [
      [ "CAPI_LW_EXTENDED_MODE_NO_OVERRIDE", "capi__def_8h.html#a29cad4b3a347d18ad416ec2ebbb8bc8da4586a105424756c3ede6d556604146e1", null ],
      [ "CAPI_LW_EXTENDED_MODE_SLICER", "capi__def_8h.html#a29cad4b3a347d18ad416ec2ebbb8bc8da73c8a943c317617843261489e81ddafa", null ],
      [ "CAPI_LW_EXTENDED_MODE_REGULAR_SLICER", "capi__def_8h.html#a29cad4b3a347d18ad416ec2ebbb8bc8da82359a9314250f3dc97f13de9d94b745", null ]
    ] ],
    [ "capi_lw_g_loop_t", "capi__def_8h.html#a2a518363ac6cbc4ba0a65a2646ca3a4b", [
      [ "CAPI_LW_G_LOOP_ENABLE", "capi__def_8h.html#a2a518363ac6cbc4ba0a65a2646ca3a4ba91a1da52c4f717c3b81acecc29e3e6a2", null ],
      [ "CAPI_LW_G_LOO_DISABLE", "capi__def_8h.html#a2a518363ac6cbc4ba0a65a2646ca3a4bab1fa805eae3279e422f6bb2adf57f863", null ]
    ] ],
    [ "capi_lw_r_loop_t", "capi__def_8h.html#a17eb03f2145a2283653810aef0dadcdd", [
      [ "CAPI_LW_R_LOOP_ENABLE", "capi__def_8h.html#a17eb03f2145a2283653810aef0dadcdda4771f29d8af23bd6d79b0c91e0327f46", null ],
      [ "CAPI_LW_R_LOOP_DISABLE", "capi__def_8h.html#a17eb03f2145a2283653810aef0dadcddab0972f7b2bc2a609d7058be31b08e75c", null ]
    ] ],
    [ "capi_lw_link_training_t", "capi__def_8h.html#a3892efe4beca31e4ddc95f475f3b2a90", [
      [ "CAPI_LW_LINK_TRAINING_NONE", "capi__def_8h.html#a3892efe4beca31e4ddc95f475f3b2a90aa3402779d9d10ec723d4fc537f29a0b1", null ],
      [ "CAPI_LW_LINK_TRAINING_CL73", "capi__def_8h.html#a3892efe4beca31e4ddc95f475f3b2a90a292521a7a6820050d26a9ddafb1e6d24", null ],
      [ "CAPI_LW_LINK_TRAINING_802_3CD_NRZ", "capi__def_8h.html#a3892efe4beca31e4ddc95f475f3b2a90add19a470130190789d0ce3dcff5c5fa0", null ],
      [ "CAPI_LW_LINK_TRAINING_802_3CD_PAM4", "capi__def_8h.html#a3892efe4beca31e4ddc95f475f3b2a90a645ccf4ff29eb110a545054d5389d938", null ],
      [ "CAPI_LW_LINK_TRAINING_802_3CD_PAM4_PRECODER", "capi__def_8h.html#a3892efe4beca31e4ddc95f475f3b2a90a927ed794110056ec6b891963789a4ad4", null ]
    ] ],
    [ "capi_lw_cl73_t", "capi__def_8h.html#ac256c6c4ef96967eaa516bafba85efe0", [
      [ "CAPI_BH_CL73_ENABLE", "capi__def_8h.html#ac256c6c4ef96967eaa516bafba85efe0a3d06ad22972991dccd437655b4943f20", null ],
      [ "CAPI_BH_CL73_DISABLE", "capi__def_8h.html#ac256c6c4ef96967eaa516bafba85efe0a0297587135162595fbdb685880ad8601", null ]
    ] ],
    [ "capi_data_rate_t", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6f", [
      [ "CAPI_1G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa6ae38ed4ae72722395815164720a3696", null ],
      [ "CAPI_2G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa3ce00d762d7995e6489310b6f49ffd4b", null ],
      [ "CAPI_10G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa046cfad7e09afd07ed444e1034c8a07e", null ],
      [ "CAPI_25G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa47ca953fa893939e45b2ee474ee16c0d", null ],
      [ "CAPI_50G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa22394788e1e3072e7832439da1f82aef", null ],
      [ "CAPI_200G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa5c6378ce881d8a4d86b4e70b4fb47910", null ],
      [ "CAPI_400G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa43974ebeea6153a0860565be30a10776", null ],
      [ "CAPI_600G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6faae6e70beba57982b29199b5ead387f78", null ],
      [ "CAPI_800G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa40ccd14fe2faa7065fa742999d7f117c", null ],
      [ "CAPI_1000G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa8f8a21c988f205df178f0727da097e60", null ],
      [ "CAPI_1200G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fae2531c07871f0c81b226302586030e02", null ],
      [ "CAPI_1600G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa91022a72cbae8c805f7dfe6a4094b268", null ],
      [ "CAPI_2000G_DATA_RATE", "capi__def_8h.html#a504218c1dd18c304b174ec0e60002c6fa513b7ad0dfea3c88fcf0f819ce487c5c", null ]
    ] ],
    [ "capi_firmware_image_type_t", "capi__def_8h.html#ac9d2347bcbb50172feae94119a31ba9b", [
      [ "FIRMWARE_IMAGE_DEFAULT", "capi__def_8h.html#ac9d2347bcbb50172feae94119a31ba9ba09313673139edd63c420a72c0dd8ac9c", null ],
      [ "FIRMWARE_IMAGE_J", "capi__def_8h.html#ac9d2347bcbb50172feae94119a31ba9bac34680b34592a0165fe9bcbefa5b3e3b", null ]
    ] ],
    [ "capi_download_mode_t", "capi__def_8h.html#a11e649db184c6abcc4e56f19784ae190", [
      [ "CAPI_DOWNLOAD_MODE_NONE", "capi__def_8h.html#a11e649db184c6abcc4e56f19784ae190a6576d57996651320c7a173b07bd3947c", null ],
      [ "CAPI_DOWNLOAD_MODE_MDIO_SRAM", "capi__def_8h.html#a11e649db184c6abcc4e56f19784ae190a12a1ce4ab490ca43647a5d721d7e091a", null ],
      [ "CAPI_DOWNLOAD_MODE_I2C_SRAM", "capi__def_8h.html#a11e649db184c6abcc4e56f19784ae190a83a13dadbbc4e4abe50c111ad09fb290", null ],
      [ "CAPI_DOWNLOAD_MODE_MDIO_EEPROM", "capi__def_8h.html#a11e649db184c6abcc4e56f19784ae190adc2f3f2049df3d84d6aee100a79df351", null ],
      [ "CAPI_DOWNLOAD_MODE_I2C_EEPROM", "capi__def_8h.html#a11e649db184c6abcc4e56f19784ae190ae8b2881f68afbd6c9af3ac81922f5da8", null ]
    ] ],
    [ "capi_spi_program_mode_t", "capi__def_8h.html#ac2b913c96fba69a0d0b4edc12b32d483", [
      [ "CAPI_SPI_PROGRAM_STANDARD", "capi__def_8h.html#ac2b913c96fba69a0d0b4edc12b32d483a7ff96e6352724305e8c4692c3f267dd3", null ],
      [ "CAPI_SPI_PROGRAM_FAST", "capi__def_8h.html#ac2b913c96fba69a0d0b4edc12b32d483af8d5cb8a5a3f25bd1319b901134b7f25", null ]
    ] ],
    [ "capi_sram_program_mode_t", "capi__def_8h.html#a2e8644773a206c516584cb0a57426b46", [
      [ "CAPI_SRAM_PROGRAM_STANDARD", "capi__def_8h.html#a2e8644773a206c516584cb0a57426b46a6586bab80c97b21363e775fc002b2259", null ],
      [ "CAPI_SRAM_PROGRAM_FAST", "capi__def_8h.html#a2e8644773a206c516584cb0a57426b46a191c114947c1202400170d83dfcfef0a", null ]
    ] ],
    [ "capi_default_mode_t", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199", [
      [ "CAPI_NO_DEFAULT_MODE", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199ad6ad6590025d819c2b1cfcb7e274df54", null ],
      [ "CAPI_DEFAULT_MODE_1_REPEATER", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199af200143f079c1de843707b2e8f10f399", null ],
      [ "CAPI_DEFAULT_MODE_1_KP4_KP4", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199ac2c69298196c730f7ce8c42ee209bf49", null ],
      [ "CAPI_DEFAULT_MODE_5_PCS_KP4", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199a0a2080064ef1d81713dcc097ef943a98", null ],
      [ "CAPI_DEFAULT_MODE_5_KR4_KP4", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199a255296d426ab356be50f724da56895b0", null ],
      [ "CAPI_DEFAULT_MODE_4_NRZ_NRZ_REPEATER", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199a9b10603bf8f2d8c0afc46524a9dba49c", null ],
      [ "CAPI_DEFAULT_MODE_7_REPEATER", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199ad835a984cc36df65bf17d95a65603eb6", null ],
      [ "CAPI_DEFAULT_MODE_7_KP4_KP4", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199afc87700b09e4e40f5c73b0e0ca33ebc2", null ],
      [ "CAPI_DEFAULT_MODE_MAX", "capi__def_8h.html#ae21f5a5c6aeebc9141fafc8076927199a9a1dac573125cb289e868aa788867e40", null ]
    ] ],
    [ "capi_burst_write_mode_t", "capi__def_8h.html#a4b3d404a36a413f44747d9a7ff83c5d3", [
      [ "CAPI_BURST_WRITE_MODE_I2C", "capi__def_8h.html#a4b3d404a36a413f44747d9a7ff83c5d3a5a325c0738ea7f33ed252cd112f5134c", null ],
      [ "CAPI_BURST_WRITE_MODE_MDIO", "capi__def_8h.html#a4b3d404a36a413f44747d9a7ff83c5d3a19e6effce3bbfac2220e828e3d9f77fb", null ],
      [ "CAPI_BURST_WRITE_MODE_I2C_OPT", "capi__def_8h.html#a4b3d404a36a413f44747d9a7ff83c5d3ab307c161ea3ea2247864971bceb69eb6", null ]
    ] ],
    [ "capi_bus_intf_t", "capi__def_8h.html#a743ee7746322b21e2355664dfffd607d", [
      [ "CAPI_ACCESS_MODE_MDIO", "capi__def_8h.html#a743ee7746322b21e2355664dfffd607daef9c7670d258d4c3ae296fac5d202f8b", null ],
      [ "CAPI_ACCESS_MODE_I2C", "capi__def_8h.html#a743ee7746322b21e2355664dfffd607da265ba90aad7d3621244caaca8bb61077", null ]
    ] ],
    [ "broadcast_enable_t", "capi__def_8h.html#a3cb5f1d5af0bbcaa47070425c75b827a", [
      [ "BROADCAST_DISABLE", "capi__def_8h.html#a3cb5f1d5af0bbcaa47070425c75b827aa92c2cad8e5eae8ef4bb3e9ab9f144aab", null ],
      [ "BROADCAST_ENABLE", "capi__def_8h.html#a3cb5f1d5af0bbcaa47070425c75b827aa3f1e9ebe104a1e47fc6d488a387b387d", null ]
    ] ],
    [ "capi_reset_mode_t", "capi__def_8h.html#ab8afed72deff4869efff8eb2760ce793", [
      [ "CAPI_HARD_RESET_MODE", "capi__def_8h.html#ab8afed72deff4869efff8eb2760ce793a53fd6a434f6cd95d39034ab7b47765e9", null ],
      [ "CAPI_SOFT_RESET_MODE", "capi__def_8h.html#ab8afed72deff4869efff8eb2760ce793a2d1b73c48516a4a28d105acc79c4ca5c", null ],
      [ "CAPI_RESET_BRING_UP_MODE", "capi__def_8h.html#ab8afed72deff4869efff8eb2760ce793a5cc3bf2a079a6cea600fb04e1f830f45", null ]
    ] ],
    [ "capi_interface_t", "capi__def_8h.html#a6cfbbb85f6382315a5488511f6cbbcf6", [
      [ "CAPI_CR_INTERFACE_TYPE", "capi__def_8h.html#a6cfbbb85f6382315a5488511f6cbbcf6aff64e15b0b29e61ab8ce90d29c9c998c", null ],
      [ "CAPI_KR_INTERFACE_TYPE", "capi__def_8h.html#a6cfbbb85f6382315a5488511f6cbbcf6a320af32edbacaa6d5504ccca8cada4b5", null ],
      [ "CAPI_XX_INTERFACE_TYPE", "capi__def_8h.html#a6cfbbb85f6382315a5488511f6cbbcf6ab6cf4d8379b53f951ae5bd9c3ce9bc95", null ]
    ] ],
    [ "capi_interface_mode_t", "capi__def_8h.html#ac081b56efd5720418f313d9f76786ecb", [
      [ "CAPI_IEEE_INTERFACE_MODE", "capi__def_8h.html#ac081b56efd5720418f313d9f76786ecbada0ad805e222cfc83244b962b80b1b61", null ],
      [ "CAPI_OTN_INTERFACE_MODE", "capi__def_8h.html#ac081b56efd5720418f313d9f76786ecbac986961c174a759a68dc6ea134090514", null ],
      [ "CAPI_HIGIG_INTERFACE_MODE", "capi__def_8h.html#ac081b56efd5720418f313d9f76786ecba190adcb00fb83b8c5e9ed97d6ccf6dac", null ]
    ] ],
    [ "capi_core_version_t", "capi__def_8h.html#a939e8e96b05292d9b5e9f3ddb8df1731", [
      [ "CORE_VERSION_A0", "capi__def_8h.html#a939e8e96b05292d9b5e9f3ddb8df1731a284f4d20aca647142bfd840f4766c63d", null ],
      [ "CORE_VERSION_NONE", "capi__def_8h.html#a939e8e96b05292d9b5e9f3ddb8df1731ad254ccbd8bf4d493906eb48c140ab067", null ]
    ] ],
    [ "client_rx_tx_t", "capi__def_8h.html#ae163a220f81e5363d6fbc9a56d1fe4e7", [
      [ "CLIENT_RX", "capi__def_8h.html#ae163a220f81e5363d6fbc9a56d1fe4e7a797afef162bcaef8d90d6db3bda43576", null ],
      [ "CLIENT_TX", "capi__def_8h.html#ae163a220f81e5363d6fbc9a56d1fe4e7ad4ced826411b4e5b9707f25c7f78dbc5", null ],
      [ "CLIENT_MAX", "capi__def_8h.html#ae163a220f81e5363d6fbc9a56d1fe4e7ad86a77f993a9122de63428aeb2a8f96b", null ]
    ] ],
    [ "lw_rx_tx_t", "capi__def_8h.html#a427da2225c2a805430455ca97870d17a", [
      [ "LW_RX", "capi__def_8h.html#a427da2225c2a805430455ca97870d17aa7f4e0ce2fea46849df7a7d02f0a80793", null ],
      [ "LW_TX", "capi__def_8h.html#a427da2225c2a805430455ca97870d17aa21d48dac1d34b99766f33272ee1d9c3a", null ],
      [ "LW_MAX", "capi__def_8h.html#a427da2225c2a805430455ca97870d17aa9a066845b682357f2b9b08921a1851f7", null ]
    ] ],
    [ "mode_change_command_t", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bb", [
      [ "MODE_CHG_CMD_SET_ALL_MODES", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba7fcb8bcdfa0eba1ac0eec17354eb8bea", null ],
      [ "MODE_CHG_CMD_SET_PER_PLL_MODES", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbab06ca6fb9208daf9ca017af8c8674909", null ],
      [ "MODE_CHG_CMD_SET_LANE_MODE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbae0bbf9a3429c558bef2889cd35baa297", null ],
      [ "MODE_CHG_CMD_SET_BLACKHAWK_MODE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaecfe3652ea231ec6c0746177c63a6412", null ],
      [ "MODE_CHG_CMD_SET_LINE_MODE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba75c8b6dfd43634bf07c023aedccdcf1a", null ],
      [ "MODE_CHG_CMD_ASSERT_GLOBAL_LANE_RESET", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaa6b085afec5427298d7457b0be2d99fe", null ],
      [ "MODE_CHG_CMD_RELEASE_GLOBAL_LANE_RESET", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbadd9d34fbe11fedd5375ed3b02b4bbaab", null ],
      [ "MODE_CHG_CMD_ASSERT_RESET_BLACKHAWK_LANE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbab7627fe0fbf436da5ae81533155520d6", null ],
      [ "MODE_CHG_CMD_RELEASE_RESET_BLACKHAWK_LANE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba0cfd1cc8095c970aa2e00397ff0a534b", null ],
      [ "MODE_CHG_CMD_ASSERT_RESET_LINE_LANE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba70075dd0cced80f558581174a30c6f73", null ],
      [ "MODE_CHG_CMD_RELEASE_RESET_LINE_LANE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba2b6582e1c4f7bb09f70d5a425c8bef53", null ],
      [ "MODE_CHG_CMD_READ_STATUS_BLACKHAWK_LANE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba4c64ad2c4163c9ee3a5502d93a397166", null ],
      [ "MODE_CHG_CMD_READ_STATUS_LINE_LANE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbafb4ba8e22ae730404230fab8b5388c43", null ],
      [ "MODE_CHG_CMD_SUSPEND_LINE_LANE_INGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba1d8b4c2b0d7b2189d49728f103bc2ae5", null ],
      [ "MODE_CHG_CMD_RESUME_LINE_LANE_INGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbad2682d0512bd02a304883f817fa564f6", null ],
      [ "MODE_CHG_CMD_SUSPEND_LINE_LANE_EGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba0fb8a2e3259c8c82da6c9d5cc9a0b122", null ],
      [ "MODE_CHG_CMD_RESUME_LINE_LANE_EGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaf2df26c07e13bf925b2cb3ca8e708e76", null ],
      [ "MODE_CHG_CMD_SUSPEND_DUAL_WRAPPER_INGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba3bbbbe3739adeda8bce5193f9cb042b6", null ],
      [ "MODE_CHG_CMD_RESUME_DUAL_WRAPPER_INGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba881cf7f2e6d02f580e40664d772db54c", null ],
      [ "MODE_CHG_CMD_SUSPEND_DUAL_WRAPPER_EGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba3ef94a2146c4ed28534596e3f22cecba", null ],
      [ "MODE_CHG_CMD_RESUME_DUAL_WRAPPER_EGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaba7ed38e95f06dd5af689e7c6778bd87", null ],
      [ "MODE_CHG_CMD_SUSPEND_BH_LANE_INGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaaaa1a3b7627d126c7c321988cd1f4eeb", null ],
      [ "MODE_CHG_CMD_RESUME_BH_LANE_INGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba9fc3cf56a5e37d34e99ac65202cac9b6", null ],
      [ "MODE_CHG_CMD_SUSPEND_BH_LANE_EGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba1003203aa4504020decd6e859dc22f77", null ],
      [ "MODE_CHG_CMD_RESUME_BH_LANE_EGRESS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbab296c5b67845efeac0bd3d8ce66d7da6", null ],
      [ "MODE_CHG_CMD_ENABLE_LW_BH_IGNORE_FAULT", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbac048648007659565dcc4289751e9f71c", null ],
      [ "MODE_CHG_CMD_ENABLE_BH_IGNORE_FAULT", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba64461dc2f5c3d67b281917b33b0b0925", null ],
      [ "MODE_CHG_CMD_ENABLE_LW_IGNORE_FAULT", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbafe131965897f83c52167830982d4427b", null ],
      [ "MODE_CHG_CMD_DISABLE_LW_BH_IGNORE_FAULT", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba6c6af08265c466ab9e063abdc4f5380e", null ],
      [ "MODE_CHG_CMD_DISABLE_BH_IGNORE_FAULT", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba88cd730b352f5fd815ba1d01079bc65b", null ],
      [ "MODE_CHG_CMD_DISABLE_LW_IGNORE_FAULT", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba71ad18d700f947b55e36c15d47dfbc5b", null ],
      [ "MODE_CHG_CMD_SET_JFEC_INGRESS_MODE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbae061a93c7c80392a2f173bc5e33fc179", null ],
      [ "MODE_CHG_CMD_SET_JFEC_EGRESS_MODE", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbab139d4469f2a50a18ff0768bb0697247", null ],
      [ "MODE_CHG_CMD_READ_JFEC_INGRESS_PRBS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba670c5756763362adb8d5a228e388a67e", null ],
      [ "MODE_CHG_CMD_READ_JFEC_EGRESS_PRBS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba4fbe32ebdc74b313569f7a8c7923eb98", null ],
      [ "MODE_CHG_CMD_SET_KP4_CONFIG", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbae19456f5768f6f6b635c3e43837d3449", null ],
      [ "MODE_CHG_CMD_SET_JFEC_INGRESS_PRBS_GEN", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbac59143f107cd7a91dcae827583efd8a4", null ],
      [ "MODE_CHG_CMD_SET_JFEC_EGRESS_PRBS_GEN", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba03c5204defbacdf2970f1af0c3544dbd", null ],
      [ "MODE_CHG_CMD_SET_JFEC_INGRESS_PRBS_MON", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba9d05a155696b82f8beff9820849eaa16", null ],
      [ "MODE_CHG_CMD_SET_JFEC_EGRESS_PRBS_MON", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba9b96a75dca1238e6da3145363e03356d", null ],
      [ "MODE_CHG_CMD_GET_LW_STICKY_STATUS", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba20f118afe21b9fe13811bc1602375694", null ],
      [ "MODE_CHG_CMD_SUSPEND_FW", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba0277f566c78d13955fae8ba57a748e68", null ],
      [ "MODE_CHG_CMD_RESUME_FW", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbab1083fe094c0224354f0ca5279c3d868", null ],
      [ "MODE_CHG_CMD_SET_SFEC_CONFIG", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaad65b08c10a0ef8e858e47a7c92f301b", null ],
      [ "MODE_CHG_CMD_READ_SFEC_INGRESS_FRAME", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaf0bf183ee72e5ef65993123b644d73f4", null ],
      [ "MODE_CHG_CMD_READ_SFEC_INGRESS_UNCORR_FRAME", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbad7e48bd4484a1d270b29891e261fa403", null ],
      [ "MODE_CHG_CMD_READ_SFEC_INGRESS_CORR0S", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba031ebca5e16f82f40c5a0d07838f98ce", null ],
      [ "MODE_CHG_CMD_READ_SFEC_INGRESS_CORR1S", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bba9b8450d7f4e95515b1635a93aa8b1b85", null ],
      [ "MODE_CHG_CMD_LAST_CMD", "capi__def_8h.html#a65dbf18fc7ebe4c5d5a8f3ad425223bbaa7c675edad0e0446133acfc5c4ddb6a1", null ]
    ] ],
    [ "capi_avs_ctrl_t", "capi__def_8h.html#a2a8579f0dab220eacdf5ac95db365c32", [
      [ "CAPI_INTERNAL_AVS_CONTROL", "capi__def_8h.html#a2a8579f0dab220eacdf5ac95db365c32a022ffe85d4c3f9660f326cbc26e7224e", null ],
      [ "CAPI_EXTERNAL_AVS_CONTROL", "capi__def_8h.html#a2a8579f0dab220eacdf5ac95db365c32aacf74f49a88b09993012f7bb8df54a1b", null ]
    ] ],
    [ "avs_result_t", "capi__def_8h.html#a09542b3e2d175ea0becb5d54d800ff70", [
      [ "CAPI_INIT_NOT_STARTED", "capi__def_8h.html#a09542b3e2d175ea0becb5d54d800ff70a0995fb216aceb34b10b3621504c7b86b", null ],
      [ "CAPI_INIT_DONE_SUCCESS", "capi__def_8h.html#a09542b3e2d175ea0becb5d54d800ff70a9e323e87f53c1774c0f7d46c550f0b44", null ],
      [ "CAPI_INIT_ERROR", "capi__def_8h.html#a09542b3e2d175ea0becb5d54d800ff70a0e2aa24a7951a71ac11fdade125d8ec6", null ]
    ] ],
    [ "capi_set_voltage_with_fw_t", "capi__def_8h.html#addaa30810eaeb7526e042827a91ddfdf", [
      [ "CAPI_SET_VOLTAGE_WITH_FIRMWARE", "capi__def_8h.html#addaa30810eaeb7526e042827a91ddfdfa84485b34c065d3ad79715f76c12b8c7e", null ],
      [ "CAPI_SET_VOLTAGE_NO_FIRMWARE", "capi__def_8h.html#addaa30810eaeb7526e042827a91ddfdfa381ce59ba2b2cb33e380e7936c2476e0", null ]
    ] ],
    [ "capi_avs_pack_share_t", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526", [
      [ "CAPI_1_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526a9b280a72160648946e44588d8643bddd", null ],
      [ "CAPI_2_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526a8fc580b8dc4b660c09784833d817f766", null ],
      [ "CAPI_3_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526a175d4f3f2a5d73a17ef56a56afb8f507", null ],
      [ "CAPI_4_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526ae178fe4c0fd2c3245627e251c952cbbf", null ],
      [ "CAPI_5_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526a7013b29bfc89f5b22b0a3903667520d8", null ],
      [ "CAPI_6_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526a665dd0beba0c627623eae2b7f0baff8f", null ],
      [ "CAPI_7_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526a46e5c21042416eb478701b21e78f7b72", null ],
      [ "CAPI_8_PACKAGE_SHARE_AVS", "capi__def_8h.html#ad7670ee643037e22bb4c7a998d76f526ad601024efd1ddd875a829169f9d618d2", null ]
    ] ],
    [ "capi_board_dc_margin_t", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589", [
      [ "CAPI_0_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589af6b04bfa56352a6b0c624a4dd62343c9", null ],
      [ "CAPI_1_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a8a347d6c0caff0160d46bf15488ea490", null ],
      [ "CAPI_2_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a6759a02087e07cb52418bcdd28e08f5d", null ],
      [ "CAPI_3_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a00c8ede28c222a095fcf063abceb4cf9", null ],
      [ "CAPI_4_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589aab6ce7a2672e14557d80a575d696e634", null ],
      [ "CAPI_5_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a8f20e25cf2faeff0dc79c5c53589316d", null ],
      [ "CAPI_6_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a17bbcfc6de9558fb9ee8291e058fabd1", null ],
      [ "CAPI_7_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a3eb65612026946bcf52f01587ae89aea", null ],
      [ "CAPI_8_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a329b4b6354f2f4e6195c18fee93ca12c", null ],
      [ "CAPI_9_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a469b93db022b76b5ff30317058635eb7", null ],
      [ "CAPI_10_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589ab86aa2d35da8c0549573c91a1580c6c5", null ],
      [ "CAPI_11_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a6fd4b1b9bcc0749e124dfa480b093e02", null ],
      [ "CAPI_12_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589afa9fac96d03bfbcaacd6ea25ac9ccdbd", null ],
      [ "CAPI_13_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a12b40cd0e535d70b415a2179afa85f2f", null ],
      [ "CAPI_14_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a148a0f57823158249e60197a61583fb9", null ],
      [ "CAPI_15_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a388567247c02fba844a428d8324fd6e1", null ],
      [ "CAPI_16_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a80f32c396426d53f5b1e557d5ab2e186", null ],
      [ "CAPI_17_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a1cd231f6d5be193f362d34bdb989f8b7", null ],
      [ "CAPI_18_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589adc16d62fb44c3cf91983aa3b3e68c49a", null ],
      [ "CAPI_19_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589ac5b806aab888c93878ccfc8c82fabc8f", null ],
      [ "CAPI_20_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a65c0781e0bb64e0100a9412ca837b44c", null ],
      [ "CAPI_21_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a89adbe619ed4f97beac4daac720c9bd3", null ],
      [ "CAPI_22_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a63c557aed9199498d963ad9f6b502230", null ],
      [ "CAPI_23_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a9c47e2e67e511f833f352aedaa1dafe6", null ],
      [ "CAPI_24_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a85f609979f997177f9cce7535df17502", null ],
      [ "CAPI_25_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a488f83119f4c93ffe20b98d8eb3955b0", null ],
      [ "CAPI_26_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589aa52c7f2fe62464ca93746d2e1d33b529", null ],
      [ "CAPI_27_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a5c24fb270daf816694a01eb0b07b456c", null ],
      [ "CAPI_28_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589aeb9ed2be779adb7669222fc95862acf6", null ],
      [ "CAPI_29_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589ab69757da62e1f87125f4541c307586bd", null ],
      [ "CAPI_30_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589a5bc3ec2287b9dd052ae348999d701c02", null ],
      [ "CAPI_31_MV_MARGIN_ADDED_ON_TOP", "capi__def_8h.html#a0765da15d636f7e8529203830a61b589ad76534b4960e69ca91513e72d0266aef", null ]
    ] ],
    [ "capi_voltage_ctrl_t", "capi__def_8h.html#a2c2e10d2062e7886d90656f4de527cc7", [
      [ "CAPI_VOLTAGE_INCREASE", "capi__def_8h.html#a2c2e10d2062e7886d90656f4de527cc7a1bf7e0c284b28947a116d6a236f3fd27", null ],
      [ "CAPI_VOLTAGE_NO_CHANGE", "capi__def_8h.html#a2c2e10d2062e7886d90656f4de527cc7ab6dec241f77ae1f64ce308dbb413a712", null ],
      [ "CAPI_VOLTAGE_DECREASE", "capi__def_8h.html#a2c2e10d2062e7886d90656f4de527cc7aeb5b4ebe959f2a79ce7200f23e8dcce1", null ]
    ] ],
    [ "capi_type_of_regulator_t", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363", [
      [ "CAPI_REGULATOR_4677", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363a49cc2b816e699c521fd7de3544c0eaf6", null ],
      [ "CAPI_REGULATOR_TPS546", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363a702f1e3c128b4b643f30d1b605cb8463", null ],
      [ "CAPI_REGULATOR_7180", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363af901e46247d91e9fcaee4a7798212128", null ],
      [ "CAPI_REGULATOR_TPS544", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363aeffb102fbd0852ffca93bf3095ded859", null ],
      [ "CAPI_REGULATOR_MAX77812", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363a2909cf1746f67bd1a7cea4b4437f078b", null ],
      [ "CAPI_REGULATOR_20730", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363a512584c1a01fef2a9e0d3a2415dd801d", null ],
      [ "CAPI_REGULATOR_4678", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363a9b0782269c98de2db0a10d8994b16596", null ],
      [ "CAPI_REGULATOR_INTERNAL", "capi__def_8h.html#afb5505053398a4235f9a5ad255f22363ac2027dcf16543441dbc5d8f0b44c1ad6", null ]
    ] ],
    [ "capi_avs_disable_type_t", "capi__def_8h.html#aee3de4449fd2465317a82eb21faddb88", [
      [ "CAPI_AVS_DISABLE_FIRMWARE_CONTROL", "capi__def_8h.html#aee3de4449fd2465317a82eb21faddb88aa50c8e93a6d0b25019ae7d6f4d228a17", null ],
      [ "CAPI_AVS_DISABLE_NO_FIRMWARE_CONTROL", "capi__def_8h.html#aee3de4449fd2465317a82eb21faddb88a175e3d7dac76a2dad460c5f4ddc07256", null ]
    ] ],
    [ "capi_avs_external_ctrl_step_t", "capi__def_8h.html#a2b90decbc877a28a342a0852d32a95fd", [
      [ "CAPI_AVS_EXTERNAL_CONTROL_STEP_1", "capi__def_8h.html#a2b90decbc877a28a342a0852d32a95fdae960b0f4b6a13d1326d8c3595927b419", null ],
      [ "CAPI_AVS_EXTERNAL_CONTROL_STEP_N", "capi__def_8h.html#a2b90decbc877a28a342a0852d32a95fda4cfe7d6cf24ad7b465c12d82cc2b784c", null ]
    ] ],
    [ "capi_intf_lane_st_t", "capi__def_8h.html#a36ca9a50d72cedac41df11c0a3c7a6ed", [
      [ "INTF_LANE_ST_IDLE", "capi__def_8h.html#a36ca9a50d72cedac41df11c0a3c7a6eda9085a9106e555e8f3a4391886f3176be", null ],
      [ "INTF_LANE_ST_IN_PROGRESS", "capi__def_8h.html#a36ca9a50d72cedac41df11c0a3c7a6edadef974800bb9fd4b18a6b9301323f2e1", null ],
      [ "INTF_LANE_ST_READY", "capi__def_8h.html#a36ca9a50d72cedac41df11c0a3c7a6eda7fd2a165bc7ebfb8a71c4f25d9d7e2a8", null ],
      [ "INTF_LANE_ST_FAIL", "capi__def_8h.html#a36ca9a50d72cedac41df11c0a3c7a6eda08ebd662aa85ef75fc2bad9eaa091524", null ]
    ] ],
    [ "capi_phy_interface_t", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6", [
      [ "CAPI_INTERFACE_Bypass", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a89c3233ffa5111dbbbc4920f3ef48b20", null ],
      [ "CAPI_INTERFACE_SR", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6af102f4c6c09a2046feeac6e2710ce7bf", null ],
      [ "CAPI_INTERFACE_SR4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6aaee10926bb740e8d825c6229db57637e", null ],
      [ "CAPI_INTERFACE_KX", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6abb86ab6278288b6d2ba7537f80fceb00", null ],
      [ "CAPI_INTERFACE_KX4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ad3e089a294be1956817f9c60aec3e756", null ],
      [ "CAPI_INTERFACE_KR", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ac574ff0fffab2a6b54f78ef23f20159e", null ],
      [ "CAPI_INTERFACE_KR2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a7b7548eec3672c105e0bf0355f581bd7", null ],
      [ "CAPI_INTERFACE_KR4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a5511d8849591b62f0339f898984c8985", null ],
      [ "CAPI_INTERFACE_CX", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a9a0ac2696ac30d8d592013d2e7071254", null ],
      [ "CAPI_INTERFACE_CX2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a48c2fe26298a3ad9ea257c1d62825fab", null ],
      [ "CAPI_INTERFACE_CX4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a48ca3d81767575bb579dab2128cb3b14", null ],
      [ "CAPI_INTERFACE_CR", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a3ab2b9ca08a769e26266accd31f369d7", null ],
      [ "CAPI_INTERFACE_CR2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a005e1c5cd9d04c9517c713ddf3baa038", null ],
      [ "CAPI_INTERFACE_CR4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a5dc462617b47f1bec43df37dcb033522", null ],
      [ "CAPI_INTERFACE_CR10", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ac22daa76c1d17ce7e53262f1028a18c0", null ],
      [ "CAPI_INTERFACE_XFI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a7fef1fb56ed226ada084562ac8f6568d", null ],
      [ "CAPI_INTERFACE_SFI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ae23536f4777e48906a4f6b11f4c5ca7a", null ],
      [ "CAPI_INTERFACE_SFPDAC", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6aefbdd6a0f3ceb9ffdc4daf161f2c8642", null ],
      [ "CAPI_INTERFACE_XGMII", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a60c87f99d32756cb96999548bfaf5438", null ],
      [ "CAPI_INTERFACE_1000X", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6af340e2df5eff86e54e97a41b97abd5f4", null ],
      [ "CAPI_INTERFACE_SGMII", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a3f3cb1eb187c671f9f1f0575aae0d497", null ],
      [ "CAPI_INTERFACE_XAUI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6aaae3f04da192080a699dbdfd462d1dda", null ],
      [ "CAPI_INTERFACE_RXAUI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ae80c67830f8444e5d1c3b8aa9f73a7f9", null ],
      [ "CAPI_INTERFACE_X2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6aa3be734c2839f33ad4358672056b71c3", null ],
      [ "CAPI_INTERFACE_XLAUI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a4dc72d64b671086c56a3df5674229f61", null ],
      [ "CAPI_INTERFACE_XLAUI2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ac850e49dc1247fdb1b809bdb49710188", null ],
      [ "CAPI_INTERFACE_CAUI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ab2a3397e4d7f11d181b32e7d749f2ad0", null ],
      [ "CAPI_INTERFACE_QSGMII", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6aafe26105277bb9dda8a9e08477572d87", null ],
      [ "CAPI_INTERFACE_LR4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6aa3764afe633a9a4e85d385c128b810bd", null ],
      [ "CAPI_INTERFACE_LR", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a6a6f5cce146f28f232c74ba2b3588060", null ],
      [ "CAPI_INTERFACE_LR2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ad51757848460c17a044bdf05df861095", null ],
      [ "CAPI_INTERFACE_ER", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6ae7654ba39cff0a8d47c1c5c45835a510", null ],
      [ "CAPI_INTERFACE_ER2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a3d65da88f0e6a0855fe133c3beb638b0", null ],
      [ "CAPI_INTERFACE_ER4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a631fb32e6ba762e2d5a6ed8a24b2934f", null ],
      [ "CAPI_INTERFACE_SR2", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a65edbbdf797bc1ebe466aa368d07f668", null ],
      [ "CAPI_INTERFACE_SR10", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a6fa69f7348a0fc5b87826670e62d8882", null ],
      [ "CAPI_INTERFACE_CAUI4", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a0b5cc89a929e511164364fa9f1b558d7", null ],
      [ "CAPI_INTERFACE_VSR", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a749e07b28a59617445d94b5013911f99", null ],
      [ "CAPI_INTERFACE_LR10", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a0dd350fea78008f79b8e2600e89c5f6d", null ],
      [ "CAPI_INTERFACE_KR10", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a74832ec11d9f71e4a67be768489e1e6e", null ],
      [ "CAPI_INTERFACE_CAUI4_C2C", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6adf3c297f2ba799fd8fad45cc6e2198b9", null ],
      [ "CAPI_INTERFACE_CAUI4_C2M", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a81bec1e16f3b49314c1d9f968897c9dd", null ],
      [ "CAPI_INTERFACE_ZR", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6afb151619359d2d2980843e5b8258883d", null ],
      [ "CAPI_INTERFACE_LRM", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a3d383a9832b6264a864ec87c5a0efa13", null ],
      [ "CAPI_INTERFACE_XLPPI", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a07e7c8167087f784c7b65f71d94f703d", null ],
      [ "CAPI_INTERFACE_OTN", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6a7f4b966ec67849e7be51026edfa91069", null ],
      [ "CAPI_INTERFACE_Count", "capi__def_8h.html#aec3b63667857ace4047ece86119956b6abe76aa7ba3a541b2e3d547e8c94f10cc", null ]
    ] ],
    [ "capi_link_sts_t", "capi__def_8h.html#a1b39535d3df1cbe05ea78362fe035d31", [
      [ "CAPI_LINK_STATUS_DOWN", "capi__def_8h.html#a1b39535d3df1cbe05ea78362fe035d31aba490c17b34ce0152dd00ceb5241f845", null ],
      [ "CAPI_LINK_STATUS_UP", "capi__def_8h.html#a1b39535d3df1cbe05ea78362fe035d31a39f9528318384a72ad775fe688dd5282", null ]
    ] ],
    [ "capi_test_pattern_type_t", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934", [
      [ "SSPRQ", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934a50ec77f570029a21583d7b31fc93131c", null ],
      [ "QPRBS13", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934a55620f563dde115784fd78a86129870a", null ],
      [ "SQUARE_WAVE", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934aa040931136591fc086b57b785f3c0fb1", null ],
      [ "PCS_SCRM_IDLE", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934ae3467598d6aea2fa4e334cd0819018ca", null ],
      [ "KP4_PRBS", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934a6326ec61836a66ae2d789a3ee5b9f055", null ],
      [ "NO_TEST_PATTERN", "capi__def_8h.html#a99bffe15694300d8ee9f62e0f8b5e934acfde1c0395dbec3ca9502d4aeb075fa1", null ]
    ] ],
    [ "capi_fec_mode_t", "capi__def_8h.html#a830de7a38dec1671fbac6104d45bb498", [
      [ "CAPI_FEC_INGRESS", "capi__def_8h.html#a830de7a38dec1671fbac6104d45bb498a31269068a45c4600117315c320337539", null ],
      [ "CAPI_FEC_EGRESS", "capi__def_8h.html#a830de7a38dec1671fbac6104d45bb498a56d7f270eccaf6dbb9adb0b9c56f1b39", null ],
      [ "CAPI_FEC_LINE", "capi__def_8h.html#a830de7a38dec1671fbac6104d45bb498a22f54a87f8532875cbbd09edb7dceb77", null ],
      [ "CAPI_FEC_CLIENT", "capi__def_8h.html#a830de7a38dec1671fbac6104d45bb498af753505d132d5ed3a9f38ba9ef1e18ff", null ],
      [ "CAPI_FEC_MODE_MAX", "capi__def_8h.html#a830de7a38dec1671fbac6104d45bb498a864961483bef20063d9f81a8280d5062", null ]
    ] ],
    [ "capi_lane_data_rate_t", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1", [
      [ "CAPI_LANE_DATA_RATE_NONE", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a7568c5f6e7bf3b8e2514efbedcf7b6d9", null ],
      [ "CAPI_LANE_DATA_RATE_1P25", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a8251e21c33893d87961f570c528ae373", null ],
      [ "CAPI_LANE_DATA_RATE_10P3125", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a5877ab612bff15f9a0f617f69efeab1d", null ],
      [ "CAPI_LANE_DATA_RATE_20P625", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a233da02ae6b6f0e6e8cf80b4bb79b931", null ],
      [ "CAPI_LANE_DATA_RATE_25P78125", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a7f03fa043c222e5e8c46a631203bb468", null ],
      [ "CAPI_LANE_DATA_RATE_26P5625", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a9d56d598509afb9399ba26d48127deaa", null ],
      [ "CAPI_LANE_DATA_RATE_28P125", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1abf724274cc680f4959c728ede0153782", null ],
      [ "CAPI_LANE_DATA_RATE_51P5625", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a9b6e764513a6e4f689e8f26bf15585db", null ],
      [ "CAPI_LANE_DATA_RATE_53P125", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1acbf8328fbc8b1efba82468fc617ca013", null ],
      [ "CAPI_LANE_DATA_RATE_56P25", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a59657865c83fd3d3b53a4b0785fcb526", null ],
      [ "CAPI_LANE_DATA_RATE_MAX", "capi__def_8h.html#acc25f418fbde936f7e55eda43bc748c1a32a11f19380ce3d3b8008efd5bd686cb", null ]
    ] ],
    [ "capi_pll_ratio_type_t", "capi__def_8h.html#aaf6c48fc8a4da66a513668ff2f4c27fa", [
      [ "CAPI_CW_LW_PLL_MULTIPLIER_170", "capi__def_8h.html#aaf6c48fc8a4da66a513668ff2f4c27faa68d95d71cb8ab9d9925c06301b934b93", null ],
      [ "CAPI_CW_LW_PLL_MULTIPLIER_165", "capi__def_8h.html#aaf6c48fc8a4da66a513668ff2f4c27faa16e7b47d072f64de6d9a4e56cdcd031c", null ],
      [ "CAPI_CW_LW_PLL_MULTIPLIER_168", "capi__def_8h.html#aaf6c48fc8a4da66a513668ff2f4c27faaa1379a0781ef96d4d9786e27c4c681e1", null ],
      [ "CAPI_CW_LW_PLL_TYPE_MAX", "capi__def_8h.html#aaf6c48fc8a4da66a513668ff2f4c27faa2da272ed6c55c68b3c86408f02aa5d42", null ]
    ] ],
    [ "capi_bh_pll_ratio_type_t", "capi__def_8h.html#ad76bc4a161411e5e8c9d73ec8b9c5a49", [
      [ "CAPI_CW_BH_PLL_MULTIPLIER_170", "capi__def_8h.html#ad76bc4a161411e5e8c9d73ec8b9c5a49a360c8e77a0bbcd641c2f18791887dbdc", null ],
      [ "CAPI_CW_BH_PLL_MULTIPLIER_165", "capi__def_8h.html#ad76bc4a161411e5e8c9d73ec8b9c5a49adb6c004b86a2fd182bb34741ce71bf83", null ],
      [ "CAPI_CW_BH_PLL_MULTIPLIER_160", "capi__def_8h.html#ad76bc4a161411e5e8c9d73ec8b9c5a49ac2fac60dbf8aa4e3c38098cfe7c0c7fc", null ],
      [ "CAPI_CW_BH_PLL_TYPE_MAX", "capi__def_8h.html#ad76bc4a161411e5e8c9d73ec8b9c5a49af5665fa1e4e224a367068f9ece61311d", null ]
    ] ],
    [ "capi_egr_port_type_t", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467a", [
      [ "EGR_PAM4_RPTR", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa639bc919261e173993bf4f383fe77cc6", null ],
      [ "EGR_NRZ_RPTR", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa961a91bcbf0a0b547dfd9eacbfedac75", null ],
      [ "EGR_P2N_RGB_BM", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aafc341eb135cfd7488e962f9086071e97", null ],
      [ "EGR_N2P_FGB_BM", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aaf3797b8afbc1759b3e93e54f7751383f", null ],
      [ "EGR_N2N_RGB_BM", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aadea352f4b90fddd1ee0cc8a9d7bc9c21", null ],
      [ "EGR_N2N_FGB_BM", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa9ac51428ee016dbe25003c0579f646ae", null ],
      [ "EGR_P2N_RGB_FEC", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa031725d9ea14b9875c1f232c107bb823", null ],
      [ "EGR_N2P_FGB_FEC", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa8dfd37e099e2583217b553b21f51714f", null ],
      [ "EGR_PAM4_HMUX", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa65819e2b68f9bb863408b3adc595bd23", null ],
      [ "EGR_NRZ_HMUX", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aad494e465495912d5204983302836e52c", null ],
      [ "EGR_P2N_RGB_HMUX", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa7f06898a492caee333ad83d722318136", null ],
      [ "EGR_N2N_RGB_HMUX", "capi__def_8h.html#ad6b787f891c6c5f0f5f4e5417b85467aa845802071b15fe82af8301c6163f7434", null ]
    ] ],
    [ "capi_igr_port_type_t", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49", [
      [ "IGR_PAM4_RPTR", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a0b97e43b77d958287a8bd6ec518e620f", null ],
      [ "IGR_NRZ_RPTR", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a97b2668e3538b72f9feabd6e778d818b", null ],
      [ "IGR_N2P_FGB_BM", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49af4f7c26bb739ba8be53a6c478bae2b20", null ],
      [ "IGR_P2N_RGB_BM", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a0a586bdf84deab1200f0695bf35c8484", null ],
      [ "IGR_N2N_FGB_BM", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a9f3ea3183379b1ff932033578b7b889f", null ],
      [ "IGR_N2N_RGB_BM", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a5d8441a9071e3f338ce891541b6c449f", null ],
      [ "IGR_N2P_FGB_FEC", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49aa4d1a964024334e8fd4777280b73fe98", null ],
      [ "IGR_P2N_RGB_FEC", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49abd8907f8082086a1aa3a30e7efbe5986", null ],
      [ "IGR_PAM4_RPTR_BCST", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49af2d150b3b802364e32f9a431d20fdcc2", null ],
      [ "IGR_NRZ_RPTR_BCSM", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a4f036ef8a9c2e284feb714aebeafcee0", null ],
      [ "IGR_N2P_FGB_FEC_BCST", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a072d1cbdf67d9d545cb27dc1d82ba781", null ],
      [ "IGR_N2N_FGB_BM_BCST", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a43f6288a15ecb7f12c92e12ace8f5187", null ],
      [ "IGR_NRZ_RPTR_BCST", "capi__def_8h.html#a3b1d1e0f4681465284cfb69a78754e49a393e9cb163cb173f0b6793eb4a7367a3", null ]
    ] ],
    [ "capi_host_osr_t", "capi__def_8h.html#a52913437449467f7c2db298677d6fcf3", [
      [ "CAPI_CW_BH_OSR_1", "capi__def_8h.html#a52913437449467f7c2db298677d6fcf3a5a2dd5fd0c811fe997429ec355586aba", null ]
    ] ],
    [ "capi_line_osr_t", "capi__def_8h.html#a903a1807f91f5d183da512f72bfc6e07", [
      [ "CAPI_CW_LW_OSR_1", "capi__def_8h.html#a903a1807f91f5d183da512f72bfc6e07a5d4c1e3244c76e0514a55972eca8127d", null ],
      [ "CAPI_CW_LW_OSR_2", "capi__def_8h.html#a903a1807f91f5d183da512f72bfc6e07a7709bd3f2fab00cc9330277746e09886", null ]
    ] ],
    [ "capi_q0q1_t", "capi__def_8h.html#a399ee1d5b43240b155b36e056bcb94dc", [
      [ "CW_QUAD_Q0", "capi__def_8h.html#a399ee1d5b43240b155b36e056bcb94dca387a24706d94915e8bbd203c0ebd0f9e", null ],
      [ "CW_QUAD_Q1", "capi__def_8h.html#a399ee1d5b43240b155b36e056bcb94dcaa60a74395d5cf4f3dc4f9dc2c8dce3ef", null ]
    ] ],
    [ "capi_host_inst_t", "capi__def_8h.html#a4cf37ae25c4788e173004424b71f8d8f", [
      [ "CW_INST_HOST_0", "capi__def_8h.html#a4cf37ae25c4788e173004424b71f8d8fafa18f62bf1d096a1b800959a73b57f48", null ],
      [ "CW_INST_HOST_1", "capi__def_8h.html#a4cf37ae25c4788e173004424b71f8d8faa38071d7b6369963abf5b2d93379decd", null ]
    ] ],
    [ "capi_hostline_t", "capi__def_8h.html#a4debd965786ad82360e9297de09683d9", [
      [ "CW_INST_LINE", "capi__def_8h.html#a4debd965786ad82360e9297de09683d9af94ff5f56abfe2bde21b758fe1e65a09", null ],
      [ "CW_INST_HOST", "capi__def_8h.html#a4debd965786ad82360e9297de09683d9af65f7eabcb69613d50515731245209a7", null ]
    ] ],
    [ "capi_ch_t", "capi__def_8h.html#a403a1a9c659d2f5e59eeab852345b563", [
      [ "CW_FEC_CORE_CH0A", "capi__def_8h.html#a403a1a9c659d2f5e59eeab852345b563a027f0af1d0b56c51c65d36a7b5112d3f", null ],
      [ "CW_FEC_CORE_CH0B", "capi__def_8h.html#a403a1a9c659d2f5e59eeab852345b563a66a0c65b2d9c6b503783f69d7fccabbd", null ],
      [ "CW_FEC_CORE_CH1A", "capi__def_8h.html#a403a1a9c659d2f5e59eeab852345b563a6d7d4c893299233aeb98b1ef2735f8e4", null ],
      [ "CW_FEC_CORE_CH1B", "capi__def_8h.html#a403a1a9c659d2f5e59eeab852345b563a46230c5db71466fc49916fc33a2dff17", null ]
    ] ],
    [ "capi_rm_ovrd_type_t", "capi__def_8h.html#aba7d6e2c6a3535849f0ed7f1edda6453", [
      [ "CAPI_RM_OVRD_NA", "capi__def_8h.html#aba7d6e2c6a3535849f0ed7f1edda6453a57375fbf51f11045184b4d5b9abfc921", null ],
      [ "CAPI_RM_OVRD_NO", "capi__def_8h.html#aba7d6e2c6a3535849f0ed7f1edda6453a9cc44150b5dd105e995584116373e161", null ],
      [ "CAPI_RM_OVRD_YES", "capi__def_8h.html#aba7d6e2c6a3535849f0ed7f1edda6453a0f348a876ebc425c11b6dcf0de573d1d", null ]
    ] ],
    [ "return_stat_t", "capi__def_8h.html#a040a35159e52d6b65f79162397c83cb2", [
      [ "PORT_NO_MATCH", "capi__def_8h.html#a040a35159e52d6b65f79162397c83cb2aca7815766ab1c4fe35582c59f7d2e19b", null ],
      [ "PORT_MATCH_FOUND", "capi__def_8h.html#a040a35159e52d6b65f79162397c83cb2a912e32e65817c3ebe8dec8f294db7bf2", null ],
      [ "PORT_MATCH_MULTI", "capi__def_8h.html#a040a35159e52d6b65f79162397c83cb2aff336039047381bd7f3f315e1301cd8f", null ]
    ] ],
    [ "capi_archive_type_t", "capi__def_8h.html#a0f5d21790e85dd2f262170d654accdae", [
      [ "CAPI_ARCHIVE_INFO_EPDM_TYPE", "capi__def_8h.html#a0f5d21790e85dd2f262170d654accdaea67d97eea87d557529cd3cffe3603d8c7", null ]
    ] ],
    [ "poll_feature_t", "capi__def_8h.html#a0a30338d387292a5b1782e6946c2f9bd", [
      [ "SNR_POLL", "capi__def_8h.html#a0a30338d387292a5b1782e6946c2f9bdaafa3919df1a58f42468e7c23637d41d8", null ]
    ] ],
    [ "capi_dsp_mode_type_t", "capi__def_8h.html#a90da17fa7ac343106a084201af844c5c", [
      [ "CAPI_DSP_OPTICAL_MODE", "capi__def_8h.html#a90da17fa7ac343106a084201af844c5ca6032c816b81619e850cc01d5ff6a69ad", null ],
      [ "CAPI_DSP_ELECTRIC_MODE", "capi__def_8h.html#a90da17fa7ac343106a084201af844c5ca95214b8d128781222b8ff839546c48bf", null ]
    ] ],
    [ "capi_dsp_tc_tx_type_t", "capi__def_8h.html#ac91b64d175493a6d5438a710b185281a", [
      [ "CAPI_DSP_TC_TX_DIFFERENTIAL_MODE", "capi__def_8h.html#ac91b64d175493a6d5438a710b185281aac151dad75d746ec4a6123626d7e5c5f8", null ],
      [ "CAPI_DSP_TC_TX_SE_MODE", "capi__def_8h.html#ac91b64d175493a6d5438a710b185281aaa4b9d2bbc04a58cd9fdb16e4be948065", null ]
    ] ],
    [ "capi_set_chip_default_info", "capi__def_8h.html#a8c2574c0405ebdb19e037079f1cd569c", null ]
];