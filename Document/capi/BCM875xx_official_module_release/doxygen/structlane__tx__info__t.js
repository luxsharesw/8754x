var structlane__tx__info__t =
[
    [ "content", "structlane__tx__info__t.html#adf082da9a87849e99597bffcdba06eba", null ],
    [ "dsp_precode", "structlane__tx__info__t.html#a4b0fdd8d534b064e81c2e028b77a32e5", null ],
    [ "dsp_skewp", "structlane__tx__info__t.html#a59d9e87b2c62f7e3bcd68c2aadf03243", null ],
    [ "dsp_high_swing", "structlane__tx__info__t.html#a7249bd1ed5156bd54c9d481d8b477617", null ],
    [ "dsp_graycode", "structlane__tx__info__t.html#a53a2932f78ab84ef4fe0dd636e68d488", null ],
    [ "dsp_level_shift", "structlane__tx__info__t.html#a2a8f4b0c657a4c26c7275a9548ebe403", null ],
    [ "symbol_swap", "structlane__tx__info__t.html#a6c719262e7b70b6701a155fef17565c1", null ],
    [ "txfir", "structlane__tx__info__t.html#ac8bcbc40d5665b9dc64792f805f6a371", null ],
    [ "reserved", "structlane__tx__info__t.html#a8fb981e8eb5dcbf29bc3f999745d9d8b", null ],
    [ "is", "structlane__tx__info__t.html#a9cf74e67421c1e6445c03578e58e6848", null ],
    [ "param", "structlane__tx__info__t.html#ad15478dc3105daa66ffecee4296c6cd2", null ],
    [ "txfir", "structlane__tx__info__t.html#a3812bcab4b3c207f0f8bc8594577c8ec", null ],
    [ "symbol_swap", "structlane__tx__info__t.html#aed3e341312e9e76659c90e91a7cad765", null ],
    [ "dsp_level_shift", "structlane__tx__info__t.html#ac6763fd5c3e2828959caab67441ea6e5", null ],
    [ "dsp_graycode", "structlane__tx__info__t.html#ac073c8fc67228177f32abac0fbcd34aa", null ],
    [ "dsp_high_swing", "structlane__tx__info__t.html#ad6dd4f10e45d34dcbaf0c9f5d563eb94", null ],
    [ "dsp_skewp", "structlane__tx__info__t.html#a8e9634035944ca46ee60ecc3580ebcf4", null ],
    [ "dsp_precode", "structlane__tx__info__t.html#a882da30608ba1396dedc0e179dd2dbc6", null ],
    [ "value", "structlane__tx__info__t.html#ae5d0fd46b1f0d55f9679bfc30c50f55f", null ]
];