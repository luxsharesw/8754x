var unioncommon__lw__lane__config1__reg__t =
[
    [ "words", "unioncommon__lw__lane__config1__reg__t.html#a0881ff000b3fe21a53caf7588f75d594", null ],
    [ "lw_optical_mode", "unioncommon__lw__lane__config1__reg__t.html#a476327027f8b05b159191cc36539d6e7", null ],
    [ "lw_extd_mode", "unioncommon__lw__lane__config1__reg__t.html#afbe4d94009f40eb4cae570c68ed5b019", null ],
    [ "lw_rloop", "unioncommon__lw__lane__config1__reg__t.html#a058ae2e2ff53a50404209fbcbc684983", null ],
    [ "lw_gloop", "unioncommon__lw__lane__config1__reg__t.html#a6fbcb9825cd2282b15b1ee8aa234af6f", null ],
    [ "lw_link_training", "unioncommon__lw__lane__config1__reg__t.html#a38528524ab1c1e89d45e76737016b003", null ],
    [ "lw_lnk_train_frame_size", "unioncommon__lw__lane__config1__reg__t.html#a8ec1fc18dd65c7d8991f120dfd2dedde", null ],
    [ "lw_auto_neg", "unioncommon__lw__lane__config1__reg__t.html#a126ead024b6b2977eabd5bbe0dc0e6aa", null ],
    [ "lw_lnk_trn_init_cond", "unioncommon__lw__lane__config1__reg__t.html#aaa47281502d426ce04d497260cf07561", null ],
    [ "lw_lnk_trn_restart", "unioncommon__lw__lane__config1__reg__t.html#a3b1115a40d7e364aa6304ae7d60d7ed3", null ],
    [ "lw_lnk_trn_dis_timer", "unioncommon__lw__lane__config1__reg__t.html#aec14c595bd62f49d9209bd6eb99882f0", null ],
    [ "lw_lnk_trn_syscdr_first", "unioncommon__lw__lane__config1__reg__t.html#a31480710366772df39c72ad84b7202ee", null ],
    [ "fields", "unioncommon__lw__lane__config1__reg__t.html#a492f0b6751f95f3a09c4831bbac4579c", null ]
];