var unioncommon__lw__lane__config4__reg__t =
[
    [ "words", "unioncommon__lw__lane__config4__reg__t.html#ae76bec827b9a27bc65be8deb07d14fb1", null ],
    [ "lw_ffe_mu_override", "unioncommon__lw__lane__config4__reg__t.html#a41da5dbbb467bf45ca7b5ebd2c58939f", null ],
    [ "lw_ffe_mu", "unioncommon__lw__lane__config4__reg__t.html#aa48564698c088f8429bdcd8b6712f418", null ],
    [ "lw_ffe_mu_tracking_override", "unioncommon__lw__lane__config4__reg__t.html#add2d1aa592150d939da35e99030e98c0", null ],
    [ "lw_ffe_mu_tracking", "unioncommon__lw__lane__config4__reg__t.html#a377a4e188ba00d6d90d9d3a8eee343b0", null ],
    [ "lw_phd_mode_override", "unioncommon__lw__lane__config4__reg__t.html#a70bf7ba91ffd8f71ba6404b5191077e9", null ],
    [ "lw_phd_mode", "unioncommon__lw__lane__config4__reg__t.html#a75ddc68de7dfe51cdf627e92e6ba0af9", null ],
    [ "lw_eq_tap_sel", "unioncommon__lw__lane__config4__reg__t.html#a3f9178e452e3a677cbe1c880217a1ce3", null ],
    [ "lw_eqpp_post", "unioncommon__lw__lane__config4__reg__t.html#a13b6d8bfc01accc4b125dc50e521de0c", null ],
    [ "fields", "unioncommon__lw__lane__config4__reg__t.html#abb1717b79225ee4cce8db62a0ae0b432", null ]
];