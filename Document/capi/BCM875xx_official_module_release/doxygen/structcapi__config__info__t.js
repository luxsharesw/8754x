var structcapi__config__info__t =
[
    [ "ref_clk", "structcapi__config__info__t.html#a030ba4bd3e6504e3a962a2e947a11dbf", null ],
    [ "func_mode", "structcapi__config__info__t.html#a922ae0f45182aeeff6e59414b895d54f", null ],
    [ "fec_term", "structcapi__config__info__t.html#a8eb5c33d84c55986fbbd3c7126b2da76", null ],
    [ "mux_type", "structcapi__config__info__t.html#a4a2e58e6d0a6278b8dac1193e6c2d3f7", null ],
    [ "line_fec_type", "structcapi__config__info__t.html#ac26f60ebb5932cfb8dcf8ffe2e4a2980", null ],
    [ "host_fec_type", "structcapi__config__info__t.html#a6287cb2466ffdd66105b32705febae40", null ],
    [ "bh_br", "structcapi__config__info__t.html#acccb80c3a7776e01fa138b02a075489c", null ],
    [ "lw_br", "structcapi__config__info__t.html#a2ce689804249c619ba6799601ca0afec", null ],
    [ "line_lane", "structcapi__config__info__t.html#a8b0b70eaad4611a55c28d8b30635cd95", null ],
    [ "host_lane", "structcapi__config__info__t.html#a16f0e6ccb2992142571769e65ec197b9", null ],
    [ "status", "structcapi__config__info__t.html#af072801f36af3cf758282bfcdc1dd327", null ],
    [ "pwd_status", "structcapi__config__info__t.html#a3b620cbb0941e162d7a4b550c39807aa", null ]
];