var structcw__port__entry__ex__t =
[
    [ "port_type", "structcw__port__entry__ex__t.html#a6d8c962901567ab70ec6958f37131d2b", null ],
    [ "func_mode", "structcw__port__entry__ex__t.html#a0685ba1c24271db460fa8992210a5a78", null ],
    [ "fec_term", "structcw__port__entry__ex__t.html#abaead1c275ca3bf5660e80a067eea578", null ],
    [ "host_fec", "structcw__port__entry__ex__t.html#abfb890ee30f28536827d7d930692799a", null ],
    [ "line_fec", "structcw__port__entry__ex__t.html#ab82f281399ba2e50b5a6b1a0feebc823", null ],
    [ "bh_cfg", "structcw__port__entry__ex__t.html#aca5b053dc0a0f2d0c9bc4efc938a3a3b", null ],
    [ "lw_cfg", "structcw__port__entry__ex__t.html#a36d1c54215194e0f733a1cfef38e1970", null ],
    [ "port_mux", "structcw__port__entry__ex__t.html#a5021d5cc9d392c2d3831bcfd156c8cc5", null ],
    [ "bh_lane_mask", "structcw__port__entry__ex__t.html#a57db3344bfdc25f59072f9933b5a46a2", null ],
    [ "lw_lane_mask", "structcw__port__entry__ex__t.html#abea6dabfee75d2071b679f201be02e9c", null ]
];