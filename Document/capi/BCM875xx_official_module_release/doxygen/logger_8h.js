var logger_8h =
[
    [ "logger_t", "structlogger__t.html", "structlogger__t" ],
    [ "dprintf", "logger_8h.html#a0fb7bf82c787085c8b1ef0ad7d028424", null ],
    [ "logprintf", "logger_8h.html#a7e496370fd14077ec47f8f2cfe269f17", null ],
    [ "dprintf_time", "logger_8h.html#ac8f18a66afcf8a644c5c59371a519414", null ],
    [ "ddprintf", "logger_8h.html#adc616d59037815877252fdebe90c0aea", null ],
    [ "LogMsg", "logger_8h.html#aee22d5e4737a05e4b5741507814f219c", null ],
    [ "LogMsgLevel", "logger_8h.html#a48966c1673437fcf8014c4cc69031d3d", null ],
    [ "srds_access_t", "logger_8h.html#a4317941b7b831f55c324481c438e00f7", null ],
    [ "logger_open", "logger_8h.html#a96b72af2dcf6cfc38195e2c7b398e8be", null ],
    [ "logger_set_verbose_level", "logger_8h.html#accfb6f6167fd850c69b19ca740c6b5cc", null ],
    [ "logger_get_verbose_level", "logger_8h.html#a73732b7cca07140b7aa0d32c88433d88", null ],
    [ "logger_write", "logger_8h.html#a7fd130cc1dd8b5ca2d0dd47637ec6cfc", null ],
    [ "logger_only_write", "logger_8h.html#a467832612d505f96d30b225cffadb442", null ],
    [ "logger_print_timestamp", "logger_8h.html#a2aab08310c0a17312556a4f5d3d0b15d", null ],
    [ "logger_close", "logger_8h.html#aff849e36eaf2015674a4a85adafde4b0", null ]
];