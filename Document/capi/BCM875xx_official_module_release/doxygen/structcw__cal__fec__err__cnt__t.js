var structcw__cal__fec__err__cnt__t =
[
    [ "tot_frame_cnt", "structcw__cal__fec__err__cnt__t.html#aac7b02d2a3e14845af51fa60dedb98ac", null ],
    [ "tot_frame_uncorr_cnt", "structcw__cal__fec__err__cnt__t.html#a65757f2209ff63897609f4dd78a917f9", null ],
    [ "tot_bits_corr_cnt", "structcw__cal__fec__err__cnt__t.html#a137cd6cc7807749379de0c757ec88c6b", null ],
    [ "prbs_errcnt", "structcw__cal__fec__err__cnt__t.html#aada27c022673289b530ce4348c8edf15", null ],
    [ "fec_frame_size", "structcw__cal__fec__err__cnt__t.html#ae0bc375de11b540d4db4165fe643077c", null ],
    [ "uncorr_bits_per_frame", "structcw__cal__fec__err__cnt__t.html#a7a735a20e9ebb419d50493cdc32b73e7", null ]
];