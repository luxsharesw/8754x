var structdiag__cmis__info__t =
[
    [ "content", "structdiag__cmis__info__t.html#ab38b0db955e12d8372aea1eb148fbd17", null ],
    [ "dsp_snr_ltp", "structdiag__cmis__info__t.html#a8c3f327f91e7e6b5ae97d85f1b9dea97", null ],
    [ "serdes_snr_ltp", "structdiag__cmis__info__t.html#abfddcdb833c88a896b5692e1a7986bd5", null ],
    [ "reserved", "structdiag__cmis__info__t.html#a2e3d2810cd01baef2680aa9246a70872", null ],
    [ "is", "structdiag__cmis__info__t.html#ae5ff4d899e3826effc8f874887d38e43", null ],
    [ "param", "structdiag__cmis__info__t.html#aa721ae5d7361977c56f6a93d782f0370", null ],
    [ "dsp_snr_ltp", "structdiag__cmis__info__t.html#aad99dd3391dfd4a16921185a5a185cc1", null ],
    [ "serdes_snr_ltp", "structdiag__cmis__info__t.html#a951322e1fc59e4942b662400e81102e1", null ],
    [ "value", "structdiag__cmis__info__t.html#a5d5f875428cbec242c17185636363e03", null ]
];