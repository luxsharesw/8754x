var hr__time_8c =
[
    [ "GPTIMER0_CURR_CNT", "hr__time_8c.html#a621b8d1633e9719a6c520e33ba1b3d5e", null ],
    [ "GPTIMER_1_US_INTERVAL", "hr__time_8c.html#ae5019db763e886ec15fb67db92bb2854", null ],
    [ "phy_delay_us", "hr__time_8c.html#abc9f3574fad068d0ae4d0d624d203825", null ],
    [ "delay_s", "hr__time_8c.html#ada592bfee05feffe520ef2e57f022830", null ],
    [ "delay_ms", "hr__time_8c.html#aae493176895610a6326417ea9ea3f57c", null ],
    [ "delay_us", "hr__time_8c.html#a86ce98aa6d8c38be0bb34599e98dbd00", null ],
    [ "HATS_DelayMs", "hr__time_8c.html#a11e16190c260711fb00f56ae039ec259", null ],
    [ "HATS_DelayUsec", "hr__time_8c.html#a03500e2fc1fecb4ef60f19d8270311ea", null ],
    [ "_get_timestamp", "hr__time_8c.html#a50c7e1c235bd2acdb10f9fe34ab9a986", null ],
    [ "_get_elapsed_timestamp", "hr__time_8c.html#ae745285b2152e215cfcf955249849798", null ]
];