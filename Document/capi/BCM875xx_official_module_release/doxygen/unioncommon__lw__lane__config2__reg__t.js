var unioncommon__lw__lane__config2__reg__t =
[
    [ "words", "unioncommon__lw__lane__config2__reg__t.html#a3f1f7375b9aaa52d09aba421270bcc06", null ],
    [ "lw_invert_rx", "unioncommon__lw__lane__config2__reg__t.html#a106849c9995867a6b57a6844d5af666e", null ],
    [ "lw_rx_symbol_swap", "unioncommon__lw__lane__config2__reg__t.html#ac16feec4e418652bb291883fb5272b9b", null ],
    [ "lw_rx_graycode", "unioncommon__lw__lane__config2__reg__t.html#a19797447347f8d46d08d84a026c50785", null ],
    [ "lw_snr_lvl", "unioncommon__lw__lane__config2__reg__t.html#a7a4c4e7e1a111b05b17b7f7964712943", null ],
    [ "lw_los_ignore", "unioncommon__lw__lane__config2__reg__t.html#ac45af501da69840cd8f5f476130e5947", null ],
    [ "lw_peaking_en", "unioncommon__lw__lane__config2__reg__t.html#a3e9db8a5e647fe509dc32c6376bcd5ea", null ],
    [ "lw_peaking", "unioncommon__lw__lane__config2__reg__t.html#a25636c496a63841b961de636e357c370", null ],
    [ "lw_kp_hlf_stp", "unioncommon__lw__lane__config2__reg__t.html#a2978eeddc3b83abb5f7da67ff59b2032", null ],
    [ "lw_kp_track_hlf_stp", "unioncommon__lw__lane__config2__reg__t.html#a34d6aa0d22abb4460a9c8dd69bfa24a1", null ],
    [ "lw_rx_disable", "unioncommon__lw__lane__config2__reg__t.html#a50716e8529ee7deff61945e6de462525", null ],
    [ "fields", "unioncommon__lw__lane__config2__reg__t.html#af1ae5822d4dbec30cf72672de85b440a", null ]
];