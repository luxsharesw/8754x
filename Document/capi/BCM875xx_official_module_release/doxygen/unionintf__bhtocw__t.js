var unionintf__bhtocw__t =
[
    [ "words", "unionintf__bhtocw__t.html#a50c748a2e58a6faeb398d7aee0baacb7", null ],
    [ "cdr_ppm", "unionintf__bhtocw__t.html#a93db74c979ea67377c50b31fc4008a81", null ],
    [ "txpi_conv_fail", "unionintf__bhtocw__t.html#aa362b7bf7c3d4a57074d3359528236cb", null ],
    [ "txpi_done", "unionintf__bhtocw__t.html#a4f994940f66bc5fc5fe2d64f8de3319b", null ],
    [ "cdr_lock", "unionintf__bhtocw__t.html#adfac1363b279337319efa4f6334c1587", null ],
    [ "gloopback_en", "unionintf__bhtocw__t.html#ad97a9efe5cec7ff58d1fae43bfd0f7fc", null ],
    [ "lane_cfg_stat", "unionintf__bhtocw__t.html#a70bbcc26f5edc223c4fba88ce4765ff9", null ],
    [ "fields", "unionintf__bhtocw__t.html#a455772a820be3d3fdeb1e0951f362311", null ]
];