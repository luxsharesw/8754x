var host__diag_8h =
[
    [ "host_diag_dmode_e", "host__diag_8h.html#affc40e738d0c19f36c31aeec1e9bd522", [
      [ "DMODE_NRZ", "host__diag_8h.html#affc40e738d0c19f36c31aeec1e9bd522a7b55651618618a8dbc29501ff4bcd1d7", null ],
      [ "DMODE_EPAM2", "host__diag_8h.html#affc40e738d0c19f36c31aeec1e9bd522aa100badb44d77b55a12ddd48b2039a58", null ],
      [ "DMODE_PAM4", "host__diag_8h.html#affc40e738d0c19f36c31aeec1e9bd522a3059980a87dd0a80beb3a361ae77d968", null ],
      [ "DMODE_EPAM4", "host__diag_8h.html#affc40e738d0c19f36c31aeec1e9bd522aa1fa947a58efd009efea45a22b66df8e", null ]
    ] ],
    [ "host_util_get_lw_phy_info", "host__diag_8h.html#a0d3dafc3497d554ee7673ed64876eb0c", null ],
    [ "host_util_get_lane_lw_top_pam_bbaddr", "host__diag_8h.html#aabf2319f95d32b0eed49fdf4769ac334", null ],
    [ "host_lw_get_snr", "host__diag_8h.html#a7e97c2b8dfb7c42afc8fc794f42389f7", null ],
    [ "host_lw_get_snr_wo_suspend_resume", "host__diag_8h.html#a49b4424e1aeb2e9ef5488d88716b504b", null ],
    [ "host_lw_get_lvl_snr", "host__diag_8h.html#a0484cb11c9bf6d3e74bcf417e1046390", null ],
    [ "host_lw_get_usr_diagnostics", "host__diag_8h.html#a09d2375d6d30d7760d899ca8c7664c72", null ],
    [ "host_lw_get_usr_cmis_diagnostics", "host__diag_8h.html#a98d4b8ee6509cdd2f96c2e3059130118", null ],
    [ "host_client_get_usr_cmis_diagnostics", "host__diag_8h.html#a9c97378756614bd41cd75fea8897d4f7", null ],
    [ "host_dsp_get_slicer_histogram_diagnostics", "host__diag_8h.html#acc31e427f8678ebad9b6509a6d59343b", null ],
    [ "host_lw_get_smode", "host__diag_8h.html#a61aeb5e14db44e23adb6fe7548c3086f", null ],
    [ "host_set_mpi_config", "host__diag_8h.html#a90d8155451d684064d678f39bd29bc89", null ],
    [ "host_get_mpi_config", "host__diag_8h.html#a76f0f7d3a5d316bd7c1aea18f506e9dd", null ],
    [ "host_get_mpi_state", "host__diag_8h.html#a52daf4a6076736488004ce6934095dbc", null ],
    [ "host_set_mpi_dynamic_config", "host__diag_8h.html#a7462fbf930bafe77b9adf0e98437a611", null ],
    [ "host_get_mpi_dynamic_config", "host__diag_8h.html#a2151124a04af8746eaee73c8f6acecba", null ],
    [ "host_get_dynamic_mpi_state", "host__diag_8h.html#a24c655a347c19dc8ed815e7b9446fdf6", null ]
];