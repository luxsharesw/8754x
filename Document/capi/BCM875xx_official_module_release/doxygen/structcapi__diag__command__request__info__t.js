var structcapi__diag__command__request__info__t =
[
    [ "command_id", "structcapi__diag__command__request__info__t.html#a4e2de642a4b334a346448bc8981dbf33", null ],
    [ "cmis_snr_ltp", "structcapi__diag__command__request__info__t.html#a8137d6f68c9fee5ec3464207ddbd83b8", null ],
    [ "mpi_cfg", "structcapi__diag__command__request__info__t.html#a1ffffddb69afb137db2c93fddbc7d650", null ],
    [ "mpi_st", "structcapi__diag__command__request__info__t.html#a48260aaff701873ae6da08f3126f2ea5", null ],
    [ "mission_mpi_cfg", "structcapi__diag__command__request__info__t.html#a795520b1168e826223f0629ba492c9ff", null ],
    [ "mission_mpi_st", "structcapi__diag__command__request__info__t.html#a09910f10817493adf377f15f3296a830", null ],
    [ "serdes_diag_info", "structcapi__diag__command__request__info__t.html#a864753e3dca8b4518812ef68103d0db3", null ],
    [ "type", "structcapi__diag__command__request__info__t.html#a3771f44bb7b928d632cc7212534b9601", null ]
];