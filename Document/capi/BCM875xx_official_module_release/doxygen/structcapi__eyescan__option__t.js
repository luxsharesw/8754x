var structcapi__eyescan__option__t =
[
    [ "linerate_in_khz", "structcapi__eyescan__option__t.html#a87a0a041c865bf13e4ffc7424b688656", null ],
    [ "timeout_in_ms", "structcapi__eyescan__option__t.html#a17c27945ca5229e6a80e1d97ec5bf15d", null ],
    [ "horz_max", "structcapi__eyescan__option__t.html#a43f2807aff59a1c274a071dc1963ddd8", null ],
    [ "horz_min", "structcapi__eyescan__option__t.html#a0544dc47be6e4f18d15645f465dc4cb7", null ],
    [ "hstep", "structcapi__eyescan__option__t.html#a3cf81684f0548b91471f87ce4c3a3ef8", null ],
    [ "vert_max", "structcapi__eyescan__option__t.html#af7228597e02faaa4eaee369bfc0dab0d", null ],
    [ "vert_min", "structcapi__eyescan__option__t.html#a707d0a2304ecb6209ea1c29c60bf14e8", null ],
    [ "vstep", "structcapi__eyescan__option__t.html#a9be0dfde3f2209e1ae637053c05f4891", null ],
    [ "ber_proj_scan_mode", "structcapi__eyescan__option__t.html#a81d217cf06fd8c4dd068d16d7064ce1b", null ],
    [ "ber_proj_timer_cnt", "structcapi__eyescan__option__t.html#ab449d3489da6a3ede1998b3c894f6891", null ],
    [ "ber_proj_err_cnt", "structcapi__eyescan__option__t.html#ab94d7fb422c050eb7ba0d39e819cebb0", null ],
    [ "mode", "structcapi__eyescan__option__t.html#a2d51fb1f6636ed597464429e4de97714", null ]
];