var unionchip__top__chip__refclk__config__reg__t =
[
    [ "words", "unionchip__top__chip__refclk__config__reg__t.html#acbc446e61a548113cce2e33c812fc561", null ],
    [ "ref_clk_freq_select", "unionchip__top__chip__refclk__config__reg__t.html#a266f8f443e02d2253109d12ddc438679", null ],
    [ "reserved0", "unionchip__top__chip__refclk__config__reg__t.html#a9801298439843201ba7854e21200abd7", null ],
    [ "ref_clk_valid", "unionchip__top__chip__refclk__config__reg__t.html#a8b95f907354665237442f54bd985546d", null ],
    [ "fields", "unionchip__top__chip__refclk__config__reg__t.html#a856fc68e1fcf666267e6ee54a6a502f9", null ]
];