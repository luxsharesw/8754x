var test_8c =
[
    [ "cli_test_type_e", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bf", [
      [ "CHANGE_USB_ID", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfafc614e5d6d51bda6640f909e796c5add", null ],
      [ "CHANGE_PHY_ID", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfac091e717714254c42eb47c42e0050d7e", null ],
      [ "FIRMWARE_DOWNLOAD", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfac582ed70e1a830a2e0b32dbd13dc284e", null ],
      [ "FIRMWARE_DOWNLOAD_STATUS", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa40ebdb67d429f94f4b9b1dc108c51b26", null ],
      [ "FIRMWARE_SPI_PROGRAM", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa87ca55b62a50e33cfb017c640407dce6", null ],
      [ "CHANGE_CHIP_MODE", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfae533f41387c5dc86a17a61dcd627da62", null ],
      [ "CAPI_TEST_CASES", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa048d1e95c95aba0054dba3f1c7a641d6", null ],
      [ "FEC_ST_CASE", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfabbce85d65cb4393c3deb98bcde272d3e", null ],
      [ "GET_CHIP_INFO", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa4f173441d130572b14034981ded33498", null ],
      [ "GET_CHIP_STATUS", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa1722c4db1c882742c346f8a389165608", null ],
      [ "GET_VOLTAGE_TEMPERATURE", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa3ec07eb1ab0e599f99d0d5641d832e8a", null ],
      [ "CHIP_TEST_SOFT_RESET", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa65a5ec5c08725d38c1c55dba1855854b", null ],
      [ "CHIP_TEST_HARD_RESET", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfaf461f471d32ff35fff3b94c99b9322ed", null ],
      [ "AVS_CONFIG", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa79ec1905c1d85e8c300059620c49778f", null ],
      [ "AVS_STATUS", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa87599e14b58e150449fd671aa2d4fba1", null ],
      [ "INTERNAL_0p75v_TEST", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa856426c40b03421f7e24292de80e2ce0", null ],
      [ "INTERNAL_0p9v_TEST", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa33093d333b8162195b4671ca32bd9feb", null ],
      [ "CONFIG_BOOTUP_DEFAULT", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfacd77ba89e562d2e74fef3e11259a747a", null ],
      [ "CONFIG_RPTR_INDEPENDENT_LANE_CTRL", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfaf552dd07610114d15d25d04da2441f4b", null ],
      [ "CONFIG_LINE_PLL_DDCC_CTRL", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfa7b2b7d04abd9038e40cc2f137ee71e59", null ],
      [ "CLI_TEST_MAX", "test_8c.html#af1ec43f1af1f92cf392d897385bff7bfae41fd2762fe3ee9256390473e748332c", null ]
    ] ],
    [ "cli_change_usb_id", "test_8c.html#ac54045a2d3bf521e88efd70fa8706c23", null ],
    [ "cli_change_phy_id", "test_8c.html#a1d0019641212310b250f93f94c511238", null ],
    [ "cli", "test_8c.html#a125f60b3accaebf618e899f6245f0a17", null ],
    [ "test_description", "test_8c.html#a7beffe8fbc304d6802da2b597ced59fb", null ]
];