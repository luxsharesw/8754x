var capi__test_8c =
[
    [ "capi_set_prbs_info", "capi__test_8c.html#aa6a948afb55cfad3407610ffd5905c3f", null ],
    [ "capi_get_prbs_info", "capi__test_8c.html#a4e5636b8fc294a11ad15decb57b55703", null ],
    [ "capi_get_prbs_status", "capi__test_8c.html#a97f7eee463e9eb1adc9cd05cf2379f14", null ],
    [ "capi_clear_prbs_status", "capi__test_8c.html#a4911b8747982723ae73ba74f72d8dc48", null ],
    [ "capi_set_loopback", "capi__test_8c.html#afb64de01a9aad88abc37c515f79300b6", null ],
    [ "capi_get_loopback", "capi__test_8c.html#aec7f1b7bd74f3dde4b074deb9fd22eb6", null ],
    [ "capi_prbs_inject_error", "capi__test_8c.html#aec164a627d55aca1227d143cabc6f78b", null ],
    [ "capi_test_set_command", "capi__test_8c.html#a57d9a52260f4427be8f1957d86075ddf", null ]
];