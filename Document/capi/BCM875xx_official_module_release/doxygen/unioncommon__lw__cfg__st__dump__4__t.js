var unioncommon__lw__cfg__st__dump__4__t =
[
    [ "words", "unioncommon__lw__cfg__st__dump__4__t.html#a4dac0f06884e5cb650d0e07e31b167c8", null ],
    [ "enable_cu_pam_kp_sweep", "unioncommon__lw__cfg__st__dump__4__t.html#ad71a65b0df6e974a66c408e71f408835", null ],
    [ "dpam_pf_init_sweep_en", "unioncommon__lw__cfg__st__dump__4__t.html#af96ea0ee379ba56d4ffedd85bafd4582", null ],
    [ "eq_tap_sel", "unioncommon__lw__cfg__st__dump__4__t.html#aee01815d90f44a279aa30cab7e1beb1a", null ],
    [ "rx_symbol_swap", "unioncommon__lw__cfg__st__dump__4__t.html#a384a9c4b09585483020dafa3a96f890c", null ],
    [ "snr_threshold", "unioncommon__lw__cfg__st__dump__4__t.html#a01381ed2d1f3795823c61e01a2c0d4ce", null ],
    [ "dpam_cu_adcclip_en", "unioncommon__lw__cfg__st__dump__4__t.html#a3047b2ff3f1b9ff5d0b97d8aec79ff31", null ],
    [ "dpam_cu_adcclip_th", "unioncommon__lw__cfg__st__dump__4__t.html#abffbb8ee7badd271dba6bf84e7a91e09", null ],
    [ "eq_post", "unioncommon__lw__cfg__st__dump__4__t.html#a829ce08ea91fe2ba72e07bdb5075b7ad", null ],
    [ "exslicer", "unioncommon__lw__cfg__st__dump__4__t.html#a1c2ab9f4409858450d37bb467fa55ab4", null ],
    [ "fields", "unioncommon__lw__cfg__st__dump__4__t.html#a11eb8cfbfc0b588551be4d48fd4c6841", null ]
];