var structcw__fec__rx__status__t =
[
    [ "host_fifo_flt", "structcw__fec__rx__status__t.html#a4379e72eaed5d05cc9876a97e79c29d5", null ],
    [ "host_pcs_gbox_flt", "structcw__fec__rx__status__t.html#a8ec629ba0ba079455f45b2cc4dd5a7c6", null ],
    [ "fec_sync_algn_unlock", "structcw__fec__rx__status__t.html#a239b83e0a51259ce9371a2818e865670", null ],
    [ "pcs_sync_flt", "structcw__fec__rx__status__t.html#a4ea429f63c7efac51c4651625ad151c2", null ],
    [ "xdec_gbox_clsn_flt", "structcw__fec__rx__status__t.html#a196b8a249aed7bcff87a14ceebd34fd5", null ],
    [ "xdec_err_flt", "structcw__fec__rx__status__t.html#a19c495b38c14855f8a0d5951328c6d3d", null ],
    [ "xenc_gbox_clsn_flt", "structcw__fec__rx__status__t.html#a4b4c0b7df66c304bbf3a9339ca90e770", null ],
    [ "xenc_err_flt", "structcw__fec__rx__status__t.html#a13935e089c476676e6709031fc05c819", null ],
    [ "krkp_gbox_clsn", "structcw__fec__rx__status__t.html#a820ffd8e6daa4e1b83a4b5f7c6bdd3fd", null ],
    [ "line_pfifo_flt", "structcw__fec__rx__status__t.html#a47c446d1d0ab6b631b3adb11b8e60ab1", null ]
];