var common__def_8h =
[
    [ "phy_info_t", "structphy__info__t.html", "structphy__info__t" ],
    [ "SQUELCH", "common__def_8h.html#a9771f5beac41acfbe2696a4248ddf27a", null ],
    [ "UN_SQUELCH", "common__def_8h.html#a62239e0cdcad35c0cf03c7b957123285", null ],
    [ "TURN_OFF", "common__def_8h.html#a20b9dd82db1ddc2ec72f24580fc5254f", null ],
    [ "TURN_ON", "common__def_8h.html#a96701203b8a7febc3b53a7eede64433d", null ],
    [ "RESET_RELEASE", "common__def_8h.html#aa7e3206af66d7c67702a3adff61f46aa", null ],
    [ "RESET", "common__def_8h.html#ab702106cf3b3e96750b6845ded4e0299", null ],
    [ "NIBBLE_SHIFT", "common__def_8h.html#a4f1c0edd0a22eee2033fa9ebc43696cf", null ],
    [ "UPPER_NIBBLE_MASK", "common__def_8h.html#a619584469be1d008c2817bbd0ea7b574", null ],
    [ "LOWER_NIBBLE_MASK", "common__def_8h.html#a2d6a97f48a6b991c47ac9038ca9f3e09", null ],
    [ "core_ip_t", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6", [
      [ "CORE_IP_ALL", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6a160c76a678ccda3ede5da0f181bd726e", null ],
      [ "CORE_IP_CW", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6a4a06c34bbb6e7b6400dc7267d4a782a6", null ],
      [ "CORE_IP_MEDIA_DSP", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6ae329bdb728fbdfd85e412bf33a24cc79", null ],
      [ "CORE_IP_MEDIA_SERDES", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6ae4622a3d874ceb6b11199e3461085ef2", null ],
      [ "CORE_IP_HOST_DSP", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6a7db07bf690c2cfb466abdd45f49c4675", null ],
      [ "CORE_IP_HOST_SERDES", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6ae7301ae4e70ed793e87944120d572a40", null ],
      [ "CORE_IP_KP4_KR4_FEC_DEC", "common__def_8h.html#a65bb192d5b47530e6f597b6fb9853db6a32e053002ce46e0cb9a1f4cb2721b543", null ]
    ] ],
    [ "enabled_t", "common__def_8h.html#a623dab3e982680545ca7462e4c2c0ea0", [
      [ "IS_DISABLED", "common__def_8h.html#a623dab3e982680545ca7462e4c2c0ea0a726398df1f3689d17a970546f7de1b50", null ],
      [ "IS_ENABLED", "common__def_8h.html#a623dab3e982680545ca7462e4c2c0ea0a769bdfa06190532b58d5e7d393722e2d", null ]
    ] ],
    [ "rst_assert_t", "common__def_8h.html#ae97f932b2d3cb1d84dda24d8731307c8", [
      [ "RST_DEASSERTED", "common__def_8h.html#ae97f932b2d3cb1d84dda24d8731307c8a72f0fd54135261254fc226bb4b0d2963", null ],
      [ "RST_ASSERTED", "common__def_8h.html#ae97f932b2d3cb1d84dda24d8731307c8a5d1fc129a232a5c99ec5bc54d0221e9d", null ]
    ] ],
    [ "nibble_t", "common__def_8h.html#a6cc6ed49af48abe106057b157025c693", [
      [ "LOWER_NIBBLE", "common__def_8h.html#a6cc6ed49af48abe106057b157025c693a9d241f2b20851d6fcb0c30e8e180c00e", null ],
      [ "UPPER_NIBBLE", "common__def_8h.html#a6cc6ed49af48abe106057b157025c693a67e17b78321a8b494b96fccf8e3041a1", null ]
    ] ]
];