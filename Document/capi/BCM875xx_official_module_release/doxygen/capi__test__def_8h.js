var capi__test__def_8h =
[
    [ "capi_ngen_err_inject_t", "structcapi__ngen__err__inject__t.html", "structcapi__ngen__err__inject__t" ],
    [ "capi_ngen_err_cnt_t", "structcapi__ngen__err__cnt__t.html", "structcapi__ngen__err__cnt__t" ],
    [ "capi_rclk_info_t", "structcapi__rclk__info__t.html", "structcapi__rclk__info__t" ],
    [ "capi_test_command_info_t", "structcapi__test__command__info__t.html", "structcapi__test__command__info__t" ],
    [ "capi_rclk_div_ratio_t", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862", [
      [ "CAPI_RCLK_DIV_RATIO_NONE", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862aa023de6f1a8966638d3cb3c444bb5f9c", null ],
      [ "CAPI_RCLK_DIV_RATIO_1", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862a4ed39f1f3d8a19ce8c003105102933b1", null ],
      [ "CAPI_RCLK_DIV_RATIO_2", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862af420c0406fc4b9ea1e500f497b4cba97", null ],
      [ "CAPI_RCLK_DIV_RATIO_4", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862a2741447145ff68ef28869acf41a0a7ce", null ],
      [ "CAPI_RCLK_DIV_RATIO_8", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862a33f1c3097ebf056a8c39ae440cc4bc27", null ],
      [ "CAPI_RCLK_DIV_RATIO_16", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862a72efede0c4c0ad06178d4c6393a644ba", null ],
      [ "CAPI_RCLK_DIV_RATIO_32", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862a00631e26d6b04317b808138714abc292", null ],
      [ "CAPI_RCLK_DIV_RATIO_64", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862af69d9cd47c4532c9d2a7b0c9a09704b1", null ],
      [ "CAPI_RCLK_DIV_RATIO_128", "capi__test__def_8h.html#a999bc1e4afc9010a80596d5da8aa5862a96815414ca50f278b6ccc20f142f2c0c", null ]
    ] ],
    [ "capi_rclk_t", "capi__test__def_8h.html#a541e4e7d96f2e2f6481f04985345691a", [
      [ "CAPI_RCLK_32T", "capi__test__def_8h.html#a541e4e7d96f2e2f6481f04985345691aa1546e227671a149fd1d9a9e060ab3bb2", null ],
      [ "CAPI_RCLK_80T", "capi__test__def_8h.html#a541e4e7d96f2e2f6481f04985345691aa50c99633bcd63665cddbe5175a720507", null ]
    ] ],
    [ "kp4_fec_symbol_num_type_e", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9a", [
      [ "CAPI_KP4_FEC_SYMBOL_NUM_0", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aa589468a1e434a11490d56d4d4c264d6b", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_1", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aabf6ee0bcf5b991618a916f1d16ef4a33", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_2", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aa58c61287bfa898675b6db7814b2748e2", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_3", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aace3ad7bb410def67d8f2ba1a455cbadf", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_4", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aa430060e454f4ae6a619c34576109e3a9", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_5", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aa7cb53a9f8f5d8445ddd556d00ddc108c", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_6", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aadf72c01b8b0a45099d80c5c6aa421ee9", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_7", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aa089e8cb343a48722478af67a18ce7c98", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_8", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aaf3eb8bbb6953c5945987da52383d094d", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_9", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aad88ccd1de1f5143ea012f315a1370d32", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_A", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aafcc440e6e7a9f78f359b651ab8ac60f9", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_B", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aa666ba8da78972a1cc01a1bfa4ad419da", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_C", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aae05f8c6a2bd0f263b5d99570e729a8ed", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_D", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aae208e4449bbbc46b40917e935746cb9a", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_E", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aadfe24fcc7383461be4c1c8dff53fd06d", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_F", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aaa3d1d1d2b0cc8d07c63c29ca5e0fbdd7", null ],
      [ "CAPI_KP4_FEC_SYMBOL_NUM_10", "capi__test__def_8h.html#a31ffe85d12b7b20d62b44999d2dceb9aae221a1ba2d4eaf605f44b3a7e265f5d1", null ]
    ] ]
];