var unionoctal__fw__internal__cfg__1__t =
[
    [ "words", "unionoctal__fw__internal__cfg__1__t.html#a087ba94e2e59e234c072d995ba4fcfd5", null ],
    [ "super_cmode_cfg_en", "unionoctal__fw__internal__cfg__1__t.html#a1de37bd7e8505ae43489e791fccf3f3d", null ],
    [ "super_cmode_ignore_pllclsn_en", "unionoctal__fw__internal__cfg__1__t.html#aa93f76dc7c93d8039978feb0c8a6bbc3", null ],
    [ "rsvd", "unionoctal__fw__internal__cfg__1__t.html#aad0f6aa53acf9c714761098c60b721b2", null ],
    [ "qsfp_force_en", "unionoctal__fw__internal__cfg__1__t.html#ab51c8eb2abf789eefbe74f29010f9b70", null ],
    [ "qsfp_force_val", "unionoctal__fw__internal__cfg__1__t.html#a2784800df0bd19073031235e045cc355", null ],
    [ "rsvdd", "unionoctal__fw__internal__cfg__1__t.html#a2b616bb564ea2fcfb989ad5665b8639e", null ],
    [ "disable_cw_clk66_rst", "unionoctal__fw__internal__cfg__1__t.html#aef0f8abb2d3f7d3dbd6ceef52c337698", null ],
    [ "ignore_lw_pll_lol", "unionoctal__fw__internal__cfg__1__t.html#ae728a435efd741709c0c5b2ce33f5f45", null ],
    [ "fields", "unionoctal__fw__internal__cfg__1__t.html#a4cce7105a81c96156bfc2ad6317c83bc", null ]
];