var structcapi__cmis__info__t =
[
    [ "slicer_threshold", "structcapi__cmis__info__t.html#a2b32db98439da2fcd5e52e1f4f59a2f9", null ],
    [ "slicer_target", "structcapi__cmis__info__t.html#a3a9ce9c897a1757fef25fbd73a0fdb7d", null ],
    [ "slicer_mean_value", "structcapi__cmis__info__t.html#ada2360cca53bf0410b0db4edbc66ab9f", null ],
    [ "slicer_sigma_sqr_value", "structcapi__cmis__info__t.html#a5ae1dd6ad7d783693397f5bda9041e8a", null ],
    [ "slicer_p_value", "structcapi__cmis__info__t.html#a1b76832c9ae476e8d60b4baff39227f1", null ],
    [ "slicer_p_location", "structcapi__cmis__info__t.html#a7244c464dde2065790549e1a12b2f941", null ],
    [ "slicer_v_value", "structcapi__cmis__info__t.html#a2d3182708d60ab306c38270e07157110", null ],
    [ "slicer_v_location", "structcapi__cmis__info__t.html#aaf51bba7406fd1bc8c2595f1d825d6bb", null ]
];