var ipc__regs_8h =
[
    [ "intf_cwtolw_t", "unionintf__cwtolw__t.html", "unionintf__cwtolw__t" ],
    [ "intf_lwtocw_t", "unionintf__lwtocw__t.html", "unionintf__lwtocw__t" ],
    [ "intf_cwtobh_t", "unionintf__cwtobh__t.html", "unionintf__cwtobh__t" ],
    [ "intf_bhtocw_t", "unionintf__bhtocw__t.html", "unionintf__bhtocw__t" ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_CMDSTAT_CHG_MASK", "ipc__regs_8h.html#a8c05cbf14e54b1d4b86df124fe5340f0", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_CMDSTAT_CHG_SHIFT", "ipc__regs_8h.html#a1043f05b15d23317777db36f2bf5b527", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_RSVD1_MASK", "ipc__regs_8h.html#a5a0d8f2238b570f20ea68617cb4c5779", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_RSVD1_SHIFT", "ipc__regs_8h.html#a476f686c84fc21b1348613c18d0f8467", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_ANLINKUP_MASK", "ipc__regs_8h.html#aa10afb64cbef9b95a0910402ea7d2ffa", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_ANLINKUP_SHIFT", "ipc__regs_8h.html#ae90fe779d340ad681115522006d727df", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_EGR_MISSION_STATE_MASK", "ipc__regs_8h.html#aff1deaa5baa69ad9d749aaa77f92066c", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_EGR_MISSION_STATE_SHIFT", "ipc__regs_8h.html#a62a3dfcd8f04d754d3d5668922f24435", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_OPPOSITESIDECDRLOCK_MASK", "ipc__regs_8h.html#a35d2d3b85f639e9057008de4a41aeebf", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_OPPOSITESIDECDRLOCK_SHIFT", "ipc__regs_8h.html#add4cf645ef29695e3637cf418ca89cba", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_RSVD2_MASK", "ipc__regs_8h.html#a06f550e8b5d2605e66fc51ed01053fc0", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_RSVD2_SHIFT", "ipc__regs_8h.html#a98504f8faf5b8f3f7b9b9f0e7712f060", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_TXPI_RESTART_MASK", "ipc__regs_8h.html#a81118c6282fdf163e74492a11f8d9b07", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_TXPI_RESTART_SHIFT", "ipc__regs_8h.html#a1f2309cd87d9f5dbde887391635f4d84", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_TURNONEGRCLK_MASK", "ipc__regs_8h.html#a57346019ea9d825cea0c0f270c0e904a", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_TURNONEGRCLK_SHIFT", "ipc__regs_8h.html#a1c565817287cbd09dcd283904bebe8ed", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_TURNONIGRCLK_MASK", "ipc__regs_8h.html#acd802c59098c910b0ad98288dee96460", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_TURNONIGRCLK_SHIFT", "ipc__regs_8h.html#a06562a3f45205771c983ff7a7b56de72", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_SQUELCHTX_MASK", "ipc__regs_8h.html#a4af5cb8463ec85a264ed0d541ee98897", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_SQUELCHTX_SHIFT", "ipc__regs_8h.html#a72c743ceac06583df857bf5f9a4466e7", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_FORCECDRRESTART_MASK", "ipc__regs_8h.html#a1eb36d3a0c1972bc30513e88c233c57a", null ],
    [ "INTF_CWTOLW_COMMANDSTATUS_LANE0_FORCECDRRESTART_SHIFT", "ipc__regs_8h.html#a67e2a99405d5c8938c750501fb9e2c4b", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_LANE_CFG_STATUS_MASK", "ipc__regs_8h.html#a9e993d468e5da85bc7401a067a4c7e13", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_LANE_CFG_STATUS_SHIFT", "ipc__regs_8h.html#a44ee9726d07969dcd4474f71cfc7465a", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_GLOBALLOOPBACKENABLE_MASK", "ipc__regs_8h.html#a32594483fd42222b8bb9b8aafe23af50", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_GLOBALLOOPBACKENABLE_SHIFT", "ipc__regs_8h.html#af7c68476cebb7155317ba5a8ef497833", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_CDR_LOCKED_MASK", "ipc__regs_8h.html#a7632c93d4d5fe4a5a48ff5f335797f0a", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_CDR_LOCKED_SHIFT", "ipc__regs_8h.html#a05189c2bdacdc967020febe4fc85cf5b", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_TXPI_DONE_MASK", "ipc__regs_8h.html#ae54a8bb9ac788adfb172db871608e6cf", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_TXPI_DONE_SHIFT", "ipc__regs_8h.html#a3a4b5ad8877139f75b0cdf28d7531690", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_TXPI_CONV_FAILED_MASK", "ipc__regs_8h.html#ad7c077dbf1c74403e5f93dec192099ee", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_TXPI_CONV_FAILED_SHIFT", "ipc__regs_8h.html#a16acdf727ad052faeb7d386d4d33f9f6", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_CDRPPM_MASK", "ipc__regs_8h.html#a4b0aaf6e680a02ff163ae3dc311422c0", null ],
    [ "INTF_LWTOCW_COMMANDSTATUS_LANE0_CDRPPM_SHIFT", "ipc__regs_8h.html#a1f31702300ab01b1795d46d083bb1ee7", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_CMDSTAT_CHG_MASK", "ipc__regs_8h.html#a72888b94ea4cc7ff71436a27e47c76a5", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_CMDSTAT_CHG_SHIFT", "ipc__regs_8h.html#a31c143b5710fb3d0618d3a03be6cf838", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_RSVD1_MASK", "ipc__regs_8h.html#a468921cdd2802797b40947b51c435158", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_RSVD1_SHIFT", "ipc__regs_8h.html#a12d1fa1962bc42c9b95e61e9bfb1da75", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_ANLINKUP_MASK", "ipc__regs_8h.html#a2b76d86826d2026d7303af4caf5c49d9", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_ANLINKUP_SHIFT", "ipc__regs_8h.html#a7ef6e591e290e23c242a1c4eab9573a9", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_EGR_MISSION_STATE_MASK", "ipc__regs_8h.html#a4c71bfeba2f9767f09f10ce58ad95e78", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_EGR_MISSION_STATE_SHIFT", "ipc__regs_8h.html#afcad57a7e85deef3d8451be75c5815ee", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_OPPOSITESIDECDRLOCK_MASK", "ipc__regs_8h.html#a940560c5b3d65be6f8ccebd58f2625d1", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_OPPOSITESIDECDRLOCK_SHIFT", "ipc__regs_8h.html#afe4339e99ab044885a0c6ecf8e43f83c", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_RSVD2_MASK", "ipc__regs_8h.html#a360b0c86a1d42b8ad843dd51ab26125c", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_RSVD2_SHIFT", "ipc__regs_8h.html#ab0a23a13a221cee9b5ced78bdbcc05b4", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_TXPI_RESTART_MASK", "ipc__regs_8h.html#ae879fa89817ea35897654789c736c944", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_TXPI_RESTART_SHIFT", "ipc__regs_8h.html#a2d8230372e66a47504b71e97430b81eb", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_TURNONEGRCLK_MASK", "ipc__regs_8h.html#ae14b4bf4b732b816d053a4199dc7c881", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_TURNONEGRCLK_SHIFT", "ipc__regs_8h.html#a0beb15df5122a7e939c6805dfc33413a", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_TURNONIGRCLK_MASK", "ipc__regs_8h.html#ad9fa71e5ba40714c915beb872836a1a0", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_TURNONIGRCLK_SHIFT", "ipc__regs_8h.html#a5e019af2131e325f4d7906b69820eb3f", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_SQUELCHTX_MASK", "ipc__regs_8h.html#a83a82b5682842ff5bdb29a8db6108474", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_SQUELCHTX_SHIFT", "ipc__regs_8h.html#a7855fed564d37149c2157f664254cdd7", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_FORCECDRRESTART_MASK", "ipc__regs_8h.html#af76c4b717dd9704d24867bdbea16d323", null ],
    [ "INTF_CWTOBH_COMMANDSTATUS_LANE0_FORCECDRRESTART_SHIFT", "ipc__regs_8h.html#abf1cc0473f5ce3156766408c9ac75c8a", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_LANE_CFG_STATUS_MASK", "ipc__regs_8h.html#ae4c92a230df083ea8ded878da3b10dc0", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_LANE_CFG_STATUS_SHIFT", "ipc__regs_8h.html#a050af3267cbaaec05b6dbfd78badab68", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_GLOBALLOOPBACKENABLE_MASK", "ipc__regs_8h.html#ab177bb0145fc00a3fa11cf6cc2051463", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_GLOBALLOOPBACKENABLE_SHIFT", "ipc__regs_8h.html#a2d1197442806c1849c2952b588ee5863", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_CDR_LOCKED_MASK", "ipc__regs_8h.html#a4521a7aca7b13731909e7152218255e6", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_CDR_LOCKED_SHIFT", "ipc__regs_8h.html#a80275cf88a9cb27b095e3f63cba52593", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_TXPI_DONE_MASK", "ipc__regs_8h.html#a269d6a323cdba9e69cceb66cc32971b2", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_TXPI_DONE_SHIFT", "ipc__regs_8h.html#a5fbc295d6cd60b0a7da8521f53cb3aae", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_TXPI_CONV_FAILED_MASK", "ipc__regs_8h.html#a8e7f9b7b36015764d36b4344b804d4a8", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_TXPI_CONV_FAILED_SHIFT", "ipc__regs_8h.html#a5d778f6f18b0664f955f361805bd76ab", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_CDRPPM_MASK", "ipc__regs_8h.html#afc53a7077d8ca1a1a21182e5c2a99c55", null ],
    [ "INTF_BHTOCW_COMMANDSTATUS_LANE0_CDRPPM_SHIFT", "ipc__regs_8h.html#ac1be575149074154bdc735acdc1a4216", null ]
];