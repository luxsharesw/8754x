var reg__access_8c =
[
    [ "rd_reg_ex", "reg__access_8c.html#acddeffe1684e90352ecd1a5de015e2f8", null ],
    [ "wr_reg_ex", "reg__access_8c.html#a8a158aef9a91ebc3991269b487718d1e", null ],
    [ "wr16_reg_ex", "reg__access_8c.html#a06f67947cb4571d3320ac4c299837d3b", null ],
    [ "rd16_reg_ex", "reg__access_8c.html#ad99347983cbed75f4bc9610370f9e055", null ],
    [ "rd8_reg_ex", "reg__access_8c.html#a26b65ca23df4aa39ea41c93df35ac558", null ],
    [ "wr8_reg_ex", "reg__access_8c.html#ab162563aef009958d2aa2151976d8bcc", null ],
    [ "rd_qsfp_reg_ex", "reg__access_8c.html#a8e2bb5876a4e718783c1ca8903746734", null ],
    [ "set_i2c_driver_target_address", "reg__access_8c.html#a57d6eb92f319bd89b68ccd459c91cd93", null ],
    [ "wr_qsfp_reg_ex", "reg__access_8c.html#aa9d02844ef753a9687792df62b2539c9", null ],
    [ "display_settings_view", "reg__access_8c.html#ac29b97084fabca758f2f499e0411e971", null ],
    [ "set_usb_com", "reg__access_8c.html#a9b3ebffd01adec5a253ff13151dc0862", null ],
    [ "addrmdio45", "reg__access_8c.html#ae02110e2a242e44dd4c5139841c02ad3", null ],
    [ "writemdio45", "reg__access_8c.html#a74e83177a17e06b1725201bc4ad3dd5f", null ],
    [ "readmdio45", "reg__access_8c.html#a624acc572e752d85fa508396c22e86c7", null ],
    [ "burst_write_mdio", "reg__access_8c.html#a3b577ae80af2ee2f7627e3ae69458255", null ],
    [ "burst_write_i2c", "reg__access_8c.html#a567085f2485dfdd20c540862ab58d6ef", null ],
    [ "burst_read_chip_sram", "reg__access_8c.html#af009a3ac6fb18e61256f7081bb9b42ff", null ],
    [ "burst_write_chip_sram", "reg__access_8c.html#a66f5cef51ad944d4cd110d8cabca314c", null ],
    [ "phy_id", "reg__access_8c.html#a431496d2d329aa7e2e70275ad75646cb", null ],
    [ "passed_port", "reg__access_8c.html#a420c4d902b61dc25616dfd22f41417e4", null ]
];