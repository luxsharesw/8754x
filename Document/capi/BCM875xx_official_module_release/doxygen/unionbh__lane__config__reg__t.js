var unionbh__lane__config__reg__t =
[
    [ "content", "unionbh__lane__config__reg__t.html#a20f6b8b9e2f7cba81e17737dc770b946", null ],
    [ "db_lost", "unionbh__lane__config__reg__t.html#a1b7525d594b8af6a6a162b1d26758791", null ],
    [ "lane_mode", "unionbh__lane__config__reg__t.html#a1a02df11500a2fb797ff40d8b4f31076", null ],
    [ "medium", "unionbh__lane__config__reg__t.html#ac6b9113bfe6aa27edeecdc09960d730c", null ],
    [ "ns_es", "unionbh__lane__config__reg__t.html#a053c1f7eba46af312f9ce8c72e24931f", null ],
    [ "precoder", "unionbh__lane__config__reg__t.html#aad7eba3d93a2a07daa2a218e55256f54", null ],
    [ "dfe", "unionbh__lane__config__reg__t.html#a34151df30354646415c57daddb5102b4", null ],
    [ "link_training", "unionbh__lane__config__reg__t.html#ae09dd77f31a803a42a047e1b0d65864e", null ],
    [ "tx", "unionbh__lane__config__reg__t.html#a564530abf1e24f10bad7c8450386a9b6", null ],
    [ "rx", "unionbh__lane__config__reg__t.html#a3d10c92d683bf6bab2b19cace492db7f", null ],
    [ "is", "unionbh__lane__config__reg__t.html#a9595f52b431cea203892fde7a16f093e", null ]
];