var structcapi__lane__info__t =
[
    [ "content", "structcapi__lane__info__t.html#ae5ad614fe2ecbe7cddb79f51f7c367cc", null ],
    [ "sticky_loss_link", "structcapi__lane__info__t.html#a4db3d255d8f0a0e31717e6b223f13c8d", null ],
    [ "sticky_loss_optical", "structcapi__lane__info__t.html#a3dbc673bb96d431fe5db25baa08acbb3", null ],
    [ "sticky_loss_electrical", "structcapi__lane__info__t.html#a2a29531ec0d7b18d3b0332311b7aa835", null ],
    [ "cw_status", "structcapi__lane__info__t.html#a4f76078f583bd4ab89abc7597b0700c4", null ],
    [ "diag_status", "structcapi__lane__info__t.html#ad008da5381e324c3bd42f4049bae7de6", null ],
    [ "reserved2", "structcapi__lane__info__t.html#a9d490cd884b751797c6003b900de2363", null ],
    [ "lnktrn_status", "structcapi__lane__info__t.html#aeddfd8d83600f0c331e4aa7601679de7", null ],
    [ "lane_status", "structcapi__lane__info__t.html#a1a411daf4351435865c8d6906b81a007", null ],
    [ "reserved", "structcapi__lane__info__t.html#a77a17a552b0b377691325b0ff282370f", null ],
    [ "is", "structcapi__lane__info__t.html#aec449f06971165fa6dac69717887f40b", null ],
    [ "param", "structcapi__lane__info__t.html#ac0fa663b896c8e075dcdb30222fe7335", null ],
    [ "lane_status", "structcapi__lane__info__t.html#ae19313d04fd22b1b0ed9ed1262b508cd", null ],
    [ "lnktrn_status", "structcapi__lane__info__t.html#a91ac57f228cd1e6c71d43cf4d523042b", null ],
    [ "diag_status", "structcapi__lane__info__t.html#a0f59315820f33d6c912f6e6d0e8b8b1a", null ],
    [ "cw_status", "structcapi__lane__info__t.html#a9818be0689cf2b4ffc6875e2c791f36a", null ],
    [ "sticky_loss", "structcapi__lane__info__t.html#aa031989b93d8ea1368d0304201eeff83", null ],
    [ "value", "structcapi__lane__info__t.html#a4f9299bbf773e11f87de662948623747", null ]
];