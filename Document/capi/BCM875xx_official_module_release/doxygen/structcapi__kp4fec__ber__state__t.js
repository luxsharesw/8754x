var structcapi__kp4fec__ber__state__t =
[
    [ "fec_mode", "structcapi__kp4fec__ber__state__t.html#a837db05ae3a8408e04a56647bd28e5dc", null ],
    [ "clr_cnt", "structcapi__kp4fec__ber__state__t.html#a7b69f8a89b5ca53047c3194c877d9218", null ],
    [ "only_cur", "structcapi__kp4fec__ber__state__t.html#ad71ea72118a8a8dc9c21e63a63ae607b", null ],
    [ "err_latch_times", "structcapi__kp4fec__ber__state__t.html#ae41103d5036facf16603e1c78df5c9ef", null ],
    [ "pre_fec_ber", "structcapi__kp4fec__ber__state__t.html#a3085f040ab245be50c9cfea20f8f6b53", null ],
    [ "post_fec_ber", "structcapi__kp4fec__ber__state__t.html#ac143ce9a9a67e01eb12ac3e3a907b511", null ],
    [ "cur_err", "structcapi__kp4fec__ber__state__t.html#a094acda9185d2c7ac9b64ef173e73352", null ],
    [ "min_err", "structcapi__kp4fec__ber__state__t.html#a9c41777396838935fc0f89de750201ad", null ],
    [ "max_err", "structcapi__kp4fec__ber__state__t.html#a0479881c49651d2ccd08b653a338c162", null ],
    [ "average_err", "structcapi__kp4fec__ber__state__t.html#a270dbd0ffae7c10a3267840b60df5127", null ],
    [ "backup_err", "structcapi__kp4fec__ber__state__t.html#a02240c7742a57175e12eb37b63e9219a", null ],
    [ "total_err", "structcapi__kp4fec__ber__state__t.html#a39cb670e0ba33a6d9481e84abaf4be90", null ]
];