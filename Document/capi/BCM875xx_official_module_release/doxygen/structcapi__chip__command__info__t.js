var structcapi__chip__command__info__t =
[
    [ "command_id", "structcapi__chip__command__info__t.html#aaee865d9c9949709933dbb73da523294", null ],
    [ "lpm_info", "structcapi__chip__command__info__t.html#a69861a2c7077f989bcf8fbca21144d9d", null ],
    [ "dsp_power_info", "structcapi__chip__command__info__t.html#a5018c82e4bda7bf9122475347af11f40", null ],
    [ "indep_ctrl", "structcapi__chip__command__info__t.html#a63c7b2a90f19d0ccd14ef39e68e1eb5c", null ],
    [ "line_pll_ctrl", "structcapi__chip__command__info__t.html#af166d66bc57725093178ae6219cfc924", null ],
    [ "optrxlos_host_fast_tx_squelch", "structcapi__chip__command__info__t.html#a4bbef937117045c6bab7876d120a47dc", null ],
    [ "serdes_lane_cdr_mode", "structcapi__chip__command__info__t.html#a8e482f9ab5f6a8e70cde2ed4ef4382a5", null ],
    [ "serdes_lane_tuning_control", "structcapi__chip__command__info__t.html#ab615ab01803db90ca2f6f79e8e1590e5", null ],
    [ "type", "structcapi__chip__command__info__t.html#afdb3e641303038ba2e7f7f0d0adf3d51", null ]
];