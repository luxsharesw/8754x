var top__test_8c =
[
    [ "MAX_SIZE_SRAM", "top__test_8c.html#a6c3494cc3d14cc00c150bb484727169a", null ],
    [ "MAX_SIZE_SPI", "top__test_8c.html#a67b426b974ff197185d7f244a640718b", null ],
    [ "get_image_hex", "top__test_8c.html#a1e9dda79b4da7c6b8fbb080e210b710a", null ],
    [ "get_image_bin", "top__test_8c.html#a721d17d3365c49b8c88a6b4c26fc4a80", null ],
    [ "test_download_hexfile", "top__test_8c.html#aa80b526290f96923c97dafe4aa211036", null ],
    [ "test_download_binfile", "top__test_8c.html#abf1c3694f71ea12b39239bf08a70c1e8", null ],
    [ "test_sram_download", "top__test_8c.html#a222f34308ba0d2d37b472afaf7f7f630", null ],
    [ "test_spi_download", "top__test_8c.html#a966501e92755620fd1b4eb0efa7d801c", null ],
    [ "test_download_status", "top__test_8c.html#af74fe76ae018c918f79a66ee49dbcea1", null ],
    [ "test_soft_reset", "top__test_8c.html#afc26ceec51fa40b2fe64f023b80a031b", null ],
    [ "chip_avs_config", "top__test_8c.html#a4cf2054100f80dd83a3265ad6e0df1b7", null ],
    [ "chip_avs_get_status", "top__test_8c.html#afb98d9ef8cd7da5ac0da62f4218ac447", null ],
    [ "chip_internal_vddm_test", "top__test_8c.html#a3c2317dd628f6d8e8ca3b6a17038cff1", null ],
    [ "chip_internal_avdd_test", "top__test_8c.html#aa4b1e1cc180b16696508d7dc2a1c2348", null ],
    [ "test_config_bootup_default", "top__test_8c.html#a34b86d7449dd6d9879fae69bded828fb", null ],
    [ "test_cfg_rptr_indep_lane_ctrl", "top__test_8c.html#a79f4f6f0f116cf150dc80700e19d0091", null ],
    [ "test_cfg_line_pll_ddcc_ctrl", "top__test_8c.html#a73f4bf519abfa47a29e61da24a050785", null ],
    [ "wholeimage_sram_hex", "top__test_8c.html#a5217971f9b78fed831498927ee4d3545", null ],
    [ "wholeimage_spi_hex", "top__test_8c.html#af3bd44215e39639cf8b263ff04a4010d", null ],
    [ "images_path", "top__test_8c.html#a0af789350eb2c0048c19353c43d0eab7", null ]
];