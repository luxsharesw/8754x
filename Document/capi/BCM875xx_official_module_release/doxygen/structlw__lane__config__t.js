var structlw__lane__config__t =
[
    [ "lt_disable_timer", "structlw__lane__config__t.html#aa72d57254371ccb443f6594a913c0240", null ],
    [ "lt_1st_ppm_step_converge", "structlw__lane__config__t.html#ab3f31aa892ff41ba0c71bf1668ba4636", null ],
    [ "tx_ppm_converge_en", "structlw__lane__config__t.html#af0f523431eca6850e5538078f3a693f1", null ],
    [ "disable_lt_bh_txfir_lmt", "structlw__lane__config__t.html#a88e75385ec7f9a6a673690ff26b53806", null ],
    [ "disable_lt_rxflt_restart_tx", "structlw__lane__config__t.html#ab36ef837d2da449dcaf4488eceea604c", null ],
    [ "disable_lt_rxlos_restart_tx", "structlw__lane__config__t.html#aa80ec69585548625bc9d9312e566e553", null ]
];