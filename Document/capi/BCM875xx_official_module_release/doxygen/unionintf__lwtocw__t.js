var unionintf__lwtocw__t =
[
    [ "words", "unionintf__lwtocw__t.html#af3d4f5d3e0a603dc0e963ed8a3286651", null ],
    [ "cdr_ppm", "unionintf__lwtocw__t.html#a33f698602d5a4ad0a2e0cdd790af6af9", null ],
    [ "txpi_conv_fail", "unionintf__lwtocw__t.html#aa1f1d3df00bdda683b99d4562bd71f60", null ],
    [ "txpi_done", "unionintf__lwtocw__t.html#a530815c7d7902e1e2495137cce4a15e6", null ],
    [ "cdr_lock", "unionintf__lwtocw__t.html#a93b8fdc148ca2d11ab3d23b1bc0e65e1", null ],
    [ "gloopback_en", "unionintf__lwtocw__t.html#a42ecfeb1912239d39dfe21c91af7bf71", null ],
    [ "lane_cfg_stat", "unionintf__lwtocw__t.html#a6c3196813e440af3858f1a9d3f321a54", null ],
    [ "fields", "unionintf__lwtocw__t.html#aac16eb6e2d0790c803961deefda1fb29", null ]
];