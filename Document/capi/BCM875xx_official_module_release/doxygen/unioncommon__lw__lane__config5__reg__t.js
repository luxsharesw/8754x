var unioncommon__lw__lane__config5__reg__t =
[
    [ "words", "unioncommon__lw__lane__config5__reg__t.html#a31d93f005c1fd3c96fa9240db6424f44", null ],
    [ "lw_invert_tx", "unioncommon__lw__lane__config5__reg__t.html#a00372bc01565e6c09691d29edad7b04e", null ],
    [ "lw_tx_symbol_swap", "unioncommon__lw__lane__config5__reg__t.html#ac3ed3de061331d002ad6568baa07aa67", null ],
    [ "lw_tx_datapath_sel", "unioncommon__lw__lane__config5__reg__t.html#a4ac301ba2883e231015494fd23e9c95e", null ],
    [ "lw_prbs_sel", "unioncommon__lw__lane__config5__reg__t.html#a6d69629633cc225a18449516bd3dc196", null ],
    [ "lw_tx_graycode", "unioncommon__lw__lane__config5__reg__t.html#aee969738f81b71214af3d31cf22852b6", null ],
    [ "lw_tx_prbs_or_squelch", "unioncommon__lw__lane__config5__reg__t.html#aa2e58fce6ed1d131d57f96b5bf03004f", null ],
    [ "lw_tx_precode_indep", "unioncommon__lw__lane__config5__reg__t.html#afe1e022ddf5b9c1061ab94c77d314ecc", null ],
    [ "lw_tx_restart_txpi", "unioncommon__lw__lane__config5__reg__t.html#a3b90204644fc08b7e7704134ae1f1044", null ],
    [ "lw_optical_user_override", "unioncommon__lw__lane__config5__reg__t.html#ac97914063c96953298aa44494c702f35", null ],
    [ "lw_tx_user_override", "unioncommon__lw__lane__config5__reg__t.html#a7471b2b0922805d23acd4a721d2d7dc8", null ],
    [ "lw_tx_disable", "unioncommon__lw__lane__config5__reg__t.html#aeff38c34485a216f4e0f7c963e6b13fc", null ],
    [ "lw_tx_squelch", "unioncommon__lw__lane__config5__reg__t.html#a53380c1fa2da1770a2417f1cbd677eeb", null ],
    [ "fields", "unioncommon__lw__lane__config5__reg__t.html#a42818b7054eed4f5d70e5178dd388575", null ]
];