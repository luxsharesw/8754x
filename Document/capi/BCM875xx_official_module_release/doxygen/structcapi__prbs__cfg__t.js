var structcapi__prbs__cfg__t =
[
    [ "poly", "structcapi__prbs__cfg__t.html#a991b9bb4301cdaddff19f89a36a846a2", null ],
    [ "bh_checker_mode", "structcapi__prbs__cfg__t.html#ad5a988ad0bc835d08e6ab24a4a8049c3", null ],
    [ "rx_invert", "structcapi__prbs__cfg__t.html#af3bc875e4f9d5f5024a7f0aa73a7943e", null ],
    [ "tx_invert", "structcapi__prbs__cfg__t.html#a6bce385db16f5ea3a6083f415fe2a3e7", null ],
    [ "lw_checker_auto_det", "structcapi__prbs__cfg__t.html#aca2f25003535c75b4c23d5fd992f3fe2", null ]
];