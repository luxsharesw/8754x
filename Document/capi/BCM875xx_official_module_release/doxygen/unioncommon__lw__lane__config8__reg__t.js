var unioncommon__lw__lane__config8__reg__t =
[
    [ "words", "unioncommon__lw__lane__config8__reg__t.html#a6a5cba8395c33191d95f33f8fc5c1058", null ],
    [ "lw_eq_maintap", "unioncommon__lw__lane__config8__reg__t.html#aa1ad04f1b11194421910c5f765c355c9", null ],
    [ "lw_dc_wander_mu_override", "unioncommon__lw__lane__config8__reg__t.html#acdbfce87f90bebfc6ed73a9edbf6ce20", null ],
    [ "lw_dc_wander_mu", "unioncommon__lw__lane__config8__reg__t.html#a170a99df29d152c4b80341830ee0ba63", null ],
    [ "lw_los_th_cfg", "unioncommon__lw__lane__config8__reg__t.html#acabf6c60f08adb2db2f2aafeae3fbe90", null ],
    [ "lw_gain_boost", "unioncommon__lw__lane__config8__reg__t.html#af45652895ecedbe7f968fdffc7c8947a", null ],
    [ "fields", "unioncommon__lw__lane__config8__reg__t.html#a5676438423139f547ff8a49e369f3441", null ]
];