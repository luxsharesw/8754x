var unionchip__top__chip__firmware__interal1__reg__t =
[
    [ "words", "unionchip__top__chip__firmware__interal1__reg__t.html#a495eba55306655759d871ee3392b22c2", null ],
    [ "single_or_dual", "unionchip__top__chip__firmware__interal1__reg__t.html#a60733bc367bc07732a57ce007b248047", null ],
    [ "lw_pending", "unionchip__top__chip__firmware__interal1__reg__t.html#a6dc88e445ef6849ba1e7c4d05b8471b5", null ],
    [ "bh_pending", "unionchip__top__chip__firmware__interal1__reg__t.html#a897af7b689380481d0b38cd6c7417f32", null ],
    [ "dw_pending", "unionchip__top__chip__firmware__interal1__reg__t.html#a57d17c29463637f6d392772fef94b135", null ],
    [ "tdb", "unionchip__top__chip__firmware__interal1__reg__t.html#af79496d463071e720b663b361e834739", null ],
    [ "fw_sub_ver_num", "unionchip__top__chip__firmware__interal1__reg__t.html#a356a4a8cc720bc7e876a2a4712fc6216", null ],
    [ "fields", "unionchip__top__chip__firmware__interal1__reg__t.html#adb6c0c839740c405c5c22ae6764650e5", null ]
];