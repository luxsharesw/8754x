var structcapi__status__t =
[
    [ "status_type", "structcapi__status__t.html#a592fe4dbdbcd8bda7de17a94c681511a", null ],
    [ "lock_status", "structcapi__status__t.html#acb4022ed852bf332b19b8b948975b55c", null ],
    [ "cdr", "structcapi__status__t.html#addfc7a4086d470a145de71f418b9ffe3", null ],
    [ "lol_sticky", "structcapi__status__t.html#a600367d4d199255427734e5ea8a18fb4", null ],
    [ "los_status", "structcapi__status__t.html#a6c82c1dbf1bc444e8822c18fe5ffcd4e", null ],
    [ "los", "structcapi__status__t.html#a6aff2e3879bbf4f676ce6b954667c805", null ],
    [ "lane_sigdet_status", "structcapi__status__t.html#ad9b7cdd862c5f23825a71ba1403b00d7", null ],
    [ "lane_tx_squelch_status", "structcapi__status__t.html#a5b91fe4a82639f642b3a216206b40cd4", null ],
    [ "lane_rx_output_status", "structcapi__status__t.html#ab0cc709ea03931a0f25ace0a437b5cb9", null ],
    [ "lane_rx_output_latch_status", "structcapi__status__t.html#ad00bcb5b99ff774843985d018ba931f1", null ],
    [ "lane_cdr_restart_counter", "structcapi__status__t.html#ad147c51f5741c7c9d1b0bbec00df2bdd", null ],
    [ "param", "structcapi__status__t.html#a9051cce6d7f3b26acec9bacbe1389772", null ]
];