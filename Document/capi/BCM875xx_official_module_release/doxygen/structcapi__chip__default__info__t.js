var structcapi__chip__default__info__t =
[
    [ "content", "structcapi__chip__default__info__t.html#a56f4e4db66b604138dbbfa4072551fb9", null ],
    [ "avs", "structcapi__chip__default__info__t.html#a914fffc95642cc4118ff3a3ef5b915ea", null ],
    [ "internal_regulator", "structcapi__chip__default__info__t.html#a4808f5df5154caec34c2e83ab275176f", null ],
    [ "reserved", "structcapi__chip__default__info__t.html#aff8a03df3a5cb03446571ac9fe07f0a1", null ],
    [ "is", "structcapi__chip__default__info__t.html#a4cde6f1a4c226a7d68b3e4a7c311ed93", null ],
    [ "module", "structcapi__chip__default__info__t.html#a0bf37de7528dbba416929890052aebfd", null ],
    [ "avs_default", "structcapi__chip__default__info__t.html#a0dca63d707ae1062ff4e4b7fe62e7e98", null ],
    [ "int_regulator", "structcapi__chip__default__info__t.html#ab1f54dd4546eb5dc70c7c10941a854be", null ],
    [ "config", "structcapi__chip__default__info__t.html#ac7ea30be7b13baed0a0a9b9aff90c1a1", null ]
];