var unionocw__default__mode__reg__t =
[
    [ "words", "unionocw__default__mode__reg__t.html#af0ed7fab8b02fee9152f41c3b70a8900", null ],
    [ "skip", "unionocw__default__mode__reg__t.html#a7e93be2e1f52f30d1d8127d542aa0c4d", null ],
    [ "pre_programmed", "unionocw__default__mode__reg__t.html#ad8601274095082893bc0486ee6999529", null ],
    [ "mode_config", "unionocw__default__mode__reg__t.html#a56122ddd9277933f07af3a7928628813", null ],
    [ "avs_enable", "unionocw__default__mode__reg__t.html#a58915fd66eb441b860f40c4dfd123425", null ],
    [ "no_surge", "unionocw__default__mode__reg__t.html#ade1bdf3a960ace3d68a931fe39178d9a", null ],
    [ "port_mask", "unionocw__default__mode__reg__t.html#a31638397ddea081666790912ba36bdd4", null ],
    [ "tdb", "unionocw__default__mode__reg__t.html#a0afd33dccecf6212f92e9be2a831fa81", null ],
    [ "fields", "unionocw__default__mode__reg__t.html#a44f39e880e4b6c7c00e6fb39b661b454", null ]
];