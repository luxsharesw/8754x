var unionintf__cwtobh__t =
[
    [ "words", "unionintf__cwtobh__t.html#a89637f44f59d171b7cfc330e14f791ca", null ],
    [ "force_cdr_restart", "unionintf__cwtobh__t.html#a81094d9ce89c919eecd7821c2b3f5b3f", null ],
    [ "squelch_tx", "unionintf__cwtobh__t.html#a788802cb087953c73a87173148525e99", null ],
    [ "turnon_igr_clock", "unionintf__cwtobh__t.html#a197e28c293e8ea65f43c82217ba24326", null ],
    [ "turnon_egr_clock", "unionintf__cwtobh__t.html#a35210f2e40586833aebf7d992de1e834", null ],
    [ "txpi_restart", "unionintf__cwtobh__t.html#a27c80e87c4a1ad72d72aa74769fffa64", null ],
    [ "rsvd2", "unionintf__cwtobh__t.html#a4e698f78e0cca121f98f7353bd033662", null ],
    [ "oppsite_side_cdrlock", "unionintf__cwtobh__t.html#a321b0730a1d565d382b9960662a14c87", null ],
    [ "egress_mission", "unionintf__cwtobh__t.html#ab6c8cd53542a2421a9ce703e46a134a2", null ],
    [ "an_linkup", "unionintf__cwtobh__t.html#a11ce792d6fb3f7da6ef2ff1880e80f41", null ],
    [ "rsvd1", "unionintf__cwtobh__t.html#aeb73003b85dd3f2730272d69cc4ad251", null ],
    [ "cmdstat_chg", "unionintf__cwtobh__t.html#a339c9b08eeea62426d6870977f43e3d1", null ],
    [ "fields", "unionintf__cwtobh__t.html#a2c8aaa7fcae203efc8edf056cb8578c7", null ]
];