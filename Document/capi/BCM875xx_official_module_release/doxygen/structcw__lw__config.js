var structcw__lw__config =
[
    [ "br", "structcw__lw__config.html#a5a45aa896c93d0e49da3497906aa1529", null ],
    [ "mod", "structcw__lw__config.html#a0bf90dfa0e478adfe2ed27e6bdcf3bf0", null ],
    [ "osr", "structcw__lw__config.html#a00ced32240b60fceb13f0f0aaaef7a89", null ],
    [ "multiplier", "structcw__lw__config.html#a0af14e3bcc1453ca0d2a87e31f68e1d5", null ],
    [ "vco_rate", "structcw__lw__config.html#aae264588e60559fee3270db0dfb7f41d", null ],
    [ "lane_mask", "structcw__lw__config.html#a485927205fcb7d51ccf3aa3c5699c39b", null ]
];