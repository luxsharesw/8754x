var chip__mode__def_8h =
[
    [ "txfir_info_t", "structtxfir__info__t.html", "structtxfir__info__t" ],
    [ "chip_bh_config", "structchip__bh__config.html", "structchip__bh__config" ],
    [ "cw_lw_config", "structcw__lw__config.html", "structcw__lw__config" ],
    [ "cw_port_entry_t", "structcw__port__entry__t.html", "structcw__port__entry__t" ],
    [ "cw_port_entry_ex_t", "structcw__port__entry__ex__t.html", "structcw__port__entry__ex__t" ],
    [ "MAX_PORT", "chip__mode__def_8h.html#a401e1a60d6381236216b6a130a6685bd", null ],
    [ "MAX_LW_LANES", "chip__mode__def_8h.html#a8b3125e80b9c246c8f735c1917b90850", null ],
    [ "MAX_CLIENT_LANES", "chip__mode__def_8h.html#adfad176689ea2304939eedac4b8f191e", null ],
    [ "NEW_CMODE_NUM", "chip__mode__def_8h.html#acc44d2d2ad233ca81007aed7165e63f0", null ],
    [ "NEW_CMODE_MIN", "chip__mode__def_8h.html#a252641554c8e37b762daf425e64666b2", null ],
    [ "NEW_CMODE_MAX", "chip__mode__def_8h.html#a439469879ba500ae1f7145f666852f86", null ],
    [ "cw_chip_mode_t", "chip__mode__def_8h.html#ae31cf29bdef38478c76826dac2fca094", null ],
    [ "cw_function_mode_t", "chip__mode__def_8h.html#ab314f632032b69fd783b2656e4a73b0b", null ],
    [ "cw_host_fec_type_t", "chip__mode__def_8h.html#aa33639087fd1d4e3eaf1de76a95bff55", null ],
    [ "cw_line_fec_type_t", "chip__mode__def_8h.html#af62d1b33064968c956bc0808b806effc", null ],
    [ "cw_port_mux_t", "chip__mode__def_8h.html#ab70f8637fc6239bc2be12b8eb4961d92", null ],
    [ "cw_port_fec_term_type_t", "chip__mode__def_8h.html#a629e507c7f540c42a0b923222e37c647", null ],
    [ "capi_port_entry_t", "chip__mode__def_8h.html#aec1e5db78653cc35770e20289df5f005", null ],
    [ "ref_clk_freq_t", "chip__mode__def_8h.html#a8256e50340d15aecc0d9bdb9a8affa50", [
      [ "CHIP_REF_CLK_156_25_MHZ", "chip__mode__def_8h.html#a8256e50340d15aecc0d9bdb9a8affa50aeb332ec7200c704981a51bd6aaed92c7", null ],
      [ "CHIP_REF_CLK_106_50_MHZ", "chip__mode__def_8h.html#a8256e50340d15aecc0d9bdb9a8affa50a5431d9356d5cb0e61b27b81b6b73e4a9", null ],
      [ "CHIP_REF_CLK_122_88_MHZ", "chip__mode__def_8h.html#a8256e50340d15aecc0d9bdb9a8affa50a28cea83e8b1c416a73a55b3cd62ad815", null ],
      [ "CHIP_REF_CLK_212_50_MHZ", "chip__mode__def_8h.html#a8256e50340d15aecc0d9bdb9a8affa50af678ab4653449bb368c1d4cb822e3458", null ],
      [ "CHIP_REF_CLK_MAX", "chip__mode__def_8h.html#a8256e50340d15aecc0d9bdb9a8affa50af4a76dd0c2ff96b52d8385e7e79ba10f", null ]
    ] ],
    [ "txfir_taps_t", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91c", [
      [ "TXFIR_TAPS_NRZ_LP_3TAP", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91cabf6a77dff2f79b85989ecdd36874b2e7", null ],
      [ "TXFIR_TAPS_NRZ_6TAP", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91cab5d360a8a63a4b6b9d66220aa83b3145", null ],
      [ "TXFIR_TAPS_PAM4_LP_3TAP", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91cab0f863c4d7902542459d57964c384441", null ],
      [ "TXFIR_TAPS_PAM4_6TAP", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91ca1963092f5e629f7b0947e11144d5a1fc", null ],
      [ "TXFIR_TAPS_7TAP", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91ca190b159dadcd5a804934a9468f9ad3a2", null ],
      [ "TXFIR_TAPS_12TAP", "chip__mode__def_8h.html#a349c9897960d947d2e3138515b7bf91ca852917ab4733190643f20b1caec8fb19", null ]
    ] ],
    [ "cw_chip_mode_e", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715", [
      [ "CHIP_MODES_NONE", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a5a7915bc8b5d344c27de65dd3f24b527", null ],
      [ "CHIP_MODE_4X53G_PAM4_4X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715ac372442f102abedc07fac6b0acda9272", null ],
      [ "CHIP_MODE_2X53G_PAM4_2X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715ad04920b8b26c231233f11f0fb2c58374", null ],
      [ "CHIP_MODE_4X25G_NRZ_4X25G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715afa78b78f4bfc2675887efafcd096c94c", null ],
      [ "CHIP_MODE_1X53G_PAM4_1X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a05135d1132c578104266d4a2c7c52a55", null ],
      [ "CHIP_MODE_4X26G_NRZ_2X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a8ac7a553ddc110485e7e3a1b5499fa36", null ],
      [ "CHIP_MODE_2X26G_NRZ_1X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a695be92588ec42d982dca9c0ebaba953", null ],
      [ "CHIP_MODE_2X25G_NRZ_1X50G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a931a57d4747d5d5e77e8154aaa755f93", null ],
      [ "CHIP_MODE_4X25G_NRZ_2X50G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a0fe0ef6e1ceb248f34d9801f21b987a7", null ],
      [ "CHIP_MODE_1X10G_NRZ_1X10G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715ae6f122a29d7d3e4965e87b8cc1cae32b", null ],
      [ "CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a79d628abcf08c16b67552cb3a23b5f83", null ],
      [ "CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715ac7e709ae7dbaa040d2ec7fcc66477e08", null ],
      [ "CHIP_MODE_4X26G_NRZ_4X26G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a06f67a551b31096895cba6138d45d8af", null ],
      [ "CHIP_MODE_1X25G_NRZ_1X25G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a3292681ce6fc2837cdba9a54588b7d17", null ],
      [ "CHIP_MODE_2X26G_NRZ_2X26G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a2595697c18674662fb160985d329f535", null ],
      [ "CHIP_MODE_2X25G_NRZ_2X25G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715aab2ae6212e7621e6d2d9bd422acd8ddd", null ],
      [ "CHIP_MODE_2X25G_NRZ_1X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a25876531c3a9482e420fc6b09c9efc82", null ],
      [ "CHIP_MODE_4X25G_NRZ_2X53G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a945affb1889604fc22637ee1867fa578", null ],
      [ "CHIP_MODE_4X50G_PAM4_4X50G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715ac6e6e032bc35c9ae042741377ab56d5b", null ],
      [ "CHIP_MODE_2X25G_NRZ_1X53G_PAM4_M1", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a0975096d0a476442381d8803ec755e18", null ],
      [ "CHIP_MODE_4X25G_NRZ_4X25G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a9c958051ac00db52723575826b30a149", null ],
      [ "CHIP_MODE_2X50G_PAM4_2X50G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a706f3cf2dcde76284c8b8b83d6fc6b58", null ],
      [ "CHIP_MODE_1X50G_PAM4_1X50G_PAM4", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a8b72ce62693373ab43382f07cf7dd270", null ],
      [ "CHIP_MODE_4X10G_NRZ_2X20G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a75effc174bd01a64124e7b95930326a2", null ],
      [ "CHIP_MODE_4X10G_NRZ_4X10G_NRZ", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715a9ba49467190b7c23d27382593424c9d2", null ],
      [ "CHIP_MODE_MAX_MODE", "chip__mode__def_8h.html#a0dee74a3a620fc183e2990ec2f654715abc82f0e30889bf1756264932b7bf39ad", null ]
    ] ],
    [ "chip_bh_baud_rate_t", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124", [
      [ "CHIP_BH_BR_53_125", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124a9929493b8e8f565a613f3f3195070ab7", null ],
      [ "CHIP_BH_BR_51_5625", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124ab82550096150e2358d56b38b9413a1d3", null ],
      [ "CHIP_BH_BR_25_78125", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124a32bcaa7d38e5813ecbaacbe9994f47b7", null ],
      [ "CHIP_BH_BR_26_5625", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124aab3c470b7e70f91bca9c5e6c080bc63c", null ],
      [ "CHIP_BH_BR_10_3125", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124a83b95da54f555f82f83b0629f0644319", null ],
      [ "CHIP_BH_BR_20_625", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124a5f289b1c4b415d0329d7e1dc6159f443", null ],
      [ "CHIP_BH_BR_1_25", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124a6a53d9ab6e1f60fce86b20085dc591fd", null ],
      [ "CHIP_BH_BR_MAX", "chip__mode__def_8h.html#a74ba9ee3aa38d2044bda757124e73124a8d87d680909dd5c3124a8f7000baff0e", null ]
    ] ],
    [ "chip_modulation_t", "chip__mode__def_8h.html#a628565f5dd86ada4184a156ba8427203", [
      [ "CHIP_MOD_NRZ", "chip__mode__def_8h.html#a628565f5dd86ada4184a156ba8427203a114bafdc63b71902487827b7af977c33", null ],
      [ "CHIP_MOD_PAM4", "chip__mode__def_8h.html#a628565f5dd86ada4184a156ba8427203a7fd5e7df01ebe63421125113fb099011", null ]
    ] ],
    [ "chip_bh_osr_t", "chip__mode__def_8h.html#a3bfd290fb3251330fc69a4bfc8469ecd", [
      [ "CHIP_BH_OSR_1", "chip__mode__def_8h.html#a3bfd290fb3251330fc69a4bfc8469ecdaf33eecbcbacdca1ca42b3e03d5758ac9", null ],
      [ "CHIP_BH_OSR_2", "chip__mode__def_8h.html#a3bfd290fb3251330fc69a4bfc8469ecdad0c5e78087d0921f9ff293d86ec91a1e", null ],
      [ "CHIP_BH_OSR_16_5", "chip__mode__def_8h.html#a3bfd290fb3251330fc69a4bfc8469ecdace3efd63505af551084ee0ed8407498b", null ],
      [ "CHIP_BH_OSR_MAX", "chip__mode__def_8h.html#a3bfd290fb3251330fc69a4bfc8469ecda5264aa27a5ae75ff4a119a976693c7e1", null ]
    ] ],
    [ "chip_bh_pll_multiplier_t", "chip__mode__def_8h.html#acc7d8fedc34969881d11990e6869c7d5", [
      [ "CHIP_BH_PLL_MULTIPLIER_170", "chip__mode__def_8h.html#acc7d8fedc34969881d11990e6869c7d5abd9ca9c842522f4f6226f1e8e9f9e39e", null ],
      [ "CHIP_BH_PLL_MULTIPLIER_165", "chip__mode__def_8h.html#acc7d8fedc34969881d11990e6869c7d5a56e422d3e288d93c44eea5c3fd804fa9", null ],
      [ "CHIP_BH_PLL_MULTIPLIER_160", "chip__mode__def_8h.html#acc7d8fedc34969881d11990e6869c7d5a0a157ce437490a000a5d9f7f4db475ff", null ],
      [ "CHIP_BH_PLL_MULTIPLIER_132", "chip__mode__def_8h.html#acc7d8fedc34969881d11990e6869c7d5ac29cdd85d263b2b24fde5b641b0a0856", null ],
      [ "CHIP_BH_PLL_MULTIPLIER_MAX", "chip__mode__def_8h.html#acc7d8fedc34969881d11990e6869c7d5a877f46547d4d64bf7cfb9297323fe2f4", null ]
    ] ],
    [ "chip_bh_vco_rate_t", "chip__mode__def_8h.html#a71b7f7ad80bc38e9e117e02d7ace4309", [
      [ "CHIP_BH_VCO_26P5625", "chip__mode__def_8h.html#a71b7f7ad80bc38e9e117e02d7ace4309ad5d1dd637a5c2fa83cd03b6e24eecbed", null ],
      [ "CHIP_BH_VCO_25P78125", "chip__mode__def_8h.html#a71b7f7ad80bc38e9e117e02d7ace4309a62e8782a04ed97aaddd94207eb4c4537", null ],
      [ "CHIP_BH_VCO_20P625", "chip__mode__def_8h.html#a71b7f7ad80bc38e9e117e02d7ace4309a0dbabaead11f42667ef46e6e8cc31365", null ],
      [ "CHIP_BH_VCO_MAX", "chip__mode__def_8h.html#a71b7f7ad80bc38e9e117e02d7ace4309a2c412d5cdad0185dd724dcdf47dee5b1", null ]
    ] ],
    [ "cw_lw_baud_rate_t", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8", [
      [ "CHIP_LW_BR_53_125", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8ad8dc1d740ae40d802f51935d9b15d8c2", null ],
      [ "CHIP_LW_BR_51_5625", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8a93ceecc17f6f3373ff2852a33a62bf44", null ],
      [ "CHIP_LW_BR_25_78125", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8a197e7cedcf2412e83c595ca60c81cee3", null ],
      [ "CHIP_LW_BR_26_5625", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8a97def5a0f4cb70e84402ecf017662ed5", null ],
      [ "CHIP_LW_BR_10_3125", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8ad96dbde51bd190df89e4bc1f2097602b", null ],
      [ "CHIP_LW_BR_20_625", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8afbe6815272f0b723b6908194a9ce7060", null ],
      [ "CHIP_LW_BR_1_25", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8aba898269838b3ecac4fbd845399fe866", null ],
      [ "CHIP_LW_BR_MAX", "chip__mode__def_8h.html#aa0b8b5b0ea5759f3a929bfa9d6c4dbe8aca6939fb8c9ccd751dc28515f9d56e51", null ]
    ] ],
    [ "cw_lw_osr_t", "chip__mode__def_8h.html#ae46aff7ae9423f16e30e5c4c943eccba", [
      [ "CW_LW_OSR_1", "chip__mode__def_8h.html#ae46aff7ae9423f16e30e5c4c943eccbaaa1201fcf629ab59cfc1138d524ae4d5d", null ],
      [ "CW_LW_OSR_2", "chip__mode__def_8h.html#ae46aff7ae9423f16e30e5c4c943eccbaa3b7d87211d40f9e7671665fb2566115f", null ],
      [ "CW_LW_OSR_MAX", "chip__mode__def_8h.html#ae46aff7ae9423f16e30e5c4c943eccbaa03be2e5815ca3c94a79c3fb39222c64b", null ]
    ] ],
    [ "cw_lw_pll_multiplier_t", "chip__mode__def_8h.html#a4414c8392abb94b9b6753e40445030c9", [
      [ "CW_LW_PLL_MULTIPLIER_170", "chip__mode__def_8h.html#a4414c8392abb94b9b6753e40445030c9aeb742d6d08622cfb66280935896332b6", null ],
      [ "CW_LW_PLL_MULTIPLIER_165", "chip__mode__def_8h.html#a4414c8392abb94b9b6753e40445030c9ae9b4661c4b1a130e81ce0a1fec6efeb7", null ],
      [ "CW_LW_PLL_MULTIPLIER_160", "chip__mode__def_8h.html#a4414c8392abb94b9b6753e40445030c9a00a923f21ffe34d7a7dd2f80502bf9eb", null ],
      [ "CW_LW_PLL_MULTIPLIER_132", "chip__mode__def_8h.html#a4414c8392abb94b9b6753e40445030c9a0f069759b1622d6c97965daf3d9f2d07", null ],
      [ "CW_LW_PLL_MULTIPLIER_MAX", "chip__mode__def_8h.html#a4414c8392abb94b9b6753e40445030c9a74f7e185e2701ea93c3340196a7b58a1", null ]
    ] ],
    [ "cw_lw_vco_rate_t", "chip__mode__def_8h.html#a93ae2f3898b5654f35cb20ef1c02d65e", [
      [ "CHIP_LW_VCO_26P5625", "chip__mode__def_8h.html#a93ae2f3898b5654f35cb20ef1c02d65eac866edea8c397e1aa1aaed6898cdba70", null ],
      [ "CHIP_LW_VCO_25P78125", "chip__mode__def_8h.html#a93ae2f3898b5654f35cb20ef1c02d65ead9c1a6a08504aad663f8d3362642ece6", null ],
      [ "CHIP_LW_VCO_20P625", "chip__mode__def_8h.html#a93ae2f3898b5654f35cb20ef1c02d65ea60369d841b9fa0263f321f91e2cfeaf2", null ],
      [ "CHIP_LW_VCO_MAX", "chip__mode__def_8h.html#a93ae2f3898b5654f35cb20ef1c02d65ea854de7f429b43d39d0a490a1c364545a", null ]
    ] ],
    [ "cw_function_mode_e", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2b", [
      [ "CHIP_MODE_NONE", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2ba2ff8d37e76236b4d3765712c578dbd4b", null ],
      [ "CHIP_MODE_400G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2bae7ca05dc003da495677d3d52def4e101", null ],
      [ "CHIP_MODE_200G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2bad26118bf98c98a00d330cc0cdd7c302b", null ],
      [ "CHIP_MODE_100G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2ba41ba4dd6e6cb2aef809107782cf8516f", null ],
      [ "CHIP_MODE_50G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2baf4aba2aefc20a9a842c0ca2bb6ef1844", null ],
      [ "CHIP_MODE_25G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2ba53646b9661fe466d218403e4b5e95ebd", null ],
      [ "CHIP_MODE_10G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2ba2e75dc713e041b1299855cd7a9486207", null ],
      [ "CHIP_MODE_1G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2bac8f2f74f841daa14cdacacd0954760fd", null ],
      [ "CHIP_MODE_20G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2ba6eff8ccab0fae864b6fc2d662fcbe1d8", null ],
      [ "CHIP_MODE_40G", "chip__mode__def_8h.html#a6d4bf229a38f764f5d19767ddccabc2ba4d81a220cf81fbc48a9034099c9b550c", null ]
    ] ],
    [ "cw_host_fec_type_e", "chip__mode__def_8h.html#af3695972e4b80995777c7de9fe6a178c", [
      [ "CHIP_HOST_FEC_TYPE_NA", "chip__mode__def_8h.html#af3695972e4b80995777c7de9fe6a178ca6d50c7a3086fd8b9f31e1f99391e6cf7", null ],
      [ "CHIP_HOST_FEC_TYPE_RS528", "chip__mode__def_8h.html#af3695972e4b80995777c7de9fe6a178cabb2a6a86c8d1a028cc52bde3ed5ef6d2", null ],
      [ "CHIP_HOST_FEC_TYPE_RS544", "chip__mode__def_8h.html#af3695972e4b80995777c7de9fe6a178caad5502ff2dc3b197a7a4bbaf0213943d", null ],
      [ "CHIP_HOST_FEC_TYPE_PCS", "chip__mode__def_8h.html#af3695972e4b80995777c7de9fe6a178ca7ed8a667fc1b4970e4a7e2a404d8269e", null ],
      [ "CHIP_HOST_FEC_TYPE_MAX", "chip__mode__def_8h.html#af3695972e4b80995777c7de9fe6a178ca6f6d924de46c4bbe2448bb4fa3465e98", null ]
    ] ],
    [ "cw_line_fec_type_e", "chip__mode__def_8h.html#a24c0ea11e58c80c682d3a06c50ee2c15", [
      [ "CHIP_LINE_FEC_TYPE_NA", "chip__mode__def_8h.html#a24c0ea11e58c80c682d3a06c50ee2c15a70453950c023cbfcc3721e45b2a0b1e3", null ],
      [ "CHIP_LINE_FEC_TYPE_RS528", "chip__mode__def_8h.html#a24c0ea11e58c80c682d3a06c50ee2c15a837a57c11ff56c2c542be29e3a57fe2f", null ],
      [ "CHIP_LINE_FEC_TYPE_RS544", "chip__mode__def_8h.html#a24c0ea11e58c80c682d3a06c50ee2c15a8aba37e8b37b6e04d490dec60afa9625", null ],
      [ "CHIP_LINE_FEC_TYPE_PCS", "chip__mode__def_8h.html#a24c0ea11e58c80c682d3a06c50ee2c15aadfbd803a95bac67efcef0f2f183565a", null ],
      [ "CHIP_LINE_FEC_TYPE_MAX", "chip__mode__def_8h.html#a24c0ea11e58c80c682d3a06c50ee2c15a48730adcdc24a0dc0cbebeb72a6a514c", null ]
    ] ],
    [ "cw_port_mux_e", "chip__mode__def_8h.html#aa83930d1ee59dc763ff6e0beb2bfcd90", [
      [ "CHIP_PORT_MUX_BIT_MUX", "chip__mode__def_8h.html#aa83930d1ee59dc763ff6e0beb2bfcd90a286115d3922f27207f1e189d4a169f5b", null ],
      [ "CHIP_PORT_MUX_SYMBOL_MUX", "chip__mode__def_8h.html#aa83930d1ee59dc763ff6e0beb2bfcd90af5813509328e3be4799bc6fff750fbbb", null ],
      [ "CHIP_PORT_MUX_MAX", "chip__mode__def_8h.html#aa83930d1ee59dc763ff6e0beb2bfcd90a2a0f2dd5dc06241fc1ef41e77beb5f0f", null ]
    ] ],
    [ "cw_port_fec_term_type_e", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8", [
      [ "CHIP_PORT_FEC_TERM_BYPASS", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8a792de8b5a2a1a43110c248ef6f80f4fe", null ],
      [ "CHIP_PORT_FEC_DEC_FWD", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8ac2ae8821c4c0ae9bd758b8454a083bca", null ],
      [ "CHIP_PORT_FEC_DEC_ENC", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8a5541b911c484808984aa598b13d3235d", null ],
      [ "CHIP_LANE_PCS_XENC", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8a707cd15d6bd68b3ddb02bca50f6cc903", null ],
      [ "CHIP_LANE_FEC_DEC_XDEC_XENC_ENC", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8a94eb76b51e4ba498695bc5a93ff39cad", null ],
      [ "CHIP_LANE_XENC_PCS", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8a5dca53c0dcf188fab2f397ea4108e5a2", null ],
      [ "CHIP_LANE_FEC_TERM_MAX", "chip__mode__def_8h.html#a9233f5f76da5499d802b659d64580dc8a36410c06a87777e4c1b97be51f9a5e66", null ]
    ] ]
];