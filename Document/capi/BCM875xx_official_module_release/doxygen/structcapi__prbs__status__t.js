var structcapi__prbs__status__t =
[
    [ "prbs_type", "structcapi__prbs__status__t.html#a8ab10bd185754b8df96348be24daec1b", null ],
    [ "lock", "structcapi__prbs__status__t.html#ac6b044a755d6b4b892fc4abd6dda88f9", null ],
    [ "lock_loss", "structcapi__prbs__status__t.html#a38bccebac2df965d9f68d5f7b360d3f6", null ],
    [ "s_err", "structcapi__prbs__status__t.html#a1ee79b8fc3e7b6ce762a1a9f10deea69", null ],
    [ "ml_err", "structcapi__prbs__status__t.html#a078db84d6d4e11edfec156c7c8869b97", null ],
    [ "err", "structcapi__prbs__status__t.html#a92043b3f3fc99b4c9243808a55e9fdaa", null ]
];