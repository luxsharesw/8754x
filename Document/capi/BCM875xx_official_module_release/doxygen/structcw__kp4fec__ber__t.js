var structcw__kp4fec__ber__t =
[
    [ "cur_ber", "structcw__kp4fec__ber__t.html#a46c0a91531abc11351d394dbae3d9b3e", null ],
    [ "min_ber", "structcw__kp4fec__ber__t.html#a5ee482e890921a63fd9e7d3ba4981798", null ],
    [ "max_ber", "structcw__kp4fec__ber__t.html#ae9d1f91752c36f13770eea5f826cd216", null ],
    [ "average_ber", "structcw__kp4fec__ber__t.html#aacd09f886d59dbf44e06e1c4e9ae3724", null ],
    [ "total_ber", "structcw__kp4fec__ber__t.html#a5a419df6a1aea016096eb964a34b15a7", null ]
];