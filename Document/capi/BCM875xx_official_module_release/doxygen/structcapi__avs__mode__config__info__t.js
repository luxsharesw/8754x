var structcapi__avs__mode__config__info__t =
[
    [ "avs", "structcapi__avs__mode__config__info__t.html#a7b7747bd79a8560533c6330a3bc90576", null ],
    [ "disable_type", "structcapi__avs__mode__config__info__t.html#a6192b408795a22ab770fd4f6929b36b9", null ],
    [ "avs_ctrl", "structcapi__avs__mode__config__info__t.html#a5b155cb0f11bd7a8b128c462139302a3", null ],
    [ "package_share", "structcapi__avs__mode__config__info__t.html#a44370a0f0dba65303fff19f432f145fa", null ],
    [ "board_dc_margin", "structcapi__avs__mode__config__info__t.html#aa0a1a37c2eb8acb72f0b29ed5f2a74b0", null ],
    [ "type_of_regulator", "structcapi__avs__mode__config__info__t.html#a9546817221d0cd86bd67d1d0d9de37a7", null ],
    [ "i2c_slave_address", "structcapi__avs__mode__config__info__t.html#a1a256fb5d507737f9127a86aaf57c11f", null ],
    [ "external_ctrl_step", "structcapi__avs__mode__config__info__t.html#ac74d71a9b97c91997597eb891628a6e6", null ]
];