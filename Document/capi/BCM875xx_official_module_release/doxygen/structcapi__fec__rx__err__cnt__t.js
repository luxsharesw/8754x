var structcapi__fec__rx__err__cnt__t =
[
    [ "tot_frame_rev_cnt", "structcapi__fec__rx__err__cnt__t.html#ad6f0dfea005671c3bef5b7f57bb623c1", null ],
    [ "tot_frame_corr_cnt", "structcapi__fec__rx__err__cnt__t.html#abe08a4afeb26b726ce73fb54f69113c4", null ],
    [ "tot_frame_uncorr_cnt", "structcapi__fec__rx__err__cnt__t.html#a338c52dd3a9f4140f574a56f5d50dc9b", null ],
    [ "tot_symbols_corr_cnt", "structcapi__fec__rx__err__cnt__t.html#a12106361c15d0d32abb3ddc6740def01", null ],
    [ "tot_bits_corr_cnt", "structcapi__fec__rx__err__cnt__t.html#a3d56b0d5996f5a06cdcebcb7de57d05f", null ],
    [ "tot_frames_err_cnt", "structcapi__fec__rx__err__cnt__t.html#a58b52132abc945d96ecda468f7b6893c", null ]
];