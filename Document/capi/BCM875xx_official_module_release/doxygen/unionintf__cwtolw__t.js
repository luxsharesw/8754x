var unionintf__cwtolw__t =
[
    [ "words", "unionintf__cwtolw__t.html#ae075e5d53ad469825039116e967d1e88", null ],
    [ "force_cdr_restart", "unionintf__cwtolw__t.html#a9360e75b77756bd85262c6872a5c3ca7", null ],
    [ "squelch_tx", "unionintf__cwtolw__t.html#a705f2df82edaedad31742b0f58e6f89a", null ],
    [ "turnon_igr_clock", "unionintf__cwtolw__t.html#a5adc3454839e2d62ebf5a2b5a5d7b6dd", null ],
    [ "turnon_egr_clock", "unionintf__cwtolw__t.html#a6bb5e4c89e826953e8454ae34a37511c", null ],
    [ "txpi_restart", "unionintf__cwtolw__t.html#a84be76fe76bca9011a25f39cd502b57f", null ],
    [ "rsvd2", "unionintf__cwtolw__t.html#a0e51e4d0987c50b38768ebffbf5957ff", null ],
    [ "oppsite_side_cdrlock", "unionintf__cwtolw__t.html#a8713ebc8e9f7075c0167979651d4ae48", null ],
    [ "egress_mission", "unionintf__cwtolw__t.html#a4ba1291a0624d30f3df654a86eb7db98", null ],
    [ "an_linkup", "unionintf__cwtolw__t.html#a7e6814c1e94f650e92b079965f428d62", null ],
    [ "rsvd1", "unionintf__cwtolw__t.html#af990ed77477fca6305c225e61c58166b", null ],
    [ "cmdstat_chg", "unionintf__cwtolw__t.html#ac7d6fc9a0a351d1ba643920b52a86529", null ],
    [ "fields", "unionintf__cwtolw__t.html#acbefbee6d5a1818fc4e6fef9b0a294d8", null ]
];