var host__power__util_8c =
[
    [ "power_util_ana_reg_access", "host__power__util_8c.html#acdda0999296317d41a323c5a6bcb4144", null ],
    [ "power_util_ana_reg_rdwr_access", "host__power__util_8c.html#a894c59f5dc5638a002b0c52dae6b7748", null ],
    [ "power_util_ana_vddm_set_volt", "host__power__util_8c.html#a1d113abddba2e310181992ead837c211", null ],
    [ "power_util_ana_vddm_get_volt", "host__power__util_8c.html#a7a78f6966fb8f5c31717de95e25f412a", null ],
    [ "power_util_ana_avdd_set_volt", "host__power__util_8c.html#ac2647dcdd28afa90306b5eb9419010b6", null ],
    [ "power_util_ana_avdd_get_volt", "host__power__util_8c.html#aee1331ba71d08dfd94e4c9afb7a4085d", null ],
    [ "host_power_util_boot_config", "host__power__util_8c.html#a4a1e6f80a40bf308bb4b47b260066710", null ]
];