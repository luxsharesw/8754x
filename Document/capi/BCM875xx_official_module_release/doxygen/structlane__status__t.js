var structlane__status__t =
[
    [ "cdr_lock", "structlane__status__t.html#aa1fec72dd86be39ec848541c99dff252", null ],
    [ "los", "structlane__status__t.html#a42e89ef2ba96bc96d459deb9c9f73463", null ],
    [ "tx_squelch", "structlane__status__t.html#a318cf97e4ed485ea780b88df101381c8", null ],
    [ "tx_mission", "structlane__status__t.html#a1c1d2695b31f8096ba4bfa42e4769392", null ],
    [ "rx_mission", "structlane__status__t.html#ad9c8555e26e73e7146910ef6330d21c7", null ],
    [ "pll_lock", "structlane__status__t.html#a136ff2471df309299d5996082558e5cf", null ]
];