var unioncommon__lw__cfg__st__dump__2__t =
[
    [ "words", "unioncommon__lw__cfg__st__dump__2__t.html#a0ca6030d5af879fc3ce32439af76ab48", null ],
    [ "phbias_tune_static_dis", "unioncommon__lw__cfg__st__dump__2__t.html#a22415c2ff07d5ce8d224fe0e134a7141", null ],
    [ "phbias_tune_dync_dis", "unioncommon__lw__cfg__st__dump__2__t.html#ab1cc8a5cb60de82bfbbf7c5921c9017c", null ],
    [ "phbias_min", "unioncommon__lw__cfg__st__dump__2__t.html#a2d61de2d89647f73a6e6995269e81d89", null ],
    [ "phbias_max", "unioncommon__lw__cfg__st__dump__2__t.html#a819e0683f38b6e079a29a1da7b47c593", null ],
    [ "phbias_step_size", "unioncommon__lw__cfg__st__dump__2__t.html#a02b4e1010d74631cd08c5479642ef13d", null ],
    [ "reserved", "unioncommon__lw__cfg__st__dump__2__t.html#ad65470faa496e5a74e550c44fe9ca1b7", null ],
    [ "fields", "unioncommon__lw__cfg__st__dump__2__t.html#a0af6073cca552b182d19ec60a5def248", null ]
];