var serdes__config_8h =
[
    [ "SERDES_LANE_NUM_MAX", "serdes__config_8h.html#ab58d35250cd1940826c16f15eb12a6c7", null ],
    [ "SERDES_LANE_NUM_PER_CORE", "serdes__config_8h.html#a43e4d02fd076c0420f71f32679834f61", null ],
    [ "SERDES_HOST_SIDE_ADDRESS", "serdes__config_8h.html#ae2c7261e6a99346ab8e0385a3454d3bc", null ],
    [ "SERDES_MEDIA_SIDE_ADDRESS", "serdes__config_8h.html#a5d3b1ad7e89cf1b68636ebccc326154d", null ],
    [ "serdes_lane_bbaddr", "serdes__config_8h.html#a12783874540c4be3dbb1a28f330e9d10", null ]
];