var structdsp__cmis__info__t =
[
    [ "cmis_snr", "structdsp__cmis__info__t.html#a6ad12baf80918b82430674475f7d1f81", null ],
    [ "cmis_ltp", "structdsp__cmis__info__t.html#a2b57c5dea0c93e0ecafeff1687815038", null ],
    [ "slicer_threshold", "structdsp__cmis__info__t.html#a02a9fe26c2969efbfe24f2463ee93512", null ],
    [ "slicer_target", "structdsp__cmis__info__t.html#a3a4e03281f2a6c3bb3206e9943012a65", null ],
    [ "slicer_mean_value", "structdsp__cmis__info__t.html#aeb1d3247ee27c76a79ac2159e4e687b8", null ],
    [ "slicer_sigma_value", "structdsp__cmis__info__t.html#af758bcfdf257bd02886c06a58cb8ab9e", null ],
    [ "slicer_p_value", "structdsp__cmis__info__t.html#acaf20bdfb7159ad09b9912ced1ceccd6", null ],
    [ "slicer_p_location", "structdsp__cmis__info__t.html#a5777cf14c475b254e5fd64ef89b709a6", null ],
    [ "slicer_v_value", "structdsp__cmis__info__t.html#aee912d4d880cfa8cf1815bc6c3e518f9", null ],
    [ "slicer_v_location", "structdsp__cmis__info__t.html#ad285c5538120cf253d1639a6128e4d38", null ]
];