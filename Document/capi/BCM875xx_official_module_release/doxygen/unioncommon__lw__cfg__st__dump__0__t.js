var unioncommon__lw__cfg__st__dump__0__t =
[
    [ "words", "unioncommon__lw__cfg__st__dump__0__t.html#a070a11db3a73e2840f48f7139436c2d3", null ],
    [ "peaking_filter", "unioncommon__lw__cfg__st__dump__0__t.html#a4a1b455993e432f03e18684679d402d2", null ],
    [ "dc_wander_mu", "unioncommon__lw__cfg__st__dump__0__t.html#a02bb2579ca474926d52a0e35d73f4822", null ],
    [ "gain_boost", "unioncommon__lw__cfg__st__dump__0__t.html#ae32c9068a193d02e475b3212918f56f4", null ],
    [ "nldet", "unioncommon__lw__cfg__st__dump__0__t.html#a153dc36f799534e4f7168dfdd3df2510", null ],
    [ "rx_graycode", "unioncommon__lw__cfg__st__dump__0__t.html#a2cb8b871a7e48c3492bd577cc5f2f71a", null ],
    [ "rsvr", "unioncommon__lw__cfg__st__dump__0__t.html#aca0df7c63cae4637abfc105160cedf84", null ],
    [ "fields", "unioncommon__lw__cfg__st__dump__0__t.html#a08a975b1bd863ae28561ae43c046cedc", null ]
];