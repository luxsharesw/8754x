var unionchip__top__avs__mode__config__reg__t =
[
    [ "words", "unionchip__top__avs__mode__config__reg__t.html#afcbf4081622e7a79516f170aca334a1f", null ],
    [ "avs_enable", "unionchip__top__avs__mode__config__reg__t.html#a36df7c9076988881659de20e1594ae66", null ],
    [ "avs_regulator_control_mode", "unionchip__top__avs__mode__config__reg__t.html#a5153136fb6f8a1bd4641ede94f3f8684", null ],
    [ "disable_type", "unionchip__top__avs__mode__config__reg__t.html#a14d8318aad1202cc771aef92a2a71762", null ],
    [ "num_of_phy", "unionchip__top__avs__mode__config__reg__t.html#a9f2c7cff3dcd7a2bf72885424499dd91", null ],
    [ "type_of_regulator", "unionchip__top__avs__mode__config__reg__t.html#aece70a9cdb9dd4e8ffdda7229b3e62a0", null ],
    [ "avs_dc_margin", "unionchip__top__avs__mode__config__reg__t.html#a319408fd9fd2feb15f45dc9e6d7dbf6f", null ],
    [ "avs_status", "unionchip__top__avs__mode__config__reg__t.html#a1e68ef27e65c065e4f45270e89a738d6", null ],
    [ "fields", "unionchip__top__avs__mode__config__reg__t.html#a929113de50756677f1e746059c1e573c", null ]
];