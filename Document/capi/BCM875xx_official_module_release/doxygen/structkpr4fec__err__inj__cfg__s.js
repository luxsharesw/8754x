var structkpr4fec__err__inj__cfg__s =
[
    [ "max_frame", "structkpr4fec__err__inj__cfg__s.html#a6d5528535ba23cd6a0e00422792e3325", null ],
    [ "mask_a_id", "structkpr4fec__err__inj__cfg__s.html#a26a7fc9ef067d9dd1e330f2b901aa030", null ],
    [ "mask_b_id", "structkpr4fec__err__inj__cfg__s.html#a19385b3ca4a745916f74f652d896ad84", null ],
    [ "mask_a_0", "structkpr4fec__err__inj__cfg__s.html#accf805945c8b4dbe6ef592f0548a6e0c", null ],
    [ "mask_a_1", "structkpr4fec__err__inj__cfg__s.html#ae8ab3fd57cbbd169954e980f68f842e8", null ],
    [ "mask_a_2", "structkpr4fec__err__inj__cfg__s.html#afce386e1bb9d4103d9b662c241fda549", null ],
    [ "mask_a_3", "structkpr4fec__err__inj__cfg__s.html#ab8522e6d6ba065b8a7f232e3a480afe2", null ],
    [ "mask_a_4", "structkpr4fec__err__inj__cfg__s.html#a5cc263f3464fce47dc1b40e47698a2e7", null ],
    [ "mask_b_0", "structkpr4fec__err__inj__cfg__s.html#a45d84a01417287b9fd215f7206dd46a8", null ],
    [ "mask_b_1", "structkpr4fec__err__inj__cfg__s.html#acd49f231f649e9d9dc5e01d5c0f528b4", null ],
    [ "mask_b_2", "structkpr4fec__err__inj__cfg__s.html#ad37b8713d9efae68bf360e2e3c50ed76", null ],
    [ "mask_b_3", "structkpr4fec__err__inj__cfg__s.html#ae7ad358efe8b9ca22e439ef7cb5cef53", null ],
    [ "mask_b_4", "structkpr4fec__err__inj__cfg__s.html#a9ad1e019edc27a3953865c4763a82251", null ]
];