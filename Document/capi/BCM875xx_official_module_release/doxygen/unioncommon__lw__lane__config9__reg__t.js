var unioncommon__lw__lane__config9__reg__t =
[
    [ "words", "unioncommon__lw__lane__config9__reg__t.html#aca535013ae7c6dbd93bbb757d9346b62", null ],
    [ "phase_dtune_bias_range", "unioncommon__lw__lane__config9__reg__t.html#a8f00dc6c341c85ce416a9fddfc086be4", null ],
    [ "phase_dtune_snr_change_th", "unioncommon__lw__lane__config9__reg__t.html#a374cbd52741598c5b737f7b64707f76c", null ],
    [ "phase_tune_link_down_snr", "unioncommon__lw__lane__config9__reg__t.html#aa5b920c6d553be97bb961ee1069e0778", null ],
    [ "phase_tune_dwell_time", "unioncommon__lw__lane__config9__reg__t.html#a898ee70436e0c50d27f492bb287ab5e3", null ],
    [ "reserved", "unioncommon__lw__lane__config9__reg__t.html#a6381a41d03277cd262e1e23313cb92c8", null ],
    [ "fields", "unioncommon__lw__lane__config9__reg__t.html#a753645ecf9b3fd402f8ec756232bb0ad", null ]
];