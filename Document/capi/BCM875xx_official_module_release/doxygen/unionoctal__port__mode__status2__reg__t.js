var unionoctal__port__mode__status2__reg__t =
[
    [ "words", "unionoctal__port__mode__status2__reg__t.html#ac24ad5462407f467dda145e3d405d5d2", null ],
    [ "host_fec_type", "unionoctal__port__mode__status2__reg__t.html#a86b02f85df264aa44e1aa911db5b7ba2", null ],
    [ "line_fec_type", "unionoctal__port__mode__status2__reg__t.html#a3efa4cbe71d3f89cdb4c7cd3a9e15731", null ],
    [ "fec_term", "unionoctal__port__mode__status2__reg__t.html#a79622796e061421c1ce3ed0f8e1a0485", null ],
    [ "port_index", "unionoctal__port__mode__status2__reg__t.html#acccdfa46292e4a451c8decc641097ecd", null ],
    [ "power_down_status", "unionoctal__port__mode__status2__reg__t.html#a087b042bcbebc170d20552403c41d748", null ],
    [ "line_symbol_mux", "unionoctal__port__mode__status2__reg__t.html#a8a90cfb435bf911c04b5803079c86114", null ],
    [ "rsvd", "unionoctal__port__mode__status2__reg__t.html#a3d64ef9dc3e1ad4f1c128d90da4c9ba1", null ],
    [ "fields", "unionoctal__port__mode__status2__reg__t.html#ab375180ca3cf09431d56a216f7c29bbe", null ]
];