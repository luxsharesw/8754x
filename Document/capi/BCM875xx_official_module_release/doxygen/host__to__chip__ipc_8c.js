var host__to__chip__ipc_8c =
[
    [ "intf_util_assign_tocken", "host__to__chip__ipc_8c.html#ab61bafa14d927f3fa1aa47dba0093168", null ],
    [ "intf_util_read_sram", "host__to__chip__ipc_8c.html#a7c42a1087a62a64d4b0ad2949f3462cb", null ],
    [ "intf_read_memory", "host__to__chip__ipc_8c.html#ac7de9eb856ca35b3a74d0a4d5b338036", null ],
    [ "intf_util_read_cmd_rsp_sram", "host__to__chip__ipc_8c.html#af022f9ef7416bb0d548be29c0e01b1a4", null ],
    [ "intf_util_write_sram", "host__to__chip__ipc_8c.html#af2ff26e841c7fad6549bce94210ce9da", null ],
    [ "intf_util_write_cmd_req_payload_sram", "host__to__chip__ipc_8c.html#abd4f3d67747817e270687a3ef71bbc3c", null ],
    [ "intf_capi2fw_command_response", "host__to__chip__ipc_8c.html#adf190ce27dcc2ac958db13017fe12dce", null ],
    [ "intf_capi2fw_command_request", "host__to__chip__ipc_8c.html#a87321cc06f442c53be56dc4613013890", null ],
    [ "intf_capi2fw_command_Handler", "host__to__chip__ipc_8c.html#a2e01ac227e78fce9deb5420d46155b98", null ],
    [ "cmd_tocken", "host__to__chip__ipc_8c.html#a8840ff54e90e560464e65eb7f59b3fb0", null ]
];