var unioncommon__lw__lane__config25__reg__t =
[
    [ "words", "unioncommon__lw__lane__config25__reg__t.html#a67120daf9d6e84754b16cfd526657b05", null ],
    [ "lw_nldet_casel_ovrd", "unioncommon__lw__lane__config25__reg__t.html#a73a032655067796039d885beac8b324a", null ],
    [ "lw_nldet_casel", "unioncommon__lw__lane__config25__reg__t.html#af482660d325387511a78c8aed97ce3e9", null ],
    [ "lw_nldet_dsel_ovrd", "unioncommon__lw__lane__config25__reg__t.html#a951404a14c2cb427f88bb6a989376daa", null ],
    [ "lw_nldet_dsel", "unioncommon__lw__lane__config25__reg__t.html#a0db3a52eca0d9e99ce20f00c174f4f0a", null ],
    [ "lw_nldet_lms_mu_ovrd", "unioncommon__lw__lane__config25__reg__t.html#aad13949531fd4a4a7c995f47a8ca5b92", null ],
    [ "lw_nldet_lms_mu", "unioncommon__lw__lane__config25__reg__t.html#a9ff6954c031919e5c464a2aa8abcd62c", null ],
    [ "lw_nldet_mse_mu_ovrd", "unioncommon__lw__lane__config25__reg__t.html#a9a8efdf927f3ee1b17fa10119e12f53e", null ],
    [ "lw_nldet_mse_mu", "unioncommon__lw__lane__config25__reg__t.html#a1f2b08c93ecbc1c3b0038dd4c2e87519", null ],
    [ "reserve", "unioncommon__lw__lane__config25__reg__t.html#a6d552020f875f5bbb8491ece31cb5ef4", null ],
    [ "fields", "unioncommon__lw__lane__config25__reg__t.html#a2e7051732887a0541d2bd8c35d60550e", null ]
];