var diag__fec__statistics__def_8h =
[
    [ "kpr4fec_err_cntr_s", "structkpr4fec__err__cntr__s.html", "structkpr4fec__err__cntr__s" ],
    [ "kpr4fec_err_cnt_u", "unionkpr4fec__err__cnt__u.html", "unionkpr4fec__err__cnt__u" ],
    [ "kpr4fec_cnt_s", "structkpr4fec__cnt__s.html", "structkpr4fec__cnt__s" ],
    [ "kpr4fec_ieee_cnt_s", "structkpr4fec__ieee__cnt__s.html", "structkpr4fec__ieee__cnt__s" ],
    [ "diag_port_cfg_list_t", "structdiag__port__cfg__list__t.html", "structdiag__port__cfg__list__t" ],
    [ "kpr4fec_err_inj_cfg_s", "structkpr4fec__err__inj__cfg__s.html", "structkpr4fec__err__inj__cfg__s" ],
    [ "KP4_FEC_TOT_BITS_CORR_NUM", "diag__fec__statistics__def_8h.html#af6b53bb8dc9c78f0c85cfc8a26f9eab8", null ],
    [ "KP4_FEC_TOT_FRAMES_ERR_NUM", "diag__fec__statistics__def_8h.html#a99b774978770bbc3e685c9787b3b5fc0", null ],
    [ "MAX_FEC_LANE_NUM", "diag__fec__statistics__def_8h.html#a4903cc45d2796a2223c59ecacaf3e627", null ],
    [ "kpr4fec_lock_e", "diag__fec__statistics__def_8h.html#a91957dbcf4fe6fee959dcda32dfb028e", [
      [ "KP4FEC_AM_UNLOCK", "diag__fec__statistics__def_8h.html#a91957dbcf4fe6fee959dcda32dfb028ea117d17e5fb5d9800cc2f49c8cb0adf5b", null ],
      [ "KP4FEC_AM_LOCKED", "diag__fec__statistics__def_8h.html#a91957dbcf4fe6fee959dcda32dfb028ea6012fcb8208deb289a361ced17aa0916", null ]
    ] ]
];