var unioncapi__lane__status__t =
[
    [ "bytes", "unioncapi__lane__status__t.html#aa521ef7781468d17c61816b3032d28e9", null ],
    [ "plllock", "unioncapi__lane__status__t.html#acd07d8475011edf35e5b88495af6090d", null ],
    [ "los", "unioncapi__lane__status__t.html#a9ca978a09cbfec6ea759ffe6ef2efb87", null ],
    [ "adcclip", "unioncapi__lane__status__t.html#aae472708b642c2376e8c57db8980486e", null ],
    [ "tx_ready", "unioncapi__lane__status__t.html#a215fbb02cfb5b0bdd6b93d9592d61cc3", null ],
    [ "cdrlock", "unioncapi__lane__status__t.html#a45f567be79116230a6f3f337d58c4170", null ],
    [ "linktrn_done", "unioncapi__lane__status__t.html#ad52d146a4056062aad4e77d356443869", null ],
    [ "linktrn_err", "unioncapi__lane__status__t.html#ac3067b35a81d00fc9adcbbe1476b21e1", null ],
    [ "lw", "unioncapi__lane__status__t.html#ad85fc447fac06103baae8f10f3fb89e2", null ],
    [ "pll_lock", "unioncapi__lane__status__t.html#a65667318a7c4b2fed9bd00d2c877c89c", null ],
    [ "cdr_lock", "unioncapi__lane__status__t.html#ad2b27bdd9c1b0e98cfb16462fa1568bc", null ],
    [ "pll_unlock_sticky", "unioncapi__lane__status__t.html#a6cdf68bc117922b66e1da36063fda140", null ],
    [ "bh2bh_fifo_status", "unioncapi__lane__status__t.html#aff8c7ef7b230570501afdbdbe24256e8", null ],
    [ "reserved", "unioncapi__lane__status__t.html#a0a4d22e552fcc38f22fe34ba1a4a7280", null ],
    [ "client", "unioncapi__lane__status__t.html#a0e39fe3d189aab33b53885434acc3a5a", null ]
];