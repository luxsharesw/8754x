var unioncommon__lw__lane__configa__reg__t =
[
    [ "words", "unioncommon__lw__lane__configa__reg__t.html#ad58ab7ebcbb6004a3a35ba1f86f32df1", null ],
    [ "ppm_coverge_en", "unioncommon__lw__lane__configa__reg__t.html#a6448c438ced144df724f5b287cdbc95a", null ],
    [ "berlinetta_compatible", "unioncommon__lw__lane__configa__reg__t.html#afbf892937dda84ecc21db78909eaf9da", null ],
    [ "lnktrn_auto_restart", "unioncommon__lw__lane__configa__reg__t.html#aa0f2513d1b4a9dc20c18557be1280c22", null ],
    [ "disable_lnktrn_bh_txfir_lmt", "unioncommon__lw__lane__configa__reg__t.html#a5fa934348a1fb5688718226a628f6911", null ],
    [ "enable_cl136_preset1", "unioncommon__lw__lane__configa__reg__t.html#a792e6b7b9bf07fb64b9ed678d7e17c87", null ],
    [ "enable_channelid_autopk", "unioncommon__lw__lane__configa__reg__t.html#a7e19daef822ab3327689670daed4e80b", null ],
    [ "enable_cl72_cl93_preset_req", "unioncommon__lw__lane__configa__reg__t.html#a538e32769e423ddd9a9014ae28ad3247", null ],
    [ "disable_rxlos_rst_tx", "unioncommon__lw__lane__configa__reg__t.html#a74e7d14155c3a83459248c0a4dd41403", null ],
    [ "disable_rxflt_rst_tx", "unioncommon__lw__lane__configa__reg__t.html#ab837b191a8a6956bfab5030b651bd5c0", null ],
    [ "enable_cu_pam4_kp_sweep", "unioncommon__lw__lane__configa__reg__t.html#a5206f27a3efaa7361849e37ba8e38a93", null ],
    [ "enable_pam_lnktrn_tx_amp_tune", "unioncommon__lw__lane__configa__reg__t.html#af664449d9e7f32fded4c0a4eef285cfe", null ],
    [ "enable_pam_lnktrn_slicer_auto_sel", "unioncommon__lw__lane__configa__reg__t.html#afe3387641fe036aee1b2141562226063", null ],
    [ "reserve", "unioncommon__lw__lane__configa__reg__t.html#aa54743ae4d90a744ffc4ee33c2a49f76", null ],
    [ "txpi_ppm_en", "unioncommon__lw__lane__configa__reg__t.html#a3a7db5fc200897d1f1b07ec3601b8e9f", null ],
    [ "copper_trn_en", "unioncommon__lw__lane__configa__reg__t.html#a824978c5c2f5bb7516d0322a315d96be", null ],
    [ "fields", "unioncommon__lw__lane__configa__reg__t.html#a9ed9d6bd42b064e9178a00663a179c78", null ]
];