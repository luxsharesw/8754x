var unioncommon__lw__lane__config15__reg__t =
[
    [ "words", "unioncommon__lw__lane__config15__reg__t.html#a901b1030104f50e8d8292dd49cd71123", null ],
    [ "lw_rx_analog_set_ovrd", "unioncommon__lw__lane__config15__reg__t.html#ade2037931cae297bf9091af6c9cc9059", null ],
    [ "lw_rx_analog_set", "unioncommon__lw__lane__config15__reg__t.html#ade2b5332e89627c6d5b149ba76dab2a3", null ],
    [ "lw_pam_slicer_autosel_th", "unioncommon__lw__lane__config15__reg__t.html#a570c04dd880bb9d6aa6b1877e733b4f7", null ],
    [ "lw_en_pam_ap_temp_dbg1", "unioncommon__lw__lane__config15__reg__t.html#a4e1e97d0d4ab5f5ea77d74f49a0b6d02", null ],
    [ "lw_en_pam_ap_temp_dbg2", "unioncommon__lw__lane__config15__reg__t.html#aa0c7aabc3a87f9dbbf2a06c682170472", null ],
    [ "lw_en_pam_ap_temp_dbg3", "unioncommon__lw__lane__config15__reg__t.html#aa88f488c0d31077d23b78c314d41d628", null ],
    [ "lw_ap_metric_pgac_th", "unioncommon__lw__lane__config15__reg__t.html#a4e74b9b79ded4a01cbea0e62c5783375", null ],
    [ "lw_b0_en_rx_atten_ovrd", "unioncommon__lw__lane__config15__reg__t.html#a69eeea970bc74069eaf3144b469c51d6", null ],
    [ "lw_b0_en_rx_atten", "unioncommon__lw__lane__config15__reg__t.html#a5d5910f4eb9c168627aa7b88c5e4c0fd", null ],
    [ "lw_en_pll_double", "unioncommon__lw__lane__config15__reg__t.html#a77e5e7ee04964882c5142cc21c3596af", null ],
    [ "fields", "unioncommon__lw__lane__config15__reg__t.html#a18234da557da76c37634a7e2df5657e2", null ]
];