var structcw__chip__port__info__for__mode__t =
[
    [ "cw_chip_mode_t", "structcw__chip__port__info__for__mode__t.html#a0d893b15e8c3500b702d7ea77ea492e3", null ],
    [ "cw_chip_port_mask", "structcw__chip__port__info__for__mode__t.html#a20a5dc51b206f74bd499dc82ed1d6669", null ],
    [ "cw_chip_bh_lane_mask", "structcw__chip__port__info__for__mode__t.html#a5e0402e29d33f4e76bb09622dab85159", null ],
    [ "cw_chip_lw_lane_mask", "structcw__chip__port__info__for__mode__t.html#a6270a9a4ff987e6722325c3f06981e3f", null ]
];