var structcapi__prbs__info__t =
[
    [ "ptype", "structcapi__prbs__info__t.html#ab6f6aa3d651a6fa6dc110580672f0640", null ],
    [ "gen_switch", "structcapi__prbs__info__t.html#ad7bf10147086b1bfd96d96926d7c069f", null ],
    [ "pcfg", "structcapi__prbs__info__t.html#a822157b6b690821cf484da7f999c404f", null ],
    [ "ssprq", "structcapi__prbs__info__t.html#ae2abbd9352e0398e544f8a619b3686c7", null ],
    [ "qprbs13", "structcapi__prbs__info__t.html#a8d9787f5d36f2de4ada9fecc8edb1956", null ],
    [ "sqr_wave", "structcapi__prbs__info__t.html#afff7b008e95903a79282926ffe287a77", null ],
    [ "shared_tx_ptrn", "structcapi__prbs__info__t.html#a1b4838e4321531b3040be6cdfcea01de", null ],
    [ "staircase_pat", "structcapi__prbs__info__t.html#a3d74e1d1073030042663607084567b53", null ],
    [ "op", "structcapi__prbs__info__t.html#a85a3baf0fae2b687d242cc7583219b53", null ]
];