var unionchip__top__fixed__voltage__config__reg__t =
[
    [ "words", "unionchip__top__fixed__voltage__config__reg__t.html#a7fab7d2dba302d0f0295d8cbe9e82085", null ],
    [ "fixed_enable", "unionchip__top__fixed__voltage__config__reg__t.html#a0eb4bebb2ff4dc7959134d2e0739f545", null ],
    [ "type_of_regulator", "unionchip__top__fixed__voltage__config__reg__t.html#a55768147bf77f5ee4426a160d3c535ac", null ],
    [ "set_status", "unionchip__top__fixed__voltage__config__reg__t.html#a45a3b6e770b09f2eeed4323ac2a23977", null ],
    [ "reason", "unionchip__top__fixed__voltage__config__reg__t.html#a21c0622355eb706bfd653221dee84eb1", null ],
    [ "digital_analog", "unionchip__top__fixed__voltage__config__reg__t.html#a89cde0a606323b633dc6fe07206cc9f1", null ],
    [ "reserved", "unionchip__top__fixed__voltage__config__reg__t.html#afb23306c67318ced3ab817712e910e45", null ],
    [ "fields", "unionchip__top__fixed__voltage__config__reg__t.html#a50f3a677e728410529c7e8bd6552f612", null ]
];