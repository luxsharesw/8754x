var structcapi__other__voltage__config__info__t =
[
    [ "fixed_voltage_enable", "structcapi__other__voltage__config__info__t.html#ab38f67f5e9ed789a497bbef031b76018", null ],
    [ "fixed_voltage_set", "structcapi__other__voltage__config__info__t.html#a2fcde962f65f0075c7d1135488ec2b13", null ],
    [ "fixed_voltage_read", "structcapi__other__voltage__config__info__t.html#a3479f8bc540945ac4b96debd4b9910f0", null ],
    [ "capi_set_voltage_with_fw", "structcapi__other__voltage__config__info__t.html#a3ad0055f533fc6059a025df550ca7e3d", null ]
];