var unioncommon__lw__lane__config13__reg__t =
[
    [ "words", "unioncommon__lw__lane__config13__reg__t.html#a6f2f38850866ef22dbc39ff4bf2f7686", null ],
    [ "lw_lt_pam_ap_dis_pgac_chk", "unioncommon__lw__lane__config13__reg__t.html#a29f8f794deb6f7b0948725a8c31238a7", null ],
    [ "lw_force_skew_on", "unioncommon__lw__lane__config13__reg__t.html#ab93a76f75e601cfd6f250d3a77f19ecb", null ],
    [ "lw_lt_pam_ap_adcclip_th", "unioncommon__lw__lane__config13__reg__t.html#a9072e42384eaf054331f9ce540927018", null ],
    [ "lw_en_los_adcdiv_rst", "unioncommon__lw__lane__config13__reg__t.html#a1ebd910cc592f5dc320e0e1aa6c4f68f", null ],
    [ "lw_cfg_nrzc_dcoff_skew_ctrl", "unioncommon__lw__lane__config13__reg__t.html#ac32cbb70e78f38554478425968341199", null ],
    [ "lw_en_rslicer_ffe_2", "unioncommon__lw__lane__config13__reg__t.html#aeca0cd0c2d5f023c7105d0f6ba5e87d6", null ],
    [ "lw_cfg_dpam_ap_adcclip_th", "unioncommon__lw__lane__config13__reg__t.html#ab02099574e8b878f8f69a3a6450670f7", null ],
    [ "lw_cfg_dpam_ap_adcclip_en", "unioncommon__lw__lane__config13__reg__t.html#adab1817e352c26bd9af70f72da2df10c", null ],
    [ "lw_cfg_dpam_pf_init_sweep_en", "unioncommon__lw__lane__config13__reg__t.html#a60dd234da525c350d193c254ec6a0c9f", null ],
    [ "lw_cfg_nrz_ap_coef_th", "unioncommon__lw__lane__config13__reg__t.html#a637a7fb099ad3cce45a67c5f2576ed83", null ],
    [ "fields", "unioncommon__lw__lane__config13__reg__t.html#adc9b2fd3129142b8c66756787547f873", null ]
];