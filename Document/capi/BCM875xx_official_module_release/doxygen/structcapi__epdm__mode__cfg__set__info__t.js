var structcapi__epdm__mode__cfg__set__info__t =
[
    [ "lane_mask", "structcapi__epdm__mode__cfg__set__info__t.html#a97a9977200b3743e406bf22ded714f7f", null ],
    [ "lane_data_rate", "structcapi__epdm__mode__cfg__set__info__t.html#a60e8640166fea2484faac2c6adc5a135", null ],
    [ "media_type", "structcapi__epdm__mode__cfg__set__info__t.html#a7cdc1107def00299a0278445778a8750", null ],
    [ "lane_fec_type", "structcapi__epdm__mode__cfg__set__info__t.html#a9e45d4b70478d7a8c04d688df5726b3b", null ],
    [ "lane_modulation", "structcapi__epdm__mode__cfg__set__info__t.html#abda3506ed91884dbee89bf68a5850960", null ],
    [ "hitless_mux_mode", "structcapi__epdm__mode__cfg__set__info__t.html#a4c3383b520439a741476d58146f8e0b7", null ],
    [ "hybrid_port_mode", "structcapi__epdm__mode__cfg__set__info__t.html#a9fbad463461ee4884511558c10758758", null ]
];