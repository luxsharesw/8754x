var structlink__training__status__t =
[
    [ "enabled", "structlink__training__status__t.html#a7fc62711ecd514c5e8bf8372af152822", null ],
    [ "signal_detect", "structlink__training__status__t.html#a78a09bef658e47c1b313bd2553ce8069", null ],
    [ "failure_detected", "structlink__training__status__t.html#ad4aa2170a09fad755379b507b9b6aa39", null ],
    [ "training_status", "structlink__training__status__t.html#a319d333d85f09b0e17c1d5f57987ba4e", null ],
    [ "receiver_status", "structlink__training__status__t.html#ad178b12f841208197b40edb8356e054f", null ],
    [ "receiver_lock", "structlink__training__status__t.html#a6c6e55c469515ca32a6585319e373e05", null ],
    [ "frame_lock", "structlink__training__status__t.html#aa5bc1fce0332940178dde575169d19c7", null ]
];