var structcw__port__config__t =
[
    [ "port_1g_en", "structcw__port__config__t.html#a6d34bc2bca332e12b07aaedf46509d33", null ],
    [ "port_20g_en", "structcw__port__config__t.html#a0be256b23dff2a4401943d005ba300bc", null ],
    [ "port_40g_en", "structcw__port__config__t.html#a295aa969b7363dc342eeab0c0f8f42d0", null ],
    [ "port_25g_en", "structcw__port__config__t.html#af46f7d0473da609eabebbc301910669e", null ],
    [ "port_50g_en", "structcw__port__config__t.html#a2af87564a462b281b6c5acf14c22f382", null ],
    [ "port_100g_en", "structcw__port__config__t.html#a9cc2eff8ae065de64d2cfe10148a6492", null ],
    [ "port_200g_en", "structcw__port__config__t.html#afed23cc32a4654ebd5443e8dd2f87b86", null ],
    [ "port_400g_en", "structcw__port__config__t.html#ab4fb0df14c638ccc2fbc2227a0e94d38", null ]
];