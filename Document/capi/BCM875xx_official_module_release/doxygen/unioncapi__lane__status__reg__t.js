var unioncapi__lane__status__reg__t =
[
    [ "content", "unioncapi__lane__status__reg__t.html#a826ae131b67687b6cdf04676e791289c", null ],
    [ "pll_lock", "unioncapi__lane__status__reg__t.html#a16d285cecf10cb7238256a6b10e00db6", null ],
    [ "los", "unioncapi__lane__status__reg__t.html#a62f187808f16b01fb199b4379daee7e9", null ],
    [ "adc_clip_detect", "unioncapi__lane__status__reg__t.html#acd52e8b3c321a4e320f7d33d0cde6498", null ],
    [ "cdr_lock", "unioncapi__lane__status__reg__t.html#ad1c8d3f2b0f093813441336b4178258c", null ],
    [ "linktrn_done", "unioncapi__lane__status__reg__t.html#acdb59d0b239023576921c961af067d37", null ],
    [ "linktrn_error_code", "unioncapi__lane__status__reg__t.html#a01758074cace995053370b5b98425469", null ],
    [ "error_code", "unioncapi__lane__status__reg__t.html#aaf18ec8fdf240e87ee1c6c394887df0e", null ],
    [ "line_lock", "unioncapi__lane__status__reg__t.html#a2d029949089d1602da1b983f5954bf2e", null ],
    [ "command_accepted", "unioncapi__lane__status__reg__t.html#a6c978ce773591a7bd74c1296c26ae018", null ],
    [ "lw", "unioncapi__lane__status__reg__t.html#a19c5b4f0443c38a1eb1c4e5d5d5c68a8", null ],
    [ "bh2bh_fifo_status", "unioncapi__lane__status__reg__t.html#a533ec5baed751b383914d758935a76c7", null ],
    [ "pll_unlock_sticky", "unioncapi__lane__status__reg__t.html#ad9996a66075cc1622382f5f90ca3e252", null ],
    [ "reserved2", "unioncapi__lane__status__reg__t.html#a155c2db1825ce3b674985ef2e67f80ee", null ],
    [ "client", "unioncapi__lane__status__reg__t.html#a26127ab1c6882e4ad6c6d4955ba12f62", null ]
];