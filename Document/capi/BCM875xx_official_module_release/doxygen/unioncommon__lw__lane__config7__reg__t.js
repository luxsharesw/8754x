var unioncommon__lw__lane__config7__reg__t =
[
    [ "words", "unioncommon__lw__lane__config7__reg__t.html#ae14f22c97ea062d53745aedc7af792da", null ],
    [ "lw_phbias_min", "unioncommon__lw__lane__config7__reg__t.html#af6543c7dac24f90c6be51b9a28ad9172", null ],
    [ "lw_phbias_max", "unioncommon__lw__lane__config7__reg__t.html#afe6d6fa5afefa846f1026af3e2f37f20", null ],
    [ "lw_disable_s_phbias", "unioncommon__lw__lane__config7__reg__t.html#a1c7cbfc73ca975b808b1a73bc6015fa9", null ],
    [ "lw_disable_d_phbias", "unioncommon__lw__lane__config7__reg__t.html#a347fbf1d32ea4106336d360586880b40", null ],
    [ "lw_phbias_step_size", "unioncommon__lw__lane__config7__reg__t.html#a497ece9ff0d8b113edfcd91a49e43556", null ],
    [ "reserved", "unioncommon__lw__lane__config7__reg__t.html#a3a6da589369c49c6b82d72df28d6308e", null ],
    [ "fields", "unioncommon__lw__lane__config7__reg__t.html#a4616577c2c2896c2c36c470cf65f4297", null ]
];