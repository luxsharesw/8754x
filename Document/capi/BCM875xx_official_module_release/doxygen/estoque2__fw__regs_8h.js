var estoque2__fw__regs_8h =
[
    [ "chip_top_chip_temp_reading_reg_t", "unionchip__top__chip__temp__reading__reg__t.html", "unionchip__top__chip__temp__reading__reg__t" ],
    [ "chip_top_chip_volt_reading_reg_t", "unionchip__top__chip__volt__reading__reg__t.html", "unionchip__top__chip__volt__reading__reg__t" ],
    [ "chip_top_pre_load_config_reg_t", "unionchip__top__pre__load__config__reg__t.html", "unionchip__top__pre__load__config__reg__t" ],
    [ "chip_top_pre_load_config2_reg_t", "unionchip__top__pre__load__config2__reg__t.html", "unionchip__top__pre__load__config2__reg__t" ],
    [ "chip_top_avs_mode_config_reg_t", "unionchip__top__avs__mode__config__reg__t.html", "unionchip__top__avs__mode__config__reg__t" ],
    [ "chip_top_fixed_voltage_config_reg_t", "unionchip__top__fixed__voltage__config__reg__t.html", "unionchip__top__fixed__voltage__config__reg__t" ],
    [ "CHIP_TOP_CHIP_REFCLK_CONFIG_REG_REF_CLK_FREQ_SELECT_MASK", "estoque2__fw__regs_8h.html#a4b1d3224ecf72b506d8750185cb40f19", null ],
    [ "CHIP_TOP_CHIP_REFCLK_CONFIG_REG_REF_CLK_FREQ_SELECT_SHIFT", "estoque2__fw__regs_8h.html#a8af0bb06d894ba87294bf567a226f5ce", null ]
];