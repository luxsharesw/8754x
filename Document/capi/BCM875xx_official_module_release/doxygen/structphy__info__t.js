var structphy__info__t =
[
    [ "phy_id", "structphy__info__t.html#affb8dc2a12bfa4329ef74f29f049981e", null ],
    [ "core_ip", "structphy__info__t.html#a731d4a8c50ca7406d898f7f75233c28d", null ],
    [ "lane_mask", "structphy__info__t.html#ada56f7d8a23cbff4394617205697b9fd", null ],
    [ "ref_ptr", "structphy__info__t.html#aa10ed88158874f091ccd1c6f9eb8bb05", null ],
    [ "base_addr", "structphy__info__t.html#afe937add216d40eb256de6bd857817d9", null ],
    [ "chip_id", "structphy__info__t.html#a136a49cead281226a6fd2fe8f7dc9993", null ],
    [ "i2c_address", "structphy__info__t.html#a5d13fb08c4ac068772ba923d45f9c11c", null ],
    [ "i2c_block_write_size", "structphy__info__t.html#a098f7093166e2b24be9e796708b338f0", null ]
];