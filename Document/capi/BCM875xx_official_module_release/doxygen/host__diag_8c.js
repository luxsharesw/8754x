var host__diag_8c =
[
    [ "host_util_get_lw_phy_info", "host__diag_8c.html#a0d3dafc3497d554ee7673ed64876eb0c", null ],
    [ "host_util_get_lane_lw_top_pam_bbaddr", "host__diag_8c.html#aabf2319f95d32b0eed49fdf4769ac334", null ],
    [ "host_lw_get_smode", "host__diag_8c.html#a61aeb5e14db44e23adb6fe7548c3086f", null ],
    [ "host_lw_get_snr_wo_suspend_resume", "host__diag_8c.html#a49b4424e1aeb2e9ef5488d88716b504b", null ],
    [ "host_lw_get_snr", "host__diag_8c.html#a7e97c2b8dfb7c42afc8fc794f42389f7", null ],
    [ "host_lw_get_lvl_snr", "host__diag_8c.html#a0484cb11c9bf6d3e74bcf417e1046390", null ],
    [ "host_lw_get_snr_from_gp", "host__diag_8c.html#ad3560f77235918a31aafcf568d151ec0", null ],
    [ "host_lw_get_asnr_from_gp", "host__diag_8c.html#afe87165284d98ad8216b8ea79d6fafb4", null ],
    [ "host_lw_get_usr_diagnostics", "host__diag_8c.html#a8e876000376cc0aa24045af05af7c44b", null ],
    [ "host_lw_get_usr_cmis_diagnostics", "host__diag_8c.html#a98d4b8ee6509cdd2f96c2e3059130118", null ],
    [ "host_client_get_cmis_values", "host__diag_8c.html#ad7396607e3bbbaf8600a646ddc53d148", null ],
    [ "host_client_get_usr_cmis_diagnostics", "host__diag_8c.html#a9c97378756614bd41cd75fea8897d4f7", null ],
    [ "host_dsp_get_slicer_histogram_diagnostics", "host__diag_8c.html#acc31e427f8678ebad9b6509a6d59343b", null ],
    [ "host_set_mpi_config", "host__diag_8c.html#a90d8155451d684064d678f39bd29bc89", null ],
    [ "host_get_mpi_config", "host__diag_8c.html#a76f0f7d3a5d316bd7c1aea18f506e9dd", null ],
    [ "host_get_mpi_state", "host__diag_8c.html#a52daf4a6076736488004ce6934095dbc", null ],
    [ "host_set_mpi_dynamic_config", "host__diag_8c.html#a7462fbf930bafe77b9adf0e98437a611", null ],
    [ "host_get_mpi_dynamic_config", "host__diag_8c.html#a2151124a04af8746eaee73c8f6acecba", null ],
    [ "host_get_dynamic_mpi_state", "host__diag_8c.html#a24c655a347c19dc8ed815e7b9446fdf6", null ],
    [ "client_cmis_lut", "host__diag_8c.html#a3ebb9958c31a04650c817b0ef88b5035", null ],
    [ "line_lane_bbaddr", "host__diag_8c.html#a0cf572a68c4d888b53d9981714cf0615", null ]
];