var structcapi__download__info__t =
[
    [ "status", "structcapi__download__info__t.html#aeabafd138c320e4c6f9c1498c75c6be9", null ],
    [ "mode", "structcapi__download__info__t.html#a87c07c7a7a53e0849bb6d1241e2204f4", null ],
    [ "phy_static_config_ptr", "structcapi__download__info__t.html#a7bd02d8b2b37246d7e3e03625f2086d5", null ],
    [ "image_info", "structcapi__download__info__t.html#ab4d01c35c3741621ed66b09525965c54", null ],
    [ "spi_program_mode", "structcapi__download__info__t.html#a9e069ae79d6e98766ab848950fd36165", null ],
    [ "burst_write_mode", "structcapi__download__info__t.html#a8769a743e1948e9e313ffa3e92bcba21", null ],
    [ "sram_program_mode", "structcapi__download__info__t.html#ae551b2e3e0598e2fcecc9b8969c6d465", null ]
];