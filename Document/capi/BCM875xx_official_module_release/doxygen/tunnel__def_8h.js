var tunnel__def_8h =
[
    [ "TUNNELING_ACCESS", "tunnel__def_8h.html#a7b9ccd7efe97d518434b12d5acd2c041", null ],
    [ "tunnel_return_result_t", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39", [
      [ "TUNNEL_RR_ERROR", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39a28b7a53d35207654bb2a9da862e98220", null ],
      [ "TUNNEL_WRITE_WAIT", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39aa25fba18cfe10cfafe312c694e1ce286", null ],
      [ "TUNNEL_WRITE_FAILED", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39a80e505b15b2e74e60484550f195533d7", null ],
      [ "TUNNEL_READ_WAIT", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39a3447be41293b4142dd90ec2126864989", null ],
      [ "TUNNEL_READ_FAILED", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39aa024d87f264a70c22889d4dbb7c334aa", null ],
      [ "TUNNEL_RR_SUCCESS", "tunnel__def_8h.html#a4b6d86a66d5ac931d1938176cc53ce39af521f0e58470beea4c5573ca0270ac94", null ]
    ] ],
    [ "tunnel_b_write", "tunnel__def_8h.html#ae5059362e6a1e6066f644eef963e300f", null ],
    [ "tunnel_write_request", "tunnel__def_8h.html#a91b8ba440349fe3f54a24e53f31cf11e", null ],
    [ "tunnel_write_poll_ack", "tunnel__def_8h.html#a4290d94bbc08230f0134254a84914793", null ],
    [ "tunnel_b_read", "tunnel__def_8h.html#ab75c0634a732f85aa6d941bd7df6ec6e", null ],
    [ "tunnel_read_request", "tunnel__def_8h.html#a890f7fe91272124f74a5854ec87885ba", null ],
    [ "tunnel_read_poll_ack", "tunnel__def_8h.html#aaf6bde2ce4af3a05f5fbdc0cf71be2f4", null ]
];