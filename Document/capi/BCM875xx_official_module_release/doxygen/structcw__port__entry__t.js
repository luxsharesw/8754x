var structcw__port__entry__t =
[
    [ "port_type", "structcw__port__entry__t.html#aed52fba7025c689b25d76dd9f3038cb7", null ],
    [ "func_mode", "structcw__port__entry__t.html#ade47915173df99b1ec96a701a9c8e030", null ],
    [ "fec_term", "structcw__port__entry__t.html#acaa07e297ff09c9b3626101c1ef261f9", null ],
    [ "host_fec", "structcw__port__entry__t.html#a7d887079f3ecf6c29054637b55d18417", null ],
    [ "line_fec", "structcw__port__entry__t.html#aca38a88c1c5143ce777b5b1498013b2f", null ],
    [ "bh_cfg", "structcw__port__entry__t.html#a0a71ba1288c0ae7e50721598e555c940", null ],
    [ "lw_cfg", "structcw__port__entry__t.html#ae3c193394e5f10184d49037ea6a51540", null ],
    [ "port_mux", "structcw__port__entry__t.html#a6d55590b3d11fd4edb9c5cdef9f10d22", null ]
];