var unionlw__mode__reg__t =
[
    [ "content", "unionlw__mode__reg__t.html#ad26640881e1877d2edcd38a96666046f", null ],
    [ "auto_neg", "unionlw__mode__reg__t.html#affd97d9157e30cb5250f80b9689c5e35", null ],
    [ "link_training", "unionlw__mode__reg__t.html#a45f336a2a04702461da9c0d80d84171b", null ],
    [ "g_loop", "unionlw__mode__reg__t.html#aafc0917e9b4c01154d2ab8883e021070", null ],
    [ "r_loop", "unionlw__mode__reg__t.html#a035ad8e3838616ca0c3ad2cdf4c5f52d", null ],
    [ "extended_mode", "unionlw__mode__reg__t.html#acac4d72482993de4a8441a0be8eed49f", null ],
    [ "dfe", "unionlw__mode__reg__t.html#a097bf26b5729f3259304f5b63891b693", null ],
    [ "nldet", "unionlw__mode__reg__t.html#a7046096ffb1e02e46fa397e1bb6ccf83", null ],
    [ "eqpp", "unionlw__mode__reg__t.html#aaee8d7ce6cff7b0c9710b574f9a68419", null ],
    [ "eq_tap_sel", "unionlw__mode__reg__t.html#afe2894ecc802b420175603c3cca1d7c1", null ],
    [ "phd_mode", "unionlw__mode__reg__t.html#aa40407cbc2cd1873914482dfa5437528", null ],
    [ "optical_mode", "unionlw__mode__reg__t.html#a704eb5cbb7cee8cc1be67dfce4268025", null ],
    [ "is", "unionlw__mode__reg__t.html#a6c1e74a56494291f2bf9712772b33fd0", null ]
];