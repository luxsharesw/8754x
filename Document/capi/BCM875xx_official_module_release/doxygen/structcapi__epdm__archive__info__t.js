var structcapi__epdm__archive__info__t =
[
    [ "content", "structcapi__epdm__archive__info__t.html#a6ca487cc8fe3e669c0d591aaba2d0673", null ],
    [ "buffer", "structcapi__epdm__archive__info__t.html#ade959cee5851c99fd174a17d6a231699", null ],
    [ "mode_config_info", "structcapi__epdm__archive__info__t.html#a69e53f9287dfb2dc5de243d31229f563", null ],
    [ "line_intf_type", "structcapi__epdm__archive__info__t.html#af9a3e257d74e59f8a7af1938e7d943fa", null ],
    [ "sys_intf_type", "structcapi__epdm__archive__info__t.html#a48cf87c52fc894406f808160820e6375", null ],
    [ "line_prbs_cfg_rx", "structcapi__epdm__archive__info__t.html#a4c108a13f7b6f5b6b679409170171a62", null ],
    [ "line_prbs_cfg_tx", "structcapi__epdm__archive__info__t.html#aa696f1b4bf98c781fde362e6bd586e0c", null ],
    [ "sys_prbs_cfg_rx", "structcapi__epdm__archive__info__t.html#a840a43210d9d15b6cc97c3bbb01ee1e8", null ],
    [ "sys_prbs_cfg_tx", "structcapi__epdm__archive__info__t.html#af2a85f4a028840fda9f6c29d7aac28f5", null ],
    [ "reserved", "structcapi__epdm__archive__info__t.html#ae260328b91361f096e82f17cefbf3ba6", null ],
    [ "is", "structcapi__epdm__archive__info__t.html#aacc1a945ab543be3da33b904da79e5df", null ],
    [ "archive", "structcapi__epdm__archive__info__t.html#a267bc126cb98dd815e6f58d29d24d9c6", null ],
    [ "sys_prbs_cfg_tx", "structcapi__epdm__archive__info__t.html#a45e43cc5ad607bc718d97f55ce868ac5", null ],
    [ "sys_prbs_cfg_rx", "structcapi__epdm__archive__info__t.html#ab5c9c0f2da07e3906d0e4f42b89bef31", null ],
    [ "line_prbs_cfg_tx", "structcapi__epdm__archive__info__t.html#a05ab6fbfc67aa8194517a727cc6456f1", null ],
    [ "line_prbs_cfg_rx", "structcapi__epdm__archive__info__t.html#a817df4f18c8fecdefc3cc40b05f2df51", null ],
    [ "sys_interface_type", "structcapi__epdm__archive__info__t.html#a88367a8bb009cdc1283036ff9048d8ca", null ],
    [ "line_interface_type", "structcapi__epdm__archive__info__t.html#aaec40e3ad1432db50bb5743f50d41e83", null ],
    [ "mode_config_info", "structcapi__epdm__archive__info__t.html#a58407bfd4771f28e9bc7af56ab47ae7b", null ],
    [ "buffer", "structcapi__epdm__archive__info__t.html#a7b80a888428643966953d4579109108e", null ],
    [ "value", "structcapi__epdm__archive__info__t.html#a192579d33b44840c7b976be9517ad9b6", null ]
];