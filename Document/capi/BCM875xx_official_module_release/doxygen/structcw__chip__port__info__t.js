var structcw__chip__port__info__t =
[
    [ "cw_chip_port_mask", "structcw__chip__port__info__t.html#af012313f43513d1a018af8d093f3d98c", null ],
    [ "cw_chip_bh_lane_mask", "structcw__chip__port__info__t.html#a6365197108db28b6bc3970c940deaae8", null ],
    [ "cw_chip_lw_lane_mask", "structcw__chip__port__info__t.html#a8348e58955943ddecab490d624278c45", null ],
    [ "cw_chip_serdes_mask", "structcw__chip__port__info__t.html#a0c410b159fa79d532075e22f3343424b", null ],
    [ "cw_chip_media_mask", "structcw__chip__port__info__t.html#afc00b414c1ba39a58847a145a025460d", null ]
];