var structcapi__diag__lane__status__t =
[
    [ "content", "structcapi__diag__lane__status__t.html#ae65a6a4bc14f2c9e894254bfe9024565", null ],
    [ "dc_offset", "structcapi__diag__lane__status__t.html#ac8aa7a49e5d3b90bfe98efd240dc1af7", null ],
    [ "eye_margin_estimate", "structcapi__diag__lane__status__t.html#aedda9e47d9858acb9379a4cf7d3966cd", null ],
    [ "reserved", "structcapi__diag__lane__status__t.html#a7d468358f8f2ca0989c987d4886dead0", null ],
    [ "is", "structcapi__diag__lane__status__t.html#a92cc8329571ba981a611dbc2ecc05568", null ],
    [ "param", "structcapi__diag__lane__status__t.html#a791cbd44bdba738b9d5c6569637edb80", null ],
    [ "eye_margin_est", "structcapi__diag__lane__status__t.html#a661e7f8fa4c555171f724ca77ed81cb6", null ],
    [ "dc_offset", "structcapi__diag__lane__status__t.html#a8177b66c4fb55d3bc6ad9d4be173a58c", null ],
    [ "payload", "structcapi__diag__lane__status__t.html#adbf86f2a954d2c9ff7ca52de911023f3", null ],
    [ "value", "structcapi__diag__lane__status__t.html#a3edf3fc6a181c086ec3a5abcb96bef73", null ]
];