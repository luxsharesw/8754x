var structcapi__chip__status__info__t =
[
    [ "content", "structcapi__chip__status__info__t.html#a9fb3edca25c218f4fd28379bc215ce43", null ],
    [ "uc_ready", "structcapi__chip__status__info__t.html#a4c35d091a2fb0331753bfb812c25b04f", null ],
    [ "avs_done", "structcapi__chip__status__info__t.html#ad1045da09952c3673c01ceadb4717b5c", null ],
    [ "download_done", "structcapi__chip__status__info__t.html#a43c9ff86d892024cccd14bcb522a68ca", null ],
    [ "spi_dev_ready", "structcapi__chip__status__info__t.html#a16163ea09728749a233a6559bd49b8a9", null ],
    [ "reserved", "structcapi__chip__status__info__t.html#a476023092e9c01da1dd9bf8249b07cc1", null ],
    [ "is", "structcapi__chip__status__info__t.html#a2edcc2e224caa8ba255c726c8693e45b", null ],
    [ "param", "structcapi__chip__status__info__t.html#a92a030c6d8febe197d4eff715e8b5a56", null ],
    [ "value", "structcapi__chip__status__info__t.html#af430c17069782b5b8155d7ebb269663c", null ]
];