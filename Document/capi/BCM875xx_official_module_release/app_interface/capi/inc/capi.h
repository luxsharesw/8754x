/**
 *
 * @file       capi.h
 * @author     PLP Firmware Team
 * @date       03/15/2020
 * @version    1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 *             Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief      The CAPI files contain all the CAPIs used by the the module and line card products.
 *
 */

#ifndef CAPI_H
#define CAPI_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief       capi_read_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr)
 * @details     This API is used to read register/s
 *
 * @param[in]   phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[i/o]  reg_info_ptr: When calling, give the 32 bit reg_address element defined in the
                              capi_reg_info_t and return content element defined in  the capi_reg_info_t
 * 
 * @return      returns the result of the called method/function
 */
return_result_t capi_read_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr);

/**
 * @brief       capi_write_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr)
 * @details     This API is used to write register/s
 *
 * @param[in]   phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]   reg_info_ptr: This pointer contains the information to write to the register
 *                            give the 32 bit reg_address element defined in  the capi_reg_info_t
 *                            and register value, content element defined in  the capi_reg_info_t
 * 
 * @return      returns the result of the called method/function
 */
return_result_t capi_write_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr);

/**
 * @brief      capi_get_chip_info(capi_phy_info_t* phy_info_ptr, capi_chip_info_t* chip_info_ptr)
 * @details    This API is used to get the Chip information
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] chip_info_ptr: this pointer contains chip info defined by capi_chip_info_t, which has 
 *                            chip_id and chip_revision
 * 
 * @return     returns the result of the called method/function, RR_SUCCESS
 */
return_result_t capi_get_chip_info(capi_phy_info_t* phy_info_ptr, capi_chip_info_t* chip_info_ptr);

/**
* @brief        capi_get_chip_status(capi_phy_info_t*         phy_info_ptr,
                                     capi_chip_status_info_t* chip_status_info_ptr)
* @details      This API is used to get the chip status
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    chip_status_info_ptr: pointer to capi_chip_status_info_t
*
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_chip_status(capi_phy_info_t*         phy_info_ptr,
                                     capi_chip_status_info_t* chip_status_info_ptr);



/**
 * @brief       capi_set_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
 * @details     This API is used to set the Host side or Media side RX and TX polarity swap
 *
 * @param[in]   phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]   polarity_info_ptr: this pointer carries the necessary information to set polarity
 * 
 * @return      returns the performance result of the called method/function
 */
return_result_t capi_set_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr);

/**
 * @brief       capi_get_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
 * @details     This API is used to get the Host side or Media side RX or TX polarity swap
 *
 * @param[in]   phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out]  polarity_info_ptr: this pointer return polarity information
 * 
 * @return      returns the performance result of the called method/function
 */
return_result_t capi_get_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr);

/**
 * @brief      capi_set_lane_config_info(capi_phy_info_t*         phy_info_ptr,
 *                                       capi_lane_config_info_t* lane_config_info_ptr)
 * @details    This API is used to set lane config info parameters
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  lane_config_info_ptr: a pointer which holds lane config parameters to be set
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_set_lane_config_info(capi_phy_info_t*         phy_info_ptr,
                                          capi_lane_config_info_t* lane_config_info_ptr);

/**
 * @brief      capi_get_lane_config_info(capi_phy_info_t*         phy_info_ptr,
 *                                       capi_lane_config_info_t* lane_config_info_ptr)
 * @details    This API is used to get lane config parameter value from Client side or Line side
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] lane_config_info_ptr: a pointer which holds lane config parameters read back from Client or Line
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_get_lane_config_info(capi_phy_info_t*         phy_info_ptr,
                                          capi_lane_config_info_t* lane_config_info_ptr);

/**
 * @brief      capi_set_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
 * @details    This API is used to set lane control of Client or Line side<BR>
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  lane_ctrl_info_ptr: a pointer of lane control command
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_set_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr);

/**
 * @brief      capi_get_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
 * @details    This API is used to get lane control status of Client or Line side
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] lane_ctrl_info_ptr: a pointer of lane control status
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_get_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr);

/**
* @brief        capi_get_lane_info(capi_phy_info_t* phy_info_ptr, capi_lane_info_t* lane_info_ptr)
* @details      This API is used to get the lane info
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   lane_info_ptr: this pointer return lane status, refer to capi_lane_info_t
*
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_lane_info(capi_phy_info_t* phy_info_ptr, capi_lane_info_t* lane_info_ptr);

/**
 * @brief      capi_set_config(capi_phy_info_t* phy_info_ptr, capi_config_info_t* config_info_ptr)
 * @details    This API is used to set the chip mode configuration information including:
 *             Note: This function is used for whole lane/whole chip per chip feature
 *             ref_clk:   Reference clock
 *             mode_rate: Data rate (please use the client side mode rate if client and line side rate mode is different)
 *             fec_mode:  FEC mode
 *             func_mode: Function Mode
 *             Please call this function before all other configuration function;
 *             Refer to design document for each parameter detailed value assignment
 *             and make sure the parameter combination are valid
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  config_info_ptr: this pointer carries the necessary information for configuration procedure
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_set_config(capi_phy_info_t* phy_info_ptr, capi_config_info_t* config_info_ptr);

/**
* @brief      capi_get_config(capi_phy_info_t* phy_info_ptr, capi_config_info_t* config_info_ptr)
* @details    This API is used to get the configuration information
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out] config_info_ptr: this pointer return configuration information
* 
* @return     returns the performance result of the called method/function
*/
return_result_t capi_get_config(capi_phy_info_t* phy_info_ptr, capi_config_info_t* config_info_ptr);

/**
 * @brief      capi_command_request(capi_phy_info_t*     phy_info_ptr,
 *                                  capi_command_info_t* cmd_inf_ptr,
 *                                  uint16_t             payload_size,
 *                                  uint8_t              config_cmd)
 * @details    This API is used to send command to interface.
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  cmd_inf_ptr: a pointer which carries command 
 * @param[in]  payload_size: size of the payload type
 * @param[in]  config_cmd: the command configuration flag for set or get configuration
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_command_request(capi_phy_info_t*     phy_info_ptr,
                                     capi_command_info_t* cmd_inf_ptr,
                                     uint16_t             payload_size,
                                     uint8_t              config_cmd);


/************************************************************************************************************************/
/****************************************************Download Related CAPIs**********************************************/
/************************************************************************************************************************/

/**
 * @brief    capi_reset(capi_phy_info_t* phy_info_ptr, capi_reset_mode_t* reset_mode_ptr)
 * @details  This API is used to reset the chip 
 *           for example:
 *               capi_phy_info_t capi_phy;
 *               capi_reset_mode_t reset_mode;
 *               reset_mode = CAPI_HARD_RESET_MODE;
 *               ret = capi_reset(&capi_phy, &reset_mode);
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  reset_mode_ptr: this pointer carries the necessary information for reseting procedure
 *                             defined by capi_reset_mode_t
 * 
 * @return     returns the  result of the called method/function
 */
return_result_t capi_reset(capi_phy_info_t* phy_info_ptr, capi_reset_mode_t* reset_mode_ptr);

/**
 * @brief      capi_download(capi_phy_info_t* phy_info_ptr,
 *                           capi_download_info_t* download_info_ptr)
 * @details    This API is used to download firmware , require to include  "whole_image_sram.h" for SRAM images.
 *             SPI EEPROM download will support later, in order to compile this CAPI, need to include both 
 *              "whole_image_sram.h" and "whole_image_spi.h" files
 *             for example:   
 *             #include "whole_image_sram.h"
 *               #include "whole_image_spi.h"
 *             capi_phy_info_t capi_phy;
 *             capi_download_info_t download_info;
 *             download_info.mode = CAPI_DOWNLOAD_MODE_I2C_SRAM;
 *             capi_download(&capi_phy, &download_info);
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  download_info_ptr: This pointer did not return download status because this API only download firmware
 *             please use capi_get_download_status to get download status.
 * 
 * @return     returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
 */
return_result_t capi_download(capi_phy_info_t*      phy_info_ptr,
                              capi_download_info_t* download_info_ptr);

/**
 * @brief      capi_pre_sram_download(capi_phy_info_t*  phy_info_ptr)
 * @details    This API is used to call before calling capi_download for SRAM 
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 *
 * @return     returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
 */
return_result_t capi_pre_sram_download(capi_phy_info_t*  phy_info_ptr);

/**
 * @brief    capi_get_download_status(capi_phy_info_t*      phy_info_ptr,
 *                                    capi_download_info_t* download_info_ptr)
 * @details  This API is used to get firmware download status
 *
 *           for example: 
 *               capi_phy_info_t capi_phy;
 *               capi_download_info_t download_info;
 *               download_info.mode = CAPI_DOWNLOAD_MODE_I2C_SRAM;
 *               capi_get_download_status(&capi_phy, &download_info);
 *               dprintf("crc check:%x, status result:%x, version:%x \n", 
 *                       download_info.status.crc_check, 
 *                       download_info.status.result, 
 *                       download_info.status.version);
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  download_info_ptr: this pointer carries the necessary information for downloading status
 *                                defined by capi_download_info_t
 * 
 * @return     returns the  result of the called method/function, either RR_ERROR or RR_SUCCESS
 */
return_result_t capi_get_download_status(capi_phy_info_t*      phy_info_ptr,
                                         capi_download_info_t* download_info_ptr);

/**
 * @brief       capi_get_firmware_status(capi_phy_info_t* phy_info_ptr, capi_status_info_t* status_info_ptr)
 * @details     This API is used to get firmware status
 *
 * @param[in]   phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out]  status_info_ptr: this pointer retuns the firmware status info defined by capi_status_info_t
 * 
 * @return      returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
 */
return_result_t capi_get_firmware_status(capi_phy_info_t* phy_info_ptr, capi_status_info_t* status_info_ptr);


return_result_t capi_set_broadcast(capi_phy_info_t*       phy_info_ptr,
                                   capi_broadcast_intf_t* broadcast_inft_ptr);

broadcast_enable_t capi_get_broadcast(capi_phy_info_t* phy_info_ptr);

/************************************************************************************************************************/
/*******************************************************AVS Related CAPIs************************************************/
/************************************************************************************************************************/

/**
 * @brief      capi_get_temperture_status(capi_phy_info_t*         phy_info_ptr,
 *                                        capi_temp_status_info_t* temp_status_info_ptr)
 * @details    This API is used obtain the temperature in degree C
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  capi_temp_status_info_ptr: this parameter pointed to the temperature info defined by
 *                                        capi_temp_status_info_t
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_get_temperture_status(capi_phy_info_t*         phy_info_ptr,
                                           capi_temp_status_info_t* capi_temp_status_info_ptr);

/**
 * @brief    capi_set_avs_voltage(capi_phy_info_t*         phy_info_ptr,
 *                                capi_avs_voltage_info_t* avs_voltage_info_ptr)
 * @details  This API is used to set the AVS configuration
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  avs_voltage_info_ptr: this pointer contains AVS voltage information
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_set_avs_voltage(capi_phy_info_t*         phy_info_ptr,
                                     capi_avs_voltage_info_t* avs_voltage_info_ptr);

/**
 * @brief    capi_set_avs_config(capi_phy_info_t*             phy_info_ptr,
 *                               capi_avs_mode_config_info_t* avs_config_ptr)
 * @details  This API is used to set the AVS configuration
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  avs_config_ptr: this pointer contains AVS mode configuration defined by capi_avs_mode_config_info_t
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_set_avs_config(capi_phy_info_t*             phy_info_ptr,
                                    capi_avs_mode_config_info_t* avs_config_ptr);

/**
 * @brief    capi_get_avs_config(capi_phy_info_t*             phy_info_ptr,
 *                               capi_avs_mode_config_info_t* avs_config_ptr)
 * @details  This API is used to set the AVS configuration
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  avs_config_ptr: this pointer contains AVS mode configuration defined by 
 *                                   capi_avs_mode_config_info_t
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_get_avs_config(capi_phy_info_t*             phy_info_ptr,
                                    capi_avs_mode_config_info_t* avs_config_ptr);

/**
 * @brief      capi_get_avs_status(capi_phy_info_t*   phy_info_ptr,
 *                                 capi_avs_status_t* capi_avs_status_ptr)
 * @details    This API is used to set the AVS configuration
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  capi_avs_status_ptr: this pointer contains AVS mode configuration defined by
 *                                  capi_avs_status_t, also dump avs status if AVS_TRACE_DEBUG is enabled
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_get_avs_status(capi_phy_info_t*   phy_info_ptr,
                                    capi_avs_status_t* capi_avs_status_ptr);


/**
 * @brief    capi_set_0p75v_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                           capi_other_voltage_config_info_t* fixed_config_ptr)
 * @details  This API is used to set the 0.75V voltage
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  fixed_config_ptr: this pointer contains AVS mode configuration defined
 *             by capi_avs_mode_config_info_t
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_set_0p75v_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                              capi_other_voltage_config_info_t* fixed_config_ptr);

/**
 * @brief    capi_set_0p9v_voltage_config(capi_phy_info_t*        phy_info_ptr,
                                    capi_other_voltage_config_info_t* fixed_config_ptr)
 * @details  This API is used to set the 0.9V voltage
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  fixed_config_ptr:
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_set_0p9v_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                             capi_other_voltage_config_info_t* fixed_config_ptr);

/**
 * @brief    capi_set_voltage_config(capi_phy_info_t*                  phy_info_ptr,
 *                                   capi_fixed_voltage_config_info_t* fixed_config_ptr)
 * @details  This API is used to set the fixed voltage
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  fixed_config_ptr: this pointer contains AVS mode configuration defined
 *             by capi_fixed_voltage_config_info_t
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_set_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                        capi_fixed_voltage_config_info_t* fixed_config_ptr);

/**
 * @brief      capi_get_voltage_status(capi_phy_info_t*            phy_info_ptr,
 *                                     capi_voltage_status_info_t* capi_voltage_status_info_ptr)
 * @details    This API is used obtain the voltage in MV
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  capi_voltage_status_info_ptr: this parameter pointed to the voltage info defined by
 *                                             capi_voltage_status_info_t
 * 
 * @return     returns the result of the called method/function
 */
return_result_t capi_get_voltage_status(capi_phy_info_t*            phy_info_ptr,
                                        capi_voltage_status_info_t* capi_voltage_status_info_ptr);



/**
 * @brief      capi_set_chip_command(capi_phy_info_t*          phy_info_ptr,
 *                                   capi_chip_command_info_t* chip_cmd_info_ptr)
 * @details    This API is used to invoke chip set command
 *
 * @param[in]  phy_info_ptr: a reference to the phy information object
 * @param[in]  chip_cmd_inf_ptr: a reference to the chip command object 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_set_chip_command(capi_phy_info_t*          phy_info_ptr,
                                      capi_chip_command_info_t* chip_cmd_info_ptr);

/**
 * @brief      capi_get_chip_command(capi_phy_info_t*          phy_info_ptr,
 *                                   capi_chip_command_info_t* chip_cmd_inf_ptr)
 * @details    This API is used to invoke chip get command
 *
 * @param[in]  phy_info_ptr: a reference to the phy information object
 * @param[in]  chip_cmd_inf_ptr: a reference to the chip command object 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_get_chip_command(capi_phy_info_t*          phy_info_ptr,
                                      capi_chip_command_info_t* chip_cmd_inf_ptr);

/**
 * @brief      capi_get_status(capi_phy_info_t* phy_info_ptr, capi_status_t* status_ptr)
 * @details    Get lane status from GP registers
 *
 * @param      phy_info_ptr : phy information
 * @param      status_ptr : output gpr lane status
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t capi_get_status(capi_phy_info_t* phy_info_ptr, capi_status_t* status_ptr);

#ifdef __cplusplus
}
#endif

#endif /**< CAPI_H */

