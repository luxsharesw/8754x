/**
 *
 * @file host_diag_util.h
 * @author
 * @date     12/01/2016
 * @version 1.0
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#ifndef HOST_DIAG_UTIL_H
#define HOST_DIAG_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#define TXFIR_MAX_VALUE 168

#ifdef ENABLE_CMD_SANITY_CHECK
/* IPC based command sanity checker */
#define CMD_SANITY_CHECK(phy_info_ptr, cmd_info, chk_type)   do { \
            return_result_t retcode = RR_SUCCESS; \
            retcode = host_diag_cmd_sanity_checker(phy_info_ptr, cmd_info, chk_type); \
            if (retcode != RR_SUCCESS) return retcode; \
            }   while(0)

/* non-IPC host based structure sanity checker */
#define HOST_SANITY_CHECK(phy_info_ptr, in_struct_ptr, struct_id)   do { \
            return_result_t retcode = RR_SUCCESS; \
            retcode = host_diag_struct_sanity_checker(phy_info_ptr, in_struct_ptr, struct_id); \
            if (retcode != RR_SUCCESS) return retcode; \
            }   while(0)
#else
#define CMD_SANITY_CHECK(phy_info_ptr, cmd_info, chk_type)
#define HOST_SANITY_CHECK(phy_info_ptr, in_struct_ptr, struct_id)
#endif

typedef enum sanity_chk_e {
    CHK_USR_INPUT = 0,  /**< Perform sanity check on the user input         */
    CHK_FW_OUTPUT = 1   /**< Perform sanity check on the FW provided output */

} sanity_chk_t;

typedef enum host_struct_sanity_id_e {
    SANITY_AVS_CONFIG_ID = 0   /**< AVS config input struture               */
} host_struct_sanity_id_t;

/**
 * @brief      util_check_param_range(int source, int lower_limit, int upper_limit)
 * @details    This API is used to check if a parameter is inside lower and upper limit
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  source: parameter to be checked
 * @param[in]  lower_limit: lower boundary for source
 * @param[in]  upper_limit: upper boundary for source
 *
 * @return     TRUE: source is in lower/upper limit, FALSE: source is outside lower/upper limit
 */

bool util_check_param_range(int source, int lower_limit, int upper_limit);


/**
 * @brief      util_lw_tx_fir_lut_validate_coef_and_lvl(phy_info_t* phy_info_ptr, int16_t *coef, int8_t *lvl)
 * @details    This API is used to check if txfir level shift is valid
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr : phy info
 * @param[in]  coef         : Coeff.
 * @param[in]  lvl          : Tap level
 *
 * @return     RR_SUCCESS: Valid, RR_WARNING_BOUNDS: Out of range
 */
return_result_t util_lw_tx_fir_lut_validate_coef_and_lvl (phy_info_t* phy_info_ptr, int16_t *coef, int8_t *lvl);

/**
 * @brief      lane_ctrl_info_sanity_checker_get(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
 * @details    This API is used to sanity check lane control info get parameters
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr         : this parameter 
 * @param[in]  lane_ctrl_info_t     : this parameter 
 * @param[in]  sanity_chk_t         : chk_type - 0: Check user input, 1: Chec kFW output
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_get_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr, sanity_chk_t chk_type);

/**
 * @brief      lane_ctrl_info_sanity_checker(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
 * @details    This API is used to sanity check lane control info parameters
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr           : this parameter 
 * @param[in]  lane_ctrl_info_ptr     : this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_set_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr);


/**
 * @brief      sanity_checker_get_polarity_info(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check polarity info params when doing a get
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr     : this parameter 
 * @param[in]  polarity_info_ptr: this parameter 
 * @param[in]  sanity_chk_t     : chk_type - 0: Check user input, 1: Chec kFW output
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_get_polarity_info (capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr, sanity_chk_t chk_type);

/**
 * @brief      sanity_checker_set_polarity_info(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
 * @details    This API is used to sanity check polarity info params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr     : this parameter 
 * @param[in]  polarity_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_set_polarity_info(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr);

/**
 * @brief      diag_set_prbs_config_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
 * @details    This API is used to sanity check set prbs config params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter 
 * @param[in]  prbs_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t diag_set_prbs_config_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr);

/**
 * @brief      sanity_checker_clear_prbs_status(capi_phy_info_t *phy_info_ptr, capi_prbs_status_t* prbs_st_ptr)
 * @details    This API is used to sanity check clear prbs status params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter 
 * @param[in]  ptype: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_clear_prbs_status(capi_phy_info_t *phy_info_ptr, capi_prbs_status_t* prbs_st_ptr);

/**
 * @brief      sanity_checker_set_txpi_override(capi_phy_info_t *phy_info_ptr, txpi_override_t* txpi_ovrd)
 * @details    This API is used to sanity check txpi override parameters
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  txpi_ovrd   : this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_set_txpi_override(capi_phy_info_t *phy_info_ptr, txpi_override_t* txpi_ovrd);

/**
 * @brief      sanity_checker_prbs_inject_error(capi_phy_info_t *phy_info_ptr, capi_prbs_err_inject_t* prbs_err_inj_ptr)
 * @details    This API is used to sanity check prbs inject error params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter 
 * @param[in]  prbs_err_inj_ptr: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_prbs_inject_error(capi_phy_info_t *phy_info_ptr, capi_prbs_err_inject_t* prbs_err_inj_ptr);


/**
 * @brief      sanity_checker_prbs_status_get(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
 * @details    This API is used to sanity check get prbs status params
 *
 * @param[in]  phy_info_ptr: this parameter 
 * @param[in]  prbs_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_prbs_status_get(capi_phy_info_t *phy_info_ptr, capi_prbs_status_t* prbs_status_ptr, sanity_chk_t chk_type) ;

/**
 * @brief      sanity_checker_set_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check set prbs config params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter 
 * @param[in]  prbs_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_set_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr);

/**
 * @brief      sanity_checker_get_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check get prbs config params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter 
 * @param[in]  prbs_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_get_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr, sanity_chk_t chk_type);

/**
 * @brief      sanity_checker_set_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check set config info params
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  config_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_set_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type);

/**
 * @brief      sanity_checker_get_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check get config info params
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  config_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_get_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type);

#ifdef ENABLE_CMD_SANITY_CHECK
/**
 * @brief       host_diag_cmd_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_command_info_t *cmd_info_ptr, sanity_chk_t chk_type)
 * @details       This function is used to check the capi cmd sanity
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]
 *
 * @return       returns the performance result of the called method/function
 */
return_result_t host_diag_cmd_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_command_info_t *cmd_info_ptr, sanity_chk_t chk_type);

/**
 * @brief      host_diag_struct_sanity_checker(capi_phy_info_t *phy_info_ptr, void *in_struct_ptr)
 * @details    Host based capi function's input structure sanity checker
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      in_struct_ptr : pointer to input structure, type cast with void*
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_diag_struct_sanity_checker(capi_phy_info_t *phy_info_ptr, void *in_struct_ptr, host_struct_sanity_id_t struct_id);
#endif
/**
 * @brief      host_get_memory_payload(capi_phy_info_t*    phy_info_ptr, 
 *                                     void*               dest_ptr,
 *                                     memory_info_t*      mem_data_ptr)
 *
 * @details    A fast access API to get response data populated by FW.
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      dest_ptr      : dest data pointer
 * @param      mem_data_ptr :  memory data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_memory_payload(capi_phy_info_t*    phy_info_ptr, 
                                                      void*               dest_ptr,
                                                      memory_info_t*      mem_data_ptr);

/**
 * @brief       sanity_checker_mpi_config_info(capi_phy_info_t *phy_info_ptr, dsp_mpi_cfg_info_t* mpi_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check MPI config info params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  mpi_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_mpi_config_info(capi_phy_info_t *phy_info_ptr, dsp_mpi_cfg_info_t* mpi_info_ptr, sanity_chk_t chk_type);

/**
 * @brief      dsp_mission_mpi_cfg_sanity_checker(capi_phy_info_t* phy_info_ptr, dsp_mission_mpi_cfg_info_t* dsp_mission_mpi_cfg_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check steady state MPI configuration
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr     : this parameter 
 * @param[in]  dsp_mission_mpi_cfg_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called methode/function
 */
return_result_t dsp_mission_mpi_cfg_sanity_checker(capi_phy_info_t* phy_info_ptr, dsp_mission_mpi_cfg_info_t* dsp_mission_mpi_cfg_info_ptr, sanity_chk_t chk_type);
#ifdef __cplusplus
}
#endif

#endif /* HOST_DIAG_UTIL_H */
