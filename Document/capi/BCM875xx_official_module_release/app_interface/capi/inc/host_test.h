/**
 *
 * @file host_test.h
 * @author   Firmware Team
 * @date     4/01/2019
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#ifndef HOST_TEST_H
#define HOST_TEST_H

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief        uint16_t host_test_get_valid_port_mask(uint8_t chip_mode)
 * @details     based on chip mode enum,  to get the valid port_mask
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  chip_mode:  chip mode index
 * 
 * @return     returns the valid port mask
 */


uint16_t host_test_get_valid_port_mask(uint8_t chip_mode);

/**
 * @brief        host_test_get_port_lane_mask(uint8_t chip_mode, uint8_t is_lw, uint16_t port_idx)
 * @details     based on chip mode enum, port_idx  to get the valid bh_lane_mask or lw_lane_mask
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  chip_mode:  chip mode index
 * @param[in]  is_lw:  0-client; 1-line
 * @param[in]  port_idx:  port index
 * 
 * @return     returns the lw/bh lane_mask for specified port
 */

uint16_t host_test_get_port_lane_mask(uint8_t chip_mode, uint8_t is_lw, uint16_t port_idx);

/**
 * @brief        host_test_check_port_validation(uint8_t chip_mode, uint16_t port_idx)
 * @details     based on chip mode enum, port_idx  validate the port idx validation
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  chip_mode:  chip mode index
 * @param[in]  port_idx:  port index
 * 
 * @return     returns specified port is valid or not
 */
uint8_t host_test_check_port_validation(uint8_t chip_mode, uint16_t port_idx);

/**
 * @brief      host_test_set_dsp_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_mode_type_t dsp_mode)
 * @details    This API is used to config to LW dsp mode:
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  dsp_mode: mode to be configured
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t host_test_set_dsp_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_mode_type_t dsp_mode);

/**
 * @brief      host_test_get_dsp_mode(capi_phy_info_t* phy_info_ptr);
 * @details    This API is used to get dsp mode
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * 
 * @return     returns dsp mode as an uint8_t
 */
uint8_t host_test_get_dsp_mode(capi_phy_info_t* phy_info_ptr);

/**
 * @brief      host_test_set_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t tc_tx_mode)
 * @details    This API is used to config to LW TC TX type:
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  tc_tx_mode:Test Chip TX mode 
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t host_test_set_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t tc_tx_mode);

/**
 * @brief      host_test_get_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t* tc_tx_mode)
 * @details    This API is used to get  LW TC TX mode
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] tc_tx_mode:  Test Chip TX mode 
 * 
 * @return     returns dsp mode as an uint8_t
 */
return_result_t host_test_get_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t* tc_tx_mode);

#ifdef __cplusplus
}
#endif

#endif /*HOST_TEST_H*/

