/**
 * @file     host_diag.h
 * @author  
 * @date     08-5-2019
 * @version 1.0
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#ifndef HOST_DIAG_H
#define HOST_DIAG_H

#ifdef __cplusplus
extern "C" {
#endif

/*Data mode */
typedef enum {
    DMODE_NRZ = 0,   /**< NRZ data mode       */
    DMODE_EPAM2,     /**< EPAM2 data mode     */
    DMODE_PAM4,      /**< PAM4 data mode      */
    DMODE_EPAM4      /**< EPAM4 data mode     */
} host_diag_dmode_e; /**< host diag data mode */

/**
 * @brief      host_util_get_lw_phy_info(capi_phy_info_t* capi_phy_info_ptr, phy_info_t* phy_info_ptr, uint8_t lane_index)
 * @details    populate new phy info structure based on lane index
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param[in]  capi_phy_info_ptr  : phy info pointer that carries the original information
 * @param[in]  phy_info_ptr       : phy info pointer to be populated
 * @param[in]  lane_id            : LW lane index
 *
 * @return void
 */
void host_util_get_lw_phy_info(capi_phy_info_t* capi_phy_info_ptr, phy_info_t* phy_info_ptr, uint8_t lane_index);

/**
 * @brief      host_util_get_lane_lw_top_pam_bbaddr(uint8_t lane_index)
 *
 * @details    Get TOP PAM base address
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param[in] lane_id  : LW lane index
 *
 * @return base address
 */
ubaddr_t host_util_get_lane_lw_top_pam_bbaddr(uint8_t lane_index);

/**
 * @brief  host_lw_get_snr(phy_info_t* phy_info_ptr, uint8_t mode, float* snr)
 * Calculate SNR value
 * Equation :  SNR = 10 log (signal_value) 10 log (MSE/2^28)
 * @param[in]  phy_info_ptr  phy_info containing device base address and port
 * @param[in]  snr: store snr value
 * @return  0: RR_OK; 0xFF:RR_ERROR
 */
return_result_t host_lw_get_snr(phy_info_t* phy_info_ptr, float* snr);

/**
 * @brief  host_lw_get_snr_wo_suspend_resume(phy_info_t* phy_info_ptr, uint8_t mode, double* snr)
 * Calculate SNR value
 * Equation :  SNR = 10 log (signal_value) 10 log (MSE/2^28)
 * @param[in]  phy_info_ptr  phy_info containing device base address and port
 * @param[in]  snr: store snr value
 * @return  0: RR_OK; 0xFF:RR_ERROR
 */
return_result_t host_lw_get_snr_wo_suspend_resume(phy_info_t* phy_info_ptr, float* snr);
/**
 * @brief      host_lw_get_lvl_snr(capi_phy_info_t* phy_info_ptr, float *dsnrptr, bool suspend_fw)
 * @details    This API is used to read SNR leveled
 *             Note: This function include the suspend and resume; Don't apply the suspend & resume sequence external;
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out]  dsnrptr: {SNR level number: N, level0 SNR value, level1 SNR value, level N-1 SNR value}
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t host_lw_get_lvl_snr(capi_phy_info_t* phy_info_ptr, float *dsnrptr, bool suspend_fw);

/**
 * @brief      host_lw_get_usr_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
 * @details    This API is used to get user diag information for Line side <BR>
 *             when core_ip is CORE_IP_MEDIA/HOST_DSP, the LW will report below parameters: <BR>
 *             brcm snr and brcm snr level values <BR>
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  usr_diag_info_ptr: a pointer which carries detail user diag information
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t host_lw_get_usr_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr);


/**
* @brief      host_lw_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
* @details    This API is used to get the user cmis diag info
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  usr_diag_info_ptr: this parameter
*
* @return     returns the performance result of the called method/function
*/
return_result_t host_lw_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr);

/**
 * @brief      host_client_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
 * @details    This API is used to get user cmis diag information for serdes side <BR>
 *             when core_ip is CORE_IP_HOST/MEDIA_SERDES, the serdes will report cmis snr and ltp parameters <BR>
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  usr_diag_info_ptr: a pointer which carries detail user diag information
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t host_client_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr);


/**
* @brief      host_dsp_get_slicer_histogram_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
* @details    This API is used to get the slicer histogram
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  usr_diag_info_ptr: this parameter
*
* @return     returns the performance result of the called method/function
*/
return_result_t host_dsp_get_slicer_histogram_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr);
return_result_t host_lw_get_smode(capi_phy_info_t* phy_info_ptr, uint8_t *smode);



/**
 * @brief      host_set_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_set_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr);


/**
 * @brief      host_get_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr);


/**
 * @brief      host_get_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr)
 *
 * @details    get mpi state information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_st_ptr      : mpi state data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr);


/**
 * @brief      host_set_mpi_dynamic_config(capi_phy_info_t*    phy_info_ptr, 
 *                                          dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    set mpi dynamic configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi dynamic config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_set_mpi_dynamic_config(capi_phy_info_t*            phy_info_ptr,
                                            dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr);
/**
 * @brief      host_get_mpi_dynamic_config(capi_phy_info_t*    phy_info_ptr, 
 *                                          dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi dynamic configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi dynamic config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_mpi_dynamic_config(capi_phy_info_t*            phy_info_ptr,
                                            dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr);


/**
 * @brief      host_get_dynamic_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr)
 *
 * @details    get mpi steady state information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_st_ptr      : mpi state data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_dynamic_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr);

#ifdef __cplusplus
}
#endif

#endif /* HOST_DIAG_H */

