/**
 *
 * @file      host_chip_wrapper.h
 * @author    Firmware Team
 * @date      02/01/2019
 * @version   1.0
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */

/** @file host_chip_wrapper.h
 *  cAPI prototype definition
 */
#ifndef HOST_CHIP_WRAPPER_H
#define HOST_CHIP_WRAPPER_H


#ifdef __cplusplus
extern "C" {
#endif


/**
* @brief      util_wait_for_uc_ready(capi_phy_info_t* phy_info_ptr)
* @details    This utility function to pool the FW ready state
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t util_wait_for_uc_ready(capi_phy_info_t* phy_info_ptr);

/**
 * @brief    host_get_hw_info(capi_phy_info_t* phy_info_ptr, capi_chip_info_t* chip_info_ptr)
 * @details  This API is used to get the Chip information
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] chip_info_ptr: this pointer contains chip info defined by capi_chip_info_t, which has 
 *                            chip_id and chip_revision
 * 
 * @return     returns the result of the called methode/function, RR_SUCCESS
 */
return_result_t host_get_hw_info(capi_phy_info_t* phy_info_ptr, capi_hw_info_t* chip_info_ptr);

/**
* @brief        host_get_sw_info(capi_phy_info_t* phy_info_ptr, capi_sw_info_t* sw_info_ptr)
* @details      This function returns firmware version information
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   sw_info_ptr: this pointer retuns the firmware version info defined by capi_sw_info_t
* 
* @return    returns the result of the called methode/function, either RR_ERROR or RR_SUCCESS
*/
return_result_t host_get_sw_info(capi_phy_info_t* phy_info_ptr, capi_sw_info_t* sw_info_ptr);


/**
 * @brief    host_get_lpm_st(capi_phy_info_t* phy_info_ptr, capi_lpm_info_t* lpm_ptr)
 * @details  This API is used to get firmware LPM status: IN LPM(1) or NOT IN LPM(0)
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  lpm_ptr: a pointer for lpm information
 * 
 * @return     returns the result of the called methode/function
 */
return_result_t host_get_lpm_st(capi_phy_info_t* phy_info_ptr, capi_lpm_info_t* lpm_ptr);
#ifdef __cplusplus
}
#endif


#endif /**< HOST_CHIP_WRAPPER_H */
