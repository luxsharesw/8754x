/**
 *
 * @file    host_power_util.h
 * @author
 * @date    12/11/2017
 * @version 1.0
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#ifndef HOST_POWER_UTIL_H
#define HOST_POWER_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#define ANA_REG_WRITE              0x1
#define ANA_REG_READ               0x2
#define VOLT_ADJ_DAC_REG_ADDR      0xC
#define ANA_REG_0                  0x0
#define ANA_AVS_MASTER_PHY_ID      0x0
#define ANA_DVDDM_MASTER_PHY_ID    0x1
#define ANA_AVDD_MASTER_PHY_ID     0x2
#define ANA_AVS_SLAVE1_PHY_ID      0x3 //dont care
#define ANA_AVS_SLAVE2_PHY_ID      0x4 // dont care
#define ANA_DVDDM_SLAVE_PHY_ID     0x5 // dont care

#define MDIOM_RDB_CMD              0x0000b004        
#define MDIOM_RDB_MDIOM_CTRL       0x0000b000        
#define MDIOS_RDB_DEV_ADDR         0x00006028        
#define MDIOS_RDB_DEV_ADDREN       0x0000602c        
#define MDIOS_RDB_HRSVD_CTRL       0x00006008        
#define MDIOS_RDB_MDIOS_INTEN      0x00006004        
#define MDIOS_RDB_LRDATA           0x00006044        
#define MDIOS_RDB_MC_PRT_ADDR      0x00006018        
#define MDIOS_RDB_MC_PRT_ADDREN    0x0000601c        
#define MDIOS_RDB_MC_PRT_ADDRMEN   0x00006020        
#define MDIOS_RDB_MDIOS_PID        0x00006040        
#define MDIOS_RDB_PRT_ADDR         0x0000600c        
#define MDIOS_RDB_PRT_ADDREN       0x00006010        
#define MDIOS_RDB_PRT_ADDRMEN      0x00006014        
#define MDIOS_RDB_PRT_CTRL         0x00006024        
#define MDIOS_RDB_SCRATCH_PAD0     0x00006030        
#define MDIOS_RDB_SCRATCH_PAD1     0x00006034        
#define MDIOS_RDB_SCRATCH_PAD2     0x00006038        
#define MDIOS_RDB_SCRATCH_PAD3     0x0000603c        
#define MDIOS_RDB_MDIOS_STAT       0x00006000        

#define PCR_RST_CTR2                                                 0x0001cc9c		/*QUAD_CORE_GPCTRL_GPCTRL_295_RDB*/
#define COM_REMAP2                                                   0x0001cca0		/*QUAD_CORE_GPCTRL_GPCTRL_296_RDB*/
#define COM_COM_CTRL2                                                0x0001cc9c
#define CHIP_TOP_CHIP_PRE_LOAD_CONFIG22_REG                          0x0001cca0
#define CHIP_TOP_CHIP_PRE_LOAD_CONFIG23_REG                          0x0001cc9c

#define PCR_RST_CTR2_M0P3_SRST_EN_MASK                                                              0x00001000   /*!< M0P3_SRST_EN */ 
#define PCR_RST_CTR2_M0P3_SRST_EN_SHIFT                                                             12  
#define PCR_RST_CTR2_M0P2_SRST_EN_MASK                                                              0x00000800   /*!< M0P2_SRST_EN */ 
#define PCR_RST_CTR2_M0P2_SRST_EN_SHIFT                                                             11  

#define PCR_RST_CTR2_SPIF_SRST_EN_MASK                                                              0x00000010   /*!< SPIF_SRST_EN */ 
#define PCR_RST_CTR2_SPIF_SRST_EN_SHIFT                                                             4  

#define PCR_RST_CTR2_M0P1_SRST_EN_MASK                                                              0x00000002   /*!< M0P1_SRST_EN */ 
#define PCR_RST_CTR2_M0P1_SRST_EN_SHIFT                                                             1  
#define PCR_RST_CTR2_M0P0_SRST_EN_MASK                                                              0x00000001   /*!< M0P0_SRST_EN */ 
#define PCR_RST_CTR2_M0P0_SRST_EN_SHIFT                                                             0  

#define PCR_RST_CTR2                                                 0x0001cc9c		/*QUAD_CORE_GPCTRL_GPCTRL_295_RDB*/
#define COM_REMAP2                                                   0x0001cca0		/*QUAD_CORE_GPCTRL_GPCTRL_296_RDB*/


#define PCR_RST_CTR2_M0P3_SRST_EN_MASK                                                              0x00001000   /*!< M0P3_SRST_EN */ 
#define PCR_RST_CTR2_M0P3_SRST_EN_SHIFT                                                             12  
#define PCR_RST_CTR2_M0P2_SRST_EN_MASK                                                              0x00000800   /*!< M0P2_SRST_EN */ 
#define PCR_RST_CTR2_M0P2_SRST_EN_SHIFT                                                             11  

#define PCR_RST_CTR2_SPIF_SRST_EN_MASK                                                              0x00000010   /*!< SPIF_SRST_EN */ 
#define PCR_RST_CTR2_SPIF_SRST_EN_SHIFT                                                             4  

#define PCR_RST_CTR2_M0P1_SRST_EN_MASK                                                              0x00000002   /*!< M0P1_SRST_EN */ 
#define PCR_RST_CTR2_M0P1_SRST_EN_SHIFT                                                             1  
#define PCR_RST_CTR2_M0P0_SRST_EN_MASK                                                              0x00000001   /*!< M0P0_SRST_EN */ 
#define PCR_RST_CTR2_M0P0_SRST_EN_SHIFT                                                             0  

/*return_result_t power_util_ana_avs_set_volt(phy_info_t*  phy_info_ptr, uint32_t avs_mv);*/
/*return_result_t power_util_ana_avs_get_volt(phy_info_t*  phy_info_ptr, uint32_t* avs_reading);*/
return_result_t power_util_ana_vddm_set_volt(phy_info_t* phy_info_ptr, uint32_t vddm_mv);
return_result_t power_util_ana_vddm_get_volt(phy_info_t* phy_info_ptr, uint32_t* reading);
return_result_t power_util_ana_avdd_set_volt(phy_info_t* phy_info_ptr, uint32_t avdd_mv);
return_result_t power_util_ana_avdd_get_volt(phy_info_t* phy_info_ptr, uint32_t* reading);
return_result_t host_power_util_boot_config(capi_phy_info_t* phy_info_ptr, capi_chip_default_info_t* chip_default_info_ptr);


#ifdef __cplusplus
}
#endif

#endif  /* HOST_POWER_UTIL_H */
