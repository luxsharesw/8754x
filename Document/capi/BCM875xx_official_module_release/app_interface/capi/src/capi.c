/**
 *
 * @file       capi.c
 * @author     PLP Firmware Team
 * @date       03/15/2020
 * @version    1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 *             Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief      The CAPI files contain all the CAPIs used by the the module and line card products.
 *
 * @section  description
 *
 */

#include "hr_time.h"
#include "access.h"
#include "common_def.h"
#include "regs_common.h"
#include "chip_config_def.h"
#include "chip_common_config_ind.h"
#include "ipc_regs.h"
#include "chip_mode_def.h"
#include "cw_def.h"
#include "capi_def.h"
#include "capi_diag_def.h"
#include "host_diag_util.h"                  /* CMD_SANITY_CHECK() */
#include "host_diag.h"
#include "common_util.h"
#include "fw_gp_reg_map.h"
#include "host_power_util.h"
#include "host_log_util.h"
#include "host_chip_wrapper.h"
#include "host_download_util.h"
#include "host_to_chip_ipc.h"
#include "lw_common_config_ind.h"
#include "capi.h"
#include "whole_image_sram.h"
#include "whole_image_spi.h"

/************************************************************************************************************************/
/****************************************************Read Register*******************************************************/
/************************************************************************************************************************/
/**
* @brief         capi_read_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr)
* @details       This API is used to read register/s
*
* @param[in]     phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in/out] reg_info_ptr: when calling, give the 32 bit reg_address element defined in
*                the capi_reg_info_t and return content element defined in  the capi_reg_info_t
* 
* @return        returns the result of the called method/function
*/
return_result_t capi_read_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr)
{
    /*Shall be move to Sanity Checker (Jian Guo)*/
    /*CAPI_LOG_FUNC("capi_read_register", phy_info_ptr);*/
    /*CAPI_LOG_INFO("register addr: 0x%x\n", reg_info_ptr->reg_address);*/

    phy_info_ptr->base_addr = 0x0;
    ERR_HSIP(reg_info_ptr->content  = hsip_rd_reg_(phy_info_ptr, reg_info_ptr->reg_address));
    return (RR_SUCCESS);
}

/************************************************************************************************************************/
/****************************************************Write Register******************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_write_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr)
* @details      This API is used to read register/s
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    reg_info_ptr: this pointer contains the information to write to the register
*               give the 32 bit reg_address element defined in  the capi_reg_info_t
*               and register value, content element defined in  the capi_reg_info_t
* 
* @return       returns the result of the called method/function
*/
return_result_t capi_write_register(capi_phy_info_t* phy_info_ptr, capi_reg_info_t* reg_info_ptr)
{
    /*Shall be move to Sanity Checker c/src/capi.c*/
    /*CAPI_LOG_FUNC("capi_write_register", phy_info_ptr);*/
    /*CAPI_LOG_INFO("register addr: 0x%x value 0x%x\n", reg_info_ptr->reg_address, reg_info_ptr->content);*/

    phy_info_ptr->base_addr = 0x0;
    hsip_wr_reg_(phy_info_ptr, reg_info_ptr->reg_address, reg_info_ptr->content);
    return (RR_SUCCESS);
}

/************************************************************************************************************************/
/****************************************************Get Chip Info*******************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_get_chip_info(capi_phy_info_t* phy_info_ptr, capi_chip_info_t* chip_info_ptr)
* @details      This API is used to get the Chip information
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   chip_info_ptr: this pointer contains chip info defined by capi_chip_info_t, which has 
*                              chip_id and chip_revision
* 
* @return       returns the result of the called method/function, RR_SUCCESS
*/
return_result_t capi_get_chip_info(capi_phy_info_t* phy_info_ptr, capi_chip_info_t* chip_info_ptr)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;
    CAPI_LOG_FUNC("capi_get_chip_info", phy_info_ptr);

    if (chip_info_ptr->param.is.hw_info) {
        return_result = host_get_hw_info(phy_info_ptr, &chip_info_ptr->value.hw_info);
        if (return_result) return return_result;
    }
    if (chip_info_ptr->param.is.sw_info) {
        return_result = host_get_sw_info(phy_info_ptr, &chip_info_ptr->value.sw_info);
    }

    return return_result;
}

static return_result_t _capi_clr_dual_die_sync_up_st(capi_phy_info_t* phy_info_ptr)
{
    capi_phy_info_t capi_phy;
    uint16_t        dual_die_handle_active;
    uint8_t         phy_idx;

    util_memcpy((void *)&capi_phy, phy_info_ptr, sizeof(capi_phy_info_t));
    capi_phy.base_addr = OCTAL_TOP_REGS;
    ERR_HSIP(dual_die_handle_active = (uint16_t)hsip_rd_field_(&capi_phy, FW_STATUS_SET_REG, DUAL_DIE_SYNC_ACTIVE_FLAG));
    if(dual_die_handle_active==0)
        return RR_SUCCESS;

    /*Die 0: cAPI clear below all bits before initializing config command to FW*/
    if(phy_info_ptr->phy_id==0){
        for(phy_idx=0; phy_idx<=2; phy_idx+=2){
            capi_phy.phy_id = phy_idx;
            hsip_wr_field_(&capi_phy, HOST_MEDIA_PLL_ST_REG,   HOST_CMODE_RDY_ST,  0);
            hsip_wr_field_(&capi_phy, HOST_MEDIA_PLL_ST_REG,   MEDIA_CMODE_RDY_ST, 0);
            hsip_wr_field_(&capi_phy, EXTERNAL_CONFIG_REQ_REG, START_LANE_CFG,     0);
        }
    }
    return RR_SUCCESS;
}
static return_result_t _capi_config_dual_die_sync_up_handle(capi_phy_info_t* phy_info_ptr)
{
    capi_phy_info_t capi_phy;
    uint16_t        dual_die_handle_active, rdy_st;
    uint8_t         start_cfg = 1;
    uint8_t         phy_idx;

    util_memcpy((void *)&capi_phy, phy_info_ptr, sizeof(capi_phy_info_t));
    capi_phy.base_addr = OCTAL_TOP_REGS;
    ERR_HSIP(dual_die_handle_active = (uint16_t)hsip_rd_field_(&capi_phy, FW_STATUS_SET_REG, DUAL_DIE_SYNC_ACTIVE_FLAG));
    if(dual_die_handle_active==0)
        return RR_SUCCESS;

    /*Die 2: cAPI check both die status, if matched expectation, cAPI set START_CFG flag to notify fw*/
    if(phy_info_ptr->phy_id==2){
        /*check both dies' cmode ready status flag*/
        for(phy_idx=0; phy_idx<=2; phy_idx+=2){
            capi_phy.phy_id = phy_idx;
            ERR_HSIP(rdy_st = (uint16_t)hsip_rd_reg_(&capi_phy, HOST_MEDIA_PLL_ST_REG));
            if((rdy_st & 0x3)!=0x3)
                start_cfg &= 0;
        }
        /*set the both dies' start config flag*/
        if(start_cfg){
            for(phy_idx=0; phy_idx<=2; phy_idx+=2){
                capi_phy.phy_id = phy_idx;
                hsip_wr_field_(&capi_phy, EXTERNAL_CONFIG_REQ_REG, START_LANE_CFG,  1);
            }
        }else{
            return RR_ERROR_DUAL_DIE_SYNC_NOT_READY;
        }
    }

    return RR_SUCCESS;
}
/**
* @brief      capi_get_chip_status(capi_phy_info_t*         phy_info_ptr,
*                                  capi_chip_status_info_t* chip_status_info_ptr)
* @details    This API is used to get the chip status
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  chip_status_info_ptr: pointer to capi_chip_status_info_t
* @return     returns the performance result of the called method/function
*/
return_result_t capi_get_chip_status(capi_phy_info_t* phy_info_ptr, capi_chip_status_info_t* chip_status_info_ptr)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;

    if (chip_status_info_ptr->param.is.download_done) {
        capi_download_info_t download_info;
        phy_static_config_t  static_config;

        util_memset(&download_info, 0, sizeof(capi_download_info_t));
        util_memset(&static_config, 0, sizeof(phy_static_config_t));

        download_info.mode                  = CAPI_DOWNLOAD_MODE_MDIO_SRAM;
        download_info.image_info.image_ptr  = NULL;
        download_info.phy_static_config_ptr = &static_config;

        return_result = capi_get_download_status(phy_info_ptr, &download_info);

        if (return_result) return(return_result);

        if (download_info.status.result == 0x1) {
             chip_status_info_ptr->value.download_done = 1;
        } else {
            return(RR_ERROR_DOWNLOAD_FAILED);
        }
    }

    if (chip_status_info_ptr->param.is.avs_done) {
        capi_avs_status_t avs_status        = {0};
        uint16_t          avs_ready_timer   = 30; /*3 seconds*/
        uint16_t          avs_ready_counter = 0;

        while ((avs_status.avs_result != CAPI_INIT_DONE_SUCCESS) && (avs_ready_counter < avs_ready_timer)) {
            delay_ms(100);
            avs_ready_counter++;
            return_result = capi_get_avs_status(phy_info_ptr, &avs_status);

            if (!avs_status.avs_enable) break;

            if (return_result || (avs_status.avs_result == CAPI_INIT_ERROR)) {
                return(RR_ERROR_AVS_INITIALIZATION_FAILED);
            }
        }

        if (avs_status.avs_enable && avs_status.avs_result == CAPI_INIT_DONE_SUCCESS) {
            chip_status_info_ptr->value.avs_done = 1;
        } else {
            chip_status_info_ptr->value.avs_done = 0;
            return(RR_ERROR_AVS_INITIALIZATION_TIME_OUT);
        }
    }

    if (chip_status_info_ptr->param.is.uc_ready) {
        if (util_wait_for_uc_ready(phy_info_ptr) == RR_SUCCESS) {
            chip_status_info_ptr->value.uc_ready = 1;
            if(_capi_config_dual_die_sync_up_handle(phy_info_ptr)!=RR_SUCCESS)
                return RR_ERROR_DUAL_DIE_SYNC_NOT_READY;
            else
                return_result = RR_SUCCESS;
        } else {
            chip_status_info_ptr->value.uc_ready = 0;
            return_result = RR_ERROR_FIRMWARE_NOT_READY;
        }
    }

    if (chip_status_info_ptr->param.is.spi_dev_ready) {
        return (RR_ERROR_WRONG_INPUT_VALUE);
    }

    return return_result;
}

/************************************************************************************************************************/
/************************************************Get GPR LANE STATUS*****************************************************/
/************************************************************************************************************************/


/************************************************************************************************************************/
/****************************************************SET AND GET POLARITY************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_set_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
* @details      This API is used to set the polarity configuration 
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    polarity_info_ptr: this pointer carries the necessary information to set polarity
* 
* @return       returns the performance result of the called method/function
*/
return_result_t capi_set_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
{
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_SET_POLARITY;
    command_info.type.polarity_info = *polarity_info_ptr;

    return(capi_command_request(phy_info_ptr, &command_info, sizeof(capi_polarity_info_t), SET_CONFIG));
}

/**
* @brief        capi_get_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* config_info_ptr)
* @details      This API is used to get the configuration information
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   polarity_info_ptr: this pointer return polarity information
* 
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_polarity(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
{
    return_result_t     return_result = RR_SUCCESS;
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_GET_POLARITY;
    command_info.type.polarity_info = *polarity_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_polarity_info_t), GET_CONFIG);
    *polarity_info_ptr = command_info.type.polarity_info;

    return(return_result);
}

/************************************************************************************************************************/
/****************************************************SET AND GET LANE CONFIG INFO*************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_set_lane_config_info(capi_phy_info_t*         phy_info_ptr,
*                                         capi_lane_config_info_t* lane_config_info_ptr)
* @details      This API is used to
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    lane_config_info_ptr: this parameter
*
* @return       returns the performance result of the called method/function
*/
return_result_t capi_set_lane_config_info(capi_phy_info_t*         phy_info_ptr,
                                          capi_lane_config_info_t* lane_config_info_ptr)
{
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_SET_LANE_CONFIG_INFO;
    command_info.type.lane_config_info = *lane_config_info_ptr;

    return(capi_command_request(phy_info_ptr, &command_info, sizeof(capi_lane_config_info_t), SET_CONFIG));
}

/**
* @brief        capi_get_lane_config_info(capi_phy_info_t*         phy_info_ptr,
                                          capi_lane_config_info_t* lane_config_info_ptr)
* @details      This API is used to
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   lane_config_info_ptr: this parameter
*
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_lane_config_info(capi_phy_info_t*         phy_info_ptr,
                                          capi_lane_config_info_t* lane_config_info_ptr)
{
    return_result_t return_result;
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_GET_LANE_CONFIG_INFO;
    command_info.type.lane_config_info = *lane_config_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_lane_config_info_t), GET_CONFIG);
    *lane_config_info_ptr = command_info.type.lane_config_info;

    return(return_result);
}

/************************************************************************************************************************/
/**************************************************SET AND GET LANE CTRL INFO********************************************/
/************************************************************************************************************************/
/**
* @brief        capi_set_lane_ctrl_info(capi_phy_info_t*       phy_info_ptr,
*                                       capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
* @details      This API is used to set the lane control information
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   lane_ctrl_info_ptr: this parameter 
* 
* @return       returns the performance result of the called method/function
*/
return_result_t capi_set_lane_ctrl_info(capi_phy_info_t*       phy_info_ptr,
                                        capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
{
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_SET_LANE_CTRL_INFO;
    command_info.type.lane_ctrl_info = *lane_ctrl_info_ptr;

    return(capi_command_request(phy_info_ptr, &command_info, sizeof(capi_lane_ctrl_info_t), SET_CONFIG));
}

/**
* @brief        capi_get_lane_ctrl_info(capi_phy_info_t*       phy_info_ptr,
*                                       capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
* @details      This API is used to get the lane control information
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    lane_ctrl_info_ptr: this parameter 
* 
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_lane_ctrl_info(capi_phy_info_t*       phy_info_ptr,
                                        capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
{
    return_result_t return_result;
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_GET_LANE_CTRL_INFO;
    command_info.type.lane_ctrl_info = *lane_ctrl_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_lane_ctrl_info_t), GET_CONFIG);
    *lane_ctrl_info_ptr = command_info.type.lane_ctrl_info;

    return(return_result);
}

/************************************************************************************************************************/
/****************************************************GET LANE INFO*******************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_get_lane_info(capi_phy_info_t* phy_info_ptr, capi_lane_info_t* lane_info_ptr)
* @details      This API is used to get the lane info
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   lane_info_ptr: this pointer return lane status, refer to capi_lane_info_t
*
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_lane_info(capi_phy_info_t* phy_info_ptr, capi_lane_info_t* lane_info_ptr)
{
    return_result_t return_result;
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_GET_LANE_INFO;
    command_info.type.lane_info = *lane_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_lane_info_t), GET_CONFIG);

    *lane_info_ptr = command_info.type.lane_info;

    return(return_result);
}

/************************************************************************************************************************/
/****************************************************SET AND GET Config**************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_set_config(capi_phy_info_t* phy_info_ptr_in, capi_config_info_t* config_info_ptr)
* @details      This API is used to set the configuration information
*
* @param[in]    phy_info_ptr_in: a pointer which carries detail information necessary to identify the Phy
* @param[in]    config_info_ptr: this pointer carries the necessary information for configuration procedure
*   
* @return       returns the performance result of the called method/function
*/
return_result_t capi_set_config(capi_phy_info_t* phy_info_ptr_in, capi_config_info_t* config_info_ptr)
{
    return_result_t     return_result = RR_SUCCESS;
    capi_command_info_t command_info;
    uint8_t             lpm_st = 0;
    uint8_t             counter_mode_config_in_progress = 0;
    uint16_t            is_chip_mode_config_in_progress = 1;
    uint32_t            port_mode_cfg_log;
    capi_phy_info_t     capi_phy = {0}, *phy_info_ptr = &capi_phy;


    if(_capi_clr_dual_die_sync_up_st(phy_info_ptr_in)!=RR_SUCCESS)
        return RR_ERROR_DUAL_DIE_SYNC_NOT_READY;

    util_memcpy((void *)&capi_phy, phy_info_ptr_in, sizeof(capi_phy_info_t));

    phy_info_ptr->core_ip = CORE_IP_CW;
    if (config_info_ptr->line_lane.lane_mask)
        phy_info_ptr->lane_mask = config_info_ptr->line_lane.lane_mask;
    else if (config_info_ptr->host_lane.lane_mask)
        phy_info_ptr->lane_mask = config_info_ptr->host_lane.lane_mask << 16;

    command_info.command_id = COMMAND_ID_SET_CONFIG_INFO;
    command_info.type.config_info = *config_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_config_info_t), SET_CONFIG);

    if (return_result != RR_SUCCESS) return return_result;

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;

    while (is_chip_mode_config_in_progress) {
        delay_ms(10);
        counter_mode_config_in_progress++;
        ERR_HSIP(is_chip_mode_config_in_progress = (uint16_t)hsip_rd_reg_(phy_info_ptr, CHIP_MODE_FAST_CONFIG_COMMAND_REG));
        if (counter_mode_config_in_progress >= 200)
            return(RR_WARNING_BUSY_TRY_LATER);
    }

    ERR_HSIP(port_mode_cfg_log = hsip_rd_reg_(phy_info_ptr, OCTAL_PORT_MODE_CFG_LOG));
    CAPI_LOG_INFO("\n\nFIRMWRE CHIP MODE CONFIG LOG 0x%x\r\n", port_mode_cfg_log);
    ERR_HSIP(port_mode_cfg_log = hsip_rd_field_(phy_info_ptr,OCTAL_PORT_MODE_CFG_LOG, CFG_RESULT));

    if (port_mode_cfg_log != 0xF){
        CAPI_LOG_ERROR("FW report the CHIP MODE CONFIG fail \r\n");
        return RR_ERROR;
    }

    ERR_HSIP(lpm_st = (uint8_t)hsip_rd_field_(phy_info_ptr, OCW_CHIP_LPM_FW_REG, CHIP_IN_LPM));

    if (lpm_st == 0) {
        uint8_t lane_idx;
        uint16_t line_lane_cfg_done=0, host_lane_cfg_done=0;
        uint8_t line_lane_cfg_rdy=1, host_lane_cfg_rdy=1;
        uint8_t timeout, line_max_lane = 4, host_max_lane = 4, lane_cfg_rdy, lane_cw_cfg_rdy;
        uint8_t line_cfg_st[16] = {0}, host_cfg_st[16] = {0};
        /*check if all related lane get the done*/
        for (timeout=0; timeout<PORT_CFG_LANE_RDY_TIMEOUT; timeout++) {
            delay_ms(1);

            for (lane_idx=0; lane_idx<line_max_lane;lane_idx++) {
                /*if no valid lane or lane has responsed status, SKIP*/
                if ((config_info_ptr->line_lane.lane_mask&(1<<lane_idx))==0 || (line_lane_cfg_done&(1<<lane_idx)))
                    continue;
                phy_info_ptr->base_addr = OCTAL_TOP_REGS+(lane_idx<<2);
                ERR_HSIP(lane_cfg_rdy = hsip_rd_field_(phy_info_ptr, INTF_LWTOCW_COMMANDSTATUS_LANE0, LANE_CFG_STATUS));
                line_lane_cfg_done |= ((lane_cfg_rdy==INTF_LANE_ST_READY || lane_cfg_rdy==INTF_LANE_ST_FAIL)<<lane_idx);

                if (lane_cfg_rdy==INTF_LANE_ST_FAIL)
                    line_lane_cfg_rdy = 0;
                line_cfg_st[lane_idx] = lane_cfg_rdy;
            }

            for (lane_idx=0; lane_idx<host_max_lane;lane_idx++) {
                /*if no valid lane or lane has responsed status, SKIP*/
                if ((config_info_ptr->host_lane.lane_mask&(1<<lane_idx))==0 || (host_lane_cfg_done&(1<<lane_idx)))
                    continue;
                phy_info_ptr->base_addr = OCTAL_TOP_REGS+(lane_idx<<2);
                ERR_HSIP(lane_cfg_rdy = hsip_rd_field_(phy_info_ptr, INTF_BHTOCW_COMMANDSTATUS_LANE0, LANE_CFG_STATUS));
                host_lane_cfg_done |= ((lane_cfg_rdy==INTF_LANE_ST_READY || lane_cfg_rdy==INTF_LANE_ST_FAIL)<<lane_idx);
                if (lane_cfg_rdy==INTF_LANE_ST_FAIL)
                    host_lane_cfg_rdy = 0;
                host_cfg_st[lane_idx] = lane_cfg_rdy;
            }
        
            /*if(line_lane_cfg_done==cmode_lane_mask[port_entry.port_type-1].lw_lane_mask && host_lane_cfg_done==cmode_lane_mask[port_entry.port_type-1].bh_lane_mask)*/
            if (line_lane_cfg_done==config_info_ptr->line_lane.lane_mask && host_lane_cfg_done==config_info_ptr->host_lane.lane_mask)
                break;
        }
        /*Check and Wait the cores are ready to confirm the new config is IN*/
        if (timeout>=PORT_CFG_LANE_RDY_TIMEOUT || line_lane_cfg_rdy==0 || host_lane_cfg_rdy==0){
            CAPI_LOG_ERROR("Set config didn't get all lanes ready\n");
            return_result =  RR_ERROR;
        }

        /* LW lane calibration done takes >10ms *2*4*2 = 160ms */
        delay_ms(200);
        
        /*Check DW LW & BH lane config rdy status*/
        phy_info_ptr->base_addr = OCTAL_TOP_REGS;

        for (lane_idx=0; lane_idx<line_max_lane;lane_idx++) {
            ERR_HSIP(lane_cw_cfg_rdy =((hsip_rd_reg_(phy_info_ptr, OCW_LW_LANE_CONFIG_ST))>>(lane_idx<<1))&0x3);
            if(lane_cw_cfg_rdy!=line_cfg_st[lane_idx] && (config_info_ptr->line_lane.lane_mask&(1<<lane_idx)))
                return_result = RR_ERROR;
        }

        for(lane_idx=0; lane_idx<host_max_lane;lane_idx++) {
            ERR_HSIP(lane_cw_cfg_rdy = ((hsip_rd_reg_(phy_info_ptr, OCW_BH1_LANE_CONFIG_ST))>>((lane_idx%8)<<1))&0x3);
            if(lane_cw_cfg_rdy!=host_cfg_st[lane_idx]&& (config_info_ptr->host_lane.lane_mask&(1<<lane_idx)))
                return_result = RR_ERROR;
        }

#if (CAPI_LOG_LEVEL & CAPI_LOG_INFO_LEVEL)
        CAPI_LOG_INFO("Line side lane config status:");
        for(lane_idx=0; lane_idx<line_max_lane;lane_idx++) {
            /*if no valid lane or lane has responsed status, SKIP*/
            if(config_info_ptr->line_lane.lane_mask&(1<<lane_idx))
                CAPI_LOG_DATA("lane%d-%d;", lane_idx, line_cfg_st[lane_idx]);
        }

        CAPI_LOG_DATA("\n");
        CAPI_LOG_INFO("host side lane config status:");

        for (lane_idx=0; lane_idx<host_max_lane; lane_idx++) {
            /*if no valid lane or lane has responsed status, SKIP*/
            if (config_info_ptr->host_lane.lane_mask&(1<<lane_idx))
               CAPI_LOG_DATA("lane%d-%d;", lane_idx, host_cfg_st[lane_idx]);
        }

        CAPI_LOG_DATA("\n");
        CAPI_LOG_INFO("DW lane config status:\n");
        CAPI_LOG_INFO(" BH 0~7  0x%x \n", hsip_rd_reg_(phy_info_ptr, OCW_BH1_LANE_CONFIG_ST));
        CAPI_LOG_INFO(" LW 0~4  0x%x \n", hsip_rd_reg_(phy_info_ptr, OCW_LW_LANE_CONFIG_ST));
#endif
    }
    if(return_result != RR_SUCCESS)
        return return_result;
    return_result = _capi_config_dual_die_sync_up_handle(phy_info_ptr_in);
    return return_result;
}

/**
* @brief        capi_get_config(capi_phy_info_t* phy_info_ptr_in, capi_config_info_t* config_info_ptr)
* @details      This API is used to get the configuration information
*
* @param[in]    phy_info_ptr_in: a pointer which carries detail information necessary to identify the Phy
* @param[out]   config_info_ptr: this pointer return configuration information
* 
* @return       returns the performance result of the called method/function
*/
return_result_t capi_get_config(capi_phy_info_t* phy_info_ptr_in, capi_config_info_t* config_info_ptr)
{
    return_result_t     return_result = RR_SUCCESS;
    capi_command_info_t command_info;
    capi_phy_info_t     capi_phy = {0}, *phy_info_ptr = &capi_phy;

    util_memcpy((void *)&capi_phy, phy_info_ptr_in, sizeof(capi_phy_info_t));

    phy_info_ptr->core_ip = CORE_IP_CW;
    if (config_info_ptr->line_lane.lane_mask)
        phy_info_ptr->lane_mask = config_info_ptr->line_lane.lane_mask;
    else if (config_info_ptr->host_lane.lane_mask)
        phy_info_ptr->lane_mask = config_info_ptr->host_lane.lane_mask << 16;

    command_info.command_id = COMMAND_ID_GET_CONFIG_INFO;
    command_info.type.config_info = *config_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_config_info_t), GET_CONFIG);

    if (return_result != RR_SUCCESS) return return_result;

    *config_info_ptr = command_info.type.config_info;

    return(return_result);
}

/************************************************************************************************************************/
/****************************************************COMMAND REQIEST*****************************************************/
/************************************************************************************************************************/
/**
 * @brief      capi_command_request(capi_phy_info_t*     phy_info_ptr,
 *                                  capi_command_info_t* cmd_inf_ptr,
 *                                  uint16_t             payload_size)
 * @details    This API is used to send command to interface.
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  cmd_inf_ptr: a pointer which carries command 
 * @param[in]  payload_size: size of the payload type
 * @param[in]  config_cmd: the command configuration flag for set or get configuration
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_command_request(capi_phy_info_t*     phy_info_ptr,
                                     capi_command_info_t* cmd_inf_ptr,
                                     uint16_t             payload_size,
                                     uint8_t              config_cmd)
{
    return_result_t return_result = RR_SUCCESS;

    CAPI_LOG_FUNC("capi_command_request()", phy_info_ptr);
    CAPI_LOG_COMMAND_INFO(cmd_inf_ptr);

    CMD_SANITY_CHECK(phy_info_ptr, cmd_inf_ptr, CHK_USR_INPUT);

    return_result = intf_capi2fw_command_Handler(phy_info_ptr,
                                                 cmd_inf_ptr->command_id,
                                                 (uint8_t*)&cmd_inf_ptr->type.payload,
                                                 payload_size,
                                                 config_cmd);

    if (config_cmd == GET_CONFIG && return_result == RR_SUCCESS) {
        CMD_SANITY_CHECK(phy_info_ptr, cmd_inf_ptr, CHK_FW_OUTPUT);
    }

    return (return_result);
}

/************************************************************************************************************************/
/************************************************************RESET*******************************************************/
/************************************************************************************************************************/
#define GP_CFG_NUM   128
#define RST_DELAY_MS 2000
/**
* @brief        capi_reset(capi_phy_info_t* phy_info_ptr, capi_reset_mode_t* reset_mode_ptr)
* @details      This API is used to reset the chip
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    reset_mode_ptr: this pointer carries the necessary information for reseting procedure
*                               defined by capi_reset_mode_t
*
* @return       returns the  result of the called method/function
*/
return_result_t capi_reset(capi_phy_info_t* phy_info_ptr, capi_reset_mode_t* reset_mode_ptr)
{
    uint8_t    i = 0;
    phy_info_t fw_phy_core_top_reg;
    return_result_t ret_code = RR_SUCCESS;
    uint32_t read_value=0;
    

    CAPI_LOG_FUNC("capi_reset", phy_info_ptr);
    CAPI_LOG_INFO("reset mode : %s\n", util_capi_reset_mode_str(*reset_mode_ptr));

    util_memcpy(&fw_phy_core_top_reg, phy_info_ptr, sizeof(phy_info_t));
    phy_info_ptr->base_addr = MGT;
    fw_phy_core_top_reg.base_addr = OCTAL_TOP_REGS;

    switch(*reset_mode_ptr) {

    /* boot_en_in pin is set */
    case  CAPI_SOFT_RESET_MODE:
        phy_info_ptr->base_addr = MGT;
        hsip_wr_field_(&fw_phy_core_top_reg, REG_BOOT_EN_CTRL, BOOT_EN_VAL, 0x0);
        hsip_wr_field_(&fw_phy_core_top_reg, REG_BOOT_EN_CTRL, BOOT_EN_OVR, 0x1);

        hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                   PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P0_SRST_EN_MASK);
        
        ERR_HSIP(read_value = hsip_rd_reg_(&fw_phy_core_top_reg, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION));

        for (i=0; i<GP_CFG_NUM; i++) {
            if((i!=3) && (i!=4) && (i!=5))
            hsip_wr_reg_(&fw_phy_core_top_reg, QUAD_CORE_CONFIG_CFGVAL_0_RDB + i*4, 0);
        }

        hsip_wr_field_(&fw_phy_core_top_reg, QUAD_CORE_EGRMGT_QC_CHIP_SW_RST_RDB, MD_CHIP_SW_RST, 0x1);
        
        /* Wait for 1ms. */
        delay_ms(1);
       
        hsip_wr_reg_(&fw_phy_core_top_reg, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION, read_value);

        hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                   PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P0_SRST_EN_MASK);
        hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x0);    /* disable remap */

        host_set_clock(phy_info_ptr);

        hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                   PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P1_SRST_EN_MASK);
    break;
    /* boot_en_in pin is set */
    case  CAPI_HARD_RESET_MODE:
        hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                   PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P0_SRST_EN_MASK);

        hsip_wr_field_(&fw_phy_core_top_reg, REG_BOOT_EN_CTRL, BOOT_EN_VAL, 0x1);
        hsip_wr_field_(&fw_phy_core_top_reg, REG_BOOT_EN_CTRL, BOOT_EN_OVR, 0x1);

        /* clear all gpcfg regs for a clean env for now */
        for (i=0; i<GP_CFG_NUM; i++) {
            hsip_wr_reg_(&fw_phy_core_top_reg, QUAD_CORE_CONFIG_CFGVAL_0_RDB + i*4, 0);
        }

        hsip_wr_field_(&fw_phy_core_top_reg, QUAD_CORE_EGRMGT_QC_CHIP_SW_RST_RDB, MD_CHIP_SW_RST, 0x1);

    break;

    default:
        ret_code = RR_ERROR_WRONG_INPUT_VALUE;
    break;

    }

    return (ret_code);
}

#ifndef CAPI_IMAGE_DOWNLOAD_DISABLED

/************************************************************************************************************************/
/**********************************************************DOWNLOAD******************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_download(capi_phy_info_t*      phy_info_ptr,
*                             capi_download_info_t* download_info_ptr)
* @details      This API is used to download firmware 
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    download_info_ptr: This pointer did not return download status because this API only download firmware
*               please use capi_get_download_status to get download status.
* 
* @return       returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
*/

return_result_t capi_download(capi_phy_info_t*      phy_info_ptr,
                              capi_download_info_t* download_info_ptr)
{
    return_result_t ret = RR_SUCCESS;

    CAPI_LOG_FUNC("capi_download", phy_info_ptr);
    CAPI_LOG_INFO("mode : %s\n", util_capi_download_mode_str(download_info_ptr->mode));

    switch (download_info_ptr->mode) {

    case CAPI_DOWNLOAD_MODE_MDIO_EEPROM:
    case CAPI_DOWNLOAD_MODE_I2C_EEPROM:
        if((download_info_ptr->image_info.image_ptr == NULL) || (download_info_ptr->image_info.image_size==0)) {
             download_info_ptr->image_info.image_ptr=(uint32_t*) &wholeimage_spi[0];
             download_info_ptr->image_info.image_size=sizeof(wholeimage_spi);
            if (download_info_ptr->spi_program_mode==CAPI_SPI_PROGRAM_FAST)
                ret = host_download_spi_internal_fast(phy_info_ptr, download_info_ptr);
            else 
                ret = host_download_spi_internal(phy_info_ptr, download_info_ptr);
        }
        else {
            if (download_info_ptr->spi_program_mode==CAPI_SPI_PROGRAM_FAST)
                ret = host_download_spi_internal_fast(phy_info_ptr, download_info_ptr);
            else 
                ret = host_download_spi_internal(phy_info_ptr, download_info_ptr);
        }
        break;

    case CAPI_DOWNLOAD_MODE_MDIO_SRAM:
    case CAPI_DOWNLOAD_MODE_I2C_SRAM:
           phy_info_ptr->base_addr = MGT;
           hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                      PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                      PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                      PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                      PCR_RST_CTR_M0P0_SRST_EN_MASK);
           phy_info_ptr->base_addr = HOST_BASE; // BLACKHAWK_COM;
           hsip_wr_reg_(phy_info_ptr, PMD_BHW_PMD_SW_RESET_REGISTER,
                                      PMD_BHW_PMD_SW_RESET_REGISTER_SW_RSTB_MASK);

        if((download_info_ptr->image_info.image_ptr == NULL) || (download_info_ptr->image_info.image_size==0)) {
            download_info_ptr->image_info.image_ptr=(uint32_t*) &wholeimage_sram[0];
            download_info_ptr->image_info.image_size=sizeof(wholeimage_sram);
        }
        if (download_info_ptr->sram_program_mode==CAPI_SRAM_PROGRAM_FAST)
            ret = host_download_sram_fast(phy_info_ptr, download_info_ptr);
        else
            ret = host_download_sram(phy_info_ptr,
                                     download_info_ptr->image_info.image_ptr,
                                     download_info_ptr->image_info.image_size,
                                     download_info_ptr->phy_static_config_ptr);
        break;
    default:
        return (RR_ERROR_WRONG_INPUT_VALUE);
    }
    return ret;
}

return_result_t capi_pre_sram_download(capi_phy_info_t*  phy_info_ptr) {
      return (host_pre_sram_download(phy_info_ptr));
}


/**
* @brief        capi_get_download_status(capi_phy_info_t*      phy_info_ptr,
*                                        capi_download_info_t* download_info_ptr)
* @details      This API is used to get firmware download status
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    download_info_ptr: this pointer carries the necessary information for downloading status
*                                  defined by capi_download_info_t
* 
* @return       returns the  result of the called method/function, either RR_ERROR or RR_SUCCESS
*/
return_result_t capi_get_download_status(capi_phy_info_t*      phy_info_ptr,
                                         capi_download_info_t* download_info_ptr)
{
    uint8_t num_of_phy = 1;
    uint8_t phy_num = 0;
    uint32_t phy_id;
    uint32_t read_version = 0, read_version_minor = 0;
    uint32_t read_crc = 0; 
    phy_info_t fw_phy;  
    uint32_t read_value =0;

    CAPI_LOG_FUNC("capi_get_download_status", phy_info_ptr);
    CAPI_LOG_INFO("download mode = %s\n", util_capi_download_mode_str(download_info_ptr->mode));

    download_info_ptr->status.crc_check = 0;

    util_memset((void *)&fw_phy, 0, sizeof(capi_phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(capi_phy_info_t));

    phy_id = phy_info_ptr->phy_id;
    phy_info_ptr->base_addr = 0;

    ERR_HSIP(download_info_ptr->status.crc_check = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_FIRMWARE_CRC_REG));

    download_info_ptr->status.result = TRUE;
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    ERR_HSIP(read_version = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_FIRMWARE_VERSION_REG));
    ERR_HSIP(read_version_minor = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_FIRMWARE_VERSION_MINOR_REG_ADDR));

    download_info_ptr->status.version     = read_version;
    download_info_ptr->status.sub_version = read_version_minor;
    for (phy_num = 0; phy_num <  num_of_phy; phy_num++){
        CAPI_LOG_INFO("capi_get_download_status( phy = %x )\n", phy_num);
        fw_phy.phy_id = phy_id + phy_num;
        fw_phy.base_addr = OCTAL_TOP_REGS;
        ERR_HSIP(read_version  = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_VERSION_REG));
        ERR_HSIP(read_version_minor = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_FIRMWARE_VERSION_MINOR_REG_ADDR));
        ERR_HSIP(read_value = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION));
        fw_phy.base_addr = 0;
        ERR_HSIP(read_crc = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_CRC_REG));

        switch (download_info_ptr->mode) {
        case CAPI_DOWNLOAD_MODE_MDIO_SRAM:
        case CAPI_DOWNLOAD_MODE_I2C_SRAM:
        default:
            if (read_version != FIRMWARE_VERSION){
                download_info_ptr->status.result = FALSE;
                CAPI_LOG_ERROR("Not matching! FW version: 0x%x vs read_version: 0x%x \n",\
                               FIRMWARE_VERSION, read_version);
                return (RR_ERROR_FW_VERSION_MISMATCH);
            }
            if (read_version_minor != FIRMWARE_VERSION_MINOR){
                download_info_ptr->status.result = FALSE;
                CAPI_LOG_ERROR("Not matching! FW version: 0x%x vs read_version_minor: 0x%x \n",\
                               FIRMWARE_VERSION_MINOR, read_version_minor);
                return (RR_ERROR_FW_VERSION_MISMATCH);
            }
            if (read_crc != CRC32_CHECK){
                download_info_ptr->status.result = FALSE;
                CAPI_LOG_ERROR("Not matching! crc: 0x%x vs read_crc: 0x%x\n",\
                                 CRC32_CHECK, read_crc);
                return (RR_ERROR_CRC32_MISMATCH);
            }
            if(read_value!= 0x600d){
                CAPI_LOG_INFO(" First download status wrong \n");
                return (RR_ERROR_CLIENT_CRC_MISMATCH);
            }

            /*else*/
            download_info_ptr->status.result = TRUE;
            break;

        case CAPI_DOWNLOAD_MODE_MDIO_EEPROM:
        case CAPI_DOWNLOAD_MODE_I2C_EEPROM:
            if (read_version != FIRMWARE_VERSION){
                download_info_ptr->status.result = FALSE;
                CAPI_LOG_ERROR("Not matching! FW version: 0x%x vs read_version: 0x%x \n",\
                               FIRMWARE_VERSION, read_version);
                return (RR_ERROR_FW_VERSION_MISMATCH);
            }
            if (read_version_minor != FIRMWARE_VERSION_MINOR){
                download_info_ptr->status.result = FALSE;
                CAPI_LOG_ERROR("Not matching! FW version: 0x%x vs read_version_minor: 0x%x \n",\
                               FIRMWARE_VERSION_MINOR, read_version_minor);
                return (RR_ERROR_FW_VERSION_MISMATCH);
            }
            if (read_crc != CRC32_CHECK_SPI){
                download_info_ptr->status.result = FALSE;
                CAPI_LOG_ERROR("Not matching! spi_crc: 0x%x vs read_crc: 0x%x\n",\
                                 CRC32_CHECK_SPI, read_crc);
                return (RR_ERROR_CRC32_SPI_MISMATCH);
            }
            ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION));
            if(read_value!= 0x600d){
                CAPI_LOG_INFO(" First download status wrong \n");
                return (RR_ERROR_CLIENT_CRC_MISMATCH);
            }

            /*else*/
            download_info_ptr->status.result = TRUE;
            break;
        }
    }
    CAPI_LOG_INFO("get download status success\n");
    return (RR_SUCCESS);
}

/**
* @brief    capi_get_firmware_status(capi_phy_info_t* phy_info_ptr, capi_status_info_t* status_info_ptr)
* @details  This API is used checking the firmware status
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   status_info_ptr: this pointer retuns the firmware status info defined by capi_status_info_t
* 
* @return    returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
*/
return_result_t capi_get_firmware_status(capi_phy_info_t* phy_info_ptr, capi_status_info_t* status_info_ptr)
{
    phy_info_t       fw_phy;
    uint32_t major_version, minior_version;
    uint32_t read_crc = 0;
    uint32_t read_value =0;

    CAPI_LOG_FUNC("capi_get_firmware_status", phy_info_ptr);

    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));
    fw_phy.base_addr = OCTAL_TOP_REGS;

    fw_phy.phy_id = phy_info_ptr->phy_id;
    ERR_HSIP(major_version = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_VERSION_REG));
    ERR_HSIP(minior_version = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_VERSION_MINOR_REG_ADDR));
    ERR_HSIP(read_value = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION));
  
    fw_phy.base_addr = 0;
    ERR_HSIP(read_crc = hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_CRC_REG));
    status_info_ptr->crc_check = read_crc;

    status_info_ptr->version     = major_version;
    status_info_ptr->sub_version = minior_version;

    if ((major_version == FIRMWARE_VERSION) && (minior_version == FIRMWARE_VERSION_MINOR)) {
        status_info_ptr->result = true;
    }
    else {
        status_info_ptr->result = false;      
        PHYMOD_DIAG_OUT(("Not matching! FW version: 0x%x_%x - read_version: 0x%x_%x \n",\
                    FIRMWARE_VERSION, FIRMWARE_VERSION_MINOR,
                    major_version, minior_version));

        return (RR_ERROR_FW_VERSION_MISMATCH);
    }

    if (read_crc != CRC32_CHECK) {
        status_info_ptr->result = false;           
        PHYMOD_DIAG_OUT(("Not matching! crc: 0x%x - read_crc: 0x%x\n", CRC32_CHECK, read_crc));
        return (RR_ERROR_CRC32_MISMATCH);
    }

    if(read_value!= 0x600d) {
        status_info_ptr->result = false; 
        PHYMOD_DIAG_OUT(("Not matching! BH crc: 0x600d - read_crc: 0x%x\n", read_crc));
        return (RR_ERROR_CLIENT_CRC_MISMATCH);
     }

    status_info_ptr->result = TRUE; 

    CAPI_LOG_INFO("_capi_get_firmware_status(version = %x, crc = 0x%x)\n", status_info_ptr->version, status_info_ptr->crc_check);
    return (RR_SUCCESS);
}

return_result_t capi_set_broadcast(capi_phy_info_t* phy_info_ptr, capi_broadcast_intf_t* broadcast_inft_ptr) {

    capi_phy_info_t local_phy_info;
    broadcast_enable_t broadcast_enable = broadcast_inft_ptr->enable;

    CAPI_LOG_FUNC("capi_set_broadcast", phy_info_ptr);
    CAPI_LOG_INFO("broadcast_enable : %d\n", broadcast_enable);

    util_memcpy((void *)&local_phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info_ptr->base_addr = 0x00;

    switch (broadcast_inft_ptr->bus)
    {
        case CAPI_ACCESS_MODE_MDIO:
            if (broadcast_enable == BROADCAST_ENABLE) {
                local_phy_info.base_addr = 0x00;
                hsip_wr_reg16_(&local_phy_info, 0x1F000B, 0x1);
                delay_ms(10);
            }
            else if (broadcast_enable == BROADCAST_DISABLE) {
                 local_phy_info.base_addr = 0x00;
                 hsip_wr_reg16_(&local_phy_info, 0x1F000B, 0x0);
                 delay_ms(10);
            }
            break;
        default:
            break;
    }
    return (RR_SUCCESS);
}

broadcast_enable_t capi_get_broadcast(capi_phy_info_t* phy_info_ptr) {

    CAPI_LOG_FUNC("capi_get_broadcast", phy_info_ptr);

    phy_info_ptr->base_addr = 0x00;
    if (hsip_rd_reg16_(phy_info_ptr, 0x1F000B)) {
        return BROADCAST_ENABLE;
    }
    else {
        return BROADCAST_DISABLE;
    }
    return BROADCAST_DISABLE;
}

#endif /* CAPI_IMAGE_DOWNLOAD_DISABLED */

/************************************************************************************************************************/
/****************************************************GET TEMPERTURE******************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_get_temperture_status(capi_phy_info_t*         phy_info_ptr,
*                                          capi_temp_status_info_t* temp_status_info_ptr)
* @details      This API is used obtin the temperture in degree C
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    capi_temp_status_info_ptr: this parameter pointed to the temperature infor defined by 
*                                          capi_temp_status_info_t
* 
* @return       returns the result of the called method/function
*/
return_result_t capi_get_temperture_status(capi_phy_info_t*         phy_info_ptr,
                                           capi_temp_status_info_t* capi_temp_status_info_ptr)
{
    return_result_t ret = RR_ERROR;
    chip_top_chip_temp_reading_reg_t chip_top_chip_temp_reading_reg;
    uint32_t count = 200;

    CAPI_LOG_FUNC("capi_get_temperture_status", phy_info_ptr);
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_reg_(phy_info_ptr, CHIP_TOP_CHIP_TEMP_READING_REG, 0x0);

    do {
        ERR_HSIP(chip_top_chip_temp_reading_reg.words = (uint16_t)hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_TEMP_READING_REG));
        if (chip_top_chip_temp_reading_reg.fields.temp_reading_valid == 1)
            break;
        delay_ms(10);
        count--;
    } while (count > 0);

    CAPI_LOG_INFO("capi_get_temperture(valid: %d, Temp:%d) \n", chip_top_chip_temp_reading_reg.fields.temp_reading_valid, chip_top_chip_temp_reading_reg.fields.temp_reading);
    capi_temp_status_info_ptr->temperature = 0;
    if (chip_top_chip_temp_reading_reg.fields.temp_reading_valid == 1) {
        ret = RR_SUCCESS;
         if ((chip_top_chip_temp_reading_reg.fields.temp_reading & 0x7000) != 0x7000) {
            capi_temp_status_info_ptr->temperature = (int16_t)(chip_top_chip_temp_reading_reg.fields.temp_reading);
        } else {
            capi_temp_status_info_ptr->temperature = (int16_t)(chip_top_chip_temp_reading_reg.words);
        }
    }

    return ret;
}

/************************************************************************************************************************/
/****************************************************SET AND GET AVS*****************************************************/
/************************************************************************************************************************/
/**
* @brief        capi_set_avs_config(capi_phy_info_t*             phy_info_ptr,
*                                   capi_avs_mode_config_info_t* avs_config_ptr)
* @details      This API is used to set the AVS configuration
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    avs_config_ptr: this pointer contains AVS mode configuration defined by capi_avs_mode_config_info_t
* 
* @return       returns the result of the called method/function
*/
return_result_t capi_set_avs_config(capi_phy_info_t*             phy_info_ptr,
                                    capi_avs_mode_config_info_t* avs_config_ptr)
{
    chip_top_avs_mode_config_reg_t chip_top_avs_mode_config;
    CAPI_LOG_FUNC("capi_set_avs_config", phy_info_ptr);
    HOST_SANITY_CHECK(phy_info_ptr, (void*)avs_config_ptr, SANITY_AVS_CONFIG_ID);

    chip_top_avs_mode_config.words = 0;
    chip_top_avs_mode_config.fields.avs_enable = avs_config_ptr->avs;
    chip_top_avs_mode_config.fields.disable_type = avs_config_ptr->disable_type;
    chip_top_avs_mode_config.fields.avs_regulator_control_mode = avs_config_ptr->avs_ctrl;
    chip_top_avs_mode_config.fields.num_of_phy = avs_config_ptr->package_share;
    chip_top_avs_mode_config.fields.avs_dc_margin = avs_config_ptr->board_dc_margin;
    chip_top_avs_mode_config.fields.type_of_regulator = avs_config_ptr->type_of_regulator;
  
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_reg_(phy_info_ptr, CHIP_TOP_CHIP_AVS_MODE_CONFIG_REG, chip_top_avs_mode_config.words);
    CAPI_LOG_INFO("capi_set_avs_config:%x\n", chip_top_avs_mode_config.words);
    return (RR_SUCCESS);
}

/**
* @brief        capi_get_avs_config(capi_phy_info_t*             phy_info_ptr,
*                                   capi_avs_mode_config_info_t* avs_config_ptr)
* @details      This API is used to set the AVS configuration
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    avs_config_ptr: this pointer contains AVS mode configuration defined by 
*                               capi_avs_mode_config_info_t
* 
* @return       returns the result of the called method/function
*/
return_result_t capi_get_avs_config(capi_phy_info_t*             phy_info_ptr,
                                    capi_avs_mode_config_info_t* avs_config_ptr)
{
    chip_top_avs_mode_config_reg_t chip_top_avs_mode_config;
    CAPI_LOG_FUNC("capi_get_avs_config", phy_info_ptr);

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    ERR_HSIP(chip_top_avs_mode_config.words = (uint16_t) hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_AVS_MODE_CONFIG_REG));
    CAPI_LOG_INFO("capi_get_avs_config:%x\n", chip_top_avs_mode_config.words);
    util_memset((void*)avs_config_ptr, 0, sizeof(capi_avs_mode_config_info_t));

    avs_config_ptr->avs = (capi_enable_t)chip_top_avs_mode_config.fields.avs_enable;

    avs_config_ptr->package_share     = (capi_avs_pack_share_t)chip_top_avs_mode_config.fields.num_of_phy;
    avs_config_ptr->board_dc_margin   = (capi_board_dc_margin_t)chip_top_avs_mode_config.fields.avs_dc_margin;
    avs_config_ptr->avs_ctrl          = (capi_avs_ctrl_t)chip_top_avs_mode_config.fields.avs_regulator_control_mode;
    avs_config_ptr->type_of_regulator = (capi_type_of_regulator_t)chip_top_avs_mode_config.fields.type_of_regulator;
    avs_config_ptr->disable_type      = (capi_avs_disable_type_t)chip_top_avs_mode_config.fields.disable_type;
    return (RR_SUCCESS);
}

/**
* @brief        capi_get_avs_status(capi_phy_info_t*   phy_info_ptr,
*                                   capi_avs_status_t* capi_avs_status_ptr)
* @details      This API is used to set the AVS configuration
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    capi_avs_status_ptr: this pointer contains AVS mode configuration defined by 
*                                      capi_avs_status_t, also dump avs status if AVS_TRACE_DEBUG is enabled
* 
* @return       returns the result of the called method/function
*/
return_result_t capi_get_avs_status(capi_phy_info_t*   phy_info_ptr,
                                    capi_avs_status_t* capi_avs_status_ptr)
{
    chip_top_avs_mode_config_reg_t chip_top_avs_mode_config;

    CAPI_LOG_FUNC("capi_get_avs_status", phy_info_ptr);

    util_memset((void*)capi_avs_status_ptr, 0, sizeof(capi_avs_status_t));
    util_memset((void*)&chip_top_avs_mode_config, 0, sizeof(chip_top_avs_mode_config_reg_t));

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;

    ERR_HSIP(chip_top_avs_mode_config.words = (uint16_t) hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_AVS_MODE_CONFIG_REG));
    capi_avs_status_ptr->avs_result = (avs_result_t) chip_top_avs_mode_config.fields.avs_status;
    capi_avs_status_ptr->avs_enable = (uint8_t) chip_top_avs_mode_config.fields.avs_enable;
    return RR_SUCCESS;
}

/**
 * @brief       capi_set_voltage_config(capi_phy_info_t*                  phy_info_ptr,
 *                                      capi_fixed_voltage_config_info_t* fixed_config_ptr)
 * @details     This API is used to set the fixed volatge 
 *
 * @param[in]   phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]   fixed_config_ptr: this pointer contains configuration defined by capi_fixed_voltage_config_info_t
 * 
 * @return      returns the result of the called method/function
 */
return_result_t capi_set_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                        capi_fixed_voltage_config_info_t* fixed_config_ptr)
{
    chip_top_fixed_voltage_config_reg_t chip_top_fixed_voltage_config_reg;
    uint16_t value = 0;
    capi_phy_info_t fw_phy;
    uint32_t tries = 0;

    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));
    fw_phy.base_addr = OCTAL_TOP_REGS;
    chip_top_fixed_voltage_config_reg.words = 0;
    chip_top_fixed_voltage_config_reg.fields.fixed_enable = fixed_config_ptr->fixed_voltage_enable;    
    chip_top_fixed_voltage_config_reg.fields.type_of_regulator = fixed_config_ptr->type_of_regulator;    

    if((fixed_config_ptr->fixed_voltage==0) || (fixed_config_ptr->fixed_voltage < 500)){
        value = 500;
    }
    else {
        value = fixed_config_ptr->fixed_voltage;
    }

    hsip_wr_reg_(&fw_phy, CHIP_TOP_CHIP_FIXED_VOLTAGE_INPUT_REG, value);
    hsip_wr_reg_(&fw_phy, CHIP_TOP_CHIP_FIXED_VOLTAGE_CONFIG_REG,  chip_top_fixed_voltage_config_reg.words);
    CAPI_LOG_INFO("capi_set_fixed voltage_config:%x\n", chip_top_fixed_voltage_config_reg.words);

    do  {
       ERR_HSIP(chip_top_fixed_voltage_config_reg.words = (uint16_t)hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIXED_VOLTAGE_CONFIG_REG));
       if ((chip_top_fixed_voltage_config_reg.fields.fixed_enable==0) && (chip_top_fixed_voltage_config_reg.fields.set_status==1)){
           hsip_wr_reg_(&fw_phy, CHIP_TOP_CHIP_FIXED_VOLTAGE_CONFIG_REG,  0x0);
          return(RR_SUCCESS);

       }
       delay_ms(100);
       tries++;
    } while(tries < 100);

    if (tries >=100) {
        fw_phy.base_addr = OCTAL_TOP_REGS;
        hsip_wr_reg_(&fw_phy, CHIP_TOP_CHIP_FIXED_VOLTAGE_CONFIG_REG,  0x0);
        return(RR_ERROR);
    }

    hsip_wr_reg_(&fw_phy, CHIP_TOP_CHIP_FIXED_VOLTAGE_CONFIG_REG,  0x0);
    return(RR_SUCCESS);
}


return_result_t capi_set_0p75v_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                              capi_other_voltage_config_info_t* fixed_config_ptr)
{

    uint16_t value = 0;
    capi_phy_info_t fw_phy;
    uint32_t read_value =0;
    phy_info_t fw_phy_core_top_reg;
    return_result_t ret =0;
    uint32_t firmware_flag =0;

    uint32_t try_counter =0;

    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));
    util_memcpy(&fw_phy_core_top_reg, phy_info_ptr, sizeof(phy_info_t));
   
    fw_phy_core_top_reg.base_addr = OCTAL_TOP_REGS;
    fw_phy_core_top_reg.phy_id = phy_info_ptr->phy_id;


    if((fixed_config_ptr->fixed_voltage_set==0)){
        value = 800;
    }
    else {
        value = fixed_config_ptr->fixed_voltage_set;
    }

     fw_phy.base_addr = MGT;
   
     power_util_ana_vddm_set_volt(&fw_phy, (value* 1000));
     power_util_ana_vddm_get_volt(&fw_phy, &read_value);
     fixed_config_ptr->fixed_voltage_read = read_value/1000;


     return(RR_SUCCESS);

}

return_result_t capi_set_0p9v_voltage_config(capi_phy_info_t*                  phy_info_ptr,
                                             capi_other_voltage_config_info_t* fixed_config_ptr)
{

    uint16_t value = 0;
    capi_phy_info_t fw_phy;
    uint32_t read_value =0;
    return_result_t ret =0;
   
    uint32_t try_counter =0;
    uint32_t firmware_flag =0;

    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));


    if((fixed_config_ptr->fixed_voltage_set==0)){
        value = 900;
    }
    else {
        value = fixed_config_ptr->fixed_voltage_set;
    }

     fw_phy.base_addr = MGT;

     power_util_ana_avdd_set_volt(&fw_phy, (value* 1000));
     power_util_ana_avdd_get_volt(&fw_phy, &read_value);
     fixed_config_ptr->fixed_voltage_read = read_value/1000;

     return(RR_SUCCESS);
}

/**
* @brief        capi_get_voltage_status(capi_phy_info_t*            phy_info_ptr,
*                                       capi_voltage_status_info_t* capi_voltage_status_info_ptr)
* @details      This API is used obtin the voltage in MV
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]    capi_voltage_status_info_ptr: this parameter pointed to the volatge infor defined by 
*                                               capi_voltage_status_info_t
*
* @return       returns the result of the called method/function
*/
return_result_t capi_get_voltage_status(capi_phy_info_t*            phy_info_ptr,
                                        capi_voltage_status_info_t* capi_voltage_status_info_ptr)
{
    return_result_t ret = RR_ERROR;
    chip_top_chip_volt_reading_reg_t chip_top_chip_volt_reading_reg;

    CAPI_LOG_FUNC("capi_get_voltage_status", phy_info_ptr);
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_reg_(phy_info_ptr, CHIP_TOP_CHIP_VOLT_READING_REG, 0x0);
    delay_ms(2000);

    ERR_HSIP(chip_top_chip_volt_reading_reg.words = (uint16_t)hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_VOLT_READING_REG));

    CAPI_LOG_INFO("capi_get_voltage(valid: %d, volatage:%d) \n",
                  chip_top_chip_volt_reading_reg.fields.voltage_reading_valid,
                  chip_top_chip_volt_reading_reg.fields.volatge_reading);

    capi_voltage_status_info_ptr->voltage = 0;

    if (chip_top_chip_volt_reading_reg.fields.voltage_reading_valid == 1) {
        ret = RR_SUCCESS;
        capi_voltage_status_info_ptr->voltage = (uint16_t)(chip_top_chip_volt_reading_reg.fields.volatge_reading);
    }

    return ret;
}


/************************************************************************************************************************/
/*********************************************SET/GET CHIP COMMAND INFO**************************************************/
/************************************************************************************************************************/
/**
 * @brief      capi_set_chip_command(capi_phy_info_t*          phy_info_ptr,
 *                                   capi_chip_command_info_t* chip_cmd_info_ptr)
 * @details    This API is used to invoke chip set command
 *
 * @param[in]  phy_info_ptr: a reference to the phy information object
 * @param[in]  chip_cmd_inf_ptr: a reference to the chip command object
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_set_chip_command(capi_phy_info_t*          phy_info_ptr,
                                      capi_chip_command_info_t* chip_cmd_info_ptr)
{
    uint16_t            payload_size;
    capi_command_info_t command_info;
    capi_phy_info_t     phy_info;
    uint16_t            chk_cnt = 0, ddcc_cfg_en;

    util_memset(&command_info, 0x0, sizeof(capi_command_info_t));
    command_info.command_id = chip_cmd_info_ptr->command_id;

    switch(chip_cmd_info_ptr->command_id) {
        case COMMAND_ID_SET_DSP_POWER_INFO:
            command_info.type.dsp_power_info = chip_cmd_info_ptr->type.dsp_power_info;
            payload_size = sizeof(capi_power_info_t);
            break;

        case COMMAND_ID_SET_RPTR_INDEP_HDL:
            util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
            phy_info.base_addr = OCTAL_TOP_REGS;
            hsip_wr_field_(&phy_info, FW_INTERNAL_CONFIG_REG_1, EN_RPTR_INDEP_LANE_HANDLE,
                chip_cmd_info_ptr->type.indep_ctrl.indep_ctrl_en);
            return RR_SUCCESS;

        case COMMAND_ID_SET_LINE_PLL_DDCC_CFG:
            util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
            phy_info.base_addr = OCTAL_TOP_REGS;
            hsip_wr_field_(&phy_info, FW_INTERNAL_CONFIG_REG_3, ENABLE_PLL_DDCC_CFG, 
                chip_cmd_info_ptr->type.line_pll_ctrl.ddcc_cfg_en);
            phy_info.base_addr = host_util_get_lane_lw_top_pam_bbaddr(0);
            hsip_wr_field_(&phy_info, TOP_PLL_CAL_CTRL_0, PLLCAL_EN_CALIB_N, 1);
            do {
                ERR_HSIP(ddcc_cfg_en = hsip_rd_field_(&phy_info, TOP_PLL_CAL_CTRL_0, PLLCAL_EN_CALIB_N));
                chk_cnt++;
                delay_ms(10);
            } while (ddcc_cfg_en && chk_cnt < 100);
            if (ddcc_cfg_en)
                return RR_ERROR;
            else
                return RR_SUCCESS;

        case COMMAND_ID_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH:
            command_info.type.optrxlos_host_fast_tx_squelch = chip_cmd_info_ptr->type.optrxlos_host_fast_tx_squelch;
            payload_size = sizeof(uint8_t);
            break;

        case COMMAND_ID_SET_SERDES_LANE_CDR_MODE:
            command_info.type.serdes_lane_cdr_mode = chip_cmd_info_ptr->type.serdes_lane_cdr_mode;
            payload_size = sizeof(capi_lane_cdr_mode_t);
            break;

        default:
            return RR_ERROR_WRONG_INPUT_VALUE;
    }

    return (capi_command_request(phy_info_ptr, &command_info, payload_size, SET_CONFIG));
}


/**
 * @brief      capi_get_chip_command(capi_phy_info_t*          phy_info_ptr,
 *                                   capi_chip_command_info_t* chip_cmd_inf_ptr)
 * @details    This API is used to invoke chip get command
 *
 * @param[in]  phy_info_ptr: a reference to the phy information object
 * @param[in]  chip_cmd_inf_ptr: a reference to the chip command object 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_get_chip_command(capi_phy_info_t*          phy_info_ptr,
                                      capi_chip_command_info_t* chip_cmd_inf_ptr)
{
    uint16_t            payload_size;
    return_result_t     return_result = RR_SUCCESS;
    capi_command_info_t command_info;
    capi_phy_info_t     phy_info;

    command_info.command_id = chip_cmd_inf_ptr->command_id;

    switch(chip_cmd_inf_ptr->command_id) {
        case COMMAND_ID_GET_LOW_POWER_MODE:
            return (host_get_lpm_st(phy_info_ptr, &chip_cmd_inf_ptr->type.lpm_info));

        case COMMAND_ID_GET_DSP_POWER_INFO:
            command_info.type.dsp_power_info = chip_cmd_inf_ptr->type.dsp_power_info;
            payload_size = sizeof(capi_power_info_t);
            break;

        case COMMAND_ID_GET_RPTR_INDEP_HDL:
            util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
            phy_info.base_addr = OCTAL_TOP_REGS;
            ERR_HSIP(chip_cmd_inf_ptr->type.indep_ctrl.indep_ctrl_en = (capi_enable_t)hsip_rd_field_(&phy_info, 
                FW_INTERNAL_CONFIG_REG_1, EN_RPTR_INDEP_LANE_HANDLE));
            return RR_SUCCESS;

        case COMMAND_ID_GET_LINE_PLL_DDCC_CFG:
            util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
            phy_info.base_addr = OCTAL_TOP_REGS;
            ERR_HSIP(chip_cmd_inf_ptr->type.line_pll_ctrl.ddcc_cfg_en = (capi_enable_t)hsip_rd_field_(&phy_info, 
                FW_INTERNAL_CONFIG_REG_3, ENABLE_PLL_DDCC_CFG));
            return RR_SUCCESS;

        case COMMAND_ID_GET_SERDES_LANE_CDR_MODE:
            command_info.type.serdes_lane_cdr_mode = chip_cmd_inf_ptr->type.serdes_lane_cdr_mode;
            payload_size = sizeof(capi_lane_cdr_mode_t);
            break;
        default:
            return RR_ERROR_WRONG_INPUT_VALUE;
    }

    return_result = capi_command_request(phy_info_ptr, &command_info, payload_size, GET_CONFIG);

    if (return_result == RR_SUCCESS) {
        //CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
        switch (chip_cmd_inf_ptr->command_id) {
            case COMMAND_ID_GET_DSP_POWER_INFO:
                chip_cmd_inf_ptr->type.dsp_power_info = command_info.type.dsp_power_info;
                break;
            case COMMAND_ID_GET_SERDES_LANE_CDR_MODE:
                chip_cmd_inf_ptr->type.serdes_lane_cdr_mode = command_info.type.serdes_lane_cdr_mode;
                break;
            default:
                return RR_ERROR_WRONG_INPUT_VALUE;
        }
    }

    return return_result;
}

/**
 * @brief      capi_get_status(capi_phy_info_t* phy_info_ptr, capi_gpr_lane_status_t* status_ptr)
 * @details    Get lane status from GP registers
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr : phy information
 * @param      status_ptr : output gpr lane status
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t capi_get_status(capi_phy_info_t* phy_info_ptr, capi_status_t* status_ptr)
{
    return_result_t return_result = RR_SUCCESS;
    capi_phy_info_t phy_info;
    uint16_t        reg_value     = 0;
    uint32_t        lane_reg_addr = 0;
    uint8_t         lane_idx      = 0;
    uint16_t        cdr_lock_reg_value;

    util_memcpy(&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info.base_addr = OCTAL_TOP_REGS;

    if ((phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES) || (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES)) {

        switch((capi_status_type_t)status_ptr->status_type)
        {
            case GPR_LANE_CDR_LOCK_STATUS:
                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_CDR_LOCK_STATUS_GPREG));
                status_ptr->param.cdr.lock_status = reg_value & phy_info.lane_mask;
                break;

            case GPR_LANE_LOL_LOS_STATUS:
                {
                    uint8_t  loop_counter = 0;
                    uint16_t lol_sticky;

                    ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_LOL_LOS_STATUS_GPREG));

                    status_ptr->param.los.los_status = (reg_value & 0x00FF) & phy_info.lane_mask;
                    lol_sticky = (0xFF00 & reg_value) & (phy_info.lane_mask<<8);
                    status_ptr->param.los.lol_sticky = lol_sticky >> 8;

                    /* Read the CDR LOL status */
                    ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_CDR_LOCK_STATUS_GPREG));

                    /* cdr lol_sticky is level triggered.
                     * if lanes are already cdr lol, then we dont have to clear the lol_sticky as
                     * it will be set again with lol asserted.
                     * Perform lol_sticky clear procedure only if lol is deasserted */
                    if (reg_value & phy_info.lane_mask) {
                        reg_value  &= phy_info.lane_mask;  /* only lol deasserted lanes */
                        lol_sticky &= (reg_value << 8);    /* set lol_sticky for lol deasserted lanes only */

                        if (lol_sticky) {
                            hsip_wr_reg_(&phy_info, HOST_LANE_LOL_STATUS_ACK_GPREG, lol_sticky);

                            do {
                                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_LOL_LOS_STATUS_GPREG));
                                if (loop_counter++ > 100) {
                                    return_result = RR_WARNING_BUSY_TRY_LATER;
                                    break;
                                }
                                /* read CDR_LOCK_STATUS_GPREG in case any lane changes from CDR lock to loss
                                 * while capi waits for LOL_LOS_STATUS_GPREG been cleared */
                                ERR_HSIP(cdr_lock_reg_value = (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_CDR_LOCK_STATUS_GPREG));
                                cdr_lock_reg_value  &= phy_info.lane_mask;
                                lol_sticky &= (cdr_lock_reg_value << 8);
                            } while (reg_value & lol_sticky);

                            hsip_wr_reg_(&phy_info, HOST_LANE_LOL_STATUS_ACK_GPREG, 0x00);
                        }
                    }
                }
                break;

            case GPR_LANE_SIGDET_STATUS:
                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_LOL_LOS_STATUS_GPREG));
                reg_value = ~reg_value;
                reg_value &= 0x00FF;
                status_ptr->param.lane_sigdet_status = reg_value & phy_info.lane_mask;
                break;

            case GPR_LANE_TX_SQUELCH_STATUS:
                ERR_HSIP(status_ptr->param.lane_tx_squelch_status =
                            (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_TX_SQUELCH_STATUS_GPREG));
                status_ptr->param.lane_tx_squelch_status &= phy_info.lane_mask;
                break;

            case GPR_LANE_RX_OUTPUT_STATUS:
                ERR_HSIP(status_ptr->param.lane_rx_output_status = 
                            (uint16_t)hsip_rd_reg_(&phy_info, HOST_LANE_RX_OUTPUT_STATUS_GPREG));
                status_ptr->param.lane_rx_output_status &= phy_info.lane_mask;
                break;

            case GPR_LANE_RX_OUTPUT_LATCH_STATUS:
                status_ptr->param.lane_rx_output_latch_status = 0;

                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info,
                                HOST_LANE_RX_OUTPUT_LATCH_STATUS_GPREG));

                status_ptr->param.lane_rx_output_latch_status  = reg_value;
                status_ptr->param.lane_rx_output_latch_status &= phy_info.lane_mask;
                reg_value &= ~phy_info_ptr->lane_mask;
                hsip_wr_reg_(&phy_info, HOST_LANE_RX_OUTPUT_LATCH_STATUS_GPREG, reg_value);
                break;

            case GPR_LANE_CDR_RESTART_COUNTER:
                status_ptr->param.lane_cdr_restart_counter = 0;
                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info,
                                                            HOST_LANE_CDR_RESTART_COUNT_STATUS_GPREG));
                for (lane_idx = 0; lane_idx < MAX_BH_LANES; lane_idx++) {
                    if (phy_info_ptr->lane_mask & (1 << lane_idx)) {
                        reg_value &= (SERDES_CDR_LANE_RESTART_COUNTER_MASK << (lane_idx * SERDES_CDR_LANE_RESTART_COUNTER_SHIFT));
                        status_ptr->param.lane_cdr_restart_counter = reg_value >> (lane_idx * SERDES_CDR_LANE_RESTART_COUNTER_SHIFT);
                        break;
                    }
                }
                break;

            default:
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
                CAPI_LOG_ERROR("ERROR: Invalid status_type\n");
                break;
        }
    }
    else if ((phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) || (phy_info_ptr->core_ip == CORE_IP_HOST_DSP)) {

        switch((capi_status_type_t)status_ptr->status_type)
        {
            case GPR_LANE_CDR_LOCK_STATUS:
                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_CDR_LOCK_STATUS_GPREG));
                status_ptr->param.cdr.lock_status = reg_value & phy_info.lane_mask;
                break;

            case GPR_LANE_LOL_LOS_STATUS:
                {
                    uint8_t  loop_counter = 0;
                    uint16_t lol_sticky;

                    ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_LOL_LOS_STATUS_GPREG));

                    status_ptr->param.los.los_status = (reg_value & 0x00FF) & phy_info.lane_mask;
                    lol_sticky = (0xFF00 & reg_value) & (phy_info.lane_mask<<8);
                    status_ptr->param.los.lol_sticky = lol_sticky >> 8;

                    /* Read the CDR LOL status */
                    ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_CDR_LOCK_STATUS_GPREG));

                    /* cdr lol_sticky is level triggered.
                     * if lanes are already cdr lol, then we dont have to clear the lol_sticky as
                     * it will be set again with lol asserted.
                     * Perform lol_sticky clear procedure only if lol is deasserted */
                    if (reg_value & phy_info.lane_mask) {
                        reg_value  &= phy_info.lane_mask;  /* only lol deasserted lanes */
                        lol_sticky &= (reg_value << 8);    /* set lol_sticky for lol deasserted lanes only */

                        if (lol_sticky) {
                            hsip_wr_reg_(&phy_info, MEDIA_LANE_LOL_STATUS_ACK_GPREG, lol_sticky);

                            do {
                                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_LOL_LOS_STATUS_GPREG));
                                if (loop_counter++ > 100) {
                                    return_result = RR_WARNING_BUSY_TRY_LATER;
                                    break;
                                }

                                /* read CDR_LOCK_STATUS_GPREG in case any lane changes from CDR lock to loss
                                 * while capi waits for LOL_LOS_STATUS_GPREG been cleared */
                                ERR_HSIP(cdr_lock_reg_value = (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_CDR_LOCK_STATUS_GPREG));
                                cdr_lock_reg_value  &= phy_info.lane_mask;  /* only lol deasserted lanes */
                                lol_sticky &= (cdr_lock_reg_value << 8);    /* set lol_sticky for lol deasserted lanes only */
                            } while (reg_value & lol_sticky);

                            hsip_wr_reg_(&phy_info, MEDIA_LANE_LOL_STATUS_ACK_GPREG, 0x00);
                        }
                    }
                }

                break;

            case GPR_LANE_SIGDET_STATUS:
                ERR_HSIP(reg_value = (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_LOL_LOS_STATUS_GPREG));
                reg_value = ~reg_value;
                reg_value &= 0x00FF;
                status_ptr->param.lane_sigdet_status = reg_value & phy_info.lane_mask;
                break;

            case GPR_LANE_TX_SQUELCH_STATUS:
                ERR_HSIP(status_ptr->param.lane_tx_squelch_status =
                            (uint16_t)hsip_rd_reg_(&phy_info, MEDIA_LANE_TX_SQUELCH_STATUS_GPREG));
                status_ptr->param.lane_tx_squelch_status &= phy_info.lane_mask;
                break;

            case GPR_LANE_CDR_RESTART_COUNTER:
                status_ptr->param.lane_cdr_restart_counter = 0;
                for (lane_idx = 0; lane_idx < MAX_LW_LANES; lane_idx++) {
                    if (phy_info_ptr->lane_mask & (1 << lane_idx)) {
                        phy_info.base_addr = host_util_get_lane_lw_top_pam_bbaddr(lane_idx);
                        ERR_HSIP(status_ptr->param.lane_cdr_restart_counter =
                            hsip_rd_field_(&phy_info, COMMON_LW_LANE_CONFIG26_REG, CDR_RETRY_CNT));
                        break;
                    }
                }
                break;

            default:
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
                CAPI_LOG_ERROR("ERROR: Invalid status_type\n");
                break;
        }
    }
    else {
        return_result = RR_ERROR_WRONG_INPUT_VALUE;
        CAPI_LOG_ERROR("ERROR: Invalid core_ip\n");
    }

    return return_result;
}

/**
 * @brief      return_result_t capi_set_chip_default_info(phy_info_t* phy_info_ptr, capi_chip_default_info_t chip_default_info_ptr)
 * @details    This interface is for the client application to configure/initialize the default chip configuration prior to the chip bring up
 * @param      phy_info_ptr : phy information
 * @param      chip_default_info_ptr : pointer to capi_chip_default_info_t
 *
 * @return     return_result_t:return status code as success/warning/error
 */

return_result_t capi_set_chip_default_info(phy_info_t* phy_info_ptr, capi_chip_default_info_t* chip_default_info_ptr)
{
    return_result_t ret = RR_SUCCESS;
    ret = host_power_util_boot_config(phy_info_ptr, chip_default_info_ptr);

   
    return ret;
}
