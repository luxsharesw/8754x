/**
 *
 * @file     host_test.c
 * @author   
 * @date     05/01/2019
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "type_defns.h"
#include "common_def.h"
#include "access.h"
#include "estoque2_regs.h"
#include "fw_gp_reg_map.h"
#include "chip_common_config_ind.h"
#include "chip_mode_def.h"
#include "hr_time.h"
#include "capi_def.h"
#include "capi_diag_def.h"
#include "host_test.h"
#include "host_diag.h"
#include "common_util.h"
#include "capi.h"

extern const cw_chip_port_info_t cw_cmode[];

/**
 * @brief        uint16_t host_test_get_valid_port_mask(uint8_t chip_mode)
 * @details     based on chip mode enum,  to get the valid port_mask
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  chip_mode:  chip mode index
 * 
 * @return     returns the valid port mask
 */

uint16_t host_test_get_valid_port_mask(uint8_t chip_mode)
{
    return cw_cmode[chip_mode-1].cw_chip_port_mask;
}

/**
 * @brief        host_test_get_port_lane_mask(uint8_t chip_mode, uint8_t is_lw, uint16_t port_idx)
 * @details     based on chip mode enum, port_idx  to get the valid bh_lane_mask or lw_lane_mask
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  chip_mode:  chip mode index
 * @param[in]  is_lw:  0-client; 1-line
 * @param[in]  port_idx:  port index
 * 
 * @return     returns the lw/bh lane_mask for specified port
 */

uint16_t host_test_get_port_lane_mask(uint8_t chip_mode, uint8_t is_lw, uint16_t port_idx)
{
    if(is_lw)
        return cw_cmode[chip_mode-1].cw_chip_lw_lane_mask[port_idx];
    else
        return cw_cmode[chip_mode-1].cw_chip_bh_lane_mask[port_idx];
}


/**
 * @brief        host_test_check_port_validation(uint8_t chip_mode, uint16_t port_idx)
 * @details     based on chip mode enum, port_idx  validate the port idx validation
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  chip_mode:  chip mode index
 * @param[in]  port_idx:  port index
 * 
 * @return     returns specified port is valid or not
 */

uint8_t host_test_check_port_validation(uint8_t chip_mode, uint16_t port_idx)
{
    return ((cw_cmode[chip_mode-1].cw_chip_port_mask&(1<<port_idx))?1:0);
}

/**
 * @brief      host_test_set_dsp_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_mode_type_t dsp_mode)
 * @details    This API is used to config to LW dsp mode:
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  dsp_mode: mode to be configured
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t host_test_set_dsp_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_mode_type_t dsp_mode)
{
    uint8_t pll_lock;
    uint8_t dis_try;
    uint8_t monitor_cnt = 0;
    uint32_t base_addr = phy_info_ptr->base_addr;
    
    /*optical_or_electric_mode*/               
    if (dsp_mode > 1 || dsp_mode < 0) {
        return RR_ERROR_WRONG_INPUT_VALUE;
    }

    /*optical_or_electric_mode: this is a per chip parameter*/
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_2, ENABLE_ELECTRIC_MODE, dsp_mode);
    hsip_wr_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_3, DISABLE_PLL_TRY_LIMIT, 1);

    phy_info_ptr->base_addr = host_util_get_lane_lw_top_pam_bbaddr(0);
    hsip_wr_field_(phy_info_ptr, TOP_PLL_CAL_CTRL_0, PLLCAL_EN_CALIB_N, 1);

    delay_ms(50);

    do {
        phy_info_ptr->base_addr = host_util_get_lane_lw_top_pam_bbaddr(0);
        ERR_HSIP(pll_lock = hsip_rd_field_(phy_info_ptr, TOP_LOCKDET_STAT, PLL_LKDT));
        phy_info_ptr->base_addr = OCTAL_TOP_REGS;
        ERR_HSIP(dis_try = hsip_rd_field_(phy_info_ptr,  FW_INTERNAL_CONFIG_REG_3, DISABLE_PLL_TRY_LIMIT));
        delay_ms(10);
    } while(pll_lock == 0 && dis_try && monitor_cnt++ < 100);

    hsip_wr_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_3, DISABLE_PLL_TRY_LIMIT, 0);

    phy_info_ptr->base_addr = base_addr;

    if (monitor_cnt >= 100)
        return RR_ERROR;
    else
        return RR_SUCCESS;
}

/**
 * @brief      host_test_get_dsp_mode(capi_phy_info_t* phy_info_ptr, chip_bench_cfg_t* bench_config_ptr)
 * @details    This API is used to get  LW bench mode
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] lane_config_ptr: this parameter 
 * 
 * @return     returns dsp mode as an uint8_t
 */
uint8_t host_test_get_dsp_mode(capi_phy_info_t* phy_info_ptr)
{
    uint8_t reg_val;
    uint32_t base_addr = phy_info_ptr->base_addr;
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;

    ERR_HSIP(reg_val = hsip_rd_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_2, ENABLE_ELECTRIC_MODE));
    phy_info_ptr->base_addr = base_addr;
    return reg_val;
}

/**
 * @brief      host_test_set_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t tc_tx_mode)
 * @details    This API is used to config to LW TC TX type:
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  tc_tx_mode: mode to be configured
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t host_test_set_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t tc_tx_mode)
{
    uint32_t base_addr    = phy_info_ptr->base_addr;
    uint32_t chipid       = fw_read_chip_id();
    capi_lane_ctrl_info_t capi_lane_ctrl_info;
    return_result_t       ret_result;
    uint8_t               lidx;
    uint16_t              tidx, is_tc, is_se, tx_pwr;
    capi_phy_info_t       lw_phy;

    /*Differential or SE mode*/               
    if (tc_tx_mode > 1 || tc_tx_mode < 0) {
        return RR_ERROR_WRONG_INPUT_VALUE;
    }
    
    /*This feature only is applied to TC chip*/
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;    
    ERR_HSIP(is_tc = (uint16_t)hsip_rd_reg_(phy_info_ptr, QUAD_CORE_CONFIG_CFG_SPARE_OPTIONS_RDB));
    if(!((is_tc==0xAC && chipid==0x87541) || (chipid==0x87542)|| (chipid==0x87582)))
        return RR_ERROR_FEATURE_NOT_SUPPORTED;
    
    /*Check current TC_TX_MODE is matched or not*/
    ERR_HSIP(is_se = (uint16_t)hsip_rd_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_2, ENABLE_TC_SE_MODE));
    if((capi_dsp_tc_tx_type_t)is_se == tc_tx_mode)
        return RR_SUCCESS;

    /*start to config the new TC_TX_MODE*/
    hsip_wr_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_2, ENABLE_TC_SE_MODE, tc_tx_mode);
    /* restart all lanes to force the new config IN */
    for(lidx=0; lidx<MAX_LW_LANES; lidx++){
        host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lidx);
        ERR_HSIP(tx_pwr = !(uint16_t)hsip_rd_field_(&lw_phy, TX_TX_CONFIG_CONTROL, TX_PWRDN));
        /*Toggle TX lane power twice to update the new config*/
        for(tidx=0; tidx<2; tidx++){
            host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lidx);
            tx_pwr ^= 0x1;
            util_memset((void *)&capi_lane_ctrl_info, 0, sizeof(capi_lane_ctrl_info_t));
            capi_lane_ctrl_info.param.is.tx_datapath_power = 1;
            capi_lane_ctrl_info.cmd_value.is.tx_datapath_power = tx_pwr;
            ret_result = capi_set_lane_ctrl_info( &lw_phy, &capi_lane_ctrl_info);
            if (ret_result != RR_SUCCESS)
                return ret_result;
            delay_ms(200);
        }
    }
    phy_info_ptr->base_addr = base_addr;
    return RR_SUCCESS;
}

/**
 * @brief      host_test_get_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t* tc_tx_mode)
 * @details    This API is used to get  LW TC TX mode
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] tc_tx_mode:  TC TX mode 
 * 
 * @return     returns dsp mode as an uint8_t
 */
return_result_t host_test_get_tc_se_mode(capi_phy_info_t* phy_info_ptr, capi_dsp_tc_tx_type_t* tc_tx_mode)
{
    uint32_t base_addr    = phy_info_ptr->base_addr;
    uint32_t chipid       = fw_read_chip_id();
    uint16_t              is_tc;

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;     
    ERR_HSIP(is_tc = (uint16_t)hsip_rd_reg_(phy_info_ptr, QUAD_CORE_CONFIG_CFG_SPARE_OPTIONS_RDB));
    if(!((is_tc==0xAC && chipid==0x87541) || (chipid==0x87542)|| (chipid==0x87582)))
        return RR_ERROR_FEATURE_NOT_SUPPORTED;
    
    /*Check current TC_TX_MODE is matched or not*/
    ERR_HSIP(*tc_tx_mode = (capi_dsp_tc_tx_type_t)hsip_rd_field_(phy_info_ptr, FW_INTERNAL_CONFIG_REG_2, ENABLE_TC_SE_MODE));
    phy_info_ptr->base_addr = base_addr;
    return RR_SUCCESS;
}
