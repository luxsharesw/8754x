/**
 *
 * @file    host_chip_wrapper.c
 * @author  Firmware Team
 * @date    03/24/2017
 * @version 0.1
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "access.h"
#include "common_def.h"
#include "regs_common.h"
#include "chip_common_config_ind.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "hr_time.h"
#include "host_log_util.h"
#include "common_util.h"
#include "host_chip_wrapper.h"

/**
* @brief    host_get_chip_info(capi_phy_info_t* phy_info_ptr, capi_hw_info_t* chip_info_ptr)
* @details  This API is used to get the Chip information
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out] chip_info_ptr: this pointer contains chip info defined by capi_chip_info_t, which has 
*                            chip_id and chip_revision
* 
* @return     returns the result of the called methode/function, RR_SUCCESS
*/
return_result_t host_get_hw_info(capi_phy_info_t* phy_info_ptr, capi_hw_info_t* chip_info_ptr)
{
    uint32_t chip_id_msb, chip_id_lsb, reg_val;

    chip_info_ptr->chip_id = 0;
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;/*Need the equivalent Centenario address*/

    ERR_HSIP(reg_val = hsip_rd_reg_(phy_info_ptr, QUAD_CORE_CONFIG_CFG_CHIPID_0_RDB));
    chip_id_lsb = reg_val & QUAD_CORE_CONFIG_CFG_CHIPID_0_RDB_CFG_CHIPID_15_0_MASK;

    ERR_HSIP(chip_id_msb = hsip_rd_field_(phy_info_ptr, QUAD_CORE_CONFIG_CFG_CHIPID_1_RDB, CFG_CHIPID_19_16));

    chip_info_ptr->chip_id = chip_id_msb << 16 | chip_id_lsb;
    ERR_HSIP(chip_info_ptr->chip_revision = hsip_rd_field_(phy_info_ptr, QUAD_CORE_CONFIG_CFG_CHIPID_1_RDB, CFG_REVID));

    return (RR_SUCCESS);
}

/**
* @brief        host_get_sw_info(capi_phy_info_t* phy_info_ptr, capi_sw_info_t* sw_info_ptr)
* @details      This function returns firmware version information
*
* @param[in]    phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out]   sw_info_ptr: this pointer retuns the firmware version info defined by capi_sw_info_t
* 
* @return    returns the result of the called methode/function, either RR_ERROR or RR_SUCCESS
*/
return_result_t host_get_sw_info(capi_phy_info_t* phy_info_ptr, capi_sw_info_t* sw_info_ptr)
{
    phy_info_t  fw_phy;
    uint16_t    major_version, minor_version;

    CAPI_LOG_FUNC("host_get_sw_info", phy_info_ptr);

    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));
    fw_phy.base_addr = OCTAL_TOP_REGS;
    fw_phy.phy_id    = phy_info_ptr->phy_id;

    ERR_HSIP(major_version  = (uint16_t) hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_VERSION_REG));
    ERR_HSIP(minor_version  = (uint16_t) hsip_rd_reg_(&fw_phy, CHIP_TOP_CHIP_FIRMWARE_VERSION_MINOR_REG_ADDR));
    sw_info_ptr->fw_version = (major_version << 16) | minor_version;
    ERR_HSIP(sw_info_ptr->cmis_version = hsip_rd_reg_(&fw_phy, CMIS_VERSION_GPREG));
    ERR_HSIP(sw_info_ptr->capi_major_version = (uint16_t) hsip_rd_reg_(&fw_phy, CHIP_CAPI_MAJOR_VERSION_REG));
    ERR_HSIP(sw_info_ptr->capi_minor_version = (uint16_t) hsip_rd_reg_(&fw_phy, CHIP_CAPI_MINOR_VERSION_REG));

    CAPI_LOG_INFO("_host_get_sw_info(firmware version = %x, cmis version = 0x%x)\n", sw_info_ptr->fw_version,
                        sw_info_ptr->cmis_version);
    CAPI_LOG_INFO("_host_get_sw_info(cAPI version = %x_%x)\n", sw_info_ptr->capi_major_version,
                        sw_info_ptr->capi_minor_version);

    return (RR_SUCCESS);
}

/**
* @brief      util_wait_for_uc_ready(capi_phy_info_t* phy_info_ptr)
* @details    This utility function to pool the FW ready state
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t util_wait_for_uc_ready(capi_phy_info_t* phy_info_ptr)
{
    uint16_t core_rdy_mask=0, core_rdy_time=0, reg_uc_rdy;

    capi_phy_info_t capi_phy;

    util_memset((void *)&capi_phy, 0, sizeof(capi_phy_info_t));
    util_memcpy((void *)&capi_phy, phy_info_ptr, sizeof(capi_phy_info_t));
    capi_phy.base_addr = OCTAL_TOP_REGS;

    core_rdy_mask = 0x0B;

    /*Check and Wait the cores are ready to receive new config*/
    ERR_HSIP(reg_uc_rdy = (uint16_t) hsip_rd_reg_(&capi_phy, UC_READY_REG));
    while(((reg_uc_rdy&core_rdy_mask) != core_rdy_mask) &&  (core_rdy_time<CORE_READY_TIMER)) {
        delay_ms(10);
        core_rdy_time++;
        ERR_HSIP(reg_uc_rdy = (uint16_t) hsip_rd_reg_(&capi_phy, UC_READY_REG));
    }

    if(core_rdy_time>=CORE_READY_TIMER){
        PRINT_DIAG_OUT(("Fail: Core isn't ready 0x%x \r\n", hsip_rd_reg_(&capi_phy, UC_READY_REG) ));
        return  RR_ERROR;
    }
    else
        return RR_SUCCESS;
}


/**
 * @brief    host_get_lpm_st(capi_phy_info_t* phy_info_ptr, capi_lpm_info_t* lpm_ptr)
 * @details  This API is used to get firmware LPM status: IN LPM(1) or NOT IN LPM(0)
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  lpm_ptr: a pointer for lpm information
 * 
 * @return     returns the result of the called methode/function
 */
return_result_t host_get_lpm_st(capi_phy_info_t* phy_info_ptr, capi_lpm_info_t* lpm_ptr)
{
    //capi_phy_info_t capi_phy={0};
    uint16_t lpm_st, pre_lpm_st,  lpm_cnt=0;


    //util_memcpy((void *)&capi_phy, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    
    ERR_HSIP(lpm_st = hsip_rd_field_(phy_info_ptr, OCW_CHIP_LPM_FW_REG, CHIP_IN_LPM));

    do {
        pre_lpm_st = lpm_st;
        ERR_HSIP(lpm_st = hsip_rd_field_(phy_info_ptr, OCW_CHIP_LPM_FW_REG, CHIP_IN_LPM));

        if(pre_lpm_st==lpm_st)
            lpm_cnt++;
        else
            lpm_cnt = 0;
    } while(lpm_cnt<5);

    lpm_ptr->lpm_en = (lpm_st?CAPI_ENABLE:CAPI_DISABLE);

    return RR_SUCCESS;
}


