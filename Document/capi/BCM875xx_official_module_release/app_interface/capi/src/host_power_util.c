/**
 *
 * @file    host_power_util.c
 * @author  Firmware Team
 * @date    12/27/2018
 * @version 0.2
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "access.h"
#include "common_def.h"
#include "regs_common.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "host_power_util.h"
#include "estoque2_regs.h"
#include "hr_time.h"
#include "common_util.h"
#include "host_chip_wrapper.h"
#include "chip_common_config_ind.h"
/**
 * @brief  power_util_ana_reg_access(phy_info_t* phy_info_ptr,
 *                                   uint8_t     acc_type,
 *                                   uint8_t     ana_phy_ad,
 *                                   uint8_t     reg_ad,
 *                                   uint16_t    wr_data,
 *                                   uint16_t*   rd_data)
 *
 * @details  MDIO indirect access to ANA regulators <BR>
 * @public   
 * @private 
 * @param  bbaddr device base address 
 * @param  acc_type  READ or WRITE
 * @param  ana_phy_ad  REGULATOR ID (0-5)
 * @param  reg_ad  REGULATOR internal register number (0x00-0xFF)
 * @param  wr_data  Write data value -- 16-b
 * @param  *rd_data  Read data value -- 16-b
 * @return enum return_result_t
 */
static return_result_t power_util_ana_reg_access(phy_info_t* phy_info_ptr,
                                                 uint8_t     acc_type,
                                                 uint8_t     ana_phy_ad,
                                                 uint8_t     reg_ad,
                                                 uint16_t    wr_data,
                                                 uint16_t*   rd_data)
{
    const uint8_t st = (0x1 & 0x3); /*- 2 bit variable*/
    const  uint8_t ta = (0x2 & 0x3); /*- 2 bit variable*/
    uint32_t read_value;

    uint32_t event_count_timeout = 0;
    uint32_t cmd = 0;
    uint8_t phy_ad;
    uint8_t reg_addr;
    uint8_t access;
    uint16_t wr_val = 0;

    phy_ad   = ana_phy_ad & 0x1F;  /* 5-bits*/
    reg_addr = reg_ad & 0x1F;      /* 5-bits*/
    access   = acc_type & 0x3;     /*2-bits*/

    if (access == ANA_REG_WRITE) { 
        wr_val   = wr_data;
    } else { 
        wr_val   = 0x0;
    }

    cmd = (st << 30 | ta << 16 | phy_ad << 23 | reg_addr << 18 | wr_val | access << 28);

    hsip_wr_reg_(phy_info_ptr, MDIOM_MDIOM_CTRL, 0x286); /* start the clock to the MDIO slave*/
  
    hsip_wr_reg_(phy_info_ptr, MDIOM_CMD, cmd);

    ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, MDIOM_MDIOM_CTRL));
    do {
        ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, MDIOM_MDIOM_CTRL));
    } while((read_value & 0x100) && (event_count_timeout++ <= 70000));

    if (event_count_timeout > 70000) { 
        return RR_ERROR;
    } 

    if(access == ANA_REG_READ) {
        ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, MDIOM_CMD));
        *rd_data = read_value & 0xFFFF;
    }
    return RR_SUCCESS;
}

/**
 * @brief  power_util_ana_reg_rdwr_access(phy_info_t* phy_info_ptr,
 *                                        uint8_t     acc_type,
 *                                        uint8_t     ana_phy_ad,
 *                                        uint8_t     reg_ad,
 *                                        uint16_t    wr_data,
 *                                        uint16_t*   rd_data)
 *
 * @details  MDIO indirect access to ANA regulators <BR>
 * @public   
 * @private 
 * @param  bbaddr device base address 
 * @param  acc_type  READ or WRITE
 * @param  ana_phy_ad  REGULATOR ID (0-5)
 * @param  reg_ad  REGULATOR internal register number (0x00-0xFF)
 * @param  wr_data  Write data value -- 16-b
 * @param  *rd_data  Read data value -- 16-b
 * @return enum return_result_t
 */
static return_result_t power_util_ana_reg_rdwr_access(phy_info_t* phy_info_ptr,
                                                      uint8_t     acc_type,
                                                      uint8_t     ana_phy_ad,
                                                      uint8_t     reg_ad,
                                                      uint16_t    wr_data,
                                                      uint16_t*   rd_data)
{
    const uint8_t st = (0x1 & 0x3); /*- 2 bit variable*/
    const  uint8_t ta = (0x2 & 0x3); /*- 2 bit variable*/
    uint32_t read_value;
    uint32_t event_count_timeout = 0;
    uint32_t cmd = 0;
    uint8_t phy_ad;
    uint8_t reg_addr;
    uint8_t access;
    uint16_t wr_val = 0;

    phy_ad   = ana_phy_ad & 0x1F;  /* 5-bits*/
    reg_addr = reg_ad & 0x1F;      /* 5-bits*/
    access   = acc_type & 0x3;     /*2-bits*/

    if (access == ANA_REG_WRITE) { 
        wr_val   = wr_data;
    } else { 
        wr_val   = 0x0;
    }

    cmd = (st << 30 | ta << 16 | phy_ad << 23 | reg_addr << 18 | wr_val | access << 28);
   
    hsip_wr_reg_(phy_info_ptr, MDIOM_MDIOM_CTRL, 0x286); /* start the clock to the MDIO slave*/
    hsip_wr_reg_(phy_info_ptr, MDIOM_CMD, cmd);
  
    ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, MDIOM_MDIOM_CTRL));
    do {
        ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, MDIOM_MDIOM_CTRL));
    } while((read_value & 0x100) && (event_count_timeout++ <= 70000));

    if (event_count_timeout > 70000) { 
        return RR_ERROR;
    } 

    if(access == ANA_REG_READ) {
        ERR_HSIP(read_value =  hsip_rd_reg_(phy_info_ptr,  MDIOM_CMD));
        *rd_data = read_value & 0xFFFF;
    }
    return RR_SUCCESS;
}


return_result_t power_util_ana_vddm_set_volt(phy_info_t* phy_info_ptr, uint32_t vddm_mv) {
    uint32_t step;
    uint32_t dac_val;
    uint16_t reg_val;
    uint16_t rd_data;
    uint16_t wr_data;

    step = vddm_mv - 500000;
    dac_val = step/3125;
    reg_val = (dac_val & 0xFF)<< 8; 

    /* Do a read modify write to regC */

    power_util_ana_reg_rdwr_access (phy_info_ptr, ANA_REG_READ, ANA_DVDDM_MASTER_PHY_ID, VOLT_ADJ_DAC_REG_ADDR,  0x0, &rd_data);

    wr_data =  reg_val;
    power_util_ana_reg_rdwr_access (phy_info_ptr, ANA_REG_WRITE, ANA_DVDDM_MASTER_PHY_ID, VOLT_ADJ_DAC_REG_ADDR, wr_data, &rd_data);

    /* Toggle by writing 1 to reg0[1] and then 0 to complete the transaction */
    power_util_ana_reg_rdwr_access (phy_info_ptr, ANA_REG_READ, ANA_DVDDM_MASTER_PHY_ID, ANA_REG_0, 0x0, &rd_data);
    wr_data = rd_data | 0x0002;
    power_util_ana_reg_rdwr_access (phy_info_ptr, ANA_REG_WRITE, ANA_DVDDM_MASTER_PHY_ID, ANA_REG_0, wr_data, &rd_data);

    power_util_ana_reg_rdwr_access(phy_info_ptr, ANA_REG_READ, ANA_DVDDM_MASTER_PHY_ID, ANA_REG_0, 0x0, &rd_data);
    wr_data = rd_data & 0xFFFD;
    power_util_ana_reg_rdwr_access (phy_info_ptr, ANA_REG_WRITE, ANA_DVDDM_MASTER_PHY_ID, ANA_REG_0,wr_data, &rd_data);
    return RR_SUCCESS;
}

return_result_t power_util_ana_vddm_get_volt(phy_info_t* phy_info_ptr, uint32_t* reading) 
{
    uint32_t dac_val;
    uint16_t reg_val;
    uint16_t rd_data;

    power_util_ana_reg_access (phy_info_ptr, ANA_REG_READ, ANA_DVDDM_MASTER_PHY_ID, VOLT_ADJ_DAC_REG_ADDR, 0x0, &rd_data);
    reg_val = (rd_data & 0xFF00) >> 8; 
    dac_val = reg_val * 3125;
    *reading = dac_val + 500000;
    return RR_SUCCESS;
}


return_result_t power_util_ana_avdd_set_volt(phy_info_t* phy_info_ptr, uint32_t avdd_mv) 
{
    uint32_t step;
    uint32_t dac_val;
    uint16_t reg_val;
    uint16_t rd_data;
    uint16_t wr_data;

    step = avdd_mv - 500000;
    dac_val = step/3125;
    reg_val = (dac_val & 0xFF)<< 8; 

    /*Do a read modify write to regC*/

    power_util_ana_reg_rdwr_access(phy_info_ptr, ANA_REG_READ, ANA_AVDD_MASTER_PHY_ID, VOLT_ADJ_DAC_REG_ADDR, 0x0, &rd_data);

    wr_data =  reg_val;
    power_util_ana_reg_rdwr_access(phy_info_ptr, ANA_REG_WRITE, ANA_AVDD_MASTER_PHY_ID, VOLT_ADJ_DAC_REG_ADDR, wr_data, &rd_data);

    /*Toggle by writing 1 to reg0[1] and then 0 to complete the transaction*/
    power_util_ana_reg_rdwr_access(phy_info_ptr,ANA_REG_READ, ANA_AVDD_MASTER_PHY_ID, ANA_REG_0, 0x0, &rd_data);
    wr_data = rd_data | 0x0002;
    power_util_ana_reg_rdwr_access(phy_info_ptr,ANA_REG_WRITE, ANA_AVDD_MASTER_PHY_ID, ANA_REG_0, wr_data, &rd_data);

    power_util_ana_reg_rdwr_access(phy_info_ptr, ANA_REG_READ, ANA_AVDD_MASTER_PHY_ID, ANA_REG_0, 0x0, &rd_data);
    wr_data = rd_data & 0xFFFD;
    power_util_ana_reg_rdwr_access(phy_info_ptr, ANA_REG_WRITE, ANA_AVDD_MASTER_PHY_ID, ANA_REG_0, wr_data, &rd_data);
    return RR_SUCCESS;
}

return_result_t power_util_ana_avdd_get_volt(phy_info_t* phy_info_ptr, uint32_t* reading) 
{
    uint32_t dac_val;
    uint16_t reg_val;
    uint16_t rd_data;

    power_util_ana_reg_access(phy_info_ptr, ANA_REG_READ, ANA_AVDD_MASTER_PHY_ID, VOLT_ADJ_DAC_REG_ADDR, 0x0, &rd_data);
    reg_val = (rd_data & 0xFF00) >> 8; 
    dac_val = reg_val * 3125;
    *reading = dac_val + 500000;
    return RR_SUCCESS;
}



/**
 * @brief      host_power_util_boot_config(capi_phy_info_t* phy_info_ptr, capi_set_pre_boot_config* config_ptr)
 * @details    set config before bootup
 *
 * @param      phy_info_ptr : phy information
 * @param      chip_default_info_ptr : pointer to capi_chip_default_info_t
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_power_util_boot_config(capi_phy_info_t* phy_info_ptr, capi_chip_default_info_t* chip_default_info_ptr)
{
    uint8_t    i = 0;   
    return_result_t ret_code = RR_SUCCESS;
    capi_hw_info_t chip_info;
    capi_phy_info_t phy_info_local;
    uint32_t num_of_phy=0;
    uint32_t retry=0;
   
    chip_top_pre_load_config_reg_t chip_top_pre_load_config_reg;
    chip_top_pre_load_config2_reg_t chip_top_pre_load_config2_reg;
   
    util_memcpy((void *)&phy_info_local, phy_info_ptr, sizeof(capi_phy_info_t));
    

    do {
        host_get_hw_info(phy_info_ptr, &chip_info);
        if( (chip_info.chip_id == 0x87580) ||  (chip_info.chip_id == 0x87540)|| (chip_info.chip_id == 0x87582)){
            break;
        }
        delay_ms(1);

    } while(retry++ < 10);
    if(retry >=10){
        return RR_ERROR;
    }
    if (chip_info.chip_id == 0x87580 || chip_info.chip_id == 0x87582) {
        num_of_phy=2;
    }
    else{
        num_of_phy=1;

    }
    for (i=num_of_phy; i> 0;i--){
        /* PCR reset*/
        phy_info_local.base_addr  = OCTAL_TOP_REGS;   

        phy_info_local.phy_id =  (i-1) *2;
        
        hsip_wr_reg_(&phy_info_local, PCR_RST_CTR2,
                                       PCR_RST_CTR2_M0P3_SRST_EN_MASK |
                                       PCR_RST_CTR2_M0P2_SRST_EN_MASK |
                                       PCR_RST_CTR2_M0P1_SRST_EN_MASK |
                                       PCR_RST_CTR2_M0P0_SRST_EN_MASK);
        
    }
    delay_ms(1);

    for (i=0; i<num_of_phy ;i++){
        phy_info_local.base_addr = OCTAL_TOP_REGS;
        phy_info_local.phy_id =  i*2;

        hsip_wr_field_(&phy_info_local, PCR_RST_CTR2, SPIF_SRST_EN, 1);

        /* SPI toggle reset */
        delay_ms(1);
        hsip_wr_field_(&phy_info_local, PCR_RST_CTR2, SPIF_SRST_EN, 0);
    }
   
   
    delay_ms(1);

    for (i=0; i<num_of_phy ;i++){
         phy_info_local.base_addr = OCTAL_TOP_REGS;
         phy_info_local.phy_id =  i*2;

         hsip_wr_reg_(&phy_info_local, COM_REMAP2, 0x1);
         hsip_wr_reg_(&phy_info_local, COM_COM_CTRL2,  0x00);
        /* set default*/
        if(chip_default_info_ptr->module.is.avs==1) {
            if(chip_default_info_ptr->config.avs_default.feature.is.avs_switch==1){
                phy_info_local.base_addr =OCTAL_TOP_REGS;   
                chip_top_pre_load_config_reg.words = 0;
                chip_top_pre_load_config_reg.fields.disable_avs = chip_default_info_ptr->config.avs_default.default_config.avs_switch;        
                hsip_wr_reg_(&phy_info_local,  CHIP_TOP_CHIP_PRE_LOAD_CONFIG22_REG, chip_top_pre_load_config_reg.words );
            }
        }
         if(chip_default_info_ptr->module.is.internal_regulator==1) {
            if(chip_default_info_ptr->config.int_regulator.feature.is.int_reg_switch==1){
                phy_info_local.base_addr =OCTAL_TOP_REGS;   
                chip_top_pre_load_config2_reg.words = 0;
                chip_top_pre_load_config2_reg.fields.disable_regulator = chip_default_info_ptr->config.int_regulator.default_conifg.int_reg_switch;
                hsip_wr_reg_(&phy_info_local,  CHIP_TOP_CHIP_PRE_LOAD_CONFIG23_REG, chip_top_pre_load_config2_reg.words );
            }
        }

    }

    /*release reset */
   
        phy_info_local.base_addr = OCTAL_TOP_REGS;
        phy_info_local.phy_id =  phy_info_ptr->phy_id;
        hsip_wr_reg_(&phy_info_local, PCR_RST_CTR2,
                                       PCR_RST_CTR2_M0P3_SRST_EN_MASK |
                                       PCR_RST_CTR2_M0P2_SRST_EN_MASK |
                                       PCR_RST_CTR2_M0P1_SRST_EN_MASK);
    
    return RR_SUCCESS;
}