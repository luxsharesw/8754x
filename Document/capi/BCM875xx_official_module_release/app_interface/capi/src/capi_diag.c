/**
 *
 * @file     capi_diag.c
 * @author   
 * @date     8/5/2019
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "access.h"
#include "common_def.h"
#include "chip_mode_def.h"
#include "fw_gp_reg_map.h"
#include "estoque2_regs.h"
#include "chip_config_def.h"
#include "cw_def.h"
#include "capi_def.h"
#include "capi.h"
#include "host_diag_util.h"
#include "capi_diag_def.h"
#include "host_diag.h"
#include "capi_diag.h"
#include "host_log_util.h"
#include "common_util.h"
#include "diag_fec_statistics_def.h"
#include "host_diag_fec_statistics.h"

/**
* @brief      capi_diag_get_lane_status(capi_phy_info_t* phy_info_ptr, capi_diag_lane_status_t * diag_lane_status_ptr)
* @details    This API is used to retrieve client lane state
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  diag_lane_status_ptr: pointer to struct that stores the inquired status
*
* @return     returns the performance result of the called method/function
*/
return_result_t capi_diag_get_lane_status(capi_phy_info_t* phy_info_ptr, capi_diag_lane_status_t * diag_lane_status_ptr)
{
    return_result_t return_result;
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_DIAG_LANE_STATUS;
    command_info.type.diag_lane_status = *diag_lane_status_ptr;

    CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_diag_lane_status_t), GET_CONFIG);
    
    if (return_result == RR_SUCCESS) {
        CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
        *diag_lane_status_ptr = command_info.type.diag_lane_status;
    }
    return (return_result);
}


/**
* @brief      capi_get_usr_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
* @details    This API is used to get the user diag info
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  usr_diag_info_ptr: this parameter
*
* @return     returns the performance result of the called method/function
*/
return_result_t capi_get_usr_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
{
    CAPI_LOG_FUNC("capi_get_usr_diagnostics", phy_info_ptr);
    if ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP) ||
        (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)) 
    {
        switch(usr_diag_info_ptr->diag_info_type){
            case DIAG_INFO_TYPE_CMIS:
                return host_lw_get_usr_cmis_diagnostics(phy_info_ptr, usr_diag_info_ptr);
            case DIAG_INFO_TYPE_BRCM_DSP_SNR:
                return host_lw_get_usr_diagnostics(phy_info_ptr, usr_diag_info_ptr);
            case DIAG_INFO_TYPE_BRCM_DSP_SNR_AVRG_ONLY:
                return host_lw_get_usr_diagnostics(phy_info_ptr, usr_diag_info_ptr);
            case DIAG_INFO_TYPE_SLICE_HISTOGRAM:
                return host_dsp_get_slicer_histogram_diagnostics(phy_info_ptr, usr_diag_info_ptr);
        }
    } else if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
               (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
        switch (usr_diag_info_ptr->diag_info_type) 
        {
            case DIAG_INFO_TYPE_CMIS:
                return host_client_get_usr_cmis_diagnostics(phy_info_ptr, usr_diag_info_ptr);

            default:
                break;
        }
    }
    return RR_ERROR_WRONG_INPUT_VALUE;
}

static return_result_t _chk_rprt_mode_fec_st_support(capi_fec_dump_status_t* capi_fec_dump_status_ptr, 
                                                        capi_config_info_t* config_info_ptr)
{
    
    /* REPEATER_PATH */
    if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE) {
        if((config_info_ptr->line_lane.modulation==CAPI_MODULATION_PAM4 && config_info_ptr->lw_br==CAPI_BH_BR_53_125) ||
           (config_info_ptr->line_lane.modulation==CAPI_MODULATION_NRZ && config_info_ptr->lw_br==CAPI_BH_BR_26_5625))                           
            capi_fec_dump_status_ptr->fec_type.line_fec  = CAPI_LINE_FEC_TYPE_RS544;
        else if((config_info_ptr->line_lane.modulation==CAPI_MODULATION_PAM4 && config_info_ptr->lw_br==CAPI_BH_BR_51_5625) ||
            (config_info_ptr->line_lane.modulation==CAPI_MODULATION_NRZ && config_info_ptr->lw_br==CAPI_BH_BR_25_78125))                           
            capi_fec_dump_status_ptr->fec_type.line_fec  = CAPI_LINE_FEC_TYPE_RS528;
        else
            return RR_ERROR_FEATURE_NOT_SUPPORTED;
    } else if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_CLIENT) {
        if((config_info_ptr->host_lane.modulation==CAPI_MODULATION_PAM4 && config_info_ptr->bh_br==CAPI_BH_BR_53_125) ||
           (config_info_ptr->host_lane.modulation==CAPI_MODULATION_NRZ && config_info_ptr->bh_br==CAPI_BH_BR_26_5625))                           
            capi_fec_dump_status_ptr->fec_type.host_fec  = CAPI_HOST_FEC_TYPE_RS544;
        else if((config_info_ptr->host_lane.modulation==CAPI_MODULATION_PAM4 && config_info_ptr->bh_br==CAPI_BH_BR_51_5625) ||
            (config_info_ptr->host_lane.modulation==CAPI_MODULATION_NRZ && config_info_ptr->bh_br==CAPI_BH_BR_25_78125))                           
            capi_fec_dump_status_ptr->fec_type.host_fec  = CAPI_HOST_FEC_TYPE_RS528;
        else
            return RR_ERROR_FEATURE_NOT_SUPPORTED;
    }
    return RR_SUCCESS;
}

static return_result_t _chk_rtmr_mode_fec_st_support(capi_fec_dump_status_t* capi_fec_dump_status_ptr, 
                                                        capi_config_info_t* config_info_ptr)
{
    /*RETIMER_MODE*/
    if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE) {
        if((capi_line_fec_type_t)config_info_ptr->line_fec_type!=CHIP_LINE_FEC_TYPE_RS528 &&
            (capi_line_fec_type_t)config_info_ptr->line_fec_type!=CHIP_LINE_FEC_TYPE_RS544)
            return RR_ERROR_FEATURE_NOT_SUPPORTED;
        capi_fec_dump_status_ptr->fec_type.line_fec  = (capi_line_fec_type_t)config_info_ptr->line_fec_type;
    } else if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_CLIENT) {
        if((capi_host_fec_type_t)config_info_ptr->host_fec_type!=CHIP_HOST_FEC_TYPE_RS528 &&
            (capi_host_fec_type_t)config_info_ptr->host_fec_type!=CHIP_HOST_FEC_TYPE_RS544 )
            return RR_ERROR_FEATURE_NOT_SUPPORTED;
        capi_fec_dump_status_ptr->fec_type.host_fec  = (capi_host_fec_type_t)config_info_ptr->host_fec_type;
    }
    return RR_SUCCESS;
}

static return_result_t _chk_chip_mode_fec_st_support(capi_fec_dump_status_t* capi_fec_dump_status_ptr, 
                                                        capi_config_info_t* config_info_ptr)
{
    if((enum cw_port_fec_term_type_e)config_info_ptr->fec_term==CHIP_PORT_FEC_TERM_BYPASS &&
        (config_info_ptr->line_lane.lane_mask != config_info_ptr->host_lane.lane_mask))
        return RR_ERROR_FEATURE_NOT_SUPPORTED;

    /*check current data path type*/
    if((enum cw_port_fec_term_type_e)config_info_ptr->fec_term==CHIP_PORT_FEC_TERM_BYPASS){
       /* REPEATER_PATH */
        ERR_FUNC(_chk_rprt_mode_fec_st_support(capi_fec_dump_status_ptr, config_info_ptr));
    }else {
       /* RETIMER_PATH */
        ERR_FUNC(_chk_rtmr_mode_fec_st_support(capi_fec_dump_status_ptr, config_info_ptr));
    }
    if ((capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE && 
        (capi_fec_dump_status_ptr->fec_type.line_fec != CAPI_LINE_FEC_TYPE_RS528) &&
        (capi_fec_dump_status_ptr->fec_type.line_fec != CAPI_LINE_FEC_TYPE_RS544)) || 
        (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_CLIENT && 
        (capi_fec_dump_status_ptr->fec_type.host_fec != CAPI_HOST_FEC_TYPE_RS528) && 
        (capi_fec_dump_status_ptr->fec_type.host_fec != CAPI_HOST_FEC_TYPE_RS544))) {
            CAPI_LOG_ERROR("Feature Not Supported since no FEC protocol. fec_mode %d host %d line %d \n", 
                capi_fec_dump_status_ptr->fec_mode,
                capi_fec_dump_status_ptr->fec_type.host_fec, capi_fec_dump_status_ptr->fec_type.line_fec);
        return RR_ERROR_FEATURE_NOT_SUPPORTED;
    }

    return RR_SUCCESS;
}

/**
* @brief      capi_init_fec_mon(capi_phy_info_t* phy_info_ptr, capi_fec_dump_status_t* capi_fec_dump_status_ptr, boolean enable)
* @details    This API is used to 
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  fec_info_ptr: this parameter 
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_init_fec_mon(capi_phy_info_t* phy_info_ptr, capi_fec_dump_status_t* capi_fec_dump_status_ptr, boolean enable)
{
    return_result_t ret_val = RR_SUCCESS;
    capi_config_info_t config_info;
    capi_phy_info_t phy_info;
    cw_port_info_t fec_port_info;

    CAPI_LOG_FUNC("capi_init_fec_mon", phy_info_ptr);

    if (phy_info_ptr->lane_mask == 0) {
        CAPI_LOG_ERROR("Invalid Lane Mask. %d \n", phy_info_ptr->lane_mask);
        return RR_ERROR_INITIALIZATION;
    }
    if ((capi_fec_dump_status_ptr->fec_mode != CAPI_FEC_LINE) && (capi_fec_dump_status_ptr->fec_mode != CAPI_FEC_CLIENT)) {
        CAPI_LOG_ERROR("Wrong input for side. %d \n", capi_fec_dump_status_ptr->fec_mode);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }

    util_memset(&config_info, 0, sizeof(capi_config_info_t));
    if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE) {
        config_info.line_lane.lane_mask = phy_info_ptr->lane_mask;   /* Using line-side lane_mask for identifying PORT */
    } else if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_CLIENT) {
        config_info.host_lane.lane_mask = phy_info_ptr->lane_mask;   /* Using host-side lane_mask for identifying PORT */
    }

    util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info.base_addr = OCTAL_TOP_REGS;

    /*retrieve the config information*/
    ERR_FUNC(host_diag_cw_get_chip_config_w_cdr_chk(&phy_info,  (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE)?DIR_INGRESS:DIR_EGRESS,
                    1, &config_info, &fec_port_info));
    
    capi_fec_dump_status_ptr->func_mode = (capi_function_mode_t)config_info.func_mode;
    /*check current chip mode support FEC ST or not*/
    ERR_FUNC(_chk_chip_mode_fec_st_support(capi_fec_dump_status_ptr, &config_info));

    
    if (enable)
        ret_val = host_diag_cw_fec_stat_init (phy_info_ptr, capi_fec_dump_status_ptr, (capi_function_mode_t)config_info.func_mode);
    else 
        ret_val = host_diag_cw_fec_stat_deinit (phy_info_ptr, capi_fec_dump_status_ptr, (capi_function_mode_t)config_info.func_mode);
    return ret_val; 
}

/**
* @brief      capi_clear_fec_mon(capi_phy_info_t* phy_info_ptr, capi_fec_dump_status_t* capi_fec_dump_status_ptr)
* @details    This API is used to clear FEC BRCM counters 
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  fec_info_ptr: this parameter 
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_clear_fec_mon (capi_phy_info_t* phy_info_ptr,capi_fec_dump_status_t* capi_fec_dump_status_ptr)
{
    return_result_t ret_val = RR_SUCCESS;
    capi_config_info_t config_info;
    capi_phy_info_t phy_info;
    cw_port_info_t port_info;

    CAPI_LOG_FUNC("capi_clear_fec_mon", phy_info_ptr);

    if (phy_info_ptr->lane_mask == 0) {
        CAPI_LOG_ERROR("Invalid Lane Mask. %d \n", phy_info_ptr->lane_mask);
        return RR_ERROR_INITIALIZATION;
    }
    if ((capi_fec_dump_status_ptr->fec_mode != CAPI_FEC_LINE) && (capi_fec_dump_status_ptr->fec_mode != CAPI_FEC_CLIENT)) {
        CAPI_LOG_ERROR("Wrong input for side. %d \n", capi_fec_dump_status_ptr->fec_mode);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }

    util_memset(&config_info, 0, sizeof(capi_config_info_t));
    if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE) {
        config_info.line_lane.lane_mask = phy_info_ptr->lane_mask;   /* Using line-side lane_mask for identifying PORT */
    } else if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_CLIENT) {
        config_info.host_lane.lane_mask = phy_info_ptr->lane_mask;   /* Using host-side lane_mask for identifying PORT */
    }

    util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info.base_addr = OCTAL_TOP_REGS;
    /*retrieve the config information*/
    ERR_FUNC(host_diag_cw_get_chip_config(&phy_info, &config_info, &port_info));
    
    capi_fec_dump_status_ptr->func_mode = (capi_function_mode_t)config_info.func_mode;

    /*check current chip mode support FEC ST or not*/
    ERR_FUNC(_chk_chip_mode_fec_st_support(capi_fec_dump_status_ptr, &config_info));

    ret_val = host_diag_cw_fec_stat_clear (phy_info_ptr, capi_fec_dump_status_ptr, (capi_function_mode_t)config_info.func_mode, TRUE);
    return ret_val; 
}

/**
* @brief      capi_get_fec_info(capi_phy_info_t* phy_info_ptr, capi_fec_dump_status_t* capi_fec_dump_status_ptr)
* @details    This API is used to 
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out] fec_info_ptr: this parameter 
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_get_fec_info(capi_phy_info_t* phy_info_ptr, capi_fec_dump_status_t* capi_fec_dump_status_ptr)
{
    return_result_t ret_val = RR_SUCCESS;
    capi_config_info_t config_info;
    capi_phy_info_t phy_info;
    cw_port_info_t port_info;

    CAPI_LOG_FUNC("capi_get_fec_info", phy_info_ptr);

    if (phy_info_ptr->lane_mask == 0) {
        CAPI_LOG_ERROR("Invalid Lane Mask. %d \n", phy_info_ptr->lane_mask);
        return RR_ERROR_INITIALIZATION;
    }

    if ((capi_fec_dump_status_ptr->fec_mode != CAPI_FEC_LINE) && (capi_fec_dump_status_ptr->fec_mode != CAPI_FEC_CLIENT)) {
        CAPI_LOG_ERROR("Wrong input for side. %d \n", capi_fec_dump_status_ptr->fec_mode);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }

    util_memset(&config_info, 0, sizeof(capi_config_info_t));
    if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE) {
        config_info.line_lane.lane_mask = phy_info_ptr->lane_mask;   /* Using line-side lane_mask for identifying PORT */
    } else if (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_CLIENT) {
        config_info.host_lane.lane_mask = phy_info_ptr->lane_mask;   /* Using host-side lane_mask for identifying PORT */
    }
    
    util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info.base_addr = OCTAL_TOP_REGS;
    /*retrieve the config information*/
    ERR_FUNC(host_diag_cw_get_chip_config_w_cdr_chk(&phy_info,  (capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE)?DIR_INGRESS:DIR_EGRESS, 1, &config_info, &port_info));
    
    capi_fec_dump_status_ptr->func_mode = (capi_function_mode_t)config_info.func_mode;
    

    /*check current chip mode support FEC ST or not*/
    ERR_FUNC(_chk_chip_mode_fec_st_support(capi_fec_dump_status_ptr, &config_info));

    ret_val = host_diag_cw_get_fec_info (phy_info_ptr, capi_fec_dump_status_ptr, (capi_function_mode_t)config_info.func_mode);
    return ret_val; 
}

/**
* @brief      capi_fec_ber_cmis_latch_request(capi_phy_info_t *phy_ptr, capi_kp4fec_ber_state_t* fec_ptr)
* @details    This API is used to send the FEC BER latch request;
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  fec_ptr: this parameter pointer point to the BER state;
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_fec_ber_cmis_latch_request(capi_phy_info_t *phy_ptr, capi_kp4fec_ber_state_t* fec_ptr)
{
    return_result_t ret_val = RR_SUCCESS;
    bool clr_cnt=fec_ptr->clr_cnt, only_cur=fec_ptr->only_cur;
    capi_fec_mode_t  fec_mode = fec_ptr->fec_mode;
    capi_fec_dump_status_t fec_dump_st;
    uint8_t regcnt, total_uncorr_bpf = 16;
    
    if ((fec_mode != CAPI_FEC_LINE) && (fec_mode != CAPI_FEC_CLIENT)) {
        CAPI_LOG_ERROR("Wrong fec mode. %d \n", fec_ptr->fec_mode);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }
    /*clear the structure in case of any wrong information is brought in*/
    util_memset(fec_ptr, 0, sizeof(capi_kp4fec_ber_state_t));
    fec_ptr->fec_mode = fec_mode;
    fec_ptr->clr_cnt = clr_cnt;
    fec_ptr->only_cur = only_cur;
    
    util_memset(&fec_dump_st, 0, sizeof(capi_fec_dump_status_t));
    fec_dump_st.fec_mode = fec_mode;

    /*If clr_cnt is TRUE, call clr function; else, read one set of data as start point*/
    if(fec_ptr->clr_cnt){
        ret_val = capi_clear_fec_mon(phy_ptr, &fec_dump_st);
        if ( ret_val!= RR_SUCCESS)
        {
             CAPI_LOG_ERROR("\n  *** capi_clear_fec_mon call failed. \r\n");
             return ret_val;
        }
    }else{ 
        ret_val = capi_get_fec_info(phy_ptr, &fec_dump_st);
        if ( ret_val!= RR_SUCCESS)
        {
             CAPI_LOG_ERROR("\n  *** capi_get_fec_info call failed. \r\n");
             return ret_val;
        }
        if ((fec_dump_st.fec_mode == CAPI_FEC_LINE && fec_dump_st.fec_type.line_fec == CAPI_LINE_FEC_TYPE_RS528) ||
            (fec_dump_st.fec_mode == CAPI_FEC_CLIENT && fec_dump_st.fec_type.host_fec == CAPI_HOST_FEC_TYPE_RS528)) {
            total_uncorr_bpf = 8;
        }
        /*Store hardware counter into callback pointer*/
        for (regcnt = 0; regcnt < total_uncorr_bpf; regcnt++) {
            fec_ptr->backup_err.tot_frames_err_cnt[regcnt] = fec_dump_st.fec_a_err_cnt.tot_frames_err_cnt[regcnt] + fec_dump_st.fec_b_err_cnt.tot_frames_err_cnt[regcnt];
        }

        for (regcnt = 0; regcnt < KP4_FEC_TOT_BITS_CORR_NUM; regcnt++) {
            fec_ptr->backup_err.tot_bits_corr_cnt[regcnt] = fec_dump_st.fec_a_err_cnt.tot_bits_corr_cnt[regcnt] + fec_dump_st.fec_b_err_cnt.tot_bits_corr_cnt[regcnt];
        }
        fec_ptr->backup_err.tot_frame_cnt = fec_dump_st.fec_a_err_cnt.tot_frame_rev_cnt + fec_dump_st.fec_b_err_cnt.tot_frame_rev_cnt;
        fec_ptr->backup_err.tot_frame_uncorr_cnt = fec_dump_st.fec_a_err_cnt.tot_frame_uncorr_cnt + fec_dump_st.fec_b_err_cnt.tot_frame_uncorr_cnt;
    }
    /* Initialize the minimum error counter and BER */
    if(fec_ptr->only_cur == 0){
        util_memset(&fec_ptr->min_err, 0xFF, sizeof(cw_kp4fec_err_cnt_t));
        fec_ptr->pre_fec_ber.min_ber = (double)0xFFFFFFFFFFFFF;
        fec_ptr->post_fec_ber.min_ber = (double)0xFFFFFFFFFFFFF;
    }
    return ret_val;
}

/**
* @brief      capi_fec_ber_cmis_latch_release(capi_phy_info_t *phy_ptr, capi_kp4fec_ber_state_t* fec_ptr)
* @details    This API is used to 
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  fec_info_ptr: this parameter 
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_fec_ber_cmis_latch_release(capi_phy_info_t *phy_ptr, capi_kp4fec_ber_state_t* fec_ptr)
{
    return_result_t ret_val = RR_SUCCESS;
    capi_fec_dump_status_t fec_dump_st;
    
    if ((fec_ptr->fec_mode != CAPI_FEC_LINE) && (fec_ptr->fec_mode != CAPI_FEC_CLIENT)) {
        CAPI_LOG_ERROR("Wrong fec mode. %d \n", fec_ptr->fec_mode);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }
    util_memset(&fec_dump_st, 0, sizeof(capi_fec_dump_status_t));
    fec_dump_st.fec_mode = fec_ptr->fec_mode;

    ret_val = capi_get_fec_info(phy_ptr, &fec_dump_st);
    if ( ret_val!= RR_SUCCESS)
    {
         CAPI_LOG_ERROR("\n  *** capi_get_fec_info call failed 0x%x \r\n", ret_val);
         return ret_val;
    }
    /*if request clear hardware counter, then clear up the backup*/
    if(fec_ptr->clr_cnt)
    {        
        ret_val = capi_clear_fec_mon(phy_ptr, &fec_dump_st);
        if ( ret_val!= RR_SUCCESS)
        {
             CAPI_LOG_ERROR("\n  *** capi_clear_fec_mon call failed 0x%x \r\n", ret_val);
             return ret_val;
        }
        util_memset((void *)&fec_ptr->backup_err, 0, sizeof(cw_kp4fec_err_cnt_t));
    }
    ret_val = host_diag_cw_calculate_ber_between_latch(phy_ptr, &fec_dump_st, fec_ptr);
    if ( ret_val!= RR_SUCCESS)
    {
         CAPI_LOG_ERROR("\n  *** _diag_cw_calculate_ber_between_latch call failed 0x%x. \r\n", ret_val);
         return ret_val;
    }
    return ret_val;
}

/**
 * @brief      capi_diag_set_command(capi_phy_info_t*          phy_info_ptr,
 *                                   capi_diag_command_info_t* diag_cmd_info_ptr)
 * @details    This API is used to invoke test command.
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  capi_test_command_info_t: a reference to the test command object 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_diag_set_command(capi_phy_info_t*          phy_info_ptr,
                                      capi_diag_command_info_t* diag_cmd_info_ptr)
{
    switch(diag_cmd_info_ptr->command_id) {


        default:
            return RR_ERROR_WRONG_INPUT_VALUE;
    }
}

/**
 * @brief      capi_diag_command_request_info(capi_phy_info_t*                  phy_info_ptr,
 *                                            capi_diag_command_request_info_t* cmd_req_ptr)
 * @details    Function to diag command request
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr : pointer to phy_info
 * @param      cmd_req_ptr  : command request
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t capi_diag_command_request_info(capi_phy_info_t*                  phy_info_ptr,
                                               capi_diag_command_request_info_t* diag_cmd_req_info_ptr)
{
    return_result_t         return_result = RR_SUCCESS;
    capi_command_info_t     command_info;
    capi_phy_info_t         phy_info;

    util_memset((void*)&command_info, 0, sizeof(capi_command_info_t));
    util_memcpy(&command_info.type.diag_cmd_req_info, diag_cmd_req_info_ptr, sizeof(capi_diag_command_request_info_t));
    util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));

    switch(diag_cmd_req_info_ptr->command_id)
    {
        case COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO:
            command_info.command_id = diag_cmd_req_info_ptr->command_id;
            return_result = capi_command_request(&phy_info,
                                                 &command_info,
                                                 sizeof(memory_data_info_t),
                                                 GET_CONFIG);

            /* Copy the memory details to user provided cmd_req structure */
            util_memcpy(&diag_cmd_req_info_ptr->type.cmis_snr_ltp.memory_info,\
                        &command_info.type.diag_cmd_req_info.type.cmis_snr_ltp.memory_info, sizeof(memory_info_t));
            break;

        case COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO:
            command_info.command_id = diag_cmd_req_info_ptr->command_id;
            return_result = capi_command_request(&phy_info,
                                                 &command_info,
                                                 sizeof(memory_data_info_t),
                                                 GET_CONFIG);

             /* Copy the memory details to user provided cmd_req structure */
            util_memcpy(&diag_cmd_req_info_ptr->type.cmis_snr_ltp,\
                        &command_info.type.diag_cmd_req_info.type.cmis_snr_ltp, sizeof(memory_data_info_t));
            break;


        case COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_INFO:
            {
                memory_data_info_t* cmis_snr_ltp_ptr = &diag_cmd_req_info_ptr->type.cmis_snr_ltp;                    
                return_result = host_get_memory_payload(phy_info_ptr, 
                                                        cmis_snr_ltp_ptr->param.lane_data,
                                                        &cmis_snr_ltp_ptr->memory_info);
            }
            break;
            
        case COMMAND_ID_DIAG_SET_MEDIA_MPI_CONFIG:
            return_result = sanity_checker_mpi_config_info(phy_info_ptr, &diag_cmd_req_info_ptr->type.mpi_cfg, CHK_USR_INPUT);
            if(return_result!=RR_SUCCESS)
                return return_result;
            return_result = host_set_mpi_config(phy_info_ptr, &diag_cmd_req_info_ptr->type.mpi_cfg);
        break;
        
        case COMMAND_ID_DIAG_GET_MEDIA_MPI_CONFIG:
            return_result = host_get_mpi_config(phy_info_ptr, &diag_cmd_req_info_ptr->type.mpi_cfg);
            if(return_result!=RR_SUCCESS)
                return return_result;
            return_result = sanity_checker_mpi_config_info(phy_info_ptr, &diag_cmd_req_info_ptr->type.mpi_cfg, CHK_FW_OUTPUT);
        break;
        
        case COMMAND_ID_DIAG_GET_MEDIA_MPI_STATE:
            return_result = host_get_mpi_state(phy_info_ptr, &diag_cmd_req_info_ptr->type.mpi_st);
        break;

        case COMMAND_ID_DIAG_SET_MEDIA_MISSION_MPI_CONFIG:
            return_result = dsp_mission_mpi_cfg_sanity_checker(phy_info_ptr, &diag_cmd_req_info_ptr->type.mission_mpi_cfg, CHK_USR_INPUT);
            if (return_result != RR_SUCCESS)
                return return_result;
            return_result = host_set_mpi_dynamic_config(phy_info_ptr, &diag_cmd_req_info_ptr->type.mission_mpi_cfg);
        break;
        
        case COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_CONFIG:
            return_result = host_get_mpi_dynamic_config(phy_info_ptr, &diag_cmd_req_info_ptr->type.mission_mpi_cfg);
            if(return_result!=RR_SUCCESS)
                return return_result;
            return_result = dsp_mission_mpi_cfg_sanity_checker(phy_info_ptr, &diag_cmd_req_info_ptr->type.mission_mpi_cfg, CHK_FW_OUTPUT);
        break;

        case COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_STATE:
            return_result = host_get_dynamic_mpi_state(phy_info_ptr, &diag_cmd_req_info_ptr->type.mpi_st);
        break;

        case COMMAND_ID_DIAG_GET_SERDES_DIAG_INFO:
            command_info.command_id = diag_cmd_req_info_ptr->command_id;
            return_result = capi_command_request(&phy_info,
                                                 &command_info,
                                                 sizeof(serdes_diag_info_t),
                                                 GET_CONFIG);

            util_memcpy(&diag_cmd_req_info_ptr->type.serdes_diag_info,
                        &command_info.type.diag_cmd_req_info.type.serdes_diag_info, sizeof(serdes_diag_info_t));
            break;

        default:
            return_result = RR_ERROR_WRONG_INPUT_VALUE;
            break;
    }

    return return_result; 
}