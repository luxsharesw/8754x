/**
 *
 * @file    host_download_util.c
 * @author  Firmware Team
 * @date    12/11/2017
 * @version 0.2
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */

#include "hr_time.h"
#include "access.h"
#include "common_def.h"
#include "estoque2_regs.h"
#include "fw_gp_reg_map.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "capi_test_def.h"
#include "host_log_util.h"
#include "common_util.h"
#include "host_download_util.h"
#include "capi.h"
#include "phy_spi_program_fast.h"

extern void wr8_reg_ex(uint32_t addr, uint32_t data, uint32_t phy_id);
extern uint32_t rd8_reg_ex(uint32_t addr, uint32_t phy_id);

extern void set_i2c_driver_target_address(uint8_t i2c_target);


return_result_t read_status(capi_phy_info_t* phy_info_ptr, uint8_t command_id, uint8_t* status);

/* basic info of the flash devices supported */
static spi_flash_device_info_t hsip_flash_devs[] ={
    /* #jid       #cap      #name      */
    {0x00BF258D,  512,      "SST25VF040B"},
    {0x00C22815,  2048,     "MX25R1635F"},
    {0x00C22812,  256,      "MX25R2035F"},
    {0x009d7014,  1024,     "IS25WP080D"},
    {0x00620613,  512,      "SST25PF040C"},
    {0x00C22813,  512,      "MX25R4035F"},
};


/*#define DEBUG_SIM*/
bool burst_write(capi_phy_info_t * phy_info_ptr, capi_download_info_t* download_info_ptr,  uint32_t starting_address, uint32_t size, uint32_t* data) {
#if 1
    if (download_info_ptr->burst_write_mode == CAPI_BURST_WRITE_MODE_I2C) {
        return burst_write_i2c((void*)phy_info_ptr, starting_address,  size,  data);
    }
    else {
        return burst_write_mdio((void*)phy_info_ptr, starting_address,  size, data);
    }
#endif
}

static return_result_t read_flash_device_id(capi_phy_info_t* phy_info_ptr, uint8_t* fid_ptr)
{
    uint32_t retry = 0;
    uint32_t reg_val_depth, i;

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, SPI_RDID_LEN);
    wr8_reg_ex(SPIF_WFIFO_BASE, SPI_RDID, phy_info_ptr->phy_id);
    do {
        ERR_HSIP(reg_val_depth = hsip_rd_reg_(phy_info_ptr, SPIF_RFIFO_DEPTH));
        retry++;
    } while ((reg_val_depth != SPI_RDID_LEN) && (retry < 100));

    if (reg_val_depth != SPI_RDID_LEN)
        return RR_ERROR;
    for (i=0; i<SPI_RDID_LEN; i++)
        *fid_ptr++ = rd8_reg_ex(SPIF_RFIFO_BASE, phy_info_ptr->phy_id);
    return RR_SUCCESS;
}



return_result_t write_command_byte(uint8_t command_id, capi_phy_info_t* phy_info_ptr ){

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x0);

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, 0x0);

    wr8_reg_ex(0x21000000, command_id, phy_info_ptr->phy_id);

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);

    return RR_SUCCESS;
}

return_result_t read_status(capi_phy_info_t* phy_info_ptr, uint8_t command_id, uint8_t* status){
    uint32_t retry = 0;
    uint32_t reg_val_depth;  

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x0);    

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, 0x1);

    wr8_reg_ex(0x21000000, command_id, phy_info_ptr->phy_id);

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);
    phy_info_ptr->base_addr = 0x00;
    do {
          ERR_HSIP(reg_val_depth = hsip_rd_reg_(phy_info_ptr, 0x21000084));
          delay_ms(10);
          retry++;
    }
    while ((reg_val_depth !=0x1)&& (retry < 1000));

    if (reg_val_depth !=0x1) 
        return RR_ERROR;
    *status = rd8_reg_ex(SPIF_RFIFO_BASE, phy_info_ptr->phy_id);
    return RR_SUCCESS;
}

return_result_t spi_block_erase(capi_phy_info_t* phy_info_ptr, uint32_t addr)
{
    uint32_t status_reg;
    uint32_t spi_base_addr = 0;
    uint32_t spi_base_addr_1 = 0;
    uint32_t spi_base_addr_2,   spi_base_addr_3;
    uint32_t retry  = 0;
   
    CAPI_LOG_INFO("begin block erase ...... %x", addr);

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);   
    write_command_byte(SPI_WREN, phy_info_ptr);
    hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, 0);

    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x0);   

    spi_base_addr = addr;
    spi_base_addr_1 = (spi_base_addr & 0x000000ff);
    spi_base_addr_2 = (spi_base_addr & 0x0000ff00) >> 8;
    spi_base_addr_3 = ( spi_base_addr & 0x00ff0000 )>>16;

    wr8_reg_ex((SPIF_WRFIFO+SPIF),SPI_BE, phy_info_ptr->phy_id);
    wr8_reg_ex((SPIF_WRFIFO+SPIF),spi_base_addr_3, phy_info_ptr->phy_id);
    wr8_reg_ex((SPIF_WRFIFO+SPIF),spi_base_addr_2, phy_info_ptr->phy_id);
    wr8_reg_ex((SPIF_WRFIFO+SPIF),spi_base_addr_1, phy_info_ptr->phy_id);
    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);    
    do {
        read_status(phy_info_ptr, SPI_RDSR, (uint8_t*)&status_reg);
        retry++;
        delay_ms(1);

    } while (((status_reg & 0x1) != 0x0) && (retry<1000));

    printf("block erase complete...... \n");

    return RR_SUCCESS;    
}

return_result_t spi_sector_erase(capi_phy_info_t* phy_info_ptr, uint32_t addr)
{
    uint32_t status_reg;
    uint32_t spi_base_addr = 0;
    uint32_t spi_base_addr_1 = 0;
    uint32_t spi_base_addr_2,   spi_base_addr_3;
    uint32_t retry  = 0;

    CAPI_LOG_INFO("begin sector erase ...... %x", addr);

    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);   
    write_command_byte(SPI_WREN, phy_info_ptr);

    hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, 0);

    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x0);   

    spi_base_addr = addr;
    spi_base_addr_1 = (spi_base_addr & 0x000000ff);
    spi_base_addr_2 = (spi_base_addr & 0x0000ff00) >> 8;
    spi_base_addr_3 = ( spi_base_addr & 0x00ff0000 )>>16;
    
    wr8_reg_ex((SPIF_WRFIFO+SPIF),SPI_SE, phy_info_ptr->phy_id);
    
        
    wr8_reg_ex((SPIF_WRFIFO+SPIF),spi_base_addr_3, phy_info_ptr->phy_id);
        
    wr8_reg_ex((SPIF_WRFIFO+SPIF),spi_base_addr_2, phy_info_ptr->phy_id);
        
    wr8_reg_ex((SPIF_WRFIFO+SPIF),spi_base_addr_1, phy_info_ptr->phy_id);
    phy_info_ptr->base_addr = SPIF;
    hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);    
    do {
        read_status(phy_info_ptr, SPI_RDSR, (uint8_t*)&status_reg);
        retry++;
        delay_ms(1);

    } while (((status_reg & 0x1) != 0x0) && (retry<1000));

    delay_ms(1000);
    printf("sector complete...... \n");
   
    return RR_SUCCESS;
}

return_result_t verify_spi_from_external(capi_phy_info_t* phy_info_ptr,
    uint32_t* program_spi_ptr, uint32_t start_base_addr, uint32_t size )
{
    uint32_t i = 0;
    uint32_t retry = 0;

    uint32_t wr_value = 0;
    uint32_t block_id = 0;
    uint32_t data = 0;
    uint32_t spi_base_addr = 0;
    uint32_t rd_value = 0;

    uint32_t program_size = 0;

    phy_info_ptr->base_addr = MGT;
    phy_info_ptr->phy_id = 0x0;

    hsip_wr_reg_(phy_info_ptr, PCR_SPICLK_CTRL, 0x7);

    program_size = size;

    for (block_id = 0; block_id < program_size / SPI_BLOCK_SIZE; block_id++) {
        phy_info_ptr->base_addr = SPIF;
        hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x0);
        hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, SPI_BLOCK_SIZE);

        wr8_reg_ex((SPIF_WRFIFO+SPIF),READ_CMD, phy_info_ptr->phy_id);
        spi_base_addr = block_id * SPI_BLOCK_SIZE +start_base_addr;
        wr_value = spi_base_addr >> 16;
        wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

        wr_value = (spi_base_addr & 0xffff) >> 8;
        wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

        wr_value = spi_base_addr & 0xff;
        wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

        
        phy_info_ptr->base_addr = SPIF;
        hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);
        do {        
            phy_info_ptr->base_addr = SPIF;
            ERR_HSIP(rd_value  = hsip_rd_reg_(phy_info_ptr, SPIF_RFIFO_DEPTH));
            retry++;
            delay_ms(1);
        } while ((rd_value < SPI_BLOCK_SIZE) && (retry<1000));
    
        for (i = 0; i< (SPI_BLOCK_SIZE / 4); i++){
            data = program_spi_ptr[(block_id * SPI_BLOCK_SIZE)/4 + i ];
            printf("0x%08x,", data);
            phy_info_ptr->base_addr = SPIF;
            ERR_HSIP(rd_value  = hsip_rd_reg_(phy_info_ptr, SPIF_WRFIFO));

         
            if (data!=rd_value){
                printf("mismatched:b:%x, %x, data:%x, read:%x\n", block_id, i, data, rd_value);
                return RR_ERROR;
            }
            else {
                printf("matched:b:%x, %x, data:%x, read:%x\n", block_id, i, data, rd_value);
            }
        }
    }
    return RR_SUCCESS;
}

return_result_t program_spi_from_external(capi_phy_info_t* phy_info_ptr,
    uint32_t* program_spi_ptr, uint32_t start_base_addr, uint32_t size )
{
    return_result_t ret = RR_SUCCESS;
    uint32_t i = 0;
    uint32_t original_phy_id = phy_info_ptr->phy_id;
    uint32_t retry = 0;

    uint32_t wr_value = 0;
    uint32_t block_id = 0;
    uint32_t data = 0;
    uint32_t spi_base_addr = 0;
    uint32_t rd_value = 0;
    uint32_t status_reg = 0;

    uint32_t program_size = 0;

    phy_info_ptr->base_addr = MGT;
    phy_info_ptr->phy_id = 0x0;

    hsip_wr_reg_(phy_info_ptr, PCR_SPICLK_CTRL, 0x7);   

    if (start_base_addr!=0) {
    spi_sector_erase(phy_info_ptr,start_base_addr);
    }
    program_size = size;

    write_command_byte(SPI_WREN, phy_info_ptr);
    write_command_byte(SPI_WRSR, phy_info_ptr);
    read_status(phy_info_ptr, SPI_RDSR, (uint8_t*)&status_reg);
    write_command_byte(SPI_WREN, phy_info_ptr);
    CAPI_LOG_INFO("size:%u\n", program_size);

    for (block_id = 0; block_id < (program_size / SPI_BLOCK_SIZE); block_id++) {
        write_command_byte(SPI_WREN, phy_info_ptr);
        phy_info_ptr->base_addr = SPIF;
        hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x0);
        hsip_wr_reg_(phy_info_ptr, SPIF_RLEN, 0x0);
        wr8_reg_ex((SPIF_WRFIFO+SPIF),WRITE_CMD, phy_info_ptr->phy_id);
        spi_base_addr = block_id * SPI_BLOCK_SIZE +start_base_addr;
        wr_value = spi_base_addr >> 16;
        wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

        wr_value = (spi_base_addr & 0xffff) >> 8;
        wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

        wr_value = spi_base_addr & 0xff;
        wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);
        for (i = 0; i< (SPI_BLOCK_SIZE / 4); i++){

            data = program_spi_ptr[(block_id * SPI_BLOCK_SIZE)/4 + i];
            wr_value = (data & 0x000000ff);
            wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

            wr_value = (data & 0x0000ff00) >> 8;
            wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

            wr_value = (data & 0x00ff0000) >> 16;
            wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

            wr_value = (data & 0xff000000) >> 24;
            wr8_reg_ex((SPIF_WRFIFO+SPIF),wr_value, phy_info_ptr->phy_id);

        }
        phy_info_ptr->base_addr = SPIF;
        hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);
        do {
            phy_info_ptr->base_addr = SPIF;
            ERR_HSIP(rd_value =  hsip_rd_reg_(phy_info_ptr, SPIF_WFIFO_BASE));
            retry++;
            delay_ms(1);
        } while ((rd_value > 0) && (retry<1000));
        do {
             read_status(phy_info_ptr, SPI_RDSR, (uint8_t*)&status_reg);
             retry++;
             delay_ms(1);
        } while (((status_reg & 0x1) != 0x0) && (retry<1000));
    }

  

    phy_info_ptr->phy_id = original_phy_id ;
    return ret;
}

/**
 * @brief      host_program_config_to_spi(capi_phy_info_t* phy_info_ptr,
 *                                        uint32_t*        config_data_ptr,
 *                                        uint32_t         size)
 * @details    This API programs user configuration data to flash device
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr
 * @param      a pointer to the configuration data
 * @param      size of data
 *
 * @return     returns the result of the called method/function
 */
static return_result_t program_config_to_spi (capi_phy_info_t* phy_info_ptr,
    uint32_t*config_data_ptr, uint32_t size )
{
     return_result_t ret = RR_SUCCESS;
     uint32_t try_cnt =0;

     do {

         if (program_spi_from_external(phy_info_ptr, (uint32_t*)config_data_ptr, CONFIG_DATA_FLASH_LOCATION, size)==RR_ERROR) {
             ret = RR_ERROR;

         }

         else {
             if (verify_spi_from_external(phy_info_ptr, (uint32_t*)config_data_ptr, CONFIG_DATA_FLASH_LOCATION, size)==RR_ERROR) {
                 ret = RR_ERROR;
             }
             else {
                 ret = RR_SUCCESS;
             }
         }
         if(ret==RR_ERROR){
             phy_info_ptr->base_addr = MGT;
             hsip_wr_field_(phy_info_ptr, PCR_RST_CTR, SPIF_SRST_EN, 0x1);
             delay_ms(10);
             hsip_wr_field_(phy_info_ptr, PCR_RST_CTR, SPIF_SRST_EN, 0x0);
             delay_ms(10);

         }
         try_cnt++;
     } while((try_cnt<3) && (ret==RR_ERROR));
  

     return ret;

}

return_result_t  host_program_config_to_spi(capi_phy_info_t* phy_info_ptr,
                                                    uint32_t*        config_data_ptr,
                                                    uint32_t         size)
{
     return program_config_to_spi(phy_info_ptr, (uint32_t*)config_data_ptr, size);
}


#if 0
static return_result_t _apply_default_config(capi_phy_info_t*     phy_info_ptr,
                                             phy_static_config_t* phy_static_config_ptr)
{
     ocw_default_mode_reg_t default_mode;
     chip_top_chip_refclk_config_reg_t chip_top_chip_refclk_config_reg;
     phy_info_t fw_phy;


     util_memset(&fw_phy, 0, sizeof(phy_info_t));
     util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));

     default_mode.words = 0;
     /*These are designed for no_surge feature is enabled*/
     /* default_mode.fields.no_surge = 1;
        default_mode.fields.skip = 1; */

     chip_top_chip_refclk_config_reg.words = 0;
     fw_phy.base_addr = OCTAL_TOP_REGS;

     if(phy_static_config_ptr) {
         if (phy_static_config_ptr->chip_default_mode == CAPI_NO_DEFAULT_MODE) {
             default_mode.fields.skip = 1 ;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_1_REPEATER) {
            default_mode.fields.pre_programmed = 0;
            /*CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_NA    CHIP_LINE_FEC_TYPE_NA  CHIP_PORT_FEC_TERM_BYPASS  CHIP_PORT_MUX_BIT_MUX*/
            default_mode.fields.mode_config = CAPI_DEFAULT_MODE_1_REPEATER;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_1_KP4_KP4) {
            default_mode.fields.pre_programmed = 0;
            /*CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_RS544    CHIP_LINE_FEC_TYPE_RS544  CHIP_PORT_FEC_DEC_ENC  CHIP_PORT_MUX_BIT_MUX*/
            default_mode.fields.mode_config = CAPI_DEFAULT_MODE_1_KP4_KP4;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_5_PCS_KP4) {
             default_mode.fields.pre_programmed = 0;
             /*CHIP_MODE_4x25G_NRZ_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_PCS    CHIP_LINE_FEC_TYPE_RS544  CHIP_LANE_PCS_XENC  CHIP_PORT_MUX_BIT_MUX*/
             default_mode.fields.mode_config = CAPI_DEFAULT_MODE_5_PCS_KP4;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_5_KR4_KP4) {
            default_mode.fields.pre_programmed = 0;
            /*CHIP_MODE_4x25G_NRZ_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_RS528    CHIP_LINE_FEC_TYPE_RS544  CHIP_PORT_FEC_DEC_ENC  CHIP_PORT_MUX_BIT_MUX*/
            default_mode.fields.mode_config = CAPI_DEFAULT_MODE_5_KR4_KP4;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_4_NRZ_NRZ_REPEATER) {
            default_mode.fields.pre_programmed = 0;
            /*CHIP_MODE_4x25G_KR4NRZ_TO_4x25G_KR4NRZ  CHIP_HOST_FEC_TYPE_NA    CHIP_HOST_FEC_TYPE_NA  CHIP_PORT_FEC_TERM_BYPASS  CHIP_PORT_MUX_BIT_MUX*/
            default_mode.fields.mode_config = CAPI_DEFAULT_MODE_4_NRZ_NRZ_REPEATER;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_7_REPEATER) {
            default_mode.fields.pre_programmed = 0;
            /*CHIP_MODE_8x53G_KP4PAM_TO_4X106G_KP4PAM  CHIP_HOST_FEC_TYPE_NA    CHIP_LINE_FEC_TYPE_NA  CHIP_PORT_FEC_TERM_BYPASS  CHIP_PORT_MUX_BIT_MUX*/
            default_mode.fields.mode_config = CAPI_DEFAULT_MODE_7_REPEATER;
         } else if (phy_static_config_ptr->chip_default_mode == CAPI_DEFAULT_MODE_7_KP4_KP4) {
            default_mode.fields.pre_programmed = 0;
            /*CHIP_MODE_8x53G_KP4PAM_TO_4X106G_KP4PAM  CHIP_HOST_FEC_TYPE_RS544    CHIP_HOST_FEC_TYPE_RS544  CHIP_PORT_FEC_DEC_ENC  CHIP_PORT_MUX_BIT_MUX*/
            default_mode.fields.mode_config = CAPI_DEFAULT_MODE_7_KP4_KP4;
         } else {
             default_mode.fields.skip = 1 ;
         }

         default_mode.fields.avs_enable =  phy_static_config_ptr->avs_enable;
     }

     hsip_wr_reg_(&fw_phy, OCW_DEFAULT_MODE_REG, default_mode.words);
     hsip_wr_reg_(&fw_phy, CHIP_TOP_CHIP_REFCLK_CONFIG_REG, chip_top_chip_refclk_config_reg.words);

     return RR_SUCCESS;
}
#endif



#define IMAGE_OFFSET     64000

return_result_t host_download_sram_fast(capi_phy_info_t*     phy_info_ptr,
                                 capi_download_info_t* download_info_ptr)
{
    return_result_t ret = RR_SUCCESS;
    uint32_t base_addr = 0;
    uint32_t i = 0;
    uint32_t try_count = 0;
    uint32_t read_value;
    uint32_t temp_var =0;
    uint32_t total_block_num =0;
    uint32_t image_start_in_sram =0;
    uint32_t block_write_size =0;
    uint32_t*      wholeimage_sram_ptr;
    uint32_t             size;        
    uint32_t transfer_size =0;
    uint32_t last_block_transfer_size =0;
    CAPI_LOG_INFO("host_download_sram() - download\n");
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);
    host_set_clock(phy_info_ptr);

    hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x0);    /* disable remap */

    wholeimage_sram_ptr = download_info_ptr->image_info.image_ptr;
    size = download_info_ptr->image_info.image_size;
    if(phy_info_ptr->i2c_block_write_size==0) 
       block_write_size = I2C_BLOCK_WRITE_SIZE;
    else 
       block_write_size  = phy_info_ptr->i2c_block_write_size;

    phy_info_ptr->base_addr = 0x00;
    temp_var = (size - IMAGE_OFFSET * 4) ;


    if (temp_var <= block_write_size) {
        total_block_num = 1;
        transfer_size = temp_var;  
    }
    else {
        if ((temp_var % block_write_size) ==0){
            total_block_num = temp_var / block_write_size;
            transfer_size = block_write_size;  
            last_block_transfer_size = transfer_size;
        }
        else {
            total_block_num = temp_var / block_write_size + 1;
            transfer_size = block_write_size;  
            last_block_transfer_size = temp_var- (temp_var / block_write_size) * block_write_size;
        }
    }

    for (i = 0; i < total_block_num ; i++) {
        image_start_in_sram = base_addr + i * block_write_size;         
        if (i==(total_block_num-1)) {
            transfer_size = last_block_transfer_size;
        }
        if (burst_write(phy_info_ptr, download_info_ptr, image_start_in_sram, transfer_size, (uint32_t*) &wholeimage_sram_ptr[IMAGE_OFFSET+i*block_write_size/4])){
                 return  RR_ERROR_SRAM_BURST_WRITE_ERROR;                
        }
    }
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_reg_(phy_info_ptr, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION, 0);
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK);

    try_count = 0;
    
    do {
        phy_info_ptr->base_addr = OCTAL_TOP_REGS;;

        ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION));
        if(read_value== 0x600d){
            break;
        }
        delay_ms(2000);
        try_count++;
    } while(try_count < 3);
    if(read_value!=0x600d){
         CAPI_LOG_INFO(" First download failed \n");
         return RR_ERROR;
    }


    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);

     phy_info_ptr->base_addr = 0x00;

    if ((IMAGE_OFFSET *4 ) <= block_write_size) {
        total_block_num = 1;
        transfer_size = IMAGE_OFFSET * 4;  
    }
    else {
        if ((( IMAGE_OFFSET  *4 ) % block_write_size)==0) {
            total_block_num = ( IMAGE_OFFSET  *4 )/ block_write_size;
            transfer_size = block_write_size;  
            last_block_transfer_size = block_write_size;
        }
        else {
            total_block_num = ( IMAGE_OFFSET  *4 )/ block_write_size +1;
            transfer_size = block_write_size;  
            last_block_transfer_size = IMAGE_OFFSET  *4- (IMAGE_OFFSET  *4 / block_write_size) * block_write_size;
        }
    }

    for (i = 0; i < total_block_num ; i++) {
        image_start_in_sram = base_addr + i * block_write_size; 
         if (i==(total_block_num-1)) {
            transfer_size = last_block_transfer_size;
        }
        if (burst_write(phy_info_ptr, download_info_ptr, image_start_in_sram, transfer_size, (uint32_t*) &wholeimage_sram_ptr[i*block_write_size/4])){
                 return  RR_ERROR_SRAM_BURST_WRITE_ERROR;                
        }
    }

    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);

    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK);
    return ret;
}

/**
* @brief    host_download_sram(capi_phy_info_t*     phy_info_ptr,
*                              const uint32_t*      wholeimage_sram_ptr,
*                              uint32_t             size,
*                              uint32_t             offset,
*                              phy_static_config_t* phy_static_config_ptr);
*
* @details  This API is used to download firmware to sram
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  wholeimage_sram_ptr: a pointer to the fw image
* @param[in]  size: size of the fw image
* @param[in]  offset: offset of the image
* @param[in]  phy_static_config_ptr: a pointer to phy configuration
*
* @return     returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
*/



return_result_t host_download_sram(capi_phy_info_t*     phy_info_ptr,
                                    const uint32_t*      wholeimage_sram_ptr,
                                    uint32_t             size,
                                    phy_static_config_t* phy_static_config_ptr)
{
    return_result_t ret = RR_SUCCESS;
    uint32_t data_write = 0;
    uint32_t base_addr = 0;
    uint32_t i = 0;
    uint32_t try_count = 0;
    uint32_t read_value;
    uint32_t temp_var =0;
    CAPI_LOG_INFO("host_download_sram() - download\n");
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);
    host_set_clock(phy_info_ptr);
    hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x0);    /* disable remap */

    phy_info_ptr->base_addr = 0x00;
    temp_var = (size - IMAGE_OFFSET * 4) >> 2;
    for (i = 0; i <  temp_var; i++) {
        data_write = wholeimage_sram_ptr[IMAGE_OFFSET+i];
        hsip_wr_reg_(phy_info_ptr, base_addr + i * 4, data_write);
    }

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_reg_(phy_info_ptr, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION, 0);

    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK);

    try_count = 0;
   
    do {
        phy_info_ptr->base_addr = OCTAL_TOP_REGS;;

        ERR_HSIP(read_value = hsip_rd_reg_(phy_info_ptr, CHIP_TOP_CHIP_BH_CRC_CHECK_LOCATION));
        if(read_value== 0x600d){
            break;
        }
        delay_ms(2000);
        try_count++;
    } while(try_count < 3);
    if(read_value!=0x600d){
         CAPI_LOG_INFO(" First download failed \n");
         return RR_ERROR;
    }

    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);

     phy_info_ptr->base_addr = 0x00;
   
    for (i = 0; i < IMAGE_OFFSET; i++) {
        data_write = wholeimage_sram_ptr[i];
        hsip_wr_reg_(phy_info_ptr, base_addr + i * 4, data_write);
    }
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);

    
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK);
    return ret;
}


/**
* @brief    return_result_t host_download_spi_internal(capi_phy_info_t* phy_info_ptr,
                                           capi_download_info_t* download_info_ptr)    
* @details  This API is used to download firmware to spi device
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  download_info_ptr: a pointer to  the capi_download_info_t
*
* @return     returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
*/

#define IMAFE_DOWNLOAD_EACH_BLOCK_SIZE    (64*1024)

return_result_t host_download_spi_internal(capi_phy_info_t* phy_info_ptr,
                                           capi_download_info_t* download_info_ptr)        
{
    return_result_t ret = RR_SUCCESS;
    uint32_t data_write = 0;    
    uint32_t i = 0, j = 0, counter = 0;    
    uint32_t image_read_size =0;
    uint8_t phy_num = 0;
    uint32_t transfer_size = 0;
    uint32_t temp_var = 0;
    uint32_t IMAGE_SIZE_DOWNLOAD_BLOCKS =0;
    uint32_t image_start_in_sram = 0x8000;
    uint32_t*  wholeimage_spi_ptr;
    uint32_t size =0;

    
    uint32_t num_of_phy = 1;
    uint32_t original_phy_id = phy_info_ptr->phy_id;
    chip_top_spi_program_command_t chip_top_spi_program_command;

    wholeimage_spi_ptr = download_info_ptr->image_info.image_ptr;
    size = download_info_ptr->image_info.image_size;

    original_phy_id = phy_info_ptr->phy_id;

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(phy_info_ptr, REG_BOOT_EN_CTRL, BOOT_EN_VAL, 0x0);
    hsip_wr_field_(phy_info_ptr, REG_BOOT_EN_CTRL, BOOT_EN_OVR, 0x0);

    phy_info_ptr->base_addr = MGT;
    for (phy_num = 0; phy_num <  num_of_phy; phy_num++){
       phy_info_ptr->phy_id = original_phy_id + phy_num;
       hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                  PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                  PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                  PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                  PCR_RST_CTR_M0P0_SRST_EN_MASK);
    }
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x0);
    delay_ms(10);
    phy_info_ptr->base_addr = 0;   
    hsip_wr_reg_(phy_info_ptr, 0x6ffe4, 0x668899AA); 
    hsip_wr_reg_(phy_info_ptr, 0x6ffe0, 0);   

    for (phy_num = 0; phy_num <  num_of_phy; phy_num++){
        phy_info_ptr->base_addr = SPIF;
        phy_info_ptr->phy_id = original_phy_id + phy_num;
        hsip_wr_field_(phy_info_ptr,  SPIF_CTRL, SPIFEN, 0x0);
    }

    phy_info_ptr->phy_id = original_phy_id;
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x0);

    for (i = 0; i < sizeof(spi_program_fast) / 4; i++) {
        data_write = spi_program_fast[i];
        phy_info_ptr->base_addr = 0x00;
        hsip_wr_reg_(phy_info_ptr, ( i * 4), data_write);
    }


    phy_info_ptr->base_addr = MGT;        

    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                    PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                    PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                    PCR_RST_CTR_M0P1_SRST_EN_MASK);


    IMAGE_SIZE_DOWNLOAD_BLOCKS = (size / IMAFE_DOWNLOAD_EACH_BLOCK_SIZE) + 1;
    for (j = 0 ; j < IMAGE_SIZE_DOWNLOAD_BLOCKS ; j++)
    {
         phy_info_ptr->base_addr = 0x00;
         if(j==0) hsip_wr_reg_(phy_info_ptr, 0x6ffd4, 1);
         else hsip_wr_reg_(phy_info_ptr, 0x6ffd4, 0);

        if (j != (IMAGE_SIZE_DOWNLOAD_BLOCKS-1)) {
             transfer_size = 0x10000;
             hsip_wr_reg_(phy_info_ptr, 0x6ffdc, (0x0 + j * transfer_size));
         } else {
             transfer_size = size - j * 0x10000;
             hsip_wr_reg_(phy_info_ptr, 0x6ffdc, (0x0 + j * 0x10000));
         }

         hsip_wr_reg_(phy_info_ptr, 0x6ffd8, (0x0 + transfer_size));
         hsip_wr_reg_(phy_info_ptr, 0x6ffe8, image_start_in_sram);
         
         temp_var = transfer_size >> 2;
         image_read_size =  temp_var;
         phy_info_ptr->base_addr = 0x00;
         if (j!= (IMAGE_SIZE_DOWNLOAD_BLOCKS-1)) {
             for (i = 0; i < image_read_size; i++) {
                 data_write = wholeimage_spi_ptr[i+j*( temp_var)];
                 hsip_wr_reg_(phy_info_ptr, (image_start_in_sram + i * 4), data_write);
             }
         } else {
             for (i = 0; i < image_read_size; i++) {
                 data_write = wholeimage_spi_ptr[i+j*0x10000/4];
                 hsip_wr_reg_(phy_info_ptr, (image_start_in_sram + i * 4), data_write);
             }
         }
         phy_info_ptr->base_addr = 0x00;
         hsip_wr_reg_(phy_info_ptr, 0x6ffe0, 0);
         chip_top_spi_program_command.words =0;
         chip_top_spi_program_command.fields.command_change=1;
         hsip_wr_reg_(phy_info_ptr, 0x6ffe0,  chip_top_spi_program_command.words);
         phy_info_ptr->base_addr = SPIF;
         hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);

         counter = 0;
         phy_info_ptr->base_addr = 0x00;
         do {
            ERR_HSIP(chip_top_spi_program_command.words = hsip_rd_reg_(phy_info_ptr,  0x6ffe0));
            if((chip_top_spi_program_command.fields.command_change ==0) && (chip_top_spi_program_command.fields.error_status==1)){            
                break;
            }
            counter++;
            delay_ms(100);
         } while  (counter < 3000);

         if((chip_top_spi_program_command.fields.command_change !=0) || (chip_top_spi_program_command.fields.error_status!=1)){      
             return RR_ERROR_SPI_PROGRAM_VERIFY_FAILED;              
         }
    }
    
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK |
                               PCR_RST_CTR_M0P0_SRST_EN_MASK);
    hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x1);
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(phy_info_ptr, REG_BOOT_EN_CTRL, BOOT_EN_VAL, 0x1);
    hsip_wr_field_(phy_info_ptr, REG_BOOT_EN_CTRL, BOOT_EN_OVR, 0x1);
    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(phy_info_ptr, QUAD_CORE_EGRMGT_QC_CHIP_SW_RST_RDB, MD_CHIP_SW_RST, 0x1);
    delay_ms(1);
#if 0
    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                               PCR_RST_CTR_M0P3_SRST_EN_MASK |
                               PCR_RST_CTR_M0P2_SRST_EN_MASK |
                               PCR_RST_CTR_M0P1_SRST_EN_MASK);
#endif   
    phy_info_ptr->phy_id = original_phy_id ;
    return ret;
}



/**
* @brief    return_result_t host_download_spi_internal_fast(capi_phy_info_t* phy_info_ptr,
                                           capi_download_info_t* download_info_ptr)    
* @details  This API is used to download firmware to spi device
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  download_info_ptr: a pointer to  the capi_download_info_t
*
* @return     returns the result of the called method/function, either RR_ERROR or RR_SUCCESS
*/

return_result_t host_download_spi_internal_fast(capi_phy_info_t* phy_info_ptr, capi_download_info_t* download_info_ptr)                                            
{
    return_result_t ret = RR_SUCCESS; 
    uint32_t i = 0, j = 0, counter = 0;   
    uint8_t phy_num = 0;
    uint32_t transfer_size = 0;
    uint32_t temp_var = 0;
    uint32_t image_start_in_sram = 0x8000;
    uint32_t num_of_phy = 1;
    uint32_t original_phy_id = phy_info_ptr->phy_id;
    chip_top_spi_program_command_t chip_top_spi_program_command;
    uint32_t buffer_id =0;
    uint32_t next_buffer_id =0;
    uint32_t next_image_start_in_sram = 0;
    uint32_t total_i2c_block_num =0;
    uint32_t i2c_block_write_size =0;
    uint32_t IMAGE_SIZE_DOWNLOAD_BLOCKS =0;
    uint32_t*  wholeimage_spi_ptr;
    uint32_t size =0;

    wholeimage_spi_ptr = download_info_ptr->image_info.image_ptr;
    size = download_info_ptr->image_info.image_size;

    original_phy_id = phy_info_ptr->phy_id;
    if(phy_info_ptr->i2c_block_write_size==0) 
       i2c_block_write_size = I2C_BLOCK_WRITE_SIZE;
    else 
       i2c_block_write_size  = phy_info_ptr->i2c_block_write_size;

    phy_info_ptr->base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(phy_info_ptr, REG_BOOT_EN_CTRL, BOOT_EN_VAL, 0x0);
    hsip_wr_field_(phy_info_ptr, REG_BOOT_EN_CTRL, BOOT_EN_OVR, 0x0);

    phy_info_ptr->base_addr = MGT;
    for (phy_num = 0; phy_num <  num_of_phy; phy_num++){
       phy_info_ptr->phy_id = original_phy_id + phy_num;
       hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                  PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                  PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                  PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                  PCR_RST_CTR_M0P0_SRST_EN_MASK);
    }

   
    phy_info_ptr->base_addr = MGT;
    hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x0);
    delay_ms(10);
    phy_info_ptr->base_addr = 0;   
    hsip_wr_reg_(phy_info_ptr, 0x6ffe4, 0x668899AA);
    hsip_wr_reg_(phy_info_ptr, 0x6ffe0, 0);    

  
    for (phy_num = 0; phy_num <  num_of_phy; phy_num++){
        phy_info_ptr->base_addr = SPIF;
        phy_info_ptr->phy_id = original_phy_id + phy_num;
        hsip_wr_field_(phy_info_ptr,  SPIF_CTRL, SPIFEN, 0x0);
    }

    phy_info_ptr->phy_id = original_phy_id;


    IMAGE_SIZE_DOWNLOAD_BLOCKS = (size / IMAFE_DOWNLOAD_EACH_BLOCK_SIZE) + 1;
    if (((sizeof(spi_program_fast) % i2c_block_write_size) !=0) && ((sizeof(spi_program_fast) > i2c_block_write_size))) {
        return RR_ERROR_SPI_SIZE_INCORRECT;
    }    
    
    
    if (sizeof(spi_program_fast) <= i2c_block_write_size) {
        total_i2c_block_num = 1;
        transfer_size = sizeof(spi_program_fast);  
    }
    else {
        total_i2c_block_num = sizeof(spi_program_fast) / i2c_block_write_size;
        transfer_size = i2c_block_write_size;  
    }

    /* download Flash programming code to SRAM */
    for (i = 0; i < total_i2c_block_num ; i++) {
        image_start_in_sram = i * i2c_block_write_size;         
        if (burst_write(phy_info_ptr, download_info_ptr, image_start_in_sram, transfer_size, (uint32_t*) &spi_program_fast[i*i2c_block_write_size/4])){
                 return  RR_ERROR_SPI_BURST_WRITE_ERROR;                
        }
    }


    
    phy_info_ptr->base_addr = MGT;        

    hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                    PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                    PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                    PCR_RST_CTR_M0P1_SRST_EN_MASK);

    /* Download firmware image to SRAM and Program Flash */
    for (j = 0 ; j < IMAGE_SIZE_DOWNLOAD_BLOCKS ; j++)
    {
         
         buffer_id = j%2;
         image_start_in_sram = 0x8000 + buffer_id * 0x10000; /* block size*/
         phy_info_ptr->base_addr = 0x00;
         if(j==0) hsip_wr_reg_(phy_info_ptr, 0x6ffd4, 1);
         else hsip_wr_reg_(phy_info_ptr, 0x6ffd4, 0);
        
        
         if (j!= (IMAGE_SIZE_DOWNLOAD_BLOCKS-1)) {
             transfer_size = 0x10000;
             hsip_wr_reg_(phy_info_ptr, 0x6ffdc, (0x0 + j * transfer_size));

         }
         else  {
             transfer_size = size - j* 0x10000;
             hsip_wr_reg_(phy_info_ptr, 0x6ffdc, (0x0 + j * 0x10000));
         }
         
         hsip_wr_reg_(phy_info_ptr, 0x6ffd8, (0x0 + transfer_size));
         hsip_wr_reg_(phy_info_ptr, 0x6ffe8, image_start_in_sram);


         temp_var = transfer_size >> 2;
         phy_info_ptr->base_addr = 0x00;

         if( j==0) {

            /* First Segment in first bufffer */
            total_i2c_block_num = transfer_size / i2c_block_write_size;
            for (i = 0; i < total_i2c_block_num; i++) {                  
                if (burst_write(phy_info_ptr, download_info_ptr, image_start_in_sram + i*i2c_block_write_size, i2c_block_write_size, (uint32_t*) &wholeimage_spi_ptr[j*( temp_var) + (i*i2c_block_write_size/4)])){
                         return  RR_ERROR_SPI_BURST_WRITE_ERROR;                
                }
            }
         }
         phy_info_ptr->base_addr = 0x00;
         hsip_wr_reg_(phy_info_ptr, 0x6ffe0, 0);
         chip_top_spi_program_command.words =0;
         chip_top_spi_program_command.fields.command_change=1;
         hsip_wr_reg_(phy_info_ptr, 0x6ffe0,  chip_top_spi_program_command.words);
         phy_info_ptr->base_addr = SPIF;
         hsip_wr_field_(phy_info_ptr, SPIF_CTRL, SPIFEN, 0x1);

         /* check ACK of receving the image */
         counter = 0;
         phy_info_ptr->base_addr = 0x00;
         do {

            ERR_HSIP(chip_top_spi_program_command.words = hsip_rd_reg_(phy_info_ptr,  0x6ffe0));
           
            
            if(chip_top_spi_program_command.fields.command_change ==0) {
                break;
            }
             counter++;
            delay_ms(10);
         } while  (counter < 3000);
         if (chip_top_spi_program_command.fields.command_change !=0) 
             return RR_ERROR_SPI_PROGRAM_NOT_ACK;


         /* fill other buffer */
         if (j < (IMAGE_SIZE_DOWNLOAD_BLOCKS-1)) {
             next_buffer_id = (j+1)%2;
             next_image_start_in_sram = 0x8000 + next_buffer_id * 0x10000; /* block size*/

             if (j == (IMAGE_SIZE_DOWNLOAD_BLOCKS-2)) {
                transfer_size = size - (j+1)* 0x10000;                
             }
             if (transfer_size <= i2c_block_write_size) {
                total_i2c_block_num = 1;
             }
             else  if(( transfer_size % i2c_block_write_size) !=0){
                total_i2c_block_num = transfer_size / i2c_block_write_size +1;
             }
             else total_i2c_block_num = transfer_size / i2c_block_write_size;
             for (i = 0; i < total_i2c_block_num; i++) {                  
                if (burst_write(phy_info_ptr, download_info_ptr, next_image_start_in_sram + i*i2c_block_write_size, i2c_block_write_size, (uint32_t*)&wholeimage_spi_ptr[(j+1)*( temp_var) + (i*i2c_block_write_size/4)])){
                         return  RR_ERROR_SPI_BURST_WRITE_ERROR;                
                }
             }

            
         }
         /* check ACK of Flash programming */
         counter = 0;
         phy_info_ptr->base_addr = 0x00;
         do {

            ERR_HSIP(chip_top_spi_program_command.words = hsip_rd_reg_(phy_info_ptr,  0x6ffe0));
           
            if((chip_top_spi_program_command.fields.command_change ==0) && (chip_top_spi_program_command.fields.error_status==1)){            
                break;
            }
             counter++;

            delay_ms(10);
         } while  (counter < 3000);
         if (chip_top_spi_program_command.fields.error_status !=1)
             return RR_ERROR_SPI_PROGRAM_VERIFY_FAILED;
        
    }
    if (ret==RR_SUCCESS){
        phy_info_ptr->base_addr = OCTAL_TOP_REGS;
        hsip_wr_field_(phy_info_ptr, QUAD_CORE_EGRMGT_QC_CHIP_SW_RST_RDB, MD_CHIP_SW_RST, 0x1);
        delay_ms(1);
      

        phy_info_ptr->base_addr = MGT;
        hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                   PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P1_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P0_SRST_EN_MASK);
        hsip_wr_reg_(phy_info_ptr, COM_REMAP, 0x1);
        hsip_wr_reg_(phy_info_ptr, PCR_RST_CTR,
                                   PCR_RST_CTR_M0P3_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P2_SRST_EN_MASK |
                                   PCR_RST_CTR_M0P1_SRST_EN_MASK);
    }

    phy_info_ptr->phy_id = original_phy_id ;
    return ret;
}
/**
 * @brief    host_download_mem_reset(capi_phy_info_t* phy_info_ptr) 
 * @details  
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * 
 * @return     returns the result of the called methode/function
 */
return_result_t host_download_mem_reset(capi_phy_info_t* phy_info_ptr)
{
    capi_phy_info_t fw_phy;

    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));
    fw_phy.base_addr =0;
    hsip_wr_reg_(&fw_phy,0x5003C02C, 0x0010);
    delay_ms(100);
    hsip_wr_reg_(&fw_phy,0x5003C02C, 0x0000);

    fw_phy.base_addr =0;
    hsip_wr_reg_(&fw_phy,0x5201C190, 0x0010);
    delay_ms(100);
    hsip_wr_reg_(&fw_phy,0x5201C190, 0x0000);
    delay_ms(100);
    return RR_SUCCESS;
}

/**
 * @brief     host_set_i2c_addres_by_phy_info(capi_phy_info_t* phy_info_ptr)
 * @details    
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr
 *
 * @return     returns the result of the called methode/function
 */
return_result_t host_set_i2c_addres_by_phy_info(capi_phy_info_t* phy_info_ptr) {    
#if 0 /* not used */
    uint32_t saved_base_addr = phy_info_ptr->base_addr; 
    uint8_t qsfp_i2c_address =0;

    qsfp_i2c_address = phy_info_ptr->i2c_address;
    
    if(qsfp_i2c_address==0) return RR_SUCCESS;
    else {
       set_i2c_driver_target_address(QSFP_I2C_SLAVE_ADDRESS_DEFAULT); /* set driver i2c target to 0x50*/
       phy_info_ptr->base_addr = MGT;   
       hsip_wr_reg_(phy_info_ptr, I2CS_I2C_ADDR, qsfp_i2c_address);
       set_i2c_driver_target_address(qsfp_i2c_address); /* set driver i2c target to qsfp_i2c_address*/
    }
    
    phy_info_ptr->base_addr = saved_base_addr;
#endif
    return RR_SUCCESS;

}

/**
 * @brief      host_pre_mdio_download(capi_phy_info_t* phy_info_ptr)
 * @details    
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr
 *
 * @return     returns the result of the called methode/function
 */
return_result_t host_pre_sram_download(capi_phy_info_t* phy_info_ptr)
{
    return_result_t ret_val = RR_SUCCESS;
    phy_info_t fw_phy; 
    /*capi_chip_info_t capi_chip_info;*/
    uint32_t i;

    CAPI_LOG_FUNC("capi_pre_mdio_download", phy_info_ptr);

    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));
   

    fw_phy.base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(&fw_phy, REG_BOOT_EN_CTRL, BOOT_EN_VAL, 0x0);
    hsip_wr_field_(&fw_phy, REG_BOOT_EN_CTRL, BOOT_EN_OVR, 0x1);

    fw_phy.base_addr = 0x00;
    hsip_wr_reg_(&fw_phy, 0x40000000, 0x1803); /* assert all m0 in reset */
 
    
   
    /* clear all gpcfg regs for a clean env for now */
#define GP_CFG_NUM 128
#define GP_CFG_START (OCTAL_TOP_REGS + QUAD_CORE_CONFIG_CFGVAL_0_RDB)
    fw_phy.base_addr = 0x00;
    for (i=0; i<GP_CFG_NUM; i++) {
        if(i==23) /*skip top internal config register FW_INTERNAL_CONFIG_REG_3*/
            continue;
        hsip_wr_reg_(&fw_phy, GP_CFG_START + i*4, 0);
    }

    fw_phy.base_addr = OCTAL_TOP_REGS;
    hsip_wr_field_(&fw_phy, QUAD_CORE_EGRMGT_QC_CHIP_SW_RST_RDB, MD_CHIP_SW_RST, 0x1);
    delay_ms(1);

    return ret_val;
}

/**
 * @brief      host_set_clock(capi_phy_info_t* phy_info_ptr)
 * @details    
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr
 *
 * @return     returns the result of the called methode/function
 */
return_result_t host_set_clock(capi_phy_info_t* phy_info_ptr){

    phy_info_t fw_phy; 
    util_memset(&fw_phy, 0, sizeof(phy_info_t));
    util_memcpy(&fw_phy, phy_info_ptr, sizeof(phy_info_t));

    fw_phy.base_addr = TOP_LW_BLOCK_CH0;
    hsip_wr_field_(&fw_phy, ANA_PLL_ANA_PLL_CTRL_3, RTL_DIV_EN_FB ,0x1 ); 
    hsip_wr_field_(&fw_phy, ANA_PLL_ANA_PLL_PAD_CTRL_OVRD, XREF_SEL_OVRD_VAL, 0x1);
    hsip_wr_field_(&fw_phy, ANA_PLL_ANA_PLL_PAD_CTRL_OVRD, XREF_SEL_OVRD, 0x1);
    hsip_wr_field_(&fw_phy,  ANA_PLL_ANA_PLL_CTRL_1, REFCLK_FREQ2X_EN, 0x1);

    return RR_SUCCESS;
}