/**
 *
 * @file     capi_test.c
 * @author   
 * @date     9/15/2018
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "access.h"
#include "common_def.h"
#include "chip_config_def.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "capi_test_def.h"
#include "host_diag_util.h"
#include "host_log_util.h"
#include "capi.h"
#include "capi_test.h"
#include "host_test.h"

/**
* @brief      capi_set_prbs_info(capi_phy_info_t* phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
* @details    This API is used to configure the PRBS pattern generator and enable or disable it
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  prbs_info_ptr: this pointer contain PRBS config info
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_set_prbs_info(capi_phy_info_t* phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
{
    capi_command_info_t command_info;
    return_result_t ret_result;

    CAPI_LOG_FUNC("capi_set_prbs_info", phy_info_ptr);


    command_info.command_id = COMMAND_ID_SET_PRBS_INFO;
    command_info.type.prbs_info = *prbs_info_ptr;

    if (command_info.type.prbs_info.ptype == CAPI_PRBS_GEN_MON)
    {
        /* Send GEN command first */
        command_info.type.prbs_info.ptype = CAPI_PRBS_GENERATOR;
        ret_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_info_t), SET_CONFIG);
        if (ret_result == RR_SUCCESS) {
            /* Send MON command next */
            command_info.type.prbs_info.ptype = CAPI_PRBS_MONITOR;
            ret_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_info_t), SET_CONFIG);
        }
    } else {
        ret_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_info_t), SET_CONFIG);
    }
    return ret_result;
}

/**
* @brief      capi_get_prbs_info(capi_phy_info_t* phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
* @details    This API is to the the configuration setting of othe PRBS pattern generator
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out] prbs_info_ptr: this pointer return PRBS config information
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_get_prbs_info(capi_phy_info_t* phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
{
    capi_command_info_t command_info;
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;

    CAPI_LOG_FUNC("capi_get_prbs_info", phy_info_ptr);


    command_info.command_id = COMMAND_ID_GET_PRBS_INFO;
    command_info.type.prbs_info = *prbs_info_ptr;

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_info_t), GET_CONFIG);

    *prbs_info_ptr = command_info.type.prbs_info;

    if (return_result == RR_SUCCESS) {
        CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
    }
    return(return_result);
}

/**
* @brief      capi_get_prbs_status(capi_phy_info_t*    phy_info_ptr,
*                                 capi_prbs_status_t* prbs_st_ptr)
* @details    This API is used to get the PRBS  status
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[out] prbs_st_ptr: this pointer return PRBS status information
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_get_prbs_status(capi_phy_info_t*    phy_info_ptr,
                                     capi_prbs_status_t* prbs_st_ptr)
{
    return_result_t return_result;
    capi_command_info_t command_info;


    command_info.command_id = COMMAND_ID_GET_PRBS_STATUS;
    command_info.type.prbs_status_info = *prbs_st_ptr;

    CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_status_t), GET_CONFIG);
    *prbs_st_ptr = command_info.type.prbs_status_info;
    if (return_result == RR_SUCCESS) {
        CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
    }
    return(return_result);
}

/**
* @brief      capi_clear_prbs_status(capi_phy_info_t* phy_info_ptr, capi_prbs_status_t* prbs_st_ptr)
* @details    This API is used to clear the PRBS  status
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  prbs_st_ptr: this pointer return PRBS status information
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_clear_prbs_status(capi_phy_info_t* phy_info_ptr, capi_prbs_status_t* prbs_st_ptr)
{
    capi_command_info_t command_info;

    CAPI_LOG_FUNC("capi_clear_prbs_status", phy_info_ptr);
    CAPI_LOG_INFO("ptype : %s\n", util_capi_prbs_type_str(prbs_st_ptr->prbs_type));

    command_info.command_id = COMMAND_ID_CLEAR_PRBS_STATUS;
    command_info.type.prbs_status_info = *prbs_st_ptr;
    CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);

    return (capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_status_t), SET_CONFIG));
}

/**
* @brief      capi_set_loopback(capi_phy_info_t* phy_info_ptr, capi_loopback_t* loopback_ptr)
* @details    This API is used to set the loopback mode
*
* @ property  None
* @ public    None
* @ private   None
* @ example   None
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  loopback_ptr: this pointer carries the necessary information to set the loopback mode
* 
* @return     returns the performance result of the called method/function
*/
return_result_t capi_set_loopback(capi_phy_info_t* phy_info_ptr, capi_loopback_info_t* loopback_ptr)
{
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_SET_LOOPBACK_INFO;
    command_info.type.lpbk_ctrl_info = *loopback_ptr;

    CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);

    return(capi_command_request(phy_info_ptr, &command_info, sizeof(capi_loopback_info_t), SET_CONFIG));
}

/**
 * @brief      capi_get_loopback(capi_phy_info_t* phy_info_ptr, capi_loopback_t* loopback_ptr)
 * @details    This API is used to get the loopback mode <BR>
 *             Note: This function support either Line Side or Client side <BR>
 *                   This function only support single-lane operation. <BR>
 *                   If multi-lane mask is passed in, then first valid lane status will be returned. <BR>
 *             when core_ip is CORE_IP_LW, the "mode" value is: <BR>
 *             CAPI_GLOBAL_PMD_LOOPBACK_MODE: digital global loopback from Line side transmitter to receiver <BR>
 *             CAPI_REMOTE_PMD_LOOPBACK_MODE: digital remote loopback from line side receiver to transmitter <BR>
 *             when core_ip is CORE_IP_CLIENT, the "mode" value is: <BR>
 *             CAPI_SYSTEM_GLOBAL_PMD_LOOPBACK_MODE: digital global loopback from System side transmitter to receiver <BR>
 *             CAPI_SYSTEM_REMOTE_PMD_LOOPBACK_MODE: digital remote loopback from System side receiver to transmitter <BR>
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out] loopback_ptr: this pointer return loopback mode
 * 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_get_loopback(capi_phy_info_t* phy_info_ptr, capi_loopback_info_t* loopback_ptr)
{
    return_result_t return_result;
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_GET_LOOPBACK_INFO;
    command_info.type.lpbk_ctrl_info = *loopback_ptr;

    CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);

    return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_loopback_info_t), GET_CONFIG);
    *loopback_ptr = command_info.type.lpbk_ctrl_info;
    if (return_result == RR_SUCCESS) {
        CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
    }
    return(return_result);
}

/**
* @brief      capi_prbs_inject_error(capi_phy_info_t* phy_info_ptr, capi_prbs_err_inject_t* prbs_err_inj_ptr)
* @details    This API is used to clear the PRBS  status
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  ptype: this parameter indicate the PRBS type
* 
* @return     returns the performance result of the called methode/function
*/
return_result_t capi_prbs_inject_error(capi_phy_info_t* phy_info_ptr, capi_prbs_err_inject_t* prbs_err_inj_ptr)
{
    capi_command_info_t command_info;

    command_info.command_id = COMMAND_ID_INJ_PRBS_ERROR;
    command_info.type.prbs_err_inj_info = *prbs_err_inj_ptr;

    CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);

    return(capi_command_request(phy_info_ptr, &command_info, sizeof(capi_prbs_err_inject_t), SET_CONFIG));
}


/**
 * @brief      capi_test_set_command(capi_phy_info_t*          phy_info_ptr,
 *                                   capi_test_command_info_t* test_cmd_inf_ptr)
 * @details    This API is used to invoke test command.
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  capi_test_command_info_t: a reference to the test command object 
 * @return     returns the performance result of the called method/function
 */
return_result_t capi_test_set_command(capi_phy_info_t*          phy_info_ptr,
                                      capi_test_command_info_t* test_cmd_inf_ptr)
{
    uint16_t            payload_size;
    capi_command_info_t command_info;

    command_info.command_id = test_cmd_inf_ptr->command_id;

    switch(test_cmd_inf_ptr->command_id) {
        case COMMAND_ID_SET_TXPI_OVERRIDE:
            command_info.type.txpi_ovrd = test_cmd_inf_ptr->type.txpi_ovrd;
            payload_size = sizeof(txpi_override_t);
            break;

        case COMMAND_ID_SET_LW_DSP_MODE:
            return host_test_set_dsp_mode(phy_info_ptr, test_cmd_inf_ptr->type.dsp_mode);

        case COMMAND_ID_GET_LW_DSP_MODE:
            test_cmd_inf_ptr->type.dsp_mode = (capi_dsp_mode_type_t) host_test_get_dsp_mode(phy_info_ptr);
            return RR_SUCCESS;

        case COMMAND_ID_SET_TC_SE_MODE:
            return host_test_set_tc_se_mode(phy_info_ptr, test_cmd_inf_ptr->type.tc_tx_mode);

        case COMMAND_ID_GET_TC_SE_MODE:
            return host_test_get_tc_se_mode(phy_info_ptr, &test_cmd_inf_ptr->type.tc_tx_mode);

        default:
            return(RR_ERROR_WRONG_INPUT_VALUE);
    }

    return(capi_command_request(phy_info_ptr, &command_info, payload_size, SET_CONFIG));
}

