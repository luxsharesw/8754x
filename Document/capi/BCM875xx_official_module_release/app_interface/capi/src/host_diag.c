/**
 *
 * @file     host_diag.c
 * @author   HSIP FW
 * @date     8/5/2019
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include <math.h>
#include <float.h>
#include "common_def.h"
#include "access.h"
#include "estoque2_regs.h"
#include "chip_config_def.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "capi_diag_def.h"
#include "capi.h"
#include "host_diag_util.h"
#include "common_util.h"
#include "lw_common_config_ind.h"
#include "host_log_util.h"
#include "host_diag.h"
#include "fw_gp_reg_map.h"
#include "host_diag_util.h"
#include "serdes_config.h"

/* estimated lut between fec_ber and snr/ltp */
static serdes_snr_ltp_t client_cmis_lut[] = {
    {4325, 27195},
    {4294, 26440},
    {4275, 25997},
    {4262, 25684},
    {4251, 25441},
    {4242, 25242},
    {4235, 25074},
    {4228, 24929},
    {4223, 24801},
    {4218, 24686},
    {4183, 23932},
    {4162, 23491},
    {4147, 23178},
    {4136, 22936},
    {4126, 22738},
    {4118, 22570},
    {4111, 22425},
    {4104, 22297},
    {4099, 22183},
    {4060, 21431},
    {4037, 20991},
    {4020, 20679},
    {4007, 20437},
    {3997, 20239},
    {3987, 20072},
    {3980, 19928},
    {3972, 19800},
    {3966, 19686},
    {3923, 18936},
    {3897, 18498},
    {3878, 18186},
    {3863, 17946},
    {3851, 17749},
    {3840, 17582},
    {3831, 17438},
    {3823, 17311},
    {3816, 17197},
    {3766, 16450},
    {3736, 16013},
    {3715, 15704},
    {3697, 15463},
    {3683, 15268},
    {3671, 15102},
    {3661, 14958},
    {3651, 14832},
    {3643, 14719},
    {3585, 13975},
    {3550, 13540},
    {3524, 13232},
    {3504, 12994},
    {3487, 12799},
    {3473, 12634},
    {3460, 12492},
    {3449, 12366},
    {3439, 12253},
    {3370, 11515},
    {3327, 11084},
    {3296, 10778},
    {3271, 10541},
    {3251, 10348},
    {3233, 10184},
    {3218, 10043},
    {3204, 9918},
    {3191, 9807},
    {3105, 9075},
    {3052, 8649},
    {3012, 8347},
    {2980, 8113},
    {2954, 7922},
    {2931, 7761},
    {2911, 7621},
    {2893, 7498},
    {2876, 7388},
    {2762, 6668},
    {2690, 6249},
    {2636, 5953},
    {2593, 5723},
    {2556, 5537},
    {2523, 5379},
    {2495, 5243},
    {2469, 5123},
    {2446, 5016},
    {2278, 4315},
    {2169, 3910},
    {2084, 3624},
    {2015, 3405},
    {1955, 3226},
    {1902, 3076},
    {1854, 2946},
    {1810, 2832},
    {1770, 2731},
};

const static ubaddr_t line_lane_bbaddr[MAX_LW_LANES] = { TOP_LW_BLOCK_CH0,    /**< Line Lane Channel 0 Register Address */
                                                              TOP_LW_BLOCK_CH1,    /**< Line Lane Channel 1 Register Address */
                                                              TOP_LW_BLOCK_CH2,    /**< Line Lane Channel 2 Register Address */
                                                              TOP_LW_BLOCK_CH3     /**< Line Lane Channel 3 Register Address */
                                                            };

/**
 * @brief      host_util_get_lw_phy_info(capi_phy_info_t* capi_phy_info_ptr, phy_info_t* phy_info_ptr, uint8_t lane_index)
 * @details    populate new phy info structure based on lane index
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param[in]  capi_phy_info_ptr  : phy info pointer that carries the original information
 * @param[in]  phy_info_ptr       : phy info pointer to be populated
 * @param[in]  lane_id            : LW lane index
 *
 * @return void
 */
void host_util_get_lw_phy_info(capi_phy_info_t* capi_phy_info_ptr,
                               phy_info_t*      phy_info_ptr,
                               uint8_t          lane_index)
{
    util_memcpy(phy_info_ptr, capi_phy_info_ptr, sizeof(phy_info_t));

    phy_info_ptr->phy_id    += 0;
    phy_info_ptr->base_addr =  line_lane_bbaddr[lane_index];
    phy_info_ptr->lane_mask =  1 << lane_index;
}

/**
 * @brief      host_util_get_lane_lw_top_pam_bbaddr(uint8_t lane_index)
 *
 * @details    Get TOP PAM base address
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param[in] lane_id  : LW lane index
 *
 * @return base address
 */
ubaddr_t host_util_get_lane_lw_top_pam_bbaddr(uint8_t lane_index) {
    return line_lane_bbaddr[lane_index];
}

 return_result_t host_lw_get_smode(capi_phy_info_t* phy_info_ptr, uint8_t *smode)
 {
     uint8_t modulation;
     uint16_t rd_val;
      ERR_HSIP(modulation = (uint8_t)hsip_rd_field_(phy_info_ptr, COMMON_LW_LANE_CONFIG26_REG, MODULATION));
      if (modulation) {
         ERR_HSIP(rd_val = hsip_rd_field_(phy_info_ptr, EQ_EQ_CTRL_0, ESLICING_MODE));
         if (rd_val)
              *smode = DMODE_EPAM4;
         else
              *smode = DMODE_PAM4;
     } else {
          ERR_HSIP(rd_val = hsip_rd_field_(phy_info_ptr, EQ_EQ_CTRL_0, EXTENDED_PAM2_MODE));
          if (rd_val)
              *smode = DMODE_EPAM2;
          else
              *smode = DMODE_NRZ;
     }
      return RR_SUCCESS;
 }

/**
 * @brief  host_lw_get_snr_wo_suspend_resume(phy_info_t* phy_info_ptr, uint8_t mode, double* snr)
 * Calculate SNR value
 * Equation :  SNR = 10 log (signal_value) 10 log (MSE/2^28)
 * @param[in]  phy_info_ptr  phy_info containing device base address and port
 * @param[in]  snr: store snr value
 * @return  0: RR_OK; 0xFF:RR_ERROR
 */
return_result_t host_lw_get_snr_wo_suspend_resume(phy_info_t* phy_info_ptr, float* snr)
{
    double mval;
    uint32_t mse_val;
    double tmp;
    return_result_t return_slt=RR_SUCCESS;
    uint16_t ffe2_en=0/*, eqpost_mode*/;
    uint8_t mode;
    uint8_t lane_index;
    uint16_t max_lanes = MAX_LW_LANES;
    capi_phy_info_t lw_phy;

    for (lane_index = 0; lane_index < max_lanes; lane_index++) {
        if (phy_info_ptr->lane_mask & (0x01 << lane_index)) {
            if (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lane_index);
                if (host_lw_get_smode(&lw_phy, &mode) != RR_SUCCESS) {
                    CAPI_LOG_ERROR("Failed to get data mode info on lane %d\n", lane_index);
                    return RR_ERROR;
                }
                switch (mode) {
                case DMODE_NRZ:
                    mval = 9;
                    break;
                case DMODE_EPAM2:
                    mval = 9;
                    break;
                case DMODE_PAM4:
                    mval = 5;
                    ERR_HSIP(ffe2_en = hsip_rd_field_(&lw_phy, COMMON_LW_LANE_CONFIGD_REG, FFE2_CFG_VALID));
                    if(ffe2_en)
                        mval = 20;
                    break;
                case DMODE_EPAM4:
                    mval = 5;
                    break;
                default:
                    mval = 5;
                    break;
                }
                {
                    hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_0, MSE_STROBE, 0);
                    hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_0, MSE_STROBE, 1);
                    hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_1, MSE_LV, 0x7);
                    ERR_HSIP(mse_val = (uint32_t)hsip_rd_reg_(&lw_phy, EQ_SLICE_SLICE_MSE_STAT_1));
                    ERR_HSIP(mse_val = ((mse_val << 16) | hsip_rd_reg_(&lw_phy, EQ_SLICE_SLICE_MSE_STAT_0)));
                }
                /*post processing to calculate SNR */
                tmp = ((double)mse_val) / (1 << 28);
                if (tmp == 0)
                    tmp = 0.0000001;

                *snr = (float)(10 * log10(mval) - 10 * log10(tmp));
                return return_slt;
            }
        }
    }
    return RR_ERROR_WRONG_INPUT_VALUE;
}

/**
 * @brief  host_lw_get_snr(phy_info_t* phy_info_ptr, uint8_t mode, double* snr)
 * Calculate SNR value
 * Equation :  SNR = 10 log (signal_value) 10 log (MSE/2^28)
 * @param[in]  phy_info_ptr  phy_info containing device base address and port
 * @param[in]  snr: store snr value
 * @return  0: RR_OK; 0xFF:RR_ERROR
 */
return_result_t host_lw_get_snr(phy_info_t* phy_info_ptr, float* snr)
{
    double mval;
    uint32_t mse_val;
    double tmp;
    return_result_t return_slt=RR_SUCCESS;
    uint16_t ffe2_en=0/*, eqpost_mode*/;
    uint8_t mode;
    uint8_t lane_index;
    uint16_t max_lanes = MAX_LW_LANES;
    capi_phy_info_t lw_phy;
    capi_lane_ctrl_info_t lane_ctrl_info = {0};

    for (lane_index = 0; lane_index < max_lanes; lane_index++) {
        if (phy_info_ptr->lane_mask & (0x01 << lane_index)) {
            if (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lane_index);
                if (host_lw_get_smode(&lw_phy, &mode) != RR_SUCCESS) {
                    CAPI_LOG_ERROR("Failed to get data mode info on lane %d\n", lane_index);
                    return RR_ERROR;
                }
                switch (mode) {
                case DMODE_NRZ:
                    mval = 9;
                    break;
                case DMODE_EPAM2:
                    mval = 9;
                    break;
                case DMODE_PAM4:
                    mval = 5;
                    ERR_HSIP(ffe2_en = hsip_rd_field_(&lw_phy, COMMON_LW_LANE_CONFIGD_REG, FFE2_CFG_VALID));
                    if(ffe2_en)
                        mval = 20;
                    break;
                case DMODE_EPAM4:
                    mval = 5;
                    break;
                default:
                    mval = 5;
                    break;
                }
                lane_ctrl_info.param.is.rx_suspend_resume = 1;
                lane_ctrl_info.cmd_value.is.rx_suspend_resume = 1;
                if ( capi_set_lane_ctrl_info(phy_info_ptr, &lane_ctrl_info)!= RR_SUCCESS) {
                   CAPI_LOG_ERROR("Failed to suspend lane %d\n", lane_index);
                   return RR_ERROR;
                }
                {
                    hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_0, MSE_STROBE, 0);
                    hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_0, MSE_STROBE, 1);
                    hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_1, MSE_LV, 0x7);
                    ERR_HSIP(mse_val = (uint32_t)hsip_rd_reg_(&lw_phy, EQ_SLICE_SLICE_MSE_STAT_1));
                    ERR_HSIP(mse_val = ((mse_val << 16) | hsip_rd_reg_(&lw_phy, EQ_SLICE_SLICE_MSE_STAT_0)));
                }
                lane_ctrl_info.param.is.rx_suspend_resume = 1;
                lane_ctrl_info.cmd_value.is.rx_suspend_resume = 0;
                if ( capi_set_lane_ctrl_info(phy_info_ptr, &lane_ctrl_info)!= RR_SUCCESS) {
                   CAPI_LOG_ERROR("Failed to suspend lane %d\n", lane_index);
                   return RR_ERROR;
                }
                /*post processing to calculate SNR */
                tmp = ((double)mse_val) / (1 << 28);
                if (tmp == 0)
                    tmp = 0.0000001;

                *snr = (float)(10 * log10(mval) - 10 * log10(tmp));
                return return_slt;
            }
        }
    }
    return RR_ERROR_WRONG_INPUT_VALUE;
}
/**
 * @brief      host_lw_get_lvl_snr(capi_phy_info_t* phy_info_ptr, double *dsnrptr, bool suspend_fw)
 * @details    This API is used to read SNR leveled
 *             Note: This function include the suspend and resume; Don't apply the suspend & resume sequence external;
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[out]  dsnrptr: {SNR level number: N, level0 SNR value, level1 SNR value, level N-1 SNR value}
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t host_lw_get_lvl_snr(capi_phy_info_t* phy_info_ptr, float *dsnrptr, bool suspend_fw)
{
    uint8_t lane_index;
    uint16_t max_lanes = MAX_LW_LANES;
    capi_phy_info_t lw_phy;
    return_result_t return_result;
    uint8_t mode, mval, lvl, i, lvl_multiplier=1;
    uint32_t mse_val;
    double tmp;
    capi_config_info_t config_info = {0};
    uint16_t ffe2_en=0/*, eqpost_mode*/;
    capi_lane_ctrl_info_t lane_ctrl_info = {0};

    if (dsnrptr == NULL)
        return RR_ERROR;

    for (lane_index = 0; lane_index < max_lanes; lane_index++) {
        if (phy_info_ptr->lane_mask & (0x01 << lane_index)) {
            if (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lane_index);
                if (host_lw_get_smode(&lw_phy, &mode) != RR_SUCCESS) {
                    CAPI_LOG_ERROR("Failed to get data mode info on lane %d\n", lane_index);
                    return RR_ERROR;
                }
                switch (mode) {
                case DMODE_NRZ: /*0, 3*/
                    mval = 9;
                    lvl = 2;
                    lvl_multiplier = 3;
                    break;
                case DMODE_EPAM2:  /*0, 3, 6*/
                    mval = 9;
                    lvl = 3;
                    lvl_multiplier = 3;
                    break;
                case DMODE_PAM4:  /*0, 1, 2, 3*/
                    mval = 5;
                    lvl = 4;
                    ERR_HSIP(ffe2_en = hsip_rd_field_(&lw_phy, COMMON_LW_LANE_CONFIGD_REG, FFE2_CFG_VALID));
                    if(ffe2_en)
                        mval = 20;
                    break;
                case DMODE_EPAM4: /*0~6*/
                    mval = 5;
                    lvl = 7;
                    break;
                }
                *(dsnrptr) = lvl;

                //return_result = intf_send_fw_command(phy_info_ptr, 0x0, 1<<lane_index, CAPI_FW_CMD_SUSPEND_LINE_LANE_INGRESS);
                if(suspend_fw){
                    lane_ctrl_info.param.is.rx_suspend_resume = 1;
                    lane_ctrl_info.cmd_value.is.rx_suspend_resume = 1;
                    return_result = capi_set_lane_ctrl_info(phy_info_ptr, &lane_ctrl_info);
                    if (return_result != RR_SUCCESS) {
                       CAPI_LOG_ERROR("Failed to suspend lane %d\n", lane_index);
                       return return_result;
                    }
                }
                {
                    for (i = 0; i < lvl; i++) {
                        hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_0, MSE_STROBE, 0);
                        hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_0, MSE_STROBE, 1);
                        hsip_wr_field_(&lw_phy, EQ_EQ_MSE_CTRL_1, MSE_LV, i*lvl_multiplier);

                        ERR_HSIP(mse_val = (uint32_t)hsip_rd_reg_(&lw_phy, EQ_SLICE_SLICE_MSE_STAT_1));
                        ERR_HSIP(mse_val = ((mse_val << 16) | hsip_rd_reg_(&lw_phy, EQ_SLICE_SLICE_MSE_STAT_0)));

                        tmp = ((double)mse_val) / (1 << 28);
                        if (tmp == 0)
                            tmp = 0.0000001;

                        *(dsnrptr + i + 1) = (float)(10 * log10(mval) - 10 * log10(tmp));
                    }
                }
                //return_result = intf_send_fw_command(phy_info_ptr, 0x0, 1<<lane_index, CAPI_FW_CMD_RESUME_LINE_LANE_INGRESS);
                if(suspend_fw){
                    lane_ctrl_info.param.is.rx_suspend_resume = 1;
                    lane_ctrl_info.cmd_value.is.rx_suspend_resume = 0;
                    return_result = capi_set_lane_ctrl_info(phy_info_ptr, &lane_ctrl_info);
                    if (return_result != RR_SUCCESS) {
                       CAPI_LOG_ERROR("Failed to resume lane %d\n", lane_index);
                       return return_result;
                    }
                }
            } else {
                return (RR_ERROR_WRONG_INPUT_VALUE);
            }
        }
    }
    return (RR_SUCCESS);
}


return_result_t host_lw_get_snr_from_gp(phy_info_t* phy_info_ptr, float* snr, float *dsnrptr)
{
    double mval;
    uint32_t mse_val;
    double tmp;
    return_result_t return_slt=RR_SUCCESS;
    uint8_t mode, lane_index, idx, slvl;
    uint16_t max_lanes = MAX_LW_LANES, reg_val;
    capi_phy_info_t lw_phy;

    for (lane_index = 0; lane_index < max_lanes; lane_index++) {
        if (phy_info_ptr->lane_mask & (0x01 << lane_index)) {
            if (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lane_index);               
                ERR_HSIP(reg_val = (uint16_t)hsip_rd_reg_(&lw_phy, COMMON_LW_LANE_CONFIG29_REG));
                mode = (reg_val&COMMON_LW_LANE_CONFIG29_REG_LW_SLICE_MODE_MASK)>>COMMON_LW_LANE_CONFIG29_REG_LW_SLICE_MODE_SHIFT;
                mval = (reg_val&COMMON_LW_LANE_CONFIG29_REG_LW_MVAL_MASK)>>COMMON_LW_LANE_CONFIG29_REG_LW_MVAL_SHIFT;
                slvl = (reg_val&COMMON_LW_LANE_CONFIG29_REG_LW_SNR_LEVEL_MASK)>>COMMON_LW_LANE_CONFIG29_REG_LW_SNR_LEVEL_SHIFT;
                *dsnrptr = slvl;

                /*read MSE value*/
                lw_phy.base_addr = OCTAL_TOP_REGS + (lane_index<<3);
                ERR_HSIP(mse_val = (uint32_t)hsip_rd_reg_(&lw_phy, COMMON_LW_LANE0_MSE_MSB_REG));
                ERR_HSIP(mse_val = ((mse_val << 16) | hsip_rd_reg_(&lw_phy, COMMON_LW_LANE0_MSE_LSB_REG)));

                /*post processing to calculate SNR */
                tmp = ((double)mse_val) / (1 << 28);
                if (tmp == 0)
                    tmp = 0.0000001;

                *snr = (float)(10 * log10(mval) - 10 * log10(tmp));

                for(idx=0; idx<slvl; idx++){                    
                    lw_phy.base_addr = OCTAL_TOP_REGS + (lane_index<<5) + (idx<<3);
                    ERR_HSIP(mse_val = (uint32_t)hsip_rd_reg_(&lw_phy, COMMON_LW_LANE0_MSE_LVL0_MSB_REG));
                    ERR_HSIP(mse_val = ((mse_val << 16) | hsip_rd_reg_(&lw_phy, COMMON_LW_LANE0_MSE_LVL0_LSB_REG)));
                    tmp = ((double)mse_val) / (1 << 28);
                    if (tmp == 0)
                        tmp = 0.0000001;

                    *(dsnrptr+idx+1) = (float)(10 * log10(mval) - 10 * log10(tmp));
                }
                return return_slt;
            }
        }
    }
    return RR_ERROR_WRONG_INPUT_VALUE;
}

return_result_t host_lw_get_asnr_from_gp(phy_info_t* phy_info_ptr, float* snr)
{
    double mval;
    uint32_t mse_val;
    double tmp;
    return_result_t return_slt=RR_SUCCESS;
    uint8_t mode, lane_index;
    uint16_t max_lanes = MAX_LW_LANES, reg_val;
    capi_phy_info_t lw_phy;

    for (lane_index = 0; lane_index < max_lanes; lane_index++) {
        if (phy_info_ptr->lane_mask & (0x01 << lane_index)) {
            if (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, lane_index);                
                ERR_HSIP(reg_val = (uint16_t)hsip_rd_reg_(&lw_phy, COMMON_LW_LANE_CONFIG29_REG));
                mode = (reg_val&COMMON_LW_LANE_CONFIG29_REG_LW_SLICE_MODE_MASK)>>COMMON_LW_LANE_CONFIG29_REG_LW_SLICE_MODE_SHIFT;
                mval = (reg_val&COMMON_LW_LANE_CONFIG29_REG_LW_MVAL_MASK)>>COMMON_LW_LANE_CONFIG29_REG_LW_MVAL_SHIFT;

                /*read MSE value*/
                lw_phy.base_addr = OCTAL_TOP_REGS + (lane_index<<3);
                ERR_HSIP(mse_val = (uint32_t)hsip_rd_reg_(&lw_phy, COMMON_LW_LANE0_MSE_MSB_REG));
                ERR_HSIP(mse_val = ((mse_val << 16) | hsip_rd_reg_(&lw_phy, COMMON_LW_LANE0_MSE_LSB_REG)));

                /*post processing to calculate SNR */
                tmp = ((double)mse_val) / (1 << 28);
                if (tmp == 0)
                    tmp = 0.0000001;

                *snr = (float)(10 * log10(mval) - 10 * log10(tmp));

                return return_slt;
            }
        }
    }
    return RR_ERROR_WRONG_INPUT_VALUE;
}

/**
 * @brief      host_lw_get_usr_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
 * @details    This API is used to get user diag information for Line side <BR>
 *             when core_ip is CORE_IP_LW, the LW will report below parameters: <BR>
 *                  snr: SNR value <BR>
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  usr_diag_info_ptr: a pointer which carries detail user diag information
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t host_lw_get_usr_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* vdm_info_ptr)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;
    if (vdm_info_ptr == NULL)
        return return_result;
    if (vdm_info_ptr->diag_info_type == DIAG_INFO_TYPE_BRCM_DSP_SNR) {
            return_result = host_lw_get_snr_from_gp(phy_info_ptr, &vdm_info_ptr->type.diag_brcm_dsp_snr.brcm_snr_value,  &vdm_info_ptr->type.diag_brcm_dsp_snr.brcm_snr_level[0]);
            if (return_result)
                return return_result;
    }else 
    if (vdm_info_ptr->diag_info_type == DIAG_INFO_TYPE_BRCM_DSP_SNR_AVRG_ONLY) {
            return_result = host_lw_get_asnr_from_gp(phy_info_ptr, &vdm_info_ptr->type.diag_brcm_dsp_snr.brcm_snr_value);
            if (return_result)
                return return_result;
    }
    return (return_result);
}

/**
* @brief      host_lw_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
* @details    This API is used to get the user cmis diag info
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  usr_diag_info_ptr: this parameter
*
* @return     returns the performance result of the called method/function
*/
return_result_t host_lw_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;
    capi_command_info_t command_info;
    capi_cmis_info_t  lw_cmis_info;
    capi_cmis_info_t *lw_cmis_info_ptr = &lw_cmis_info;
    float snr_lvl[3]={0}, ltp_lvl[3]={0}, snr_min=0, ltp_min=FLT_MAX;
    uint8_t v_valid=1, level;

    if(usr_diag_info_ptr->type.diag_cmis_info.param.is.dsp_snr_ltp==0)
        return RR_ERROR_WRONG_INPUT_VALUE;

    util_memset(lw_cmis_info_ptr, 0x0, sizeof(capi_cmis_info_t));

    if (((phy_info_ptr->core_ip == CORE_IP_HOST_DSP) || (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)) &&
            (usr_diag_info_ptr->diag_info_type==DIAG_INFO_TYPE_CMIS)){
       command_info.command_id = COMMAND_ID_GET_CMIS_INFO;
       command_info.type.cmis_diag_info = *(lw_cmis_info_ptr);
       
       CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);
       
       return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_cmis_info_t), GET_CONFIG);
       
       if (return_result != RR_SUCCESS) {
           return return_result;
       }
       CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
       *(lw_cmis_info_ptr) = command_info.type.cmis_diag_info;
       
       for(level = 0; level < 4; level++){
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_target[level]      = lw_cmis_info_ptr->slicer_target[level];
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_mean_value[level]  = lw_cmis_info_ptr->slicer_mean_value[level];
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[level] = (float)sqrt(lw_cmis_info_ptr->slicer_sigma_sqr_value[level]);
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_value[level]     = lw_cmis_info_ptr->slicer_p_value[level];
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_location[level]  = lw_cmis_info_ptr->slicer_p_location[level];
       }
       
       for(level = 0; level < 3; level++){
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_threshold[level]   = lw_cmis_info_ptr->slicer_threshold[level];
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_value[level]     = lw_cmis_info_ptr->slicer_v_value[level];
           usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_location[level]  = lw_cmis_info_ptr->slicer_v_location[level];
       }
       /* SNR = 10* log10(min{SNR0, SNR1, SNR2 }) where SNRi= (mean(i+1)- mean(i))/( sigma(i+1) +sigma(i)), expressed in 1/256 dB units
          LTP = 10* log10(min{LTP0, LTP1, LTP2 }) where LTPi= (P(i+1) + P(i))/( 2V(i)), expressed in 1/256 dB units*/
       for(level = 0; level < 3; level++) {
           if(usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[level]+usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[level+1]){
               snr_lvl[level] = (float)((lw_cmis_info_ptr->slicer_mean_value[level+1]-lw_cmis_info_ptr->slicer_mean_value[level])/
                                  (usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[level+1]+usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[level]));
               snr_min = (level==0)?snr_lvl[0]:((snr_lvl[level]<snr_min)?snr_lvl[level]:snr_min);
            }
            ltp_lvl[level] = 0xFFFF;
            if(lw_cmis_info_ptr->slicer_v_value[level]){
                ltp_lvl[level] = (float)((lw_cmis_info_ptr->slicer_p_value[level+1]+lw_cmis_info_ptr->slicer_p_value[level])/2/lw_cmis_info_ptr->slicer_v_value[level]);
                ltp_min = (level==0)?ltp_lvl[0]:((ltp_lvl[level]<ltp_min)?ltp_lvl[level]:ltp_min);
            }else
                v_valid&=0;
        }
        if (snr_min <= 0)
            snr_min = (float)1.0;
        usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.cmis_snr = (float)(10 * log10(snr_min)*256);
        usr_diag_info_ptr->type.diag_cmis_info.value.dsp_snr_ltp.cmis_ltp = (v_valid==0)?0xFFFF:((float)(10*log10(ltp_min)*256));
    }
    
    return return_result;
}

static return_result_t host_client_get_cmis_values(serdes_cmis_info_t *cmis_info_ptr)
{
    uint8_t key = 0, offset = sizeof(client_cmis_lut)/sizeof(serdes_snr_ltp_t) - 1, tens = 0;
    double fec_ber;
    if (cmis_info_ptr == NULL)
        return RR_ERROR_WRONG_INPUT_VALUE;
    if (cmis_info_ptr->fec_ber >= 1.00e-2) {
        cmis_info_ptr->snr_ltp = client_cmis_lut[offset];
    } else if (cmis_info_ptr->fec_ber < 1.00e-12) {
        cmis_info_ptr->snr_ltp = client_cmis_lut[0];
    } else {
        fec_ber = cmis_info_ptr->fec_ber;
        do {
            fec_ber *= 10.;
            key = (uint8_t)fec_ber;
            if (key >= 1) {
                /* coverity[overrun-local]*/
                cmis_info_ptr->snr_ltp = client_cmis_lut[key-1+9*(11-tens)];
                break;
            }
            tens++;
        } while(1);
    }
    return RR_SUCCESS;
}

/**
 * @brief      host_client_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
 * @details    This API is used to get user cmis diag information for serdes side <BR>
 *             when core_ip is CORE_IP_HOST/MEDIA_SERDES, the serdes will report cmis snr and ltp parameters <BR>
 *
 * @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
 * @param[in]  usr_diag_info_ptr: a pointer which carries detail user diag information
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t host_client_get_usr_cmis_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t lane_index;
    if(usr_diag_info_ptr->type.diag_cmis_info.param.is.serdes_snr_ltp==0)
        return RR_ERROR_WRONG_INPUT_VALUE;

    for (lane_index = 0; lane_index < MAX_CLIENT_LANES; lane_index++) {
        if (phy_info_ptr->lane_mask & (0x01 << lane_index)) {
            if (host_client_get_cmis_values(&usr_diag_info_ptr->type.diag_cmis_info.value.serdes_snr_ltp) == RR_SUCCESS) {
                CAPI_LOG_INFO("\t Client_diagnostics cmis value %d %d\n",
                usr_diag_info_ptr->type.diag_cmis_info.value.serdes_snr_ltp.snr_ltp.snr_value,
                usr_diag_info_ptr->type.diag_cmis_info.value.serdes_snr_ltp.snr_ltp.ltp_value);
                return RR_SUCCESS;
            } else {
                CAPI_LOG_ERROR("Failed to get Client cmis values lane %d\n", lane_index);
                return return_result;
            }
        }
    }
    return (return_result);
}


/**
* @brief      host_dsp_get_slicer_histogram_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
* @details    This API is used to get the slicer histogram
*
* @param[in]  phy_info_ptr: a pointer which carries detail information necessary to identify the Phy
* @param[in]  usr_diag_info_ptr: this parameter
*
* @return     returns the performance result of the called method/function
*/
return_result_t host_dsp_get_slicer_histogram_diagnostics(capi_phy_info_t* phy_info_ptr, capi_usr_diag_info_t* usr_diag_info_ptr)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;
    capi_command_info_t command_info;
    capi_shstgrm_info_t dsp_shistogram_info;
    capi_shstgrm_info_t *dsp_shistogram_info_ptr = &dsp_shistogram_info;
    uint16_t idx;
    capi_phy_info_t capi_phy;

    util_memcpy((void *)&capi_phy, phy_info_ptr, sizeof(capi_phy_info_t));
    capi_phy.base_addr = 0;
    dsp_shistogram_info_ptr->shist_sel = usr_diag_info_ptr->type.diag_slc_hstgrm.shist_sel;

    if (((phy_info_ptr->core_ip == CORE_IP_HOST_DSP) || (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)) &&
            (usr_diag_info_ptr->diag_info_type==DIAG_INFO_TYPE_SLICE_HISTOGRAM)){
       command_info.command_id = COMMAND_ID_GET_SHIST_INFO;
       command_info.type.shistogram_info = *(dsp_shistogram_info_ptr);
       
       CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_USR_INPUT);
       
       return_result = capi_command_request(phy_info_ptr, &command_info, sizeof(capi_usr_diag_info_t), GET_CONFIG);
       
       if (return_result != RR_SUCCESS) {
           return return_result;
       }
       CMD_SANITY_CHECK(phy_info_ptr, &command_info, CHK_FW_OUTPUT);
       *(dsp_shistogram_info_ptr) = command_info.type.shistogram_info;
       for(idx = 0; idx < 256; idx++){
           ERR_HSIP(usr_diag_info_ptr->type.diag_slc_hstgrm.shist_data[idx] = hsip_rd_reg_(&capi_phy, (dsp_shistogram_info_ptr->shist_loc + (idx<<2))));
       }
    }
    
    return return_result;
}



/**
 * @brief      host_set_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_set_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr)
{
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t             lane_index;
    uint16_t            ratio_threshold_int;
    uint16_t            lane_mask = phy_info_ptr->lane_mask;
    phy_info_t          lw_phy_cfg;
    
   for (lane_index = 0; lane_index < MAX_LW_LANES; ++lane_index) {
       if ((lane_mask & (1 << lane_index)) == 0)
           continue;
       phy_info_ptr->lane_mask = (1 << lane_index);
       host_util_get_lw_phy_info(phy_info_ptr, &lw_phy_cfg, lane_index);
       hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG28_REG, LW_DISABLE_FFE_HISTOGRAM_MPI_DETECT,  mpi_cfg_ptr->mpi_enable?0:1);
       hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG24_REG, FFE_SLICER_TH_MPI_INTERVAL_TIMER,  mpi_cfg_ptr->mpi_interval);
       ratio_threshold_int = (uint16_t)((mpi_cfg_ptr->ratio_threshold+0.005) * 100);
       hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG21_REG, FFE_SLICER_CMIS_MPI_METRIC_RATIO_TH,  ratio_threshold_int);
       return_result = RR_SUCCESS;
   }
    return return_result;
}

/**
 * @brief      host_get_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_mpi_config(capi_phy_info_t*    phy_info_ptr, dsp_mpi_cfg_info_t *mpi_cfg_ptr)
{
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t             lane_index;
    uint16_t            lane_mask = phy_info_ptr->lane_mask;
    phy_info_t          lw_phy_cfg;
    
   for (lane_index = 0; lane_index < MAX_LW_LANES; ++lane_index) {
       if ((lane_mask & (1 << lane_index)) == 0)
           continue;
       phy_info_ptr->lane_mask = (1 << lane_index);
       host_util_get_lw_phy_info(phy_info_ptr, &lw_phy_cfg, lane_index);
       ERR_HSIP( mpi_cfg_ptr->mpi_enable  = !((uint16_t) hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG28_REG, LW_DISABLE_FFE_HISTOGRAM_MPI_DETECT)));
       ERR_HSIP( mpi_cfg_ptr->mpi_interval = (uint16_t) hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG24_REG, FFE_SLICER_TH_MPI_INTERVAL_TIMER));
       ERR_HSIP( mpi_cfg_ptr->ratio_threshold = (float) hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG21_REG, FFE_SLICER_CMIS_MPI_METRIC_RATIO_TH)/100);
       return_result =  RR_SUCCESS;
   }
    return return_result;
}

/**
 * @brief      host_get_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr)
 *
 * @details    get mpi state information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_st_ptr      : mpi state data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr)
{
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t             lane_index, sidx;
    uint16_t            lane_mask = phy_info_ptr->lane_mask;
    phy_info_t          lw_phy_cfg;
    float               b11, b21=0,  mpi_normalized_s_sqr[4];

    if(phy_info_ptr->core_ip != CORE_IP_MEDIA_DSP ||
        util_check_param_range(mpi_st_ptr->read_clear, 0, 1) == FALSE)
        return RR_ERROR_WRONG_INPUT_VALUE;

   for (lane_index = 0; lane_index < MAX_LW_LANES; ++lane_index) {
       if ((lane_mask & (1 << lane_index)) == 0)
           continue;
       phy_info_ptr->lane_mask = (1 << lane_index);
       host_util_get_lw_phy_info(phy_info_ptr, &lw_phy_cfg, lane_index);
       ERR_HSIP( mpi_st_ptr->dust_indicator         = (uint8_t)hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG22_REG, MPI_HW_HISTOGRAM_RATIO_INDICATOR));
       ERR_HSIP( mpi_st_ptr->dust_indicator_sticky  = (uint8_t)hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG22_REG, MPI_HW_HISTOGRAM_RATIO_INDICATOR_STICKY));
       if(mpi_st_ptr->read_clear)
           hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG22_REG, MPI_HW_HISTOGRAM_RATIO_INDICATOR_STICKY, 0);
       /*Normalized standard deviation of level-0~3 */
       ERR_HSIP(mpi_st_ptr->mpi_normalized_s[0] = (float)sqrt(hsip_rd_field_(&lw_phy_cfg,  COMMON_LW_LANE_CONFIG35_REG, FFE_SLICER_CMIS_SIGMA_0))/10);
       ERR_HSIP(mpi_st_ptr->mpi_normalized_s[1] = (float)sqrt(hsip_rd_field_(&lw_phy_cfg,  COMMON_LW_LANE_CONFIG36_REG, FFE_SLICER_CMIS_SIGMA_1))/10);
       ERR_HSIP(mpi_st_ptr->mpi_normalized_s[2] = (float)sqrt(hsip_rd_field_(&lw_phy_cfg,  COMMON_LW_LANE_CONFIG37_REG, FFE_SLICER_CMIS_SIGMA_2))/10);
       ERR_HSIP(mpi_st_ptr->mpi_normalized_s[3] = (float)sqrt(hsip_rd_field_(&lw_phy_cfg,  COMMON_LW_LANE_CONFIG38_REG, FFE_SLICER_CMIS_SIGMA_3))/10);
       
       for(sidx=0; sidx<4; sidx++){
            mpi_normalized_s_sqr[sidx ]  = (mpi_st_ptr->mpi_normalized_s[sidx]) * (mpi_st_ptr->mpi_normalized_s[sidx]);
            b21 += mpi_normalized_s_sqr[sidx ];
       }


       /* B11 = s1+2*s2+3*s3
          B21 = s0+s1+s2+s3
          Slope is = 0.2*B11 - 0.3*B21
          Intercept is = -0.3*B11 + 0.7*B21
          Then calculate MPI metric as: new_slope + 0.25*new_c0
       */
       b11                        = mpi_normalized_s_sqr[1] + 2*mpi_normalized_s_sqr[2] + 3*mpi_normalized_s_sqr[3];
       mpi_st_ptr->mpi_slope      = (float)(0.2*b11 - 0.3*b21);
       mpi_st_ptr->mpi_intercept  = (float)(-0.3*b11 + 0.7*b21);
       ERR_HSIP(  mpi_st_ptr->mpi_metric = (float)hsip_rd_reg_(&lw_phy_cfg,  COMMON_LW_LANE_CONFIG30_REG)/1000);
       return RR_SUCCESS;
   }
    return return_result;
}

/**
 * @brief      host_set_mpi_dynamic_config(capi_phy_info_t*    phy_info_ptr, 
 *                                          dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_set_mpi_dynamic_config(capi_phy_info_t*            phy_info_ptr,
                                            dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr)
{
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t             lane_index;
    uint16_t            lane_mask = phy_info_ptr->lane_mask;
    phy_info_t          lw_phy_cfg;
    
   for (lane_index = 0; lane_index < MAX_LW_LANES; ++lane_index) {
       if ((lane_mask & (1 << lane_index)) == 0)
           continue;
       phy_info_ptr->lane_mask = (1 << lane_index);
       host_util_get_lw_phy_info(phy_info_ptr, &lw_phy_cfg, lane_index);
       hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG32_REG, MISSION_MPI_BLW_ST_CHK_CNT, mpi_cfg_ptr->mpi_chk_cnt);
       hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG19_REG, MISSION_MPI_DETECT_SUM_TH, mpi_cfg_ptr->mpi_chk_th);
       hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG16_REG, LW_DISABLE_MISSION_MPI_DETECT,  mpi_cfg_ptr->mpi_enable?0:1);
       return_result = RR_SUCCESS;
   }
    return return_result;
}

/**
 * @brief      host_get_mpi_dynamic_config(capi_phy_info_t*    phy_info_ptr, 
 *                                          dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr)
 *
 * @details    get mpi dynamic configuration information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_cfg_ptr      : mpi dynamic config data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_mpi_dynamic_config(capi_phy_info_t*            phy_info_ptr,
                                            dsp_mission_mpi_cfg_info_t *mpi_cfg_ptr)
{
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t             lane_index;
    uint16_t            lane_mask = phy_info_ptr->lane_mask;
    phy_info_t          lw_phy_cfg;
    
   for (lane_index = 0; lane_index < MAX_LW_LANES; ++lane_index) {
       if ((lane_mask & (1 << lane_index)) == 0)
           continue;
       phy_info_ptr->lane_mask = (1 << lane_index);
       host_util_get_lw_phy_info(phy_info_ptr, &lw_phy_cfg, lane_index);
       ERR_HSIP( mpi_cfg_ptr->mpi_enable  = !((uint16_t) hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG16_REG, LW_DISABLE_MISSION_MPI_DETECT)));
       ERR_HSIP( mpi_cfg_ptr->mpi_chk_cnt = (uint16_t) hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG32_REG, MISSION_MPI_BLW_ST_CHK_CNT));
       ERR_HSIP( mpi_cfg_ptr->mpi_chk_th  = (uint16_t) hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG19_REG, MISSION_MPI_DETECT_SUM_TH));
       return  RR_SUCCESS;
   }
   return return_result;
}

/**
 * @brief      host_get_dynamic_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr)
 *
 * @details    get mpi steady state information
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      mpi_st_ptr      : mpi state data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_dynamic_mpi_state(capi_phy_info_t*    phy_info_ptr, dsp_mpi_st_info_t *mpi_st_ptr)
{
    return_result_t     return_result = RR_ERROR_WRONG_INPUT_VALUE;
    uint8_t             lane_index;
    uint16_t            lane_mask = phy_info_ptr->lane_mask;
    phy_info_t          lw_phy_cfg;
    
    if(phy_info_ptr->core_ip != CORE_IP_MEDIA_DSP ||
        util_check_param_range(mpi_st_ptr->read_clear, 0, 1) == FALSE)
        return RR_ERROR_WRONG_INPUT_VALUE;

   for (lane_index = 0; lane_index < MAX_LW_LANES; ++lane_index) {
       if ((lane_mask & (1 << lane_index)) == 0)
           continue;
       phy_info_ptr->lane_mask = (1 << lane_index);
       host_util_get_lw_phy_info(phy_info_ptr, &lw_phy_cfg, lane_index);
       
       ERR_HSIP( mpi_st_ptr->dust_indicator         = (uint8_t)hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG22_REG, MISSION_MPI_DUST_INDICATOR));
       ERR_HSIP( mpi_st_ptr->dust_indicator_sticky  = (uint8_t)hsip_rd_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG22_REG, MISSION_MPI_DUST_INDICATOR_STICKY));
       if(mpi_st_ptr->read_clear)
           hsip_wr_field_(&lw_phy_cfg, COMMON_LW_LANE_CONFIG22_REG, MISSION_MPI_DUST_INDICATOR_STICKY, 0);
       return RR_SUCCESS;
   }
    return return_result;
}
