/**
 *
 * @file    host_diag_util.c
 * @author  Firmware Team
 * @date    03/01/2019
 * @version 0.2
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "access.h"
#include "common_def.h"
#include "regs_common.h"
#include "chip_config_def.h"
#include "chip_common_config_ind.h"
#include "chip_mode_def.h"
#include "cw_def.h"
#include "capi_def.h"
#include "capi_test_def.h"
#include "host_log_util.h"
#include "chip_mode_def.h"
#include "common_util.h"
#include "host_diag_util.h"
#include "hr_time.h"
#include "hw_mutex_handler.h"
#include "host_to_chip_ipc.h"

extern const capi_port_entry_t ports[];
extern uint8_t port_total_entries;
extern const cw_chip_port_info_t cw_cmode[];

#define ERR_INVALID_RANGE(src, low, upper)   do { \
            if (!util_check_param_range((int)src, (int)low, (int)upper)) { \
                CAPI_LOG_ERROR("Invalid input: " #src " -> %d (valid: %d to %d)\n", src, low, upper); \
                return RR_ERROR_WRONG_INPUT_VALUE;\
              }\
            }   while(0)

/**
 * @brief      util_check_param_range(int source, int lower_limit, int upper_limit)
 * @details    This API is used to check if a parameter is inside lower and upper limit
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  source: parameter to be checked
 * @param[in]  lower_limit: lower boundary for source
 * @param[in]  upper_limit: upper boundary for source
 *
 * @return     TRUE: source is in lower/upper limit, FALSE: source is outside lower/upper limit
 */

bool util_check_param_range(int source, int lower_limit, int upper_limit) {
    if (source < lower_limit) { /* Check if source is less than the lower limit */
        return FALSE; /* Return false */
    }
    if (source > upper_limit) { /* Check if source is more than the upper limit */
        return FALSE; /* Return false */
    }
    return TRUE; /* Otherwise return true */
}


/**
 * @brief      sanity_checker_lane_rx_info(capi_phy_info_t* phy_info_ptr,
 *                                         lane_rx_info_t*  lane_rx_info_ptr,
 *                                         uint8_t          set_get_request,
 *                                         sanity_chk_t     chk_type)
 * @details    This API is used to sanity check lane config params
 *
 * @param[in]  phy_info_ptr: reference to the phy information object
 * @param[in]  lane_rx_info_ptr: reference to the lane rx information object
 * @param[in]  set_get_request: get or set request
 * @param[in]  chk_type: sanity cheker type
 *
 * @return     returns the performance result of the called method/function
 */
static return_result_t sanity_checker_lane_rx_info(capi_phy_info_t* phy_info_ptr,
                                                   lane_rx_info_t*  lane_rx_info_ptr,
                                                   uint8_t          set_get_request,
                                                   sanity_chk_t     chk_type)
{
    return_result_t return_result = RR_SUCCESS;

    if (!lane_rx_info_ptr->param.content) return(RR_ERROR_NOT_INITIALIZED);

    if ((phy_info_ptr->core_ip == CORE_IP_ALL) ||
        (phy_info_ptr->core_ip == CORE_IP_CW)  ||
        (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC)) {
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
    }

    /*1.Only support (CHK_USR_INPUT & SET_CONFIG) or (CHK_FW_OUTPUT & GET_CONFIG)*/
    if (!(((CHK_USR_INPUT == chk_type) && (set_get_request == SET_CONFIG)) ||
         ((CHK_FW_OUTPUT == chk_type) && (set_get_request == GET_CONFIG)))) {
        return RR_SUCCESS;
    }

    /*2.Check exclusive parameters for different core IP*/
    if ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP) ||
        (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)) {
        if (lane_rx_info_ptr->param.is.serdes_lp_has_prec_en ||
            lane_rx_info_ptr->param.is.serdes_unreliable_los ||
            lane_rx_info_ptr->param.is.serdes_scrambling_off ||               
            lane_rx_info_ptr->param.is.serdes_db_loss        ||
            lane_rx_info_ptr->param.is.serdes_pam_er_nr) {
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
        }
    } else if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
               (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
        if (lane_rx_info_ptr->param.is.dsp_graycode                    ||
            lane_rx_info_ptr->param.is.dsp_dc_wander_mu                ||
            lane_rx_info_ptr->param.is.dsp_gain_boost                  ||               
            lane_rx_info_ptr->param.is.dsp_nldet_en                    ||
            lane_rx_info_ptr->param.is.dsp_low_bw_en                   ||
            lane_rx_info_ptr->param.is.dsp_gain2_on                    ||
            lane_rx_info_ptr->param.is.dsp_lms_mode                    ||
            lane_rx_info_ptr->param.is.dsp_autopeaking_en              ||
            lane_rx_info_ptr->param.is.dsp_exslicer                    ||
            lane_rx_info_ptr->param.is.dsp_phase_bias_auto_tuning_info ||
            lane_rx_info_ptr->param.is.dsp_kp_kf_info                  ||
            lane_rx_info_ptr->param.is.dsp_ffe_info                    ||
            lane_rx_info_ptr->param.is.dsp_los_info ) {
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
        }
    }

    /*3.Validate common parameters (range, read-oly, etc)*/
    if (lane_rx_info_ptr->param.is.vga) {
        if (phy_info_ptr->core_ip == CORE_IP_HOST_DSP  ||
            phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                if (lane_rx_info_ptr->value.vga > 46) {
                    CAPI_LOG_ERROR("DSP wrong vga value : %d \r\n", lane_rx_info_ptr->value.vga);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
                }
        } else {
            if ((lane_rx_info_ptr->value.vga > 37) ||
                (set_get_request == SET_CONFIG)) {
                    CAPI_LOG_ERROR("SERDES wrong vga value: %d   set_get_request: %d \r\n",
                        lane_rx_info_ptr->value.vga, set_get_request);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
        }
    }
    if (lane_rx_info_ptr->param.is.symbol_swap) {
        if (lane_rx_info_ptr->value.symbol_swap > 1) {
            CAPI_LOG_ERROR("Wrong symbol_swap : %d \n ", lane_rx_info_ptr->value.symbol_swap);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.media_type) {
        if (lane_rx_info_ptr->value.media_type > CAPI_MEDIA_TYPE_COPPER_CABLE) {
            CAPI_LOG_ERROR("Wrong media_type : %d \n ", lane_rx_info_ptr->value.media_type);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.peaking_filter_info) {
        if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
            (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
            if (set_get_request == SET_CONFIG) {
                CAPI_LOG_ERROR("Set peaking_filter_info isn't allowed for Serdes : %d \r\n ",
                    lane_rx_info_ptr->param.is.peaking_filter_info);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
        }
        if (lane_rx_info_ptr->value.peaking_filter_info.value > 31) {
            CAPI_LOG_ERROR("Wrong Peaking Filter value :  %d \r\n",
                lane_rx_info_ptr->value.peaking_filter_info.value);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dfe_info) {
        if (phy_info_ptr->core_ip == CORE_IP_HOST_DSP  ||
            phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {     
            if ((lane_rx_info_ptr->value.dfe_info.dfe_on > 1)) {
                    CAPI_LOG_ERROR("Wrong parameter value dfe_on: %d\n ",
                        lane_rx_info_ptr->value.dfe_info.dfe_on);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
            if ((lane_rx_info_ptr->value.dfe_info.dsp_dfe_adapt_off) ||
                (lane_rx_info_ptr->value.dfe_info.dsp_force_baud_rate_dfe) ||
                (lane_rx_info_ptr->value.dfe_info.low_power_dfe_on)) {
                    CAPI_LOG_ERROR("dsp_dfe_adapt_off, dsp_force_baud_rate_dfe, low_power_dfe_on aren't allowed for DSP\n ");
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }   
        } else if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
                   (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {   
            if ((lane_rx_info_ptr->value.dfe_info.dfe_on > 1)) {
                    CAPI_LOG_ERROR("Wrong parameter value dfe_on: %d\n ",
                        lane_rx_info_ptr->value.dfe_info.dfe_on);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
            if ((lane_rx_info_ptr->value.dfe_info.low_power_dfe_on > 1)) {
                    CAPI_LOG_ERROR("Wrong parameter value low_power_dfe_on: %d\n ",
                        lane_rx_info_ptr->value.dfe_info.low_power_dfe_on);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
            if ((lane_rx_info_ptr->value.dfe_info.dsp_dfe_adapt_off) ||
                (lane_rx_info_ptr->value.dfe_info.dsp_force_baud_rate_dfe)) {
                    CAPI_LOG_ERROR("dsp_dfe_adapt_off, dsp_force_baud_rate_dfe aren't allowed for DSP\n ");
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }   
        }
    }

    
    
    /*********************************DSP**********************************/
    /*4, validate DSP specified parameters   (range, read-only, etc)*/
    /*********************************DSP**********************************/
    if (lane_rx_info_ptr->param.is.dsp_graycode) {
        if (lane_rx_info_ptr->value.core_ip.dsp.graycode > 1) {
            CAPI_LOG_ERROR("Wrong DSP rx_graycode  %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.graycode);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_dc_wander_mu) {
        if (lane_rx_info_ptr->value.core_ip.dsp.dc_wander_mu > 6) {
            CAPI_LOG_ERROR("Wrong DSP dc_wander_mu : %d \r\n",
                lane_rx_info_ptr->value.core_ip.dsp.dc_wander_mu);
            return_result = RR_ERROR_WRONG_INPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_gain_boost) {
        if (lane_rx_info_ptr->value.core_ip.dsp.gain_boost >= 0x20) {
            CAPI_LOG_ERROR("Wrong DSP gain_boost : %d \r\n",
                lane_rx_info_ptr->value.core_ip.dsp.gain_boost);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_nldet_en) {
        if ((lane_rx_info_ptr->value.core_ip.dsp.nldet_info.nldet_en > 1) && (lane_rx_info_ptr->value.core_ip.dsp.nldet_info.nldet_en != 0xFF)) {
            CAPI_LOG_ERROR("Wrong DSP nldet_en %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.nldet_info.nldet_en);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_low_bw_en) {
        if (lane_rx_info_ptr->value.core_ip.dsp.low_bw_en > 1) {
            CAPI_LOG_ERROR("Wrong DSP low_bw_en %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.low_bw_en);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_gain2_on) {
        if (lane_rx_info_ptr->value.core_ip.dsp.gain2_on > 1) {
            CAPI_LOG_ERROR("Wrong DSP gain2_on %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.gain2_on);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_lms_mode) {
        if (lane_rx_info_ptr->value.core_ip.dsp.lms_mode > 1) {
            CAPI_LOG_ERROR("Wrong DSP lms_mode %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.lms_mode);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_autopeaking_en) {
        if (lane_rx_info_ptr->value.core_ip.dsp.autopeaking_en > 1) {
            CAPI_LOG_ERROR("Wrong DSP autopeaking_en %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.autopeaking_en);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_exslicer) {
        if (lane_rx_info_ptr->value.core_ip.dsp.exslicer > 2) {
            CAPI_LOG_ERROR("Wrong DSP exslicer %d\r\n",
                lane_rx_info_ptr->value.core_ip.dsp.exslicer);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_phase_bias_auto_tuning_info) {
        if ((lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_auto_tune_en > 1) ||
            (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.dynamic_phase_auto_tune_en > 1)) {
                CAPI_LOG_ERROR("Invalid DSP phase_auto_tune_en %d\r\n",
                    lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_auto_tune_en);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        } else if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_auto_tune_en) {
            if ((lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th <
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th)            ||
                ((lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th >  31) ||
                (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th < -32))   ||
                ((lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th >  31) ||
                (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th < -32))) {
                    CAPI_LOG_ERROR("Invalid DSP phase_bias_th=[%d, %d] \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th,
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
            if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_dwell_time < 0 ||
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_dwell_time > 4) {
                    CAPI_LOG_ERROR("Invalid DSP phase_tune_dwell_time %d \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_dwell_time);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
            if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size < 0 ||
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size > 1) {
                    CAPI_LOG_ERROR("Invalid DSP phase_tune_step_size %d \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
            if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size == 1 && (
                (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th % 2) != 0 || 
                (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th % 2) != 0)) {
                    CAPI_LOG_ERROR("Invalid DSP phase_bias_th=[%d, %d] step %d \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th,
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th,
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }  

            if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.dynamic_phase_auto_tune_en == 1 && (
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_bias_range < 0 || 
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_bias_range > 10)) {
                    CAPI_LOG_ERROR("Invalid DSP phase_dtune_bias_range=%d \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_bias_range);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }            

            if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.dynamic_phase_auto_tune_en == 1 && (
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_snr_change_th < 0 || 
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_snr_change_th > 10)) {
                    CAPI_LOG_ERROR("Invalid DSP phase_dtune_snr_change_th=%d \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_snr_change_th);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }            

            if (lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_link_down_snr < 0 || 
                lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_link_down_snr > 7) {
                    CAPI_LOG_ERROR("Invalid DSP phase_tune_link_down_snr=%d \r\n",
                        lane_rx_info_ptr->value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_link_down_snr);
                    return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }            

        }

    }
    if (lane_rx_info_ptr->param.is.dsp_kp_kf_info) {
        if ((lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp_tracking > 7) ||
            (lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kf_tracking > 7) ||
            (lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp > 7)          ||
            (lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kf > 7)          ||
            (lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp_hlf_stp > 1)  ||
            (lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp_tracking_hlf_stp > 1)) {
                CAPI_LOG_ERROR("Wrong DSP dsp_kp_kf_info : %d, %d, %d, %d, %d, %d\r\n",
                    lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp_tracking,
                    lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kf_tracking,
                    lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp,
                    lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kf,
                    lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp_hlf_stp,
                    lane_rx_info_ptr->value.core_ip.dsp.kp_kf_info.kp_tracking_hlf_stp);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_ffe_info) {
        if ((lane_rx_info_ptr->value.core_ip.dsp.ffe_info.power_mode > 3) ||
            (lane_rx_info_ptr->value.core_ip.dsp.ffe_info.ffe_slicer_multiplier > 2)) {
                CAPI_LOG_ERROR("Wrong DSP dsp_ffe_info; power_mode: %d, ffe_slicer_multiplier: %d \n ",
                    lane_rx_info_ptr->value.core_ip.dsp.ffe_info.power_mode,
                    lane_rx_info_ptr->value.core_ip.dsp.ffe_info.ffe_slicer_multiplier);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.dsp_los_info) {
        if ((lane_rx_info_ptr->value.core_ip.dsp.los_info.los_th_idx  >= 0xC) ||
            (lane_rx_info_ptr->value.core_ip.dsp.los_info.los_bypass     > 1) ||
            (lane_rx_info_ptr->value.core_ip.dsp.los_info.los_bypass_val > 1)) {
                CAPI_LOG_ERROR("Wrong dsp_los_info:   th_idx:%d   bypass:%d    bypass_val:%d \r\n",
                    lane_rx_info_ptr->value.core_ip.dsp.los_info.los_th_idx,
                    lane_rx_info_ptr->value.core_ip.dsp.los_info.los_bypass,
                    lane_rx_info_ptr->value.core_ip.dsp.los_info.los_bypass_val);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }



    /*********************************SERDES**********************************/
    /*5, validate Serdes specified parameters   (range, read-only, etc)*/
    /*********************************SERDES**********************************/
    if (lane_rx_info_ptr->param.is.serdes_lp_has_prec_en) {
        if (lane_rx_info_ptr->value.core_ip.serdes.lp_has_prec_en > 1) {
            CAPI_LOG_ERROR("Wrong Serdes lp_has_prec_en : %d \n ",
                lane_rx_info_ptr->value.core_ip.serdes.lp_has_prec_en);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.serdes_pam_er_nr) {
        if (lane_rx_info_ptr->value.core_ip.serdes.pam_er_nr > 2) {
            CAPI_LOG_ERROR("Wrong Serdes serdes_pam_er_nr : %d \n ",
                lane_rx_info_ptr->value.core_ip.serdes.pam_er_nr);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (lane_rx_info_ptr->param.is.serdes_db_loss) {
        if (lane_rx_info_ptr->value.core_ip.serdes.db_loss > 35) {
            CAPI_LOG_ERROR("Wrong Serdes serdes_db_loss : %d \n ",
                lane_rx_info_ptr->value.core_ip.serdes.db_loss);
            return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }
    if (set_get_request == SET_CONFIG) { /*serdes_unreliable_los and serdes_scrambling_off are read-only*/
        if ((lane_rx_info_ptr->param.is.serdes_unreliable_los) ||
            (lane_rx_info_ptr->param.is.serdes_scrambling_off)) {
            if (lane_rx_info_ptr->param.is.serdes_unreliable_los) {
                CAPI_LOG_ERROR("Read Only parameter serdes_unreliable_los : %d  \n ",
                    lane_rx_info_ptr->param.is.serdes_unreliable_los);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            } else {
                CAPI_LOG_ERROR("Read Only parameter serdes_scrambling_off : %d  \n ",
                    lane_rx_info_ptr->param.is.serdes_scrambling_off);
                return_result = (chk_type == CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
            }
        }        
    }

    return(return_result);
}

/**
 * @brief      sanity_checker_loopback(capi_phy_info_t*      phy_info_ptr,
 *                                              capi_loopback_info_t* loopback_ptr)
 * @details    This API is used to sanity check set loopback params
 *
 * @param[in]  phy_info_ptr: reference to the phy information object
 * @param[in]  loopback_ptr: reference to the loopback information object
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_loopback(capi_phy_info_t*      phy_info_ptr,
                                        capi_loopback_info_t* loopback_ptr)
{
    if ((phy_info_ptr->core_ip == CORE_IP_ALL) ||
        (phy_info_ptr->core_ip == CORE_IP_CW)  ||
        (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC)) {
        CAPI_LOG_ERROR("Wrong core ip : %d\n", phy_info_ptr->core_ip);
        return(RR_ERROR_WRONG_CORE_IP_VALUE);
    }

    if ((loopback_ptr->mode != CAPI_GLOBAL_LOOPBACK_MODE) &&
        (loopback_ptr->mode != CAPI_REMOTE_LOOPBACK_MODE)) {
        CAPI_LOG_ERROR("Invalid loopback mode: %d\r\n", loopback_ptr->mode);
        return(RR_ERROR_WRONG_INPUT_VALUE);
    }

    if (loopback_ptr->enable > 1) {
        CAPI_LOG_ERROR("Invalid enable value: %d\r\n", loopback_ptr->enable);
        return(RR_ERROR_WRONG_INPUT_VALUE);
    }

    return(RR_SUCCESS);
}

/**
 * @brief      sanity_checker_set_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
 * @details    This API is used to sanity check set prbs config params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  prbs_info_ptr: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_set_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
{

    if(!util_check_param_range(prbs_info_ptr->gen_switch, 0 , 1)) {
        CAPI_LOG_ERROR("Invalid switch status: %d\r\n", prbs_info_ptr->gen_switch);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }
    
    if ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP) ||
        (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)) {
        if ((prbs_info_ptr->ptype == CAPI_PRBS_MONITOR)  ||
            (prbs_info_ptr->ptype == CAPI_PRBS_GENERATOR)) {
                if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                    return RR_SUCCESS;
                switch(prbs_info_ptr->op.pcfg.poly.poly_type) {
                    case CAPI_PRBS_POLY_7:
                    case CAPI_PRBS_POLY_9  :
                    case CAPI_PRBS_POLY_10 :
                    case CAPI_PRBS_POLY_11 :
                    case CAPI_PRBS_POLY_13 :
                    case CAPI_PRBS_POLY_15 :
                    case CAPI_PRBS_POLY_20 :
                    case CAPI_PRBS_POLY_23 :
                    case CAPI_PRBS_POLY_31 :
                    case CAPI_PRBS_POLY_49 :
                    case CAPI_PRBS_POLY_58 :
                        break;
                    default:
                        CAPI_LOG_ERROR("Invalid LW poly: %d\r\n", prbs_info_ptr->op.pcfg.poly.poly_type);
                        return RR_ERROR_WRONG_INPUT_VALUE;
                }
                if(!util_check_param_range(prbs_info_ptr->op.pcfg.tx_invert, 0 , 1)) {
                    CAPI_LOG_ERROR("Invalid TX invert: %d\r\n",prbs_info_ptr->op.pcfg.tx_invert);
                    return RR_ERROR_WRONG_INPUT_VALUE;
                }
                if(!util_check_param_range(prbs_info_ptr->op.pcfg.rx_invert, 0 , 1)) {
                    CAPI_LOG_ERROR("Invalid RX invert: %d\r\n",prbs_info_ptr->op.pcfg.rx_invert);
                    return RR_ERROR_WRONG_INPUT_VALUE;
                }
                if(!util_check_param_range(prbs_info_ptr->op.pcfg.lw_checker_auto_det, 0 , 1)) {
                    CAPI_LOG_ERROR("Invalid lw checker auto det: %d\r\n",prbs_info_ptr->op.pcfg.lw_checker_auto_det);
                    return RR_ERROR_WRONG_INPUT_VALUE;
                }
        } else if ((CAPI_PRBS_SSPRQ_GEN_MON == prbs_info_ptr->ptype) ||
                  (CAPI_PRBS_SSPRQ_GENERATOR == prbs_info_ptr->ptype)     ||
                  (CAPI_PRBS_SSPRQ_MONITOR == prbs_info_ptr->ptype)) {

            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;

            if (util_check_param_range(prbs_info_ptr->op.ssprq.modulation, 0 , 1) &&
                util_check_param_range(prbs_info_ptr->op.ssprq.bit_swap  , 0 , 1) &&
                util_check_param_range(prbs_info_ptr->op.ssprq.gray_code , 0 , 1)) {
            } else {
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else if (CAPI_PRBS_SQUARE_WAVE_GENERATOR == prbs_info_ptr->ptype) {
            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;

            if (util_check_param_range(prbs_info_ptr->op.sqr_wave.ptrn_wave_type, 0, 4)) {
            } else {
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else if ((CAPI_PRBS_Q_PRBS_13_GENERATOR == prbs_info_ptr->ptype) ||
                    (CAPI_PRBS_Q_PRBS_13_MONITOR == prbs_info_ptr->ptype)  ||
                    (CAPI_PRBS_Q_PRBS_13_GEN_MON == prbs_info_ptr->ptype)) {

            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;

            if (util_check_param_range(prbs_info_ptr->op.qprbs13.invert_bits, 0, 1) &&
                util_check_param_range(prbs_info_ptr->op.qprbs13.training_frame, 0, 1)) {
            } else {
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else {
            CAPI_LOG_ERROR("Invalid ptype: %d\r\n", prbs_info_ptr->ptype);
            return RR_ERROR_WRONG_INPUT_VALUE;
        }
    } else if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) || (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
        if ((prbs_info_ptr->ptype == CAPI_PRBS_MONITOR)  ||
            (prbs_info_ptr->ptype == CAPI_PRBS_GENERATOR)) {
            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;
            switch(prbs_info_ptr->op.pcfg.poly.bh_poly) {
                case CAPI_BH_PRBS_POLY_7:
                case CAPI_BH_PRBS_POLY_9:
                case CAPI_BH_PRBS_POLY_11:
                case CAPI_BH_PRBS_POLY_15:
                case CAPI_BH_PRBS_POLY_23:
                case CAPI_BH_PRBS_POLY_31:
                case CAPI_BH_PRBS_POLY_58:
                case CAPI_BH_PRBS_POLY_49:
                case CAPI_BH_PRBS_POLY_10:
                case CAPI_BH_PRBS_POLY_20:
                case CAPI_BH_PRBS_POLY_13:
                case CAPI_BH_PRBS_USER_40_BIT_REPEAT:
                case CAPI_BH_PRBS_PRBS_AUTO_DETECT:
                  break;
                default:
                  CAPI_LOG_ERROR("Invalid BH poly: %d\r\n", prbs_info_ptr->op.pcfg.poly.bh_poly);
                  return RR_ERROR_WRONG_INPUT_VALUE;
            }
            if(!util_check_param_range(prbs_info_ptr->op.pcfg.rx_invert, 0 , 1)) {
                CAPI_LOG_ERROR("Invalid RX invert: %d\r\n",prbs_info_ptr->op.pcfg.rx_invert);
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
            if(!util_check_param_range(prbs_info_ptr->op.pcfg.tx_invert, 0 , 1)) {
                CAPI_LOG_ERROR("Invalid TX invert: %d\r\n",prbs_info_ptr->op.pcfg.tx_invert);
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
       } else if ((CAPI_PRBS_SSPRQ_GEN_MON == prbs_info_ptr->ptype) ||
                  (CAPI_PRBS_SSPRQ_GENERATOR == prbs_info_ptr->ptype)     ||
                  (CAPI_PRBS_SSPRQ_MONITOR == prbs_info_ptr->ptype)) {
            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;
            if (util_check_param_range(prbs_info_ptr->op.ssprq.modulation, 0 , 1) && /* Modulation not used in ML? Remove? */
                util_check_param_range(prbs_info_ptr->op.ssprq.bit_swap  , 0 , 1) && /* None of these used in ML */
                util_check_param_range(prbs_info_ptr->op.ssprq.gray_code , 0 , 1)) {
            } else {
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else if (CAPI_PRBS_Q_PRBS_13_GENERATOR == prbs_info_ptr->ptype) {
            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;
            if (util_check_param_range(prbs_info_ptr->op.qprbs13.invert_bits, 0, 1) &&
                util_check_param_range(prbs_info_ptr->op.qprbs13.training_frame, 0, 1)) {
            } else {
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else if (CAPI_PRBS_SQUARE_WAVE_GENERATOR == prbs_info_ptr->ptype) {
            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;
            if (util_check_param_range(prbs_info_ptr->op.sqr_wave.ptrn_wave_type, 0, 4)) {
            } else {
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        /* Enable / disable switch already confirmed by the first check */
        } else if (CAPI_PRBS_TX_LINEARITY_GENERATOR == prbs_info_ptr->ptype) {
            return RR_SUCCESS;
        } else if (CAPI_PRBS_SHARED_TX_PATTERN_GENERATOR == prbs_info_ptr->ptype) {
            if(prbs_info_ptr->gen_switch == CAPI_SWITCH_OFF)
                return RR_SUCCESS;
            if (!util_check_param_range(prbs_info_ptr->op.shared_tx_ptrn.length, 0, 240)) {
                /* TODO confirm pattern */
                CAPI_LOG_ERROR("Invalid pattern length: %d\r\n", prbs_info_ptr->op.shared_tx_ptrn.length);
                return RR_ERROR_WRONG_INPUT_VALUE;
            } 
        } else {
            CAPI_LOG_ERROR("Invalid ptype: %d\r\n", prbs_info_ptr->ptype);
            return RR_ERROR_WRONG_INPUT_VALUE;
        }
    } else {
        CAPI_LOG_ERROR("Invalid core_ip: %d\r\n", phy_info_ptr->core_ip);
        return RR_ERROR_WRONG_CORE_IP_VALUE;
    }

    return RR_SUCCESS;
}

/**
 * @brief      sanity_checker_get_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check get prbs config params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  prbs_info_ptr: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_get_prbs_info(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr, sanity_chk_t chk_type)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;

    if (CHK_USR_INPUT == chk_type) {
        if ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP)    ||
            (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
            (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)   ||
            (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
            return_result = RR_SUCCESS;
        } else {
            CAPI_LOG_ERROR("Invalid core_ip: %d\r\n", phy_info_ptr->core_ip);
            return RR_ERROR_WRONG_CORE_IP_VALUE;
        }
        /*TODO: Revisit with KP4 PRBS*/
        if ((prbs_info_ptr->ptype == CAPI_PRBS_GENERATOR)                  ||
            (prbs_info_ptr->ptype == CAPI_PRBS_MONITOR)                    ||
            (prbs_info_ptr->ptype == CAPI_PRBS_SSPRQ_GENERATOR)            ||
            (prbs_info_ptr->ptype == CAPI_PRBS_SSPRQ_MONITOR)              ||
            (prbs_info_ptr->ptype == CAPI_PRBS_Q_PRBS_13_GENERATOR)        ||
            (prbs_info_ptr->ptype == CAPI_PRBS_Q_PRBS_13_MONITOR)          ||
            (prbs_info_ptr->ptype == CAPI_PRBS_SQUARE_WAVE_GENERATOR)      ||
            (prbs_info_ptr->ptype == CAPI_PRBS_TX_LINEARITY_GENERATOR)     ||
            (prbs_info_ptr->ptype == CAPI_PRBS_SHARED_TX_PATTERN_GENERATOR)) {
            return_result = RR_SUCCESS;
        } else {
            CAPI_LOG_ERROR("Invalid pattern type: %d\r\n", prbs_info_ptr->ptype);
            return RR_ERROR_WRONG_INPUT_VALUE;
        }
    } else if (CHK_FW_OUTPUT == chk_type) {
        return_result = sanity_checker_set_prbs_info(phy_info_ptr, prbs_info_ptr);
    }
    
    return(return_result);

}


/**
 * @brief      sanity_checker_prbs_status_get(capi_phy_info_t *phy_info_ptr, capi_prbs_info_t* prbs_info_ptr)
 * @details    This API is used to sanity check get prbs status params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  prbs_info_ptr: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_prbs_status_get(capi_phy_info_t *phy_info_ptr, capi_prbs_status_t* prbs_status_ptr, sanity_chk_t chk_type)
{
    if (CHK_USR_INPUT == chk_type) {
        if((((prbs_status_ptr->prbs_type == CAPI_PRBS_MONITOR) 
             || (prbs_status_ptr->prbs_type == CAPI_PRBS_SSPRQ_MONITOR) || 
             (prbs_status_ptr->prbs_type == CAPI_PRBS_Q_PRBS_13_MONITOR)) &&
            ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP)    ||
             (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
             (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)   ||
             (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES))) || 
          (((prbs_status_ptr->prbs_type == CAPI_PRBS_FEC_INGRESS_MONITOR) ||
            (prbs_status_ptr->prbs_type == CAPI_PRBS_FEC_INGRESS_GEN_MON) ||
            (prbs_status_ptr->prbs_type == CAPI_PRBS_FEC_EGRESS_MONITOR) ||
            (prbs_status_ptr->prbs_type == CAPI_PRBS_FEC_EGRESS_GEN_MON)) &&
            (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC))) { /*TODO: Revisit when KP4 PRBS is implemented*/
            return RR_SUCCESS;
        } else {
            return RR_ERROR_WRONG_INPUT_VALUE;
        }
    } else if (CHK_FW_OUTPUT == chk_type) {
        if (util_check_param_range(prbs_status_ptr->lock, 0, 1) &&
            util_check_param_range(prbs_status_ptr->lock_loss, 0, 1)) {
            return RR_SUCCESS;
        } else {
            return RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    } else {
        return RR_ERROR;
    }
}

/**
 * @brief      sanity_checker_clear_prbs_status(capi_phy_info_t *phy_info_ptr,  capi_prbs_status_t* prbs_st_ptr)
 * @details    This API is used to sanity check clear prbs status params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  ptype: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_clear_prbs_status(capi_phy_info_t *phy_info_ptr,  capi_prbs_status_t* prbs_st_ptr)
{
    if (((phy_info_ptr->core_ip != CORE_IP_HOST_DSP)    &&
        (phy_info_ptr->core_ip != CORE_IP_HOST_SERDES) &&
        (phy_info_ptr->core_ip != CORE_IP_MEDIA_DSP)   &&
        (phy_info_ptr->core_ip != CORE_IP_MEDIA_SERDES)) || 
        !((prbs_st_ptr->prbs_type == CAPI_PRBS_MONITOR) || (prbs_st_ptr->prbs_type == CAPI_PRBS_Q_PRBS_13_MONITOR) || (prbs_st_ptr->prbs_type == CAPI_PRBS_SSPRQ_MONITOR))
        ) {
        CAPI_LOG_ERROR("Invalid core_ip: %d\r\n", phy_info_ptr->core_ip);
        return RR_ERROR_WRONG_CORE_IP_VALUE;
    }
    return RR_SUCCESS;
}

/**
 * @brief      sanity_checker_set_txpi_override(capi_phy_info_t *phy_info_ptr,  txpi_override_t* txpi_ovrd)
 * @details    This API is used to sanity check txpi override parameters
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  txpi_ovrd   : this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_set_txpi_override(capi_phy_info_t *phy_info_ptr,  txpi_override_t* txpi_ovrd)
{
    if (/*(phy_info_ptr->core_ip != CORE_IP_HOST_DSP)    && */
        (phy_info_ptr->core_ip != CORE_IP_HOST_SERDES) &&
        /*(phy_info_ptr->core_ip != CORE_IP_MEDIA_DSP)   && */
        (phy_info_ptr->core_ip != CORE_IP_MEDIA_SERDES)) {
        CAPI_LOG_ERROR("Invalid core_ip: %d\r\n", phy_info_ptr->core_ip);
        return RR_ERROR_WRONG_CORE_IP_VALUE;
    }

    if (!util_check_param_range(txpi_ovrd->enable, 0, 1)) {
        return RR_ERROR_WRONG_INPUT_VALUE;
    }

    return RR_SUCCESS;
}


/**
 * @brief      sanity_checker_prbs_inject_error(capi_phy_info_t *phy_info_ptr, capi_prbs_err_inject_t* prbs_err_inj_ptr)
 * @details    This API is used to sanity check prbs inject error params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  prbs_err_inj_ptr: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_prbs_inject_error(capi_phy_info_t *phy_info_ptr, capi_prbs_err_inject_t* prbs_err_inj_ptr)
{

    if ((phy_info_ptr->core_ip != CORE_IP_HOST_DSP)    &&
        (phy_info_ptr->core_ip != CORE_IP_HOST_SERDES) &&
        (phy_info_ptr->core_ip != CORE_IP_MEDIA_DSP)   &&
        (phy_info_ptr->core_ip != CORE_IP_MEDIA_SERDES)) {
        CAPI_LOG_ERROR("Invalid core ip: %d\r\n", phy_info_ptr->core_ip);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }
    if(prbs_err_inj_ptr->inject_err_num == 0) {
         CAPI_LOG_ERROR("Invalid error number: %d\r\n",prbs_err_inj_ptr->inject_err_num);
         return RR_ERROR_WRONG_INPUT_VALUE;
    }

    return RR_SUCCESS;
}

/**
 * @brief      sanity_checker_get_lane_info(capi_phy_info_t*  phy_info_ptr,
 *                                          capi_lane_info_t* lane_info_ptr,
 *                                          sanity_chk_t      chk_type)
 * @details    This API is used to sanity check lane status info params when doing a get
 *
 * @param[in]  phy_info_ptr  : reference to the phy information object 
 * @param[in]  lane_info_ptr : reference to the lane information object 
 * @param[in]  sanity_chk_t  : chk_type - 0: Check user input, 1: Chec kFW output
 * 
 * @return     returns the performance result of the called methode/function
 */
static return_result_t sanity_checker_get_lane_info(capi_phy_info_t*  phy_info_ptr,
                                                    capi_lane_info_t* lane_info_ptr,
                                                    sanity_chk_t      chk_type)
{
    if (!lane_info_ptr->param.content) return(RR_ERROR_NOT_INITIALIZED);

    if ((phy_info_ptr->core_ip == CORE_IP_CW) ||
        (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC)) {
        if ((lane_info_ptr->param.is.lane_status)            ||
            (lane_info_ptr->param.is.lnktrn_status)          ||
            (lane_info_ptr->param.is.diag_status)            ||
            (lane_info_ptr->param.is.sticky_loss_electrical) ||
            (lane_info_ptr->param.is.sticky_loss_optical)    ||
            (lane_info_ptr->param.is.sticky_loss_link)) {
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
        }

        if (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC) {
            if (lane_info_ptr->param.is.cw_status) return(RR_ERROR_WRONG_CORE_IP_VALUE);
        } else if (!lane_info_ptr->param.is.cw_status) {
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
        }
    } else if (phy_info_ptr->core_ip == CORE_IP_ALL) {
        return(RR_ERROR_WRONG_CORE_IP_VALUE);
    } else {
        if (lane_info_ptr->param.is.cw_status) {
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
        }
    }

    if (CHK_FW_OUTPUT == chk_type) {
        ;
        /*We might need to do some Sanity checking for the FW returned value in future*/
    }

    return(RR_SUCCESS);
}

/**
 * @brief      sanity_checker_get_polarity_info(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check polarity info params when doing a get
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr     : this parameter
 * @param[in]  polarity_info_ptr: this parameter
 * @param[in]  sanity_chk_t     : chk_type - 0: Check user input, 1: Check FW output
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_get_polarity_info (capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr, sanity_chk_t chk_type)
{
    return_result_t return_result = RR_ERROR_WRONG_INPUT_VALUE;

    if (CHK_USR_INPUT == chk_type) {
        /* Sanity check the input by the user */
        if (((phy_info_ptr->core_ip == CORE_IP_HOST_DSP)    ||
             (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
             (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)   ||
             (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) &&
            ((DIR_INGRESS == polarity_info_ptr->direction) || (DIR_EGRESS == polarity_info_ptr->direction))) {
            return_result = RR_SUCCESS;
        } /* If not LW or BH and not ingress of egress, return wrong input */
    } else if (CHK_FW_OUTPUT == chk_type) {
        /* Sanity check the output by the FW */
        if ((POLARITY_SWAP_CURRENT_SETTING == polarity_info_ptr->action)   ||
            (POLARITY_INVERT_DEFAULT_SETTING == polarity_info_ptr->action) ||
            (POLARITY_DEFAULT_SETTING == polarity_info_ptr->action)) {
            return_result = RR_SUCCESS;
        } else {
            /* If FW doesn't return swap_current, invert_default or default, return wrong output */
            return_result = RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    } else {
        return_result = RR_ERROR; /* What is the correct error type in this case? */
    }
    return return_result;
}

/**
 * @brief      sanity_checker_set_polarity_info(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
 * @details    This API is used to sanity check polarity info params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr     : this parameter
 * @param[in]  polarity_info_ptr: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_set_polarity_info(capi_phy_info_t* phy_info_ptr, capi_polarity_info_t* polarity_info_ptr)
{
    return_result_t return_result = RR_SUCCESS;

    if ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP)    ||
        (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
        (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)   ||
        (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
        if ((polarity_info_ptr->direction == DIR_INGRESS) || (polarity_info_ptr->direction == DIR_EGRESS)) {
            if ((polarity_info_ptr->action == POLARITY_DEFAULT_SETTING) || (polarity_info_ptr->action == POLARITY_INVERT_DEFAULT_SETTING) || (polarity_info_ptr->action == POLARITY_SWAP_CURRENT_SETTING)){
                return_result = RR_SUCCESS;
            } else {
                CAPI_LOG_ERROR("Invalid polarity action value: %d\r\n", polarity_info_ptr->action);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else { /* DIR_BOTH and others are not supported */
            CAPI_LOG_ERROR("Invalid direction value: %d\r\n", polarity_info_ptr->direction);
            return_result = RR_ERROR_WRONG_INPUT_VALUE;
        }
    } else {
        CAPI_LOG_ERROR("Invalid core ip value: %d\r\n", phy_info_ptr->core_ip);
        return_result = RR_ERROR_WRONG_CORE_IP_VALUE;
    }

    return return_result;
}

/**
 * @brief      sanity_checker_get_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
 * @details    This API is used to sanity check lane control info get parameters
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr      : this parameter
 * @param[in]  lane_ctrl_info_ptr: this parameter
 * @param[in]  sanity_chk_t      : chk_type - 0: Check user input, 1: Chec kFW output
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_get_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr, sanity_chk_t chk_type)
{
    return_result_t return_result = RR_SUCCESS;

    if (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES || phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES ||
        phy_info_ptr->core_ip == CORE_IP_HOST_DSP || phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
        if (CHK_USR_INPUT == chk_type)  {
            if ((lane_ctrl_info_ptr->param.is.rx_datapath_power)  ||
                (lane_ctrl_info_ptr->param.is.tx_datapath_power)  ||
                (lane_ctrl_info_ptr->param.is.rx_lane_cfg_reset)  ||
                (lane_ctrl_info_ptr->param.is.tx_lane_cfg_reset)  ||
                (lane_ctrl_info_ptr->param.is.rx_squelch)         ||
                (lane_ctrl_info_ptr->param.is.tx_squelch)         ||
                (lane_ctrl_info_ptr->param.is.tx_electric_idle)   ||
                (lane_ctrl_info_ptr->param.is.rx_suspend_resume)  ||
                (lane_ctrl_info_ptr->param.is.tx_suspend_resume)  ||
                (lane_ctrl_info_ptr->param.is.rx_afe_power)       ||
                (lane_ctrl_info_ptr->param.is.tx_afe_power)       ||
                (lane_ctrl_info_ptr->param.is.ignore_fault))       
            {
                return_result = RR_SUCCESS;
            }
            else {
                CAPI_LOG_ERROR("Unsupported parameters\r\n");
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        } else if (CHK_FW_OUTPUT == chk_type) {
            if ((util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.rx_datapath_power, 0, 1))  || /* Can't return TOGGLE */
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.tx_datapath_power, 0, 1))  || /* Can't return TOGGLE */
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.rx_lane_cfg_reset, 0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.tx_lane_cfg_reset, 0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.rx_squelch,        0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.tx_squelch,        0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.tx_electric_idle,  0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.rx_suspend_resume, 0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.tx_suspend_resume, 0, 1))  ||
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.rx_afe_power,      0, 1))  || /* Can't return TOGGLE */
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.tx_afe_power,      0, 1))  || /* Can't return TOGGLE */
                (util_check_param_range(lane_ctrl_info_ptr->cmd_value.is.ignore_fault,      0, 1)))       
            {
                return_result = RR_SUCCESS;
            }
            else {
                CAPI_LOG_ERROR("Unsupported parameters\r\n");
                return_result = RR_ERROR_WRONG_OUTPUT_VALUE;
            }
        } else {
            return_result = RR_ERROR; /* What is the correct error type in this case? */
        }
    } else {
        CAPI_LOG_ERROR("Core ip is incorrect: %d\r\n", phy_info_ptr->core_ip);
        return_result = RR_ERROR_WRONG_CORE_IP_VALUE;
    }
    return return_result;
}

/**
 * @brief      sanity_checker_set_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
 * @details    This API is used to sanity check lane control info parameters
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr         : this parameter
 * @param[in]  lane_ctrl_info_t: this parameter
 *
 * @return     returns the performance result of the called methode/function
 */
return_result_t sanity_checker_set_lane_ctrl_info(capi_phy_info_t* phy_info_ptr, capi_lane_ctrl_info_t* lane_ctrl_info_ptr)
{
    return_result_t return_result = RR_SUCCESS;

    if (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES || phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES ||
        phy_info_ptr->core_ip == CORE_IP_HOST_DSP || phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
            if ((lane_ctrl_info_ptr->param.is.ignore_fault)       ||
                (lane_ctrl_info_ptr->param.is.rx_datapath_power)  ||
                (lane_ctrl_info_ptr->param.is.rx_lane_cfg_reset)  ||
                (lane_ctrl_info_ptr->param.is.rx_squelch)         ||
                (lane_ctrl_info_ptr->param.is.rx_suspend_resume)  ||
                (lane_ctrl_info_ptr->param.is.rx_afe_power)       ||
                (lane_ctrl_info_ptr->param.is.tx_datapath_power)  ||
                (lane_ctrl_info_ptr->param.is.tx_lane_cfg_reset)  ||
                (lane_ctrl_info_ptr->param.is.tx_squelch)         ||
                (lane_ctrl_info_ptr->param.is.tx_electric_idle)   ||
                (lane_ctrl_info_ptr->param.is.tx_suspend_resume)  ||
                (lane_ctrl_info_ptr->param.is.tx_afe_power))
            {
                return_result = RR_SUCCESS;
            }
            else {
                CAPI_LOG_ERROR("Unsupported parameters\r\n");
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
    } 
    else {
        CAPI_LOG_ERROR("Core ip is incorrect: %d\r\n", phy_info_ptr->core_ip);
        return_result = RR_ERROR_WRONG_CORE_IP_VALUE;
    }
    return return_result;
}

/**
 * @brief      sanity_checker_lane_tx_info(capi_phy_info_t* phy_info_ptr,
 *                                         lane_tx_info_t*  lane_tx_info_ptr,
 *                                         sanity_chk_t     chk_type)
 * @details    This API is used to sanity check lane Tx information params
 *
 * @param[in]  phy_info_ptr: reference to the phy information object
 * @param[in]  lane_tx_info_ptr : reference to the lane Tx information object
 * @param[in]  chk_type    : specifies user input of FW output check type
 *
 * @return     returns the performance result of the called methode/function
 */
static return_result_t sanity_checker_lane_tx_info(capi_phy_info_t* phy_info_ptr,
                                                   lane_tx_info_t*  lane_tx_info_ptr,
                                                   sanity_chk_t     chk_type)
{
    return_result_t return_result = RR_SUCCESS;
    uint8_t idx, tap_max;

    if (!lane_tx_info_ptr->param.content) return(RR_ERROR_NOT_INITIALIZED);

    if ((phy_info_ptr->core_ip == CORE_IP_ALL) ||
        (phy_info_ptr->core_ip == CORE_IP_CW)  ||
        (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC)) {
        CAPI_LOG_ERROR("Wrong Core IP : %d  \r\n", RR_ERROR_WRONG_CORE_IP_VALUE);
        return(RR_ERROR_WRONG_CORE_IP_VALUE);
    }

    if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
        (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
        if (lane_tx_info_ptr->param.is.dsp_level_shift ||
            lane_tx_info_ptr->param.is.dsp_graycode    ||
            lane_tx_info_ptr->param.is.dsp_high_swing  ||
            lane_tx_info_ptr->param.is.dsp_precode) {
            CAPI_LOG_ERROR("Wrong Core IP : %d  \r\n", RR_ERROR_WRONG_CORE_IP_VALUE);
            return(RR_ERROR_WRONG_CORE_IP_VALUE);
        }
    }

    if (lane_tx_info_ptr->param.is.txfir) {
        int16_t txfir_sum =0;
        int16_t MAX_TXFIR_SUM = 168;

        if(chk_type == CHK_USR_INPUT) {
            if ((phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP || phy_info_ptr->core_ip == CORE_IP_HOST_DSP) && 
                lane_tx_info_ptr->value.txfir.numb_of_taps == TXFIR_TAPS_7TAP) {
                // do not check  7 tap txfir for now 
                tap_max = 7;
                txfir_sum = 0;
                for(idx=0; idx<tap_max; idx++)
                    txfir_sum = abs(lane_tx_info_ptr->value.txfir.tap[idx]);
                
                if(txfir_sum > MAX_TXFIR_SUM)
                {
                    CAPI_LOG_ERROR("Wrong TXFIR > %d, tap[0]:%d, tap[1]:%d, tap[2]:%d, tap[3]:%d, tap[4]:%d, tap[5]:%d tap[6]:%d\r\n", MAX_TXFIR_SUM,
                    lane_tx_info_ptr->value.txfir.tap[0], lane_tx_info_ptr->value.txfir.tap[1], lane_tx_info_ptr->value.txfir.tap[2], lane_tx_info_ptr->value.txfir.tap[3], lane_tx_info_ptr->value.txfir.tap[4], lane_tx_info_ptr->value.txfir.tap[5], lane_tx_info_ptr->value.txfir.tap[6]);
                    return_result = RR_ERROR_WRONG_INPUT_VALUE;
                }
            }
            else {
                if(((phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES || phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) && 
                    !util_check_param_range(lane_tx_info_ptr->value.txfir.numb_of_taps, TXFIR_TAPS_NRZ_LP_3TAP, TXFIR_TAPS_PAM4_6TAP)) ||
                    ((phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP || phy_info_ptr->core_ip == CORE_IP_HOST_DSP) &&
                     lane_tx_info_ptr->value.txfir.numb_of_taps==TXFIR_TAPS_7TAP))
                     return RR_ERROR_WRONG_INPUT_VALUE;
                if((lane_tx_info_ptr->value.txfir.numb_of_taps == TXFIR_TAPS_NRZ_LP_3TAP) || (lane_tx_info_ptr->value.txfir.numb_of_taps == TXFIR_TAPS_NRZ_6TAP)) {
                    MAX_TXFIR_SUM = 127;
                }
                if(lane_tx_info_ptr->value.txfir.numb_of_taps == TXFIR_TAPS_12TAP){
                    tap_max = 12;
                }else
                    tap_max = 6;
                for(idx=0; idx<tap_max; idx++)
                    txfir_sum += abs(lane_tx_info_ptr->value.txfir.tap[idx]);
                if(txfir_sum > MAX_TXFIR_SUM ||
                    (lane_tx_info_ptr->value.txfir.numb_of_taps == TXFIR_TAPS_12TAP &&
                     (!util_check_param_range(lane_tx_info_ptr->value.txfir.tap[6], -64, 63) || 
                     !util_check_param_range(lane_tx_info_ptr->value.txfir.tap[7], -64, 63) || 
                     !util_check_param_range(lane_tx_info_ptr->value.txfir.tap[8], -64, 63) || 
                     !util_check_param_range(lane_tx_info_ptr->value.txfir.tap[9], -64, 63) ||    
                     !util_check_param_range(lane_tx_info_ptr->value.txfir.tap[10], -64, 63) || 
                     !util_check_param_range(lane_tx_info_ptr->value.txfir.tap[11], -64, 63) ))) 
                {
                    CAPI_LOG_ERROR("Wrong TXFIR > %d, tap[0]:%d, tap[1]:%d, tap[2]:%d, tap[3]:%d, tap[4]:%d, tap[5]:%d\r\n", MAX_TXFIR_SUM,
                    lane_tx_info_ptr->value.txfir.tap[0], lane_tx_info_ptr->value.txfir.tap[1], lane_tx_info_ptr->value.txfir.tap[2], lane_tx_info_ptr->value.txfir.tap[3], lane_tx_info_ptr->value.txfir.tap[4], lane_tx_info_ptr->value.txfir.tap[5]);
                    if(lane_tx_info_ptr->value.txfir.numb_of_taps == TXFIR_TAPS_12TAP)
                        CAPI_LOG_ERROR("Wrong TXFIR > %d, tap[6]:%d, tap[7]:%d, tap[8]:%d, tap[9]:%d, tap[10]:%d, tap[11]:%d\r\n", MAX_TXFIR_SUM,
                        lane_tx_info_ptr->value.txfir.tap[6], lane_tx_info_ptr->value.txfir.tap[7], lane_tx_info_ptr->value.txfir.tap[8], lane_tx_info_ptr->value.txfir.tap[9], lane_tx_info_ptr->value.txfir.tap[10], lane_tx_info_ptr->value.txfir.tap[11]);

                    return_result = RR_ERROR_WRONG_INPUT_VALUE;
                }
            }
        }
        /* TODO: Add output */
    }

    if (lane_tx_info_ptr->param.is.symbol_swap) {
        if (!util_check_param_range(lane_tx_info_ptr->value.symbol_swap, 0, 1)) {
            CAPI_LOG_ERROR("Wrong parameter symbol_swap : %d \r\n",
                           lane_tx_info_ptr->value.symbol_swap);
            return_result = (chk_type == CHK_USR_INPUT) ? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }

    if (lane_tx_info_ptr->param.is.dsp_graycode) {
        if (!util_check_param_range(lane_tx_info_ptr->value.dsp_graycode, 0, 1)) {
            CAPI_LOG_ERROR("Wrong parameter dsp_graycode : %d \r\n",
                           lane_tx_info_ptr->value.dsp_graycode);
            return_result = (chk_type == CHK_USR_INPUT) ? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }

    if (lane_tx_info_ptr->param.is.dsp_high_swing) {
        if (!util_check_param_range(lane_tx_info_ptr->value.dsp_high_swing, 0, 1)) {
            CAPI_LOG_ERROR("Wrong parameter dsp_high_swing : %d \r\n",
                           lane_tx_info_ptr->value.dsp_high_swing);
            return_result = (chk_type == CHK_USR_INPUT) ? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }

    if (lane_tx_info_ptr->param.is.dsp_precode) {
        if (!util_check_param_range(lane_tx_info_ptr->value.dsp_precode,
                                    CAPI_LW_INDEP_TX_PRECODE_DEFAULT,
                                    CAPI_LW_INDEP_TX_PRECODE_ON)) {
            CAPI_LOG_ERROR("Wrong parameter dsp_precode : %d \r\n",
                           lane_tx_info_ptr->value.dsp_precode);
            return_result = (chk_type == CHK_USR_INPUT) ? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE;
        }
    }


    return(return_result);
}

static unsigned int intf_count_set_bits(uint32_t n)
{
    unsigned int count = 0;
    while (n)
    {
      n &= (n-1) ;
      count++;
    }
    return count;
}

static bool validate_ports(capi_phy_info_t* phy_info_ptr, capi_config_info_t* config_info_ptr, cw_chip_mode_t port_type)
{
    bool port_valid = false;
    int i;
    uint8_t port_line_mask, port_host_mask;
    uint8_t mode_table_index = port_type -1;
    /* Total no.of ports in this mode */
    int total_ports_in_mode = intf_count_set_bits(cw_cmode[mode_table_index].cw_chip_port_mask);
    int port_count = total_ports_in_mode;
    uint8_t total_line_mask =0, total_host_mask=0;
    /* If total no. of ports are between 1 and max ports else invalid port */
    if( total_ports_in_mode >= 1 && total_ports_in_mode <= CAPI_MAX_PORTS ){
        /* Iterate through the port slot for this mode entry in the mode table */
        for(i=0; i < CAPI_MAX_PORTS;i++){
            /* Extract the port from the user input */
            port_line_mask = cw_cmode[mode_table_index].cw_chip_lw_lane_mask[i] & config_info_ptr->line_lane.lane_mask;
            port_host_mask = cw_cmode[mode_table_index].cw_chip_bh_lane_mask[i] & config_info_ptr->host_lane.lane_mask;

            /* If this slot contains the port mask else go to the next one */
            if(!(cw_cmode[mode_table_index].cw_chip_lw_lane_mask[i] == 0 &&
                 cw_cmode[mode_table_index].cw_chip_bh_lane_mask[i] == 0)){

                /* user config wants to skip this port */
                if(!(port_line_mask == 0 && port_host_mask == 0)){
                    /* If the user input contains this port then it is valid else not valid */
                    if( port_host_mask == cw_cmode[mode_table_index].cw_chip_bh_lane_mask[i] &&
                        port_line_mask == cw_cmode[mode_table_index].cw_chip_lw_lane_mask[i]){
                        total_line_mask |= port_line_mask;
                        total_host_mask |= port_host_mask;
                        port_valid = true;
                       } else { /* encountered a bad port so no point to continue */
                         port_valid = false;
                         break;
                    }
                }
                /* Decrement the port count here */
                port_count--;
                /* Are we done going over all the ports */
                if(!port_count){
                    break;
                }
            }
        }
    }
    if(port_valid){
        /* The user mask is not the same as the sum total of all the port masks for this mode */
       if( total_line_mask != config_info_ptr->line_lane.lane_mask ||
          total_host_mask != config_info_ptr->host_lane.lane_mask ){
          port_valid = false;
        }
    }
    (void)phy_info_ptr;
    return port_valid;
}

/**
 * @brief      sanity_checker_get_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check get config info params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  config_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_get_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type)
{
    uint8_t port_idx;
    cw_chip_mode_t port_type = CHIP_MODES_NONE;

    /* Validate phy info and config parameter*/
    /* Do not check fw output since fw will zero out user input per request */
    if (chk_type == CHK_USR_INPUT) {
        if (!(config_info_ptr->host_lane.lane_mask || config_info_ptr->line_lane.lane_mask)) {
            CAPI_LOG_ERROR("Invalid lane mask. Host lane mask %x, Line lane mask %x", config_info_ptr->host_lane.lane_mask, config_info_ptr->line_lane.lane_mask);
            return RR_ERROR_WRONG_INPUT_VALUE;
        }
    } else {
        if (CAPI_PORT_CONFIG_STATUS_SUCCESS == config_info_ptr->status) {
            for (port_idx=0; port_idx < port_total_entries; port_idx++)
            {
                if ((ports[port_idx].func_mode         == config_info_ptr->func_mode) &&
                    (ports[port_idx].lw_cfg.modulation == config_info_ptr->line_lane.modulation) &&
                    (ports[port_idx].bh_cfg.modulation == config_info_ptr->host_lane.modulation) &&
                    (ports[port_idx].lw_cfg.baud_rate  == config_info_ptr->lw_br) &&
                    (ports[port_idx].bh_cfg.baud_rate  == config_info_ptr->bh_br) &&
                    validate_ports(phy_info_ptr, config_info_ptr, ports[port_idx].port_type))

                {
                   port_type = ports[port_idx].port_type;
                   break;
                }
            }
            if (CHIP_MODES_NONE == port_type) {
                CAPI_LOG_ERROR("Wrong configure info for the mode \n", config_info_ptr->ref_clk);
                return RR_ERROR;
            }
        }
    }
    return RR_SUCCESS;
}

/**
 * @brief      sanity_checker_set_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check set config info params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  config_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_set_config_info(capi_phy_info_t *phy_info_ptr, capi_config_info_t* config_info_ptr, sanity_chk_t chk_type)
{
    uint8_t port_idx;
    cw_chip_mode_t port_type = CHIP_MODES_NONE;

    /*validate phy info and config parameter*/
    if (!(config_info_ptr->host_lane.lane_mask || config_info_ptr->line_lane.lane_mask)) {
        CAPI_LOG_ERROR("Invalid lane mask. Host lane mask %x, Line lane mask %x", config_info_ptr->host_lane.lane_mask, config_info_ptr->line_lane.lane_mask);
        return RR_ERROR_WRONG_INPUT_VALUE;
    }

    /* only do this for set config */
    if (config_info_ptr->ref_clk != CAPI_REF_CLK_FRQ_156_25_MHZ_ETHERNET)
    {
        CAPI_LOG_ERROR("Wrong input ref clock %d \n", config_info_ptr->ref_clk);
        return(RR_ERROR_WRONG_INPUT_VALUE);
    }

    for (port_idx=0; port_idx < port_total_entries; port_idx++)
    {
        if ((ports[port_idx].func_mode         == config_info_ptr->func_mode) &&
            (ports[port_idx].lw_cfg.modulation == config_info_ptr->line_lane.modulation) &&
            (ports[port_idx].bh_cfg.modulation == config_info_ptr->host_lane.modulation) &&
            (ports[port_idx].lw_cfg.baud_rate  == config_info_ptr->lw_br) &&
            (ports[port_idx].bh_cfg.baud_rate  == config_info_ptr->bh_br) &&
            validate_ports(phy_info_ptr, config_info_ptr, ports[port_idx].port_type))
        {
           port_type = ports[port_idx].port_type;
           break;
        }
    }

    /* do not validate config info input for small CAPI to reduce code size */
    if (port_type == CHIP_MODES_NONE){
       CAPI_LOG_ERROR("Wrong configure info for the mode \n", config_info_ptr->ref_clk);
       return RR_ERROR_WRONG_INPUT_VALUE;
    }

    return RR_SUCCESS;
}

/**
 * @brief      sanity_checker_set_lw_rclk_info(capi_phy_info_t *phy_info_ptr, capi_rclk_info_t* rclk_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check set lw rclk info params
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  rclk_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
static return_result_t sanity_checker_set_lw_rclk_info(capi_phy_info_t *phy_info_ptr, capi_rclk_info_t* rclk_info_ptr, sanity_chk_t chk_type)
{
    /*validate phy info and rclk_info parameter*/
    if (chk_type == CHK_USR_INPUT) {
        /* only brings out Lane 3 Recovered Clock */
        if (phy_info_ptr->lane_mask != 0x8
            || rclk_info_ptr->rclk_div_ratio > CAPI_RCLK_DIV_RATIO_128
            || rclk_info_ptr->rclk_div_ratio < CAPI_RCLK_DIV_RATIO_1
            || (rclk_info_ptr->rclk_type != CAPI_RCLK_80T && rclk_info_ptr->rclk_type != CAPI_RCLK_32T))
            return (RR_ERROR_WRONG_INPUT_VALUE);
    }
    return RR_SUCCESS;
}

/**
 * @brief      sanity_checker_lnktrn_info(capi_phy_info_t*    phy_info_ptr,
 *                                        capi_lnktrn_info_t* lnktrn_info_ptr,
 *                                        uint8_t             set_get_request,
 *                                        sanity_chk_t        chk_type)
 * @details    This API is used to sanity check set lnktrn info params
 *
 * @param[in]  phy_info_ptr: reference to the phy information objecct
 * @param[in]  lnktrn_info_ptr: referencce to the link training info object
 * @param[in]  set_get_request: set or get capi request
 * @param[in]  chk_type: sanity check type, input or output
 *
 * @return     returns the performance result of the called method/function
 */
static return_result_t sanity_checker_lnktrn_info(capi_phy_info_t*    phy_info_ptr,
                                                  capi_lnktrn_info_t* lnktrn_info_ptr,
                                                  uint8_t             set_get_request,
                                                  sanity_chk_t        chk_type)
{
    return_result_t return_result = RR_SUCCESS;

    if (!lnktrn_info_ptr->param.content) return(RR_ERROR_NOT_INITIALIZED);

    if ((phy_info_ptr->core_ip == CORE_IP_ALL) ||
        (phy_info_ptr->core_ip == CORE_IP_CW)  ||
        (phy_info_ptr->core_ip == CORE_IP_KP4_KR4_FEC_DEC)) {
        CAPI_LOG_ERROR("Wrong Core IP : %d  \r\n", RR_ERROR_WRONG_CORE_IP_VALUE);
        return(RR_ERROR_WRONG_CORE_IP_VALUE);
    }
    
    if ((CHK_USR_INPUT == chk_type) && (set_get_request == GET_CONFIG)) {
        /*  */
    } else {
        if ((phy_info_ptr->core_ip == CORE_IP_HOST_SERDES) ||
            (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES)) {
            if (lnktrn_info_ptr->param.is.dsp_linktrn_timer                ||
                lnktrn_info_ptr->param.is.dsp_disable_lt_rxflt_restart_tx  ||
                lnktrn_info_ptr->param.is.dsp_disable_lt_rxlos_restart_tx  ||               
                lnktrn_info_ptr->param.is.dsp_cl72_auto_pol_en             ||
                lnktrn_info_ptr->param.is.dsp_cl72_rest_to                 ||
                lnktrn_info_ptr->param.is.dsp_enable_cl72_cl93_preset_req  ||               
                lnktrn_info_ptr->param.is.dsp_enable_802_3cd_preset_req    ||
                lnktrn_info_ptr->param.is.dsp_lnktrn_cfg) {
                CAPI_LOG_ERROR("Wrong Core IP : %d  \r\n", RR_ERROR_WRONG_CORE_IP_VALUE);
                return(RR_ERROR_WRONG_CORE_IP_VALUE);
            }
        }

        if ((phy_info_ptr->core_ip == CORE_IP_HOST_DSP) ||
            (phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)) {
            if (lnktrn_info_ptr->param.is.serdes_lnktrn_restart_timeout ||
                lnktrn_info_ptr->param.is.serdes_lnktrn_type) {
                CAPI_LOG_ERROR("Wrong Core IP : %d  \r\n", RR_ERROR_WRONG_CORE_IP_VALUE);
                return(RR_ERROR_WRONG_CORE_IP_VALUE);
            }
        }
        
        if (lnktrn_info_ptr->param.is.opposite_cdr_first) {
            if (!util_check_param_range(lnktrn_info_ptr->opposite_cdr_first, 0, 1)) {
                CAPI_LOG_ERROR("Invalid opposite_cdr_first value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->opposite_cdr_first);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }

        if (lnktrn_info_ptr->param.is.lnktrn_en) {
            if (!util_check_param_range(lnktrn_info_ptr->lnktrn_en, 0, 1)) {
                CAPI_LOG_ERROR("Invalid serdes_lnktrn_en value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->lnktrn_en);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.serdes_lnktrn_restart_timeout) {
            if (!util_check_param_range(lnktrn_info_ptr->value.serdes.lnktrn_restart_timeout, 0, 1)) {
                CAPI_LOG_ERROR("Invalid serdes_lnktrn_restart_timeout value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.serdes.lnktrn_restart_timeout);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.serdes_lnktrn_type) {
            if ((set_get_request == SET_CONFIG) ||
                !util_check_param_range(lnktrn_info_ptr->value.serdes.lnktrn_type,
                                        LNKTRN_CL72,
                                        LNKTRN_802_3CD)) {
                CAPI_LOG_ERROR("Invalid serdes_lnktrn_type value: %d    set_get_request: %d \r\n",
                               lnktrn_info_ptr->value.serdes.lnktrn_type,
                               set_get_request);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }

#if CAPI_DSP_PLANNED
        if (lnktrn_info_ptr->param.is.dsp_linktrn_timer) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.linktrn_timer.disable_lt_timeout, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_linktrn_timer value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.linktrn_timer.disable_lt_timeout);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_restart) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.restart, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_restart value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.restart);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_disable_lt_rxflt_restart_tx) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.disable_lt_rxflt_restart_tx, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_disable_lt_rxflt_restart_tx value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.disable_lt_rxflt_restart_tx);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_disable_lt_rxlos_restart_tx) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.disable_lt_rxlos_restart_tx, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_disable_lt_rxlos_restart_tx value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.disable_lt_rxlos_restart_tx);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_cl72_auto_pol_en) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.cl72_auto_pol_en, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_cl72_auto_pol_en value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.cl72_auto_pol_en);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_cl72_rest_to) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.cl72_rest_to, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_cl72_rest_to value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.cl72_rest_to);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_enable_cl72_cl93_preset_req) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.enable_cl72_cl93_preset_req, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_enable_cl72_cl93_preset_req value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.enable_cl72_cl93_preset_req);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_enable_802_3cd_preset_req) {
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.enable_802_3cd_preset_req, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_enable_802_3cd_preset_req value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.enable_802_3cd_preset_req);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
        
        if (lnktrn_info_ptr->param.is.dsp_lnktrn_cfg) {
            /*
            if (!util_check_param_range(lnktrn_info_ptr->value.dsp.lnktrn_cfg.pam_802_3, 0, 1)) {
                CAPI_LOG_ERROR("Invalid dsp_lnktrn_cfg value: %d. Should be 0 or 1.\r\n",
                               lnktrn_info_ptr->value.dsp.lnktrn_cfg);
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
            */
        }
#endif
    }
    return(return_result);
}

/**
 * @brief       sanity_checker_mpi_config_info(capi_phy_info_t *phy_info_ptr, dsp_mpi_cfg_info_t* mpi_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check MPI config info params
 *
 * @param[in]  phy_info_ptr: this parameter
 * @param[in]  mpi_info_ptr: this parameter
 * @param[in]  chk_type: this parameter
 *
 * @return     returns the performance result of the called method/function
 */
return_result_t sanity_checker_mpi_config_info(capi_phy_info_t *phy_info_ptr, dsp_mpi_cfg_info_t* mpi_info_ptr, sanity_chk_t chk_type)
{
    if(phy_info_ptr->core_ip != CORE_IP_MEDIA_DSP ||
        util_check_param_range(mpi_info_ptr->mpi_enable, 0, 1) == FALSE ||
        util_check_param_range(mpi_info_ptr->mpi_interval, 0, 15) == FALSE ||
        (mpi_info_ptr->ratio_threshold!=0 && (mpi_info_ptr->ratio_threshold<0)))
        return ((chk_type== CHK_USR_INPUT)? RR_ERROR_WRONG_INPUT_VALUE : RR_ERROR_WRONG_OUTPUT_VALUE);
    return RR_SUCCESS;
}


/**
 * @brief       host_diag_cmd_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_command_info_t *cmd_info_ptr, sanity_chk_t chk_type)
 * @details       This function is used to check the capi cmd sanity
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]
 *
 * @return       returns the performance result of the called methode/function
 */
return_result_t host_diag_cmd_sanity_checker(capi_phy_info_t *phy_info_ptr, 
                                             capi_command_info_t *cmd_info_ptr, 
                                             sanity_chk_t chk_type)
{
    return_result_t return_result = RR_SUCCESS;

    if (phy_info_ptr == NULL || cmd_info_ptr == NULL)
        return RR_ERROR_WRONG_INPUT_VALUE;

    switch(cmd_info_ptr->command_id) {

        
        case COMMAND_ID_SET_LOW_POWER_MODE:
            break;
        case COMMAND_ID_GET_LOW_POWER_MODE:
            break;
        case COMMAND_ID_SET_POLARITY:
            CAPI_LOG_POLARITY_INFO(&cmd_info_ptr->type.polarity_info);
            return_result = sanity_checker_set_polarity_info(phy_info_ptr,
                                                             &cmd_info_ptr->type.polarity_info);
            break;

        case COMMAND_ID_GET_POLARITY:
            CAPI_LOG_POLARITY_INFO(&cmd_info_ptr->type.polarity_info);
            return_result = sanity_checker_get_polarity_info(phy_info_ptr,
                                                             &cmd_info_ptr->type.polarity_info, chk_type);
            break;

        case COMMAND_ID_SET_LANE_CONFIG_INFO:
            if (cmd_info_ptr->type.lane_config_info.lane_config_type == LANE_CONFIG_TYPE_LANE_RX_INFO) {
                CAPI_LOG_RX_INFO(&cmd_info_ptr->type.lane_config_info.type.lane_rx_info);
                return_result = sanity_checker_lane_rx_info(phy_info_ptr,
                                                            &cmd_info_ptr->type.lane_config_info.type.lane_rx_info,
                                                            SET_CONFIG,
                                                            chk_type);
            } else if (cmd_info_ptr->type.lane_config_info.lane_config_type == LANE_CONFIG_TYPE_LANE_TX_INFO) {
                CAPI_LOG_TX_INFO(&cmd_info_ptr->type.lane_config_info.type.lane_tx_info);
                return_result = sanity_checker_lane_tx_info(phy_info_ptr,
                                                            &cmd_info_ptr->type.lane_config_info.type.lane_tx_info,
                                                            chk_type);
            } else if (cmd_info_ptr->type.lane_config_info.lane_config_type == LANE_CONFIG_TYPE_LANE_LINK_TRAINING_INFO) {
                CAPI_LOG_LNKTRN_INFO(&cmd_info_ptr->type.lane_config_info.type.lane_lnktrn_info);
                return_result = sanity_checker_lnktrn_info(phy_info_ptr,
                                                           &cmd_info_ptr->type.lane_config_info.type.lane_lnktrn_info,
                                                           SET_CONFIG,
                                                           chk_type);
            } else {
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
            break;

        case COMMAND_ID_GET_LANE_CONFIG_INFO:
            if (cmd_info_ptr->type.lane_config_info.lane_config_type == LANE_CONFIG_TYPE_LANE_RX_INFO) {
                CAPI_LOG_RX_INFO(&cmd_info_ptr->type.lane_config_info.type.lane_rx_info);
                return_result = sanity_checker_lane_rx_info(phy_info_ptr,
                                                            &cmd_info_ptr->type.lane_config_info.type.lane_rx_info,
                                                            GET_CONFIG,
                                                            chk_type);
            } else if (cmd_info_ptr->type.lane_config_info.lane_config_type == LANE_CONFIG_TYPE_LANE_TX_INFO) {
                CAPI_LOG_TX_INFO(&cmd_info_ptr->type.lane_config_info.type.lane_tx_info);
                return_result = sanity_checker_lane_tx_info(phy_info_ptr,
                                                            &cmd_info_ptr->type.lane_config_info.type.lane_tx_info,
                                                            chk_type);
            } else if (cmd_info_ptr->type.lane_config_info.lane_config_type == LANE_CONFIG_TYPE_LANE_LINK_TRAINING_INFO) {
                CAPI_LOG_LNKTRN_INFO(&cmd_info_ptr->type.lane_config_info.type.lane_lnktrn_info);
                return_result = sanity_checker_lnktrn_info(phy_info_ptr,
                                                           &cmd_info_ptr->type.lane_config_info.type.lane_lnktrn_info,
                                                           GET_CONFIG,
                                                           chk_type);
            } else {
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            }
            break;

        case COMMAND_ID_GET_LANE_CTRL_INFO:
            CAPI_LOG_LANE_CTRL_INFO(&cmd_info_ptr->type.lane_ctrl_info);
            return_result = sanity_checker_get_lane_ctrl_info(phy_info_ptr,
                                                              &cmd_info_ptr->type.lane_ctrl_info,
                                                              chk_type);
            break;

        case COMMAND_ID_SET_LANE_CTRL_INFO:
            CAPI_LOG_LANE_CTRL_INFO(&cmd_info_ptr->type.lane_ctrl_info);
            return_result = sanity_checker_set_lane_ctrl_info(phy_info_ptr,
                                                          &cmd_info_ptr->type.lane_ctrl_info);
            break;

        case COMMAND_ID_GET_LANE_INFO:
            CAPI_LOG_LANE_INFO(&cmd_info_ptr->type.lane_info);
            return_result = sanity_checker_get_lane_info(phy_info_ptr,
                                                         &cmd_info_ptr->type.lane_info,
                                                         chk_type);
            break;

        case COMMAND_ID_GET_CONFIG_INFO:
            CAPI_LOG_CONFIG_INFO(&cmd_info_ptr->type.config_info);
            return_result = sanity_checker_get_config_info(phy_info_ptr, &cmd_info_ptr->type.config_info, chk_type);
            break;

        case COMMAND_ID_SET_CONFIG_INFO:
            CAPI_LOG_CONFIG_INFO(&cmd_info_ptr->type.config_info);
            return_result = sanity_checker_set_config_info(phy_info_ptr, &cmd_info_ptr->type.config_info, chk_type);
            break;

        case COMMAND_ID_SET_LW_RCLK_INFO:
            return_result = sanity_checker_set_lw_rclk_info(phy_info_ptr, (capi_rclk_info_t*)&cmd_info_ptr->type, chk_type);
            break;

        case COMMAND_ID_DIAG_LANE_STATUS:
            break;

        case COMMAND_ID_SET_ARCHIVE_INFO:
            break;

        case COMMAND_ID_GET_ARCHIVE_INFO:
            break;

        case COMMAND_ID_SET_LOOPBACK_INFO:
            CAPI_LOG_LPBK_INFO(&cmd_info_ptr->type.lpbk_ctrl_info);
            return_result = sanity_checker_loopback(phy_info_ptr,
                                                    &cmd_info_ptr->type.lpbk_ctrl_info);
            break;

        case COMMAND_ID_GET_LOOPBACK_INFO:
            CAPI_LOG_LPBK_INFO(&cmd_info_ptr->type.lpbk_ctrl_info);
            return_result = sanity_checker_loopback(phy_info_ptr,
                                                    &cmd_info_ptr->type.lpbk_ctrl_info);
            break;

        case COMMAND_ID_SET_PRBS_INFO: /* Need confirmation on pattern values */
            return_result = sanity_checker_set_prbs_info(phy_info_ptr, &cmd_info_ptr->type.prbs_info);
            break;

        case COMMAND_ID_GET_PRBS_INFO:
            return_result = sanity_checker_get_prbs_info(phy_info_ptr, &cmd_info_ptr->type.prbs_info, chk_type);
            break;

        case COMMAND_ID_GET_PRBS_STATUS:
            CAPI_LOG_PRBS_STATUS(&cmd_info_ptr->type.prbs_status_info);
            return_result = sanity_checker_prbs_status_get(phy_info_ptr, &cmd_info_ptr->type.prbs_status_info, chk_type);
            break;

        case COMMAND_ID_CLEAR_PRBS_STATUS:
            return_result = sanity_checker_clear_prbs_status(phy_info_ptr, &cmd_info_ptr->type.prbs_status_info);
            break;

        case COMMAND_ID_INJ_PRBS_ERROR:
            return_result = sanity_checker_prbs_inject_error(phy_info_ptr, &cmd_info_ptr->type.prbs_err_inj_info);
            break;

        case COMMAND_ID_SET_TXPI_OVERRIDE:
            return_result = sanity_checker_set_txpi_override(phy_info_ptr, &cmd_info_ptr->type.txpi_ovrd);
            break;
            
        case COMMAND_ID_SET_CW_RPTR_FEC_MON_CONFIG:
            if (cmd_info_ptr->type.rptr_fec_st.fec_mode != CAPI_FEC_CLIENT && cmd_info_ptr->type.rptr_fec_st.fec_mode != CAPI_FEC_LINE) 
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            break;

        case COMMAND_ID_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH:
            CAPI_LOG_LANE_INFO(&cmd_info_ptr->type.optrxlos_host_fast_tx_squelch);
            return_result = RR_SUCCESS;
            break;            

        case COMMAND_ID_GET_CMIS_INFO:
            break;
        case COMMAND_ID_GET_SHIST_INFO:
            if (cmd_info_ptr->type.shistogram_info.shist_sel < 0 || cmd_info_ptr->type.shistogram_info.shist_sel > 3 ) 
                return_result = RR_ERROR_WRONG_INPUT_VALUE;
            break;

        case COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO:
        case COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO:
        case COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_INFO:
        case COMMAND_ID_DIAG_GET_MEDIA_MPI_STATE:
        case COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_STATE:
        case COMMAND_ID_DIAG_GET_SERDES_DIAG_INFO:
            break;
        
        case COMMAND_ID_DIAG_SET_MEDIA_MPI_CONFIG:
        case COMMAND_ID_DIAG_GET_MEDIA_MPI_CONFIG:
        case COMMAND_ID_DIAG_SET_MEDIA_MISSION_MPI_CONFIG:
        case COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_CONFIG:
            break;

        case COMMAND_ID_SET_SERDES_LANE_CDR_MODE:
        case COMMAND_ID_GET_SERDES_LANE_CDR_MODE:
            break;

        default:
            return_result = RR_ERROR_WRONG_INPUT_VALUE;
            break;
    }
    return return_result;
}

/**
 * @brief      avs_struct_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_avs_mode_config_info_t* avs_config_ptr)
 * @details    AVS structure sanity checker
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr   : phy info ptr
 * @param      avs_config_ptr : capi_avs_mode_config_info_t pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t avs_struct_sanity_checker(capi_phy_info_t *phy_info_ptr, capi_avs_mode_config_info_t* avs_config_ptr)
{
    return_result_t return_result = RR_SUCCESS;

    ERR_INVALID_RANGE(avs_config_ptr->avs, (int)CAPI_DISABLE, (int)CAPI_ENABLE);

    ERR_INVALID_RANGE(avs_config_ptr->disable_type, (int)CAPI_AVS_DISABLE_FIRMWARE_CONTROL,
                                (int)CAPI_AVS_DISABLE_NO_FIRMWARE_CONTROL);

    ERR_INVALID_RANGE(avs_config_ptr->avs_ctrl, (int)CAPI_INTERNAL_AVS_CONTROL,
                                (int)CAPI_EXTERNAL_AVS_CONTROL);

    ERR_INVALID_RANGE(avs_config_ptr->package_share, (int)CAPI_1_PACKAGE_SHARE_AVS,
                                (int)CAPI_8_PACKAGE_SHARE_AVS);

    ERR_INVALID_RANGE(avs_config_ptr->board_dc_margin, (int)CAPI_0_MV_MARGIN_ADDED_ON_TOP,
                                (int)CAPI_31_MV_MARGIN_ADDED_ON_TOP);

    ERR_INVALID_RANGE(avs_config_ptr->type_of_regulator, (int)CAPI_REGULATOR_INTERNAL,
                                (int)CAPI_REGULATOR_INTERNAL);

    ERR_INVALID_RANGE(avs_config_ptr->external_ctrl_step, (int)CAPI_AVS_EXTERNAL_CONTROL_STEP_1,
                                (int)CAPI_AVS_EXTERNAL_CONTROL_STEP_N);

    return return_result;
}

/**
 * @brief      host_diag_struct_sanity_checker(capi_phy_info_t *phy_info_ptr, void *in_struct_ptr, host_struct_sanity_id_t struct_id)
 * @details    Non-IPC, host based cAPI function's input structure validation
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      in_struct_ptr : input structure pointer
 * @param      struct_id     : unique structure id. refer to host_struct_sanity_id_t
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_diag_struct_sanity_checker(capi_phy_info_t *phy_info_ptr, void *in_struct_ptr, host_struct_sanity_id_t struct_id)
{
    return_result_t return_result = RR_SUCCESS;

    if (phy_info_ptr == NULL || in_struct_ptr == NULL)
        return RR_ERROR_WRONG_INPUT_VALUE;

    switch (struct_id)
    {
        case SANITY_AVS_CONFIG_ID:
            return_result = avs_struct_sanity_checker(phy_info_ptr, (capi_avs_mode_config_info_t*) in_struct_ptr);
            break;

        default:
            return_result = RR_ERROR_WRONG_INPUT_VALUE;
            break;
    }

    return return_result;
}

/**
 * @brief      host_get_memory_payload(capi_phy_info_t*    phy_info_ptr, 
 *                                     void*               dest_ptr,
 *                                     memory_info_t*      mem_data_ptr)
 *
 * @details    A fast access API to get response data populated by FW.
 *
 * @property   None
 * @public     None
 * @private    None
 * @example    None
 *
 * @param      phy_info_ptr  : phy info pointer
 * @param      dest_ptr      : dest data pointer
 * @param      mem_data_ptr :  memory data pointer
 *
 * @return     return_result_t:return status code as success/warning/error
 */
return_result_t host_get_memory_payload(capi_phy_info_t*    phy_info_ptr, 
                                                      void*               dest_ptr,
                                                      memory_info_t*      mem_data_ptr)
{
    return_result_t     return_result = RR_SUCCESS;
    capi_phy_info_t     phy_info;
    uint16_t            timeout_ms;

    if (!mem_data_ptr->ref_address) return RR_ERROR_WRONG_INPUT_VALUE;

    util_memcpy((void *)&phy_info, phy_info_ptr, sizeof(capi_phy_info_t));
    phy_info.base_addr = 0;

    /* Wait up to 100ms to acquire hw mutex before reading FW memory */
    for (timeout_ms = 0; timeout_ms < 100; timeout_ms++) {
        return_result = acquire_hw_mutex(&phy_info, mem_data_ptr->hw_mutex);
        if (return_result == RR_SUCCESS) {
            /* Read FW memory data info */
            intf_read_memory(&phy_info,
                    (uint32_t*) dest_ptr,
                    (uint32_t*) (mem_data_ptr->ref_address),
                    mem_data_ptr->ref_data_len);
            release_hw_mutex(&phy_info, mem_data_ptr->hw_mutex);
            break;
        }
        delay_ms(1);
    }
    if (return_result != RR_SUCCESS)
        CAPI_LOG_ERROR("host_get_memory_payload: failed to acquire hw mutex");
    return return_result;
}



/**
 * @brief      dsp_mission_mpi_cfg_sanity_checker(capi_phy_info_t* phy_info_ptr, dsp_mission_mpi_cfg_info_t* dsp_mission_mpi_cfg_info_ptr, sanity_chk_t chk_type)
 * @details    This API is used to sanity check steady state MPI configuration
 *
 * @ property  None
 * @ public    None
 * @ private   None
 * @ example   None
 *
 * @param[in]  phy_info_ptr     : this parameter 
 * @param[in]  dsp_mission_mpi_cfg_info_ptr: this parameter 
 * 
 * @return     returns the performance result of the called methode/function
 */
return_result_t dsp_mission_mpi_cfg_sanity_checker(capi_phy_info_t* phy_info_ptr, dsp_mission_mpi_cfg_info_t* dsp_mission_mpi_cfg_info_ptr, sanity_chk_t chk_type)
{
    /* MPI detect is line side only. 
     steady state MPI check counte default is 512, settable option are 128, 256, 512, 1024
     steady state MPI check threshold, default is 11, with 9,10,11,12,13 and 14 as settable options */
    if ((CORE_IP_MEDIA_DSP == phy_info_ptr->core_ip) &&
        ((128 == dsp_mission_mpi_cfg_info_ptr->mpi_chk_cnt) || (256 == dsp_mission_mpi_cfg_info_ptr->mpi_chk_cnt) || (512 == dsp_mission_mpi_cfg_info_ptr->mpi_chk_cnt) || (1024 == dsp_mission_mpi_cfg_info_ptr->mpi_chk_cnt)) &&
        (dsp_mission_mpi_cfg_info_ptr->mpi_chk_th >= 9 && dsp_mission_mpi_cfg_info_ptr->mpi_chk_th <= 14)) {
        return RR_SUCCESS;
    }
    else
        return RR_ERROR_WRONG_INPUT_VALUE;
}
