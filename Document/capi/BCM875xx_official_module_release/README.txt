This README file intends to describe general information of the release package

ROOT_FOLDER
   |
   |--- README.txt <==> This file. Describes release folder structure
   |
   |--- app_interface <==> application interface
   |        |
   |        |--- capi
   |               |
   |               | 
   |               |-- public ==> *.h/*.c <==> cAPI related header/source files
   |
   |--- chip <==> chip related files
   |      |
   |      |--- dep
   |      |     |
   |      |     |
   |      |     |-- common
   |      |     |     |
   |      |     |     |
   |      |     |     | -- inc <==> chip dependent header files
   |      |     |     |
   |      |     |     | -- src <==> chip dependent source files
   |      |     |
   |      |     |-- serdes <==> serdes files
   |      |     
   |      |     
   |      |--- indep
   |            |   
   |            |
   |            |-- inc <==> chip indepedent headers
   |            |
   |            |-- src <==> chip independent source files
   |
   |--- images <==> Latest firmware binaries that is compatible with this cAPI release
   |       |
   |       |
   |       |--- whole_image_sram.h <==> Firmware binary for MDIO / SRAM interface
   |       |--- whole_image_sram.bin <==> Binary format of the above
   |       |--- whole_image_sram.hex <==> Hex format of the above
   |       |--- whole_image_spi.h <==> Firmware binary for SPI interface
   |       |--- whole_image_spi.bin <==> Binary format of the above
   |       |--- whole_image_spi.hex <==> Hex format of the above
   |
   |--- platform
   |       | 
   |       |---- Platform dependent utilies files required by sample application
   |
   |
   |--- estoque2_cli <==> Sample Windows application prepared in visual c++ 2010 which demonstrates cAPI usage
            |
            |--- inc <==> sample code header files
            |
            |--- src <==> sample source files
    
               
