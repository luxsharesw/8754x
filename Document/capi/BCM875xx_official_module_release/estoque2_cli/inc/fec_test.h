/**
 *
 * @ fec_test.h
 * @author	 Team
 * @date	1/25/2016
 * @version	0.1
 *
 * @property	$ Copyright: (c) 2016 Broadcom Limited All Rights Reserved $
 *    	No portions of this material may be reproduced in any form without the
 *    	written permission of: 
 *                	Broadcom Limited
 *                	1320 Ridder Park Drive
 *                	San Jose, California 95131
 *                	United States
 *    	All information contained in this document/file is Broadcom Limit company
 *    	private proprietary, trade secret, and remains the property of Broadcom
 *     	Limited. The intellectual and technical concepts contained herein are
 *     	proprietary to Broadcom Limited and may be covered by U.S. and Foreign Patents,
 *    	patents in process, and are protected by trade secret or copyright law.
 *    	Dissemination of this information or reproduction of this material is strictly
 *    	forbidden unless prior written permission is obtained from Bloadcom Limited.
 *
 * @brief	brief description of the block source file, sequences/flow/etc.
 *
 * @section	description
 * 
 */

/** @file fec_test.h
 *  This file contains definitions and prototype for the top test
 */

#ifndef FEC_TEST_H
#define FEC_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

void fec_stat_tests(capi_phy_info_t* phy_info_ptr);

#ifdef __cplusplus
}
#endif

#endif /* FEC_TEST_H */
