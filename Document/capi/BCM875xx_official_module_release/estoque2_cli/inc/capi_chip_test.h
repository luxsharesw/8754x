/**
 * @file     bh_test.h
 * @author  
 * @date     06-06-2018
 * @version 1.0
 *
 * @property    $ Copyright: (c) 2018 Broadcom Limited All Rights Reserved $
 *       No portions of this material may be reproduced in any form without the
 *       written permission of: 
 *               Broadcom Limited
 *               1320 Ridder Park Drive
 *               San Jose, California 95131
 *               United States
 * All information contained in this document/file is Broadcom Limit company
 * private proprietary, trade secret, and remains the property of Broadcom
 * Limited. The intellectual and technical concepts contained herein are
 * proprietary to Broadcom Limited and may be covered by U.S. and Foreign
 * Patents, patents in process, and are protected by trade secret or copyright
 * law. Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from Bloadcom
 * Limited.
 *
 * @brief   
 *
 * @section
 * 
 */

#ifndef BH_TEST_H
#define BH_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

#define CHP_TST_MAX_LANE 4

void capi_test_top (capi_phy_info_t* phy_info_ptr);
return_result_t capi_test(capi_phy_info_t * phy_info_ptr, uint8_t test_case);

#ifdef __cplusplus
}
#endif

#endif  /* BH_TEST_H */
