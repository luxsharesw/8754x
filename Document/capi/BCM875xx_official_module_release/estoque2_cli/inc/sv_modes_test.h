#ifndef SV_MODES_TEST_H
#define SV_MODES_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

uint8_t test_chip_get_port_mode(capi_phy_info_t* phy_info_ptr);
void test_modes_program_grp (capi_phy_info_t* phy_info_ptr);

#ifdef __cplusplus
}
#endif

#endif  /* SV_MODES_TEST_H */
