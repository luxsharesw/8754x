#ifndef SV_MODES_H
#define SV_MODES_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct chip_capi_config_s {
    capi_config_info_t cfg;
    char desc[100];
    cw_chip_mode_t mode_name;
} chip_capi_config_t;

typedef struct chip_capi_config_grp_s {
    chip_capi_config_t* cfg;
    uint16_t size;
    char desc[100];
    cw_chip_mode_t mode_name;
} chip_capi_config_grp_t;

/* Util Functions */
chip_capi_config_grp_t* util_modes_get_grp_cfg (uint16_t* size_ptr);
uint8_t test_chip_get_port_mode(capi_phy_info_t* phy_info_ptr);


#ifdef __cplusplus
}
#endif

#endif  /* SV_MODES_H */
