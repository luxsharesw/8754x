/**
 *
 * @top_test.h
 * @author  HSIP FW Team
 * @date	1/25/2016
 * @version	0.1
 *
 * @property	$ Copyright: (c) 2016 Broadcom Limited All Rights Reserved $
 *    	No portions of this material may be reproduced in any form without the
 *    	written permission of: 
 *                	Broadcom Limited
 *                	1320 Ridder Park Drive
 *                	San Jose, California 95131
 *                	United States
 *    	All information contained in this document/file is Broadcom Limit company
 *    	private proprietary, trade secret, and remains the property of Broadcom
 *     	Limited. The intellectual and technical concepts contained herein are
 *     	proprietary to Broadcom Limited and may be covered by U.S. and Foreign Patents,
 *    	patents in process, and are protected by trade secret or copyright law.
 *    	Dissemination of this information or reproduction of this material is strictly
 *    	forbidden unless prior written permission is obtained from Bloadcom Limited.
 *
 * @brief	brief description of the block source file, sequences/flow/etc.
 *
 * @section	description
 * 
 */

/** @file top_test.h
 *  This file contains definitions and prototype for the top test
 */

#ifndef TOP_TEST_H
#define TOP_TEST_H

#ifdef __cplusplus
extern "C" {
#endif

bool test_sram_download(capi_phy_info_t* capi_phy_in_ptr, uint32_t skip_input);
bool test_spi_download(capi_phy_info_t* capi_phy_in_ptr, uint32_t skip_input);
void test_download_status(capi_phy_info_t* capi_phy_in_ptr);
void test_soft_reset(capi_phy_info_t* capi_phy_ptr, uint8_t mode);
void chip_avs_config(capi_phy_info_t* capi_phy_in_ptr);
void chip_avs_get_status(capi_phy_info_t* capi_phy_in_ptr);
void chip_internal_vddm_test(capi_phy_info_t* capi_phy_in_ptr);
void chip_internal_avdd_test(capi_phy_info_t* capi_phy_in_ptr);
return_result_t test_cfg_rptr_indep_lane_ctrl(capi_phy_info_t* capi_phy_ptr);
return_result_t test_cfg_line_pll_ddcc_ctrl(capi_phy_info_t* capi_phy_ptr);
void test_config_bootup_default(capi_phy_info_t* capi_phy_in_ptr);
#ifdef __cplusplus
}
#endif

#endif /* TOP_TEST_H */
