#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include <math.h>
#include <float.h>

#include "access.h"
#include "type_defns.h"
#include "chip_mode_def.h"
#include "estoque2_regs.h"

#include "hr_time.h"
#include "logger.h"

#include "common_def.h"
#include "capi_def.h"
#include "capi_test_def.h"
#include "capi_diag_def.h"

#include "common_util.h"
#include "capi.h"
#include "capi_test.h"
#include "capi_diag.h"
#include "host_diag.h"

#include "capi_chip_test.h"
#include "capi_diag_def.h"


typedef struct {
    float prbs_ber;
    uint64_t prbs_err;
    uint32_t pmon_lock;
    uint32_t pmon_loss_lock;
} prbs_err_ber_wlock_s;

static float host_drate[CAPI_BH_BR_MAX] = {53.125F, 51.5625F, 25.78125F, 26.5625F, 10.3125F, 20.625F, 1.25F};
static float media_drate[CAPI_LW_BR_MAX] = {53.125F, 51.5625F, 25.78125F, 26.5625F, 10.3125F, 20.625F, 1.25F};

typedef enum {
    CAPI_PRBS_GEN,
    CAPI_PRBS_CHK,
    CAPI_GET_PRBS_INFO,
    CAPI_PRBS_DISABLE,
    CAPI_PRBS_CLEAR,
    CAPI_PRBS_INJECT_ERROR,
    CAPI_READ_REGISTER,
    CAPI_WRITE_REGISTER,
    CAPI_GET_POLARITY,
    CAPI_CHANGE_POLARITY,
    CAPI_SET_TX_INFO,
    CAPI_GET_TX_INFO,
    CAPI_SET_SQUELCH,
    CAPI_GET_TX_ELECTRIC_IDLE_INFO,
    CAPI_SET_LOOPBACK,
    CAPI_GET_LOOPBACK,
    CAPI_GET_LANE_STATUS,
    CAPI_SET_LANE_CTRL,
    CAPI_GET_LANE_CTRL,
    CAPI_LANE_RESET,
    CAPI_SET_RX_INFO,
    CAPI_GET_RX_INFO,
    CAPI_HOST_SERDES_SET_TX_SHARED_PTRN,
    CAPI_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH,
    CAPI_TEST_SET_ELECTRICAL_OPTICAL,
    CAPI_GET_GPR_LANE_STATUS,
    CAPI_DIAG_GET_LANE_STATUS,
    CAPI_SET_DSP_POWER_INFO,
    CAPI_GET_DSP_POWER_INFO,
    CAPI_TEST_FAST_CMIS_SNR,
    CAPI_TEST_CONFIG_MPI,
    CAPI_TEST_GET_MPI_STATE,
    CAPI_TEST_CONFIG_DYNAMIC_MPI,
    CAPI_TEST_GET_DYNAMIC_MPI_STATE,
    CAPI_SERDES_SET_LANE_CDR_MODE,
    CAPI_SERDES_GET_LANE_CDR_MODE,
    CAPI_SERDES_GET_DIAG_INFO,
    CAPI_TEST_SET_TC_TX_TYPE,
    CAPI_GET_USR_DIAG,
    CAPI_GET_DSP_SLICER_HISTOGRAM,
    BACK_TO_PREV_MENU,
    CAPI_TEST_MAX
} estoque2_capi_test_type_e;

typedef enum {
    LANE_SQUELCH,
    LANE_RESET,
    TRAFFIC,
    ELECTRIC_IDLE,
    DATAPATH_POWER,
    IGNORE_FAULT,
    SUSPEND_RESUME
} test_lane_ctrl_e;

static char capi_test_description[CAPI_TEST_MAX][100] = {
    "CAPI_PRBS_GEN",
    "CAPI_PRBS_CHK",
    "CAPI_GET_PRBS_INFO",
    "CAPI_PRBS_DISABLE",
    "CAPI_PRBS_CLEAR",
    "CAPI_PRBS_INJECT_ERROR",
    "CAPI_READ_REGISTER",
    "CAPI_WRITE_REGISTER",
    "CAPI_GET_POLARITY",
    "CAPI_CHANGE_POLARITY",
    "CAPI_SET_TX_INFO",
    "CAPI_GET_TX_INFO",
    "CAPI_SET_SQUELCH",
    "CAPI_GET_TX_ELECTRIC_IDLE_INFO",
    "CAPI_SET_LOOPBACK",
    "CAPI_GET_LOOPBACK",
    "CAPI_GET_LANE_STATUS",
    "CAPI_SET_LANE_CTRL",
    "CAPI_GET_LANE_CTRL",
    "CAPI_LANE_RESET",
    "CAPI_SET_RX_INFO",
    "CAPI_GET_RX_INFO",
    "CAPI_HOST_SERDES_SET_TX_SHARED_PTRN",
    "CAPI_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH",
    "CAPI_TEST_SET_ELECTRICAL_OPTICAL",
    "CAPI_GET_GPR_LANE_STATUS",
    "CAPI_DIAG_GET_LANE_STATUS",
    "CAPI_SET_DSP_POWER_INFO",
    "CAPI_GET_DSP_POWER_INFO",
    "CAPI_TEST_FAST_CMIS_SNR",
    "CAPI_TEST_CONFIG_MPI",
    "CAPI_TEST_GET_MPI_STATE",
    "CAPI_TEST_CONFIG_DYNAMIC_MPI",
    "CAPI_TEST_GET_DYNAMIC_MPI_STATE",
    "CAPI_SERDES_SET_LANE_CDR_MODE",
    "CAPI_SERDES_GET_LANE_CDR_MODE",
    "CAPI_SERDES_GET_DIAG_INFO",
    "CAPI_TEST_SET_TC_TX_TYPE",
    "CAPI_GET_USR_DIAG",
    "CAPI_GET_DSP_SLICER_HISTOGRAM",
    "BACK_TO_PREV_MENU"
};

static char lane_ctrl_options[7][100] = {
    "Squelch",
    "Lane Reset",
    "Traffic",
    "Electric Idle",
    "DataPath Power",
    "Ignore Fault",
    "Suspend/Resume"
};

#define lane_config_rx_set_dsp_options_len 64
static char lane_config_rx_set_dsp_options[][lane_config_rx_set_dsp_options_len] = {
    "None",
    "set dsp_graycode",
    "set dsp_dc_wander_mu",
    "set dsp_nldet_en",
    "set oplos ignore",
    "set elos ignore",
    "set los_th_idx",
    "set inlos_th_idx",
    "set outlos_th_idx",
    "set kp_kf info",
    "set phase_bias_auto_tuning_info",
    "gain boost",
    "set power_mode",
};


static char direction_str[3][10] = { "Tx", "Rx", "BOTH" };

uint32_t get_lane_mask(void)
{
    int channel_id, lane_num = 4;
    uint32_t lane_mask;

    dprintf("\n\nPlease choose channel ID: 0-all_ch; 1-ch_0; 2: ch_1; 3:ch_2; 4:ch_3; \r\n");
    scanf("%d", &channel_id);

    if (channel_id == 0) {
        lane_mask = 0xF;
    } else if (channel_id > 0 && channel_id <= lane_num)  {
        lane_mask = 1 << (channel_id-1);
    } else {
        dprintf("\n** invalid channel ID %d, choose all 4 lanes \r\n", channel_id);
        lane_mask = 0xF;
    }
    return lane_mask;
}




void swap_rx_polarity(capi_phy_info_t *capi_phy, uint8_t laneid)
{
    capi_polarity_info_t capi_polarity;
    uint8_t rx_polarity, tx_polarity, idx;
    uint8_t lane_mask_bk = capi_phy->lane_mask;

    //Fill in the polarity type and config
    if ((capi_phy->core_ip == CORE_IP_HOST_DSP) ||
        (capi_phy->core_ip == CORE_IP_HOST_SERDES)) {
        capi_polarity.direction = DIR_EGRESS; //egress
    } else {
        capi_polarity.direction = DIR_INGRESS; //ingress
    }
    for (idx = 0; idx < CHP_TST_MAX_LANE; idx++) {
        if ((lane_mask_bk & (1 << idx)) == 0)
            continue;
        capi_phy->lane_mask = (1 << idx);
        capi_get_polarity(capi_phy, &capi_polarity);
        dprintf("\n\nCurrent lane %d Rx polarity setting is: %d \r\n", idx, capi_polarity.action);
    }

    capi_phy->lane_mask = lane_mask_bk;
    capi_polarity.action = POLARITY_SWAP_CURRENT_SETTING;

    if (capi_set_polarity(capi_phy, &capi_polarity) != RR_SUCCESS)
        dprintf("\n\n*****  Config CLIENT Polarity FAILED ! ***** \r\n");
    else
        dprintf("\n\n***** SWAP RX polarity ! ***** \r\n");

    lane_mask_bk = capi_phy->lane_mask;

    delay_ms(2000);
    capi_polarity.direction = DIR_BOTH; // 2
    for (idx = 0; idx < CHP_TST_MAX_LANE; idx++) {
        if ((lane_mask_bk & (1 << idx)) == 0)
            continue;
        capi_phy->lane_mask = (1 << idx);
        if (capi_polarity.direction == DIR_BOTH) {
            capi_polarity.direction = DIR_INGRESS;
            capi_get_polarity(capi_phy, &capi_polarity);
            if ((capi_phy->core_ip == CORE_IP_HOST_DSP) || (capi_phy->core_ip == CORE_IP_HOST_SERDES)) 
            {
                tx_polarity = capi_polarity.action;
            }
            else
            {
                rx_polarity = capi_polarity.action;
            }

            capi_polarity.direction = DIR_EGRESS;
            capi_get_polarity(capi_phy, &capi_polarity);
            if ((capi_phy->core_ip == CORE_IP_HOST_DSP) || (capi_phy->core_ip == CORE_IP_HOST_SERDES)) 
            {
                rx_polarity = capi_polarity.action;
            }
            else
            {
                tx_polarity = capi_polarity.action;
            }

            dprintf("\n\nCurrent lane %d polarity setting is RX: %d -- TX %d \r\n", idx, rx_polarity, tx_polarity);

            capi_polarity.direction = DIR_BOTH;
        }
    }
}


static return_result_t cli_get_usr_diag(capi_phy_info_t* phy_info_ptr)
{
    int idx, idx2, levels;
    uint16_t lane_mask = phy_info_ptr->lane_mask;
    capi_usr_diag_info_t usr_diag_info;
    capi_usr_diag_info_t vdm_info;
    uint8_t max_lanes = 0;
    float slicer_sigma_reorder[4];

    
    if (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES || phy_info_ptr->core_ip == CORE_IP_HOST_DSP)
        max_lanes = MAX_CLIENT_LANES;
    else if (phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES || phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)
        max_lanes = MAX_LW_LANES;
    

    for (idx = 0; idx < max_lanes; idx++) {
        if (lane_mask & (1 << idx)) {
            phy_info_ptr->lane_mask = (1 << idx);
            if (phy_info_ptr->core_ip == CORE_IP_HOST_SERDES || phy_info_ptr->core_ip == CORE_IP_MEDIA_SERDES) {
                util_memset(&usr_diag_info.type.diag_cmis_info.value.serdes_snr_ltp.snr_ltp, 0, sizeof(serdes_snr_ltp_t));
                usr_diag_info.diag_info_type = DIAG_INFO_TYPE_CMIS;
                usr_diag_info.type.diag_cmis_info.param.is.serdes_snr_ltp = 1;
                dprintf("\n\n Please input fec ber:");
                scanf("%lf", &usr_diag_info.type.diag_cmis_info.value.serdes_snr_ltp.fec_ber);
                if (capi_get_usr_diagnostics(phy_info_ptr, &usr_diag_info) != RR_SUCCESS) {
                    dprintf("\n\n***** Get usr diagnostics failed! *****\r\n");
                    return RR_ERROR;
                } 
                else {
                    dprintf("\n\n ***** serdes lane %x cmis snr=%d ltp=%d ***** \r\n",
                        idx, usr_diag_info.type.diag_cmis_info.value.serdes_snr_ltp.snr_ltp.snr_value,
                        usr_diag_info.type.diag_cmis_info.value.serdes_snr_ltp.snr_ltp.ltp_value);
                }
            }
            else if (phy_info_ptr->core_ip == CORE_IP_HOST_DSP || phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP) {
                util_memset(&vdm_info, 0, sizeof(vdm_info));
                vdm_info.diag_info_type = DIAG_INFO_TYPE_BRCM_DSP_SNR;
                if (capi_get_usr_diagnostics(phy_info_ptr, &vdm_info) != RR_SUCCESS) {
                    dprintf("\n\n***** Get usr RX diagnostics failed! *****\r\n");
                    return RR_ERROR;
                } 
                else {
                    dprintf("\tLW usr diag lane %d BRCM SNR=%f\n", idx, vdm_info.type.diag_brcm_dsp_snr.brcm_snr_value);
                    levels = (int)vdm_info.type.diag_brcm_dsp_snr.brcm_snr_level[0];
                    for (idx2 = 1; idx2 <= levels; idx2++)
                        dprintf("\tLW usr diag lane %d BRCM SNR LEVEL[%d]=%f\n", idx, idx2, vdm_info.type.diag_brcm_dsp_snr.brcm_snr_level[idx2]);
                }
                util_memset(&vdm_info, 0, sizeof(vdm_info));
                vdm_info.diag_info_type = DIAG_INFO_TYPE_CMIS;
                vdm_info.type.diag_cmis_info.param.is.dsp_snr_ltp = 1;
                if (capi_get_usr_diagnostics(phy_info_ptr, &vdm_info) != RR_SUCCESS) {
                    dprintf("\n\n***** Get usr CMIS diagnostics failed! *****\r\n");
                    return RR_ERROR;
                } 
                else {
                    dprintf("\n\n***** Get LW lane %d CMIS diagnostics SNR %f LTP %f! *****\r\n", idx,
                    vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.cmis_snr,vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.cmis_ltp);

                    dprintf("FFE config: slicer threshold:  %d %d %d, slicer target  %d %d %d %d\n",
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_threshold[0], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_threshold[1],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_threshold[2],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_target[0], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_target[1],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_target[2], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_target[3]);
                    dprintf("FFE slicer mean values:  %f %f %f %f\n",
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_mean_value[0], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_mean_value[1],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_mean_value[2], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_mean_value[3]);
                    dprintf("FFE slicer sigma values: %f %f %f %f\n",
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[0], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[1],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[2], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[3]);
                    if(vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[3] < vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[0]){
                           for(idx2=0; idx2<4; idx2++)
                               slicer_sigma_reorder[idx2] = vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[3 - idx2];
                       }else{
                           for(idx2=0; idx2<4; idx2++)
                               slicer_sigma_reorder[idx2] = vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_sigma_value[idx2];
                       }
                    dprintf("FFE slicer sigma re-ordered normalized (100/256) values: %f %f %f %f \n",
                        slicer_sigma_reorder[0]*100.0/256.0,
                        slicer_sigma_reorder[1]*100.0/256.0,
                        slicer_sigma_reorder[2]*100.0/256.0, 
                        slicer_sigma_reorder[3]*100.0/256.0);

                    dprintf("FFE slicer P values: \t%d(%d) \t%d(%d) \t%d(%d) \t%d(%d)\n",
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_value[0], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_location[0],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_value[1], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_location[1],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_value[2], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_location[2],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_value[3], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_p_location[3]);
                    dprintf("FFE slicer V values: \t%d(%d) \t%d(%d) \t%d(%d)\n\n",
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_value[0], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_location[0],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_value[1], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_location[1],
                            vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_value[2], vdm_info.type.diag_cmis_info.value.dsp_snr_ltp.slicer_v_location[2]);
                }
            }
            else {
                dprintf("Incorrect core ip!\r\n");
                return RR_ERROR_WRONG_INPUT_VALUE;
            }
        }
    }
    return RR_SUCCESS;
}

static return_result_t cli_get_dsp_slicer_shistogram(capi_phy_info_t * phy_info_ptr)
{
    int idx, hidx;
    int usr_input;
    uint16_t lane_mask_bk = phy_info_ptr->lane_mask;
    capi_usr_diag_info_t shist_info;
    
    dprintf("\n\nPlease select slicer histogram traffic source: 0 ~3 \r\n");
    scanf("%d", &usr_input);
    usr_input &= 0x3;

    for (idx = 0; idx < MAX_LW_LANES; idx++) {
        if ((lane_mask_bk & (1 << idx)) == 0)
            continue;
        phy_info_ptr->lane_mask = (1 << idx);
        util_memset(&shist_info, 0, sizeof(capi_usr_diag_info_t));
        shist_info.diag_info_type = DIAG_INFO_TYPE_SLICE_HISTOGRAM;
        shist_info.type.diag_slc_hstgrm.shist_sel = usr_input;
        if (capi_get_usr_diagnostics(phy_info_ptr, &shist_info) != RR_SUCCESS) {
            dprintf("\n\n***** Get usr RX diagnostics failed! *****\r\n");
            return RR_ERROR;
        } else {
            dprintf("\n\n\tLW lane %d slice histogram traffic source: %d \n", idx, shist_info.type.diag_slc_hstgrm.shist_sel);
            for (hidx = 0; hidx < 256; hidx++) {
                if ((hidx % 8) == 0) 
                    dprintf("\n%4d:    ", hidx - 128);
                dprintf("%10d, ", shist_info.type.diag_slc_hstgrm.shist_data[hidx]);
            }
        }
    }
    return RR_SUCCESS;
}

static return_result_t cli_test_set_elec_optical(capi_phy_info_t * phy_info_ptr)
{
    uint8_t idx;
    int usr_input;
    uint8_t rx_en_loss;
    uint8_t rx_loadc2, rx_loadc2_trck;
    uint8_t nldet_en;
    uint8_t rd_en_loss,hs_ls;
    uint8_t configure_success = 1;
    uint16_t pidx;
    uint16_t reg_val;
    uint16_t reg_val1;

    phy_info_t       lw_phy;

    return_result_t  return_result ;
    capi_test_command_info_t test_command_info;
    capi_dsp_mode_type_t usr_dsp_mode;

    phy_info_ptr->lane_mask = 1;

    util_memset((void *)&test_command_info, 0, sizeof(capi_test_command_info_t));
    test_command_info.command_id = COMMAND_ID_GET_LW_DSP_MODE;
    return_result = capi_test_set_command(phy_info_ptr, &test_command_info);
    if (return_result) {
        dprintf("\n\n***** Failed to retrieve line dsp mode! *****\r\n");
        return return_result;
    }
    else {
        dprintf("\n\n*****  Current line dsp uses %s mode! *****\r\n", test_command_info.type.dsp_mode == CAPI_DSP_ELECTRIC_MODE ? "Electrical" : "Optical");
    }

    dprintf("\n\nPlease select from desired mode: 0 - optical; 1 - eletrical\r\n");
    scanf("%d", &usr_input);
    if (usr_input < 0 || usr_input > 1) {
        dprintf("Invalid input ! Defaults to optical mode\r\n");
        usr_input = 0; // defaults to optical
    }
    usr_dsp_mode = (capi_dsp_mode_type_t) usr_input;

    util_memset((void *)&test_command_info, 0, sizeof(capi_test_command_info_t));
    test_command_info.command_id = COMMAND_ID_SET_LW_DSP_MODE;
    test_command_info.type.dsp_mode = (capi_dsp_mode_type_t) usr_input;
    return_result = capi_test_set_command(phy_info_ptr, &test_command_info);

    if (return_result) {
        dprintf("Failed to set dsp mode!\r\n");
        return return_result;
    }

    util_memset((void *)&test_command_info, 0, sizeof(capi_test_command_info_t));
    test_command_info.command_id = COMMAND_ID_GET_LW_DSP_MODE;
    return_result = capi_test_set_command(phy_info_ptr, &test_command_info);
    if (return_result) {
        dprintf("\n\n***** Failed to retrieve line dsp mode! *****\r\n");
        return return_result;
    }
    else {
        dprintf("\n\n*****  Current line dsp uses %s mode! *****\r\n", test_command_info.type.dsp_mode == CAPI_DSP_ELECTRIC_MODE ? "Electrical" : "Optical");
    }

    if(test_command_info.type.dsp_mode != usr_dsp_mode) {
        dprintf("\n\n*****  Detected mode mismatch between set %s and get %s! *****\r\n",
                test_command_info.type.dsp_mode? "Electric mode" : "Optical mode",
                usr_dsp_mode? "Electric mode" : "Optical mode");
                return RR_ERROR;
    }

    delay_s(1);

    if (usr_dsp_mode == CAPI_DSP_OPTICAL_MODE) {
        rx_en_loss = 0;
        rx_loadc2  = 0xF;
        rx_loadc2_trck = 0x8;
        nldet_en   = 1;
    } else {
        host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, 0);
        ERR_HSIP(hs_ls = (uint8_t)hsip_rd_field_(&lw_phy, ANA_TX_ANA_TX_STAT_0, SEL_LS_HS_STAT));
        rx_en_loss = hs_ls?7:0;
        rx_loadc2  = 0;
        rx_loadc2_trck = 0;
        nldet_en   = 0;
    }

    for (idx = 0; idx < MAX_LW_LANES; idx++) {
        host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, idx);
        ERR_HSIP(reg_val = hsip_rd_field_(&lw_phy, ANA_RX_ANA_RX_CTRL_13, MD_ANA_RX_EN_LOSS));
        rd_en_loss = 0;

        for (pidx = 0; pidx < 12; pidx++) {
            if (reg_val & (1 << pidx))
            rd_en_loss++;
        }
        if (rd_en_loss != rx_en_loss) {
            dprintf("lane %d en_loss is incorrect. Expected %d rd %d \r\n", idx, rx_en_loss, rd_en_loss);
            configure_success &= 0;
        }

        host_util_get_lw_phy_info(phy_info_ptr, &lw_phy, idx);
        ERR_HSIP(reg_val = hsip_rd_field_(&lw_phy, ANA_RX_ANA_RX_CTRL_13, MD_ANA_RX_LOADC2 ));
        if (reg_val != rx_loadc2 && reg_val != rx_loadc2_trck){
            dprintf("lane %d rx_loadc2 is incorrect. Expected %d or %d rd %d \r\n", idx, rx_loadc2, rx_loadc2_trck, reg_val);
            configure_success &= 0;
        }

        ERR_HSIP(reg_val = hsip_rd_field_(&lw_phy, EQ_POST_NLDET_CSR0_REG, MD_NLDET_CLK_ENA ));
        ERR_HSIP(reg_val1 = hsip_rd_field_(&lw_phy, EQ_POST_NLDET_CSR0_REG, MD_NLDET_SW_RSTN ));
        if (reg_val != nldet_en || reg_val1 != nldet_en){
            dprintf("lane %d nldet_en is incorrect. Expected %d rd %d \r\n", idx, nldet_en, reg_val);
            configure_success &= 0;
        }
    }

    if (configure_success)  {
        dprintf("\n Config %s successfully \r\n", test_command_info.type.dsp_mode ? "Electric bench mode" : "Optical bench mode");
        dprintf("\r RX EN_LOSS %d \r\n", rx_en_loss);
        dprintf("\r RX LOADC2  %d \r\n", rx_loadc2);
        dprintf("\r NLDET      %d \r\n", nldet_en);
        return RR_SUCCESS;
    }
    else {
        return RR_ERROR;
    }
}

static capi_diag_command_request_info_t req_info = {0};
void cli_test_fast_cmis_snr(capi_phy_info_t capi_phy)
{
    int op_code, i, loops, lane_id;
#if defined _WIN32
    LARGE_INTEGER liStart;
    double elapsed_time;
#endif
    capi_cmis_snr_ltp_t *cmis_snr_ltp_ptr;
    return_result_t return_rslt;

    dprintf("\n Start SNR polling: how many loops to run: \r\n");
    scanf("%d", &op_code);
    loops = op_code;
    
    dprintf("\n Need reconfig? 0-no, 1-yes \r\n");
    scanf("%d", &op_code);
    if(op_code){

        dprintf("Start SNR poll, loops %d, lane_mask 0x%x\n", loops, capi_phy.lane_mask);    

        req_info.command_id = COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO;
        req_info.type.cmis_snr_ltp.param.command.start_stop = 1;
        req_info.type.cmis_snr_ltp.param.command.poll_interval = 1000;
    
        return_rslt = capi_diag_command_request_info(&capi_phy, &req_info);
        if (return_rslt != RR_SUCCESS) {
            dprintf("COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO enable polling failed with ecode %d\n", return_rslt);
            return;
        }
        dprintf("COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO return ref_address 0x%x ref_data_len %d hw_mutex 0x%x \n",
            req_info.type.cmis_snr_ltp.memory_info.ref_address,
            req_info.type.cmis_snr_ltp.memory_info.ref_data_len,
            req_info.type.cmis_snr_ltp.memory_info.hw_mutex);
    
        req_info.command_id = COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO;
    return_rslt = capi_diag_command_request_info(&capi_phy, &req_info);
        if (return_rslt != RR_SUCCESS) {
            dprintf("COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO failed with ecode %d\n",return_rslt);
            return;
        }
        if (req_info.type.cmis_snr_ltp.param.command.start_stop != 1 || req_info.type.cmis_snr_ltp.param.command.poll_interval != 1000) {
            dprintf("COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO mismatch: start_stop %d poll_interval %d \n",
                req_info.type.cmis_snr_ltp.param.command.start_stop,
                req_info.type.cmis_snr_ltp.param.command.poll_interval);
            return;
        }
    }
    dprintf("COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO return ref_address 0x%x ref_data_len %d hw_mutex 0x%x \n",
        req_info.type.cmis_snr_ltp.memory_info.ref_address,
        req_info.type.cmis_snr_ltp.memory_info.ref_data_len,
        req_info.type.cmis_snr_ltp.memory_info.hw_mutex);

    req_info.command_id = COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_INFO;
    for (i = 0; i < loops; i++)
    {
        delay_ms(1000);

#if defined _WIN32
        QueryPerformanceCounter(&liStart); 
#endif
        return_rslt = capi_diag_command_request_info(&capi_phy, &req_info);
        if (return_rslt != RR_SUCCESS) {
            dprintf("COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_INFO failed with ecode %d at loop %d\n", 
                        return_rslt, loops);
            return;
        }
        
#if defined _WIN32
        elapsed_time = get_elapsed_ms(&liStart);
        dprintf("loop %d read SNR takes %f ms\n", i, elapsed_time);
#endif

        for (lane_id = 0; lane_id < MAX_LW_LANES; lane_id++) {
            cmis_snr_ltp_ptr = &req_info.type.cmis_snr_ltp.param.lane_data[lane_id];
            dprintf("Lane %d: brcm_snr %f cmis_snr %f cmis_ltp %f\n", lane_id,
                cmis_snr_ltp_ptr->brcm_snr_value,
                cmis_snr_ltp_ptr->cmis_snr_value,
                cmis_snr_ltp_ptr->cmis_ltp_value);
        }
    }

    dprintf("\n Disable SNR polling: 1 - disable 0 - do not disable \r\n");
    scanf("%d", &op_code);
    if (op_code) {
        req_info.command_id = COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO;
        req_info.type.cmis_snr_ltp.param.command.start_stop = 0;
        req_info.type.cmis_snr_ltp.param.command.poll_interval = 1000;
        return_rslt = capi_diag_command_request_info(&capi_phy, &req_info);
        if (return_rslt != RR_SUCCESS) {
            dprintf("COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO disable polling failed with ecode %d\n", return_rslt);
            return;
        }
    }
}
static return_result_t cli_test_set_tc_tx_mode(capi_phy_info_t * phy_info_ptr)
{
    uint8_t                     idx;
    int                         usr_input;
    phy_info_t                  lw_phy;
    return_result_t             return_result ;
    capi_test_command_info_t    test_command_info;
    capi_dsp_tc_tx_type_t       usr_tc_tx_mode;

    phy_info_ptr->lane_mask = 1;

    util_memset((void *)&test_command_info, 0, sizeof(capi_test_command_info_t));
    test_command_info.command_id = COMMAND_ID_GET_TC_SE_MODE;
    return_result = capi_test_set_command(phy_info_ptr, &test_command_info);
    if (return_result!=RR_SUCCESS) {
        dprintf("\n\n***** Failed to retrieve TC tx mode with ecode %d! *****\r\n", return_result);
        return return_result;
    }
    else {
        dprintf("\n\n*****  Current TC tx is in %s mode! *****\r\n", 
            test_command_info.type.tc_tx_mode == CAPI_DSP_TC_TX_DIFFERENTIAL_MODE ? "Differential" : "Single End");
    }

    dprintf("\n\nPlease select from desired mode: 0 - Differential; 1 - Single End\r\n");
    scanf("%d", &usr_input);
    if (usr_input < 0 || usr_input > 1) {
        dprintf("Invalid input TC TX mode\r\n");
        usr_input = 0; // defaults to optical
    }
    usr_tc_tx_mode = (capi_dsp_tc_tx_type_t) usr_input;

    util_memset((void *)&test_command_info, 0, sizeof(capi_test_command_info_t));
    test_command_info.command_id      = COMMAND_ID_SET_TC_SE_MODE;
    test_command_info.type.tc_tx_mode = usr_tc_tx_mode;
    return_result = capi_test_set_command(phy_info_ptr, &test_command_info);

    if (return_result!=RR_SUCCESS) {
        dprintf("Failed to set TC tx mode with ecode %d!!\r\n", return_result);
        return return_result;
    }

    util_memset((void *)&test_command_info, 0, sizeof(capi_test_command_info_t));
    test_command_info.command_id = COMMAND_ID_GET_TC_SE_MODE;
    return_result = capi_test_set_command(phy_info_ptr, &test_command_info);
    if (return_result) {
        dprintf("\n\n***** Failed to retrieve TC tx mode with ecode %d! *****\r\n", return_result);
        return return_result;
    }
    else {
        dprintf("\n\n*****  Current TC tx is in %s mode! *****\r\n", 
            test_command_info.type.tc_tx_mode == CAPI_DSP_TC_TX_DIFFERENTIAL_MODE ? "Differential" : "Single End");
    }

    if(test_command_info.type.dsp_mode != usr_tc_tx_mode) {
        dprintf("\n\n*****  Detected mode mismatch between set %s and get %s! *****\r\n",
                test_command_info.type.tc_tx_mode? "Single End mode" : "Differential mode",
                usr_tc_tx_mode? "Single End mode" : "Differential mode");
                return RR_ERROR;
    }

    delay_s(1);
    
    return RR_SUCCESS;
}

static return_result_t get_prbs_data_rate(capi_phy_info_t *capi_phy, float *data_rate)
{
    capi_config_info_t config_info;
    util_memset((void *)&config_info, 0, sizeof(capi_config_info_t));
    if (capi_phy->core_ip == CORE_IP_MEDIA_DSP)
        config_info.line_lane.lane_mask = capi_phy->lane_mask;
    else if (capi_phy->core_ip == CORE_IP_HOST_SERDES)
        config_info.host_lane.lane_mask = capi_phy->lane_mask;
    else
        return RR_ERROR;

    if (capi_get_config(capi_phy, &config_info) != RR_SUCCESS ||
        config_info.func_mode <= 0 || config_info.func_mode >= CAPI_MODE_MAX)
        return RR_ERROR;

    if (capi_phy->core_ip == CORE_IP_MEDIA_DSP)
        *data_rate = media_drate[config_info.lw_br];
    else
        *data_rate = host_drate[config_info.bh_br];
    return RR_SUCCESS;
}

static return_result_t get_prbs_ber(capi_phy_info_t *capi_phy, void *berp_ptr, uint32_t interval_ms, uint32_t read_times, uint32_t option)
{
    uint32_t i;
    prbs_err_ber_wlock_s * berp = (prbs_err_ber_wlock_s *)berp_ptr;
    capi_prbs_status_t capi_prbs_status = {0};
    float data_rate;
    char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13"};

    if (get_prbs_data_rate(capi_phy, &data_rate)!=RR_SUCCESS)
        return RR_ERROR;
    if (option == 0)
        capi_prbs_status.prbs_type = CAPI_PRBS_MONITOR;
    else if (option == 1)
        capi_prbs_status.prbs_type = CAPI_PRBS_SSPRQ_MONITOR;
    else
        capi_prbs_status.prbs_type = CAPI_PRBS_Q_PRBS_13_MONITOR;
    for (i = 1; i <= read_times; i++) {
        delay_ms(interval_ms);
        if (capi_get_prbs_status(capi_phy, &capi_prbs_status) != RR_SUCCESS) {
            dprintf("capi_get_prbs_status error\n");
            return RR_ERROR;
        }

        berp->pmon_lock = capi_prbs_status.lock;
        berp->pmon_loss_lock = capi_prbs_status.lock_loss;

        // cal BER
        berp->prbs_ber = 1.0;
        if (berp->pmon_lock) {
            if (capi_phy->core_ip == CORE_IP_HOST_SERDES)
                berp->prbs_err += capi_prbs_status.err.s_err;
            else
                berp->prbs_err += (capi_prbs_status.err.ml_err.lsb_err + capi_prbs_status.err.ml_err.msb_err);
            berp->prbs_ber = (float)(berp->prbs_err / (data_rate) /(1000000000.) / (i*interval_ms/1000.));
        } else {
            dprintf("%s not locked!\n", &patt_gen_mon_type[option]);
        }
        dprintf("%s lock = 0x%lx,  loss_of_lock_sticky = 0x%lx, err_cnt = 0x%lx ",
                &patt_gen_mon_type[option], berp->pmon_lock, berp->pmon_loss_lock, berp->prbs_err);
        dprintf("ber=%.6e\n", berp->prbs_ber);
    }
    return RR_SUCCESS;
}

static void util_get_poly(capi_phy_info_t * capi_phy_info_ptr, int * poly_ptr) {

    capi_prbs_poly_type_t line_poly[] = {CAPI_PRBS_POLY_7, /**< Line PRBS polynomial 7 */
                                        CAPI_PRBS_POLY_9,  /**< Line PRBS polynomial 9 */
                                        CAPI_PRBS_POLY_11, /**< Line PRBS polynomial 11 */
                                        CAPI_PRBS_POLY_15, /**< Line PRBS polynomial 15 */
                                        CAPI_PRBS_POLY_23, /**< Line PRBS polynomial 23 */
                                        CAPI_PRBS_POLY_31, /**< Line PRBS polynomial 31 */
                                        CAPI_PRBS_POLY_58, /**< Line PRBS polynomial 58 */
                                        CAPI_PRBS_POLY_49, /**< Line PRBS polynomial 49 */
                                        CAPI_PRBS_POLY_10, /**< Line PRBS polynomial 10 */
                                        CAPI_PRBS_POLY_20, /**< Line PRBS polynomial 20 */
                                        CAPI_PRBS_POLY_13, /**< Line PRBS polynomial 13 */
                                        };

    do {
        dprintf("\r\n** Please choose poly: \
                         \r\n0: PRBS_7 \
                         \r\n1: PRBS_9 \
                         \r\n2 :PRBS_11 \
                         \r\n3: PRBS_15 \
                         \r\n4: PRBS_23 \
                         \r\n5: PRBS_31 \
                         \r\n6: PRBS_58 \
                         \r\n7: PRBS_49 \
                         \r\n8: PRBS_10 \
                         \r\n9: PRBS_20 \
                         \r\n10: PRBS_13\r\n");
        scanf("%d", poly_ptr);
    } while (*poly_ptr < 0 || *poly_ptr > 10);

    if (capi_phy_info_ptr->core_ip == CORE_IP_HOST_DSP || 
        capi_phy_info_ptr->core_ip == CORE_IP_MEDIA_DSP)
    {
        *poly_ptr = (int) (line_poly[*poly_ptr]);
    }
}


void cli_config_media_mpi(capi_phy_info_t capi_phy)
{
    
   uint32_t                             lane_mask = capi_phy.lane_mask;
   uint8_t                              idx;
   int                                  usr_input;
   capi_diag_command_request_info_t     diag_cmd, diag_cmd_rd;
   return_result_t                      return_rslt;
   float                                   usr_float_input;

   if(capi_phy.core_ip != CORE_IP_MEDIA_DSP){
       dprintf("\n *** Only Media side MPI feature\r\n");
       return;
   }
   for (idx = 0; idx < MAX_LW_LANES; ++idx) {
       if ((lane_mask & (1 << idx)) == 0)
           continue;
       capi_phy.lane_mask = (1 << idx);
       util_memset((void *)&diag_cmd_rd, 0, sizeof(capi_diag_command_request_info_t));
       diag_cmd_rd.command_id = COMMAND_ID_DIAG_GET_MEDIA_MPI_CONFIG;
       return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd_rd);
       if(return_rslt != RR_SUCCESS){
           dprintf("Get MPI config failed on lane %d with ecode %d \r\n", idx, return_rslt);
           return;
       }
       dprintf("lane %d current MPI setting is:  %s; chk_interval %d sec. chk_ratio_threshold %.2f ;\r\n", idx, 
            diag_cmd_rd.type.mpi_cfg.mpi_enable?"ENABLED":"DISABLED", diag_cmd_rd.type.mpi_cfg.mpi_interval, 
            diag_cmd_rd.type.mpi_cfg.ratio_threshold );
   }
   
   dprintf("\n** Please choose continue to config MPI information: 0-quit; 1-config\r\n");
   scanf("%d", &usr_input);
   if(usr_input==0)
       return;

   capi_phy.lane_mask = lane_mask;
   util_memset((void *)&diag_cmd, 0, sizeof(capi_diag_command_request_info_t));
   diag_cmd.command_id = COMMAND_ID_DIAG_SET_MEDIA_MPI_CONFIG;
   dprintf("\n** Please choose MPI monitor feature enable/disable: 0- disable; 1 - enable\r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mpi_cfg.mpi_enable = usr_input;
   dprintf("\n** Please choose MPI indicator update interval (unit: seconds): 0- firmware default value 1; 1~15\r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mpi_cfg.mpi_interval = usr_input;
   dprintf("\n** Please choose MPI metric threshold: 0- firmware default value 3.50; \r\n");
   scanf("%f", &usr_float_input);
   diag_cmd.type.mpi_cfg.ratio_threshold = usr_float_input;
   return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd);
   if(return_rslt != RR_SUCCESS){
       dprintf("Config MPI feature failed on lane %d with ecode %d \r\n", idx, return_rslt);
       return;
   }
   delay_ms(1000);
   for (idx = 0; idx < MAX_LW_LANES; ++idx) {
       if ((lane_mask & (1 << idx)) == 0)
           continue;
       capi_phy.lane_mask = (1 << idx);
       util_memset((void *)&diag_cmd_rd, 0, sizeof(capi_diag_command_request_info_t));
       diag_cmd_rd.command_id = COMMAND_ID_DIAG_GET_MEDIA_MPI_CONFIG;
       return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd_rd);
       if(return_rslt != RR_SUCCESS){
           dprintf("Get MPI config failed on lane %d with ecode %d \r\n", idx, return_rslt);
           return;
       }
       if(diag_cmd.type.mpi_cfg.mpi_interval==0)
           diag_cmd.type.mpi_cfg.mpi_interval = 1;
       if(diag_cmd.type.mpi_cfg.ratio_threshold==0)
           diag_cmd.type.mpi_cfg.ratio_threshold = (float)3.5;
       if(diag_cmd_rd.type.mpi_cfg.mpi_enable != diag_cmd.type.mpi_cfg.mpi_enable ||
           ( diag_cmd.type.mpi_cfg.mpi_enable && 
           (diag_cmd_rd.type.mpi_cfg.mpi_interval != diag_cmd.type.mpi_cfg.mpi_interval  ||
           diag_cmd_rd.type.mpi_cfg.ratio_threshold != diag_cmd.type.mpi_cfg.ratio_threshold))){
            dprintf("lane %d MPI config set&get mismatch enable %d vs %d; chk_interval %d vs %d sec; chk_ratio_threshold %.2f vs %.2f\r\n", idx, 
                    diag_cmd_rd.type.mpi_cfg.mpi_enable, diag_cmd.type.mpi_cfg.mpi_enable,
                    diag_cmd.type.mpi_cfg.mpi_interval, diag_cmd_rd.type.mpi_cfg.mpi_interval,
                    diag_cmd.type.mpi_cfg.ratio_threshold, diag_cmd_rd.type.mpi_cfg.ratio_threshold);
       }else{
            dprintf("lane %d MPI config successfully %s chk_interval %d sec ;chk_ratio_threshold %.2f \r\n", idx, 
                    diag_cmd_rd.type.mpi_cfg.mpi_enable?"ENABLED":"DISABLED",
                    diag_cmd_rd.type.mpi_cfg.mpi_interval, 
                    diag_cmd_rd.type.mpi_cfg.ratio_threshold );
       }
   }
   
}


void cli_config_mdia_dynamic_mpi(capi_phy_info_t capi_phy)
{
    
   uint32_t                             lane_mask = capi_phy.lane_mask;
   uint8_t                              idx;
   int                                  usr_input;
   capi_diag_command_request_info_t     diag_cmd, diag_cmd_rd;
   return_result_t                      return_rslt;

   if(capi_phy.core_ip != CORE_IP_MEDIA_DSP){
       dprintf("\n *** Only Media side MPI feature\r\n");
       return;
   }
   util_memset((void *)&diag_cmd, 0, sizeof(capi_diag_command_request_info_t));
   diag_cmd.command_id = COMMAND_ID_DIAG_SET_MEDIA_MISSION_MPI_CONFIG;
   dprintf("\n** Please choose enable or disable steady state MPI : 0 - disable 1 - enable\r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mission_mpi_cfg.mpi_enable = usr_input ? 1 : 0;   
   dprintf("\n** Please choose steady state MPI check counter, valid options are 128, 256, 512, 1024 \r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mission_mpi_cfg.mpi_chk_cnt = usr_input;
   dprintf("\n** Please choose steady state MPI check threshold: valid options are  9,10,11,12,13 and 14 \r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mission_mpi_cfg.mpi_chk_th = usr_input;

   return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd);
   if(return_rslt != RR_SUCCESS){
       dprintf("MPI steady state config failed on lane 0x%x with ecode %d \r\n", lane_mask, return_rslt);
       return;
   }
   delay_ms(200);
   for (idx = 0; idx < MAX_LW_LANES; ++idx) {
       if ((lane_mask & (1 << idx)) == 0)
           continue;
       capi_phy.lane_mask = (1 << idx);
       util_memset((void *)&diag_cmd_rd, 0, sizeof(capi_diag_command_request_info_t));
       diag_cmd_rd.command_id = COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_CONFIG;
       return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd_rd);
       if(return_rslt != RR_SUCCESS){
           dprintf("Get MPI steady state config failed on lane %d with ecode %d \r\n", idx, return_rslt);
           return;
       }
       if(diag_cmd_rd.type.mission_mpi_cfg.mpi_enable != diag_cmd.type.mission_mpi_cfg.mpi_enable ||
           diag_cmd_rd.type.mission_mpi_cfg.mpi_chk_cnt != diag_cmd.type.mission_mpi_cfg.mpi_chk_cnt ||
           diag_cmd_rd.type.mission_mpi_cfg.mpi_chk_th != diag_cmd.type.mission_mpi_cfg.mpi_chk_th ){
            dprintf("lane %d MPI steady state config set&get mismatch mpi_enable %d vs %d mpi_chk_cnt %d vs %d; mpi_chk_th %d vs %d; \r\n", idx, 
                    diag_cmd_rd.type.mission_mpi_cfg.mpi_enable, diag_cmd.type.mission_mpi_cfg.mpi_enable, 
                    diag_cmd_rd.type.mission_mpi_cfg.mpi_chk_cnt, diag_cmd.type.mission_mpi_cfg.mpi_chk_cnt, 
                    diag_cmd_rd.type.mission_mpi_cfg.mpi_chk_th, diag_cmd.type.mission_mpi_cfg.mpi_chk_th);
       }else{
            dprintf("lane %d MPI config successfully mpi_enable %s chk_cnt %d ; chk_th %d ;\r\n", idx, 
                    diag_cmd_rd.type.mission_mpi_cfg.mpi_enable?"ENABLED":"DISABLED",
                    diag_cmd_rd.type.mission_mpi_cfg.mpi_chk_cnt, diag_cmd_rd.type.mission_mpi_cfg.mpi_chk_th);
       }
   }
}


void cli_get_media_mpi_state(capi_phy_info_t capi_phy)
{
   uint32_t                            tidx, ttimes, lane_mask = capi_phy.lane_mask;
   uint8_t                              idx, lidx;
   int                                  usr_input;
   capi_diag_command_request_info_t     diag_cmd;
   return_result_t                      return_rslt;

   if(capi_phy.core_ip != CORE_IP_MEDIA_DSP){
       dprintf("\n *** Only Media side MPI feature\r\n");
       return;
   }
   util_memset((void *)&diag_cmd, 0, sizeof(capi_diag_command_request_info_t));
   dprintf("\n** Please choose read MPI check dust indicator option: 0-read only; 1-read & clear\r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mpi_st.read_clear = usr_input;
   dprintf("\n** Please input read MPI state times: \r\n");
   scanf("%d", &usr_input);
   ttimes = usr_input;
   for (tidx = 0; tidx < ttimes; ++tidx) {
       for (idx = 0; idx < MAX_LW_LANES; ++idx) {
           if ((lane_mask & (1 << idx)) == 0)
               continue;
           capi_phy.lane_mask = (1 << idx);
           diag_cmd.command_id = COMMAND_ID_DIAG_GET_MEDIA_MPI_STATE;
           return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd);
           if(return_rslt != RR_SUCCESS){
               dprintf("Get MPI state failed on lane %d with ecode %d \r\n", idx, return_rslt);
               return;
           }else{
               dprintf("\r\nLane %d MPI state present dust_indicator %d dust_indicator_sticky %d \r\n", idx, 
                   diag_cmd.type.mpi_st.dust_indicator, diag_cmd.type.mpi_st.dust_indicator_sticky);
               for(lidx=0; lidx<4; lidx++)
                   dprintf("Normalized standard deviation of level %d -- %.2f \r\n", lidx, diag_cmd.type.mpi_st.mpi_normalized_s[lidx]);
               dprintf("Slope of the linear fit of the four normalized variances -- %.2f \r\n", diag_cmd.type.mpi_st.mpi_slope);
               dprintf("Intercept of the linear fit of the four normalized variances -- %.2f \r\n", diag_cmd.type.mpi_st.mpi_intercept);
               dprintf("MPI metric -- %.2f \r\n", diag_cmd.type.mpi_st.mpi_metric);
           }
       }
       dprintf("\r\n");
       delay_s(1);
   }
}




void cli_get_media_dynamic_mpi_state(capi_phy_info_t capi_phy)
{
   uint32_t                             lane_mask = capi_phy.lane_mask;
   uint8_t                              idx;
   int                                  usr_input;
   capi_diag_command_request_info_t     diag_cmd;
   return_result_t                      return_rslt;

   if(capi_phy.core_ip != CORE_IP_MEDIA_DSP){
       dprintf("\n *** Only Media side MPI feature\r\n");
       return;
   }
   util_memset((void *)&diag_cmd, 0, sizeof(capi_diag_command_request_info_t));
   dprintf("\n** Please choose choose read steady state MPI check dust indicator option: 0-read only; 1-read & clear\r\n");
   scanf("%d", &usr_input);
   diag_cmd.type.mpi_st.read_clear = usr_input;
   for (idx = 0; idx < MAX_LW_LANES; ++idx) {
       if ((lane_mask & (1 << idx)) == 0)
           continue;
       capi_phy.lane_mask = (1 << idx);
       diag_cmd.command_id = COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_STATE;
       return_rslt = capi_diag_command_request_info(&capi_phy, &diag_cmd);
       if(return_rslt != RR_SUCCESS){
           dprintf("Get MPI steady state failed on lane %d with ecode %d \r\n", idx, return_rslt);
           return;
       }else{
           dprintf("lane %d MPI steady state present dust_indicator %d dust_indicator_sticky %d \r\n", idx, 
               diag_cmd.type.mpi_st.dust_indicator, diag_cmd.type.mpi_st.dust_indicator_sticky);
       }
   }
}

static void cli_serdes_set_lane_cdr_mode(capi_phy_info_t * phy_info_ptr)
{
    return_result_t return_result;
    int usr_input;
    capi_chip_command_info_t capi_chip_command;
    util_memset(&capi_chip_command, 0, sizeof(capi_chip_command_info_t));

    dprintf("Please choose from the following options:\r\n");
    dprintf("0 : Force OS-CDR mode\r\n");
    dprintf("1 : Disable Force OS-CDR mode\r\n");
    dprintf("2 : Force BR-CDR mode\r\n");
    dprintf("3 : Disable Force BR-CDR mode\r\n");
    dprintf("4 : Disable Force CDR modes\r\n");
    scanf("%d", &usr_input);
    if (usr_input < 0 || usr_input > 4) {
        return;
    }

    capi_chip_command.command_id = COMMAND_ID_SET_SERDES_LANE_CDR_MODE;
    capi_chip_command.type.serdes_lane_cdr_mode = (capi_lane_cdr_mode_t) usr_input;
    return_result = capi_set_chip_command(phy_info_ptr, &capi_chip_command);
    if (return_result) {
        dprintf("Failed to set serdes cdr mode on lane mask 0x%x\r\n", phy_info_ptr->lane_mask);
    }
    else {
        dprintf("Successfully set serdes cdr mode on lane mask 0x%x\r\n", phy_info_ptr->lane_mask);
    }
}

static void cli_serdes_get_lane_cdr_mode(capi_phy_info_t * phy_info_ptr)
{
    uint8_t idx;
    return_result_t return_result;
    capi_phy_info_t capi_phy = *phy_info_ptr;
    capi_chip_command_info_t capi_chip_command;

    for (idx = 0; idx < MAX_CLIENT_LANES; ++idx) {
        if (phy_info_ptr->lane_mask & (1 << idx)) {
            capi_phy.lane_mask = 1 << idx;
            util_memset(&capi_chip_command, 0, sizeof(capi_chip_command_info_t));
            capi_chip_command.command_id = COMMAND_ID_GET_SERDES_LANE_CDR_MODE;

            return_result = capi_get_chip_command(&capi_phy, &capi_chip_command);
            if (return_result) {
                dprintf("Failed to retrieve serdes cdr mode on lane %d, result code = 0x%x\r\n", idx, return_result);
            }
            else {
                dprintf("serdes lane %d, cdr mode: %d\r\n", idx, capi_chip_command.type.serdes_lane_cdr_mode);
            }
        }
    }
}

static void cli_serdes_get_diag_info(capi_phy_info_t * phy_info_ptr)
{
    uint8_t idx;
    capi_phy_info_t capi_phy = *phy_info_ptr;
    capi_diag_command_request_info_t diag_command_request_info;
    return_result_t return_result;

    for (idx = 0; idx < MAX_CLIENT_LANES; ++idx) {
        if (phy_info_ptr->lane_mask & (1 << idx)) {
            capi_phy.lane_mask = 1 << idx;
            util_memset(&diag_command_request_info, 0, sizeof(diag_command_request_info));
            diag_command_request_info.command_id = COMMAND_ID_DIAG_GET_SERDES_DIAG_INFO;
            return_result = capi_diag_command_request_info(&capi_phy, &diag_command_request_info);
            if (return_result) {
                dprintf("\r\n *** Failed to retrieve serdes diagnostic info on lane %d! Return result = 0x%x *** \r\n", idx, return_result);
            }
            else {
                dprintf("\r\n *** Successfully retrieved serdes diagnotics info on lane %d! *** \r\n", idx);
                dprintf("\tsignal detect = 0x%x\r\n", diag_command_request_info.type.serdes_diag_info.signal_detect);
                dprintf("\tcdr lock = 0x%x\r\n", diag_command_request_info.type.serdes_diag_info.rx_lock);
                dprintf("\tmodulation = 0x%x\r\n", diag_command_request_info.type.serdes_diag_info.pmd_mode);
                dprintf("\tosr = 0x%x\r\n", diag_command_request_info.type.serdes_diag_info.osr_mode);
                dprintf("\tdc offset = %d\r\n", diag_command_request_info.type.serdes_diag_info.dc_offset);
                dprintf("\tlink_time = %d\r\n", diag_command_request_info.type.serdes_diag_info.link_time);
                dprintf("\tpf main value = %d\r\n", diag_command_request_info.type.serdes_diag_info.pf_main);
                dprintf("\tdfe1 value = %d\r\n", diag_command_request_info.type.serdes_diag_info.dfe1);
                dprintf("\tdfe2 value = %d\r\n", diag_command_request_info.type.serdes_diag_info.dfe2);
                dprintf("\tdfe3 value = %d\r\n", diag_command_request_info.type.serdes_diag_info.dfe3);
                dprintf("\tdfe4 value = %d\r\n", diag_command_request_info.type.serdes_diag_info.dfe4);
                dprintf("\tdfe5 value = %d\r\n", diag_command_request_info.type.serdes_diag_info.dfe5);
                dprintf("\tdfe6 value = %d\r\n", diag_command_request_info.type.serdes_diag_info.dfe6);
                dprintf("\trx ppm = %d\r\n", diag_command_request_info.type.serdes_diag_info.rx_ppm);
                dprintf("\ttx ppm = %d\r\n", diag_command_request_info.type.serdes_diag_info.tx_ppm);
                dprintf("\tleft_eye_mui = %d\r\n", diag_command_request_info.type.serdes_diag_info.eye_margin_estimate.left_eye_mui);
                dprintf("\tright_eye_mui = %d\r\n", diag_command_request_info.type.serdes_diag_info.eye_margin_estimate.right_eye_mui);
                dprintf("\tupper_eye_mv = %d\r\n", diag_command_request_info.type.serdes_diag_info.eye_margin_estimate.upper_eye_mv);
                dprintf("\tlower_eye_mv = %d\r\n", diag_command_request_info.type.serdes_diag_info.eye_margin_estimate.lower_eye_mv);
                dprintf("\tTxFIR:\r\n");
                dprintf("\tpre2 = %d\r\n", diag_command_request_info.type.serdes_diag_info.txfir_pre2);
                dprintf("\tpre1 = %d\r\n", diag_command_request_info.type.serdes_diag_info.txfir_pre);
                dprintf("\tmain = %d\r\n", diag_command_request_info.type.serdes_diag_info.txfir_main);
                dprintf("\tpost1 = %d\r\n", diag_command_request_info.type.serdes_diag_info.txfir_post1);
                dprintf("\tpost2 = %d\r\n", diag_command_request_info.type.serdes_diag_info.txfir_post2);
                dprintf("\tpost3 = %d\r\n", diag_command_request_info.type.serdes_diag_info.txfir_post3);  
            }
        }
    }
}


void capi_test_top (capi_phy_info_t* phy_info_ptr)
{
    capi_test(phy_info_ptr, CAPI_TEST_MAX);
}

return_result_t capi_test(capi_phy_info_t * phy_info_ptr, uint8_t test_case)
{
    int ii;
    capi_phy_info_t capi_phy = {0};
    capi_reg_info_t reg_info;
    capi_prbs_info_t capi_prbs;
    capi_polarity_info_t capi_polarity;
    int idx;
    int poly;
    int restart;

    capi_prbs_status_t capi_prbs_status;
    capi_chip_info_t chip_info;
    return_result_t  return_result;
    int usr_input;
    char core_ip[8];
    uint8_t max_lanes;


    util_memcpy(&capi_phy, phy_info_ptr, sizeof(capi_phy));


    util_memset(&chip_info, 0, sizeof(capi_chip_info_t));
    chip_info.param.is.hw_info = 1;
    return_result = capi_get_chip_info(&capi_phy, &chip_info);
    if (return_result != RR_SUCCESS) {
        dprintf("\nFailed to read chip info!!\r\n");
    }
    do
    {
        if (test_case >= CAPI_TEST_MAX) {
            dprintf("\n*** Please choose from following test items ......\r\n");
            dprintf("\n\nPlease choose core IP:\r\n");
            dprintf("0: Host Core IP\r\n");
            dprintf("1: Media Core IP\r\n");
            dprintf("2: BACK_TO_PREV_MENU\r\n");
            scanf("%d", &usr_input);
            if(usr_input >= 2)
                break; /* Break out of while loop */

            capi_phy.core_ip = usr_input ? CORE_IP_MEDIA_DSP: CORE_IP_HOST_SERDES;
            util_memcpy(core_ip, usr_input ? "Media" : "Host", sizeof(core_ip));
            max_lanes = usr_input ? MAX_LW_LANES : MAX_CLIENT_LANES;
            for (idx = 0; idx < CAPI_TEST_MAX; idx++) {
                dprintf("** %2d -- %s\r\n", idx, capi_test_description[idx]);
            }
            scanf("%d", &usr_input);
            if(usr_input == BACK_TO_PREV_MENU)
                break; /* Break out of while loop */

            if (usr_input >= CAPI_TEST_MAX)
                test_case = CAPI_TEST_MAX;
            else
                test_case = (uint8_t) usr_input;

            if(test_case != CAPI_TEST_MAX){
                dprintf("\n\nPlease choose channel ID:\r\n");
                dprintf("0: all channels\r\n");
                for (idx = 1; idx <= max_lanes; ++ idx) {
                    dprintf("%d: channel %d\r\n", idx, idx - 1);
                }
                dprintf("%d: user specified lane mask\r\n", idx);

                scanf("%d", &usr_input);
                if (usr_input > 0 && usr_input <= max_lanes)  {
                    capi_phy.lane_mask = 1 << (usr_input - 1);
                } 
                else if (usr_input == max_lanes + 1)  {
                    dprintf("\n** Please input lane mask in hex\r\n");
                    scanf("%x", &usr_input);
                    capi_phy.lane_mask = (uint32_t) usr_input;
                } 
                else {
                    dprintf("\n**Choose all %s lanes %d\r\n", core_ip, usr_input);
                    capi_phy.lane_mask = 0xF;
                }
            }else
                capi_phy.lane_mask = 0xF;
        }

        switch (test_case) 
        {
            case CAPI_PRBS_GEN:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask, option;
                char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13", "SQRWAVE"};
                dprintf("\n**Input: PRBS (0), SSPRQ (1), QPRBS13 (2) or SQRWAVE (3):");
                scanf("%d", &option);
                if (option == 0)
                    util_get_poly(&capi_phy, &poly);

                if (option == 3) {
                    if (capi_phy.core_ip == CORE_IP_MEDIA_DSP) {
                        dprintf("\n** Please choose square wave type: 0- 1 consecutive 1s; 1- 2 consecutive 1s;2- 4 consecutive 1s;3- 8 consecutive 1s; 4- 16 consecutive 1s \r\n");
                        scanf("%d", &usr_input);
                    } else {
                        dprintf("SQRWAVE is only supported with CORE_IP_MEDIA_DSP !\r\n");
                        break;
                    }
                }
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;

                    dprintf("\n** Turn on %s %s gen on lane %d\r\n",core_ip, &patt_gen_mon_type[option], idx);
                    capi_phy.lane_mask = 1 << idx;

                    util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));
                    if (option == 0)
                        capi_prbs.ptype = CAPI_PRBS_GENERATOR;
                    else if (option == 1)
                        capi_prbs.ptype = CAPI_PRBS_SSPRQ_GENERATOR;
                    else if (option == 2)
                        capi_prbs.ptype = CAPI_PRBS_Q_PRBS_13_GENERATOR;
                    else {
                        capi_prbs.ptype = CAPI_PRBS_SQUARE_WAVE_GENERATOR;
                        capi_prbs.op.sqr_wave.ptrn_wave_type = (square_wave_pattern_t)usr_input;
                    }
                    capi_prbs.gen_switch = CAPI_SWITCH_ON;
                    if (option == 0) {
                        if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                            capi_prbs.op.pcfg.poly.bh_poly = (uint8_t) poly; // capi_bh_prbs_poly_t
                        else
                            capi_prbs.op.pcfg.poly.poly_type = (uint8_t) poly; /* capi_prbs_poly_type_t */
                    }

                    return_result = capi_set_prbs_info(&capi_phy, &capi_prbs);
                    if (return_result != RR_SUCCESS) {
                        dprintf ("%s side capi_set_prbs_info_failed\r\n", core_ip);
                        break;
                    }
                }
            }
            break;

            case CAPI_PRBS_CHK:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask, option, auto_det;
                char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13"};
                uint8_t try_polarity = 1;
                uint8_t polarity = 0;
                capi_phy_info_t capi_phy_bk;
                util_memcpy((void*)&capi_phy_bk, &capi_phy, sizeof(capi_phy_info_t));

                restart = 1;

                dprintf("\n**Input: PRBS (0), SSPRQ (1) or QPRBS13 (2):");
                scanf("%d", &option);
                if ((option == 1) &&
                    (capi_phy_bk.core_ip == CORE_IP_HOST_SERDES || capi_phy_bk.core_ip == CORE_IP_HOST_DSP)) {
                    dprintf("\n*********HOST side SSPRQ monitor is not supported!\r\n");
                    break;
                }
                if ((option == 0) &&
                    (capi_phy_bk.core_ip == CORE_IP_HOST_DSP || capi_phy_bk.core_ip == CORE_IP_MEDIA_DSP)) {
                    dprintf("\n** Reconfig PMON auto lock: 0 => No, 1 => Yes \r\n");
                    scanf("%d", &auto_det);
                    if (auto_det) {
                        dprintf("\n** Turn on/off PMON auto lock: 0 => OFF, 1 => ON \r\n");
                        scanf("%d", &auto_det);
                    } else {
                        /* enable auto lock by default */
                        auto_det = 1;
                    }
                }
                dprintf("\n** Re-start %s_PMON : 0 => No, 1 => Yes \r\n", &patt_gen_mon_type[option]);
                scanf("%d", &restart);

                if (restart && option == 0) {
                    util_get_poly(&capi_phy_bk, &poly);
                }

                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    try_polarity=1;
                    polarity = 0;

                    capi_phy_bk.lane_mask = 1 << idx;

                    while(try_polarity)
                    {
                        if(polarity) {
                            swap_rx_polarity(&capi_phy_bk, idx);
                        }

                        if (restart) 
                        {
                            dprintf("\n** Turn on %s %s mon on lane %d\r\n", core_ip,  &patt_gen_mon_type[option], idx);

                            util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));
                            if (option == 0)
                                capi_prbs.ptype = CAPI_PRBS_MONITOR;
                            else if (option == 1)
                                capi_prbs.ptype = CAPI_PRBS_SSPRQ_MONITOR;
                            else
                                capi_prbs.ptype = CAPI_PRBS_Q_PRBS_13_MONITOR;
                            capi_prbs.gen_switch = CAPI_SWITCH_ON;
                            if (option == 0) {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                                    capi_prbs.op.pcfg.poly.bh_poly = (capi_bh_prbs_poly_t)poly;
                                else {
                                    capi_prbs.op.pcfg.poly.poly_type = (capi_prbs_poly_type_t)poly;
                                    capi_prbs.op.pcfg.lw_checker_auto_det = auto_det;
                                }
                            }
                            capi_prbs.op.pcfg.rx_invert = 0;
                            capi_prbs.op.pcfg.tx_invert = 0;
                            return_result = capi_set_prbs_info(&capi_phy_bk, &capi_prbs);
                            if (return_result != RR_SUCCESS) {
                                dprintf ("%s side capi_set_prbs_info_failed\r\n", core_ip);
                                break;
                            }
                        }

                        // Configure is done, now do checking....
                        dprintf("start check %s %s_MON status\r\n", core_ip, &patt_gen_mon_type[option]);
                        {
                            prbs_err_ber_wlock_s local_ber = {0};
                            if (get_prbs_ber(&capi_phy_bk, &local_ber, 500, 5, option) != RR_SUCCESS)
                                dprintf("%s get %s ber failed\n", core_ip, &patt_gen_mon_type[option]);
                            capi_prbs_status.lock = local_ber.pmon_lock;
                        }
                        if (capi_prbs_status.lock) {
                            // pmon locked, mark pmon check completed
                            try_polarity = 0;
                        }
                        else {
                            if (polarity) {

                                // pmon lock failed after polarity swap,
                                // mark pmon check completed
                                try_polarity = 0;
                                // swap the polarity back
                                swap_rx_polarity(&capi_phy_bk, idx);
                            }
                            else {
                                // do priority swap, then check it again
                                dprintf("\n Swap PMON polarity, try again.... \r\n\n");
                                polarity = 1;
                            }
                        }
                    }
                }
            }
            break;

            case CAPI_GET_PRBS_INFO:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask, option;
                char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13", "SQRWAVE"};
                dprintf("\n**Input: PRBS (0), SSPRQ (1), QPRBS13 (2) or SQRWAVE (3):");
                scanf("%d", &option);
                if (option == 3 && capi_phy.core_ip != CORE_IP_MEDIA_DSP) {
                        dprintf("SQRWAVE is only supported with CORE_IP_MEDIA_DSP !\r\n");
                        break;
                }
                dprintf("\n** Get %s %s INFO \r\n", core_ip, &patt_gen_mon_type[option]);
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);
                    dprintf("\n\n** %s %s_GEN on lane %d\r\n", core_ip, &patt_gen_mon_type[option], idx);
                    util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));
                    if (option == 0)
                        capi_prbs.ptype = CAPI_PRBS_GENERATOR;
                    else if (option == 1)
                        capi_prbs.ptype = CAPI_PRBS_SSPRQ_GENERATOR;
                    else if (option == 2)
                        capi_prbs.ptype = CAPI_PRBS_Q_PRBS_13_GENERATOR;
                    else
                        capi_prbs.ptype = CAPI_PRBS_SQUARE_WAVE_GENERATOR;
                    capi_get_prbs_info(&capi_phy, &capi_prbs);
                    if (option == 0) {
                        if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                            dprintf("capi_prbs.op.pcfg.poly.bh_poly =  %d\r\n", capi_prbs.op.pcfg.poly.bh_poly);
                        else
                            dprintf("capi_prbs.op.pcfg.poly.poly_type =  %d\r\n", capi_prbs.op.pcfg.poly.poly_type);
                    }
                    if (option == 3) {
                        dprintf("capi_prbs.op.sqr_wave.ptrn_wave_type = %d\r\n",capi_prbs.op.sqr_wave.ptrn_wave_type);
                        dprintf("capi_prbs.gen_switch = %d\r\n", capi_prbs.gen_switch);
                        continue;
                    }
                    dprintf("capi_prbs.gen_switch = %d\r\n", capi_prbs.gen_switch);

                    dprintf("\n** %s %s_MON on lane %d\r\n", core_ip, &patt_gen_mon_type[option], idx);
                    util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));
                    if (option == 0)
                        capi_prbs.ptype = CAPI_PRBS_MONITOR;
                    else if (option == 1)
                        capi_prbs.ptype = CAPI_PRBS_SSPRQ_MONITOR;
                    else
                        capi_prbs.ptype = CAPI_PRBS_Q_PRBS_13_MONITOR;
                    capi_get_prbs_info(&capi_phy, &capi_prbs);
                    if (option == 0) {
                        if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                            dprintf("capi_prbs.op.pcfg.poly.bh_poly =  %d\r\n", capi_prbs.op.pcfg.poly.bh_poly);
                        else {
                            dprintf("capi_prbs.op.pcfg.poly.poly_type =  %d\r\n", capi_prbs.op.pcfg.poly.poly_type);
                            dprintf("capi_prbs.op.pcfg.lw_checker_auto_det =  %d\r\n", capi_prbs.op.pcfg.lw_checker_auto_det);
                        }
                    }
                    dprintf("capi_prbs.gen_switch = %d\r\n", capi_prbs.gen_switch);
                }
            } break;

            case CAPI_PRBS_DISABLE:
            {
                int option;
                char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13", "SQRWAVE"};
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                dprintf("\n**Input: PRBS (0), SSPRQ (1), QPRBS13 (2) or SQRWAVE (3):");
                scanf("%d", &option);
                if (option == 3 && capi_phy.core_ip != CORE_IP_MEDIA_DSP) {
                        dprintf("SQRWAVE is only supported with CORE_IP_MEDIA_DSP !\r\n");
                        break;
                }
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    //disable PRBS
                    capi_phy.lane_mask = 1 << idx;
                    if (option != 3) {
                        util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));
                        if (option == 0)
                            capi_prbs.ptype = CAPI_PRBS_MONITOR;
                        else if (option == 1)
                            capi_prbs.ptype = CAPI_PRBS_SSPRQ_MONITOR;
                        else
                            capi_prbs.ptype = CAPI_PRBS_Q_PRBS_13_MONITOR;

                        capi_prbs.gen_switch = CAPI_SWITCH_OFF;

                        capi_set_prbs_info(&capi_phy, &capi_prbs);
                        dprintf("\n** Turn OFF %s side %s CHECKER on lane %d\r\n", &patt_gen_mon_type[option], core_ip, idx);
                    }
                    util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));
                    if (option == 0)
                        capi_prbs.ptype = CAPI_PRBS_GENERATOR;
                    else if (option == 1)
                        capi_prbs.ptype = CAPI_PRBS_SSPRQ_GENERATOR;
                    else if (option == 2)
                        capi_prbs.ptype = CAPI_PRBS_Q_PRBS_13_GENERATOR;
                    else
                        capi_prbs.ptype = CAPI_PRBS_SQUARE_WAVE_GENERATOR;
                    capi_prbs.gen_switch = CAPI_SWITCH_OFF;

                    capi_set_prbs_info(&capi_phy, &capi_prbs);
                    dprintf("\n** Turn OFF %s side %s GEN on lane %d\r\n", &patt_gen_mon_type[option], core_ip, idx);
                }
            } break;

            case CAPI_PRBS_CLEAR:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask, option;
                char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13"};
                capi_prbs_status_t prbs_st = {0};

                dprintf("\n**Input: PRBS (0), SSPRQ (1) or QPRBS13 (2):");
                scanf("%d", &option);
                if (option == 0)
                    prbs_st.prbs_type = CAPI_PRBS_MONITOR;
                else if (option == 1)
                    prbs_st.prbs_type = CAPI_PRBS_SSPRQ_MONITOR;
                else
                    prbs_st.prbs_type = CAPI_PRBS_Q_PRBS_13_MONITOR;
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = 1 << idx;
                    return_result = capi_clear_prbs_status(&capi_phy, &prbs_st);
                    if (return_result == RR_SUCCESS)
                        dprintf("\n**%s stats cleared on %s side on lane %d\r\n", &patt_gen_mon_type[option], core_ip, idx);
                    else
                        dprintf("\n**Error: %s stats clear failed on %s side on lane %d\r\n", &patt_gen_mon_type[option], core_ip, idx);
                }
            }
            break;

            case CAPI_PRBS_INJECT_ERROR:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                capi_prbs_err_inject_t inj_err;
                int err_cnt, option, patt_type;
                char patt_gen_mon_type[][8]={"PRBS", "SSPRQ", "QPRBS13"};
                dprintf("\n**Input inject error count:");
                scanf("%d", &err_cnt);
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = 1 << idx;

                    inj_err.enable = 1;
                    inj_err.inject_err_num = err_cnt;
                    dprintf("\n**Input err inj option: PRBS (0), SSPRQ (1) or QPRBS13 (2):");
                    scanf("%d", &patt_type);
                    if (patt_type != 0) {
                        if (patt_type == 1)
                            inj_err.ptype = CAPI_PRBS_SSPRQ_GENERATOR;
                        else
                            inj_err.ptype = CAPI_PRBS_Q_PRBS_13_GENERATOR;
                        dprintf("\n**Input err inj type LSB(1) or MSB(2) or both(3):");
                        scanf("%d", &option);
                        inj_err.err_type = option;
                    } else if (capi_phy.core_ip == CORE_IP_MEDIA_DSP) {
                        inj_err.ptype = CAPI_PRBS_MONITOR;
                        inj_err.err_type = PRBS_ERR_INJ_LSB;
                    }
                    return_result = capi_prbs_inject_error(&capi_phy, &inj_err);
                    if (return_result == RR_SUCCESS)
                        dprintf("\n**%s inject error on %s side on lane %d\r\n", &patt_gen_mon_type[patt_type], core_ip, idx);
                    else
                        dprintf("\n**Error: %s inject error failed on %s side on lane %d\r\n", &patt_gen_mon_type[patt_type], core_ip, idx);
                }
            }
            break;

            case CAPI_CHANGE_POLARITY:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask;

                //Fill in the polarity type and config
                dprintf("\n\nPlease choose polarity direction: 0 - Ingress; 1 - Egress\r\n");
                scanf("%d", &usr_input);
                if (usr_input < 0 || usr_input > 1) {
                    dprintf("\n\nWrong polarity direction %d\r\n", usr_input);
                    break;
                }
                capi_polarity.direction = usr_input;

                dprintf("\n\nPlease choose polarity operation: 0 - Default Polarity; 1 - Default Polarity inverted; 2 - Swap Current Polarity\r\n");
                scanf("%d", &usr_input);
                if (usr_input < 0 || usr_input > 2) {
                    dprintf("\n\nWrong polarity action: %d  \r\n", usr_input);
                    break;
                }
                capi_polarity.action = usr_input;

                if (capi_set_polarity(&capi_phy, &capi_polarity) != RR_SUCCESS)
                    dprintf("\n\n*****  Config %s Polarity FAILED ! ***** \r\n", core_ip);
                else
                    dprintf("\n\n*****  Config %s Polarity SUCCESSFUL ! ***** \r\n", core_ip);


                // Read back polarity
                dprintf("\n\nRead back polarity to check\n");
                for (ii = 0; ii < max_lanes; ii++) {
                    if ((lane_mask_bk & (1 << ii)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << ii);
                    util_memset((void*)&capi_polarity, 0, sizeof(capi_polarity_info_t));
                    capi_polarity.direction = DIR_EGRESS;
                    return_result = capi_get_polarity(&capi_phy, &capi_polarity);
                    if (return_result == RR_SUCCESS)
                        dprintf("\n Get_Lane_%d egress polarity = %d, ", ii, capi_polarity.action);
                    else {
                        dprintf("\n Get_Lane_%d egress polarity failed with return code %d, \r\n", ii, return_result);
                        break;
                    }
                }

                dprintf("\n\n");
                for (ii = 0; ii < max_lanes; ii++) {
                    if ((lane_mask_bk & (1 << ii)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << ii);
                    util_memset((void*)&capi_polarity, 0, sizeof(capi_polarity_info_t));
                    capi_polarity.direction = DIR_INGRESS;

                    return_result = capi_get_polarity(&capi_phy, &capi_polarity);
                    if (return_result == RR_SUCCESS)
                        dprintf("\n Get Lane_%d ingress polarity = %d, ", ii, capi_polarity.action);
                    else {
                        dprintf("\n Get Lane_%d ingress polarity failed with return code %d \r\n", ii, return_result);
                        break;
                    }
                }
            } break;

            case CAPI_SET_TX_INFO:
            {
                uint32_t lane_mask, i;
                int modulation;
                capi_lane_config_info_t lane_config_info = {0};
                uint8_t tap_idx;

                lane_mask = capi_phy.lane_mask;
                lane_config_info.lane_config_type = LANE_CONFIG_TYPE_LANE_TX_INFO;

                dprintf("Configure symbol swap? 0 - No; 1 - Yes\r\n");
                scanf("%d", &usr_input);
                if (usr_input) {
                    lane_config_info.type.lane_tx_info.param.is.symbol_swap = (uint8_t) usr_input;
                    dprintf("0 - Disable symbol swap ; 1 - Enable symbol swap\r\n");
                    scanf("%d", &usr_input);
                    lane_config_info.type.lane_tx_info.value.symbol_swap = (uint8_t) usr_input;
                }
                dprintf("Configure TxFiR? 0 - No; 1 - Yes\r\n");
                scanf("%d", &usr_input);
                lane_config_info.type.lane_tx_info.param.is.txfir = (uint8_t) usr_input;
                if (lane_config_info.type.lane_tx_info.param.is.txfir) {
                    if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES) {
                        dprintf("Number of taps? 0 - 3 taps; 1 - 6 taps \r\n");
                        scanf("%d", &usr_input);
                        dprintf("Modulation? 0 - NRZ; 1 - PAM \r\n");
                        scanf("%d", &modulation);
                        if (modulation) {
                            lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps = usr_input?CAPI_PAM4_6TAP:CAPI_PAM4_LP_3TAP;
                        } else {
                            lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps = usr_input?CAPI_NRZ_6TAP:CAPI_NRZ_LP_3TAP;
                        }
                    }
                    
                    if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                        dprintf("Number of taps? 0 - 6 taps; 1 - 7 taps; 2- 12 taps \r\n");
                        scanf("%d", &usr_input);
                        if(usr_input>2)
                            usr_input = 0;
                        switch(usr_input){
                        case 0: /*6 TAP*/
                            dprintf("\n 6 TAPS: Enter modulation? 0 - NRZ; 1 - PAM \r\n");
                            scanf("%d", &modulation);
                            lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps = modulation?CAPI_PAM4_6TAP:CAPI_NRZ_6TAP;
                            dprintf("Please enter value for pre2\r\n");
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.txfir.tap[0] = (int16_t) usr_input;
                            dprintf("Please enter value for pre1\r\n");
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.txfir.tap[1] = (int16_t) usr_input;
                            dprintf("Please enter value for main\r\n");
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.txfir.tap[2] = (int16_t) usr_input;
                            dprintf("Please enter value for post1\r\n");
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.txfir.tap[3] = (int16_t) usr_input;
                            dprintf("Please enter value for post2\r\n");
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.txfir.tap[4] = (int16_t) usr_input;
                            dprintf("Please enter value for post3\r\n");
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.txfir.tap[5] = (int16_t) usr_input;
                        break;
                        case 1: /*7 TAP*/
                            lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps = TXFIR_TAPS_7TAP;
                            for (tap_idx = 0; tap_idx < 7; tap_idx++) {
                                dprintf("\nPlease config TX FIR 7 TAPS, tap %d \r\n", tap_idx);
                                scanf("%d", &usr_input);
                                lane_config_info.type.lane_tx_info.value.txfir.tap[tap_idx] = (int16_t)usr_input;
                            }
                        break;
                        case 2: /*12 TAP*/
                            lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps = TXFIR_TAPS_12TAP;
                            for (tap_idx = 0; tap_idx < 12; tap_idx++) {
                                dprintf("\nPlease config TX FIR 12 TAPS, tap %d \r\n", tap_idx);
                                scanf("%d", &usr_input);
                                lane_config_info.type.lane_tx_info.value.txfir.tap[tap_idx] = (int16_t)usr_input;
                            }
                        break;
                        }
                    }
                }
                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                    dprintf("Configure dsp level_shift? 0 - No; 1 - Yes\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input) {
                        lane_config_info.type.lane_tx_info.param.is.dsp_level_shift = 1;
                        for (i=0;i<4;i++) {
                            dprintf("level_shift[%d]:\r\n", i);
                            scanf("%d", &usr_input);
                            lane_config_info.type.lane_tx_info.value.dsp_level_shift[i] = (int8_t) usr_input;
                        }
                    }
                    dprintf("Configure dsp graycode? 0 - No; 1 - Yes\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input) {
                        lane_config_info.type.lane_tx_info.param.is.dsp_graycode = 1;
                        dprintf("0 - Disable graycode ; 1 - Enable graycode\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_tx_info.value.dsp_graycode = (uint8_t) usr_input;
                    }
                    dprintf("Configure dsp precode? 0 - No; 1 - Yes\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input) {
                        lane_config_info.type.lane_tx_info.param.is.dsp_precode = 1;
                        dprintf("0 - User firmware default value ; 1 - Force TX precode OFF; 2- Force TX precode ON\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_tx_info.value.dsp_precode = (uint8_t) usr_input;
                    }
                }
                for (idx = 0; idx < max_lanes; ++idx) {
                    if ((lane_mask & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);

                    return_result = capi_set_lane_config_info(&capi_phy, &lane_config_info);
                    if (return_result != RR_SUCCESS) {
                        dprintf("\n\n ***** Failed to set %s lane config info with return code %d! ***** \r\n", core_ip, return_result);
                    } else {
                        dprintf("\n\n ***** Successfully set %s lane config info! ***** \r\n", core_ip);
                    }
                }
            }
            break;

            case CAPI_GET_TX_INFO:
            {
                uint32_t lane_mask, i;
                capi_lane_config_info_t lane_config_info = {0};
                uint8_t tap_idx, tap_max;
                char txfir_type[][20]={"NRZ_LP_3TAP", "NRZ_6TAP", "PAM4_LP_3TAP", "PAM4_6TAP", "7TAP", "12TAP"};
    
                lane_mask = capi_phy.lane_mask;
                lane_config_info.lane_config_type = LANE_CONFIG_TYPE_LANE_TX_INFO;

                for (idx = 0; idx < max_lanes; ++idx) {
                    if ((lane_mask & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);

                    util_memset(&(lane_config_info.type.lane_tx_info), 0, sizeof(lane_tx_info_t));

                    lane_config_info.type.lane_tx_info.param.is.txfir = 1;
                    lane_config_info.type.lane_tx_info.param.is.symbol_swap = 1;

                    if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                        lane_config_info.type.lane_tx_info.param.is.dsp_level_shift = 1;
                        lane_config_info.type.lane_tx_info.param.is.dsp_graycode = 1;
                        lane_config_info.type.lane_tx_info.param.is.dsp_precode = 1;
                    }

                    return_result = capi_get_lane_config_info(&capi_phy, &lane_config_info);

                    if (return_result != RR_SUCCESS) {
                        dprintf("\n\n ***** Failed to get %s lane %d config info. return code %d! ***** \r\n", core_ip, idx, return_result);
                    } else {
                        dprintf("\n\n ***** Successfully get %s lane %d config info! ***** \r\n", core_ip, idx);
                        dprintf("Symbol swap: %s\n", lane_config_info.type.lane_tx_info.value.symbol_swap ? "enable" : "disable");
                        dprintf("TAP enum: %d %s\n", lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps, 
                            txfir_type[lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps]);
                        if(lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps!=TXFIR_TAPS_7TAP && 
                            lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps!=TXFIR_TAPS_12TAP){
                            dprintf("Pre2: %d\n", lane_config_info.type.lane_tx_info.value.txfir.tap[0]);
                            dprintf("Pre1: %d\n", lane_config_info.type.lane_tx_info.value.txfir.tap[1]);
                            dprintf("Main: %d\n", lane_config_info.type.lane_tx_info.value.txfir.tap[2]);
                            dprintf("Post1: %d\n", lane_config_info.type.lane_tx_info.value.txfir.tap[3]);
                            dprintf("Post2: %d\n", lane_config_info.type.lane_tx_info.value.txfir.tap[4]);
                            dprintf("Post3: %d\n", lane_config_info.type.lane_tx_info.value.txfir.tap[5]);
                        }else{
                            if (lane_config_info.type.lane_tx_info.value.txfir.numb_of_taps == TXFIR_TAPS_7TAP)
                                tap_max = 7;
                            else
                                tap_max = 12;
                            for(tap_idx=0; tap_idx<tap_max; tap_idx++)
                                dprintf("tap[%d]: %d\n", tap_idx, lane_config_info.type.lane_tx_info.value.txfir.tap[tap_idx]);
                        }
                            
                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                            for (i = 0; i < 4; i++)
                                dprintf("dsp level_shift[%d]: %d\n", i, lane_config_info.type.lane_tx_info.value.dsp_level_shift[i]);
                            dprintf("dsp graycode: %d\n", lane_config_info.type.lane_tx_info.value.dsp_graycode);
                            dprintf("dsp precode: %d\n", lane_config_info.type.lane_tx_info.value.dsp_precode);
                        }
                    }
                }
            }
            break;

            case CAPI_SET_RX_INFO:
            {
                capi_lane_config_info_t lane_config_info = {0}, lane_config_info_rd = {0};
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                int sub_user_input;

                lane_config_info.lane_config_type = LANE_CONFIG_TYPE_LANE_RX_INFO;

                /* common params */
                dprintf("\n** set VGA : 0 - No, 1 - Yes\r\n");
                scanf("%d", &usr_input);
                if (usr_input) {
                    lane_config_info.type.lane_rx_info.param.is.vga = 1;
                    dprintf("input VGA value host[0-37] media[0-46]:");
                    scanf("%d", &usr_input);
                    lane_config_info.type.lane_rx_info.value.vga = (uint8_t)usr_input;
                }

                dprintf("\n** set media type : 0 - No, 1 - Yes\r\n");
                scanf("%d", &usr_input);
                if (usr_input) {
                    dprintf("\n** Rx media type value:(0:optical, 1:backplane, 2:copper)\r\n");
                    scanf("%d", &usr_input);
                    lane_config_info.type.lane_rx_info.param.is.media_type = 1;
                    lane_config_info.type.lane_rx_info.value.media_type = (uint8_t)usr_input;
                }

                dprintf("\n** peaking filter binary value : 0 - No, 1 - Yes\r\n");
                scanf("%d", &usr_input);
                if (usr_input) {
                    dprintf("\n** peaking filter binary value [0-31]\r\n");
                    scanf("%d", &usr_input);
                    lane_config_info.type.lane_rx_info.param.is.peaking_filter_info = 1;
                    lane_config_info.type.lane_rx_info.value.peaking_filter_info.value = (int8_t)usr_input;
                }

                if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES) {
                    dprintf("\n** set serdes db_loss : 0 - No, 1 - Yes\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input) {
                        dprintf("\n** serdes db_loss value[0-35]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.serdes_db_loss = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.serdes.db_loss = (uint8_t) usr_input;
                    }

                    dprintf("\n** set serdes rx slicer : 0 - No, 1 - Yes\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input) {
                        dprintf("\n** firmware default settings - 0, nr - 1, er - 2\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.serdes_pam_er_nr = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.serdes.pam_er_nr = (uint8_t) usr_input;
                    }

                }

                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
choose_dsp_options:
                    dprintf("Please choose option for setting dsp params\r\n");
                    for (idx = 0; idx < sizeof(lane_config_rx_set_dsp_options) / lane_config_rx_set_dsp_options_len; idx++)
                        dprintf("** %d -- %s\r\n", idx, lane_config_rx_set_dsp_options[idx]);

                    scanf("%d", &usr_input);

                    switch (usr_input) {
                    case 0:
                    break;
                    case 1:
                        dprintf("\n** dsp_graycode value\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_graycode = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.graycode = (int8_t)usr_input;
                    break;
                    case 2:
                        dprintf("\n** dsp_dc_wander_mu value\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_dc_wander_mu = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.dc_wander_mu = (int8_t)usr_input;
                    break;
                    case 3:
                        util_memset(&(lane_config_info_rd.type.lane_rx_info), 0, sizeof(lane_rx_info_t));
                        lane_config_info_rd.lane_config_type = LANE_CONFIG_TYPE_LANE_RX_INFO;
                        lane_config_info_rd.type.lane_rx_info.param.is.dsp_nldet_en = 1;
                        if ((return_result = capi_get_lane_config_info(&capi_phy, &lane_config_info_rd)) != RR_SUCCESS) {
                            dprintf("\n\n***** get rx info failed with return code %d! *****\r\n", return_result);
                            return return_result;
                        }

                        util_memcpy((void *)&lane_config_info.type.lane_rx_info.value.core_ip.dsp.nldet_info, (void *)&lane_config_info_rd.type.lane_rx_info.value.core_ip.dsp.nldet_info,
                                sizeof(lane_config_info.type.lane_rx_info.value.core_ip.dsp.nldet_info));
                        lane_config_info.type.lane_rx_info.param.is.dsp_nldet_en = 1;
                        dprintf("\n** set nldet enable : 0 - No set, 1 - Yes set\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** nldet enable value(HEX): 0-disable; 1-enable; 0xFF-firmware default\r\n");
                            scanf("%x", &usr_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.nldet_info.nldet_en = (int8_t)usr_input;
                        }
                        break;
                    case 4:
                        dprintf("\n** oplos ignore value[0-1]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_oplos_ignore = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.oplos_ignore = (uint8_t)usr_input;
                    break;
                    case 5:
                        dprintf("\n** elos ignore value[0-1]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_elos_ignore = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.elos_ignore = (uint8_t)usr_input;
                    break;
                    case 6:
                        dprintf("\n** los_th_idx value[0-11]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_los_th_idx = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.los_th_idx = (uint8_t)usr_input;
                    break;
                    case 7:
                        dprintf("\n** inlos_th_idx value[0-11]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_inlos_th_idx = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.inlos_th_idx = (uint8_t)usr_input;
                    break;
                    case 8:
                        dprintf("\n** outlos_th_idx value[0-11]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_outlos_th_idx = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.outlos_th_idx = (uint8_t)usr_input;
                    break;
                    case 9:
                        util_memset(&(lane_config_info_rd.type.lane_rx_info), 0, sizeof(lane_rx_info_t));
                        lane_config_info_rd.lane_config_type = LANE_CONFIG_TYPE_LANE_RX_INFO;
                        lane_config_info_rd.type.lane_rx_info.param.is.dsp_kp_kf_info = 1;
                        if ((return_result = capi_get_lane_config_info(&capi_phy, &lane_config_info_rd)) != RR_SUCCESS) {
                            dprintf("\n\n***** get rx info failed with return code %d! *****\r\n", return_result);
                            return return_result;
                        }
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info = lane_config_info_rd.type.lane_rx_info.value.core_ip.dsp.kp_kf_info;
                        lane_config_info.type.lane_rx_info.param.is.dsp_kp_kf_info = 1;
                        dprintf("\n** set kp : 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** kp value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set kf : 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** kf value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kf = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set kp tracking : 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** kp tracking value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp_tracking = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set kf tracking : 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** kf tracking value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kf_tracking = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set kp hlf stp : 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** kp hlf stp value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp_hlf_stp = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set kp tracking hlf stp : 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** kp tracking hlf stp value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp_tracking_hlf_stp = (int8_t)sub_user_input;
                        }
                    break;
                    case 10:
                        util_memset(&(lane_config_info_rd.type.lane_rx_info), 0, sizeof(lane_rx_info_t));
                        lane_config_info_rd.lane_config_type = LANE_CONFIG_TYPE_LANE_RX_INFO;
                        lane_config_info_rd.type.lane_rx_info.param.is.dsp_phase_bias_auto_tuning_info = 1;
                        if ((return_result = capi_get_lane_config_info(&capi_phy, &lane_config_info_rd)) != RR_SUCCESS) {
                            dprintf("\n\n***** get rx info failed with return code %d! *****\r\n", return_result);
                            return return_result;
                        }
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info = lane_config_info_rd.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info;
                        lane_config_info.type.lane_rx_info.param.is.dsp_phase_bias_auto_tuning_info = 1;
                        dprintf("\n** set phase_auto_tune_en: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** phase_auto_tune_en value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_auto_tune_en = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set dynamic_phase_auto_tune_en: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** dynamic_phase_auto_tune_en value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.dynamic_phase_auto_tune_en = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set max_phase_bias_th: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** max_phase_bias_th value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set min_phase_bias_th: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** min_phase_bias_th value: \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th = (int8_t)sub_user_input;
                        }                        
                        dprintf("\n** set phase_tune_step_size: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** phase_tune_step_size value: 0 - single step, 1 - double step \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size = (int8_t)sub_user_input;
                        }
                        dprintf("\n** set phase_tune_dwell_time: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** phase_tune_dwell_time value: 0: 10 ms, 1: 20ms, 2: 30 ms, 3: 40ms, 4: 50ms \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_dwell_time = (int8_t)sub_user_input;
                        }

                        dprintf("\n** set phase_dtune_bias_range: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** phase_dtune_bias_range value: 0 - 10 \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_bias_range = (int8_t)sub_user_input;
                        }                        
                        dprintf("\n** set phase_dtune_snr_change_th: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** phase_dtune_snr_change_th value: 0: 0.1db ms, 1: 0.2db, .. 10: 1.1db \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_snr_change_th = (int8_t)sub_user_input;
                        }                        
                        dprintf("\n** set phase_tune_link_down_snr: 0 - No, 1 - Yes\r\n");
                        scanf("%d", &sub_user_input);
                        if (sub_user_input) {
                            dprintf("\n** phase_tune_link_down_snr value, 0: 12db, 1: 13db, 2: 14db (default) ... 7: 19db \r\n");
                            scanf("%d", &sub_user_input);
                            lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_link_down_snr = (int8_t)sub_user_input;
                        }                        
                    break;
                    case 11:
                        dprintf("\n** gain boost binary value [0~30]\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_gain_boost = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.gain_boost = (int8_t)usr_input;
                        break;
                    case 12:
                        dprintf("\n** power_mode value[0-3]: 0-firmware default setting; 1-high power; 2-medium power; 3-low power:\r\n");
                        scanf("%d", &usr_input);
                        lane_config_info.type.lane_rx_info.param.is.dsp_ffe_info = 1;
                        lane_config_info.type.lane_rx_info.value.core_ip.dsp.ffe_info.power_mode = (int8_t)usr_input;
                        break;
                        
                    default:
                        dprintf("Invalid input option!\r\n");
                        return RR_ERROR;
                    }
                    dprintf("set more params? 0: No, 1: Yes\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input == 1)
                        goto choose_dsp_options;
                }
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);

                    return_result = capi_set_lane_config_info(&capi_phy, &lane_config_info);
                    if (return_result != RR_SUCCESS)
                        dprintf("\n\n***** Set %s side lane %d rx info failed with return code %d! *****\r\n", core_ip, idx, return_result);
                    else {
                        dprintf("\n\n***** Set %s side lane %d rx info successful! *****\r\n", core_ip, idx);
                    }
                }
            } break;

            case CAPI_GET_RX_INFO:
            {
                capi_lane_config_info_t lane_config_info;
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                char pwr_mode[4][20]={"firmware default", "high power", "medium power", "low power"};
                
                util_memset(&lane_config_info, 0, sizeof(capi_lane_config_info_t));
                lane_config_info.lane_config_type = LANE_CONFIG_TYPE_LANE_RX_INFO;
                /* get all params */
                /*lane_config_info.type.lane_rx_info.param.content = 0xFFFFFFFF */;
                /* common parames */
                lane_config_info.type.lane_rx_info.param.is.vga = 1;
                lane_config_info.type.lane_rx_info.param.is.symbol_swap = 1;
                lane_config_info.type.lane_rx_info.param.is.dfe_info = 1;
                lane_config_info.type.lane_rx_info.param.is.media_type = 1;
                lane_config_info.type.lane_rx_info.param.is.peaking_filter_info = 1;
                /* serdes related params */
                if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES) {
                    lane_config_info.type.lane_rx_info.param.is.serdes_db_loss = 1;
                }
                /* dsp related params */
                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                    /* bring up high priority */
                    lane_config_info.type.lane_rx_info.param.is.dsp_graycode = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_dc_wander_mu = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_nldet_en = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_ffe_info = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_phase_bias_auto_tuning_info = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_oplos_ignore = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_elos_ignore = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_los_th_idx = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_inlos_th_idx = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_outlos_th_idx = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_kp_kf_info = 1;
                    lane_config_info.type.lane_rx_info.param.is.dsp_gain_boost = 1;
                }
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);

                    return_result = capi_get_lane_config_info(&capi_phy, &lane_config_info);
                    if (return_result != RR_SUCCESS)
                        dprintf("\n\n***** Get %s Lane_%d RX_INFO Failed with return code %d! *****\r\n",
                                core_ip, idx, return_result);
                    else {
                        dprintf("\n\n***** Get %s Lane_%d RX_INFO Successful ! *****\r\n", core_ip, idx);
                        //dprintf("--- Rx symbol swap = %s \r\n", (lane_config_info.type.lane_rx_info.value.symbol_swap) ? "enable" : "disable");
                        if (lane_config_info.type.lane_rx_info.param.is.vga == 1)
                            dprintf("--- Rx VGA = %d \r\n", lane_config_info.type.lane_rx_info.value.vga);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_ffe_info == 1) {
                            dprintf("--- Rx power_mode  = %d %s\r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.ffe_info.power_mode,
                                pwr_mode[lane_config_info.type.lane_rx_info.value.core_ip.dsp.ffe_info.power_mode]);
                        }
                        if (lane_config_info.type.lane_rx_info.param.is.media_type == 1)
                            dprintf("--- Rx media type   = %d \r\n", lane_config_info.type.lane_rx_info.value.media_type);
                        if (lane_config_info.type.lane_rx_info.param.is.peaking_filter_info == 1)
                            dprintf("--- Rx pf value   = %d \r\n", lane_config_info.type.lane_rx_info.value.peaking_filter_info.value);

                        if (lane_config_info.type.lane_rx_info.param.is.serdes_db_loss == 1)
                            dprintf("--- Rx serdes_db_loss    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.serdes.db_loss);

                        if (lane_config_info.type.lane_rx_info.param.is.dsp_graycode == 1)
                            dprintf("--- Rx dsp_graycode    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.graycode);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_dc_wander_mu == 1)
                            dprintf("--- Rx dc_wander_mu    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.dc_wander_mu);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_nldet_en == 1) {
                            dprintf("--- Rx nldet_en     = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.nldet_info.nldet_en);
                        }

                        if (lane_config_info.type.lane_rx_info.param.is.dsp_phase_bias_auto_tuning_info == 1) {

                            dprintf("--- phase auto tune enable          = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_auto_tune_en);
                            dprintf("--- dynamic phase auto tune enable  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.dynamic_phase_auto_tune_en);
                            dprintf("--- min phase bias                  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.min_phase_bias_th);
                            dprintf("--- max phase_bias                  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.max_phase_bias_th);
                            dprintf("--- phase tune step size            = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_step_size);
                            dprintf("--- phase tune dwell time           = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_dwell_time);
                            
                            dprintf("--- phase_dtune_bias_range          = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_bias_range);
                            dprintf("--- phase_dtune_snr_change_th       = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_dtune_snr_change_th);
                            dprintf("--- phase_tune_link_down_snr        = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.phase_bias_auto_tuning_info.phase_tune_link_down_snr);
                        }
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_oplos_ignore == 1)
                            dprintf("--- Rx dsp_oplos_ignore    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.oplos_ignore);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_elos_ignore == 1)
                            dprintf("--- Rx dsp_elos_ignore    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.elos_ignore);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_los_th_idx == 1)
                            dprintf("--- Rx dsp_los_th_idx    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.los_th_idx);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_inlos_th_idx == 1)
                            dprintf("--- Rx dsp_inlos_th_idx    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.inlos_th_idx);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_outlos_th_idx == 1)
                            dprintf("--- Rx dsp_outlos_th_idx    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.outlos_th_idx);
                        if (lane_config_info.type.lane_rx_info.param.is.dsp_kp_kf_info == 1) {
                            dprintf("--- Rx kp  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp);
                            dprintf("--- Rx kf  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kf);
                            dprintf("--- Rx kp tracking = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp_tracking);
                            dprintf("--- Rx kf tracking = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kf_tracking);
                            dprintf("--- Rx kp hlf stp  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp_hlf_stp);
                            dprintf("--- Rx kp tracking hlf stp  = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.kp_kf_info.kp_tracking_hlf_stp);
                        }

                        if (lane_config_info.type.lane_rx_info.param.is.dsp_gain_boost == 1)
                            dprintf("--- Rx dsp_gain_boost    = %d \r\n", lane_config_info.type.lane_rx_info.value.core_ip.dsp.gain_boost);
                    }
                }
            }
            break;

            case CAPI_SET_SQUELCH:
            {
                uint32_t squelch;
                uint32_t lane_mask_bk = capi_phy.lane_mask;

                capi_lane_ctrl_info_t capi_ctrl;
                util_memset(&capi_ctrl, 0, sizeof(capi_lane_ctrl_info_t));

                dprintf("\n\nPlease choose: 0 -- Rx; 1 -- Tx)\r\n");
                scanf("%d", &usr_input);
                if (usr_input > 1) {
                    dprintf("\n\nWrong selection, defaults to Tx\r\n");
                    usr_input = 0;
                    break;
                }
                dprintf("\n\nPlease Choose: 0 -- Unsquelch ; 1 -- Squelch\r\n");
                scanf("%d", &squelch);
                if (squelch > 1) {
                    dprintf("\n\nWrong selection, defaults to Squelch\r\n");
                    squelch = 1;
                    break;
                }
                if (usr_input)
                {
                    capi_ctrl.param.is.tx_squelch = 1;
                    if (squelch)
                        capi_ctrl.cmd_value.is.tx_squelch = 1;
                    else
                        capi_ctrl.cmd_value.is.tx_squelch = 0;
                }
                else
                {
                    capi_ctrl.param.is.rx_squelch = 1;
                    if (squelch)
                        capi_ctrl.cmd_value.is.rx_squelch = 1;
                    else
                        capi_ctrl.cmd_value.is.rx_squelch = 0;
                }

                return_result = capi_set_lane_ctrl_info(&capi_phy, &capi_ctrl);
                if (return_result != RR_SUCCESS)
                {
                    dprintf("\n\n***** Failed to config serdes %s %s. Return code is %d! *****\r\n", direction_str[usr_input], squelch ? "Squelch" : "Unsquelch", return_result);
                    break;
                }
                else {
                    dprintf("\n\n***** Successfully config serdes %s %s! *****\r\n", direction_str[usr_input], squelch ? "Squelch" : "Unsquelch");
                }

                printf("Reading back the Squelch values ...\n");

                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);

                    util_memset(&capi_ctrl, 0, sizeof(capi_lane_ctrl_info_t));
                    capi_ctrl.param.is.rx_squelch = 1;
                    capi_ctrl.param.is.tx_squelch = 1;
                    
                    return_result = capi_get_lane_ctrl_info(&capi_phy, &capi_ctrl);
                    if (return_result != RR_SUCCESS)
                        dprintf("\n\n*****  Failed to read serdes squelch status on lane %d. Return code %d! *****\r\n", idx, return_result);
                    else
                        dprintf("\n\n*****  Successfully read serdes squelch status on lane %d ! *****\r\n", idx);
                        dprintf("\n\n*****  Serdes lane %d Rx Squelch : %s *****\r\n", idx, capi_ctrl.cmd_value.is.rx_squelch ? "On" : "Off");
                        dprintf("\n\n*****  Serdes lane %d Tx Squelch : %s *****\r\n", idx, capi_ctrl.cmd_value.is.tx_squelch ? "On" : "Off");
                }
                delay_ms(200);
            } break;

            case CAPI_SET_LOOPBACK:
            {
                capi_loopback_info_t capi_lpbk;
                int loopback_mode;
                return_result_t retval;

                //Fill in the loopback type and config
                dprintf("\r\nPlease choose %s loopback type: 0-G-loopback 1- Remote-loopback\r\n", core_ip);
                scanf("%d", &loopback_mode);
                if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                    capi_lpbk.mode = (loopback_mode == 0) ? CAPI_SYSTEM_GLOBAL_PMD_LOOPBACK_MODE : CAPI_SYSTEM_REMOTE_PMD_LOOPBACK_MODE;
                else
                    capi_lpbk.mode = (loopback_mode == 0) ? CAPI_GLOBAL_PMD_LOOPBACK_MODE : CAPI_REMOTE_PMD_LOOPBACK_MODE;
                dprintf("\n\nPlease choose %s loopback enable/disable: 0--disable; 1-enable\r\n", core_ip);
                scanf("%d", &usr_input);
                capi_lpbk.enable = usr_input;

                retval = capi_set_loopback(&capi_phy, &capi_lpbk);
                if (retval != RR_SUCCESS)
                    dprintf("\n\n ***** Configure %s loopback FAILED with return code %d! ***** \r\n", core_ip, retval);
                else {
                    dprintf("\n\n ***** Configure %s loopback SUCCESSFULLY! ***** \r\n", core_ip);
                }
            } break;

            case CAPI_GET_LOOPBACK:
            {
                uint8_t lane_mask = capi_phy.lane_mask;
                capi_loopback_info_t capi_lpbk = {0};
                int loopback_mode;
                return_result_t retval;

                //Fill in the loopback type and config
                dprintf("\r\nPlease choose %s loopback type: 0 - Global / digital loopback; 1 - Remote loopback\r\n", core_ip);
                scanf("%d", &loopback_mode);
                if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                    capi_lpbk.mode = (loopback_mode == 0) ? CAPI_SYSTEM_GLOBAL_PMD_LOOPBACK_MODE : CAPI_SYSTEM_REMOTE_PMD_LOOPBACK_MODE;
                else
                    capi_lpbk.mode = (loopback_mode == 0) ? CAPI_GLOBAL_PMD_LOOPBACK_MODE : CAPI_REMOTE_PMD_LOOPBACK_MODE;
                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask & (1 << idx)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << idx);

                    retval = capi_get_loopback(&capi_phy, &capi_lpbk);
                    if (retval != RR_SUCCESS)
                       dprintf("\n\n ***** Failed to get %s side loopback info with return code %d! ***** \r\n", core_ip, retval);
                    else {
                        if (capi_phy.core_ip == CORE_IP_MEDIA_SERDES || capi_phy.core_ip == CORE_IP_HOST_SERDES)
                            dprintf("\n\n ***** %s side: lane %x %s loopback is %s! ***** \r\n", core_ip,
                                idx, capi_lpbk.mode == CAPI_SYSTEM_REMOTE_PMD_LOOPBACK_MODE ? "remote" : "global",
                                capi_lpbk.enable ? "enabled" : "disabled");
                        else
                            dprintf("\n\n ***** %s side: lane %x %s loopback is %s! ***** \r\n", core_ip,
                                idx, capi_lpbk.mode == CAPI_REMOTE_PMD_LOOPBACK_MODE ? "remote" : "global",
                                capi_lpbk.enable ? "enabled" : "disabled");
                    }
                }
            } break;

            case CAPI_HOST_SERDES_SET_TX_SHARED_PTRN:
            {
                int length;
                int enable;
                char pattern[32] = {0};
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                return_result_t return_result;

                if (capi_phy.core_ip != CORE_IP_HOST_SERDES) {
                    dprintf("CORE IP not supported!\r\n");
                    break;
                }

                dprintf("\r\nPlease read the following for usage\r\n");
                dprintf("\r\n1. For NRZ  mode, the input pattern should be provided in either hex or binary.\r\n");
                dprintf("*   eg: For a repeating pattern \"0000010110101111\", input_pattern = \"0000010110101111\" or \"0x05AF\" and patt_length_bits = 16\r\n");
                dprintf("\r\n2. For PAM4 mode, the input pattern should be provided in PAM4 format, as described below.\r\n");
                dprintf("*   PAM4 format uses four symbols - 0, 1, 2 or 3 (corresponding to voltage levels -3,-1,+1 and +3).\r\n");

                dprintf("*   - The input should be preceded by \"p\" to indicate a PAM4 format. The input could be spaced with \"_\" for better readability\r\n");
                dprintf("*   - Since each symbol is equivalent to 2 bits, the pattern_length = 2 * number of symbols in the pattern. \n");
                dprintf("*     eg: For a repeating pattern of \"-3-3-1-1+1+1+3+3\", input_pattern = \"p0011_2233\" and patt_length_bits = 16\r\n");

                dprintf("\nPlease enter pattern string (31 characters or less):\r\n");
                scanf("%s", &(pattern[0]));
                dprintf("\nPlease enter string pattern length:\r\n");
                scanf("%d", &length);
                dprintf("\nPlease enter value for 0 - disable, 1 - enable\r\n");
                scanf("%d", &enable);

                if (length > sizeof(pattern) - 1) {
                    dprintf("Length of pattern string exceeded maximum!");
                    break;
                }

                enable = enable ? 1 : 0;

                for (idx = 0; idx < max_lanes; idx++) {
                    if ((lane_mask_bk & (1 << idx)) == 0)
                        continue;

                    dprintf("\n** Turn on serdes tx shared pattern on lane %d\r\n", idx);
                    capi_phy.lane_mask = 1 << idx;
                    capi_phy.core_ip = CORE_IP_HOST_SERDES;

                    util_memset((void*)&capi_prbs, 0, sizeof(capi_prbs_info_t));

                    capi_prbs.gen_switch = (capi_switch_t) enable;
                    capi_prbs.ptype = CAPI_PRBS_SHARED_TX_PATTERN_GENERATOR;
                    capi_prbs.op.shared_tx_ptrn.length = length;
                    util_memcpy((void*) (capi_prbs.op.shared_tx_ptrn.pattern), (void*) pattern, sizeof(pattern) - 1);
                    return_result = capi_set_prbs_info(&capi_phy, &capi_prbs);
                    if (return_result != RR_SUCCESS) {
                        dprintf ("client side capi set prbs for tx shared pattern failed\r\n");
                        break;
                    }
                }
            } break;

            case CAPI_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH:
            {
                return_result_t result_result;
                int optrxlos_host_fast_tx_squelch;
                capi_chip_command_info_t chip_command_info;

                if (capi_phy.core_ip != CORE_IP_HOST_SERDES) {
                    dprintf("CORE IP not supported!\r\n");
                    break;
                }

                dprintf("Fast client Tx squelch upon optrxlos assertion: 0 - disable this feature, 1 - re-enable this feature\r\n");
                scanf("%x", &optrxlos_host_fast_tx_squelch);
                if (optrxlos_host_fast_tx_squelch != 0) {
                    dprintf("Please use capi_reset to re-enable the feature\r\n");
                    break;
                }
                capi_phy.core_ip = CORE_IP_CW;
                chip_command_info.type.optrxlos_host_fast_tx_squelch = (uint8_t) optrxlos_host_fast_tx_squelch;
                chip_command_info.command_id = COMMAND_ID_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH;

                result_result = capi_set_chip_command(&capi_phy, &chip_command_info);
                if (result_result) {
                    dprintf("\n\n ***** Failed to configure host Tx fast unsquelch upon assertion of optrxlos. Error code = 0x%x ****\r\n");
                }
                else {
                    dprintf("\n\n***** Successfully configured host Tx fast squelch upon assertion of optrxlos *****\r\n");
                }
            }
            break;

            case CAPI_TEST_SET_ELECTRICAL_OPTICAL:
                if (capi_phy.core_ip != CORE_IP_MEDIA_DSP) {
                    dprintf("Error: core ip is not supported\n");
                }
                else {
                    cli_test_set_elec_optical(&capi_phy);
                }
                break;

            case CAPI_TEST_FAST_CMIS_SNR:
                if (capi_phy.core_ip != CORE_IP_MEDIA_DSP) {
                    dprintf("Error: core ip is not supported\n");
                }
                cli_test_fast_cmis_snr(capi_phy);
                break;

            case CAPI_TEST_CONFIG_MPI:
                cli_config_media_mpi(capi_phy);
                break;

            case CAPI_TEST_GET_MPI_STATE:
                cli_get_media_mpi_state(capi_phy);
                break;

            case CAPI_TEST_CONFIG_DYNAMIC_MPI:
                cli_config_mdia_dynamic_mpi(capi_phy);
                break;

            case CAPI_TEST_GET_DYNAMIC_MPI_STATE:
                cli_get_media_dynamic_mpi_state(capi_phy);
                break;

            case CAPI_SERDES_SET_LANE_CDR_MODE:
                cli_serdes_set_lane_cdr_mode(&capi_phy);
                break;

            case CAPI_SERDES_GET_LANE_CDR_MODE:
                cli_serdes_get_lane_cdr_mode(&capi_phy);
                break;

            case CAPI_SERDES_GET_DIAG_INFO:
                cli_serdes_get_diag_info(&capi_phy);
                break;

            case CAPI_TEST_SET_TC_TX_TYPE:
                if (capi_phy.core_ip != CORE_IP_MEDIA_DSP) {
                    dprintf("Error: core ip is not supported\n");
                }
                else {
                    cli_test_set_tc_tx_mode(&capi_phy);
                }
                break;

            case CAPI_GET_GPR_LANE_STATUS:
            {
                capi_status_t capi_gpr_lane_status;
                capi_phy_info_t rst_phy_info;

                util_memset(&capi_gpr_lane_status, 0, sizeof(capi_status_t));
                util_memcpy(&rst_phy_info, &capi_phy, sizeof(capi_phy_info_t));

                if (capi_phy.core_ip == CORE_IP_HOST_SERDES) {
                    dprintf("Please choose from the following status:\r\n");
                    dprintf("0 - CDR status\r\n");
                    dprintf("1 - LoS and CDR LoL sticky\r\n");
                    dprintf("2 - Signal Detect\r\n");
                    dprintf("3 - Tx Squelch status\r\n");
                    dprintf("4 - CDR restart counter\r\n");
                    dprintf("5 - CMIS rx output status\r\n");
                    dprintf("6 - CMIS rx latched output status\r\n");
                    scanf("%d", &usr_input);

                    if (usr_input < 0 || usr_input > 6) {
                        usr_input = 0; /* defaults to cdr lock status */
                    }

                    capi_gpr_lane_status.status_type = usr_input;

                    if (usr_input == GPR_LANE_CDR_RESTART_COUNTER) {
                        for (idx = 0; idx < max_lanes; ++idx) {
                            if (capi_phy.lane_mask & (1 << idx)) {
                                rst_phy_info.lane_mask = 1 << idx;
                                return_result = capi_get_status(&rst_phy_info, &capi_gpr_lane_status);
                                if (return_result == RR_SUCCESS) {
                                    dprintf("Reading lane %d CDR restart counter %d\r\n", idx, capi_gpr_lane_status.param.lane_cdr_restart_counter);
                                }
                                else {
                                    dprintf("Failed to get lane CDR reset counter. return code %d\r\n", return_result);
                                }
                            }
                        }
                    }
                    else {
                        return_result = capi_get_status(&capi_phy, &capi_gpr_lane_status);
                        if (return_result == RR_SUCCESS) {
                            for (idx = 0; idx < max_lanes; ++idx) {
                                if (capi_phy.lane_mask & (1 << idx)) {
                                    switch (usr_input) {
                                        case GPR_LANE_CDR_LOCK_STATUS:
                                            dprintf("Reading lane %d cdr status %d\r\n", idx, (capi_gpr_lane_status.param.cdr.lock_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        case GPR_LANE_LOL_LOS_STATUS:
                                            dprintf("Reading lane %d cdr lol_sticky %d los %d\r\n", idx,
                                                    (capi_gpr_lane_status.param.los.lol_sticky & (1 << idx)) ? 1 : 0,
                                                    (capi_gpr_lane_status.param.los.los_status & (1 << idx)) ? 1 : 0
                                                   );
                                            break;
                                        case GPR_LANE_SIGDET_STATUS:
                                            dprintf("Reading lane %d SigDet status %d\r\n", idx, (capi_gpr_lane_status.param.lane_sigdet_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        case GPR_LANE_TX_SQUELCH_STATUS:
                                            dprintf("Reading lane %d tx_squelch status %d\r\n", idx, (capi_gpr_lane_status.param.lane_tx_squelch_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        case GPR_LANE_RX_OUTPUT_STATUS:
                                            dprintf("Reading lane %d cmis rx output status %d\r\n", idx, (capi_gpr_lane_status.param.lane_rx_output_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        case GPR_LANE_RX_OUTPUT_LATCH_STATUS:
                                            dprintf("Reading lane %d cmis rx output latch status %d\r\n", idx, (capi_gpr_lane_status.param.lane_rx_output_latch_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        else {
                            dprintf("Failed to get lane status. return code %d\r\n", return_result);
                        }
                    }
                }
                else if (capi_phy.core_ip == CORE_IP_MEDIA_DSP) {
                    dprintf("Please choose from the following status:\r\n");
                    dprintf("0 - CDR status\r\n");
                    dprintf("1 - LoS and CDR LoL sticky\r\n");
                    dprintf("2 - Signal Detect\r\n");
                    dprintf("3 - Tx Squelch status\r\n");
                    dprintf("4 - CDR restart counter\r\n");
                    scanf("%d", &usr_input);

                    if (usr_input < 0 || usr_input > 4) {
                        usr_input = 0; /* defaults to cdr lock status */
                    }

                    capi_gpr_lane_status.status_type = usr_input;
                    if (usr_input == GPR_LANE_CDR_RESTART_COUNTER) {
                        for (idx = 0; idx < max_lanes; ++idx) {
                            if (capi_phy.lane_mask & (1 << idx)) {
                                rst_phy_info.lane_mask = 1 << idx;
                                return_result = capi_get_status(&rst_phy_info, &capi_gpr_lane_status);
                                if (return_result == RR_SUCCESS) {
                                    dprintf("Reading lane %d CDR restart counter %d\r\n", idx, capi_gpr_lane_status.param.lane_cdr_restart_counter);
                                }
                                else {
                                    dprintf("Failed to get lane CDR reset counter. return code %d\r\n", return_result);
                                }
                            }
                        }
                    }
                    else {
                        return_result = capi_get_status(&capi_phy, &capi_gpr_lane_status);
                        if (return_result == RR_SUCCESS) {
                            for (idx = 0; idx < max_lanes; ++idx) {
                                if (capi_phy.lane_mask & (1 << idx)) {
                                    switch (usr_input) {
                                        case GPR_LANE_CDR_LOCK_STATUS:
                                            dprintf("Reading lane %d cdr status %d\r\n", idx, (capi_gpr_lane_status.param.cdr.lock_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        case GPR_LANE_LOL_LOS_STATUS:
                                            dprintf("Reading lane %d cdr lol_sticky %d los %d\r\n", idx,
                                                    (capi_gpr_lane_status.param.los.lol_sticky & (1 << idx)) ? 1 : 0,
                                                    (capi_gpr_lane_status.param.los.los_status & (1 << idx)) ? 1 : 0
                                                   );
                                            break;
                                        case GPR_LANE_SIGDET_STATUS:
                                            dprintf("Reading lane %d SigDet status %d\r\n", idx, (capi_gpr_lane_status.param.lane_sigdet_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        case GPR_LANE_TX_SQUELCH_STATUS:
                                            dprintf("Reading lane %d tx_squelch status %d\r\n", idx, (capi_gpr_lane_status.param.lane_tx_squelch_status & (1 << idx)) ? 1 : 0);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        else {
                            dprintf("Failed to get lane status. return code %d\r\n", return_result);
                        }
                    }
                }
                else {
                    dprintf("Unsupported core ip\r\n");
                }
            } break;

            case CAPI_DIAG_GET_LANE_STATUS:
                {
                    uint8_t index;
                    uint32_t lane_mask = capi_phy.lane_mask;
                    capi_diag_lane_status_t capi_diag_lane_status;

                    for (index = 0; index < max_lanes; ++index) {
                        if (lane_mask & (1 << index)) {
                            capi_phy.lane_mask = 1 << index;
                            util_memset(&capi_diag_lane_status, 0x0, sizeof(capi_diag_lane_status_t));
                            capi_diag_lane_status.param.is.eye_margin_estimate = 1;
                            return_result = capi_diag_get_lane_status(&capi_phy, &capi_diag_lane_status);
                            if (return_result == RR_SUCCESS) {
                                dprintf("\r\nSuccessfully retrieve lane diagnostics for lane 0x%x!\r\n", index);
                                dprintf("Eye (Left): %d\r\n", capi_diag_lane_status.value.eye_margin_est.left_eye_mui);
                                dprintf("Eye (Right): %d\r\n", capi_diag_lane_status.value.eye_margin_est.right_eye_mui);
                                dprintf("Eye (Lower): %d\r\n", capi_diag_lane_status.value.eye_margin_est.lower_eye_mv);
                                dprintf("Eye (Upper): %d\r\n", capi_diag_lane_status.value.eye_margin_est.upper_eye_mv);
                            }
                            else {
                                dprintf("\r\nFailed to retrieve lane diagnostics on lane 0x%x!\r\n", index, return_result);
                            }
                        }
                    }

                } break;

            case CAPI_SET_DSP_POWER_INFO:
                {
                    capi_chip_command_info_t chip_command_info;
                    util_memset(&chip_command_info, 0, sizeof(capi_chip_command_info_t));

                    dprintf("Please select from the following power configuration\r\n");
                    dprintf("0 - default power settings\r\n");
                    dprintf("1 - high power settings\r\n");
                    dprintf("2 - medium power settings\r\n");
                    dprintf("3 - low power settings\r\n");
                    scanf("%d", &usr_input);

                    if (usr_input < 0 || usr_input > 3) {
                        dprintf("Invalid selection, defaults to default power settings\r\n");
                        usr_input = 0;
                    }

                    chip_command_info.command_id = COMMAND_ID_SET_DSP_POWER_INFO;
                    chip_command_info.type.dsp_power_info.power_mode = (power_mode_t) usr_input;

                    if (capi_set_chip_command(&capi_phy, &chip_command_info) == RR_SUCCESS) {
                        dprintf("\r\nSuccessfully set power info on lane mask 0x%x!\r\n", capi_phy.lane_mask);
                    }
                    else {
                        dprintf("\r\nFailed to set power info on lane mask 0x%x!\r\n", capi_phy.lane_mask);
                    }
                } break;

            case CAPI_GET_DSP_POWER_INFO:
                {
                    uint8_t index;
                    uint32_t lane_mask = capi_phy.lane_mask;
                    capi_chip_command_info_t chip_command_info;
                    char * power_info_str[] = {"default", "high", "medium", "low"};

                    for (index = 0; index < max_lanes; ++index) {
                        if (lane_mask & (1 << index)) {
                            capi_phy.lane_mask = 1 << index;
                            util_memset(&chip_command_info, 0, sizeof(capi_chip_command_info_t));
                            chip_command_info.command_id = COMMAND_ID_GET_DSP_POWER_INFO;
                            if (capi_get_chip_command(&capi_phy, &chip_command_info) == RR_SUCCESS) {
                                dprintf("\r\nSuccessfully retrieve power info for lane 0x%x! ", index);
                                dprintf("Power settings: %s\r\n", power_info_str[chip_command_info.type.dsp_power_info.power_mode]);
                            }
                            else {
                                dprintf("\r\nFailed to retrieve power info on lane 0x%x!\r\n", index);
                            }
                        }
                    }
                } break;

            case CAPI_READ_REGISTER:
            {
                capi_phy_info_t capi_phy = {0};
                capi_phy.phy_id = phy_info_ptr->phy_id;
                util_memset(&reg_info, 0, sizeof(capi_reg_info_t));

                dprintf("Please enter the register address to read in hex format\r\n");
                scanf("%x", &usr_input);
                reg_info.reg_address = usr_input;

                if ((capi_read_register(&capi_phy, &reg_info)) != RR_SUCCESS) {
                    dprintf("Failed to read register address 0x%x\n", reg_info.reg_address);
                }
                else {
                    dprintf("\tRegister address: 0x%x, Value : 0x%x\n", reg_info.reg_address, reg_info.content);
                }
            } break;

            case CAPI_WRITE_REGISTER:
            {
                capi_phy_info_t capi_phy = {0};
                capi_phy.phy_id = phy_info_ptr->phy_id;
                util_memset(&reg_info, 0, sizeof(capi_reg_info_t));

                dprintf("Please enter the register address to write in hex format\r\n");
                scanf("%x", &usr_input);
                reg_info.reg_address = usr_input;

                dprintf("Please enter the register content to write in hex format (16 bit only)\r\n");
                scanf("%x", &usr_input);
                reg_info.content = usr_input;

                if(capi_write_register(&capi_phy, &reg_info) != RR_SUCCESS) {
                    dprintf("Failed to write to register address 0x%x\n", reg_info.reg_address);
                }
                else {
                    dprintf("\tSuccessfuly wrote to register address: 0x%x\n", reg_info.reg_address);
                }
            } break;

            case CAPI_GET_POLARITY:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                dprintf("\n\nRead polarity to check\n");
                for (ii = 0; ii < max_lanes; ii++) {
                    if ((lane_mask_bk & (1 << ii)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << ii);
                    util_memset((void*)&capi_polarity, 0, sizeof(capi_polarity_info_t));
                    capi_polarity.direction = DIR_EGRESS;
                    return_result = capi_get_polarity(&capi_phy, &capi_polarity);
                    if (return_result == RR_SUCCESS)
                        dprintf("\n Get %s_Lane_%d egress polarity = %d, ", core_ip, ii, capi_polarity.action);
                    else {
                        dprintf("\n Get %s_Lane_%d egress polarity failed with return code %d, \r\n", core_ip, ii, return_result);
                        break;
                    }
                }
                dprintf("\n\n");
                for (ii = 0; ii < max_lanes; ii++) {
                    if ((lane_mask_bk & (1 << ii)) == 0)
                        continue;
                    capi_phy.lane_mask = (1 << ii);
                    util_memset((void*)&capi_polarity, 0, sizeof(capi_polarity_info_t));
                    capi_polarity.direction = DIR_INGRESS;

                    return_result = capi_get_polarity(&capi_phy, &capi_polarity);
                    if (return_result == RR_SUCCESS)
                        dprintf("\n Get %s_Lane_%d ingress polarity = %d, ", core_ip, ii, capi_polarity.action);
                    else {
                        dprintf("\n Get %s_Lane_%d ingress polarity failed with returh code %d \r\n", core_ip, ii, return_result);
                        break;
                    }
                }
            } break;

            case CAPI_GET_LANE_STATUS:
            {
                uint32_t lane_mask_bk = capi_phy.lane_mask;
                capi_lane_info_t lane_info;

                util_memset(&lane_info, 0, sizeof(capi_lane_info_t));

                for (idx = 0; idx < max_lanes; idx++) {
                    if((lane_mask_bk&(1<<idx))==0)
                        continue;

                    capi_phy.lane_mask = 1 << idx;

                    lane_info.param.is.lane_status = 1;
                    return_result = capi_get_lane_info(&capi_phy, &lane_info);
                    if (return_result != RR_SUCCESS) {
                        dprintf("\n**Error: capi_get_lane_info failed on %s side on lane %d\r\n", core_ip, idx);
                    }
                    else {
                        dprintf("\r\n");
                        dprintf("Lane %d cdr_lock   : %d\r\n", idx, lane_info.value.lane_status.cdr_lock);
                        dprintf("Lane %d los        : %d\r\n", idx, lane_info.value.lane_status.los);
                        dprintf("Lane %d tx_squelch : %d\r\n", idx, lane_info.value.lane_status.tx_squelch);
                        dprintf("Lane %d tx_mission : %d\r\n", idx, lane_info.value.lane_status.tx_mission);
                        dprintf("Lane %d rx_mission : %d\r\n", idx, lane_info.value.lane_status.rx_mission);
                        dprintf("Lane %d pll_lock   : %d\r\n", idx, lane_info.value.lane_status.pll_lock);
                    }
                }
            }
            break;

            case CAPI_SET_LANE_CTRL:
            {
                uint32_t lane_mask, usr_input;
                capi_lane_ctrl_info_t capi_lane_ctrl_info = {0};
                capi_direction_t direction;
                int test_item;
                uint8_t input_error = 0;
                return_result_t ret_result;

                lane_mask = capi_phy.lane_mask;

                dprintf("Please choose option for setting lane control ......\r\n");
                for (idx = 0; idx < 7; idx++)
                    dprintf("** %d -- %s\r\n", idx, lane_ctrl_options[idx]);
                    //dprintf("** %d -- %s\r\n", 7, "Back to main test");

                scanf("%d", &test_item);
                if ((test_item > SUSPEND_RESUME) && (test_item != 7))
                    test_item = 0; // default on get_diag_status
                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                    dprintf("\n\nChoose lane direction: 0 - Ingress (DSP Rx); 1 - Egress (DSP Tx); 2 - Both\r\n");
                else
                    dprintf("\n\nChoose lane direction: 0 - Ingress (Serdes Tx); 1 - Egress (Serdes Rx); 2 - Both\r\n");

                scanf("%d", &usr_input);
                if (usr_input > DIR_BOTH) {
                    dprintf("\r\nIncorrect selection, defaults to Ingress\r\n");
                    usr_input = DIR_INGRESS;
                }

                //capi_lane_ctrl_info.type.ctrl_info.direction = (uint8_t) usr_input;
                direction = (capi_direction_t) usr_input;
                switch ((test_lane_ctrl_e) test_item)
                {
                    case LANE_SQUELCH:
                        dprintf("\n\nEnable Squelch : 0 - No; 1 - Yes\r\n");
                        scanf("%d", &usr_input);
                        if (usr_input < 2) {
                            if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                            {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_squelch = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_squelch = usr_input ? 1 : 0;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_squelch = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_squelch = usr_input ? 1 : 0;
                                    }
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_squelch = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_squelch = usr_input ? 1 : 0;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_squelch = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_squelch = usr_input ? 1 : 0;
                                    }
                                }
                            }
                            else
                                input_error = 1;
                            break;

                        //case TRAFFIC:
                        case LANE_RESET:
                            dprintf("\n\nLane Reset   : 0 - Don't Reset; 1 - Reset\r\n");
                            scanf("%d", &usr_input);
                            if (usr_input < 3) {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_lane_cfg_reset = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_lane_cfg_reset = usr_input;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_lane_cfg_reset = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_lane_cfg_reset = usr_input;
                                    }
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_lane_cfg_reset = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_lane_cfg_reset = usr_input;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_lane_cfg_reset = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_lane_cfg_reset = usr_input;
                                    }
                                }
                            }
                            else
                                input_error = 1;
                            break;

                        case TRAFFIC:
                            dprintf("\n\nTraffic   : 0 - Off; 1 - On\r\n");
                            scanf("%d", &usr_input);
                            if (usr_input < 2) {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_data_path = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_data_path = usr_input;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_data_path = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_data_path = usr_input;
                                    }
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_data_path = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_data_path = usr_input;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_data_path = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_data_path = usr_input;
                                    }
                                }
                            }
                            else
                                input_error = 1;
                            break;

                        case ELECTRIC_IDLE:
                            dprintf("\n\nElectric Idle   : 0 - Off; 1 - On\r\n");
                            scanf("%d", &usr_input);
                            if (usr_input < 2) {
#if 0
                                if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                    capi_lane_ctrl_info.param.is.rx_electric_idle = 1;
                                    capi_lane_ctrl_info.cmd_value.is.rx_electric_idle = usr_input;
                                }
#endif
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if (direction == DIR_EGRESS) {
                                        capi_lane_ctrl_info.param.is.tx_electric_idle = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_electric_idle = usr_input;
                                    }
                                    else {
                                        dprintf("Unsupported direction\n");
                                        input_error = 1;
                                    }
                                }
                                else
                                {
                                    if (direction == DIR_INGRESS) {
                                        capi_lane_ctrl_info.param.is.tx_electric_idle = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_electric_idle = usr_input;
                                    }
                                    else {
                                        dprintf("Unsupported direction\n");
                                        input_error = 1;
                                    }
                                }
                            }
                            else
                                input_error = 1;
                            break;

                        case DATAPATH_POWER:
                            dprintf("\n\nPower   : 0 - Off; 1 - On;  2 - Toggle\r\n");
                            scanf("%d", &usr_input);
                            if (usr_input < 3) {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_datapath_power = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_datapath_power = usr_input;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_datapath_power = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_datapath_power = usr_input;
                                    }
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.rx_datapath_power = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_datapath_power = usr_input;
                                    }
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                        capi_lane_ctrl_info.param.is.tx_datapath_power = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_datapath_power = usr_input;
                                    }
                                }
                            }
                            else
                                input_error = 1;
                            break;

                        case IGNORE_FAULT:
                            dprintf("\n\nIgnore fault   : 0 - Off; 1 - On\r\n");
                            scanf("%d", &usr_input);
                            if (usr_input < 2) {
                                capi_lane_ctrl_info.param.is.ignore_fault = 1;
                                capi_lane_ctrl_info.cmd_value.is.ignore_fault = usr_input;
                            }
                            else
                                input_error = 1;
                            break;

                        case SUSPEND_RESUME:
                            dprintf("\n\nSuspend or resume   : 0 - Resume; 1 - Suspend\r\n");
                            scanf("%d", &usr_input);
                            if (usr_input < 2) {
                                if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                    if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                                        capi_lane_ctrl_info.param.is.tx_suspend_resume = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_suspend_resume = usr_input;
                                    } else {
                                        capi_lane_ctrl_info.param.is.rx_suspend_resume = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_suspend_resume = usr_input;
                                    }
                                }
                                if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                    if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP) {
                                        capi_lane_ctrl_info.param.is.rx_suspend_resume = 1;
                                        capi_lane_ctrl_info.cmd_value.is.rx_suspend_resume = usr_input;
                                    } else {
                                        capi_lane_ctrl_info.param.is.tx_suspend_resume = 1;
                                        capi_lane_ctrl_info.cmd_value.is.tx_suspend_resume = usr_input;
                                    }
                                }
                            }
                            else
                                input_error = 1;
                            break;

                        default:
                            break;
                    }

                    if (input_error == 0) {
                        for (idx = 0; idx < max_lanes; ++idx) {
                            if ((lane_mask & (1 << idx)) == 0)
                                continue;
                            capi_phy.lane_mask = (1 << idx);

                            ret_result = capi_set_lane_ctrl_info(&capi_phy, &capi_lane_ctrl_info);
                            if (ret_result != RR_SUCCESS) {
                                dprintf("\n\n***** Failed to set %s lane %d control info!  return_result : %d *****\r\n", core_ip, idx, ret_result);
                            }
                            else {
                                dprintf("\n\n\n***** Successfully set %s lane %d control info! *****\r\n", core_ip, idx);
                            }
                        }
                    }

                }
                break;

            case CAPI_GET_LANE_CTRL:
            {
                    uint32_t lane_mask, test_item;
                    capi_lane_ctrl_info_t capi_lane_ctrl_info;
                    capi_direction_t direction;
                    return_result_t ret_result;

                    util_memset((void*)&capi_lane_ctrl_info, 0, sizeof(capi_lane_ctrl_info_t));

                    lane_mask = capi_phy.lane_mask;

                    dprintf("Please choose option for getting lane control ......\r\n");
                    for (idx = 0; idx < 7; idx++)
                        dprintf("** %d -- %s\r\n", idx, lane_ctrl_options[idx]);
                    //dprintf("** %d -- %s\r\n", 7, "Back to main test");

                    scanf("%d", &test_item);
                    if ((test_item > SUSPEND_RESUME) && (test_item != 7))
                        test_item = 0; // default on get_diag_status
                    if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                        dprintf("\n\nChoose lane direction: 0 - Ingress (DSP Rx); 1 - Egress (DSP Tx) 2 - Both\r\n");
                    else
                        dprintf("\n\nChoose lane direction: 0 - Ingress (Serdes Tx); 1 - Egress (Serdes Rx) 2 - Both\r\n");

                    scanf("%d", &usr_input);
                    if (usr_input > DIR_BOTH) {
                        dprintf("\r\nIncorrect selection, defaults to Ingress\r\n");
                        usr_input = DIR_INGRESS;
                    }
                    //capi_lane_ctrl_info.type.ctrl_info.direction = (uint8_t) usr_input;
                    direction = (capi_direction_t) usr_input;
                    switch ((test_lane_ctrl_e) test_item)
                    {
                        case SQUELCH:
                            {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_squelch = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_squelch = 1;
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_squelch = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_squelch = 1;
                                }
                            }
                            break;

                        //case TRAFFIC:
                        case LANE_RESET:
                            {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_lane_cfg_reset = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_lane_cfg_reset = 1;
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_lane_cfg_reset = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_lane_cfg_reset = 1;
                                }
                            }
                            break;


                        case TRAFFIC:
                            {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_data_path = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_data_path = 1;
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_data_path = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_data_path = 1;
                                }
                            }
                            break;


                        case ELECTRIC_IDLE:

                            /*if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                capi_lane_ctrl_info.param.is.rx_electric_idle = 1;*/
                            if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                            {
                                if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                    capi_lane_ctrl_info.param.is.tx_electric_idle = 1;
                            }
                            else
                            {
                                if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                    capi_lane_ctrl_info.param.is.tx_electric_idle = 1;
                            }
                            break;

                        case DATAPATH_POWER:
                            {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_datapath_power = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_datapath_power = 1;
                                }
                                else
                                {
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.rx_datapath_power = 1;
                                    if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                        capi_lane_ctrl_info.param.is.tx_datapath_power = 1;
                                }
                            }
                            break;

                        case IGNORE_FAULT:
                            capi_lane_ctrl_info.param.is.ignore_fault = 1;
                            break;
                        case SUSPEND_RESUME:
                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                    capi_lane_ctrl_info.param.is.tx_suspend_resume = 1;
                                else
                                    capi_lane_ctrl_info.param.is.rx_suspend_resume = 1;
                            }
                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                    capi_lane_ctrl_info.param.is.rx_suspend_resume = 1;
                                else
                                    capi_lane_ctrl_info.param.is.tx_suspend_resume = 1;
                            }
                            break;
                        default:
                            break;
                    }

#if 0
                    dprintf("\n\nChoose lane direction: 0 - Ingress (Serdes Tx); 1 - Egress (Serdes Rx)\r\n");
                    scanf("%d", &usr_input);
                    if (usr_input > DIR_EGRESS) {
                        dprintf("\r\nIncorrect selection, defaults to Ingress\r\n");
                        usr_input = DIR_INGRESS;
                    }
                    direction = (capi_direction_t) usr_input;
#endif

                    for (idx = 0; idx < max_lanes; ++idx) {
                        if ((lane_mask & (1 << idx)) == 0)
                            continue;
                        capi_phy.lane_mask = (1 << idx);

#if 0
                        util_memset((void*)&capi_lane_ctrl_info, 0, sizeof(capi_lane_ctrl_info_t));
                        //capi_lane_ctrl_info.type.ctrl_info.direction = (capi_direction_t) usr_input;

                        /* Retrieve whether lane is squelch enabled */
                        //capi_lane_ctrl_info.type.ctrl_info.lane_ctrl = LANE_SQUELCH_ON;
                        if (direction == DIR_EGRESS) {
                            capi_lane_ctrl_info.param.is.egress_squelch = 1;
                        }
                        else {
                            capi_lane_ctrl_info.param.is.ingress_squelch = 1;
                        }
#endif

                        ret_result = capi_get_lane_ctrl_info(&capi_phy, &capi_lane_ctrl_info);
                        if (ret_result != RR_SUCCESS)
                            dprintf("\n\n***** Failed to Get %s Lane %d Control Info! return_result = %d *****\r\n", core_ip, idx, ret_result);
                        else
                        {
                            dprintf("\n\n\n***** Successfully Get %s Lane %d Control Info! *****\r\n", core_ip, idx);

                            switch ((test_lane_ctrl_e) test_item)
                            {
                                case SQUELCH:
                                    {
                                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info egress squelch = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.tx_squelch);
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress squelch = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.rx_squelch);
                                        }
                                        else
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info egress squelch = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.rx_squelch);
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress squelch = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.tx_squelch);
                                        }
                                    }

                                    break;

                                //case TRAFFIC:
                                case LANE_RESET:
                                    {
                                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                        {
                                            dprintf("--- %s Lane Control Info egress lane_reset = %d  \r\n",
                                                    core_ip, capi_lane_ctrl_info.cmd_value.is.tx_lane_cfg_reset);
                                            dprintf("--- %s Lane Control Info ingress lane_reset = %d  \r\n",
                                                    core_ip, capi_lane_ctrl_info.cmd_value.is.rx_lane_cfg_reset);
                                        }
                                        else
                                        {
                                            dprintf("--- %s Lane Control Info egress lane_reset = %d  \r\n",
                                                        core_ip, capi_lane_ctrl_info.cmd_value.is.rx_lane_cfg_reset);
                                            dprintf("--- %s Lane Control Info ingress lane_reset = %d  \r\n",
                                                        core_ip, capi_lane_ctrl_info.cmd_value.is.tx_lane_cfg_reset);
                                        }
                                    }
                                    break;


                                case TRAFFIC:
                                    {
                                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info egress traffic = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.tx_data_path);
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress traffic = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.rx_data_path);
                                        }
                                        else
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info egress traffic = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.rx_data_path);
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress traffic = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.tx_data_path);
                                        }
                                    }
                                    break;


                                case ELECTRIC_IDLE:
#if 0
                                    if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                        dprintf("--- %s Lane Control Info egress electric idle = %d  \r\n",
                                                    core_ip, capi_lane_ctrl_info.cmd_value.is.rx_electric_idle);
#endif
                                    {
                                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                            dprintf("--- %s Lane Control Info ingress electric idle = %d  \r\n",
                                                        core_ip, capi_lane_ctrl_info.cmd_value.is.tx_electric_idle);
                                        }
                                        else
                                        {
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress electric idle = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.tx_electric_idle);
                                        }
                                    }
                                    break;

                                case DATAPATH_POWER:
                                    {
                                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info egress power = %d  \r\n",
                                                        core_ip, capi_lane_ctrl_info.cmd_value.is.tx_datapath_power);
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress power = %d  \r\n",
                                                        core_ip, capi_lane_ctrl_info.cmd_value.is.rx_datapath_power);
                                        }
                                        else
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info egress power = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.rx_datapath_power);
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH))
                                                dprintf("--- %s Lane Control Info ingress power = %d  \r\n",
                                                            core_ip, capi_lane_ctrl_info.cmd_value.is.tx_datapath_power);
                                        }
                                    }
                                    break;

                                case IGNORE_FAULT:
                                    dprintf("--- %s Lane Control Info ingress ignore_fault = %d  \r\n",
                                                core_ip, capi_lane_ctrl_info.cmd_value.is.ignore_fault);
                                    break;

                                case SUSPEND_RESUME:
                                    {
                                        if (capi_phy.core_ip == CORE_IP_MEDIA_DSP || capi_phy.core_ip == CORE_IP_HOST_DSP)
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                                if (capi_lane_ctrl_info.param.is.tx_suspend_resume == 1)
                                                    dprintf("--- %s Lane Control Info TX suspend/resume = %d  \r\n",
                                                                core_ip, capi_lane_ctrl_info.cmd_value.is.tx_suspend_resume);
                                            }
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                                if (capi_lane_ctrl_info.param.is.rx_suspend_resume == 1)
                                                    dprintf("--- %s Lane Control Info RX suspend/resume = %d  \r\n",
                                                                core_ip, capi_lane_ctrl_info.cmd_value.is.rx_suspend_resume);
                                            }
                                        }
                                        else
                                        {
                                            if ((direction == DIR_EGRESS) || (direction == DIR_BOTH)) {
                                                if (capi_lane_ctrl_info.param.is.rx_suspend_resume == 1)
                                                    dprintf("--- %s Lane Control Info RX suspend/resume = %d  \r\n",
                                                                core_ip, capi_lane_ctrl_info.cmd_value.is.rx_suspend_resume);
                                            }
                                            if ((direction == DIR_INGRESS) || (direction == DIR_BOTH)) {
                                                if (capi_lane_ctrl_info.param.is.tx_suspend_resume == 1)
                                                    dprintf("--- %s Lane Control Info TX suspend/resume = %d  \r\n",
                                                                core_ip, capi_lane_ctrl_info.cmd_value.is.tx_suspend_resume);
                                            }
                                        }
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
                break;



            case CAPI_GET_USR_DIAG:
                cli_get_usr_diag(&capi_phy);
                break;


            case CAPI_GET_DSP_SLICER_HISTOGRAM:
                if (capi_phy.core_ip != CORE_IP_MEDIA_DSP)
                    dprintf("Error: core ip is not supported\n");
                else
                    cli_get_dsp_slicer_shistogram(&capi_phy);
                break;

        default:
            break;
    }
    test_case = CAPI_TEST_MAX;
    }while (1);
    return RR_SUCCESS;
    dprintf("\n****************************************\r\n");
}

