﻿#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "access.h"
#include "type_defns.h"
#include "hr_time.h"

#include "common_def.h"
#include "regs_common.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "capi.h"
#include "common_util.h"
#include "top_test.h"
#include "fw_gp_reg_map.h"
#include "capi_diag_def.h"
#include "host_diag.h"

#define MAX_SIZE_SRAM (0x80000/4)
#define MAX_SIZE_SPI (0x80000/4)
uint32_t wholeimage_sram_hex[MAX_SIZE_SRAM];
uint32_t wholeimage_spi_hex[MAX_SIZE_SPI];

char * images_path = "../images/";

static return_result_t get_image_hex(char* filename, uint32_t* image, uint32_t* size)
{
    FILE* file;
    uint32_t size_of_32bit = 0;
    char ch;
    char lineBuffer[200];
    uint32_t wordBuffer[4];
    char byteCharBuffer[2];
    uint32_t line_count =0;
    uint32_t byte_count =0;
    uint32_t char_count = 0;

    if ((filename == NULL) || (image == NULL) || (size == NULL)) {
        return (RR_ERROR_WRONG_INPUT_VALUE);
    }
    if ((file = fopen(filename, "r")) == NULL) {
        return (RR_ERROR);
    } 
    else {
        ch = getc(file);

        while (!feof(file)) {
            memset((void*)lineBuffer, 0, 200);
            while ((ch != '\n')) {

                if (lineBuffer == NULL) {
                    return (RR_ERROR);
                }


                if(ch!=' ') {
                    lineBuffer[line_count] = ch;
                    byteCharBuffer[char_count] = ch;

                    line_count++;
                    char_count++;
                    if((line_count%2) == 0){
                        sscanf(byteCharBuffer, "%x", &wordBuffer[byte_count]);
                        byte_count++;
                        char_count= 0;
                    }
                    if((line_count % 8) == 0 ){
                        /* dprintf("word:0:%x, 1:%x, 2:%x, 3:%x\n", wordBuffer[0], wordBuffer[1], wordBuffer[2], wordBuffer[3]);*/
                        image[size_of_32bit]= wordBuffer[3]<<24 | wordBuffer[2] << 16 | wordBuffer[1] << 8 | wordBuffer[0];
                        /* dprintf("size of 32:%x, data:0x%08x\n", size_of_32bit, image[size_of_32bit]);*/
                        size_of_32bit++;
                        byte_count =0;
                    }
                }
                ch = getc(file);
            }/*while ((ch != '\n')) */
            line_count = 0;
            ch = getc(file);
        } /*while(!feof(file))*/

    }

    *size = size_of_32bit;
    fclose(file);

    return (RR_SUCCESS);
}

static return_result_t get_image_bin(char* filename, uint32_t* image, uint32_t* size)
{
    FILE* file;
    uint32_t counter = 0;
    uint32_t my_record;
      
    if ((filename == NULL) || (image == NULL) || (size == NULL)){      
        return (RR_ERROR_WRONG_INPUT_VALUE);
    }
    if ((file = fopen(filename, "rb")) == NULL) {
        return (RR_ERROR);
    }
    counter = 0;
    while (!feof(file)) 

    {
        fread(&my_record,sizeof(uint32_t),1,file);
        image[counter] = my_record;    
       /*dprintf("counter:%x, data:%x\n", counter, image[counter]);*/
        counter++;
    }

    *size = counter-1;
    fclose(file);

    return (RR_SUCCESS);
}

static return_result_t test_download_hexfile(capi_phy_info_t* phy_info_ptr,
    capi_download_info_t* download_info_ptr) {
    uint32_t i = 0;

    char filepath[200] = {0};
    char * image_name;
    return_result_t ret = RR_SUCCESS;
    uint32_t size;

    switch (download_info_ptr->mode) 
    {
        case CAPI_DOWNLOAD_MODE_MDIO_EEPROM:
        case CAPI_DOWNLOAD_MODE_I2C_EEPROM:
            for (i = 0; i < MAX_SIZE_SPI; i++) {
                wholeimage_spi_hex[i] = 0;
            }
            image_name = "whole_image_spi.hex";
            strncat(filepath, images_path, strlen(images_path));
            strncat(filepath, image_name, strlen(image_name));
            ret = get_image_hex(filepath, wholeimage_spi_hex, &size);
            dprintf("spi size:%x\n", size);
            if (ret != RR_SUCCESS)
                return ret;
            download_info_ptr->image_info.image_ptr = &wholeimage_spi_hex[0];
            download_info_ptr->image_info.image_size = size << 2;

            capi_download(phy_info_ptr, download_info_ptr);
            break;

        case CAPI_DOWNLOAD_MODE_MDIO_SRAM:
        case CAPI_DOWNLOAD_MODE_I2C_SRAM:
        default:
            for (i = 0; i < MAX_SIZE_SRAM; i++) {
                wholeimage_sram_hex[i] = 0;
            }
            image_name = "whole_image_sram.hex";
            strncat(filepath, images_path, strlen(images_path));
            strncat(filepath, image_name, strlen(image_name));
            ret = get_image_hex(filepath, wholeimage_sram_hex, &size);
            if (ret != RR_SUCCESS)
                return ret;
            download_info_ptr->image_info.image_ptr = &wholeimage_sram_hex[0];
            download_info_ptr->image_info.image_size = size << 2;
            capi_download(phy_info_ptr, download_info_ptr); 
            break;
    }
    return RR_SUCCESS;
}

static return_result_t test_download_binfile(capi_phy_info_t* phy_info_ptr,   capi_download_info_t* download_info_ptr)
{
    uint32_t i =0;

    char filepath[200] = {0};
    char * image_name;
    return_result_t ret = RR_SUCCESS;
    uint32_t size;

    switch (download_info_ptr->mode) {

        case CAPI_DOWNLOAD_MODE_MDIO_EEPROM:
        case CAPI_DOWNLOAD_MODE_I2C_EEPROM:
            for (i = 0; i < MAX_SIZE_SPI; i++) {
                wholeimage_spi_hex[i] = 0;
            }
            image_name = "whole_image_spi.bin";
            strncat(filepath, images_path, strlen(images_path));
            strncat(filepath, image_name, strlen(image_name));
            ret = get_image_bin(filepath, wholeimage_spi_hex, &size);
            dprintf("spi size:%x\n", size);
            if (ret != RR_SUCCESS)
                return ret;
            download_info_ptr->image_info.image_ptr = &wholeimage_spi_hex[0];
            download_info_ptr->image_info.image_size = size << 2;
            capi_download(phy_info_ptr, download_info_ptr); 
            break;

        case CAPI_DOWNLOAD_MODE_MDIO_SRAM:
        case CAPI_DOWNLOAD_MODE_I2C_SRAM:
        default:
            for (i = 0; i < MAX_SIZE_SRAM; i++) {
                wholeimage_sram_hex[i] = 0;
            }
            image_name = "whole_image_sram.bin";
            strncat(filepath, images_path, strlen(images_path));
            strncat(filepath, image_name, strlen(image_name));
            ret = get_image_bin(filepath, wholeimage_sram_hex, &size);
            if (ret != RR_SUCCESS)
                return ret;
            download_info_ptr->image_info.image_ptr = &wholeimage_sram_hex[0];
            download_info_ptr->image_info.image_size = size << 2;
            capi_download(phy_info_ptr, download_info_ptr); 
        break;
    }
    return RR_SUCCESS;
}

bool test_sram_download(capi_phy_info_t* capi_phy_in_ptr, uint32_t skip_input) {
    int op_code = 0;
    capi_phy_info_t capi_phy;
    phy_info_t phy_info;
    phy_static_config_t centenario_static_config;
    uint32_t iter = 0;
    uint32_t kk=0;
    uint32_t block_size = 0;

    time_t the_time;
    char time_str[64];
    struct tm* ptm;
    capi_download_info_t download_info;
    return_result_t ret = RR_ERROR;
    capi_chip_info_t capi_chip_info = { 0 };
    capi_broadcast_intf_t capi_broadcast_intf;
    uint32_t sram_mode=0;
    uint32_t broadcast_mode = 0;

     centenario_static_config.avs_enable=0;
     centenario_static_config.chip_default_mode = (capi_default_mode_t) 0;
     memset((void*)&download_info,  0, sizeof(capi_download_info_t));
     memset((void*)&capi_broadcast_intf,  0, sizeof(capi_broadcast_intf_t));
     memset((void*)&capi_phy,  0, sizeof(capi_phy_info_t));
     
     phy_info.phy_id = capi_phy_in_ptr->phy_id ;
     capi_chip_info.param.is.hw_info = 1;
     capi_get_chip_info(&phy_info, &capi_chip_info);
     dprintf("\n\nChip ID:%x :\r\n", capi_chip_info.value.hw_info.chip_id);
    
     dprintf("\n\nPlease confirm if download firmware: 0- Don't download; 1:download \r\n");
     if (skip_input == 0)
         scanf("%d", &op_code);
     else
         op_code = 1;

    if (op_code > 1) {
        dprintf("\n\nWrong choose %d  \r\n", op_code);
    }

    if (op_code == 0 || op_code > 1)
        return 0;

    capi_phy.i2c_address =0;

    dprintf("\n\nPlease confirm if download format 0- .h header file; 1: hex file 2: bin file \r\n");
    if (skip_input == 0)
        scanf("%d", &op_code);
    else
        op_code = 0;
#if 1 /*SW does not support*/
    dprintf("\n\nPlease confirm if download mode: 0: standard mode, 1: fast mode (only if support  block write) \r\n");

    sram_mode = 0;
    if (skip_input == 0) {
        scanf("%d", &sram_mode); 
    }
    else {
        sram_mode=1;
       
    }

    if (sram_mode == 1) {
        download_info.sram_program_mode = CAPI_SRAM_PROGRAM_FAST;
        dprintf("\n\nPlease confirm the block size \r\n");

        block_size = 0x800;
#if 0
        if (skip_input == 0) {
            scanf("%x", &block_size); 
        }
        else {
            block_size=0x100;
       
        }
#endif
        capi_phy.i2c_block_write_size =block_size;
        download_info.burst_write_mode=CAPI_BURST_WRITE_MODE_MDIO;  
    }
#endif
    dprintf("\n\nPlease confirm if broadcast mode(only duel die for 87580/87582): 0: standard mode, 1: broadcast \r\n");

    broadcast_mode = 0;
    if (skip_input == 0) {
        scanf("%d", &broadcast_mode); 
        iter=1;
    }
    else {
        broadcast_mode=1;
        iter=2;
       
    }
    if (broadcast_mode ==1) iter =2;
#if 0
    else {
        if(capi_chip_info.value.hw_info.chip_id==0x87580){
            iter =2;
        }
        else {
            iter =1;
        }
    }
#endif

    for (kk =0; kk< iter; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            capi_pre_sram_download(&capi_phy);
    }
    
   // if((broadcast_mode==1) || (capi_chip_info.value.hw_info.chip_id==0x87580)){
     if((broadcast_mode==1)) {
       
        capi_broadcast_intf.bus = CAPI_ACCESS_MODE_MDIO;
        capi_broadcast_intf.enable = BROADCAST_ENABLE;
        for (kk =0; kk< 2; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            capi_set_broadcast(&capi_phy, &capi_broadcast_intf);
        }
        for (kk =0; kk< 2; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            if (capi_get_broadcast(&capi_phy)==BROADCAST_DISABLE){
                return(true);//Wendy?
            };
        }
    }


    time(&the_time);
   
    ptm = localtime(&the_time);
    strftime(time_str, sizeof(time_str), ".%Y%m%dT%H%M%S", ptm);
    dprintf("time:%s\n", time_str);

    
    capi_phy.phy_id = capi_phy_in_ptr->phy_id;
    download_info.mode = CAPI_DOWNLOAD_MODE_MDIO_SRAM;
    download_info.image_info.image_ptr = NULL;
    download_info.image_info.image_size = 0;
    centenario_static_config.chip_default_mode = CAPI_NO_DEFAULT_MODE;
    download_info.phy_static_config_ptr=&centenario_static_config;

    if (op_code == 0) {
            capi_download(&capi_phy, &download_info);
    }
    else if (op_code == 1) {
            test_download_hexfile(&capi_phy, &download_info);
    }
    else {
            test_download_binfile(&capi_phy, &download_info);
    }
    //if((broadcast_mode==1) || (capi_chip_info.value.hw_info.chip_id==0x87580)){
    if((broadcast_mode==1)){
        for (kk =0; kk< 2; kk++) {
                capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
                if (capi_get_broadcast(&capi_phy)==BROADCAST_DISABLE){
                    return(true);//Wendy?
                };
         }
    }
    time(&the_time);
    ptm = localtime(&the_time);
    strftime(time_str, sizeof(time_str), ".%Y%m%dT%H%M%S", ptm);
    dprintf("time:%s\n", time_str);
    delay_ms(100);
    if((broadcast_mode==0)){
    //if((broadcast_mode==0) && (capi_chip_info.value.hw_info.chip_id!=0x87580)){
        
        dprintf("\n\n capi_get_download_status phy: %d  \r\n", capi_phy.phy_id);
         ret = capi_get_download_status(&capi_phy, &download_info);
        if (ret) {
            dprintf("\n\n capi_get_download_status failed: %d  \r\n", ret);
            return false;
        } else {
             dprintf("\n\n capi_get_download_crc : %x  \r\n", download_info.status.crc_check);
             dprintf("FW download complete for chip (version: %x)\n",download_info.status.version);
        }
    }
    else {
     
        capi_broadcast_intf.bus = CAPI_ACCESS_MODE_MDIO;
        capi_broadcast_intf.enable = BROADCAST_DISABLE;
        for (kk =0; kk< 2; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            capi_set_broadcast(&capi_phy, &capi_broadcast_intf);
        }
        for (kk =0; kk< 2; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            if (capi_get_broadcast(&capi_phy)==BROADCAST_ENABLE){
                return(true);
            };
        }
        for (kk =0; kk< 2; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            dprintf("\n\n capi_get_download_status phy: %d  \r\n", capi_phy.phy_id);
             ret = capi_get_download_status(&capi_phy, &download_info);
            if (ret) {
                dprintf("\n\n capi_get_download_status failed: %d  \r\n", ret);
                return false;
            } else {
                 dprintf("\n\n capi_get_download_crc : %x  \r\n", download_info.status.crc_check);
                 dprintf("FW download complete for chip (version: %x)\n",download_info.status.version);
            }
        }
    }
	return(true); 
}

#if 0

bool test_sram_download(capi_phy_info_t* capi_phy_in_ptr, uint32_t skip_input) {
    int op_code = 0;
    uint32_t i2cmdio_code =0;
    capi_phy_info_t capi_phy;
    phy_info_t phy_info;
    uint32_t iter = 0;
    uint32_t kk=0;
    uint32_t block_size = 0;

    time_t the_time;
    char time_str[64];
    struct tm* ptm;
    capi_download_info_t download_info;
    return_result_t ret = RR_ERROR;
    capi_chip_info_t capi_chip_info;
    capi_broadcast_intf_t capi_broadcast_intf;
    uint32_t sram_mode=0;
    uint32_t broadcast_mode = 0;

    memset((void*)&download_info,  0, sizeof(capi_download_info_t));
    memset((void*)&capi_broadcast_intf,  0, sizeof(capi_broadcast_intf_t));
    memset((void*)&capi_phy,  0, sizeof(capi_phy_info_t));

    phy_info.phy_id = capi_phy_in_ptr->phy_id ;
    memset(&capi_chip_info, 0, sizeof(capi_chip_info_t));
    capi_chip_info.param.is.hw_info = 1;
    capi_get_chip_info(&phy_info, &capi_chip_info);
    dprintf("\n\nChip ID:%x :\r\n", capi_chip_info.value.hw_info.chip_id);

    dprintf("\n\nPlease confirm firmware download: 0 - No; 1 - Yes\r\n");
    if (skip_input == 0)
        scanf("%d", &op_code);
    else
        op_code = 1;

    if (op_code != 1) {
        dprintf("\r\nNot proceeding to image download\r\n");
        return true;
    }

    capi_phy.i2c_address =0;

    dprintf("\n\nPlease confirm image format: 0 - header file; 1 - hex file; 2 - bin file\r\n");
    if (skip_input == 0)
        scanf("%d", &op_code);
    else
        op_code = 0;
#if 1 /*SW does not support*/
    dprintf("\n\nPlease confirm if download mode: 0 - standard mode, 1 - fast mode (if block write is supported)\r\n");

    sram_mode = 0;
    if (skip_input == 0) {
        scanf("%d", &sram_mode); 
    }
    else {
        sram_mode = 1;
    }

    if (sram_mode == 1) {
        download_info.sram_program_mode = CAPI_SRAM_PROGRAM_FAST;
#if 0
        dprintf("\n\nPlease confirm the block size \r\n");

        block_size = 0;
        if (skip_input == 0) {
            scanf("%x", &block_size); 
        }
        else {
            block_size=0x100;
       
        }
#endif
        block_size=0x2000;
        capi_phy.i2c_block_write_size =block_size;
    }
    dprintf("\n\nPlease confirm if I2C (0) or MDIO (1)\r\n");
    op_code = 0;

    if (skip_input == 0) {
        scanf("%d", &i2cmdio_code); 
    }
    else {
        i2cmdio_code = 0;
    }

    if(i2cmdio_code == 1) {
       download_info.burst_write_mode=CAPI_BURST_WRITE_MODE_MDIO;
    }
    else {
       download_info.burst_write_mode=CAPI_BURST_WRITE_MODE_I2C;
    }
#endif

    iter = 1;
    for (kk =0; kk< iter; kk++) {
            capi_phy.phy_id = capi_phy_in_ptr->phy_id + kk * 2 ;
            capi_pre_sram_download(&capi_phy);
    }

    time(&the_time);

    ptm = localtime(&the_time);
    strftime(time_str, sizeof(time_str), ".%Y%m%dT%H%M%S", ptm);
    dprintf("time:%s\n", time_str);

    capi_phy.phy_id = capi_phy_in_ptr->phy_id;
    download_info.mode = CAPI_DOWNLOAD_MODE_MDIO_SRAM;
    download_info.image_info.image_ptr = NULL;
    download_info.image_info.image_size = 0;

    if (op_code == 0) {
        ret = capi_download(&capi_phy, &download_info);
    }
    else if (op_code == 1) {
        test_download_hexfile(&capi_phy, &download_info);
    }
    else {
        test_download_binfile(&capi_phy, &download_info);
    }

    time(&the_time);
    ptm = localtime(&the_time);
    strftime(time_str, sizeof(time_str), ".%Y%m%dT%H%M%S", ptm);
    dprintf("time:%s\n", time_str);
    delay_ms(300);
    if(ret != RR_SUCCESS) {
         dprintf("\n\n capi_download failed %d  \r\n", ret);
    }
    dprintf("\n\n capi_get_download_status phy: %d  \r\n", capi_phy.phy_id);
        ret = capi_get_download_status(&capi_phy, &download_info);
    if (ret) {
        dprintf("\n\n capi_get_download_status failed: %d  \r\n", ret);        
        dprintf("RR_ERROR_FW_VERSION_MISMATCH =21\n");
        dprintf("RR_ERROR_CRC32_MISMATCH = 22\n");
        dprintf("RR_ERROR_CRC32_SPI_MISMATCH = 23\n");
        return false;
    } 
    else {
        dprintf("\n\n capi_get_download_crc : %x  \r\n", download_info.status.crc_check);
        dprintf("FW download complete for chip (version: %x)\n",download_info.status.version);
        dprintf("FW download complete for chip (version: %x)\n",download_info.status.sub_version);
    }
    return(true); 
}
#endif
bool test_spi_download(capi_phy_info_t* capi_phy_in_ptr, uint32_t skip_input)
{
    int op_code = 0;
    uint32_t i2cmdio_code =0;
    capi_reset_mode_t reset_mode;
    capi_phy_info_t capi_phy, reset_phy={0};
    time_t the_time;
    char time_str[64];
    struct tm* ptm;
    capi_download_info_t download_info;
    capi_chip_status_info_t chip_status_info;
    return_result_t ret = RR_ERROR;
    uint32_t spi_mode =0;

    dprintf("\n\nPlease confirm if download format 0- .h header file; 1: hex file 2: bin file \r\n");
    if (skip_input == 0)
        scanf("%d", &op_code);
    else
        op_code = 0;

    memset(&download_info, 0, sizeof(capi_download_info_t));
    download_info.mode = CAPI_DOWNLOAD_MODE_MDIO_EEPROM;
    download_info.image_info.image_ptr = NULL;
    download_info.image_info.image_size = 0;
    dprintf("\n\nPlease confirm if download mode: 0: standard mode, 1: fast mode (only if support  block write) \r\n");
    if (skip_input == 0)
        scanf("%d", &spi_mode);
    else
        spi_mode = 1;

    if(spi_mode==1){
        download_info.spi_program_mode = CAPI_SPI_PROGRAM_FAST;
#if 0
        dprintf("\n\nPlease confirm block write buffer size (must be diviable by 256 bytes in hex \r\n");   
        scanf("%x", &spi_mode);
        capi_phy.i2c_block_write_size = spi_mode;
#endif
        capi_phy.i2c_block_write_size = 0x2000;
    }
    else {
        download_info.spi_program_mode = CAPI_SPI_PROGRAM_STANDARD;
        capi_phy.i2c_block_write_size = 0;
    }
    dprintf("\n\nPlease confirm if I2C (0) or MDIO (1)\r\n");
    if (skip_input == 1) {
        i2cmdio_code = 0;
    }
    else {
        scanf("%d", &i2cmdio_code);
    }
    if(i2cmdio_code==1){
       download_info.burst_write_mode=CAPI_BURST_WRITE_MODE_MDIO;
        
    }
    else {
       download_info.burst_write_mode=CAPI_BURST_WRITE_MODE_I2C;
       
    }

    capi_phy.i2c_address =0;
    time(&the_time);
    ptm = localtime(&the_time);
    strftime(time_str, sizeof(time_str), ".%Y%m%dT%H%M%S", ptm);
    dprintf("time:%s\n", time_str);

    capi_phy.phy_id = capi_phy_in_ptr->phy_id;

    if (op_code == 0) {
            capi_download(&capi_phy, &download_info);
    }
    else if (op_code == 1) {
            test_download_hexfile(&capi_phy, &download_info);
    }
    else {
            test_download_binfile(&capi_phy, &download_info);
    }

    time(&the_time);
    ptm = localtime(&the_time);
    strftime(time_str, sizeof(time_str), ".%Y%m%dT%H%M%S", ptm);
    dprintf("time:%s\n", time_str);

    reset_phy.phy_id = capi_phy_in_ptr->phy_id;
    reset_mode =  CAPI_HARD_RESET_MODE;
    capi_reset(&reset_phy, &reset_mode);

    capi_phy.phy_id = capi_phy_in_ptr->phy_id;
    util_memset(&chip_status_info, 0, sizeof(chip_status_info));
    chip_status_info.param.is.uc_ready = 1;
    ret = capi_get_chip_status(&capi_phy, &chip_status_info);
    if (ret)
        dprintf("capi_get_chip_status uc_ready failed\n");

    capi_phy.phy_id = capi_phy_in_ptr->phy_id;
    ret = capi_get_download_status(&capi_phy, &download_info);
    if (ret) {
        dprintf("\n\n capi_get_download_status failed: %d  \r\n", ret);
        dprintf("RR_ERROR_FW_VERSION_MISMATCH = %d\n", RR_ERROR_FW_VERSION_MISMATCH);
        dprintf("RR_ERROR_CRC32_MISMATCH = %d\n", RR_ERROR_CRC32_MISMATCH);
        dprintf("RR_ERROR_CRC32_SPI_MISMATCH = %d\n", RR_ERROR_CRC32_SPI_MISMATCH);
        return false;
    } else {
        dprintf("\n\n capi_get_download_crc : %x  \r\n", download_info.status.crc_check);
        dprintf("FW download complete for chip (version: %x)\n",download_info.status.version);
        dprintf("FW download complete for chip (version: %x)\n",download_info.status.sub_version);
    }

    if (fw_read_chip_id() == 0x87580 || fw_read_chip_id() == 0x87582) {
        reset_phy.phy_id = capi_phy_in_ptr->phy_id + 2;
        reset_mode =  CAPI_HARD_RESET_MODE;
        capi_reset(&reset_phy, &reset_mode);
        capi_phy.phy_id = capi_phy_in_ptr->phy_id + 2;
        util_memset(&chip_status_info, 0, sizeof(chip_status_info));
        chip_status_info.param.is.uc_ready = 1;
        ret = capi_get_chip_status(&capi_phy, &chip_status_info);
        if (ret)
            dprintf("capi_get_chip_status uc_ready failed\n");
        capi_phy.phy_id = capi_phy_in_ptr->phy_id + 2;
        ret = capi_get_download_status(&capi_phy, &download_info);
        if (ret) {
            dprintf("\n\n capi_get_download_status failed: %d  \r\n", ret);
            dprintf("RR_ERROR_FW_VERSION_MISMATCH = %d\n", RR_ERROR_FW_VERSION_MISMATCH);
            dprintf("RR_ERROR_CRC32_MISMATCH = %d\n", RR_ERROR_CRC32_MISMATCH);
            dprintf("RR_ERROR_CRC32_SPI_MISMATCH = %d\n", RR_ERROR_CRC32_SPI_MISMATCH);
            return false;
        } else {
            dprintf("\n\n capi_get_download_crc : %x  \r\n", download_info.status.crc_check);
            dprintf("FW download complete for chip (version: %x)\n",download_info.status.version);
            dprintf("FW download complete for chip (version: %x)\n",download_info.status.sub_version);
        }
    }
    return true;
}

void test_download_status(capi_phy_info_t* capi_phy_in_ptr) {

    int op_code = 0;
    capi_phy_info_t capi_phy;
    phy_info_t phy_info;
    phy_static_config_t static_config;
    capi_status_info_t status_info;

    capi_download_info_t download_info;
    return_result_t ret = RR_ERROR;
    uint32_t sram_mode = 0;

    static_config.avs_enable = 0;
    static_config.chip_default_mode = (capi_default_mode_t) 0;
    memset((void*)&download_info, 0, sizeof(capi_download_info_t));
    memset((void*)&capi_phy,  0, sizeof(capi_phy_info_t));
    phy_info.phy_id = capi_phy_in_ptr->phy_id ;

    capi_phy.phy_id = capi_phy_in_ptr->phy_id;
    download_info.mode = CAPI_DOWNLOAD_MODE_MDIO_SRAM;
    download_info.image_info.image_ptr = NULL;
    download_info.image_info.image_size = 0;
    static_config.chip_default_mode = CAPI_NO_DEFAULT_MODE;
    download_info.phy_static_config_ptr=&static_config;
  
    ret = capi_get_download_status(&capi_phy, &download_info);
    if (ret) {
        dprintf("\n\n capi_get_download_status failed: %d  \r\n", ret);
        dprintf("RR_ERROR_FW_VERSION_MISMATCH =21\n");
        dprintf("RR_ERROR_CRC32_MISMATCH = 22\n");
        dprintf("RR_ERROR_CRC32_SPI_MISMATCH = 23\n");
        return;
    } else {
        dprintf("\n\n capi_get_download_crc : %x  \r\n", download_info.status.crc_check);
        dprintf("FW download complete for chip (version: %x)\n",download_info.status.version);
        dprintf("FW download complete for chip (version: %x)\n",download_info.status.sub_version);
    }
    ret = capi_get_firmware_status(&capi_phy, &status_info);
    if (ret) {
        dprintf("\n\n capi_get_firmware_status failed: %d  \r\n", ret);
    } else {
         dprintf("\n\n capi_get_firmware_crc : %x  \r\n", status_info.crc_check);
         dprintf("\n\n capi_get_firmware (version: %x)\n",status_info.version);
         dprintf("\n\n capi_get_firmware success result : %x  \r\n", status_info.result);
    }
}

void test_soft_reset(capi_phy_info_t* capi_phy_ptr, uint8_t mode)
{
    capi_reset_mode_t reset_mode;
    capi_phy_info_t capi_phy;
    uint32_t op_code =0;

    capi_phy.phy_id = capi_phy_ptr->phy_id;

    if(mode == 0) {
        reset_mode = CAPI_HARD_RESET_MODE;
    }
    else {
        reset_mode = CAPI_SOFT_RESET_MODE;
    }
    capi_reset(&capi_phy, &reset_mode);
}

void chip_avs_config(capi_phy_info_t* capi_phy_in_ptr) {
    return_result_t ret = RR_ERROR;
    uint32_t input = 0;
    capi_temp_status_info_t capi_temp_status_info;
    capi_voltage_status_info_t capi_voltage_status_info;
    capi_avs_mode_config_info_t capi_avs_mode_config;
    capi_avs_status_t capi_avs_status;
    capi_chip_info_t chip_info;
    capi_phy_info_t capi_phy;
    phy_info_t phy_info;
    uint32_t chip_id = 0;
    uint32_t regulator_type=0;

    capi_phy.phy_id = capi_phy_in_ptr->phy_id ;

    util_memset(&capi_avs_mode_config, 0x0, sizeof(capi_avs_mode_config_info_t));
    util_memset(&chip_info, 0, sizeof(capi_chip_info_t));
    chip_info.param.is.hw_info = 1;
    capi_get_chip_info(&capi_phy, &chip_info);
    chip_id = chip_info.value.hw_info.chip_id;
    printf("\n\n***Please choose AVS enable/disable: 0-Disable AVS; 1- Enable AVS\r\n");
                    scanf("%d", &input);

    capi_avs_mode_config.avs = input ? CAPI_ENABLE : CAPI_DISABLE;
    capi_avs_mode_config.disable_type = CAPI_AVS_DISABLE_FIRMWARE_CONTROL;   
    capi_avs_mode_config.type_of_regulator = CAPI_REGULATOR_INTERNAL;
    capi_set_avs_config(&capi_phy, &capi_avs_mode_config);
    do {
        ret = capi_get_avs_status(&capi_phy, &capi_avs_status);
        dprintf("wait\n");
        delay_ms(1000);
    } while ((ret!=RR_SUCCESS) || (capi_avs_status.avs_result!= CAPI_INIT_DONE_SUCCESS));

    if(capi_avs_status.avs_result == CAPI_INIT_DONE_SUCCESS) {
          dprintf("AVS config is succesfully\n");
    }
    else {
           dprintf("AVS  config is Unsuccesfully\n");
    }
    phy_info.phy_id =capi_phy_in_ptr->phy_id;
    capi_get_temperture_status(&phy_info,  &capi_temp_status_info);
    dprintf("Temperature:%d\n", capi_temp_status_info.temperature);
    capi_get_voltage_status(&phy_info,  &capi_voltage_status_info);
    dprintf("avs volatge:%d\n", capi_voltage_status_info.voltage);
}

void chip_avs_get_status(capi_phy_info_t* capi_phy_in_ptr) {
    return_result_t ret = RR_ERROR;
    capi_temp_status_info_t capi_temp_status_info;
    capi_voltage_status_info_t capi_voltage_status_info;
    capi_avs_mode_config_info_t capi_avs_mode_config;
    capi_avs_status_t capi_avs_status;
    uint32_t chip_id = 0;
    uint32_t iter = 0;
    uint32_t i =0;
    capi_chip_info_t chip_info;

    capi_phy_info_t capi_phy;
    phy_info_t phy_info;

    capi_phy.phy_id = capi_phy_in_ptr->phy_id ;
    util_memset(&chip_info, 0, sizeof(capi_chip_info_t));
    chip_info.param.is.hw_info = 1;
    capi_get_chip_info(&capi_phy, &chip_info);
    chip_id = chip_info.value.hw_info.chip_id;
    util_memset(&capi_avs_mode_config, 0x0, sizeof(capi_avs_mode_config_info_t));
      iter = 1;
    for (i=0; i< iter; i++) {
        capi_phy.phy_id = capi_phy_in_ptr->phy_id  + i*2 ;
        dprintf("phy:%d\n",  capi_phy.phy_id);
        capi_get_avs_config(&capi_phy, &capi_avs_mode_config);

        if(capi_avs_mode_config.avs == CAPI_ENABLE){
             dprintf("AVS is enabled\n");
        }
        else {
            dprintf("AVS is disabled\n");
        }
        do {
            ret = capi_get_avs_status(&capi_phy, &capi_avs_status);
            dprintf("wait\n");
            delay_ms(1000);
        } while((ret!=RR_SUCCESS) || (capi_avs_status.avs_result!= CAPI_INIT_DONE_SUCCESS));

        if(capi_avs_status.avs_result== CAPI_INIT_DONE_SUCCESS) {
              dprintf("AVS Status is succesfully\n");
        }
        else {
            dprintf("AVS status is Unsuccesfully\n");
        }

        phy_info.phy_id =capi_phy_in_ptr->phy_id + i * 2;
        capi_get_temperture_status(&phy_info,  &capi_temp_status_info);
        dprintf("Temperature:%d\n", capi_temp_status_info.temperature);
        capi_get_voltage_status(&phy_info,  &capi_voltage_status_info);
        dprintf("avs volatge:%d\n", capi_voltage_status_info.voltage);
    }
}

void chip_internal_vddm_test(capi_phy_info_t* capi_phy_in_ptr) {
    capi_other_voltage_config_info_t capi_vddm_voltage_config;
    uint32_t fail_cnt =0;
    return_result_t ret;
    uint32_t voltage = 0;
    int disable_dvdd = 0;

    dprintf("Please input voltage in mv (do not enter value beyond +/-3% of the nominal voltage):\n");
    scanf("%d", &voltage);

    util_memset(&capi_vddm_voltage_config, 0, sizeof(capi_other_voltage_config_info_t));
    

    /* first */
    capi_vddm_voltage_config.fixed_voltage_set =voltage; /* mv */
    capi_vddm_voltage_config.fixed_voltage_enable = (capi_enable_t) 0x1;
    capi_vddm_voltage_config.capi_set_voltage_with_fw = disable_dvdd ? CAPI_SET_VOLTAGE_WITH_FIRMWARE : CAPI_SET_VOLTAGE_NO_FIRMWARE;
    ret =  capi_set_0p75v_voltage_config(capi_phy_in_ptr, &capi_vddm_voltage_config);
    if((ret!=RR_SUCCESS)) { 
        dprintf("capi_set_fixed failed\n"); 
        fail_cnt++; 
    }
    else {
        dprintf("capi_set_fixed success\n"); 
        dprintf("capi_get_voltage_status : %x, volatge : %d\n", ret,  capi_vddm_voltage_config.fixed_voltage_read);
    }
}

void chip_internal_avdd_test(capi_phy_info_t* capi_phy_in_ptr) {
    capi_other_voltage_config_info_t capi_avdd_voltage_config;
    uint32_t fail_cnt =0;
    return_result_t ret;
    uint32_t voltage = 0;

    dprintf("Please input voltage in mv (do not enter value beyond +/-3% of the nominal voltage):\n");
    scanf("%d", &voltage);
   
    util_memset(&capi_avdd_voltage_config, 0, sizeof(capi_other_voltage_config_info_t));
    

    /* first */
    capi_avdd_voltage_config.fixed_voltage_set =voltage; /* mv */
    capi_avdd_voltage_config.fixed_voltage_enable = CAPI_ENABLE;
    ret = capi_set_0p9v_voltage_config(capi_phy_in_ptr, &capi_avdd_voltage_config);
    if((ret!=RR_SUCCESS))
    { 
        dprintf(" capi_set_fixed faied\n"); 
        fail_cnt++; 
    }
    else {
        dprintf(" capi_set_fixed success\n"); 
        dprintf("capi_get_voltage_status:%x, volatge%d\n", ret,  capi_avdd_voltage_config.fixed_voltage_read);
    }
}

void test_config_bootup_default(capi_phy_info_t* capi_phy_in_ptr) {
    capi_chip_default_info_t chip_default_info={0};
    uint32_t disable_avs = 0;
    uint32_t disable_regulator = 0;
    return_result_t ret = RR_SUCCESS;

   
    util_memset(&chip_default_info, 0, sizeof(capi_chip_default_info_t));
    dprintf("do you want to disable AVS when bootup\n  0: no change, just use default (AVS is enabled) or \n 1: disable AVS when bootup\n");
    scanf("%d", &disable_avs);

    if(disable_avs==1){
        chip_default_info.module.is.avs = 1;
        chip_default_info.config.avs_default.feature.is.avs_switch=1;
        chip_default_info.config.avs_default.default_config.avs_switch = 1;
    }
   
    dprintf("do you want to disable regulator when bootup\n  0: no change, just use default (use internal regulator) or \n  1: disable internal regulator when bootup (need apply external power supply\n");
    scanf("%d", &disable_regulator);

    if(disable_regulator==1){
        
        chip_default_info.module.is.internal_regulator=1;
        chip_default_info.config.int_regulator.feature.is.int_reg_switch = 1;
        chip_default_info.config.int_regulator.default_conifg.int_reg_switch =1;
    }
    ret = capi_set_chip_default_info(capi_phy_in_ptr, &chip_default_info);
   
    if((ret!=RR_SUCCESS))
    { 
        dprintf(" capi_set_chip_default_info faied\n"); 
		
    }
    else {
        dprintf(" capi_set_chip_default_info success\n"); 
       
    }


}

return_result_t test_cfg_rptr_indep_lane_ctrl(capi_phy_info_t* capi_phy_ptr)
{
    capi_phy_info_t phy_info;
    int test_option;
    capi_chip_command_info_t chip_cmd_info;

    /*read current setting*/
    util_memcpy((void *)&phy_info, capi_phy_ptr, sizeof(capi_phy_info_t));
    util_memset((void *)&chip_cmd_info, 0, sizeof(capi_chip_command_info_t));
    chip_cmd_info.command_id = COMMAND_ID_GET_RPTR_INDEP_HDL;
    phy_info.base_addr = OCTAL_TOP_REGS;
    capi_get_chip_command(&phy_info, &chip_cmd_info);
    dprintf("\n**** Current FW %s 200G/100G one2one repeater mode independent lane control feature\r\n", 
        chip_cmd_info.type.indep_ctrl.indep_ctrl_en?"ENABLE":"DISABLE");

    /*update the new setting*/
    dprintf("*** Config 200G/100G one2one repeater mode independent lane control feature: 0-disable; 1-enable; \r\n");
    scanf("%d", &test_option);
    chip_cmd_info.command_id = COMMAND_ID_SET_RPTR_INDEP_HDL;
    chip_cmd_info.type.indep_ctrl.indep_ctrl_en = test_option?CAPI_ENABLE:CAPI_DISABLE;
    capi_set_chip_command(&phy_info, &chip_cmd_info);

    /*read back the new setting*/
    util_memset((void *)&chip_cmd_info, 0, sizeof(capi_chip_command_info_t));
    chip_cmd_info.command_id = COMMAND_ID_GET_RPTR_INDEP_HDL;
    capi_get_chip_command(&phy_info, &chip_cmd_info);
    dprintf("\n**** Current FW %s 200G/100G one2one repeater mode independent lane control feature\r\n", 
        chip_cmd_info.type.indep_ctrl.indep_ctrl_en?"ENABLE":"DISABLE");
    return RR_SUCCESS;
}


return_result_t test_cfg_line_pll_ddcc_ctrl(capi_phy_info_t* capi_phy_ptr)
{
    capi_phy_info_t phy_info;
    int test_option;
    capi_chip_command_info_t chip_cmd_info;
    uint16_t ddcc_cfg_en, ddcc_polflip, ddcc_frc_clm, ddcc_poldetmodesel, ddcc_grofreqsel;
    uint8_t expected_val[][4]={{1, 0, 1, 8}, {0, 1, 0, 9}};

    /*read current setting*/
    util_memcpy((void *)&phy_info, capi_phy_ptr, sizeof(capi_phy_info_t));
    util_memset((void *)&chip_cmd_info, 0, sizeof(capi_chip_command_info_t));
    chip_cmd_info.command_id = COMMAND_ID_GET_LINE_PLL_DDCC_CFG;
    phy_info.base_addr = OCTAL_TOP_REGS;
    capi_get_chip_command(&phy_info, &chip_cmd_info);
    dprintf("\n**** Current FW %s line PLL DDCC configure control feature\r\n", 
        chip_cmd_info.type.line_pll_ctrl.ddcc_cfg_en?"ENABLE":"DISABLE");

    /*update the new setting*/
    dprintf("*** Config line PLL DDCC configure feature: 0-disable; 1-enable; \r\n");
    scanf("%d", &test_option);
    chip_cmd_info.command_id = COMMAND_ID_SET_LINE_PLL_DDCC_CFG;
    chip_cmd_info.type.line_pll_ctrl.ddcc_cfg_en = test_option?CAPI_ENABLE:CAPI_DISABLE;
    capi_set_chip_command(&phy_info, &chip_cmd_info);

    /*read back the new setting*/
    util_memset((void *)&chip_cmd_info, 0, sizeof(capi_chip_command_info_t));
    chip_cmd_info.command_id = COMMAND_ID_GET_LINE_PLL_DDCC_CFG;
    capi_get_chip_command(&phy_info, &chip_cmd_info);
    dprintf("\n**** Current FW %s line PLL DDCC configure control feature\r\n", 
        chip_cmd_info.type.line_pll_ctrl.ddcc_cfg_en?"ENABLE":"DISABLE");

    phy_info.base_addr = host_util_get_lane_lw_top_pam_bbaddr(0);
    ERR_HSIP(ddcc_frc_clm       = hsip_rd_field_(&phy_info, ANA_PLL_ANA_PLL_CTRL_26, DDCC_FORCE_CLM));
    ERR_HSIP(ddcc_polflip       = hsip_rd_field_(&phy_info, ANA_PLL_ANA_PLL_CTRL_26, DDCC_POLFLIP));
    ERR_HSIP(ddcc_poldetmodesel = hsip_rd_field_(&phy_info, ANA_PLL_ANA_PLL_CTRL_26, DDCC_POLDETMODESEL));
    ERR_HSIP(ddcc_grofreqsel    = hsip_rd_field_(&phy_info, ANA_PLL_ANA_PLL_CTRL_25, DDCC_GROFREQSEL));
    ddcc_cfg_en                 = chip_cmd_info.type.line_pll_ctrl.ddcc_cfg_en;
    if(ddcc_frc_clm        != expected_val[ddcc_cfg_en][0] ||
        ddcc_polflip       != expected_val[ddcc_cfg_en][1] ||
        ddcc_poldetmodesel != expected_val[ddcc_cfg_en][2] ||
        ddcc_grofreqsel    != expected_val[ddcc_cfg_en][3] )
        dprintf("\n\n **** PLL DDCC config MIS-MATCH; expected:%d-%d-%d-%d; read-out:%d-%d-%d-%d \r\n",
            expected_val[ddcc_cfg_en][0], expected_val[ddcc_cfg_en][1], expected_val[ddcc_cfg_en][2],
            expected_val[ddcc_cfg_en][3], ddcc_frc_clm, ddcc_polflip, ddcc_poldetmodesel, ddcc_grofreqsel);
    return RR_SUCCESS;
}
