#include "logger.h"
#include "common_def.h"
#include "chip_mode_def.h"
#include "capi_def.h"
#include "host_test.h"
#include "common_util.h"
#include "chip_mode_def.h"
#include "sv_modes.h"
#include "sv_modes_test.h"
#include "capi.h"

static char port_modulation_description[][100] = {
    "NRZ",
    "PAM4"
};

static char port_func_mode_description[CAPI_MODE_MAX][100] = {
    "CAPI_MODE_NONE",
    "CAPI_MODE_400",
    "CAPI_MODE_200",
    "CAPI_MODE_100",
    "CAPI_MODE_50G",
    "CAPI_MODE_25G",
    "CAPI_MODE_10G",
    "CAPI_MODE_1G",
};

static char port_fec_term_description[CAPI_LANE_FEC_TERM_MAX][100] = {
    "CAPI_LANE_FEC_TERM_BYPASS",
    "CAPI_LANE_FEC_DEC_FWD",
    "CAPI_LANE_FEC_DEC_ENC",
    "CAPI_LANE_PCS_XENC",
    "CAPI_LANE_FEC_DEC_XDEC_XENC_ENC"
};

static char port_host_fec_type_description[CAPI_HOST_FEC_TYPE_MAX][100] = {
    "CAPI_HOST_FEC_TYPE_NA",
    "CAPI_HOST_FEC_TYPE_RS528",
    "CAPI_HOST_FEC_TYPE_RS544",
    "CAPI_HOST_FEC_TYPE_PCS",
};

static char port_line_fec_type_description[CAPI_LINE_FEC_TYPE_MAX][100] = {
    "CAPI_LINE_FEC_TYPE_NA",
    "CAPI_LINE_FEC_TYPE_RS528",
    "CAPI_LINE_FEC_TYPE_RS544",
    "CAPI_LINE_FEC_TYPE_PCS",
};

static char port_bh_data_rate_description[CAPI_BH_BR_MAX][100] = {
    "CAPI_BH_BR_53_125",
    "CAPI_BH_BR_51_5625",
    "CAPI_BH_BR_25_78125",
    "CAPI_BH_BR_26_5625",
    "CAPI_BH_BR_10_3125",
    "CAPI_BH_BR_20_625",
    "CAPI_BH_BR_1_25"
};

static char port_lw_data_rate_description[CAPI_LW_BR_MAX][100] = {
    "CAPI_LW_BR_53_125",
    "CAPI_LW_BR_51_5625",
    "CAPI_LW_BR_25_78125",
    "CAPI_LW_BR_26_5625",
    "CAPI_LW_BR_10_3125",
    "CAPI_LW_BR_20_625",
    "CAPI_LW_BR_1_25",
};

capi_config_info_t gcapi_cfg[MAX_PORT];

static unsigned int count_set_bits(uint32_t n)
{
    unsigned int count = 0;
    while (n)
    {
        n &= (n-1) ;
        count++;
    }
    return count;
}

uint8_t test_chip_get_port_mode(capi_phy_info_t* phy_info_ptr)
{
    uint8_t port_idx;
    capi_phy_info_t capi_phy;
    capi_config_info_t capi_cfg;
    uint8_t valid_ports =0;
    uint8_t next_valid_port = 0;

    util_memcpy((void *)&capi_phy, (void*)phy_info_ptr, sizeof(capi_phy_info_t));
    capi_phy.core_ip = CORE_IP_ALL;

    for (port_idx = 0; port_idx < MAX_PORT; port_idx++)
    {
        util_memset((void *)&capi_cfg, 0, sizeof(capi_config_info_t));
        util_memset((void *)&gcapi_cfg[port_idx], 0, sizeof(capi_config_info_t));

        capi_cfg.line_lane.lane_mask = 1 << port_idx;

        if (capi_get_config(&capi_phy, &capi_cfg) == RR_SUCCESS) {
            if (capi_cfg.func_mode){
                if(next_valid_port == port_idx) {
                    dprintf("\n Get the PORT %d config information \r\n", port_idx);
                    dprintf("\n** Reference Clock: %d \r\n", capi_cfg.ref_clk);
                    dprintf("\n** port low power: %s \r\n", (capi_cfg.pwd_status==CAPI_PORT_POWER_DOWN_STATUS_POWER_UP)?"UP":"DOWN");
                    dprintf("** func_mode: %d -- %s \r\n", capi_cfg.func_mode, port_func_mode_description[capi_cfg.func_mode]);
                    dprintf("** fec_term: %d -- %s \r\n", capi_cfg.fec_term, port_fec_term_description[capi_cfg.fec_term]);
                    dprintf("** HOST data_rate: %d -- %s \r\n", capi_cfg.bh_br, port_bh_data_rate_description[capi_cfg.bh_br]);
                    dprintf("** HOST fec_type: %d -- %s \r\n", capi_cfg.host_fec_type, port_host_fec_type_description[capi_cfg.host_fec_type]);
                    dprintf("** HOST modulation: %d -- %s \r\n", capi_cfg.host_lane.modulation, capi_cfg.host_lane.modulation?"PAM4":"NRZ");
                    dprintf("** HOST lane_mask: 0x%x \r\n", capi_cfg.host_lane.lane_mask);
                    dprintf("** LINE data_rate: %d -- %s \r\n", capi_cfg.lw_br, port_lw_data_rate_description[capi_cfg.lw_br]);
                    dprintf("** LINE fec_type: %d -- %s \r\n", capi_cfg.line_fec_type, port_line_fec_type_description[capi_cfg.line_fec_type]);
                    dprintf("** LINE modulation: %d -- %s \r\n", capi_cfg.line_lane.modulation, capi_cfg.line_lane.modulation?"PAM4":"NRZ");
                    dprintf("** LINE lane_mask: 0x%x \r\n", capi_cfg.line_lane.lane_mask);

                    next_valid_port = port_idx + count_set_bits(capi_cfg.line_lane.lane_mask);
                    valid_ports = (valid_ports|(1<<port_idx));
                }
                gcapi_cfg[port_idx] = capi_cfg;
            } else {
                next_valid_port++;
                dprintf("** No Active PORT mode \r\n");
            }
        }
    }
    return valid_ports;
}

void test_modes_program_grp(capi_phy_info_t* phy_info_ptr)
{
    capi_config_info_t capi_cfg;
    int cfg1, idx, pidx, count, test_case;
    int bh_lmask[MAX_PORT+1] = {0}, lw_lmask[MAX_PORT+1] = {0};
    return_result_t ret_val;
    chip_capi_config_grp_t* grp_cfg_ptr;
    chip_capi_config_grp_t* mode_cfg_ptr;
    uint16_t size = 0;
    capi_config_info_t *cmode_ptr;
    int port_cfg, usr_input;
    util_memset((void *)&capi_cfg, 0, sizeof(capi_config_info_t));
    phy_info_ptr->core_ip = CORE_IP_ALL;

    dprintf("\n**************MODE CONFIG ITEM*************\r\n");
    grp_cfg_ptr = util_modes_get_grp_cfg (&size);
    if ((grp_cfg_ptr == (chip_capi_config_grp_t*) NULL) || (size == 0)) {
        dprintf("!!! ERROR--> chip config group get function FAILED. !!! \r\n");
        return;
    }

    for (idx = 0; idx < size; idx++) {
        dprintf("*** %2d -- %s \r\n", idx+1, grp_cfg_ptr[idx].desc);
    }
    dprintf("\nPlease choose valid config mode:  ");
    scanf("%d", &cfg1);
    dprintf("%d \r\n", cfg1);
    if (cfg1 > size) {
        dprintf("\n!!! ERROR--> Invalid Chip Mode Option Selected. !!! \r\n");
        return;
    }

    mode_cfg_ptr = &(grp_cfg_ptr[cfg1-1]);
    count = mode_cfg_ptr->size;
    for (idx = 0; idx < count; idx++) {
        dprintf("*** %2d -- %s \r\n", idx, mode_cfg_ptr->cfg[idx].desc);
    }
    dprintf("\nPlease choose valid config sub mode:  ");
    scanf("%d", &test_case);
    dprintf("%d \r\n", test_case);
    test_case = (test_case >= count) ? 0 : test_case;
    pidx = 0;

    cmode_ptr = &(mode_cfg_ptr->cfg[test_case].cfg);
    for (idx = 0; idx < MAX_PORT; idx++) {
        if (host_test_get_valid_port_mask(cfg1) & (1<<idx)) {
            bh_lmask[pidx] = host_test_get_port_lane_mask(cfg1, 0, idx);
            bh_lmask[MAX_PORT] |= bh_lmask[pidx];
            lw_lmask[pidx] = host_test_get_port_lane_mask(cfg1, 1, idx);
            lw_lmask[MAX_PORT] |= lw_lmask[pidx];
            dprintf("*** %d: port %2d --%s  %s HOST lane 0x%x %s %s <=> LINE lane 0x%x %s %s  \r\n", pidx, idx,
                port_func_mode_description[cmode_ptr->func_mode], port_fec_term_description[cmode_ptr->fec_term],
                bh_lmask[pidx], port_modulation_description[cmode_ptr->host_lane.modulation], 
                port_host_fec_type_description[cmode_ptr->host_fec_type] , 
                lw_lmask[pidx], port_modulation_description[cmode_ptr->line_lane.modulation], 
                port_line_fec_type_description[cmode_ptr->line_fec_type]
                );
            pidx++;
        }
    }

    dprintf("*** %d: all ports --%s %s HOST lane 0x%x %s %s <=> LINE lane 0x%x %s %s  \r\n", pidx,   
        port_func_mode_description[cmode_ptr->func_mode], port_fec_term_description[cmode_ptr->fec_term],
        bh_lmask[MAX_PORT], port_modulation_description[cmode_ptr->host_lane.modulation], 
        port_host_fec_type_description[cmode_ptr->host_fec_type] , 
        lw_lmask[MAX_PORT], port_modulation_description[cmode_ptr->line_lane.modulation], 
        port_line_fec_type_description[cmode_ptr->line_fec_type]
        );

    dprintf("*** %d: random lane mask\r\n", pidx+1);
    dprintf("\nPlease choose config port:  ");
    scanf("%d", &port_cfg);
    dprintf("%d \r\n", port_cfg);

    util_memcpy((void *)&capi_cfg, (void*)cmode_ptr, sizeof(capi_config_info_t));
    if (port_cfg == (pidx+1)) {
        dprintf("\nPlease choose config HOST lane mask (hex):  ");
        scanf("%x", &usr_input);
        capi_cfg.host_lane.lane_mask = (uint16_t) usr_input;
        dprintf("%d \r\n", capi_cfg.host_lane.lane_mask);
        dprintf("\nPlease choose config LINE lane mask (hex):  ");
        scanf("%x", &usr_input);
        capi_cfg.line_lane.lane_mask = (uint16_t) usr_input;
        dprintf("%d \r\n", capi_cfg.line_lane.lane_mask);
        if((capi_cfg.host_lane.lane_mask & bh_lmask[MAX_PORT]) != capi_cfg.host_lane.lane_mask ||
            (capi_cfg.line_lane.lane_mask & lw_lmask[MAX_PORT]) != capi_cfg.line_lane.lane_mask) {
                dprintf("\n!!! ERROR--> Wrong lane mask HOST 0x%x vs 0x%x; LINE 0x%x vs 0x%x \r\n",
                    capi_cfg.host_lane.lane_mask, bh_lmask[MAX_PORT],
                    capi_cfg.line_lane.lane_mask, lw_lmask[MAX_PORT]);
                return;
        }
    } else if (port_cfg == pidx){
         capi_cfg.host_lane.lane_mask = bh_lmask[MAX_PORT];
         capi_cfg.line_lane.lane_mask = lw_lmask[MAX_PORT];
    } else {
         capi_cfg.host_lane.lane_mask = bh_lmask[port_cfg];
         capi_cfg.line_lane.lane_mask = lw_lmask[port_cfg];
    }

    if((ret_val = capi_set_config(phy_info_ptr, &capi_cfg)) != RR_SUCCESS) {
        dprintf("\n!!! ERROR--> %s: Set Mode Config Failed; Error Code = %d !!!\r\n", mode_cfg_ptr->cfg[test_case].desc, ret_val);
    } else {
        dprintf("\n*** INFO--> %s: Set Mode Config Success ***\r\n", mode_cfg_ptr->cfg[test_case].desc);
    }
    return;
}

