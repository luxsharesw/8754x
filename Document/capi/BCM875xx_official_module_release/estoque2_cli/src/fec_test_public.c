#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "access.h"
#include "common_def.h"
#include "regs_common.h"
#include "chip_mode_def.h"
#include "cw_def.h"
#include "capi_def.h"
#include "diag_fec_statistics_def.h"
#include "host_diag_fec_statistics.h"
#include "capi.h"
#include "capi_diag_def.h"
#include "capi_diag.h"
#include "host_test.h"
#include "fec_test.h"
#include "chip_common_config_ind.h"
#include "common_util.h"
#include "logger.h"
#include "hr_time.h"

void test_fec_status_dump(capi_fec_dump_status_t* capi_fec_dump_status_ptr);

typedef struct {
    float pre_fec_ber; /**<  pre FEC BER */
    float post_fec_ber; /**<  post FEC BER */
    cw_kp4fec_err_cnt_t cnt; 
} cw_kp4fec_err_state_s;

typedef enum {
    LINE_FEC_MON_INIT,
    LINE_FEC_MON_CLEAR,
    LINE_FEC_STATUS,
    CLIENT_FEC_MON_INIT,
    CLIENT_FEC_MON_CLEAR,
    CLIENT_FEC_STATUS,
    FEC_BER_MON,
    CMIS_FEC_BER_MON,
    FEC_MON_GET_TMAX,
    FEC_TEST_MAX
} centenario_fec_test_type_e;

static char fec_test_description[FEC_TEST_MAX][100] = {
    "LINE_FEC_MON_INIT",
    "LINE_FEC_MON_CLEAR",
    "LINE_FEC_STATUS",
    "CLIENT_FEC_MON_INIT",
    "CLIENT_FEC_MON_CLEAR",
    "CLIENT_FEC_STATUS",
    "FEC_BER_MON",
    "CMIS_FEC_BER_MON",
    "FEC_MON_GET_TMAX",
};

static char port_func_mode_description[CAPI_MODE_MAX][100] = {
    "CAPI_MODE_NONE",
    "CAPI_MODE_400",
    "CAPI_MODE_200",
    "CAPI_MODE_100",
    "CAPI_MODE_50G",
    "CAPI_MODE_25G",
};

static char port_fec_mode_description[CAPI_FEC_MODE_MAX][100] = {
    "CAPI_FEC_INGRESS",
    "CAPI_FEC_EGRESS",
    "CAPI_FEC_LINE",
    "CAPI_FEC_CLIENT",
};


static char port_line_fec_type_description[CAPI_LINE_FEC_TYPE_MAX][100] = {
    "CAPI_LINE_FEC_TYPE_NA",
    "CAPI_LINE_FEC_TYPE_RS528",
    "CAPI_LINE_FEC_TYPE_RS544",
    "CAPI_LINE_FEC_TYPE_INNER4"
};

static char port_host_fec_type_description[CAPI_HOST_FEC_TYPE_MAX][100] = {
    "CAPI_HOST_FEC_TYPE_NA",
    "CAPI_HOST_FEC_TYPE_RS528",
    "CAPI_HOST_FEC_TYPE_RS544",
    "CAPI_HOST_FEC_TYPE_PCS"
};


void fec_stat_test(uint8_t phy_id, uint8_t test_case)
{
    capi_phy_info_t capi_phy;
    int op_code;
    uint16_t fec_lane_mask;
    static capi_fec_dump_status_t capi_fec_dump_status;
    /*Fill in the CAPI PHY information*/
    capi_phy.phy_id = phy_id;
    capi_phy.core_ip = CORE_IP_CW;

    switch (test_case) 
    {
        case LINE_FEC_MON_INIT:
            {
                dprintf("\n\n Please use line-side lane_mask to identify a PORT (0x00~0xF):   ");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;
        
                dprintf("\n\n Please specify whether to enable (1) or disable (0) fec monitoring:   ");
                scanf ("%d", &op_code);
                test_case = (op_code) ? 1 : 0;
        
                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = CAPI_FEC_LINE;
                capi_phy.lane_mask = fec_lane_mask;
        
                if (capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, (test_case? TRUE:FALSE)) != RR_SUCCESS)
                {
                    dprintf("\n  *** capi_init_fec_mon call failed. \r\n");
                } else {
                    dprintf("\n  *** capi_init_fec_mon call successful. \r\n");
                }
            }
            break;

        case LINE_FEC_MON_CLEAR:
            {
                dprintf("\n\n Please use line-side lane_mask to identify a PORT (0x00~0xF):   ");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;
        
                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = CAPI_FEC_LINE;
                capi_phy.lane_mask = fec_lane_mask;
                if (capi_clear_fec_mon(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS)
                {
                    dprintf("\n  *** capi_clear_fec_mon call failed. \r\n");
                } else {
                    dprintf("\n  *** capi_clear_fec_mon call successful. \r\n");
                }
            }
            break;
        
        case LINE_FEC_STATUS:
            {
                capi_fec_dump_status_t capi_fec_dump_status_bk={0};
                uint16_t didx,dump_num, sidx, interval_ms;

                dprintf("\n\n Please use line-side lane_mask to identify a PORT (0x00~0xF):   ");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;
                dprintf("\n\n Please choose dump times(decimal):   ");
                scanf("%d", &op_code);
                dump_num = op_code&0xFFFF;
            dprintf("\n\n Please choose interval times(ms, decimal):   ");
            scanf("%d", &op_code);
            interval_ms = op_code&0xFFFF;

                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = CAPI_FEC_LINE;
                capi_phy.lane_mask = fec_lane_mask;

                for(didx=0; didx<dump_num; didx++){
                    if (capi_get_fec_info(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS)
                    {
                        dprintf("\n  *** capi_get_fec_info call failed. \r\n");
                    }
                    test_fec_status_dump(&capi_fec_dump_status);    /* display fec status */
                    if(capi_fec_dump_status_bk.fec_a_err_cnt.tot_frame_rev_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_frame_rev_cnt ||
                        capi_fec_dump_status_bk.fec_a_err_cnt.tot_frame_corr_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_frame_corr_cnt ||
                        capi_fec_dump_status_bk.fec_a_err_cnt.tot_frame_uncorr_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_frame_uncorr_cnt ||
                        capi_fec_dump_status_bk.fec_a_err_cnt.tot_symbols_corr_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_symbols_corr_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_frame_rev_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_frame_rev_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_frame_corr_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_frame_corr_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_frame_uncorr_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_frame_uncorr_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_symbols_corr_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_symbols_corr_cnt)
                        dprintf("\n ********** FAIL with frame counter \r\n");
                    for(sidx=0; sidx<FEC_TOT_BITS_CORR_NUM; sidx++){
                        if(capi_fec_dump_status_bk.fec_a_err_cnt.tot_bits_corr_cnt[sidx] > capi_fec_dump_status.fec_a_err_cnt.tot_bits_corr_cnt[sidx] ||
                            capi_fec_dump_status_bk.fec_b_err_cnt.tot_bits_corr_cnt[sidx] > capi_fec_dump_status.fec_b_err_cnt.tot_bits_corr_cnt[sidx])
                        dprintf("\n ********** FAIL with tot_bits_corr_cnt %d counter \r\n", sidx);

                    }
                    for(sidx=0; sidx<FEC_TOT_FRAMES_ERR_NUM; sidx++){
                        if(capi_fec_dump_status_bk.fec_a_err_cnt.tot_frames_err_cnt[sidx] > capi_fec_dump_status.fec_a_err_cnt.tot_frames_err_cnt[sidx] ||
                            capi_fec_dump_status_bk.fec_b_err_cnt.tot_frames_err_cnt[sidx] > capi_fec_dump_status.fec_b_err_cnt.tot_frames_err_cnt[sidx])
                        dprintf("\n ********** FAIL with tot_frames_err_cnt %d counter \r\n", sidx);

                    }

                    util_memcpy((void *)&capi_fec_dump_status_bk, (void *)&capi_fec_dump_status, sizeof(capi_fec_dump_status_t));
                    delay_ms(interval_ms);
                }
            }
            break;

        case FEC_BER_MON:
            {
                capi_fec_dump_status_t capi_fec_dump_status_bk={0};
                uint16_t didx,dump_num, sidx, is_line;
                static cw_kp4fec_err_cnt_t kp4_fec_bak_list = { 0 };
                static cw_kp4fec_err_cnt_t kp4_fec_total_list = { 0 };
                cw_kp4fec_err_cnt_t fec_cur;
                cw_kp4fec_err_cnt_t* fec_bak;
                cw_kp4fec_err_cnt_t* fec_total;
                uint16_t init, clr, deinit, total_uncorr_bpf = 16, t_max=0, interval_ms;
                uint8_t regcnt;
                int16_t bpf = 5440;
                cw_kp4fec_err_state_s fec_ber;

                fec_bak = &kp4_fec_bak_list;
                fec_total = &kp4_fec_total_list;
                
                dprintf("\n\n Please config PHY address. 0-Die 0; 2-Die 1   ");
                scanf("%x", &op_code);
                capi_phy.phy_id = op_code&0x2;

                dprintf("\n\n Please use Client side (EGRESS) or line-side(INGRESS) FEC Status: 0--client; 1-line   ");
                scanf("%x", &op_code);
                is_line = op_code&0x1;
                dprintf("\n\n Please use %s-side lane_mask to identify a PORT (0x00~%s):   ", is_line?"LINE":"CLIENT", is_line?"0xFF":"0xFF");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;
                dprintf("\n\n Please choose initFEC statistics: 0--not init; 1-init \r\n");
                scanf("%d", &op_code);
                init = op_code&0x1;
                dprintf("\n\n Please choose clear FEC counter during beginning: 0--not clear;  1--clear \r\n");
                scanf("%d", &op_code);
                clr = op_code&0x1;
                dprintf("\n\n Please choose de-init FEC statistics: 0--leave it on; 1-de-init  \r\n");
                scanf("%d", &op_code);
                deinit = op_code&0x1;
                dprintf("\n\n Please choose interval times(ms, decimal):   ");
                scanf("%d", &op_code);
                interval_ms = op_code&0xFFFF;
                dprintf("\n\n Please choose dump times(decimal):   ");
                scanf("%d", &op_code);
                dump_num = op_code&0xFFFF;

                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = is_line?CAPI_FEC_LINE:CAPI_FEC_CLIENT;
                capi_phy.lane_mask = fec_lane_mask;  
                util_memset((void*)&fec_cur, 0, sizeof(fec_cur));

                if(init) {
                    if( capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, TRUE) != RR_SUCCESS) {
                        dprintf("\n  *** capi_init_fec_mon call failed. \r\n");
                        break;
                    } else {
                        dprintf("\n  *** capi_init_fec_mon call successful. \r\n");
                    }
                }
                if(clr) {
                    if(capi_clear_fec_mon(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS) {
                        dprintf("\n  *** capi_clear_fec_mon call failed. \r\n");
                        break;
                    } else {
                        dprintf("\n  *** capi_clear_fec_mon call successful. \r\n");
                    }
                }

                for(didx=0; didx<(dump_num+1); didx++){
                    if (capi_get_fec_info(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS)
                    {
                        dprintf("\n  *** capi_get_fec_info call failed. \r\n");
                        break;
                    }
                    if ((capi_fec_dump_status.fec_mode == CAPI_FEC_LINE && capi_fec_dump_status.fec_type.line_fec == CAPI_LINE_FEC_TYPE_RS528) ||
                        (capi_fec_dump_status.fec_mode == CAPI_FEC_CLIENT && capi_fec_dump_status.fec_type.host_fec == CAPI_LINE_FEC_TYPE_RS528)) {
                        bpf = 5280;
                        total_uncorr_bpf = 8;
                    }
                    /*Post processing counter and calculate the FEC BER*/
                    for (regcnt = 0; regcnt < total_uncorr_bpf; regcnt++) {
                        fec_cur.tot_frames_err_cnt[regcnt] = capi_fec_dump_status.fec_a_err_cnt.tot_frames_err_cnt[regcnt] + capi_fec_dump_status.fec_b_err_cnt.tot_frames_err_cnt[regcnt];
                    }

                    for (regcnt = 0; regcnt < KP4_FEC_TOT_BITS_CORR_NUM; regcnt++) {
                        fec_cur.tot_bits_corr_cnt[regcnt] = capi_fec_dump_status.fec_a_err_cnt.tot_bits_corr_cnt[regcnt] + capi_fec_dump_status.fec_b_err_cnt.tot_bits_corr_cnt[regcnt];
                    }
                    fec_cur.tot_frame_cnt = capi_fec_dump_status.fec_a_err_cnt.tot_frame_rev_cnt + capi_fec_dump_status.fec_b_err_cnt.tot_frame_rev_cnt;
                    fec_cur.tot_frame_uncorr_cnt = capi_fec_dump_status.fec_a_err_cnt.tot_frame_uncorr_cnt + capi_fec_dump_status.fec_b_err_cnt.tot_frame_uncorr_cnt;

                    /*If it isn't start point, then calculate the accumulated counters*/
                    if (didx) {
                        for (regcnt = 0; regcnt < total_uncorr_bpf; regcnt++) {
                            if (fec_cur.tot_frames_err_cnt[regcnt] >= fec_bak->tot_frames_err_cnt[regcnt])
                                fec_total->tot_frames_err_cnt[regcnt] += (fec_cur.tot_frames_err_cnt[regcnt] - fec_bak->tot_frames_err_cnt[regcnt]);
                            else
                                fec_total->tot_frames_err_cnt[regcnt] += (((uint64_t)1 << 48) - fec_bak->tot_frames_err_cnt[regcnt] + fec_cur.tot_frames_err_cnt[regcnt]);
                        }

                        for (regcnt = 0; regcnt < KP4_FEC_TOT_BITS_CORR_NUM; regcnt++) {
                            if (fec_cur.tot_bits_corr_cnt[regcnt] >= fec_bak->tot_bits_corr_cnt[regcnt])
                                fec_total->tot_bits_corr_cnt[regcnt] += (fec_cur.tot_bits_corr_cnt[regcnt] - fec_bak->tot_bits_corr_cnt[regcnt]);
                            else
                                fec_total->tot_bits_corr_cnt[regcnt] += (((uint64_t)1 << 48) - fec_bak->tot_bits_corr_cnt[regcnt] + fec_cur.tot_bits_corr_cnt[regcnt]);
                        }

                        if (fec_cur.tot_frame_uncorr_cnt >= fec_bak->tot_frame_uncorr_cnt)
                            fec_total->tot_frame_uncorr_cnt += (fec_cur.tot_frame_uncorr_cnt - fec_bak->tot_frame_uncorr_cnt);
                        else
                            fec_total->tot_frame_uncorr_cnt += (((uint64_t)1 << 48) - fec_bak->tot_frame_uncorr_cnt + fec_cur.tot_frame_uncorr_cnt);

                        if (fec_cur.tot_frame_cnt >= fec_bak->tot_frame_cnt)
                            fec_total->tot_frame_cnt += (fec_cur.tot_frame_cnt - fec_bak->tot_frame_cnt);
                        else
                            fec_total->tot_frame_cnt += (((uint64_t)1 << 48) - fec_bak->tot_frame_cnt + fec_cur.tot_frame_cnt);
                    } else {
                        /*for 1st cycle, clear the total counter to zero*/
                        util_memset((void*)fec_total, 0, sizeof(*fec_total));
                        util_memset((void*)fec_bak, 0, sizeof(*fec_bak));
                    }

                    /*back-up current reading counter for next cycle*/
                    for (regcnt = 0; regcnt < total_uncorr_bpf; regcnt++) {
                        fec_bak->tot_frames_err_cnt[regcnt] = fec_cur.tot_frames_err_cnt[regcnt];
                    }

                    for (regcnt = 0; regcnt < KP4_FEC_TOT_BITS_CORR_NUM; regcnt++) {
                        fec_bak->tot_bits_corr_cnt[regcnt] = fec_cur.tot_bits_corr_cnt[regcnt];
                    }
                    fec_bak->tot_frame_cnt = fec_cur.tot_frame_cnt;
                    fec_bak->tot_frame_uncorr_cnt = fec_cur.tot_frame_uncorr_cnt;

                    /* If total frame is 0, force pre&post FEC BER to 1*/
                    if (fec_total->tot_frame_cnt == 0 || didx==0) {
                        fec_ber.pre_fec_ber = 1;
                        fec_ber.post_fec_ber = 1;
                    } else {
                        fec_ber.pre_fec_ber = (float)(fec_total->tot_bits_corr_cnt[0] + fec_total->tot_bits_corr_cnt[1] + fec_total->tot_frame_uncorr_cnt * total_uncorr_bpf) / fec_total->tot_frame_cnt / bpf;
                        fec_ber.post_fec_ber = (float)(fec_total->tot_frame_uncorr_cnt * total_uncorr_bpf) / fec_total->tot_frame_cnt / bpf;
                    }

                    /*pass the total information*/

                    for (regcnt = 0; regcnt < total_uncorr_bpf; regcnt++) {
                        fec_ber.cnt.tot_frames_err_cnt[regcnt] = fec_total->tot_frames_err_cnt[regcnt];
                    }
                    
                    for (regcnt = 0; regcnt < KP4_FEC_TOT_BITS_CORR_NUM; regcnt++) {
                        fec_ber.cnt.tot_bits_corr_cnt[regcnt] = fec_total->tot_bits_corr_cnt[regcnt];
                    }
                    fec_ber.cnt.tot_frame_cnt = fec_total->tot_frame_cnt;
                    fec_ber.cnt.tot_frame_uncorr_cnt = fec_total->tot_frame_uncorr_cnt;
                    if(didx){    
                        dprintf("\r\n testing cycle %d : pre-fec BER is %0.3e; post-fec BER is  %0.3e \r\n", didx, fec_ber.pre_fec_ber, fec_ber.post_fec_ber);
                        dprintf("Accumulative BER : pre-fec BER is %0.3e; post-fec BER is  %0.3e; projected post-fec BER %0.3e \r\n", 
                            capi_fec_dump_status.fec_ber.pre_fec_ber, capi_fec_dump_status.fec_ber.post_fec_ber, capi_fec_dump_status.fec_ber.post_fec_ber_proj);
                        dprintf("Total frame counter %lld, total frame uncorrect counter %lld \r\n", fec_ber.cnt.tot_frame_cnt, fec_ber.cnt.tot_frame_uncorr_cnt);
                        dprintf("Total correct bit0 counter %lld, total correct bit1 counter %lld \r\n", fec_ber.cnt.tot_bits_corr_cnt[0], fec_ber.cnt.tot_bits_corr_cnt[1]);
                        for(sidx=0; sidx<total_uncorr_bpf; sidx++){
                            dprintf("Total error %d counter %lld \r\n", sidx, fec_ber.cnt.tot_frames_err_cnt[sidx]);
                            if(fec_ber.cnt.tot_frames_err_cnt[sidx])
                                t_max = sidx;
                        }
                        dprintf("T-max is %d \r\n\n", t_max);
                    }
                    dprintf("\n\n  *** BRCM Sticky and Active Status *** \r\n");
                    dprintf("  *** BRCM IGBOX CLSN STICKY STATUS        = 0x%x\r\n", capi_fec_dump_status.fec_sts.igbox_clsn_sticky);
                    dprintf("  *** BRCM AM UNLOCK STICKY STATUS         = 0x%x\r\n", capi_fec_dump_status.fec_sts.am_lolock_sticky);
                    dprintf("  *** BRCM XDEC GBOX CLSN STICKY STATUS    = 0x%x\r\n", capi_fec_dump_status.fec_sts.xdec_gbox_clsn_sticky);
                    dprintf("  *** BRCM XENC GBOX CLSN STICKY STATUS    = 0x%x\r\n", capi_fec_dump_status.fec_sts.xenc_gbox_clsn_sticky);
                    dprintf("  *** BRCM OGBOX CLSN STICKY STATUS        = 0x%x\r\n", capi_fec_dump_status.fec_sts.ogbox_clsn_sticky);
                    dprintf("  *** BRCM FEC ALIGNMENT LOL STICKY STATUS = 0x%x\r\n", capi_fec_dump_status.fec_sts.fec_algn_lol_sticky);
                    dprintf("  *** BRCM FEC ALIGNMENT LIVE STAT       = %u \r\n", capi_fec_dump_status.fec_sts.fec_algn_stat);


                    delay_ms(interval_ms);
                }
                if (deinit) {
                    
                    if (capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, FALSE) != RR_SUCCESS){
                        dprintf("\n  *** capi_init_fec_mon disable  call failed. \r\n");
                    } else {
                        dprintf("\n  *** capi_init_fec_mon disable call successful. \r\n");
                    }
                }
            }
            break;


        case CLIENT_FEC_MON_INIT:
            {
                dprintf("\n\n Please use client-side lane_mask to identify a PORT (0x0000~0xFF):   ");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;

                dprintf("\n\n Please specify whether to enable (1) or disable (0) fec monitoring:   ");
                scanf ("%d", &op_code);
                test_case = (op_code) ? 1 : 0;

                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = CAPI_FEC_CLIENT;
                capi_phy.lane_mask = fec_lane_mask;

                if (capi_init_fec_mon(&capi_phy, &capi_fec_dump_status,(test_case? TRUE:FALSE)) != RR_SUCCESS)
                {
                    dprintf("\n  *** capi_init_fec_mon call failed. \r\n");
                } else {
                    dprintf("\n  *** capi_init_fec_mon call successful. \r\n");
                }
            }
            break;

        case CLIENT_FEC_MON_CLEAR:
            {
                dprintf("\n\n Please use client-side lane_mask to identify a PORT (0x0000~0xFF):   ");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;

                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = CAPI_FEC_CLIENT;
                capi_phy.lane_mask = fec_lane_mask;

                if (capi_clear_fec_mon(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS)
                {
                    dprintf("\n  *** capi_clear_fec_mon call failed. \r\n");
                } else {
                    dprintf("\n  *** capi_clear_fec_mon call successful. \r\n");
                }
            }
            break;

        case CLIENT_FEC_STATUS:
            {
                capi_fec_dump_status_t capi_fec_dump_status_bk={0};
                uint16_t didx,dump_num, sidx;

                dprintf("\n\n Please use client-side lane_mask to identify a PORT (0x0000~0xFF):   ");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;
                dprintf("\n\n Please choose dump times(decimal):   ");
                scanf("%d", &op_code);
                dump_num = op_code&0xFFFF;

                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = CAPI_FEC_CLIENT;
                capi_phy.lane_mask = fec_lane_mask;

                for(didx=0; didx<dump_num; didx++){
                    if (capi_get_fec_info(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS)
                    {
                        dprintf("\n  *** capi_get_fec_info call failed. \r\n");
                    }
                    test_fec_status_dump(&capi_fec_dump_status);    /* display fec status */
                    if(capi_fec_dump_status_bk.fec_a_err_cnt.tot_frame_rev_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_frame_rev_cnt ||
                        capi_fec_dump_status_bk.fec_a_err_cnt.tot_frame_corr_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_frame_corr_cnt ||
                        capi_fec_dump_status_bk.fec_a_err_cnt.tot_frame_uncorr_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_frame_uncorr_cnt ||
                        capi_fec_dump_status_bk.fec_a_err_cnt.tot_symbols_corr_cnt > capi_fec_dump_status.fec_a_err_cnt.tot_symbols_corr_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_frame_rev_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_frame_rev_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_frame_corr_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_frame_corr_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_frame_uncorr_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_frame_uncorr_cnt ||
                        capi_fec_dump_status_bk.fec_b_err_cnt.tot_symbols_corr_cnt > capi_fec_dump_status.fec_b_err_cnt.tot_symbols_corr_cnt)
                        dprintf("\n ********** FAIL with frame counter \r\n");
                    for(sidx=0; sidx<FEC_TOT_BITS_CORR_NUM; sidx++){
                        if(capi_fec_dump_status_bk.fec_a_err_cnt.tot_bits_corr_cnt[sidx] > capi_fec_dump_status.fec_a_err_cnt.tot_bits_corr_cnt[sidx] ||
                            capi_fec_dump_status_bk.fec_b_err_cnt.tot_bits_corr_cnt[sidx] > capi_fec_dump_status.fec_b_err_cnt.tot_bits_corr_cnt[sidx])
                        dprintf("\n ********** FAIL with tot_bits_corr_cnt %d counter \r\n", sidx);

                    }
                    for(sidx=0; sidx<FEC_TOT_FRAMES_ERR_NUM; sidx++){
                        if(capi_fec_dump_status_bk.fec_a_err_cnt.tot_frames_err_cnt[sidx] > capi_fec_dump_status.fec_a_err_cnt.tot_frames_err_cnt[sidx] ||
                            capi_fec_dump_status_bk.fec_b_err_cnt.tot_frames_err_cnt[sidx] > capi_fec_dump_status.fec_b_err_cnt.tot_frames_err_cnt[sidx])
                        dprintf("\n ********** FAIL with tot_frames_err_cnt %d counter \r\n", sidx);

                    }

                    util_memcpy((void *)&capi_fec_dump_status_bk, (void *)&capi_fec_dump_status, sizeof(capi_fec_dump_status_t));
                }
            }
            break;
        case CMIS_FEC_BER_MON:{

            uint16_t is_line, fec_lane_mask, init, clr, cmis_clr, cmis_curr_only, deinit, dump_num, regcnt, t_max=0;
            uint16_t didx, interval_ms, ridx, ridx_num=5;
            capi_fec_dump_status_t capi_fec_dump_status;
            capi_kp4fec_ber_state_t fec_ber;

            dprintf("\n\n Please use Client side (EGRESS) or line-side(INGRESS) FEC Status: 0--client; 1-line   ");
            scanf("%x", &op_code);
            is_line = op_code&0x1;
            dprintf("\n\n Please use %s-side lane_mask to identify a PORT (0x00~%s):   ", is_line?"LINE":"CLIENT", is_line?"0xF":"0xFF");
            scanf("%x", &op_code);
            fec_lane_mask = op_code&0xFFFF;
            dprintf("\n\n Please choose initFEC statistics: 0--not init; 1-init \r\n");
            scanf("%d", &op_code);
            init = op_code&0x1;
            dprintf("\n\n Please choose clear FEC counter during beginning: 0--not clear;  1--clear \r\n");
            scanf("%d", &op_code);
            clr = op_code&0x1;
            dprintf("\n\n Please choose clear FEC counter between each cycle: 0--not clear;  1--clear \r\n");
            scanf("%d", &op_code);
            cmis_clr = op_code&0x1;
            dprintf("\n\n Please choose choose return FEC current counter only: 0--return all (min, max, current, average);  1--return current cnt only \r\n");
            scanf("%d", &op_code);
            cmis_curr_only = op_code&0x1;
            dprintf("\n\n Please choose de-init FEC statistics: 0--leave it on; 1-de-init  \r\n");
            scanf("%d", &op_code);
            deinit = op_code&0x1;
            dprintf("\n\n Please choose interval times(ms, decimal):   ");
            scanf("%d", &op_code);
            interval_ms = op_code&0xFFFF;
            dprintf("\n\n Please choose latch request times(decimal):   ");
            scanf("%d", &op_code);
            ridx_num = op_code&0xFFFF;
            dprintf("\n\n Please choose latch release times per latch request(decimal):   ");
            scanf("%d", &op_code);
            dump_num = op_code&0xFFFF;

            util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
            capi_fec_dump_status.fec_mode = is_line?CAPI_FEC_LINE:CAPI_FEC_CLIENT;
            capi_phy.lane_mask = fec_lane_mask;  
            util_memset((void*)&fec_ber, 0, sizeof(capi_kp4fec_ber_state_t));

            if(init){
                if( capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, TRUE) != RR_SUCCESS) {
                    dprintf("\n  *** capi_init_fec_mon call failed. \r\n");
                    break;
                } else {
                    dprintf("\n  *** capi_init_fec_mon call successful. \r\n");
                }
            }
            if(clr){
                if( capi_clear_fec_mon(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS) {
                    dprintf("\n  *** capi_clear_fec_mon call failed. \r\n");
                    break;
                } else {
                    dprintf("\n  *** capi_clear_fec_mon call successful. \r\n");
                }
            }

            for(ridx=0; ridx<ridx_num; ridx++){
                fec_ber.fec_mode = capi_fec_dump_status.fec_mode;
                fec_ber.clr_cnt = (bool)cmis_clr;
                fec_ber.only_cur = (bool)cmis_curr_only;
                if(capi_fec_ber_cmis_latch_request(&capi_phy, &fec_ber)!=RR_SUCCESS){
                    dprintf("  *** Cycle %d: capi_fec_ber_cmis_latch_request failed \r\n", ridx);
                    break;
                }
                delay_ms(interval_ms);
                dprintf("\n  *** Cycle %d: capi_fec_ber_cmis_latch_request call successful \r\n", ridx);
                for(didx=0; didx<(dump_num); didx++){
                    if(capi_fec_ber_cmis_latch_release(&capi_phy, &fec_ber)!=RR_SUCCESS){
                        dprintf("  *** Cycle %d - %d capi_fec_ber_cmis_latch_release failed\r\n", ridx, didx);
                        break;
                    }
                    dprintf("\n  *** Cycle %d - %d capi_fec_ber_cmis_latch_release call successful\r\n", ridx, didx);
                    /*display result*/
                    dprintf(" pre_fec_ber: cur %e min %e max %e average %e total %e\r\n", 
                        fec_ber.pre_fec_ber.cur_ber, fec_ber.pre_fec_ber.min_ber, fec_ber.pre_fec_ber.max_ber, fec_ber.pre_fec_ber.average_ber, fec_ber.pre_fec_ber.total_ber);
                    dprintf(" post_fec_ber: cur %e min %e max %e average %e total %e\r\n", 
                        fec_ber.post_fec_ber.cur_ber, fec_ber.post_fec_ber.min_ber, fec_ber.post_fec_ber.max_ber, fec_ber.post_fec_ber.average_ber, fec_ber.post_fec_ber.total_ber);
                    dprintf(" err_cnt: tot_frame_cnt cur %lld min %lld max %lld average %lld total %lld hw_cnt_backup %lld \r\n", 
                        fec_ber.cur_err.tot_frame_cnt, fec_ber.min_err.tot_frame_cnt, fec_ber.max_err.tot_frame_cnt, fec_ber.average_err.tot_frame_cnt, fec_ber.total_err.tot_frame_cnt, fec_ber.backup_err.tot_frame_cnt);
                    dprintf(" err_cnt: tot_frame_uncorr_cnt cur %lld min %lld max %lld average %lld total %lld hw_cnt_backup %lld \r\n", 
                        fec_ber.cur_err.tot_frame_uncorr_cnt, fec_ber.min_err.tot_frame_uncorr_cnt, fec_ber.max_err.tot_frame_uncorr_cnt, fec_ber.average_err.tot_frame_uncorr_cnt, fec_ber.total_err.tot_frame_uncorr_cnt, fec_ber.backup_err.tot_frame_uncorr_cnt);
                    for (regcnt = 0; regcnt < KP4_FEC_TOT_BITS_CORR_NUM; regcnt++) {
                        dprintf(" err_cnt: tot_bits_corr_cnt[%d] cur %lld min %lld max %lld average %lld total %lld hw_cnt_backup %lld \r\n", regcnt,
                            fec_ber.cur_err.tot_bits_corr_cnt[regcnt], fec_ber.min_err.tot_bits_corr_cnt[regcnt], fec_ber.max_err.tot_bits_corr_cnt[regcnt], fec_ber.average_err.tot_bits_corr_cnt[regcnt], fec_ber.total_err.tot_bits_corr_cnt[regcnt], fec_ber.backup_err.tot_bits_corr_cnt[regcnt]);
                    }
                    for (regcnt = 0; regcnt < KP4_FEC_TOT_FRAMES_ERR_NUM; regcnt++) {
                        dprintf(" err_cnt: tot_frames_err_cnt[%d] cur %lld min %lld max %lld average %lld total %lld hw_cnt_backup %lld \r\n", regcnt,
                            fec_ber.cur_err.tot_frames_err_cnt[regcnt], fec_ber.min_err.tot_frames_err_cnt[regcnt], fec_ber.max_err.tot_frames_err_cnt[regcnt], fec_ber.average_err.tot_frames_err_cnt[regcnt], fec_ber.total_err.tot_frames_err_cnt[regcnt], fec_ber.backup_err.tot_frames_err_cnt[regcnt]);
                    }
                    delay_ms(interval_ms);
                }
                delay_s(1);
            }
            if (deinit) {   
                if (capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, FALSE) != RR_SUCCESS){
                    dprintf("\n  *** capi_init_fec_mon disable  call failed. \r\n");
                } else {
                    dprintf("\n  *** capi_init_fec_mon disable call successful. \r\n");
                }
            }
        }
        break;

        case FEC_MON_GET_TMAX:
            {
                uint16_t loops, max_loops, err_num, is_line;
                uint16_t total_uncorr_bpf = 16, t_max = 0, interval_ms;
                
                dprintf("\n\n Please use Client side (EGRESS) or line-side(INGRESS) FEC Status: 0--client; 1-line   ");
                scanf("%x", &op_code);
                is_line = op_code&0x1;
                dprintf("\n\n Please use %s-side lane_mask to identify a PORT (0x00~%s):   ", is_line?"LINE":"CLIENT", is_line?"0xFF":"0xFF");
                scanf("%x", &op_code);
                fec_lane_mask = op_code&0xFFFF;
                dprintf("\n\n Please choose interval times(ms, decimal):   ");
                scanf("%d", &op_code);
                interval_ms = op_code&0xFFFF;
                dprintf("\n\n Please choose dump times(decimal):   ");
                scanf("%d", &op_code);
                max_loops = op_code&0xFFFF;

                capi_phy.lane_mask = fec_lane_mask;  

                util_memset(&capi_fec_dump_status, 0, sizeof(capi_fec_dump_status_t));
                capi_fec_dump_status.fec_mode = is_line?CAPI_FEC_LINE:CAPI_FEC_CLIENT;
                if ((capi_fec_dump_status.fec_mode == CAPI_FEC_LINE && capi_fec_dump_status.fec_type.line_fec == CAPI_LINE_FEC_TYPE_RS528) ||
                    (capi_fec_dump_status.fec_mode == CAPI_FEC_CLIENT && capi_fec_dump_status.fec_type.host_fec == CAPI_LINE_FEC_TYPE_RS528)) {
                    total_uncorr_bpf = 8;
                }

                if (capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, TRUE) != RR_SUCCESS) {
                    dprintf("\n  *** capi_init_fec_mon call failed. \r\n");
                    break;
                }
                
                if (capi_clear_fec_mon(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS) {
                    dprintf("\n  *** capi_clear_fec_mon call failed. \r\n");
                    break;
                }

                for (loops = 0; loops <= max_loops; loops++){
                    delay_ms(interval_ms);
                    if (capi_get_fec_info(&capi_phy, &capi_fec_dump_status) != RR_SUCCESS)
                    {
                        dprintf("\n  *** capi_get_fec_info call failed. \r\n");
                        break;
                    }

                    for(err_num = 0; err_num < total_uncorr_bpf; err_num++){
                        uint64_t tot_frames_err_cnt = capi_fec_dump_status.fec_a_err_cnt.tot_frames_err_cnt[err_num] + capi_fec_dump_status.fec_b_err_cnt.tot_frames_err_cnt[err_num];
                        dprintf("Total error %d counter %lld \r\n", err_num, tot_frames_err_cnt);
                        if (tot_frames_err_cnt)
                            t_max = err_num;
                    }
                    dprintf("T-max is %d \r\n", t_max);
                }                    
                if (capi_init_fec_mon(&capi_phy, &capi_fec_dump_status, FALSE) != RR_SUCCESS){
                    dprintf("\n  *** capi_init_fec_mon disable  call failed. \r\n");
                }
            }
            break;
            
        default:
            break;
    }
    dprintf("\n****************************************\r\n");
}

void fec_stat_tests(capi_phy_info_t* phy_info_ptr)
{
    int fec_test_case, idx;

    dprintf("\n*** Please choose FEC testing itmes ......\r\n");
    for (idx = 0; idx < FEC_TEST_MAX; idx++)
        dprintf("** %d -- %s\r\n", idx, fec_test_description[idx]);

    scanf("%d", &fec_test_case);
    if (fec_test_case >= FEC_TEST_MAX) {
        dprintf("\n!!! ERROR--> Invalid FEC test option %d used. !!! \r\n", fec_test_case);
        return;
    }

    fec_stat_test(phy_info_ptr->phy_id, fec_test_case);
}
/****************************************************************************
 * 
 * FEC Statistics Test
 *****************************************************************************/
void test_fec_status_dump(capi_fec_dump_status_t* capi_fec_dump_status_ptr)
{
    uint8_t lane;
    char fec_ab[20];

    dprintf("\n  *** FUNC MODE = %s \r\n", port_func_mode_description[capi_fec_dump_status_ptr->func_mode]);
    dprintf("\n  *** FEC MODE  = %s \r\n", port_fec_mode_description[capi_fec_dump_status_ptr->fec_mode]);
    if(capi_fec_dump_status_ptr->fec_mode == CAPI_FEC_LINE)
        dprintf("\n  *** FEC TYPE  = %s \r\n", port_line_fec_type_description[capi_fec_dump_status_ptr->fec_type.line_fec]);
    else
        dprintf("\n  *** FEC TYPE  = %s \r\n", port_host_fec_type_description[capi_fec_dump_status_ptr->fec_type.host_fec]);
    
    dprintf("\n pre-fec BER = %0.3e, post-fec BER = %0.3e, projected post-fec BER = %0.3e\n\n", 
        capi_fec_dump_status_ptr->fec_ber.pre_fec_ber,
        capi_fec_dump_status_ptr->fec_ber.post_fec_ber,
        capi_fec_dump_status_ptr->fec_ber.post_fec_ber_proj);

    dprintf("\n\n  *** BRCM Statistics *** \r\n");
    if (capi_fec_dump_status_ptr->func_mode == CAPI_MODE_200G || capi_fec_dump_status_ptr->func_mode == CAPI_MODE_400G) {
        util_memcpy((void*)fec_ab, "FEC_a ", sizeof(fec_ab));
    } else {
        util_memcpy((void*)fec_ab, "", sizeof(fec_ab));
    }
    dprintf("\n  *** %sBRCM TOTAL CW RECIEVED COUNT       =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frame_rev_cnt, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frame_rev_cnt);
    dprintf("\n  *** %sBRCM TOTAL CW CORRECTED COUNT      =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frame_corr_cnt, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frame_corr_cnt);
    dprintf("\n  *** %sBRCM TOTAL CW UNCORRECTED COUNT    =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frame_uncorr_cnt, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frame_uncorr_cnt);
    dprintf("\n  *** %sBRCM TOTAL SYMBOL CORRECTED COUNT  =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_symbols_corr_cnt, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_symbols_corr_cnt);
    dprintf("\n  *** %sBRCM TOTAL CORRECTED 0's COUNT     =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_bits_corr_cnt[0], capi_fec_dump_status_ptr->fec_a_err_cnt.tot_bits_corr_cnt[0]);
    dprintf("\n  *** %sBRCM TOTAL CORRECTED 1's COUNT     =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_bits_corr_cnt[1], capi_fec_dump_status_ptr->fec_a_err_cnt.tot_bits_corr_cnt[1]);
    for (lane=0; lane<FEC_TOT_FRAMES_ERR_NUM; lane++) {
        dprintf("\n  *** %sBRCM TOTAL CW CORRECTED COUNT with %2d SYMBOL CORRECTED PER CW =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, lane, capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frames_err_cnt[lane], capi_fec_dump_status_ptr->fec_a_err_cnt.tot_frames_err_cnt[lane]);
    }
    if (capi_fec_dump_status_ptr->func_mode == CAPI_MODE_200G || capi_fec_dump_status_ptr->func_mode == CAPI_MODE_400G) {
        util_memcpy((void*)fec_ab, "FEC_b ", sizeof(fec_ab));
        dprintf("\n  *** %sBRCM TOTAL CW RECIEVED COUNT       =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frame_rev_cnt, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frame_rev_cnt);
        dprintf("\n  *** %sBRCM TOTAL CW CORRECTED COUNT      =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frame_corr_cnt, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frame_corr_cnt);
        dprintf("\n  *** %sBRCM TOTAL CW UNCORRECTED COUNT    =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frame_uncorr_cnt, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frame_uncorr_cnt);
        dprintf("\n  *** %sBRCM TOTAL SYMBOL CORRECTED COUNT  =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_symbols_corr_cnt, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_symbols_corr_cnt);
        dprintf("\n  *** %sBRCM TOTAL CORRECTED 0's COUNT     =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_bits_corr_cnt[0], capi_fec_dump_status_ptr->fec_b_err_cnt.tot_bits_corr_cnt[0]);
        dprintf("\n  *** %sBRCM TOTAL CORRECTED 1's COUNT     =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_bits_corr_cnt[1], capi_fec_dump_status_ptr->fec_b_err_cnt.tot_bits_corr_cnt[1]);
        for (lane=0; lane<FEC_TOT_FRAMES_ERR_NUM; lane++) {
            dprintf("\n  *** %sBRCM TOTAL CW CORRECTED COUNT with %2d SYMBOL CORRECTED PER CW =   hex( 0x%012llX )    |   dec( %llu ) \r\n", fec_ab, lane, capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frames_err_cnt[lane], capi_fec_dump_status_ptr->fec_b_err_cnt.tot_frames_err_cnt[lane]);
        }
    }
    
    dprintf("\n\n  *** BRCM Sticky and Active Status *** \r\n");
    dprintf("  *** BRCM IGBOX CLSN STICKY STATUS        = 0x%x\r\n", capi_fec_dump_status_ptr->fec_sts.igbox_clsn_sticky);
    dprintf("  *** BRCM AM UNLOCK STICKY STATUS         = 0x%x\r\n", capi_fec_dump_status_ptr->fec_sts.am_lolock_sticky);
    dprintf("  *** BRCM XDEC GBOX CLSN STICKY STATUS    = 0x%x\r\n", capi_fec_dump_status_ptr->fec_sts.xdec_gbox_clsn_sticky);
    dprintf("  *** BRCM XENC GBOX CLSN STICKY STATUS    = 0x%x\r\n", capi_fec_dump_status_ptr->fec_sts.xenc_gbox_clsn_sticky);
    dprintf("  *** BRCM OGBOX CLSN STICKY STATUS        = 0x%x\r\n", capi_fec_dump_status_ptr->fec_sts.ogbox_clsn_sticky);
    dprintf("  *** BRCM FEC ALIGNMENT LOL STICKY STATUS = 0x%x\r\n", capi_fec_dump_status_ptr->fec_sts.fec_algn_lol_sticky);
    dprintf("  *** BRCM FEC ALIGNMENT LIVE STAT         = %u \r\n",  capi_fec_dump_status_ptr->fec_sts.fec_algn_stat);

    dprintf("\n\n");

}
