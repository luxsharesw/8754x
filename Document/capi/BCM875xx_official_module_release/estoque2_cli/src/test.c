#include <time.h>
#include <stdlib.h>
#include <Windows.h>
#include "type_defns.h"
#include "access.h"
#include "reg_access.h"
#include "common_def.h"
#include "fw_gp_reg_map.h"
#include "estoque2_regs.h"

#include "logger.h"
#include "hr_time.h"

#include "chip_mode_def.h"
#include "capi_def.h"
#include "capi_diag_def.h"
#include "capi_test_def.h"

#include "capi.h"


#include "common_util.h"
#include "sv_modes.h"
#include "top_test.h"
#include "sv_modes_test.h"
#include "capi_chip_test.h"
#include "chip_common_config_ind.h"


#include "fec_test.h"

    typedef enum {
        CHANGE_USB_ID,
        CHANGE_PHY_ID,
        FIRMWARE_DOWNLOAD,
        FIRMWARE_DOWNLOAD_STATUS,
        FIRMWARE_SPI_PROGRAM,
        CHANGE_CHIP_MODE,
        CAPI_TEST_CASES,
        FEC_ST_CASE,
        GET_CHIP_INFO,
        GET_CHIP_STATUS,
        GET_VOLTAGE_TEMPERATURE,
        CHIP_TEST_SOFT_RESET,
        CHIP_TEST_HARD_RESET,
        AVS_CONFIG,
        AVS_STATUS,
        INTERNAL_0p75v_TEST,
        INTERNAL_0p9v_TEST,
        CONFIG_BOOTUP_DEFAULT,
        CONFIG_RPTR_INDEPENDENT_LANE_CTRL,
        CONFIG_LINE_PLL_DDCC_CTRL,
        CLI_TEST_MAX
    } cli_test_type_e;

    static char test_description[CLI_TEST_MAX][100] = {
        "CHANGE_USB_ID",
        "CHANGE_PHY_ID",
        "FIRMWARE_DOWNLOAD",
        "FIRMWARE_DOWNLOAD_STATUS",
        "FIRMWARE_SPI_PROGRAM",
        "CHANGE_CHIP_MODE",
        "CAPI_TEST_CASES",
        "FEC_ST_CASE",
        "GET_CHIP_INFO",
        "GET_CHIP_STATUS",
        "GET_VOLTAGE_TEMPERATURE",
        "CHIP_TEST_SOFT_RESET",
        "CHIP_TEST_HARD_RESET",
        "AVS_CONFIG",
        "AVS_STATUS",
        "INTERNAL_0p75v_TEST",
        "INTERNAL_0p9v_TEST",
        "CONFIG_BOOTUP_DEFAULT",
        "CONFIG_RPTR_INDEPENDENT_LANE_CTRL",
        "CONFIG_LINE_PLL_DDCC_CTRL",
    };


static void cli_change_usb_id(void) {
    int selection;
    dprintf("\nPlease choose USB com:  ");
    scanf("%d", &selection);
    set_usb_com(selection);
    delay_s(1);
}

static void cli_change_phy_id(capi_phy_info_t* phy_info_ptr, capi_chip_info_t* chip_info_ptr)
{
    int selection;
    util_memset(chip_info_ptr, 0x00, sizeof(capi_chip_info_t));

    dprintf("\nPlease choose phy_id: ");
    scanf("%d", &selection);
    dprintf("%d \r\n", selection);
    phy_info_ptr->phy_id = selection;

    util_memset(chip_info_ptr, 0, sizeof(capi_chip_info_t));
    chip_info_ptr->param.is.hw_info = 1;
    capi_get_chip_info(phy_info_ptr, chip_info_ptr);
    dprintf("Chip ID = 0x%x  :  Revision ID = 0x%x \r\n", chip_info_ptr->value.hw_info.chip_id, chip_info_ptr->value.hw_info.chip_revision);
}


/**
 * @brief void cli(void)
 *
 * The test code has been verified on FPGA board;
 * scanning all supported block in FPGA board
 *
 * @param  void
 * @return void
 */
void cli(void)
{
    uint8_t lane_num = 4;
    uint8_t idx;

    uint32_t chip_id;
    capi_phy_info_t      cli_phy_info;                                       /**< Phy Information    */
    capi_chip_info_t     cli_chip_info;                                      /**< Chip Information   */

    int test_item, test_case;

    
    dprintf("Configure settings via GUI? 0 - No; 1 / above - Yes\r\n");
    scanf("%d", &test_case);
    test_case = (test_case) ? 1 : 0;
    if (test_case) {
        display_settings_view(test_case);
        dprintf("\r\nIMPORTANT: Do not close GUI while Windows CLI is running!\r\n");
        dprintf("\r\nPlease perform the following in GUI settings panel before proceeding\r\n");
        dprintf("1. Configure USB port\r\n");
        dprintf("2. Access bus type and device address (if applicable)\r\n");
        dprintf("3. Tunneling scheme (if applicable)\r\n");
        dprintf("4. Close the settings panel when finished\r\n");
        fflush(stdin);
        printf("\r\nPress Enter to continue after register access is confirmed!\n");
        getchar();
    } else {
        cli_change_usb_id();
    }


    delay_s(1);

    cli_change_phy_id(&cli_phy_info, &cli_chip_info);


    while (1) {
        dprintf("\r\nPlease choose your testing itmes ......\r\n");
        for (idx = 0; idx < CLI_TEST_MAX; idx++)
            dprintf("** %d -- %s\r\n", idx, test_description[idx]);
        dprintf("** %d -- %s\r\n", 99, "Exit Test Case");

        scanf("%d", &test_item);
        if ((test_item >= CLI_TEST_MAX) && (test_item != 99)) {
            dprintf("Invalid option, please choose again!\r\n");
            system("pause");
            continue;
        }

        switch (test_item) 
        {
            case 99:
                dprintf("\n\n** Bye! \r\n");
                return;

            case CHANGE_USB_ID:
                cli_change_usb_id();

            case CHANGE_PHY_ID:
                cli_change_phy_id(&cli_phy_info, &cli_chip_info);
                break;

                case FIRMWARE_DOWNLOAD:
                    test_sram_download(&cli_phy_info, 0);
                    break;

                case FIRMWARE_DOWNLOAD_STATUS:
                    test_download_status(&cli_phy_info);
                    break;

                case FIRMWARE_SPI_PROGRAM:
                    test_spi_download(&cli_phy_info, 0);
                    break;

                case CHANGE_CHIP_MODE:
                {
                    util_memset(&cli_chip_info, 0, sizeof(capi_chip_info_t));
                    cli_chip_info.param.is.hw_info = 1;
                    capi_get_chip_info(&cli_phy_info, &cli_chip_info);
                    chip_id = cli_chip_info.value.hw_info.chip_id;
                    dprintf("Chip ID: 0x%x\r\n", chip_id);

                    test_chip_get_port_mode(&cli_phy_info);

                    test_modes_program_grp(&cli_phy_info);
                    test_chip_get_port_mode(&cli_phy_info);
                }
                break;

                case CAPI_TEST_CASES:
                    capi_test_top(&cli_phy_info);
                    break;

                case FEC_ST_CASE:
                    fec_stat_tests(&cli_phy_info);
                    break;


                case GET_CHIP_INFO:
                {
                    util_memset(&cli_chip_info, 0, sizeof(capi_chip_info_t));
                    cli_chip_info.param.is.hw_info = 1;
                    cli_chip_info.param.is.sw_info = 1;
                    capi_get_chip_info(&cli_phy_info, &cli_chip_info);
                    dprintf(" ---- HW Info ---- \r\n");
                    dprintf("Chip ID = 0x%x", cli_chip_info.value.hw_info.chip_id);
                    dprintf("Chip revision = 0x%x", cli_chip_info.value.hw_info.chip_revision);
                    dprintf(" ---- SW Info -----\r\n");
                    dprintf("cAPI major version: 0x%04x\r\n", cli_chip_info.value.sw_info.capi_major_version);
                    dprintf("cAPI minor version: 0x%04x\r\n", cli_chip_info.value.sw_info.capi_minor_version);
                    dprintf("Firmware version: %x%\r\n", cli_chip_info.value.sw_info.fw_version);
                    dprintf("CMIS version: %x\r\n", cli_chip_info.value.sw_info.cmis_version);
                } break;

                case GET_CHIP_STATUS:
                {
                    capi_chip_status_info_t chip_status_info;
                    return_result_t retval;

                    util_memset(&chip_status_info, 0, sizeof(chip_status_info));
                    //chip_status.param.is.download_done = 1;
                    chip_status_info.param.is.uc_ready = 1;
                    //chip_status.param.is.avs_done = 1;
                    retval = capi_get_chip_status(&cli_phy_info, &chip_status_info);
                    if(retval != RR_SUCCESS)
                    {
                        printf("\ncapi_get_chip_status failed with error code %d\n", retval);
                        break;
                    }
                    printf("UC Ready      = %d\n", chip_status_info.value.uc_ready);
                }
                break;

                case GET_VOLTAGE_TEMPERATURE:
                {
                    capi_temp_status_info_t capi_temp_status_info;
                    capi_voltage_status_info_t capi_voltage_status_info;

                    capi_get_temperture_status(&cli_phy_info,  &capi_temp_status_info);
                    dprintf("Temperature:%d\n", capi_temp_status_info.temperature);
                    capi_get_voltage_status(&cli_phy_info,  &capi_voltage_status_info);
                    dprintf("Voltage:%d\n", capi_voltage_status_info.voltage);
                }
                break;

                case CHIP_TEST_SOFT_RESET:
                    test_soft_reset(&cli_phy_info, 1);
                    break;

                case CHIP_TEST_HARD_RESET:
                    test_soft_reset(&cli_phy_info, 0);
                    break;

                case AVS_CONFIG:
                    chip_avs_config(&cli_phy_info);
                    break;

                case AVS_STATUS:
                    chip_avs_get_status(&cli_phy_info);
                    break;

                case INTERNAL_0p75v_TEST:
                    chip_internal_vddm_test(&cli_phy_info);
                    break;

                case INTERNAL_0p9v_TEST:
                    chip_internal_avdd_test(&cli_phy_info);
                    break;
                case CONFIG_BOOTUP_DEFAULT:
                    test_config_bootup_default(&cli_phy_info);
                    break;


                case CONFIG_RPTR_INDEPENDENT_LANE_CTRL:
                    test_cfg_rptr_indep_lane_ctrl(&cli_phy_info);
                    break;
                case CONFIG_LINE_PLL_DDCC_CTRL:
                    test_cfg_line_pll_ddcc_ctrl(&cli_phy_info);
                    break;


            default:
                break;

        } // swtich(test_id)

        dprintf("\n** Command Done, wait for next command \n\n");
        system("pause");
    } // while(1)

    return;
} // void test

