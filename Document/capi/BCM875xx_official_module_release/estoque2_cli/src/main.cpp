#include <stdio.h>
#include <string.h>

#include <direct.h>
#include <Windows.h>
#include "MON2_HSIP_ENG.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "logger.h"
extern void cli(void);

#ifdef __cplusplus
}
#endif

#define get_curr_working_dir   _getcwd
#define init_dependencies      sal_init
#define deinit_dependencies    sal_fini

#define dir_slash              "/"
#define log_dir                "log"

int main()
{
    char file_path[FILENAME_MAX] = {0};
    char curr_directory[FILENAME_MAX] = {0};
    char log_file_name[] = "chip_cli";
    logger_t logger;

    // get current working directory
    get_curr_working_dir(curr_directory, sizeof(curr_directory));
    strncat(file_path, curr_directory, strlen(curr_directory));
    strncat(file_path, dir_slash, strlen(dir_slash));
    strncat(file_path, log_dir, strlen(log_dir));

    // default logging level, can be changed later by logger_set_verbose_level()
    memset(&logger, 0, sizeof(logger));
    logger.verbose_level = 0;
    strncpy(logger.filename, file_path, strlen(file_path));
    logger_open(&logger, 1, log_file_name);

    init_dependencies();    //initialization sal dependencies

    cli();

    logger_close(&logger);
    fflush(stdin);
    printf("Press Enter to Continue\n");
    getchar();

    deinit_dependencies();

    return 0;
}
