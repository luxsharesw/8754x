/**
 *
 * @file chip_config_def.h
 * @author
 * @date     12/01/2016
 * @version 1.0
 *
 * @property
 * $Copyright: (c) 2020 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#ifndef CHIP_CONFIG_DEF_H
#define CHIP_CONFIG_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

#define ENABLE_CMD_SANITY_CHECK

#ifdef __cplusplus
}
#endif

#endif /* CHIP_CONFIG_DEF_H */

