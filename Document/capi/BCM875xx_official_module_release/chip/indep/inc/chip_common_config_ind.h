/****************************************************************************
*
*     Copyright (c) 2016 Broadcom Limited
*           All Rights Reserved
*
*     No portions of this material may be reproduced in any form without the
*     written permission of:
*
*           Broadcom Limited 
*           1320 Ridder Park Dr.
*            San Jose, California 95131
*            United States
*
*     All information contained in this document is Broadcom Limited 
*     company private, proprietary, and trade secret.
*
****************************************************************************/

/**
 *       Centenario chip common header file
 *
 * @file chip_common_confirg_ind.h
 * @author $Id$
 * @date 12-8-2016
 * @brief This file includes Centenario chip level common header
 */

#ifndef CHIP_COMMON_CONFIG_IND_H
#define CHIP_COMMON_CONFIG_IND_H

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
 * blackhawk_estoque_com_amba :: CHIP_TOP_CHIP_REFCLK_CONFIG_REG 
 ***************************************************************************/
typedef union chip_top_chip_refclk_config_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t ref_clk_valid : 1; /*!<ref_clk_valid */
        uint16_t reserved0 : 10; /*!<reserved0 */
        uint16_t ref_clk_freq_select : 5; /*!<ref_clk_freq_select */
#else /* LITTLE ENDIAN */
        uint16_t ref_clk_freq_select : 5; /*!<ref_clk_freq_select */
        uint16_t reserved0 : 10; /*!<reserved0 */
        uint16_t ref_clk_valid : 1; /*!<ref_clk_valid */
#endif
    } fields;
} chip_top_chip_refclk_config_reg_t;

/****************************************************************************
 * top_pam_amba :: COMMON_LANE_MODE_REG 
 ***************************************************************************/
typedef union common_lane_mode_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t blackhawk_loopback_alt_mode : 1; /*!<blackhawk_loopback_alt_mode */
        uint16_t function_mode : 6; /*!<function_mode */
        uint16_t fec_mode : 2; /*!<fec_mode */
        uint16_t rate_mode : 7; /*!<rate_mode */
#else /* LITTLE ENDIAN */
        uint16_t rate_mode : 7; /*!<rate_mode */
        uint16_t fec_mode : 2; /*!<fec_mode */
        uint16_t function_mode : 6; /*!<function_mode */
        uint16_t blackhawk_loopback_alt_mode : 1; /*!<blackhawk_loopback_alt_mode */
#endif
    } fields;
} common_lane_mode_reg_t;


/****************************************************************************
 * top_pam_amba :: COMMON_LANE_COMMAND_REG 
 ***************************************************************************/
typedef union common_lane_command_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t command_change : 1; /*!<command_change */
        uint16_t reserved0 : 7; /*!<reserved0 */
        uint16_t command : 8; /*!<command */
#else /* LITTLE ENDIAN */
        uint16_t command : 8; /*!<command */
        uint16_t reserved0 : 7; /*!<reserved0 */
        uint16_t command_change : 1; /*!<command_change */
#endif
    } fields;
} common_lane_command_reg_t;

/****************************************************************************
 * top_pam_amba :: COMMON_LANE_STATUS_REG, COMMON_BH_LANE_COMMAND_STATUS_REG_0 to 7, LW_LANE_STATUS_REG_0 
 ***************************************************************************/

enum cmd_status_error_code_u{
    CMD_STATUS_NO_ERROR = 0,
    CMD_STATUS_ERROR_NO_SUPPORT = 1,
};


typedef union common_lane_status_reg_u {
    uint16_t words;
    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint16_t command_accepted : 1; /*!<command_accepted */
        uint16_t ready_for_command : 1; /*!<ready_for_command */
        uint16_t error_code : 6; /*!<error_code */
        uint16_t status_return : 8; /*!<status_return */
        #else /* LITTLE ENDIAN */
        uint16_t status_return : 8; /*!<status_return */
        uint16_t error_code : 6; /*!<error_code */
        uint16_t ready_for_command : 1; /*!<ready_for_command */
        uint16_t command_accepted : 1; /*!<command_accepted */
        #endif
    } fields;
} common_lane_status_reg_t;





/****************************************************************************
 *  FW_INTERNAL_CONFIG_REG_1 QUAD_CORE_GPCTRL_RDB_GPCTRL_6_RDB
 
 ***************************************************************************/

#define FW_INTERNAL_CONFIG_REG_1_SUPER_CMODE_CFG_EN_MASK 0x00000001 /*!< SUPER_CMODE_CFG_EN*/
#define FW_INTERNAL_CONFIG_REG_1_SUPER_CMODE_CFG_EN_SHIFT 0
#define FW_INTERNAL_CONFIG_REG_1_SUPER_CMODE_IGNORE_PLLCLSN_EN_MASK 0x00000002 /*!< SUPER_CMODE_IGNORE_PLLCLSN_EN*/
#define FW_INTERNAL_CONFIG_REG_1_SUPER_CMODE_IGNORE_PLLCLSN_EN_SHIFT 1

#define FW_INTERNAL_CONFIG_REG_1_EN_RPTR_INDEP_LANE_HANDLE_MASK 0x00000010 /*!< EN_RPTR_INDEP_LANE_HANDLE*/
#define FW_INTERNAL_CONFIG_REG_1_EN_RPTR_INDEP_LANE_HANDLE_SHIFT 4

#define FW_INTERNAL_CONFIG_REG_1_QSFP_FORCE_EN_MASK 0x00000100 /*!< QSFP_FORCE_EN*/
#define FW_INTERNAL_CONFIG_REG_1_QSFP_FORCE_EN_SHIFT 8
#define FW_INTERNAL_CONFIG_REG_1_QSFP_FORCE_VAL_MASK 0x00000200 /*!< QSFP_FORCE_VAL*/
#define FW_INTERNAL_CONFIG_REG_1_QSFP_FORCE_VAL_SHIFT 9

#define FW_INTERNAL_CONFIG_REG_1_DISABLE_LW_PLL_DEPENDENCE_MASK 0x00002000 /*!< DISABLE_LW_PLL_DEPENDENCE*/
#define FW_INTERNAL_CONFIG_REG_1_DISABLE_LW_PLL_DEPENDENCE_SHIFT 13

#define FW_INTERNAL_CONFIG_REG_1_DISABLE_CW_CLK66_RST_MASK 0x00004000 /*!< DISABLE_CW_CLK66_RST*/
#define FW_INTERNAL_CONFIG_REG_1_DISABLE_CW_CLK66_RST_SHIFT 14
#define FW_INTERNAL_CONFIG_REG_1_IGNORE_LW_PLL_LOL_MASK 0x00008000 /*!< IGNORE_LW_PLL_LOL*/
#define FW_INTERNAL_CONFIG_REG_1_IGNORE_LW_PLL_LOL_SHIFT 15

typedef union octal_fw_internal_cfg_1_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t ignore_lw_pll_lol:1;
        uint16_t disable_cw_clk66_rst:1;
        uint16_t rsvdd:4;
        uint16_t qsfp_force_val:1;   /*!<qsfp_force_val*/
        uint16_t qsfp_force_en:1; /*!<qsfp_force_en*/
        uint16_t rsvd:6;
        uint16_t super_cmode_ignore_pllclsn_en:1;   
        uint16_t super_cmode_cfg_en:1; /*!<fw backdoor to support all chip_mode no matter what chip_id */
#else /* LITTLE ENDIAN */
        uint16_t super_cmode_cfg_en:1; /*!<fw backdoor to support all chip_mode no matter what chip_id */
        uint16_t super_cmode_ignore_pllclsn_en:1; 
        uint16_t rsvd:6; 
        uint16_t qsfp_force_en:1; /*!<qsfp_force_en*/ 
        uint16_t qsfp_force_val:1;   /*!<qsfp_force_val*/
        uint16_t rsvdd:4;
        uint16_t disable_cw_clk66_rst:1;
        uint16_t ignore_lw_pll_lol:1;
#endif
    } fields;
} octal_fw_internal_cfg_1_t;


/****************************************************************************
 *  FW_INTERNAL_CONFIG_REG_2 QUAD_CORE_GPCTRL_RDB_GPCTRL_7_RDB
 
 ***************************************************************************/
#define FW_INTERNAL_CONFIG_REG_2_DISABLE_DUAL_DIE_DEPENDENCE_MASK 0x00008000 /*!< DISABLE_DUAL_DIE_DEPENDENCE*/
#define FW_INTERNAL_CONFIG_REG_2_DISABLE_DUAL_DIE_DEPENDENCE_SHIFT 15

#define FW_INTERNAL_CONFIG_REG_2_ENABLE_TC_SE_MODE_MASK 0x00000004 /*!< ENABLE_TC_SE_MODE*/
#define FW_INTERNAL_CONFIG_REG_2_ENABLE_TC_SE_MODE_SHIFT 2   

#define FW_INTERNAL_CONFIG_REG_2_ENABLE_ELECTRIC_MODE_MASK 0x00000002 /*!< EANBLE_ELECTRIC_MODE*/
#define FW_INTERNAL_CONFIG_REG_2_ENABLE_ELECTRIC_MODE_SHIFT 1   
#define FW_INTERNAL_CONFIG_REG_2_ENABLE_HS_TO_LS_BOOTUP_MASK 0x00000001 /*!< ENABLE_HS_TO_LS_BOOTUP*/
#define FW_INTERNAL_CONFIG_REG_2_ENABLE_HS_TO_LS_BOOTUP_SHIFT 0





/****************************************************************************
 *  FW_INTERNAL_CONFIG_REG_3 QUAD_CORE_CONFIG_CFGVAL_35_RDB
 
 ***************************************************************************/
#define FW_INTERNAL_CONFIG_REG_3_LW_PTUNE_DISABLE_MODE_CHECK_MASK 0x00008000 /*!< LW_PTUNE_DISABLE_MODE_CHECK */
#define FW_INTERNAL_CONFIG_REG_3_LW_PTUNE_DISABLE_MODE_CHECK_SHIFT 15

#define FW_INTERNAL_CONFIG_REG_3_ENABLE_PLL_DDCC_CFG_MASK 0x00000008 /*!< ENABLE_PLL_DDCC_CFG*/
#define FW_INTERNAL_CONFIG_REG_3_ENABLE_PLL_DDCC_CFG_SHIFT 3
#define FW_INTERNAL_CONFIG_REG_3_DISABLE_PLL_DDCC_CTRL_MASK 0x00000004 /*!< DISABLE_PLL_DDCC_CTRL*/
#define FW_INTERNAL_CONFIG_REG_3_DISABLE_PLL_DDCC_CTRL_SHIFT 2
#define FW_INTERNAL_CONFIG_REG_3_DISABLE_PLL_TRY_LIMIT_MASK 0x00000002 /*!< DISABLE_PLL_TRY_LIMIT*/
#define FW_INTERNAL_CONFIG_REG_3_DISABLE_PLL_TRY_LIMIT_SHIFT 1
#define FW_INTERNAL_CONFIG_REG_3_EX_DFT_CMODE_CFG_EN_MASK 0x00000001 /*!< EX_DFT_CMODE_CFG_EN*/
#define FW_INTERNAL_CONFIG_REG_3_EX_DFT_CMODE_CFG_EN_SHIFT 0


typedef union octal_fw_internal_cfg_3_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t rsvd:15;
        uint16_t ex_dft_cmode_cfg_en:1;/*!<fw backdoor to enable default chip mode from external cAPI */
#else /* LITTLE ENDIAN */
        uint16_t ex_dft_cmode_cfg_en:1;/*!<fw backdoor to enable default chip mode from external cAPI */
        uint16_t rsvd:15;
#endif
    } fields;
} octal_fw_internal_cfg_3_t;


/****************************************************************************
 * #define LW_CONFIG_PLL_DEBUG_REG                        QUAD_CORE_GPCTRL_GPCTRL_91_RDB
 cAPI set, firmware get 
 ***************************************************************************/
#define LW_CONFIG_PLL_DEBUG_REG_PLL_RZ_EXTRA_OVRD_EN_MASK 0x00000010
#define LW_CONFIG_PLL_DEBUG_REG_PLL_RZ_EXTRA_OVRD_EN_SHIFT 4
#define LW_CONFIG_PLL_DEBUG_REG_PLL_RZ_SEL_OVRD_EN_MASK 0x00000008 /*!< PLL_RZ_SEL_OVRD_EN*/
#define LW_CONFIG_PLL_DEBUG_REG_PLL_RZ_SEL_OVRD_EN_SHIFT 3

#define LW_CONFIG_PLL_DEBUG_REG_ENABLE_PLL_LB_TRY_MASK 0x00000004 /*!< ENABLE_PLL_LB_TRY*/
#define LW_CONFIG_PLL_DEBUG_REG_ENABLE_PLL_LB_TRY_SHIFT 2

#define LW_CONFIG_PLL_DEBUG_REG_ENABLE_NRZ_INV_VCO_CAL_MASK 0x00000002 /*!< ENABLE_NRZ_INV_VCO_CAL*/
#define LW_CONFIG_PLL_DEBUG_REG_ENABLE_NRZ_INV_VCO_CAL_SHIFT 1
#define LW_CONFIG_PLL_DEBUG_REG_DISABLE_PLL_LOCK_RETRY_MASK 0x00000001 /*!< DISABLE_PLL_LOCK_RETRY*/
#define LW_CONFIG_PLL_DEBUG_REG_DISABLE_PLL_LOCK_RETRY_SHIFT 0

/****************************************************************************
 * #define LW_CONFIG_PLL_DEBUG_ST_REG                        QUAD_CORE_GPCTRL_GPCTRL_92_RDB
 cAPI get, firmware set
 
 ***************************************************************************/
#define LW_CONFIG_PLL_DEBUG_ST_REG_PLL_LOCK_TIME_MS_MASK 0x0000FFF0 /*!< PLL_LOCK_TIME_MS*/
#define LW_CONFIG_PLL_DEBUG_ST_REG_PLL_LOCK_TIME_MS_SHIFT 4
#define LW_CONFIG_PLL_DEBUG_ST_REG_PLL_LOCK_CNT_MASK 0x00000007 /*!< PLL_LOCK_CNT*/
#define LW_CONFIG_PLL_DEBUG_ST_REG_PLL_LOCK_CNT_SHIFT 0

/****************************************************************************
 * #define LW_CONFIG_PLL_DEBUG_ST1_REG                    QUAD_CORE_GPCTRL_GPCTRL_93_RDB
  cAPI get, firmware set
 ***************************************************************************/
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_HB_VCODE_0_MASK 0x000400 /*!< PLL_HB_VCODE_0*/
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_HB_VCODE_0_SHIFT 10
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_LB_VCODE_FF_MASK 0x00000200 /*!< PLL_LB_VCODE_FF*/
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_LB_VCODE_FF_SHIFT 9
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_INTERN_LOCK_IDX_MASK 0x000001C0 /*!< PLL_INTERN_LOCK_IDX*/
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_INTERN_LOCK_IDX_SHIFT 6

#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_LOCK_FAIL_CNT_MASK 0x0000003C /*!< PLL_LOCK_FAIL_CNT*/
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_LOCK_FAIL_CNT_SHIFT 2
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_REFCLK_RATESEL_MASK 0x00000003 /*!< PLL_REFCLK_RATESEL*/
#define LW_CONFIG_PLL_DEBUG_ST1_REG_PLL_REFCLK_RATESEL_SHIFT 0

/****************************************************************************
 * PORT Mode cAPI Conifg1 Registers:  QUAD_CORE_CONFIG_RDB_CFGVAL_6_RDB
 
 ***************************************************************************/
#define PORT_MODE_CAPI_CONFIG_1_REG_PORT_VLD_MASK 0x00008000 /*!< PORT Valid*/
#define PORT_MODE_CAPI_CONFIG_1_REG_PORT_VLD_SHIFT 15
#define PORT_MODE_CAPI_CONFIG_1_REG_PORT_INDEX_MASK 0x00007000 /*!< PORT_INDEX*/
#define PORT_MODE_CAPI_CONFIG_1_REG_PORT_INDEX_SHIFT 12
#define PORT_MODE_CAPI_CONFIG_1_REG_RSVD_MASK 0x00000F00 /*!< RSVD*/
#define PORT_MODE_CAPI_CONFIG_1_REG_RSVD_SHIFT 8
#define PORT_MODE_CAPI_CONFIG_1_REG_PORT_MODE_MASK 0x000000FF /*!< PORT MODE*/
#define PORT_MODE_CAPI_CONFIG_1_REG_PORT_MODE_SHIFT 0



/****************************************************************************
 * PORT Mode cAPI Conifg2 Registers:  QUAD_CORE_CONFIG_RDB_CFGVAL_7_RDB
 
 ***************************************************************************/
#define PORT_MODE_CAPI_CONFIG_2_REG_RSVD_MASK 0x0000F800 /*!<RSVD*/
#define PORT_MODE_CAPI_CONFIG_2_REG_RSVD_SHIFT 11
#define PORT_MODE_CAPI_CONFIG_2_REG_FEC_TERM_MASK 0x0000780 /*!< FEC_TERM*/
#define PORT_MODE_CAPI_CONFIG_2_REG_FEC_TERM_SHIFT 7
#define PORT_MODE_CAPI_CONFIG_2_REG_SYMBL_MUX_MASK 0x00000040 /*!< SYMBL_MUX*/
#define PORT_MODE_CAPI_CONFIG_2_REG_SYMBL_MUX_SHIFT 6
#define PORT_MODE_CAPI_CONFIG_2_REG_LINE_FEC_TYPE_MASK 0x00000038 /*!< LINE_FEC_TYPE*/
#define PORT_MODE_CAPI_CONFIG_2_REG_LINE_FEC_TYPE_SHIFT 3
#define PORT_MODE_CAPI_CONFIG_2_REG_HOST_FEC_TYPE_MASK 0x00000007 /*!< HOST_FEC_TYPE*/
#define PORT_MODE_CAPI_CONFIG_2_REG_HOST_FEC_TYPE_SHIFT 0





/****************************************************************************
 * PORT Mode Conifg1 Registers: OCTAL_PORT_MODE_CONFIG_1_REG_0
 ***************************************************************************/
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_PORT_VLD_MASK 0x00008000 /*!< PORT Valid*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_PORT_VLD_SHIFT 15
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_PORT_INDEX_MASK 0x00007000 /*!< PORT_INDEX*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_PORT_INDEX_SHIFT 12
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_RSVD_MASK 0x00000F00 /*!< RSVD*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_RSVD_SHIFT 8
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_PORT_MODE_MASK 0x000000FF /*!< PORT MODE*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_0_PORT_MODE_SHIFT 0



/****************************************************************************
 * PORT Mode Conifg2 Registers: OCTAL_PORT_MODE_CONFIG_2_REG_0
 ***************************************************************************/
    
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_RSVD_MASK 0x0000F800 /*!<RSVD*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_RSVD_SHIFT 11
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_FFEC_TERM_MASK 0x0000780 /*!< FEC_TERM*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_FEC_TERM_SHIFT 7
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_SYMBL_MUX_MASK 0x00000040 /*!< SYMBL_MUX*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_SYMBL_MUX_SHIFT 6
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_LINE_FEC_TYPE_MASK 0x00000038 /*!< LINE_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_LINE_FEC_TYPE_SHIFT 3
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_HOST_FEC_TYPE_MASK 0x00000007 /*!< HOST_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_0_HOST_FEC_TYPE_SHIFT 0


/****************************************************************************
 * PORT Mode Conifg1 Registers: OCTAL_PORT_MODE_CONFIG_1_REG_1
 ***************************************************************************/
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_PORT_VLD_MASK 0x00008000 /*!< PORT Valid*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_PORT_VLD_SHIFT 15
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_PORT_INDEX_MASK 0x00007000 /*!< PORT_INDEX*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_PORT_INDEX_SHIFT 12
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_RSVD_MASK 0x00000F00 /*!< RSVD*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_RSVD_SHIFT 8
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_PORT_MODE_MASK 0x000000FF /*!< PORT MODE*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_1_PORT_MODE_SHIFT 0




/****************************************************************************
 * PORT Mode Conifg2 Registers: OCTAL_PORT_MODE_CONFIG_2_REG_1
 ***************************************************************************/
    
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_RSVD_MASK 0x0000F800 /*!<RSVD*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_RSVD_SHIFT 11
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_FFEC_TERM_MASK 0x0000780 /*!< FEC_TERM*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_FEC_TERM_SHIFT 7
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_SYMBL_MUX_MASK 0x00000040 /*!< SYMBL_MUX*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_SYMBL_MUX_SHIFT 6
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_LINE_FEC_TYPE_MASK 0x00000038 /*!< LINE_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_LINE_FEC_TYPE_SHIFT 3
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_HOST_FEC_TYPE_MASK 0x00000007 /*!< HOST_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_1_HOST_FEC_TYPE_SHIFT 0



/****************************************************************************
 * PORT Mode Conifg1 Registers: OCTAL_PORT_MODE_CONFIG_1_REG_2
 ***************************************************************************/
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_PORT_VLD_MASK 0x00008000 /*!< PORT Valid*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_PORT_VLD_SHIFT 15
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_PORT_INDEX_MASK 0x00007000 /*!< PORT_INDEX*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_PORT_INDEX_SHIFT 12
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_RSVD_MASK 0x00000F00 /*!< RSVD*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_RSVD_SHIFT 8
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_PORT_MODE_MASK 0x000000FF /*!< PORT MODE*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_2_PORT_MODE_SHIFT 0



/****************************************************************************
 * PORT Mode Conifg2 Registers: OCTAL_PORT_MODE_CONFIG_2_REG_2
 ***************************************************************************/
    
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_RSVD_MASK 0x0000F800 /*!<RSVD*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_RSVD_SHIFT 11
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_FFEC_TERM_MASK 0x0000780 /*!< FEC_TERM*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_FEC_TERM_SHIFT 7
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_SYMBL_MUX_MASK 0x00000040 /*!< SYMBL_MUX*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_SYMBL_MUX_SHIFT 6
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_LINE_FEC_TYPE_MASK 0x00000038 /*!< LINE_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_LINE_FEC_TYPE_SHIFT 3
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_HOST_FEC_TYPE_MASK 0x00000007 /*!< HOST_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_2_HOST_FEC_TYPE_SHIFT 0


/****************************************************************************
 * PORT Mode Conifg1 Registers: OCTAL_PORT_MODE_CONFIG_1_REG_3
 ***************************************************************************/
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_PORT_VLD_MASK 0x00008000 /*!< PORT Valid*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_PORT_VLD_SHIFT 15
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_PORT_INDEX_MASK 0x00007000 /*!< PORT_INDEX*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_PORT_INDEX_SHIFT 12
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_RSVD_MASK 0x00000F00 /*!< RSVD*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_RSVD_SHIFT 8
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_PORT_MODE_MASK 0x000000FF /*!< PORT MODE*/
#define OCTAL_PORT_MODE_CONFIG_1_REG_3_PORT_MODE_SHIFT 0



/****************************************************************************
 * PORT Mode Conifg2 Registers: OCTAL_PORT_MODE_CONFIG_2_REG_3
 ***************************************************************************/

#define OCTAL_PORT_MODE_CONFIG_2_REG_3_RSVD_MASK 0x0000F800 /*!<RSVD*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_RSVD_SHIFT 11
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_FFEC_TERM_MASK 0x0000780 /*!< FEC_TERM*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_FEC_TERM_SHIFT 7
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_SYMBL_MUX_MASK 0x00000040 /*!< SYMBL_MUX*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_SYMBL_MUX_SHIFT 6
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_LINE_FEC_TYPE_MASK 0x00000038 /*!< LINE_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_LINE_FEC_TYPE_SHIFT 3
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_HOST_FEC_TYPE_MASK 0x00000007 /*!< HOST_FEC_TYPE*/
#define OCTAL_PORT_MODE_CONFIG_2_REG_3_HOST_FEC_TYPE_SHIFT 0


/****************************************************************************
 * PORT Mode Conifg1 Registers: octal_core.GP0_REGS [8,10,12,14,16,18,20,22]
 ***************************************************************************/
typedef union octal_port_mode_config1_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t port_vld:1;   /*!<0 - PORT config1/2 not valid; 1 - PORT config1/2 valid */
        uint16_t port_idx:3; /*!<HW/FW port idx */
        uint16_t rsvd:4;
        uint16_t port_mode_enum:8; /*!<HW/FW port config modes */
#else /* LITTLE ENDIAN */
        uint16_t port_mode_enum:8; /*!<HW/FW port config modes */
        uint16_t rsvd:4;
        uint16_t port_idx:3; /*!<HW/FW port idx */
        uint16_t port_vld:1;   /*!<0 - PORT config1/2 not valid; 1 - PORT config1/2 valid */
#endif
    } fields;
} octal_port_mode_config1_reg_t;


/****************************************************************************
 * PORT Mode Conifg2 Registers: octal_core.GP0_REGS [9,11,13,15,17,19,21,23]
 ***************************************************************************/
typedef union octal_port_mode_config2_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t rsvd:5;
        uint16_t fec_term:4; /*!<0-NO_FEC; 1--FEC_DEC_FWD; 2 -- FEC_DEC_ENC; 3--PCS_XENC; 4--FEC_DEC_XDEC_XENC_ENC; 5-- FEC_XENC_PCS*/
        uint16_t line_symbol_mux:1;
        uint16_t line_fec_type:3; /*!<line FEC type (0-NO_FEC;  1-RS528; 2-RS544; 3-PCS) */
        uint16_t host_fec_type:3; /*!<Host FEC Type (0-NO_FEC; 1-RS528; 2-RS544; 3-PCS ) */
#else /* LITTLE ENDIAN */
        uint16_t host_fec_type:3; /*!<Host FEC Type (0-NO_FEC; 1-RS528; 2-RS544; 3-PCS ) */
        uint16_t line_fec_type:3;  /*!<line FEC type (0-NO_FEC;  1-RS528; 2-RS544; 3-PCS) */
        uint16_t line_symbol_mux:1;       
        uint16_t fec_term:4; /*!<0-NO_FEC; 1--FEC_DEC_FWD; 2 -- FEC_DEC_ENC; 3--PCS_XENC; 4--FEC_DEC_XDEC_XENC_ENC*/
        uint16_t rsvd:5;
#endif
    } fields;
} octal_port_mode_config2_reg_t;


/****************************************************************************
 * PORT Mode Status1 Registers: OCTAL_PORT_MODE_STATUS_1_REG
 ***************************************************************************/
#define OCTAL_PORT_MODE_STATUS_1_REG_PORT_VLD_MASK 0x00008000 /*!< PORT Valid*/
#define OCTAL_PORT_MODE_STATUS_1_REG_PORT_VLD_SHIFT 15
#define OCTAL_PORT_MODE_STATUS_1_REG_RSVD_MASK 0x00007800 /*!< RSVD*/
#define OCTAL_PORT_MODE_STATUS_1_REG_RSVD_SHIFT 8
#define OCTAL_PORT_MODE_STATUS_1_REG_CONFIG_STATUS_MASK 0x00000700 /*!< Port config status*/
#define OCTAL_PORT_MODE_STATUS_1_REG_CONFIG_STATUS_SHIFT 8
#define OCTAL_PORT_MODE_STATUS_1_REG_PORT_MODE_MASK 0x000000FF /*!< PORT MODE*/
#define OCTAL_PORT_MODE_STATUS_1_REG_PORT_MODE_SHIFT 0


/****************************************************************************
 * PORT Mode Conifg2 Registers: OCTAL_PORT_MODE_STATUS_2_REG
 ***************************************************************************/
#define OCTAL_PORT_MODE_STATUS_2_REG_RSVD_MASK 0x0000C000 /*!< RSVD*/
#define OCTAL_PORT_MODE_STATUS_2_REG_RSVD_SHIFT 14
#define OCTAL_PORT_MODE_STATUS_2_REG_POWER_DOWN_ST_MASK 0x000002000 /*!< POWER_DOWN_ST*/
#define OCTAL_PORT_MODE_STATUS_2_REG_POWER_DOWN_ST_SHIFT 13
#define OCTAL_PORT_MODE_STATUS_2_REG_PORT_IDX_MASK 0x00001C00 /*!< PORT_IDX*/
#define OCTAL_PORT_MODE_STATUS_2_REG_PORT_IDX_SHIFT 10
#define OCTAL_PORT_MODE_STATUS_2_REG_FEC_TERM_MASK 0x000003c0 /*!< FEC_TERM*/
#define OCTAL_PORT_MODE_STATUS_2_REG_FEC_TERM_SHIFT 6
#define OCTAL_PORT_MODE_STATUS_2_REG_LINE_FEC_TYPE_MASK 0x00000038 /*!< LINE_FEC_TYPE*/
#define OCTAL_PORT_MODE_STATUS_2_REG_LINE_FEC_TYPE_SHIFT 3
#define OCTAL_PORT_MODE_STATUS_2_REG_HOST_FEC_TYPE_MASK 0x00000007 /*!< HOST_FEC_TYPE*/
#define OCTAL_PORT_MODE_STATUS_2_REG_HOST_FEC_TYPE_SHIFT 0



/****************************************************************************
 * PORT Mode Conifg3 Registers: OCTAL_PORT_MODE_STATUS_3_REG
 ***************************************************************************/
#define OCTAL_PORT_MODE_STATUS_3_REG_HOST_LANE_MASK_MASK 0x0000FFFF /*!< Host lane mask*/
#define OCTAL_PORT_MODE_STATUS_3_REG_HOST_LANE_MASK_SHIFT 0


/****************************************************************************
 * PORT Mode Conifg4 Registers: OCTAL_PORT_MODE_STATUS_4_REG
 ***************************************************************************/
#define OCTAL_PORT_MODE_STATUS_3_REG_LINE_LANE_MASK_MASK 0x0000FFFF /*!< Line lane mask*/
#define OCTAL_PORT_MODE_STATUS_3_REG_LINE_LANE_MASK_SHIFT 0


/****************************************************************************
 * PORT Mode Status1 Registers: OCTAL_PORT_MODE_CFG_LOG
 ***************************************************************************/
#define OCTAL_PORT_MODE_CFG_LOG_CFG_RESULT_MASK      0xF000
#define OCTAL_PORT_MODE_CFG_LOG_CFG_RESULT_SHIFT     12
#define OCTAL_PORT_MODE_CFG_LOG_CMD_STATUS_MASK      0x1E0
#define OCTAL_PORT_MODE_CFG_LOG_CMD_STATUS_SHIFT     5
#define OCTAL_PORT_MODE_CFG_LOG_TOP_STATUS_MASK      0x1F
#define OCTAL_PORT_MODE_CFG_LOG_TOP_STATUS_SHIFT     0
/****************************************************************************
 * PORT Mode Status1 Registers: OCTAL_PORT_MODE_STATUS_1_REG
 ***************************************************************************/
typedef union octal_port_mode_status1_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t port_vld:1;   /*!<0 - PORT config1/2 not valid; 1 - PORT config1/2 valid */
        uint16_t rsvd:4; 
        uint16_t port_config_status:3; /*!< 0- not configured, 1- config in progress, 2- config done, 3-Config failed */
        uint16_t port_mode_enum:8; /*!<HW/FW port config modes */
            
#else /* LITTLE ENDIAN */
        uint16_t port_mode_enum:8; /*!<HW/FW port config modes */
        uint16_t port_config_status:3; /*!< 0- not configured, 1- config in progress, 2- config done, 3-Config failed */
         uint16_t rsvd:4; 
         uint16_t port_vld:1;    /*!<0 - PORT config1/2 not valid; 1 - PORT config1/2 valid */
           
#endif
    } fields;
} octal_port_mode_status1_reg_t;
    


/****************************************************************************
 * PORT Mode Status2 Registers: OCTAL_PORT_MODE_STATUS_2_REG
 ***************************************************************************/
typedef union octal_port_mode_status2_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t rsvd:1;
        uint16_t line_symbol_mux:1;
        uint16_t power_down_status:1;
        uint16_t port_index:3;
        uint16_t fec_term:4; /*!<0-NO_FEC; 1--FEC_DEC_FWD; 2 -- FEC_DEC_ENC; 3--PCS_XENC; 4--FEC_DEC_XDEC_XENC_ENC*/
        uint16_t line_fec_type:3; /*!<line FEC type (0-NO_FEC;  1-RS528; 2-RS544; 3-INNER) */
        uint16_t host_fec_type:3; /*!<Host FEC Type (0-NO_FEC; 1-RS528; 2-RS544; 3-PCS ) */
#else /* LITTLE ENDIAN */
        uint16_t host_fec_type:3; /*!<Host FEC Type (0-NO_FEC; 1-RS528; 2-RS544; 3-PCS ) */
        uint16_t line_fec_type:3; /*!<line FEC type (0-NO_FEC;  1-RS528; 2-RS544; 3-INNER) */
        uint16_t fec_term :4; /*!<0-NO_FEC; 1--FEC_DEC_FWD; 2 -- FEC_DEC_ENC; 3--PCS_XENC; 4--FEC_DEC_XDEC_XENC_ENC*/
        uint16_t port_index:3;
        uint16_t power_down_status:1;
        uint16_t line_symbol_mux:1;
        uint16_t rsvd:1; 
#endif
    } fields;
} octal_port_mode_status2_reg_t;

/****************************************************************************
 * PORT Mode Status3 Registers: OCTAL_PORT_MODE_STATUS_3_REG
 ***************************************************************************/
typedef union octal_port_mode_status3_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t host_lane_mask:16; /*!<host lane mask */
#else /* LITTLE ENDIAN */
        uint16_t host_lane_mask:16; /*!<host lane mask */
#endif
    } fields;
} octal_port_mode_status3_reg_t;

/****************************************************************************
 * PORT Mode Status4 Registers: OCTAL_PORT_MODE_STATUS_4_REG
 ***************************************************************************/
typedef union octal_port_mode_status4_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t rsvd:8;
        uint16_t line_lane_mask:8;
#else /* LITTLE ENDIAN */
        uint16_t line_lane_mask:8;
        uint16_t rsvd:8;
#endif
    } fields;
} octal_port_mode_status4_reg_t;


typedef struct config_mode_st_s {
    octal_port_mode_status1_reg_t pmode_st1;
    octal_port_mode_status2_reg_t pmode_st2;
    octal_port_mode_status3_reg_t pmode_st3;
    octal_port_mode_status4_reg_t pmode_st4;
}config_mode_st_t;

/****************************************************************************
 * blackhawk_estoque_com_amba :: CHIP_TOP_CHIP_FIRMWARE_INTERNAL1_REG 
 ***************************************************************************/
typedef union chip_top_chip_firmware_interal1_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t fw_sub_ver_num : 4;
        uint16_t tdb : 8; /*!<TDB */
        uint16_t dw_pending : 1; /*!<dw_pending */
        uint16_t bh_pending : 1; /*!<bh_pending */
        uint16_t lw_pending : 1; /*!<lw_pending */
        uint16_t single_or_dual : 1; /*!<single or dual m0 */
#else /* LITTLE ENDIAN */
        uint16_t single_or_dual : 1; /*!<single or dual m0 */
        uint16_t lw_pending : 1; /*!<lw_pending */
        uint16_t bh_pending : 1; /*!<bh_pending */
        uint16_t dw_pending : 1; /*!<dw_pending */
        uint16_t tdb : 8; /*!<TDB */
        uint16_t fw_sub_ver_num : 4;
#endif
    } fields;
} chip_top_chip_firmware_interal1_reg_t;

typedef union ocw_default_mode_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t tdb : 1; /*!<TDB */
        uint16_t port_mask : 8; /*!< 0 or 0xF -ALL ports, 0x1- port starting on lw0, 0x2- on lw1, 0x4- lw2, 8-on lw3 */
        uint16_t no_surge:1;
        uint16_t avs_enable : 1; /*! enable AVS or not */
        uint16_t mode_config : 3; /*!<config default mode (  1 : CW_DEFAULT_MODE_1_REPEATER 2: CW_DEFAULT_MODE_2_200G_XDEC_XENC_RTMR 3:CW_DEFAULT_MODE_5_PCS_KP4 4:CW_DEFAULT_MODE_5_KR4_KP4 5:CW_DEFAULT_MODE_4_NRZ_NRZ_REPEATER */
        uint16_t pre_programmed : 1; /*!<no used. use defafult config from pre-programmed config1 and config2 registers. */
        uint16_t skip : 1; /*!<Skip default mode */
#else /* LITTLE ENDIAN */
        uint16_t skip : 1; /*!<Skip default mode */
        uint16_t pre_programmed : 1; /*!<use defafult config from pre-programmed config1 and config2 registers. */
        uint16_t mode_config : 3; /*!<config default mode (  1 : CW_DEFAULT_MODE_1_REPEATER 2: CW_DEFAULT_MODE_2_200G_XDEC_XENC_RTMR 3:CW_DEFAULT_MODE_5_PCS_KP4 4:CW_DEFAULT_MODE_5_KR4_KP4 5:CW_DEFAULT_MODE_4_NRZ_NRZ_REPEATER */
        uint16_t avs_enable : 1; /*! enable AVS or not */
        uint16_t no_surge:1;
        uint16_t port_mask : 8; /*!< 0 or 0xFF -ALL ports, 0x1- port starting on lw0, 0x2- on lw1, 0x4- lw2, 8-on lw3 */
        uint16_t tdb : 1; /*!<TDB */
#endif
    } fields;
} ocw_default_mode_reg_t;

typedef union uc_ready_reg_u {
    uint16_t words;
    struct {
#ifdef CAPI_IS_BIG_ENDIAN
        uint16_t tdb : 12; /*!<TDB */
        uint16_t bh0_3 : 1; /*!<Host side 0 to 3 */
        uint16_t lw2_3 : 1; /*!<Line side 2 to 3 */
        uint16_t lw0_1 : 1; /*!<Line side 0 to 1 */
        uint16_t oc : 1; /*!<Octal Core ready */
#else /* LITTLE ENDIAN */
        uint16_t oc : 1; /*!<Octal Core ready */
        uint16_t lw0_1 : 1; /*!<Line side 0 to 1 */
        uint16_t lw2_3 : 1; /*!<Line side 2 to 3 */
        uint16_t bh0_3 : 1; /*!<Host side 0 to 3 */
        uint16_t tdb : 12; /*!<TDB */
#endif
    } fields;
} uc_ready_reg_t;


/****************************************************************************
 * #define OCTAL_COMMON_TEST_REG   QUAD_CORE_CONFIG_CFGVAL_46_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_REG_ENABLE_ST_CLEAN_MASK          0x00008000      /*!< CFG_ENABLE_ST_CLEAN*/
#define OCTAL_COMMON_TEST_REG_ENABLE_ST_CLEAN_SHIFT         15

#define OCTAL_COMMON_TEST_REG_START_ST_CLEAN_MASK           0x00004000      /*!< START_ENABLE_ST_CLEAN*/
#define OCTAL_COMMON_TEST_REG_START_ST_CLEAN_SHIFT          14


#define OCTAL_COMMON_TEST_REG_CDR_WAIT_TIMER_MASK           0x00000F00      /*!< CDR_WAIT_TIMER*/
#define OCTAL_COMMON_TEST_REG_CDR_WAIT_TIMER_SHIFT          8
#define OCTAL_COMMON_TEST_REG_BER_WAIT_TIMER_MASK           0x00000F0       /*!< BER_WAIT_TIMER*/
#define OCTAL_COMMON_TEST_REG_BER_WAIT_TIMER_SHIFT          4

#define OCTAL_COMMON_TEST_REG_DISABLE_NEG_PPM_TEST_MASK     0x00000004      /*!< DISABLE_NEG_PPM_TEST*/
#define OCTAL_COMMON_TEST_REG_DISABLE_NEG_PPM_TEST_SHIFT    2
#define OCTAL_COMMON_TEST_REG_DISABLE_POS_PPM_TEST_MASK     0x00000002      /*!< DISABLE_POS_PPM_TEST*/
#define OCTAL_COMMON_TEST_REG_DISABLE_POS_PPM_TEST_SHIFT    1

#define OCTAL_COMMON_TEST_REG_ATE_TEST_MASK                 0x00000001      /*!< PORT MODE*/
#define OCTAL_COMMON_TEST_REG_ATE_TEST_SHIFT                0

/****************************************************************************
#define OCTAL_COMMON_TEST_LINE_BER_CNT_CFG             QUAD_CORE_CONFIG_CFGVAL_47_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_LINE_BER_CNT_CFG_LINE_BER_TH_MASK 0x0000FFFF/*!< LINE_BER_TH*/
#define OCTAL_COMMON_TEST_LINE_BER_CNT_CFG_LINE_BER_TH_SHIFT 0

/****************************************************************************
#define OCTAL_COMMON_TEST_HOST_BER_CNT_CFG             QUAD_CORE_CONFIG_CFGVAL_48_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_HOST_BER_CNT_CFG_HOST_BER_TH_MASK 0x0000FFFF/*!< HOST_BER_TH*/
#define OCTAL_COMMON_TEST_HOST_BER_CNT_CFG_HOST_BER_TH_SHIFT 0

/****************************************************************************
 * #define OCTAL_COMMON_TEST_LINE_PPM_VALUE_CFG   QUAD_CORE_CONFIG_CFGVAL_49_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_LINE_PPM_VALUE_CFG_TX_FORCE_PPM_VALUE_MASK 0x000003FF/*!< TX_FORCE_PPM_VALUE*/
#define OCTAL_COMMON_TEST_LINE_PPM_VALUE_CFG_TX_FORCE_PPM_VALUE_SHIFT 0

/****************************************************************************
 * #define OCTAL_COMMON_TEST_HOST_PPM_VALUE_CFG   QUAD_CORE_CONFIG_CFGVAL_50_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_HOST_PPM_VALUE_CFG_TX_FORCE_PPM_VALUE_MASK 0x000003FF/*!< TX_FORCE_PPM_VALUE*/
#define OCTAL_COMMON_TEST_HOST_PPM_VALUE_CFG_TX_FORCE_PPM_VALUE_SHIFT 0

/****************************************************************************
 * #define OCTAL_COMMON_TEST_ATE_ST_REG   QUAD_CORE_CONFIG_CFGVAL_51_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_ATE_ST_REG_ATE_FSM_STATUS_MASK 0x00000FF00/*!< ATE_FSM_STATUS*/
#define OCTAL_COMMON_TEST_ATE_ST_REG_ATE_FSM_STATUS_SHIFT 8
#define OCTAL_COMMON_TEST_ATE_ST_REG_ATE_RESULT_MASK 0x0000000F/*!< ATE_RESULT*/
#define OCTAL_COMMON_TEST_ATE_ST_REG_ATE_RESULT_SHIFT 0


/****************************************************************************
 * #define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST       QUAD_CORE_CONFIG_CFGVAL_52_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT3_LW_ATE_STATUS_MASK 0x0000F000/*!< PORT3_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT3_LW_ATE_STATUS_SHIFT 12
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT2_LW_ATE_STATUS_MASK 0x00000F00/*!< PORT2_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT2_LW_ATE_STATUS_SHIFT 8
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT1_LW_ATE_STATUS_MASK 0x000000F0/*!< PORT1_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT1_LW_ATE_STATUS_SHIFT 4
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT0_LW_ATE_STATUS_MASK 0x0000000F/*!< PORT0_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_POS_ST_PORT0_LW_ATE_STATUS_SHIFT 0


/****************************************************************************
 * #define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST       QUAD_CORE_CONFIG_CFGVAL_53_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT3_BH_ATE_STATUS_MASK 0x0000F000/*!< PORT3_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT3_BH_ATE_STATUS_SHIFT 12
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT2_BH_ATE_STATUS_MASK 0x00000F00/*!< PORT2_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT2_BH_ATE_STATUS_SHIFT 8
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT1_BH_ATE_STATUS_MASK 0x000000F0/*!< PORT1_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT1_BH_ATE_STATUS_SHIFT 4
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT0_BH_ATE_STATUS_MASK 0x0000000F/*!< PORT0_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_POS_ST_PORT0_BH_ATE_STATUS_SHIFT 0

/****************************************************************************
 * #define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST       QUAD_CORE_CONFIG_CFGVAL_54_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT3_LW_ATE_STATUS_MASK 0x0000F000/*!< PORT3_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT3_LW_ATE_STATUS_SHIFT 12
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT2_LW_ATE_STATUS_MASK 0x00000F00/*!< PORT2_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT2_LW_ATE_STATUS_SHIFT 8
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT1_LW_ATE_STATUS_MASK 0x000000F0/*!< PORT1_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT1_LW_ATE_STATUS_SHIFT 4
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT0_LW_ATE_STATUS_MASK 0x0000000F/*!< PORT0_ATE_STATUS*/
#define OCTAL_COMMON_TEST_LINE_PRBS_NEG_ST_PORT0_LW_ATE_STATUS_SHIFT 0

/****************************************************************************
 * #define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST       QUAD_CORE_CONFIG_CFGVAL_55_RDB
 ***************************************************************************/
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT3_BH_ATE_STATUS_MASK 0x0000F000/*!< PORT3_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT3_BH_ATE_STATUS_SHIFT 12
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT2_BH_ATE_STATUS_MASK 0x00000F00/*!< PORT2_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT2_BH_ATE_STATUS_SHIFT 8
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT1_BH_ATE_STATUS_MASK 0x000000F0/*!< PORT1_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT1_BH_ATE_STATUS_SHIFT 4
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT0_BH_ATE_STATUS_MASK 0x0000000F/*!< PORT0_ATE_STATUS*/
#define OCTAL_COMMON_TEST_HOST_PRBS_NEG_ST_PORT0_BH_ATE_STATUS_SHIFT 0



/****************************************************************************
 * #define OCW_EGRESS_FSM_SUSPEND_CFG                  QUAD_CORE_GPCTRL_RDB_GPCTRL_364_RDB
 ***************************************************************************/
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_ST_MASK  0xE000
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_ST_SHIFT  13
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_MASK  0x1000
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_SHIFT  12
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_ST_MASK  0xE00
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_ST_SHIFT  9
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_MASK  0x100
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_SHIFT  8
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_ST_MASK  0xE0
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_ST_SHIFT  5
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_MASK  0x10
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_SHIFT  4
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_ST_MASK  0xE
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_ST_SHIFT  1
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_MASK  0x1
#define OCW_EGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_SHIFT  0

/****************************************************************************
 * #define OCW_INGRESS_FSM_SUSPEND_CFG                  QUAD_CORE_GPCTRL_RDB_GPCTRL_365_RDB
  ***************************************************************************/
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_ST_MASK  0xE000
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_ST_SHIFT  13
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_MASK  0x1000
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT3_SUSPEND_SHIFT  12
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_ST_MASK  0xE00
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_ST_SHIFT  9
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_MASK  0x100
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT2_SUSPEND_SHIFT  8
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_ST_MASK  0xE0
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_ST_SHIFT  5
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_MASK  0x10
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT1_SUSPEND_SHIFT  4
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_ST_MASK  0xE
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_ST_SHIFT  1
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_MASK  0x1
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT0_SUSPEND_SHIFT  0

/****************************************************************************
 * #define OCW_EGRESS_FSM_SUSPEND_CFG                  QUAD_CORE_GPCTRL_RDB_GPCTRL_364_RDB
 ***************************************************************************/
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT7_SUSPEND_ST_MASK  0xE000
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT7_SUSPEND_ST_SHIFT  13
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT7_SUSPEND_MASK  0x1000
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT7_SUSPEND_SHIFT  12
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT6_SUSPEND_ST_MASK  0xE00
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT6_SUSPEND_ST_SHIFT  9
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT6_SUSPEND_MASK  0x100
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT6_SUSPEND_SHIFT  8
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT5_SUSPEND_ST_MASK  0xE0
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT5_SUSPEND_ST_SHIFT  5
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT5_SUSPEND_MASK  0x10
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT5_SUSPEND_SHIFT  4
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT4_SUSPEND_ST_MASK  0xE
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT4_SUSPEND_ST_SHIFT  1
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT4_SUSPEND_MASK  0x1
#define OCW_EGRESS47_FSM_SUSPEND_CFG_PORT4_SUSPEND_SHIFT  0

/****************************************************************************
 * #define OCW_INGRESS_FSM_SUSPEND_CFG                  QUAD_CORE_GPCTRL_RDB_GPCTRL_365_RDB
  ***************************************************************************/
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT7_SUSPEND_ST_MASK  0xE000
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT7_SUSPEND_ST_SHIFT  13
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT7_SUSPEND_MASK  0x1000
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT7_SUSPEND_SHIFT  12
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT6_SUSPEND_ST_MASK  0xE00
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT6_SUSPEND_ST_SHIFT  9
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT6_SUSPEND_MASK  0x100
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT6_SUSPEND_SHIFT  8
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT5_SUSPEND_ST_MASK  0xE0
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT5_SUSPEND_ST_SHIFT  5
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT5_SUSPEND_MASK  0x10
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT5_SUSPEND_SHIFT  4
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT4_SUSPEND_ST_MASK  0xE
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT4_SUSPEND_ST_SHIFT  1
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT4_SUSPEND_MASK  0x1
#define OCW_INGRESS_FSM_SUSPEND_CFG_PORT4_SUSPEND_SHIFT  0


/****************************************************************************
 * #define OCW_PORT0_FSM_FAULT_FLAG      QUAD_CORE_GPCTRL_RDB_GPCTRL_320_RDB
  ***************************************************************************/
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_FIFO_CLSN_MASK  0x8000
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_FIFO_CLSN_SHIFT  15 
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_TXCLK66_FLT_MASK  0x2000
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_TXCLK66_FLT_SHIFT  13
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_TXPI_CONV_FAIL_MASK  0x1000
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_TXPI_CONV_FAIL_SHIFT  12
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_OGBOX_CLSN_MASK  0x800
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_OGBOX_CLSN_SHIFT  11
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_FEC_LOL_MASK  0x400
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_FEC_LOL_SHIFT  10
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_IGBOX_CLSN_MASK  0x200
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_IGBOX_CLSN_SHIFT  9
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_LW_CDR_LOL_MASK  0x100
#define OCW_PORT0_FSM_FAULT_FLAG_IGR_LW_CDR_LOL_SHIFT  8

#define OCW_PORT0_FSM_FAULT_FLAG_EGR_FIFO_CLSN_MASK  0x80
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_FIFO_CLSN_SHIFT  7
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_RXCLK66_FLT_MASK  0x20
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_RXCLK66_FLT_SHIFT  5
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_TXPI_CONV_FAIL_MASK  0x10
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_TXPI_CONV_FAIL_SHIFT  4
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_OGBOX_CLSN_MASK  0x8
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_OGBOX_CLSN_SHIFT  3
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_FEC_LOL_MASK  0x4
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_FEC_LOL_SHIFT  2
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_IGBOX_CLSN_MASK  0x2
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_IGBOX_CLSN_SHIFT  1
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_BH_CDR_LOL_MASK  0x1
#define OCW_PORT0_FSM_FAULT_FLAG_EGR_BH_CDR_LOL_SHIFT  0


/****************************************************************************
 * #define OCTAL_PORT_STATUS_LOG    QUAD_CORE_CONFIG_CFGVAL_34_RDB
  ***************************************************************************/
#define OCTAL_PORT_STATUS_LOG_CW_DEFAULT_MODE_NO_SURGE_MASK  0x8000
#define OCTAL_PORT_STATUS_LOG_CW_DEFAULT_MODE_NO_SURGE_SHIFT  15
#define OCTAL_PORT_STATUS_LOG_CW_DEFAULT_MODE_NO_SURGE_IN_HANDLE_MASK  0x4000
#define OCTAL_PORT_STATUS_LOG_CW_DEFAULT_MODE_NO_SURGE_IN_HANDLE_SHIFT  14

#define OCTAL_PORT_STATUS_LOG_CW_PORT_IGR_MISSION_MASK  0x10
#define OCTAL_PORT_STATUS_LOG_CW_PORT_IGR_MISSION_SHIFT  4
#define OCTAL_PORT_STATUS_LOG_CW_PORT_EGR_MISSION_MASK  0x1
#define OCTAL_PORT_STATUS_LOG_CW_PORT_EGR_MISSION_SHIFT  0


/****************************************************************************
 * #define OCTAL_PORT_MISSION_INFO_REG                    QUAD_CORE_CONFIG_CFGVAL_50_RDB
***************************************************************************/
#define OCTAL_PORT_MISSION_INFO_REG_IGR_MISSION_MASK  0xFF00
#define OCTAL_PORT_MISSION_INFO_REG_IGR_MISSION_SHIFT  8

#define OCTAL_PORT_MISSION_INFO_REG_EGR_MISSION_MASK  0xFF
#define OCTAL_PORT_MISSION_INFO_REG_EGR_MISSION_SHIFT  0


/****************************************************************************
 * #define OCW_CHIP_LPM_FW_REG                       QUAD_CORE_GPCTRL_RDB_GPCTRL_346_RDB
  ***************************************************************************/
#define OCW_CHIP_LPM_FW_REG_CHIP_IN_LPM_MASK  0x1
#define OCW_CHIP_LPM_FW_REG_CHIP_IN_LPM_SHIFT  0

/****************************************************************************
 * #define HOST_LANE_RESET_COUNT_STATUS_GPREG             QUAD_CORE_GPCTRL_GPCTRL_145_RDB
  ***************************************************************************/
#define SERDES_CDR_LANE_RESTART_COUNTER_MASK    0xF
#define SERDES_CDR_LANE_RESTART_COUNTER_SHIFT     4


/****************************************************************************
 * #define OCW_TOP_CDR_LOCK_STATE                         QUAD_CORE_GPCTRL_GPCTRL_320_RDB
 ***************************************************************************/
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE7_CDR_LOCK_MASK       0x8000
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE7_CDR_LOCK_SHIFT      15
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE6_CDR_LOCK_MASK       0x4000
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE6_CDR_LOCK_SHIFT      14
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE5_CDR_LOCK_MASK       0x2000
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE5_CDR_LOCK_SHIFT      13
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE4_CDR_LOCK_MASK       0x1000
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE4_CDR_LOCK_SHIFT      12
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE3_CDR_LOCK_MASK       0x800
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE3_CDR_LOCK_SHIFT      11
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE2_CDR_LOCK_MASK       0x400
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE2_CDR_LOCK_SHIFT      10
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE1_CDR_LOCK_MASK       0x200
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE1_CDR_LOCK_SHIFT      9
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE0_CDR_LOCK_MASK       0x100
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE0_CDR_LOCK_SHIFT      8
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE_CDR_LOCK_MASK        0xFF00
#define OCW_TOP_CDR_LOCK_STATE_HOST_LANE_CDR_LOCK_SHIFT       8

#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE3_CDR_LOCK_MASK       0x8
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE3_CDR_LOCK_SHIFT      3
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE2_CDR_LOCK_MASK       0x4
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE2_CDR_LOCK_SHIFT      2
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE1_CDR_LOCK_MASK       0x2
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE1_CDR_LOCK_SHIFT      1
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE0_CDR_LOCK_MASK       0x1
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE0_CDR_LOCK_SHIFT      0
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE_CDR_LOCK_MASK        0xF
#define OCW_TOP_CDR_LOCK_STATE_LINE_LANE_CDR_LOCK_SHIFT       0

/****************************************************************************
 * #define OCW_400G_RPTR_INDEP_PORT_STATE                 QUAD_CORE_GPCTRL_GPCTRL_321_RDB
 ***************************************************************************/
#define OCW_RPTR_INDEP_PORT_STATE_PORT3_INDEP_ST_MASK    0x8
#define OCW_RPTR_INDEP_PORT_STATE_PORT3_INDEP_ST_SHIFT   3
#define OCW_RPTR_INDEP_PORT_STATE_PORT2_INDEP_ST_MASK    0x4
#define OCW_RPTR_INDEP_PORT_STATE_PORT2_INDEP_ST_SHIFT   2
#define OCW_RPTR_INDEP_PORT_STATE_PORT1_INDEP_ST_MASK    0x2
#define OCW_RPTR_INDEP_PORT_STATE_PORT1_INDEP_ST_SHIFT   1
#define OCW_RPTR_INDEP_PORT_STATE_PORT0_INDEP_ST_MASK    0x1
#define OCW_RPTR_INDEP_PORT_STATE_PORT0_INDEP_ST_SHIFT   0

#define OCW_RPTR_INDEP_PORT_STATE_4PORT_INDEP_ST_MASK    0xF
#define OCW_RPTR_INDEP_PORT_STATE_4PORT_INDEP_ST_SHIFT   0

/****************************************************************************
 * #define HOST_PLL_STATUS_REG                            QUAD_CORE_GPCTRL_GPCTRL_120_RDB
  ***************************************************************************/
#define HOST_PLL_STATUS_REG_HOST_PLL_LOCKED_MASK        0x1
#define HOST_PLL_STATUS_REG_HOST_PLL_LOCKED_SHIFT       0
/****************************************************************************
 * #define HOST_PLL_CONTROL_REG                           QUAD_CORE_GPCTRL_GPCTRL_121_RDB
  ***************************************************************************/
#define HOST_PLL_CONTROL_REG_MEDIA_PLL_RESTART_MASK     0x1
#define HOST_PLL_CONTROL_REG_MEDIA_PLL_RESTART_SHIFT    0


/****************************************************************************
  * #define HOST_MEDIA_PLL_ST_REG                         QUAD_CORE_GPCTRL_GPCTRL_122_RDB
  ***************************************************************************/
#define HOST_MEDIA_PLL_ST_REG_MEDIA_BOOT_FAIL2_MASK     0x8
#define HOST_MEDIA_PLL_ST_REG_MEDIA_BOOT_FAIL2_SHIFT    4
#define HOST_MEDIA_PLL_ST_REG_MEDIA_BOOT_FAIL1_MASK     0x4
#define HOST_MEDIA_PLL_ST_REG_MEDIA_BOOT_FAIL1_SHIFT    2

#define HOST_MEDIA_PLL_ST_REG_MEDIA_CMODE_RDY_ST_MASK     0x2
#define HOST_MEDIA_PLL_ST_REG_MEDIA_CMODE_RDY_ST_SHIFT    1
#define HOST_MEDIA_PLL_ST_REG_HOST_CMODE_RDY_ST_MASK      0x1
#define HOST_MEDIA_PLL_ST_REG_HOST_CMODE_RDY_ST_SHIFT     0
/****************************************************************************
  * #define EXTERNAL_CONFIG_REQ_REG                        QUAD_CORE_GPCTRL_GPCTRL_123_RDB
  ***************************************************************************/
    
#define EXTERNAL_CONFIG_REQ_REG_START_LANE_CFG_MASK      0x1
#define EXTERNAL_CONFIG_REQ_REG_START_LANE_CFG_SHIFT     0

/****************************************************************************
#define FW_STATUS_SET_REG                              QUAD_CORE_GPCTRL_GPCTRL_130_RDB
  ***************************************************************************/
#define FW_STATUS_SET_REG_DUAL_DIE_SYNC_ACTIVE_FLAG_MASK 0x00000001 /*!< DUAL_DIE_SYNC_ACTIVE_FLAG*/
#define FW_STATUS_SET_REG_DUAL_DIE_SYNC_ACTIVE_FLAG_SHIFT 0   

/****************************************************************************
#define COMMON_CONFIG_FW_TIMESTAMP_REG                 QUAD_CORE_GPCTRL_GPCTRL_96_RDB
#define COMMON_CONFIG_DSP_STICKY_LOG                      QUAD_CORE_GPCTRL_GPCTRL_97_RDB
***************************************************************************/
#define COMMON_CONFIG_DSP_STICKY_LOG_PLL_LOL_MASK      0xF
#define COMMON_CONFIG_DSP_STICKY_LOG_PLL_LOL_SHIFT     0

#ifdef __cplusplus
}
#endif


#endif /* CHIP_COMMON_CONFIG_IND_H */
