/**
 *
 * @file     capi_def.h
 * @author   
 * @date     11/11/2018
 * @version  1.0
 *
 * @property  $Copyright: Copyright 2018 Broadcom INC.
 * This program is the proprietary software of Broadcom INC
 * and/or its licensors, and may only be used, duplicated, modified
 * or distributed pursuant to the terms and conditions of a separate,
 * written license agreement executed between you and Broadcom
 * (an "Authorized License").  Except as set forth in an Authorized
 * License, Broadcom grants no license (express or implied), right
 * to use, or waiver of any kind with respect to the Software, and
 * Broadcom expressly reserves all rights in and to the Software
 * and all intellectual property rights therein.  IF YOU HAVE
 * NO AUTHORIZED LICENSE, THEN YOU HAVE NO RIGHT TO USE THIS SOFTWARE
 * IN ANY WAY, AND SHOULD IMMEDIATELY NOTIFY BROADCOM AND DISCONTINUE
 * ALL USE OF THE SOFTWARE.
 *
 * Except as expressly set forth in the Authorized License,
 *
 * 1.     This program, including its structure, sequence and organization,
 * constitutes the valuable trade secrets of Broadcom, and you shall use
 * all reasonable efforts to protect the confidentiality thereof,
 * and to use this information only in connection with your use of
 * Broadcom integrated circuit products.
 *
 * 2.     TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS
 * PROVIDED "AS IS" AND WITH ALL FAULTS AND BROADCOM MAKES NO PROMISES,
 * REPRESENTATIONS OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY,
 * OR OTHERWISE, WITH RESPECT TO THE SOFTWARE.  BROADCOM SPECIFICALLY
 * DISCLAIMS ANY AND ALL IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY,
 * NONINFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF VIRUSES,
 * ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR
 * CORRESPONDENCE TO DESCRIPTION. YOU ASSUME THE ENTIRE RISK ARISING
 * OUT OF USE OR PERFORMANCE OF THE SOFTWARE.
 *
 * 3.     TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 * BROADCOM OR ITS LICENSORS BE LIABLE FOR   CONSEQUENTIAL,
 * INCIDENTAL, SPECIAL, INDIRECT, OR EXEMPLARY DAMAGES WHATSOEVER
 * ARISING OUT OF OR IN ANY WAY RELATING TO YOUR USE OF OR INABILITY
 * TO USE THE SOFTWARE EVEN IF BROADCOM HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES; OR (ii) ANY AMOUNT IN EXCESS OF
 * THE AMOUNT ACTUALLY PAID FOR THE SOFTWARE ITSELF OR USD 1.00,
 * WHICHEVER IS GREATER. THESE LIMITATIONS SHALL APPLY NOTWITHSTANDING
 * ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.$
 *
 * @brief   This file contains CAPI related types, etc.
 *
 * @section
 * 
 */
#ifndef CAPI_DEF_H
#define CAPI_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

#define ENABLE_FEC_PRBS
#define SET_CONFIG        0                 /**< Set the configuration within the firmware     */
#define GET_CONFIG        1                 /**< Get the configuration of the firmware setting */
#define CAPI_VERSION      0xFFFF            /**< CAPI Version                                  */
#define CAPI_MAX_PORTS    8                 /**< Maximum number of ports                       */
#define CORE_READY_TIMER  400               /**< 4sec for bringup stage                        */
#define FEC_FLN_NUM       8

#define PORT_CFG_LANE_RDY_TIMEOUT  200   /*< Port Configuration Ready Timeout */
#define QSFP_I2C_SLAVE_ADDRESS_DEFAULT  0x50   /*< default qsfp i2c slave address */

#define FAST_SPI_PROGRAM_WITH_BLOCK_WRITE 
#define I2C_BLOCK_WRITE_SIZE   0x2000

#define CAPI_NUM_DFE_TAPS      6

#define CAPI_RPC_CMD_FEC_STATISTICS     /**< Enable FEC Statistics Inforation RPC CMD handler        */
#define ENABLE_FEC_ST
#define ENABLE_CMIS_INTF
#define KP4PRBS_ERR_INJECT

/*#define ENABLE_CW_PRBS*/

/**
 * Same as phy_info_t
 */
typedef phy_info_t capi_phy_info_t;

/**
 * The enumeration for Phy Side selection
 */
typedef enum capi_phy_side_e {
    PHY_HOST_SIDE,                       /**< Phy Host Side                      */
    PHY_MEDIA_SIDE,                      /**< Phy Media Side                     */
    PHY_BOTH_SIDES                       /**< Phy Both Side                      */
} capi_phy_side_t;                       /**< Phy Side type                      */

/**
 * The enumeration for enable/disable
 */
typedef enum capi_enable_e {
    CAPI_DISABLE,                        /*!< operation disable      */
    CAPI_ENABLE                          /*!< operation enable       */
} capi_enable_t;                         /*!< Enable/Disable type    */

/**
 * The enumeration of switch types
 */
typedef enum capi_switch_e {
    CAPI_SWITCH_OFF,                     /**< Switch off             */
    CAPI_SWITCH_ON,                      /**< Switch on              */
    CAPI_SWITCH_TOGGLE                   /**< Switch toggle off/on   */
} capi_switch_t;                         /**< Switch type            */

/**
 * The enumeration of voltage types
 */
typedef enum capi_voltage_e {
    CAPI_ZERO,                           /**< 0 Voltage              */
    CAPI_ZERO_P_SEVEN_FIVE_MLV,          /**< 0.75 mlV               */
    CAPI_ZERO_P_NINE_MLV,                /**< 0.9 mlV                */
    CAPI_ONE_P_TWO_MLV,                  /**< 1.2 mlV                */
    CAPI_DEFAULT                         /**< Default Voltage        */
} capi_voltage_t;                        /**< Voltage types          */

/**
 * The structure for Tx Driver
 */
typedef struct tx_driver_s {
    uint8_t voltage;                     /**< Direction: refer to capi_voltage_t    */
} tx_driver_t;                           /**< Tx Driver type                        */

/**
 * The enumeration for polarity action (default/invert default/swap current)
 */
typedef enum capi_polarity_action_e {
    POLARITY_DEFAULT_SETTING,            /**< Default Polarity       */
    POLARITY_INVERT_DEFAULT_SETTING,     /**< Inver Default Polarity */
    POLARITY_SWAP_CURRENT_SETTING        /**< Swap Current Polarity  */
} capi_polarity_action_t;                /**< Polarity catio type    */

/**
 * The enumeration for data direction (ingress/egress/duplex)
 */
typedef enum capi_direction_e {
    DIR_INGRESS,                         /**< Ingress: Switch <-  HOST <-  CW <-  MEDIA <-  Fiber/Optical */
    DIR_EGRESS,                          /**< Egress : Switch  -> HOST  -> CW  -> MEDIA  -> Fiber/Optical */
    DIR_BOTH                             /**< Both   : Switch <-> HOST <-> CW <-> MEDIA <-> Fiber/Optical */
} capi_direction_t;                      /**< Direction type                                               */

/**
 * The structure for polarity info
 */
typedef struct capi_polarity_info_s {
    uint8_t direction;                   /**< Direction: refer to capi_direction_t                         */
    uint8_t action;                      /**< Polarity setting: refer to capi_polarity_action_t            */
} capi_polarity_info_t;                  /**< Polarity Info type                                           */

/**
 * The enumeration of line lock status
 */
typedef enum line_lock_e {
    CAPI_LINE_UNLOCKED,                  /**< Line Un-locked     */
    CAPI_LINE_LOCKED                     /**< Line Locked        */
} capi_line_lock_t;                      /**< Line Locked type   */

/**
 * The enumeration of CDR lock
 */
typedef enum cdr_lock_e {
    CAPI_CDR_UNLOCKED,                    /**< CDR Locked        */
    CAPI_CDR_LOCKED                       /**< CDR Unlocked      */
} capi_cdr_lock_t;                        /**< CDR Locked type   */

/**
 * The enumeration of lose of signal
 */
typedef enum lose_of_signal_e {
    CAPI_NO_LOSE_OF_SIGNAL,               /**< No Lose of Signal   */
    CAPI_LOSE_OF_SIGNAL                   /**< Lose of Signal      */
} capi_los_t;                             /**< Lose of Signal type */

/**
 * The enumeration of pll lock
 */
typedef enum pll_lock_e {
    CAPI_PLL_OUT_OF_LOCK,                  /**< PLL out of lock    */
    CAPI_PLL_LOCKED                        /**< PLL Locked         */
} capi_pll_lock_t;                         /**< PLL Lock type      */

/**
 * The enum definition of line side link training type
 */
typedef enum {
    LNKTRN_CL72,                            /*!< 0 - CL72   */
    LNKTRN_CL93,                            /*!< 1 - CL93   */
    LNKTRN_802_3CD,                         /*!< 2 - 802.3CD*/
    LNKTRN_TYPE_MAX                         /**< Link Training Type max element */
} capi_lnktrn_type_t;                       /**< Link Training type */

/**
 * The enum definition of line side link training frame size
 */
typedef enum {
    LNKTRN_FRM_SIZE_4K,                     /*!< 0 - frame size 4K */
    LNKTRN_FRM_SIZE_16K,                    /*!<1 - frame size 16K */
    LNKTRN_FRM_SIZE_MAX                     /**< Link Training Frame Size max element */
} capi_lnktrn_frm_size_t;                   /**< Link Training Frame Size type */

/**
 * The enum definition of line side link training initial condition for 802.3cd
 */
typedef enum {
    LNKTRN_INIT_COND_NO,                     /*!< 0 - No link training init condition   */
    LNKTRN_INIT_COND_NO_EQ,                  /*!< 1 - Link training init condition Preset1 :no EQ    */
    LNKTRN_INIT_COND_MAX                     /**< Link Training Init Cond max element */
} capi_lnktrn_init_cond_t;                   /**< Link Training Init Cond type */

/**
 * The enum definition of line side link training initial condition for CL72/CL93 NRZ mode
 */
typedef enum {
    LNKTRN_CL_INIT_NORMAL,                   /*!< 0 - No link training init condition   */
    LNKTRN_CL_INIT_EN,                       /*!< 1 - Link training init condition enable  */
    LNKTRN_CL_INIT_MAX                       /**< Link Training Clause Init type */
} capi_lnktrn_cl72_cl93_init_t;

/**
 * The enum definition of line side link training preset for CL72/CL93 NRZ mode
 */
typedef enum {
    LNKTRN_CL_PRESET_NORMAL,                 /*!< 0 - No link training PRESET, normal operation  */
    LNKTRN_CL_PRESET_EN,                     /*!< 1 - Link training PRESET enable: Preset coeffiecients    */
    LNKTRN_CL_PRESET_MAX                     /**< Link Training Clause preset max element */
} capi_lnktrn_cl72_cl93_preset_t;            /**< Link Training Clause Preset type */

/**
 * The structure of line side link training cfg for 802.3cd
 */
typedef struct capi_lnktrn_802_3cd_cfg_s {
    capi_lnktrn_frm_size_t lnktrn_frm_size;    /**< Line side link training frame size, this is only valid for 802.3cd rather than CL72 & CL93 */
    capi_lnktrn_init_cond_t lnktrn_init_cond;  /**< Line side link training init condition type for 802.3cd */
} capi_lnktrn_802_3cd_cfg_t;                   /**< Line Side Link Training Config information for 802.3cd type */

/**
 * The structure of line side link training cfg for CL72 and CL93
 */
typedef struct capi_lnktrn_cl72_cl93_cfg_s {
    capi_lnktrn_cl72_cl93_init_t lnktrn_cl_init;       /**< Line side link training init condition type for CL72 & CL93 */
    capi_lnktrn_cl72_cl93_preset_t lnktrn_cl_preset;   /**< Line side link training preset type for CL72 & CL93 */
} capi_lnktrn_cl72_cl93_cfg_t;


/**
 * The enumeration of line side link training timeout
 */
typedef enum capi_link_training_timer_e {
    CAPI_LINK_TRAIN_TIMER_DEFAULT  = 0,                        /**< Link Training Timer default value: firmware default */
    CAPI_LINK_TRAIN_TIMER_500MS    = 1,                        /**< Link Training Timer value: 500ms                    */
    CAPI_LINK_TRAIN_TIMER_1S       = 2,                        /**< Link Training Timer value: 1s                       */
    CAPI_LINK_TRAIN_TIMER_1S_500MS = 3,                        /**< Link Training Timer value: 1.5s                     */
    CAPI_LINK_TRAIN_TIMER_2S       = 4,                        /**< Link Training Timer value: 2s                       */
    CAPI_LINK_TRAIN_TIMER_2S_500MS = 5,                        /**< Link Training Timer value: 2.5s                     */
    CAPI_LINK_TRAIN_TIMER_3S       = 6,                        /**< Link Training Timer value: 3s                       */
    CAPI_LINK_TRAIN_TIMER_3S_500MS = 7,                        /**< Link Training Timer value: 3.5s                     */
    CAPI_LINK_TRAIN_TIMER_4S       = 8,                        /**< Link Training Timer value: 4s                       */
    CAPI_LINK_TRAIN_TIMER_4S_500MS = 9,                        /**< Link Training Timer value: 4.5s                     */
    CAPI_LINK_TRAIN_TIMER_5S       = 10,                       /**< Link Training Timer value: 5s                       */
    CAPI_LINK_TRAIN_TIMER_5S_500MS = 11,                       /**< Link Training Timer value: 5.5s                     */
    CAPI_LINK_TRAIN_TIMER_6S       = 12,                       /**< Link Training Timer value: 6s                       */
    CAPI_LINK_TRAIN_TIMER_6S_500MS = 13,                       /**< Link Training Timer value: 6.5s                     */
    CAPI_LINK_TRAIN_TIMER_7S       = 14,                       /**< Link Training Timer value: 7s                       */
    CAPI_LINK_TRAIN_TIMER_7S_500MS = 15                        /**< Link Training Timer value: 7.5s                     */
 } capi_link_training_timer_t;                                 /**< Link Training Timer type                            */                               /**< Link Training Timer type           */

/**
 * The enumeration of link training error code
 */
typedef enum capi_lnktrn_err_e {
    LNKTRN_ERR_NONE,                                   /*!< 0 no error  */
    LNKTRN_ERR_FRM_LCK,                                /*!< 1 check frame lock timeout   */
    LNKTRN_ERR_SNR,                                    /*!< 2 SNR is lower than threshold */
    LNKTRN_ERR_TIMEOUT,                                /*!< 3 link training timeout */
    LNKTRN_ERR_MAX                                     /*!< Max Link training error element */
} capi_lnktrn_err_t;                                   /**< Link Training Error type */

/**
 * The enumeration of linked training done
 */
typedef enum linktrn_done_s {
    CAPI_LINKED_TRAINING_NOT_COMPLETED,             /**< Linked Training Not Completed */
    CAPI_LINKED_TRAINING_SUCCESSFULLU_COMPLETED     /**< Linked Training Successfully Completed */
} capi_linktrn_done_t;                              /**< Linked Training Done type*/

/**
 * The structure of line side link training status
 */
typedef struct capi_lnktrn_status_s {
    uint8_t lnktrn_en;                       /**< link training enable/disable, refer to capi_enable_t */
    uint8_t lnktrn_frm_lck;                  /**< link training frame lock */
    uint8_t lnktrn_recv_rdy;                 /**< link training receive ready */
    uint8_t lnktrn_done;                     /**< link training link training done */
    uint8_t lnktrn_err;                      /**< link training link training error code, refer to capi_lnktrn_err_t */
} capi_lnktrn_status_t;                      /**< Link Training Status type */

/**
 * The structure of line side link training status
 */
typedef struct link_training_status_s {
    uint8_t enabled;                  /**< 1:Link training enabled;                 0:Link training disabled        */
    uint8_t signal_detect;            /**< 1:Link training FSM in SEND_DATA state;  0:Link in training state        */
    uint8_t failure_detected;         /**< 1:Training failure detected;             0:Training failure not detected */
    uint8_t training_status;          /**< 1:Training protocol in progress;         0:Training protocol complete    */
    uint8_t receiver_status;          /**< 1:Receiver trained and ready to receive; 0:Receiver training             */
    uint8_t receiver_lock;            /**< 1:Receiver locked;                       0:Receiver not lock             */
    uint8_t frame_lock;               /**< 1:Frame lcoked;                          0:Frame not lock                */
} link_training_status_t;             /**< Link Training Status type                                                */

/**
 * The enumeration of lose of signal
 */
typedef enum adc_sig_clipping_e {
    CAPI_NO_ADC_SIGNAL_CLIPPING_DETECTED,   /**< No ADC signal clipping detected */
    CAPI_ADC_SIGNAL_CLIPPING_DETECTED       /**< ADC signal clipping detected */
} capi_adc_sig_clipping_t;                  /**< ADC Signal Clipping Detection type*/

/**
 * The enumeration of PLL unlock sticky
 */
typedef enum client_pll_unlock_sticky_e {
    CAPI_PLL_UNLOCK_STICKY_NOT_LOST_LOCK,   /**< Not lost lock after first lock up  */
    CAPI_PLL_UNLOCK_STICKY_LOST_LOCK        /**< Lost lock after first lock up  */
} capi_client_pll_unlock_sticky_t;          /**< client pll unlock sticky type*/

/**
 * The enumeration of client Bh2bh fifo status
 */
typedef enum bh2bh_fifo_status_e {
    CAPI_BH2BH_FIFO_STATUS_NO_COLLISION,    /**< No collision */
    CAPI_BH2BH_FIFO_STATUS_IN_COLLISION     /**< In collision */
} capi_client_bh2bh_fifo_status_t;          /**< bh2bh Fifo Status type*/

/**
 * The enumeration of bh txfir tap setting
 */
typedef enum capi_bh_txfir_tap_enable_e {
    CAPI_NRZ_LP_3TAP,                                /**<BH TX fir nrz 3 tap                                        */
    CAPI_NRZ_6TAP,                                   /**<BH TX fir nrz 6 tap                                        */
    CAPI_PAM4_LP_3TAP,                               /**<BH TX fir pam4 3 tap                                       */
    CAPI_PAM4_6TAP                                   /**<BH TX fir pam4 6 tap                                       */
} capi_bh_txfir_tap_enable_t;                        /**<BH TX fir tap enable type                                  */

/**
 * The enumeration of lw tx precode setting
 */
typedef enum capi_lw_tx_precode_indep_e {
    CAPI_LW_INDEP_TX_PRECODE_DEFAULT = 0,            /**<LW TX precode default                                      */
    CAPI_LW_INDEP_TX_PRECODE_OFF     = 1,            /**<LW TX precode off                                          */
    CAPI_LW_INDEP_TX_PRECODE_ON      = 2             /**<LW TX precode on                                           */
} capi_lw_tx_precode_indep_t;                        /**<LW TX precode type                                         */

/**
 * The structure of lane tx info
 */
typedef struct lane_tx_info_s {
    union {
        uint32_t content;
        struct {
#ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved                     : 25;               /**< reserved                                   */
            uint32_t txfir                        : 1;                /**< txfir                                      */
            uint32_t symbol_swap                  : 1;                /**< symbol_swap                                */
            uint32_t dsp_level_shift              : 1;                /**< dsp TxFIR level shift                      */
            uint32_t dsp_graycode                 : 1;                /**< dsp graycode                               */
            uint32_t dsp_high_swing               : 1;                /**< dsp high_swing                             */
            uint32_t dsp_skewp                    : 1;                /**< dsp skewp                                  */
            uint32_t dsp_precode                  : 1;                /**< dsp precode                                */
#else /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t dsp_precode                  : 1;                /**< dsp precode                                */
            uint32_t dsp_skewp                    : 1;                /**< dsp skewp                                  */
            uint32_t dsp_high_swing               : 1;                /**< dsp high_swing                             */
            uint32_t dsp_graycode                 : 1;                /**< dsp graycode                               */
            uint32_t dsp_level_shift              : 1;                /**< dsp TxFIR level shift                      */
            uint32_t symbol_swap                  : 1;                /**< symbol_swap                                */
            uint32_t txfir                        : 1;                /**< txfir                                      */
            uint32_t reserved                     : 25;               /**< reserved                                   */
#endif
        } is;
    } param;

    struct {
        /**< The following parameters are commonly used for SERDES and DSP */
        txfir_info_t  txfir;                                         /**<  TXFIR                                              */
        uint8_t       symbol_swap;                                   /**<  TX_info symbol_swap: 0- No swap 1- Swap            */

        /**< The following parameters are only used for DSP */
        int8_t        dsp_level_shift[4];                            /**<DSP : TxFIR level shift                              */
        uint8_t       dsp_graycode;                                  /**<DSP : gray code enable: 0- Disable; 1- Enable        */
        uint8_t       dsp_high_swing;                                /**<DSP : high swing enable: 0- Disable; 1- Enable       */
        uint8_t       dsp_skewp;                                     /**<DSP : skew P value, 3 bits value, [0, 7]             */
        uint8_t       dsp_precode;                                   /**<DSP : precode: refer to capi_lw_tx_precode_indep_t   */
    } value;
} lane_tx_info_t;                                                    /**<Lane Tx Info type                                    */

/**
 * The enumeration of media type
 */
typedef enum capi_media_type_e {
    CAPI_MEDIA_TYPE_OPTICAL,                             /**< Optical Media                                                  */
    CAPI_MEDIA_TYPE_PCB_TRACE_BACK_PLANE,                /**< PCB Trace back plane Media                                     */
    CAPI_MEDIA_TYPE_COPPER_CABLE                         /**< Copper Cable Media                                             */
} capi_media_type_t;                                     /**< Media type                                                     */

/**
 * The structure of lane rx info
 */
typedef struct lane_rx_info_s {
    union {
        uint32_t content;
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved                        : 4; /**< reserved                                                      */
            uint32_t vga                             : 1; /**< vga                                                           */
            uint32_t symbol_swap                     : 1; /**< symbol swap                                                   */
            uint32_t media_type                      : 1; /**< media type    : refer to capi_media_type_t                    */
            uint32_t peaking_filter_info             : 1; /**< peaking_filter_info (Serdes: just get(), DSP: get()/set())    */
            uint32_t dfe_info                        : 1; /**< DFE Information                                               */
            uint32_t serdes_lp_has_prec_en           : 1; /**< serdes : lp has prec en                                       */
            uint32_t serdes_unreliable_los           : 1; /**< serdes : unreliable los   : Set() Not Allowed                 */
            uint32_t serdes_scrambling_off           : 1; /**< serdes : scrambling off   : Set() Not Allowed                 */
            uint32_t serdes_db_loss                  : 1; /**< serdes : db loss                                              */
            uint32_t serdes_pam_er_nr                : 1; /**< serdes : pam_er_nr                                            */
            uint32_t dsp_graycode                    : 1; /**< dsp : graycode                                                */
            uint32_t dsp_dc_wander_mu                : 1; /**< dsp : dc_wander_mu                                            */
            uint32_t dsp_gain_boost                  : 1; /**< dsp : gain_boost                                              */
            uint32_t dsp_nldet_en                    : 1; /**< dsp : nldet_en                                                */
            uint32_t dsp_low_bw_en                   : 1; /**< dsp : low_bw_en                                               */
            uint32_t dsp_gain2_on                    : 1; /**< dsp : gain2_on                                                */
            uint32_t dsp_lms_mode                    : 1; /**< dsp : lms mode                                                */
            uint32_t dsp_autopeaking_en              : 1; /**< dsp : autopeaking_en                                          */
            uint32_t dsp_exslicer                    : 1; /**< dsp : exslicer                                                */
            uint32_t dsp_los_th_idx                  : 1; /**< dsp : los_th_idx                                              */
            uint32_t dsp_inlos_th_idx                : 1; /**< dsp : inlos_th_idx                                            */
            uint32_t dsp_outlos_th_idx               : 1; /**< dsp : outlos_th_idx                                           */
            uint32_t dsp_elos_ignore                 : 1; /**< dsp : elos_ignore                                             */
            uint32_t dsp_oplos_ignore                : 1; /**< dsp : oplos_ignore                                            */
            uint32_t dsp_phase_bias_auto_tuning_info : 1; /**< dsp : phase bias auto tuning info                             */
            uint32_t dsp_kp_kf_info                  : 1; /**< dsp : kp_kf_info                                              */
            uint32_t dsp_ffe_info                    : 1; /**< dsp : ffe info                                                */
            uint32_t dsp_los_info                    : 1; /**< dsp : los info                                                */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t dsp_los_info                    : 1; /**< dsp : los info                                                */
            uint32_t dsp_ffe_info                    : 1; /**< dsp : ffe info                                                */
            uint32_t dsp_kp_kf_info                  : 1; /**< dsp : kp_kf_info                                              */
            uint32_t dsp_phase_bias_auto_tuning_info : 1; /**< dsp : phase bias auto tuning info                             */
            uint32_t dsp_oplos_ignore                : 1; /**< dsp : oplos_ignore                                            */
            uint32_t dsp_elos_ignore                 : 1; /**< dsp : elos_ignore                                             */
            uint32_t dsp_outlos_th_idx               : 1; /**< dsp : outlos_th_idx                                           */
            uint32_t dsp_inlos_th_idx                : 1; /**< dsp : inlos_th_idx                                            */
            uint32_t dsp_los_th_idx                  : 1; /**< dsp : los_th_idx                                              */
            uint32_t dsp_exslicer                    : 1; /**< dsp : exslicer                                                */
            uint32_t dsp_autopeaking_en              : 1; /**< dsp : autopeaking_en                                          */
            uint32_t dsp_lms_mode                    : 1; /**< dsp : lms mode                                                */
            uint32_t dsp_gain2_on                    : 1; /**< dsp : gain2_on                                                */
            uint32_t dsp_low_bw_en                   : 1; /**< dsp : low_bw_en                                               */
            uint32_t dsp_nldet_en                    : 1; /**< dsp : nldet_en                                                */
            uint32_t dsp_gain_boost                  : 1; /**< dsp : gain_boost                                              */
            uint32_t dsp_dc_wander_mu                : 1; /**< dsp : dc_wander_mu                                            */
            uint32_t dsp_graycode                    : 1; /**< dsp : graycode                                                */
            uint32_t serdes_pam_er_nr                : 1; /**< serdes : pam_er_nr                                            */
            uint32_t serdes_db_loss                  : 1; /**< serdes : db loss                                              */
            uint32_t serdes_scrambling_off           : 1; /**< serdes : scrambling off   : Set() Not Allowed                 */
            uint32_t serdes_unreliable_los           : 1; /**< serdes : unreliable los   : Set() Not Allowed                 */
            uint32_t serdes_lp_has_prec_en           : 1; /**< serdes : lp has prec en                                       */
            uint32_t dfe_info                        : 1; /**< DFE Information                                               */
            uint32_t peaking_filter_info             : 1; /**< peaking_filter_info (Serdes: just get(), DSP: get()/set())    */
            uint32_t media_type                      : 1; /**< media type     : refer to capi_media_type_t                   */
            uint32_t symbol_swap                     : 1; /**< symbol swap                                                   */
            uint32_t vga                             : 1; /**< vga                                                           */
            uint32_t reserved                        : 4; /**< reserved                                                      */
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } param;

    struct {
        /**< The following parameters are commonly used for SERDES and DSP */
        uint8_t vga;                         /**< VGA : SERDES (Read Only): range[0, 37].   DSP range [0,46]                                           */
        int8_t  symbol_swap;                 /**< symbol_swap:0- Do not swap the MSB and LSB within a symbol ; 1- swap the MSB and LSB within a symbol */
        uint8_t media_type;                  /**< Media Type. 0: optical, 1: PCB trace or backplane, 2: copper                                         */

        struct {
            int8_t value;                    /**< peaking value in Binary coded, Client side is read only, LW can set and get, lw range [0,31] */
            int8_t low_freq;                 /**< read only, Client only                                                                       */
            int8_t high_freq;                /**< read only, Client only                                                                       */
        } peaking_filter_info;               /**< peaking_filter_info                                                                          */

        struct {
            int8_t  dfe[CAPI_NUM_DFE_TAPS];  /**< DFE taps : SERDES (Read Only)                                                                   */
            uint8_t num_of_dfe_taps;         /**< Number of elements in DFE array, read only, client side, currently support 4                    */
            uint8_t dfe_on;                  /**< Enable DFE. 0 : disable DFE, 1 : enable DFE                                                     */
            uint8_t low_power_dfe_on;        /**< Enable low power DFE. 0 : DFE operates in normal power mode, 1 : DFE operates in low power mode */
            uint8_t dsp_dfe_adapt_off;       /**< DSP : Disable DFE adaptation. 0 : disable DFE adaptation, 1 : enable DFE adaptation             */
            uint8_t dsp_force_baud_rate_dfe; /**< DSP : Force Baud rate DFE, not used.                                                            */
        } dfe_info;                          /**< DSP : DFE information                                                                           */

        union {
            /**< The following parameters are only used for SERDES */
            struct {
                uint8_t lp_has_prec_en;     /**< SERDES : Link partner TX has precoder enabled (non-LT mode):                                                           */
                                            /**<          0: LP precoder disabled, 1: LP precoder enabled, Client only                                                  */
                uint8_t unreliable_los;     /**< SERDES : Unreliable LOS. 0 : LOS is reliable, 1 : LOS cannot be trusted,                                               */
                                            /**<          for optical modules with missing optical LOS                  (Read Only)                                     */
                uint8_t scrambling_off;     /**< SERDES : Not used. 0: RX input is scrambled, 1: RX input may have repeating data patterns.      (Read Only)            */
                uint8_t pam_er_nr;          /**< SERDES : Rx Normal or Extended Slicer Mode;                                                                            */
                                            /**<          0: default firmware setting; 1: Normal Slicer, 2: Extended Slicer.   Default: 0                               */
                                            /**<          force_es: force Rx to use ES mode.  force_ns: force Rx to use NS mode                                         */
                                            /**<          Force Rx to use ES mode (longer channels). This will be in effect even if link                                */
                                            /**<          training is used. Cannot be set at same time as forced Normal Slicer.                                         */
                                            /**<          During Link Training; force RX to use NS mode (short channel). This will be in effect                         */
                                            /**<          even if link training is used. Cannot be set at same time as forced Extended Slicer                           */
                uint8_t db_loss;            /**< SERDES : Estimate of the channel loss in DB, Valid range 0-35, Client side                                             */
                                            /**<          0 is default and forces microcode to figure out best optimization.                                            */
                                            /**<          VSR Channel 1-10                                                                                              */
                                            /**<          MR Channel  11-20                                                                                             */
                                            /**<          LR Channel  20+                                                                                               */
             } serdes;

            /**< The following parameters are only used for DSP */
            struct {
                int8_t  graycode;                        /**< DSP: gray code enable: 1-enable; 0-disable*/
                int8_t  dc_wander_mu;                    /**< DSP: 3'b000:  mu = 2^-123'b001:  mu = 2^-113'b010:  mu = 2^-103'b011:  mu = 2^-93'b100:  mu = 2^-8 */
                int8_t  gain_boost;                      /**< DSP: Coarse PGA gain boost code to provide addition gain up to 6dB, Binary coded                   */
                int8_t  low_bw_en;                       /**< DSP: 0 - default (regular bandiwdth) 1 - low bandwidth                                             */
                uint8_t gain2_on;                        /**< DSP: Enable GAIN2. 0 : disable GAINS, 1 : enable GAIN2                                             */
                uint8_t lms_mode;                        /**< DSP: LMS mode. 0: 1 LMS, 1: 8 LMS                                                                  */
                uint8_t autopeaking_en;                  /**< DSP: 0: disable auto peaking algorithm in copper mode link training; 
                                                                   1: enable auto peaking algorithm in copper mode link training  */
                int8_t  exslicer;                        /**< DSP: 0: firmware use default(optical use regular slicer; copper use extended slicer)
                                                                   1: regular slicer
                                                                   2: extended slicer */
                uint8_t los_th_idx;                      /**< DSP: [0, 11] line side LOS threshold mapping index                         */
                uint8_t inlos_th_idx;                    /**< DSP: [0, 11] line side INLOS threshold mapping index                       */
                uint8_t outlos_th_idx;                   /**< DSP: [0, 11] line side OUTLOS threshold mapping index                      */
                uint8_t elos_ignore;                     /**< DSP: [0, 1] line side ignore ELOS no matter electrical LOS or optical LOS  */
                uint8_t oplos_ignore;                    /**< DSP: [0, 1] line side ignore OPLOS no matter electrical LOS or optical LOS */
                struct {
                    int8_t phase_auto_tune_en;           /**< DSP: enable/disable phase auto tuning algorithm [static tuning]                                           */
                    int8_t dynamic_phase_auto_tune_en;   /**< DSP: line side enable dynamic auto phase tuning,                                                          */
                    int8_t max_phase_bias_th;            /**< DSP: line side maximum phase bias threshold, los_th_idx. max_bias_th valid range is [-32~31]              */
                    int8_t min_phase_bias_th;            /**< DSP: line side minimum phase bias threshold, los_th_idx. max_bias_th valid range is [-32~31]              */
                    int8_t phase_tune_step_size;         /**< DSP: static phase tuning step size
                                                                   Note: minimum and maximum threshold should be even number if step size is double step. 
                                                                   0: single step
                                                                   1: double step                                                                                          */
                    int8_t phase_tune_dwell_time;        /**< DSP: phase tuning dwell time each tuning step
                                                                   0: 10 ms, 1: 20ms, 2: 30 ms, 3: 40ms, 4: 50ms                                                        */

 
                    int8_t phase_dtune_bias_range;       /**< DSP: phase bias range of dynamic tuning. default is 4, [0, 10]                                            */
                    int8_t phase_dtune_snr_change_th;    /**< DSP: SNR change threshold parameter for dynamic tuning. default is 0 (0.1db), [0, 10]. (0.1 to 1.1db)     */
                    int8_t phase_tune_link_down_snr;     /**< DSP: phase static tuning link down SNR threshold, 0: 12db, 1: 13db, 2: 14db (default) ... 7: 19db         */
                                                                   
                } phase_bias_auto_tuning_info;           /**< DSP: phase auto tuning config                                                                             */

                struct {
                    int8_t kp_tracking;                  /**< DSP: kp parameters applicable after link up            */
                    int8_t kf_tracking;                  /**< DSP: kf parameters applicable after link up            */
                    int8_t kp;                           /**< DSP: kp parameters applicable during link              */
                    int8_t kf;                           /**< DSP: kf parameters applicable during link              */
                    int8_t kp_hlf_stp;                   /**< DSP: kp half step parameters applicable during link    */
                    int8_t kp_tracking_hlf_stp;          /**< DSP: kp half step parameters applicable after link up  */
                } kp_kf_info;                            /**< DSP: kp kg parameters, LW only                         */

                struct {
                    int8_t power_mode;                   /**< DSP: power mode: 
                                                                   0: firmware default setting; 
                                                                   1: high power; 
                                                                   2: medium power
                                                                   3: low power                                     */
                    int8_t ffe_slicer_multiplier;        /**< DSP: ffe_slicer_multiplier:0: *1; 1 : *2; 2: *1.25    */
                } ffe_info;                              /**< DSP: FFE config                                       */

                struct {
                    int8_t los_th_idx;                   /**< DSP: line side LOS threshold mapping index                */
                    int8_t los_bypass;                   /**< DSP: 0 - LOS bypass disabled 1 - LOS bypass enabled       */
                    int8_t los_bypass_val;               /**< DSP: LOS value if LOS bypass enabled  0 - No LOS, 1 - LOS */
                } los_info;                              /**< DSP: LOS config, LW only                                  */
                struct {
                    uint8_t nldet_en;                   /**< DSP: [0,1] enable/disable Non-Linear Detection in PAM4 mode                                                                                             */
                    int8_t dsel;                        /**< DSP: [0,1] 0 => FIR uses 1st & 2nd samples, 1 => FIR uses 2nd & 3rd samples.                                                                            */
                    int8_t casel;                       /**< DSP: [0~8] 0 => FIR a = 0, 1 => FIR a = 1/16, 2 => FIR a = 2/16 = 1/8, 3 => FIR a = 3/16, 4 => FIR a = 4/16 = 1/4, ... , 8 = 8/16 = 1/2                 */
                    int8_t lms_mu;                      /**< DSP: [0,7] 0 => LMS mu = 2^-11, 1 => mu = 2^-9,  2 => mu = 2^-7,  3 => mu = 2^-5, 4 => Reserved, 5 => mu = 0, 6 => mu = 18'h1FFFF, 7 => mu = 18'h2000   */
                    int8_t mse_mu;                      /**< DSP: [0,2] 0 => MSE mu = 2^-9,  1 => mu = 2^-18, 2 => mu = 2^-12..                                                                                      */
                } nldet_info;                           /**< DSP: NLDET config                                                                                                                                       */
            } dsp;
        } core_ip;
    } value;
} lane_rx_info_t;                                        /**< Lane Rx Info type                                                                                                                                      */

/**
 * The structure of line side link training cfg for 802.3cd
 */
typedef struct lane_lnktrn_802_3cd_cfg_s {
    uint8_t lnktrn_frm_size;                             /**< Line side link training frame size, this is only valid for 802.3cd rather than CL72 & CL93 */
    uint8_t lnktrn_init_cond;                            /**< Line side link training init condition type for 802.3cd */
} lane_lnktrn_802_3cd_cfg_t;                             /**< Line Side Link Training Config information for 802.3cd type */

/**
 * The structure of line side link training cfg for CL72 and CL93
 */
typedef struct lane_lnktrn_cl72_cl93_cfg_s {
    uint8_t lnktrn_cl_init;                              /**< Line side link training init condition type for CL72 & CL93 */
    uint8_t lnktrn_cl_preset;                            /**< Line side link training preset type for CL72 & CL93 */
} lane_lnktrn_cl72_cl93_cfg_t;

/**
 * A structure to represent the link training timer
 */
typedef struct lane_link_training_timer_s {
    uint8_t   disable_lt_timeout;                        /**< CAPI_ENABLE: disable LT timer; CAPI_DISABLE: enable LT timer (in default) */
    uint8_t   lt_timer_duration;                         /**< LT Timer Duration which only is valid when  disable_lt_timeout=CAPI_DISABLE */
} lane_link_training_timer_t;                            /**< Link Training Timer type */

/**
 * A structure to represent the link training information
 */
typedef struct lane_lnktrn_info_s {
    union {
        uint16_t content;
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint16_t reserved                         : 3;  /**< Reservedv                            */
            uint16_t opposite_cdr_first               : 1;  /**< opposite_cdr_first                   */
            uint16_t lnktrn_en                        : 1;  /**< lnktrn_en                            */
            uint16_t serdes_lnktrn_restart_timeout    : 1;  /**< SERDES : lnktrn_restart_timeout      */
            uint16_t serdes_lnktrn_type               : 1;  /**< SERDES : lnktrn_type : Read Only     */
            uint16_t dsp_linktrn_timer                : 1;  /**< DSP : linktrn_timer                  */
            uint16_t dsp_restart                      : 1;  /**< DSP : restart                        */
            uint16_t dsp_disable_lt_rxflt_restart_tx  : 1;  /**< DSP : disable_lt_rxflt_restart_tx    */
            uint16_t dsp_disable_lt_rxlos_restart_tx  : 1;  /**< DSP : disable_lt_rxlos_restart_tx    */
            uint16_t dsp_cl72_auto_pol_en             : 1;  /**< DSP : cl72_auto_pol_en               */
            uint16_t dsp_cl72_rest_to                 : 1;  /**< DSP : cl72_rest_to                   */
            uint16_t dsp_enable_cl72_cl93_preset_req  : 1;  /**< DSP : enable_cl72_cl93_preset_req    */
            uint16_t dsp_enable_802_3cd_preset_req    : 1;  /**< DSP : enable_802_3cd_preset_req      */
            uint16_t dsp_lnktrn_cfg                   : 1;  /**< DSP : lnktrn_cfg                     */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint16_t dsp_lnktrn_cfg                   : 1;  /**< DSP : lnktrn_cfg                     */
            uint16_t dsp_enable_802_3cd_preset_req    : 1;  /**< DSP : enable_802_3cd_preset_req      */
            uint16_t dsp_enable_cl72_cl93_preset_req  : 1;  /**< DSP : enable_cl72_cl93_preset_req    */
            uint16_t dsp_cl72_rest_to                 : 1;  /**< DSP : cl72_rest_to                   */
            uint16_t dsp_cl72_auto_pol_en             : 1;  /**< DSP : cl72_auto_pol_en               */
            uint16_t dsp_disable_lt_rxlos_restart_tx  : 1;  /**< DSP : disable_lt_rxlos_restart_tx    */
            uint16_t dsp_disable_lt_rxflt_restart_tx  : 1;  /**< DSP : disable_lt_rxflt_restart_tx    */
            uint16_t dsp_restart                      : 1;  /**< DSP : restart                        */
            uint16_t dsp_linktrn_timer                : 1;  /**< DSP : linktrn_timer                  */
            uint16_t serdes_lnktrn_type               : 1;  /**< SERDES : lnktrn_type                 */
            uint16_t serdes_lnktrn_restart_timeout    : 1;  /**< SERDES : lnktrn_restart_timeout      */
            uint16_t lnktrn_en                        : 1;  /**< lnktrn_en                            */
            uint16_t opposite_cdr_first               : 1;  /**< opposite_cdr_first                   */
            uint16_t reserved                         : 3;  /**< Reserved                             */
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } param;

    uint8_t lnktrn_en;                                      /**< link training enable/disable */
    uint8_t opposite_cdr_first;                             /**< 0: start link training firstly, then check opposite cdr_lock status
                                                                 1: wait for opposite cdr_lock is ready before link training start   */
    union {
        struct {
            uint8_t lnktrn_restart_timeout;                 /**< SERDES : Restart side link training after timeout        */
            uint8_t lnktrn_type;                            /**< SERDES : link training type, refer to capi_lnktrn_type_t */
        } serdes;

        struct {
            lane_link_training_timer_t linktrn_timer;       /**< DSP : Line side link training timer */
            uint8_t lnktrn_type;                            /**< DSP : link training type */            
            uint8_t linktrn_restart_timeout_en;             /**< DSP : Restart side link training after timeout */            
            uint8_t restart;                                /**< DSP : Line side link restart */
            uint8_t disable_lt_rxflt_restart_tx;            /**< DSP : 0: LT RX fault to restart tx in training procedure; 
                                                                       1: disable LT RX fault to restart tx in training procedure, */
            uint8_t disable_lt_rxlos_restart_tx;            /**< DSP : 0: LT RX los to restart tx in training procedure; 
                                                                       1: disable LT RX los to restart tx in training procedure */
            uint8_t cl72_auto_pol_en;                       /**< DSP : CL72 auto polarity selection (not used). 
                                                                       0: no auto polarity selection
                                                                       1: toggle polarity if no framelock within 1ms during CL72. TODO: remove it */
            uint8_t cl72_rest_to;                           /**< DSP : Replaced by linktrn_restart_timeout_en. TODO: remove it */
            uint8_t enable_cl72_cl93_preset_req;            /**< DSP : 0: disable LT RX send cl72/cl93 preset request (non-IEEE compatible mode); 
                                                                       1: enable LT RX send cl72/cl93 preset request (IEEE compatible mode, default value)  */
            uint8_t enable_802_3cd_preset_req;              /**< DSP : 0: disable LT RX send 802.3cd preset1 request (non-IEEE compatible mode); 
                                                                       1: enable LT RX send 802.3cd preset1 request (IEEE compatible mode, default value) */
            union {
                lane_lnktrn_802_3cd_cfg_t pam_802_3;        /**< DSP : Line side link training cfg for 802.3cd */
                lane_lnktrn_cl72_cl93_cfg_t nrz_cl;         /**< DSP : Line side link training cfg for CL72 & CL93 */
            } lnktrn_cfg;
        } dsp;
    } value;
} lane_lnktrn_info_t;

typedef lane_lnktrn_info_t capi_lnktrn_info_t;


/**
 * The enum for serdes lane CDR mode
 */
typedef enum lane_cdr_mode_e {
    LANE_CDR_MODE_OSCDR_FORCE_ENABLE,          /*!< Force OS-CDR mode */
    LANE_CDR_MODE_OSCDR_FORCE_DISABLE,         /*!< Disable Force OS-CDR mode */
    LANE_CDR_MODE_BRCDR_FORCE_ENABLE,          /*!< Force BR-CDR mode */
    LANE_CDR_MODE_BRCDR_FORCE_DISABLE,         /*!< Disable Force BR-CDR mode */
    LANE_CDR_MODE_CDR_FORCE_DISABLE            /*!< Disable Force CDR modes */
} lane_cdr_mode_t;

typedef uint8_t capi_lane_cdr_mode_t;         /*!< Please refer to lane_cdr_mode_t */

/**
 * Lane config type enumeration
 */
typedef enum lane_config_type_e {
    LANE_CONFIG_TYPE_NONE = 0,                           /**< Lane config type none                      */
    LANE_CONFIG_TYPE_LANE_RX_INFO,                       /**< Lane config type lane rx info              */
    LANE_CONFIG_TYPE_LANE_TX_INFO,                       /**< Lane config type lane tx info              */
    LANE_CONFIG_TYPE_LANE_LINK_TRAINING_INFO,            /**< Lane config type lane link training info   */
} lane_config_type_t;                                    /**< Lane config type                           */

/**
 * The structure of command information
 */
typedef struct capi_lane_config_info_s {
    lane_config_type_t                lane_config_type;    /**< Lane config type                         */
    union {
        lane_rx_info_t                lane_rx_info;        /**< Lane rx info                             */
        lane_tx_info_t                lane_tx_info;        /**< Lane tx info                             */
        lane_lnktrn_info_t            lane_lnktrn_info;    /**< Lane lnktrn info                         */
    } type;
} capi_lane_config_info_t;                                 /**< Lane config Info                         */

/**
 * capi lane control structure definition
 */
typedef struct capi_lane_ctrl_info_s {
    union {
        uint32_t content;                                      /**< The command options                */

        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved                   : 17;          /**< reserved                            */
            uint32_t rx_datapath_power          : 1;           /**< Rx Data Path Power Switch           */
            uint32_t tx_datapath_power          : 1;           /**< Tx Data Path Power Switch           */
            uint32_t rx_lane_cfg_reset          : 1;           /**< Rx Reset Lane configuration         */
            uint32_t tx_lane_cfg_reset          : 1;           /**< Tx Reset Lane configuration         */
            uint32_t rx_squelch                 : 1;           /**< Rx path Squelch Switch              */
            uint32_t tx_squelch                 : 1;           /**< Tx path Squelch Switch              */
            uint32_t rx_data_path               : 1;           /**< Traffic Path: Off:0, On:1           */
            uint32_t tx_data_path               : 1;           /**< Traffic Path: Off:0, On:1           */
            /*uint32_t rx_electric_idle         : 1;*/         /**< Rx Electrical Idle                  */
            uint32_t tx_electric_idle           : 1;           /**< Tx Electrical Idle                  */
            uint32_t rx_suspend_resume          : 1;           /**< Rx Suspend Resume switch            */
            uint32_t tx_suspend_resume          : 1;           /**< Tx Suspend Resume switch            */
            uint32_t rx_afe_power               : 1;           /**< Rx AFE power switch                 */
            uint32_t tx_afe_power               : 1;           /**< Tx AFE power switch                 */
            uint32_t ignore_fault               : 1;           /**< Ignore Fault                        */
            uint32_t serdes_eye_margin_switch   : 1;           /**< serdes : eye_margin_switch                                    */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t serdes_eye_margin_switch   : 1;           /**< serdes : eye_margin_switch                                    */
            uint32_t ignore_fault               : 1;           /**< Ignore Fault                        */
            uint32_t tx_afe_power               : 1;           /**< Tx AFE power switch                 */
            uint32_t rx_afe_power               : 1;           /**< Rx AFE power switch                 */
            uint32_t tx_suspend_resume          : 1;           /**< Tx Suspend Resume switch            */
            uint32_t rx_suspend_resume          : 1;           /**< Rx Suspend Resume switch            */
            uint32_t tx_electric_idle           : 1;           /**< Tx Electrical Idle                  */
            /*uint32_t rx_electric_idle         : 1;*/         /**< Rx Electrical Idle                  */
            uint32_t tx_data_path               : 1;           /**< Traffic Path: Off:0, On:1           */
            uint32_t rx_data_path               : 1;           /**< Traffic Path: Off:0, On:1           */
            uint32_t tx_squelch                 : 1;           /**< Tx path Squelch Switch              */
            uint32_t rx_squelch                 : 1;           /**< Rx path Squelch Switch              */
            uint32_t tx_lane_cfg_reset          : 1;           /**< Tx Reset Lane configuration         */
            uint32_t rx_lane_cfg_reset          : 1;           /**< Rx Reset Lane configuration         */
            uint32_t tx_datapath_power          : 1;           /**< Tx Data Path Power Switch           */
            uint32_t rx_datapath_power          : 1;           /**< Rx Data Path Power Switch           */
            uint32_t reserved                   : 17;          /**< reserved                            */
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;                                                  /**< Parameter is <name>                 */
    } param;                                                   /**< The lane control parameter          */

    union {
        uint32_t content;                                      /**< The command payload                 */

        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved           :13;     /**< Reserved for later use                                      */
            uint32_t rx_datapath_power  : 2;     /**< Rx Power: Off:0, On:1, Toggle:2                             */
            uint32_t tx_datapath_power  : 2;     /**< Tx Power: Off:0, On:1, Toggle:2                             */
            uint32_t rx_lane_cfg_reset  : 1;     /**< Rx Power: 1:reset, 0:no action                              */
            uint32_t tx_lane_cfg_reset  : 1;     /**< Tx Power: 1:reset, 0:no action                              */
            uint32_t rx_squelch         : 1;     /**< Rx Squelch: Off:0, On:1                                     */
            uint32_t tx_squelch         : 1;     /**< Tx Squelch: Off:0, On:1                                     */
            uint32_t rx_data_path       : 1;     /**< Traffic Path: Off:0, On:1                                   */
            uint32_t tx_data_path       : 1;     /**< Traffic Path: Off:0, On:1                                   */
            /*uint32_t rx_electric_idle : 1; */  /**< Rx Electrical Idle: Off:0, On:1                             */
            uint32_t tx_electric_idle   : 1;     /**< Tx Electrical Idle: Off:0, On:1                             */
            uint32_t rx_suspend_resume  : 1;     /**< Rx Suspend: 1 Resume: 0                                     */
            uint32_t tx_suspend_resume  : 1;     /**< Tx Suspend: 1 Resume: 0                                     */
            uint32_t rx_afe_power       : 2;     /**< Rx AFE power: Off:0, On:1, Toggle:2                         */
            uint32_t tx_afe_power       : 2;     /**< Tx AFE power: Off:0, On:1, Toggle:2                         */
            uint32_t ignore_fault       : 1;     /**< Ignore Fault: Off:0, On:1                                   */
            uint32_t eye_margin_switch  : 1;     /**< SERDES : Enable/disable serdes eye margin estimation in PAM */
                                                 /**<          mode. Default: 0 (disabled).  In order to get the  */
                                                 /**<          eye margin in get_dig_lane_status() the application*/
                                                 /**<          client shall turn on the eye margin switch and wait*/
                                                 /**<          1 second before calling capi_get_dig_lane_status() */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t eye_margin_switch  : 1;     /**< SERDES : Enable/disable serdes eye margin estimation in PAM */
                                                 /**<          mode. Default: 0 (disabled).  In order to get the  */
                                                 /**<          eye margin in get_dig_lane_status() the application*/
                                                 /**<          client shall turn on the eye margin switch and wait*/
                                                 /**<          1 second before calling capi_get_dig_lane_status() */
            uint32_t ignore_fault       : 1;     /**< Ignore Fault: Off:0, On:1                                   */
            uint32_t tx_afe_power       : 2;     /**< Tx AFE power: Off:0, On:1, Toggle:2                         */
            uint32_t rx_afe_power       : 2;     /**< Rx AFE power: Off:0, On:1, Toggle:2                         */
            uint32_t tx_suspend_resume  : 1;     /**< Tx Suspend: 1 Resume: 0                                     */
            uint32_t rx_suspend_resume  : 1;     /**< Rx Suspend: 1 Resume: 0                                     */
            uint32_t tx_electric_idle   : 1;     /**< Tx Electrical Idle: Off:0, On:1                             */
            /*uint32_t rx_electric_idle : 1; */  /**< Rx Electrical Idle: Off:0, On:1                             */
            uint32_t tx_data_path       : 1;     /**< Traffic Path: Off:0, On:1                                   */
            uint32_t rx_data_path       : 1;     /**< Traffic Path: Off:0, On:1                                   */
            uint32_t tx_squelch         : 1;     /**< Tx Squelch: Off:0, On:1                                     */
            uint32_t rx_squelch         : 1;     /**< Rx Squelch: Off:0, On:1                                     */
            uint32_t tx_lane_cfg_reset  : 1;     /**< Tx Power: 1:reset, 0:no action                              */
            uint32_t rx_lane_cfg_reset  : 1;     /**< Rx Power: 1:reset, 0:no action                              */
            uint32_t tx_datapath_power  : 2;     /**< Tx Power: Off:0, On:1, Toggle:2                             */
            uint32_t rx_datapath_power  : 2;     /**< Rx Power: Off:0, On:1, Toggle:2                             */
            uint32_t reserved           :13;     /**< Reserved for later use                                      */
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;                                    /**< Parameter is <name>                                         */
    } cmd_value;                                 /**< The lane control command value                              */
} capi_lane_ctrl_info_t;                         /**< Lane Control Information type                               */

/**
 * The structure of pmd info
 */
typedef struct pmd_info_s {
    uint32_t pll_lock;      /**< pll lock      */
    uint32_t cdr_lock;      /**< cdr lock      */
    uint32_t los;           /**< LOS           */
    uint32_t tx_squelch;    /**< tx_squelch    */
} capi_pmd_info_t;          /**< PMD Information type */

/**
 * The enumeration of loopback modes
 */
typedef enum loopback_mode_e {
    CAPI_GLOBAL_LOOPBACK_MODE,                                         /**< Global loopback mode                                     */
    CAPI_GLOBAL_PMD_LOOPBACK_MODE        = CAPI_GLOBAL_LOOPBACK_MODE,  /**< Global PMD loopback mode, line side deep or G-Loop       */
    CAPI_SYSTEM_GLOBAL_LOOPBACK_MODE     = CAPI_GLOBAL_LOOPBACK_MODE,  /**< System global loopback mode                              */
    CAPI_SYSTEM_GLOBAL_PMD_LOOPBACK_MODE = CAPI_GLOBAL_LOOPBACK_MODE,  /**< System global PMD loopback mode, serdes dig or deep loop */

    CAPI_REMOTE_LOOPBACK_MODE,                                         /**< Global loopback mode                                     */
    CAPI_REMOTE_PMD_LOOPBACK_MODE        = CAPI_REMOTE_LOOPBACK_MODE,  /**< Remote PMD loopback mode, line side R-LOOP               */
    CAPI_SYSTEM_REMOTE_PMD_LOOPBACK_MODE = CAPI_REMOTE_LOOPBACK_MODE,  /**< System remote PMD loopback mode, serdes R-LOOP           */

    CAPI_GLOBAL_PCS_LOOPBACK_MODE,          /**< Global PCS loopback mode, No PCS for Centenario */
    CAPI_REMOTE_PCS_LOOPBACK_MODE,          /**< Remote PCS loopback mode, No PCS for Centenario */

    CAPI_SYSTEM_GLOBAL_PCS_LOOPBACK_MODE,   /**< System global PCS loopback mode, No PCS for Centenario */
    CAPI_SYSTEM_REMOTE_PCS_LOOPBACK_MODE,   /**< System remote PCS loopback mode, No PCS for Centenario */
    CAPI_LAST_LOOPBACK_MODE                 /**< Last loopback mode */
} capi_loopback_mode_t;                     /**< Loopback Mode type */

/**
 * The structure of loopback
 */
typedef struct capi_loopback_s {
    uint8_t    mode;                        /**< Loopback mode: refer to capi_loopback_mode_t */
    uint8_t    enable;                      /**< Loopback enable                              */
} capi_loopback_info_t;

/**
 * The enumeration for pattern generator and monitor types
 */
typedef enum capi_pattern_gen_mon_type_e {
    CAPI_PRBS_GENERATOR = 0x01,                   /**< PRBS Generator                          */
    CAPI_PRBS_MONITOR,                            /**< PRBS Monitor                            */
    CAPI_PRBS_GEN_MON,                            /**< PRBS Generator and Monitor              */
    CAPI_PRBS_SSPRQ_GENERATOR,                    /**< PRBS SSPRQ Generator                    */
    CAPI_PRBS_SSPRQ_MONITOR,                      /**< PRBS SSPRQ Monitor                      */
    CAPI_PRBS_SSPRQ_GEN_MON,                      /**< PRBS SSPRQ Generator and Monitor        */
    CAPI_PRBS_Q_PRBS_13_GENERATOR,                /**< PRBS Q PRBS 13 Generator                */
    CAPI_PRBS_Q_PRBS_13_MONITOR,                  /**< PRBS Q PRBS 13 Monitor                  */
    CAPI_PRBS_Q_PRBS_13_GEN_MON,                  /**< PRBS Q PRBS 13 Generator and Monitor    */
    CAPI_PRBS_SQUARE_WAVE_GENERATOR,              /**< PRBS SQUARE Wave Generator              */
    CAPI_PRBS_TX_LINEARITY_GENERATOR,             /**< PRBS Tx Linearity Generator             */
    CAPI_PRBS_SHARED_TX_PATTERN_GENERATOR,        /**< PRBS Shared Tx Generator                */
    CAPI_PRBS_FEC_INGRESS_GENERATOR,              /**< PRBS FEC Ingress Generator              */
    CAPI_PRBS_FEC_INGRESS_MONITOR,                /**< PRBS FEC Ingress Monitor                */
    CAPI_PRBS_FEC_INGRESS_GEN_MON,                /**< PRBS FEC Ingress Generator and Monitor. Is NOT supported in this chip */
    CAPI_PRBS_FEC_EGRESS_GENERATOR,               /**< PRBS FEC Egress Generator               */
    CAPI_PRBS_FEC_EGRESS_MONITOR,                 /**< PRBS FEC Egress Monitor                 */
    CAPI_PRBS_FEC_EGRESS_GEN_MON,                 /**< PRBS FEC Egress Generator and Monitor. Is NOT supported in this chip */
    CAPI_PRBS_KP4_HOST_GENERATOR,                 /**< PRBS KP4 Host Generator                 */
    CAPI_PRBS_KP4_HOST_MONITOR,                   /**< PRBS KP4 Host Monitor                   */
    CAPI_PRBS_KP4_HOST_GEN_MON,                   /**< PRBS KP4 Host Generator and Monitor     */
    CAPI_PRBS_KP4_MEDIA_GENERATOR,                /**< PRBS KP4 Media Generator                */
    CAPI_PRBS_KP4_MEDIA_MONITOR,                  /**< PRBS KP4 Media Monitor                  */
    CAPI_PRBS_KP4_MEDIA_GEN_MON,                  /**< PRBS KP4 Media Generator and Monitor    */
    CAPI_PRBS_JP03B,                            /**< PRBS Shared Tx Pattern     */
    CAPI_PRBS_STAIRCASE_PATTERN,                 /**< PRBS Shared Tx Pattern     */
    CAPI_PRBS_CW_GENERATOR,                       /**< PRBS CW Generator                       */
    CAPI_PRBS_CW_MONITOR,                         /**< PRBS CW Monitor                         */
} capi_pattern_gen_mon_type_t;                    /**< PRBS Pattern Generator Monitor type     */

/**
 * The enumeration of PRBS polynomial type
 */
typedef enum capi_bh_prbs_poly_e {
    CAPI_BH_PRBS_POLY_7 = 0,                    /**< BH PRBS Polynomial 7       */
    CAPI_BH_PRBS_POLY_9 = 1,                    /**< BH PRBS Polynomial 9       */
    CAPI_BH_PRBS_POLY_11 = 2,                   /**< BH PRBS Polynomial 11      */
    CAPI_BH_PRBS_POLY_15 = 3,                   /**< BH PRBS Polynomial 15      */
    CAPI_BH_PRBS_POLY_23 = 4,                   /**< BH PRBS Polynomial 23      */
    CAPI_BH_PRBS_POLY_31 = 5,                   /**< BH PRBS Polynomial 31      */
    CAPI_BH_PRBS_POLY_58 = 6,                   /**< BH PRBS Polynomial 58      */
    CAPI_BH_PRBS_POLY_49 = 7,                   /**< BH PRBS Polynomial 49      */
    CAPI_BH_PRBS_POLY_10 = 8,                   /**< BH PRBS Polynomial 10      */
    CAPI_BH_PRBS_POLY_20 = 9,                   /**< BH PRBS Polynomial 20      */
    CAPI_BH_PRBS_POLY_13 = 10,                  /**< BH PRBS Polynomial 13      */
    CAPI_BH_PRBS_USER_40_BIT_REPEAT = 15,       /**< BH PRBS User 40 BIT Repeat */
    CAPI_BH_PRBS_PRBS_AUTO_DETECT = 20,         /**< BH PRBS Auto Detect        */
    CAPI_BH_PCS_PRBS_7 = 100,                   /**< BH PCS PRBS Polynomial 7   */
    CAPI_BH_PCS_PRBS_15 = 101,                  /**< BH PCS PRBS Polynomial 15  */
    CAPI_BH_PCS_PRBS_23 = 102,                  /**< BH PCS PRBS Polynomial 23  */
    CAPI_BH_PCS_PRBS_31 = 103,                  /**< BH PCS PRBS Polynomial 31  */
    CAPI_BH_PRBS_PRBS_UNKNOWN = 255             /**< BH PRBS Unknown            */
} capi_bh_prbs_poly_t;                          /**< BH PRBS Polynomial type    */

/**
 * The enumeration of PRBS Checker Mode
 */
typedef enum capi_bh_prbs_checker_mode_e {
    CAPI_BH_PRBS_SELF_SYNC_HYSTERESIS = 0,
    CAPI_BH_PRBS_INITIAL_SEED_HYSTERESIS = 1,
    CAPI_BH_PRBS_INITIAL_SEED_NO_HYSTERESIS = 2
} capi_bh_prbs_checker_mode_t;

/**
 * The enumeration of FEC PRBS polynomial type
 */
typedef enum capi_fec_prbs_gen_poly_e {
    CAPI_FEC_PRBS_GEN_POLY_7  = 7,  /**< FEC PRBS GEN polynomial 7 */
    CAPI_FEC_PRBS_GEN_POLY_9  = 9,  /**< FEC PRBS GEN polynomial 9 */
    CAPI_FEC_PRBS_GEN_POLY_10 = 10, /**< FEC PRBS GEN polynomial 10 */
    CAPI_FEC_PRBS_GEN_POLY_11 = 11, /**< FEC PRBS GEN polynomial 11 */
    CAPI_FEC_PRBS_GEN_POLY_13 = 13, /**< FEC PRBS GEN polynomial 13 */
    CAPI_FEC_PRBS_GEN_POLY_15 = 15, /**< FEC PRBS GEN polynomial 15 */
    CAPI_FEC_PRBS_GEN_POLY_20 = 20, /**< FEC PRBS GEN polynomial 20 */
    CAPI_FEC_PRBS_GEN_POLY_23 = 23, /**< FEC PRBS GEN polynomial 23 */
    CAPI_FEC_PRBS_GEN_POLY_31 = 31, /**< FEC PRBS GEN polynomial 31 */
    CAPI_FEC_PRBS_GEN_POLY_49 = 49, /**< FEC PRBS GEN polynomial 49 */
    CAPI_FEC_PRBS_GEN_POLY_58 = 58, /**< FEC PRBS GEN polynomial 58 */
    CAPI_FEC_PRBS_GEN_POLY_COUNT    /**<FEC PRBS GEN polynomial count */
} capi_fec_prbs_gen_poly_t;

/**
 * The enumeration of CW PRBS polynomial type
 */
typedef enum capi_prbs_poly_type_e {
    CAPI_PRBS_POLY_7  = 7,  /**< PRBS polynomial 7 */
    CAPI_PRBS_POLY_9  = 9,  /**< PRBS polynomial 9 */
    CAPI_PRBS_POLY_10 = 10, /**< PRBS polynomial 10 */
    CAPI_PRBS_POLY_11 = 11, /**< PRBS polynomial 11 */
    CAPI_PRBS_POLY_13 = 13, /**< PRBS polynomial 13 */
    CAPI_PRBS_POLY_15 = 15, /**< PRBS polynomial 15 */
    CAPI_PRBS_POLY_20 = 20, /**< PRBS polynomial 20 */
    CAPI_PRBS_POLY_23 = 23, /**< PRBS polynomial 23 */
    CAPI_PRBS_POLY_31 = 31, /**< PRBS polynomial 31 */
    CAPI_PRBS_POLY_49 = 49, /**< PRBS polynomial 49 */
    CAPI_PRBS_POLY_58 = 58, /**< PRBS polynomial 58 */
    CAPI_PRBS_POLY_COUNT    /**< PRBS polynomial count */
} capi_prbs_poly_type_t;

/**
 * The structure of PRBS polynomial types
 */
typedef struct capi_prbs_poly_s {
    uint8_t poly_type;              /**< PRBS polynomial: capi_prbs_poly_t     */
    uint8_t bh_poly;                /**< BH PRBS polynomial: refer capi_bh_prbs_poly_t         */
    uint8_t fec_pgen_poly;          /**< FEC PRBS GEN polynomial: capi_fec_prbs_gen_poly_t     */
} capi_prbs_poly_t;                 /**< PRBS poly type                                        */

/**
 * The Enumeratio for Square Wave Pattern Mode
 */
typedef enum square_wave_pattern_e
{
    ONE_1_CONSECUTIVE  = 0,                 /**<  1 consecutive 1s        */
    ONE_2_CONSECUTIVE  = 1,                 /**<  2 consecutive 1s        */
    ONE_4_CONSECUTIVE  = 2,                 /**<  4 consecutive 1s        */
    ONE_8_CONSECUTIVE  = 3,                 /**<  8 consecutive 1s        */
    ONE_16_CONSECUTIVE = 4,                  /**< 16 consecutive 1s        */
    SQR_WAVE_PATTEN_MAX
} square_wave_pattern_t;                    /**< Square Wave Pattern type */

/**
 * The structure of PRBS configuration
 */
typedef struct capi_prbs_cfg_s {
    capi_prbs_poly_t            poly;               /**< polynomial                                      */
    uint32_t                    bh_checker_mode;    /**< checker mode: refer capi_bh_prbs_checker_mode_t */
    uint32_t                    rx_invert;          /**< RX Invert                                       */
    uint32_t                    tx_invert;          /**< TX Invert                                       */
    uint32_t                    lw_checker_auto_det;/**< checker auto det: refer capi_enable_t           */
} capi_prbs_cfg_t;                                  /**< PRBS configuration type                         */

/**
 * The structure SSPRQ pattern type
 */
typedef struct capi_prbs_ssprq_s {
    uint8_t   modulation;       /**< Modulation Mode;   1:Normal/Full for PAM4 mode  /  0:NRZ mode       */
    uint8_t   gray_code;        /**< Gray Code;         0:Do Gray Code  /  1:Do Not Gray Code            */
    uint8_t   bit_swap;         /**< Bit Swap;          0:Do Bit Swap   /  1:Do Not Bit Swap             */
} capi_prbs_ssprq_t;            /**< PRBS SSPRQ type                                                     */

/**
 * The structure Q PRBS 13 pattern type
 */
typedef struct capi_q_prbs_13_s {
    uint32_t   training_frame;        /**< 0:Generate 31096 bit long training frame  /  1:Don't Generate */
    uint32_t   invert_bits;           /**< 0:Invert PRBS data every 8191 bits  /  1:Do not Invert Bits   */
} capi_q_prbs_13_t;                   /**< Q PRBS 13 type                                                */

/**
 * The structure square wave pattern type
 */
typedef struct capi_prbs_square_wave_s {
    uint32_t   ptrn_wave_type;           /**< Refer to square_wave_pattern_t                             */
} capi_prbs_square_wave_t;               /**< PRBS Square Wave type                                      */

/** The structure shared Tx pattern type
 */
typedef struct capi_prbs_shared_tx_ptrn_s {
    uint8_t  length;                        /**< Pattern length                                             */
    uint8_t  pattern[32];                  /**< Pattern content                                            */
} capi_prbs_shared_tx_ptrn_t;               /**< PRBS Shared Tx Pattern type                                */

/**
 * The structure staircase pattern type
 */
typedef struct capi_prbs_staircase_pat_s {
    uint8_t   amplitude;           /**< amplitude [0,168]                                   */
} capi_prbs_staircase_pat_t;               /**< TX Staircase Pattern GEN type                                      */

/**
 * The structure of PRBS info
 */
typedef struct prbs_info_s {
    uint32_t ptype;                                 /**< PRBS type: refer capi_pattern_gen_mon_type_t                       */
    uint8_t  gen_switch;                            /**< Pattern Generator; 0:OFF  /  1:ON  refer capi_switch_t  */    

    union {
        capi_prbs_cfg_t            pcfg;            /**< PRBS config info, used when ptype is CAPI_PRBS_LINE_WRAPPER or CAPI_PRBS_BLACKHAWK              */
        capi_prbs_ssprq_t          ssprq;           /**< PRBS SSPRQ Pattern type : CAPI_PRBS_SSPRQ                                                       */
        capi_q_prbs_13_t           qprbs13;         /**< Q PRBS 13 Pattern type : CAPI_PRBS_Q_PRBS_13                                                    */
        capi_prbs_square_wave_t    sqr_wave;        /**< PRBS Square Wave Pattern type : CAPI_PRBS_SQUARE_WAVE, not supported by neither client nor line */
        capi_prbs_shared_tx_ptrn_t shared_tx_ptrn;  /**< PRBS Shared Tx Pattern : CAPI_PRBS_SHARED_TX_PATTERN, valid only for client                     */
        capi_prbs_staircase_pat_t  staircase_pat;   /**< Staircase Pattern GEN type :  output a stepping PAM4 pattern, that does this:  0 , 1, 2, 3, 0, 1, 2, 3,....... */
    } op;
} capi_prbs_info_t;                                   /**< PRBS Information type                                   */

/*
 * Enumeration for inject PRBS error type
 */
typedef enum capi_prbs_err_inj_type_s{
    PRBS_ERR_INJ_LSB = 0x1,                          /**< Inject LSB error counter */
    PRBS_ERR_INJ_MSB = 0x2,                          /**< Inject MSB error counter */

} capi_prbs_err_inj_type_t;

/**
 * The structure of PRBS error inject
 */
typedef struct capi_prbs_err_inject_s {
    uint32_t ptype;                                 /**< PRBS type: refer   capi_pattern_gen_mon_type_t     */
    uint32_t enable;                                /**< PRBS enable info: refer capi_enable_t */
    uint32_t inject_err_num;                        /**< PRBS err                              */
    uint32_t err_type;                              /**< PRBS error bitwise type:  bit 0: LSB err; bit 1:MSB err */
} capi_prbs_err_inject_t;                           /**< PRBS error injection type             */

/**
 * The structure of PRBS error
 */
typedef struct prbs_ml_err_s {
    #ifdef CAPI_IS_BIG_ENDIAN
    uint32_t msb_err;
    uint32_t lsb_err;
    #else
    uint32_t lsb_err;
    uint32_t msb_err;
    #endif
} prbs_ml_err_t;

/**
 * The structure of PRBS status
 */
typedef struct prbs_status_s {
    uint32_t prbs_type;         /**< refer to capi_pattern_gen_mon_type_t */
    uint32_t lock;              /**< Whether PRBS is currently locked */
    uint32_t lock_loss;         /**< Whether PRBS was unlocked since last call */

    union {
        uint64_t s_err;
        prbs_ml_err_t ml_err;
    } err;                      /**< PRBS errors count */
} capi_prbs_status_t;           /**< PRBS status type */

/*
 * Enumeration for GPR lane status type
 */
typedef enum capi_status_type_e
{
    GPR_LANE_CDR_LOCK_STATUS,           /**< Lane CDR Lock status type to retrieve              */
    GPR_LANE_LOL_LOS_STATUS,            /**< Lane los and cdr lol status                        */
    GPR_LANE_SIGDET_STATUS,             /**< Lane signal detect status                          */
    GPR_LANE_TX_SQUELCH_STATUS,         /**< Lane Tx Squelch status                             */
    GPR_LANE_CDR_RESTART_COUNTER,       /**< Lane CDR lock restart counter                      */
    GPR_LANE_RX_OUTPUT_STATUS,          /**< Lane Rx Output status type to retrieve             */
    GPR_LANE_RX_OUTPUT_LATCH_STATUS,    /**< Lane Rx Output Latch status type to retrieve       */
} capi_status_type_t;                   /**< Lane CDR Lock status type                          */

/**
 * The structure of GP register based lane status
 */
typedef struct capi_status_s {
    uint32_t status_type;                            /**< lane status type: refer capi_status_type_t             */

    union {
        struct {
            uint16_t    lock_status;                 /**< Lane CDR lock status of host or media lanes            */
        } cdr;
        struct {
            uint16_t    lol_sticky;                  /**< Lane CDR lol status of host or media lanes             */
            uint16_t    los_status;                  /**< Lane los status of host or media lanes                 */
        } los;
        uint16_t        lane_sigdet_status;          /**< Cable signal detect status                             */
        uint16_t        lane_tx_squelch_status;      /**< Lane Tx Squelch status                                 */
        uint16_t        lane_rx_output_status;       /**< Lane Rx Output status of host or media lanes           */
        uint16_t        lane_rx_output_latch_status; /**< Lane Rx Output Latch status of host or media lanes     */
        uint16_t        lane_cdr_restart_counter;    /**< Lane CDR lock restart counter                          */
    } param;
} capi_status_t;                                     /**< GPR lane/other status                                  */

/**
 * Phy Command Id Enumeration
 */
typedef enum phy_command_id_e {
    COMMAND_ID_ALL = 0,                              /**< Command Id; applies to all the modules */
    COMMAND_ID_SET_TXPI_OVERRIDE,                    /**< TxPI Override                          */
    COMMAND_ID_SET_LOW_POWER_MODE,                   /**< Set Low Power Mode : LPM               */
    COMMAND_ID_GET_LOW_POWER_MODE,                   /**< Get Low Power Mode : LPM               */
    COMMAND_ID_SET_POLARITY,                         /**< Configure Polarity                     */
    COMMAND_ID_GET_POLARITY,                         /**< Get Lane Egress Polarity               */
    COMMAND_ID_SET_LANE_CTRL_INFO,                   /**< Set lane control info                  */
    COMMAND_ID_GET_LANE_CTRL_INFO,                   /**< Get lane control info                  */
    COMMAND_ID_SET_CONFIG_INFO,                      /**< Set Configuration Information          */
    COMMAND_ID_GET_CONFIG_INFO,                      /**< Get Configuration Information          */
    COMMAND_ID_SET_LOOPBACK_INFO,                    /**< Set loopback info                      */
    COMMAND_ID_GET_LOOPBACK_INFO,                    /**< Get loopback info                      */
    COMMAND_ID_SET_PRBS_INFO,                        /**< Set PRBS pattern generator info        */
    COMMAND_ID_GET_PRBS_INFO,                        /**< Get PRBS pattern generator info        */
    COMMAND_ID_CLEAR_PRBS_STATUS,                    /**< Clear prbs status                      */
    COMMAND_ID_GET_PRBS_STATUS,                      /**< Get prbs status                        */
    COMMAND_ID_INJ_PRBS_ERROR,                       /**< Inject prbs error                      */
    COMMAND_ID_DIAG_LANE_STATUS,                     /**< Lane Status Diagnostic                 */
    COMMAND_ID_SET_LANE_CONFIG_INFO,                 /**< Set lane config info                   */
    COMMAND_ID_GET_LANE_CONFIG_INFO,                 /**< Get lane config info                   */
    COMMAND_ID_GET_LANE_INFO,                        /**< Get lane info                          */
    COMMAND_ID_SET_ARCHIVE_INFO,                     /**< Set archive info in SRAM               */
    COMMAND_ID_GET_ARCHIVE_INFO,                     /**< Get archive from from SRAM             */
    COMMAND_ID_GET_CMIS_INFO,                        /**< Get CMIS information                   */
    COMMAND_ID_GET_SHIST_INFO,                       /**< Get slicer histogram information       */

    COMMAND_ID_CLIENT_SIDE       = 40,               /**< Client related command Ids             */
    COMMAND_ID_SET_SERDES_LANE_CDR_MODE,             /**< Serdes set lane CDR Mode               */
    COMMAND_ID_GET_SERDES_LANE_CDR_MODE,             /**< Serdes get lane CDR Mode               */


    COMMAND_ID_LW_SIDE = 60,                         /**< Line wrapper related command Ids       */
    COMMAND_ID_SET_DSP_POWER_INFO,                   /**< Set DSP Power Info                     */
    COMMAND_ID_GET_DSP_POWER_INFO,                   /**< Get DSP Power Info                     */
    COMMAND_ID_SET_LW_RCLK_INFO,                     /**< Set LW recovered clock info            */
    COMMAND_ID_SET_LW_DSP_MODE,                      /**< Set LW DSP optical / electrical mode   */
    COMMAND_ID_GET_LW_DSP_MODE,                      /**< Get LW DSP optical / electrical mode   */
    COMMAND_ID_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH,    /**< Set DSP Optical Loss Host Tx Squelch   */
    COMMAND_ID_SET_TC_SE_MODE,                 /**< Set LW Test Chip Single End mode        */
    COMMAND_ID_GET_TC_SE_MODE,                 /**< Get LW Test Chip Single End mode        */

    COMMAND_ID_DIAG_SET_CMIS_SNR_LTP_INFO = 70,      /**< Diagnostic: CMIS SNR LTP Information              */
    COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_CONFIG_INFO,    /**< Diagnostic: CMIS SNR LTP Configuration Information*/
    COMMAND_ID_DIAG_GET_CMIS_SNR_LTP_INFO,           /**< Diagnostic: CMIS SNR LTP Payload Information      */

    COMMAND_ID_DIAG_SET_MEDIA_MPI_CONFIG,            /**< Diagnostic: media side config MPI Information      */
    COMMAND_ID_DIAG_GET_MEDIA_MPI_CONFIG,            /**< Diagnostic: media side get MPI config Information  */
    COMMAND_ID_DIAG_GET_MEDIA_MPI_STATE,             /**< Diagnostic: media side get MPI Information         */
    COMMAND_ID_DIAG_SET_MEDIA_MISSION_MPI_CONFIG,    /**< Diagnostic: media side config MPI Information in steady state     */
    COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_CONFIG,    /**< Diagnostic: media side get MPI config Information in steady state */
    COMMAND_ID_DIAG_GET_MEDIA_MISSION_MPI_STATE,     /**< Diagnostic: media side get MPI data in steady state               */

    COMMAND_ID_DIAG_GET_SERDES_DIAG_INFO,            /**< Diagnostic: serdes get diagnostics info            */

    COMMAND_ID_CW_SIDE = 80,                         /**< Core wrapper related command Ids       */
    COMMAND_ID_SET_CW_RPTR_FEC_MON_CONFIG,           /**< Set FEC Monitor config in repeater mode*/
    COMMAND_ID_SET_RPTR_INDEP_HDL,                  /**< Set 200G/100G one2one repeater mode independent lane handling  */
    COMMAND_ID_GET_RPTR_INDEP_HDL,                  /**< Get 200G/100G one2one repeater mode independent lane handling  */
    COMMAND_ID_SET_LINE_PLL_DDCC_CFG,               /**< Set line side DSP PLL DDCC configuration */
    COMMAND_ID_GET_LINE_PLL_DDCC_CFG,               /**< Get line side DSP PLL DDCC configuration  */
    COMMAND_ID_LAST                                  /**< Last Command Identifier                */
} phy_command_id_t;                                  /**< Phy Command Id type                    */

/**
 * The struct of LPM information
 */
typedef struct capi_lpm_info_s {
    capi_enable_t                   lpm_en;            /*!< input : LPM enable or disable status */
} capi_lpm_info_t;

/**
 * Power Mode Enumeration
 */
typedef enum power_mode_e {
    POWER_MODE_DEFAULT,                                    /**< 0: firmware default setting           */
    POWER_MODE_HIGH_POWER,                                 /**< 1: high power                         */
    POWER_MODE_MEDIUM_POWER,                               /**< 2: medium power                       */
    POWER_MODE_LOW_POWER                                   /**< 3: low power                          */
} power_mode_t;                                            /**< Power Mode type                       */

/**
 * The structure of Power Mode Information 
 */
typedef struct capi_power_mode_info_e {
        power_mode_t                power_mode;             /**< DSP: Power Mode, refer to power_mode_t */
} capi_power_info_t;                                        /**< Command Request type                   */

/**
 * The structure of capi 200g/100g one2one repeater independent lane control information 
 */
typedef struct capi_rptr_indep_lane_ctrl_info_s {
    capi_enable_t  indep_ctrl_en;
} capi_rptr_indep_lane_ctrl_info_t;                                 /**< capi 200g/100g one2one rptr independent lane ctrl Information              */

/**
 * The structure of capi line pll config control information 
 */
typedef struct capi_line_pll_ctrl_info_s {
    capi_enable_t  ddcc_cfg_en;
} capi_line_pll_ctrl_info_t;                                 /**< capi line pll config control information              */

/**
 * The structure of chip command information 
 */
typedef struct capi_chip_command_info_s {
    phy_command_id_t                     command_id;                     /**< Command Identifier                                               */
    union {
        capi_lpm_info_t                  lpm_info;                       /**< Low Power Mode Information                                       */
        capi_power_info_t                dsp_power_info;                 /**< DSP: Power Information                                           */
        capi_rptr_indep_lane_ctrl_info_t indep_ctrl;                     /**< 200g/100g one2one repeater mode independent lane squelch control */
        capi_line_pll_ctrl_info_t        line_pll_ctrl;                  /**< line pll config control information                              */
        uint8_t                          optrxlos_host_fast_tx_squelch;  /**< Fast squelch upon optrxlos detection                             */
                                                                         /**< 0 : Disable; other values not supported                          */
        capi_lane_cdr_mode_t             serdes_lane_cdr_mode;           /**< Serdes lane CDR mode                                             */
        uint8_t                          serdes_lane_tuning_control;     /**< Serdes lane tuning control                                       */
    } type;                                                              /**< Payload/data type of the command Id                              */
} capi_chip_command_info_t;                                              /**< Command Information type                                         */


/**
 * The structure to represent eye margin estimate
 */
typedef struct eye_margin_estimate_s {
    uint16_t left_eye_mui;                          /**< Left eye mUI                            */
    uint16_t right_eye_mui;                         /**< Right eye mUI                           */
    uint16_t upper_eye_mv;                          /**< Upper eye mUI                           */
    uint16_t lower_eye_mv;                          /**< Lower eye mUI                           */
} eye_margin_estimate_t;                            /**< Eye Margin Estimate type                */

/**
 * The structure of diagnostic lane status
 */
typedef struct capi_diag_lane_status_s {
    union {
        uint32_t content;                            /**< The command payload                     */
        struct {
#ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved            : 30;      /**< Reserved for later use                  */
            uint32_t eye_margin_estimate : 1;       /**< Serdes : Eye Margin Estimate            */
            uint32_t dc_offset           : 1;       /**< DC Offset                               */
#else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t dc_offset           : 1;       /**< DC Offset                               */
            uint32_t eye_margin_estimate : 1;       /**< Serdes : Eye Margin Estimate            */
            uint32_t reserved            : 30;      /**< Reserved for later use                  */
#endif /* CAPI_IS_BIG_ENDIAN */
        } is;                                       /**< Parameter is <name>                     */
    } param;

    union {
        eye_margin_estimate_t eye_margin_est;       /**< Eye Margin Est; eye_margin_estimate_t   */
        int8_t                dc_offset;            /**< DC Offset value                         */
        int8_t                payload[8];           /**< Padding                                 */
    } value;
} capi_diag_lane_status_t;                          /**< Client Lane Status Information type     */

/**
 * The structure of firmware reset
 */
typedef struct reset_s {
    uint8_t direction;                           /**< Ingress/Egress/Both:  refer to capi_direction_t  */
    uint8_t on_off;                              /**< Switch On/Off/Toggle: refer to capi_switch_t     */
} reset_t;                                       /**< Reset type                                       */

/**
 * The structure of Low Power Mode
 */
typedef struct lpm_s {
    uint8_t on_off;                               /**< Switch On/Off/Toggle: refer to capi_switch_t */
} lpm_t;                                          /**< Low Power Mode type                          */

/**
 * The structure of TxPI override
 */
typedef struct txpi_override_s {
    uint8_t enable;                               /**< txpi override, refer to capi_enable_t */
    int16_t ppm;                                  /**< PPM value                             */
} txpi_override_t;                                /**< TxPI Override Type                    */

/**
 * The enumeration of Host FEC Type
 */
typedef enum capi_host_fec_type_e {
    CAPI_HOST_FEC_TYPE_NA = 0,          /**< No FEC Mode */
    CAPI_HOST_FEC_TYPE_RS528 = 1,       /**< RS528 KR4*/
    CAPI_HOST_FEC_TYPE_RS544 = 2,       /**< RS544 KP4 */
    CAPI_HOST_FEC_TYPE_PCS = 3,         /**< PCS */
    CAPI_HOST_FEC_TYPE_MAX
} capi_host_fec_type_t;                 /**< FEC type definition */

/**
 * The enumeration of Line FEC Type
 */
typedef enum capi_line_fec_type_e {
    CAPI_LINE_FEC_TYPE_NA = 0,          /**< No FEC Mode */
    CAPI_LINE_FEC_TYPE_RS528 = 1,       /**< RS528 KR4*/
    CAPI_LINE_FEC_TYPE_RS544 = 2,       /**< RS544 KP4 */
    CAPI_LINE_FEC_TYPE_PCS = 3,       /**< PCS */
    CAPI_LINE_FEC_TYPE_MAX
} capi_line_fec_type_t;                 /**< FEC type definition */

/**
 * The enumeration of reference clock frequency mode
 */
typedef enum ref_clk_frq_mode_e {
    CAPI_REF_CLK_FRQ_156_25_MHZ_ETHERNET = 0,       /**< 156.25 Mhz (Ethernet) */
    CAPI_REF_CLK_FRQ_625_MHZ_ETHERNET = 1,          /**< 625 Mhz (Ethernet)    */
    CAPI_REF_CLK_FRQ_166_15625_MHZ_ETHERNET = 2,    /**< 166.15625 Mhz         */
    CAPI_REF_CLK_FRQ_MAX
} capi_ref_clk_frq_mode_t;                          /**< Reference clock frequency mode type */

/**
 * The enumeration of lane mux Type
 */
typedef enum capi_lane_mux_e {
    CAPI_LANE_MUX_BIT_MUX = 0,          /**< Bit mux type */
    CAPI_LANE_MUX_SYMBOL_MUX = 1,       /**< Symbol mux type  */
    CAPI_LANE_MUX_MAX
} capi_lane_mux_t;                      /**< Chip lane mux type */

/**
 * The enumeration of Path Type
 */
typedef enum {
    CAPI_REPEATER_PATH,         /*!< Repeater */
    CAPI_RETIMER_PATH,          /*!< Retimer */
    CAPI_PATH_MAX
} capi_cfg_dp_type_t;           /*!< Chip data path type */

/**
 * The enumeration of lane FEC term Type
 */
typedef enum capi_lane_fec_term_type_e {
    CAPI_LANE_FEC_TERM_BYPASS = 0,          /**< FEC_TERM_BYPASS */
    CAPI_LANE_FEC_DEC_FWD = 1,              /**< FEC_DEC_FWD */
    CAPI_LANE_FEC_DEC_ENC = 2,              /**< FEC_DEC_ENC */
    CAPI_LANE_PCS_XENC = 3,                 /**< PCS_XENC */
    CAPI_LANE_FEC_DEC_XDEC_XENC_ENC = 4,    /**< FEC_DEC_XDEC_XENC_ENC */
    CAPI_LANE_FEC_TERM_MAX
} capi_lane_fec_term_type_t;                /**< Chip lane fec term type */

/**
 * The enumeration of Line Wrapper Baud Rate
 */
typedef enum capi_lw_baud_rate_e {
    CAPI_LW_BR_53_125 = 0,                  /**< LW baud rate 53.125G    */
    CAPI_LW_BR_51_5625,                     /**< LW baud rate 51.5625G    */
    CAPI_LW_BR_25_78125,                    /**< LW baud rate 25.78125G    */
    CAPI_LW_BR_26_5625,                     /**< LW baud rate 26.5625G    */
    CAPI_LW_BR_10_3125,                     /**< LW baud rate 10.3125G    */
    CAPI_LW_BR_20_625,                      /**< LW baud rate 20.625G    */
    CAPI_LW_BR_1_25,                        /**< LW baud rate 1.25G    */
    CAPI_LW_BR_MAX
} capi_lw_baud_rate_t;                      /**< LW baud rates           */

/**
 * The enumeration of BH Baud Rate
 */
typedef enum capi_bh_baud_rate_e {
    CAPI_BH_BR_53_125 = 0,                  /**< BH baud rate 53.125G    */
    CAPI_BH_BR_51_5625,                     /**< BH baud rate 51.5625G    */
    CAPI_BH_BR_25_78125,                    /**< BH baud rate 25.78125G  */
    CAPI_BH_BR_26_5625,                     /**< BH baud rate 26.5625G   */
    CAPI_BH_BR_10_3125,
    CAPI_BH_BR_20_625,
    CAPI_BH_BR_1_25,
    CAPI_BH_BR_MAX
} capi_bh_baud_rate_t;                      /**< BH baud rates           */

/**
 * The enumeration of chip function mode
 */
typedef enum capi_function_mode_e {
    CAPI_MODE_NONE = 0,                     /**< No suitable func mode                       */
    CAPI_MODE_400G = 1,                     /**< 400G mode                                   */
    CAPI_MODE_200G = 2,                     /**< 200G mode                                   */
    CAPI_MODE_100G = 3,                     /**< 100G mode                                   */
    CAPI_MODE_50G  = 4,                     /**< 50G mode                                    */
    CAPI_MODE_25G  = 5,                     /**< 25G mode                                    */
    CAPI_MODE_10G  = 6,                     /**< 10G mode                                    */
    CAPI_MODE_1G   = 7,                     /**< 1G mode                                     */
    CAPI_MODE_MAX
} capi_function_mode_t;                     /**< Chip function mode type                     */

/**
 * The enumeration of modulation type
 */
typedef enum capi_modulation_e {
    CAPI_MODULATION_NRZ  = 0,               /**< NRZ modulation type                          */
    CAPI_MODULATION_PAM4 = 1,               /**< PAM4 modulation type                         */
} capi_modulation_t;                        /**< Chip modulation type list                    */

/**
 * A structure to represent lane configuration information
 */
typedef struct config_lane_mod_s {
    uint16_t        modulation;             /**< modulation type; refer to capi_modulation_t   */
    uint16_t        lane_mask;              /**< lane mask related to specified port_mode_enum */
} capi_lane_config_mod_t;                   /**< Lane configuration information type           */

/**
 * A structure to represent port configuration status
 */
typedef enum capi_port_config_status_e {
    CAPI_PORT_CONFIG_STATUS_NOT_CONFIGURED = 0,     /**< Port not configured               */
    CAPI_PORT_CONFIG_STATUS_IN_PROGRESS = 1,        /**< Port configurtion is in progress  */
    CAPI_PORT_CONFIG_STATUS_SUCCESS = 2,            /**< Port configuration success        */
    CAPI_PORT_CONFIG_STATUS_FAILED = 3,             /**< Port configuration failed         */
} capi_port_config_status_t;                        /**< Port config status type           */

/**
 * A structure to represent port power down status
 */
typedef enum capi_port_power_down_status_e {
    CAPI_PORT_POWER_DOWN_STATUS_POWER_UP=0,         /**< Port is powered up          */
    CAPI_PORT_POWER_DOWN_STATUS_POWER_DOWN=1        /**< Port is powered down        */
} capi_port_power_down_status_t;                     /**< Port power down status type */

/**
 * The structure to represent chip configuration information
 */
typedef struct config_info_s {
    uint16_t                ref_clk;        /**< Reference clock; refer to capi_ref_clk_frq_mode_t               */
    uint16_t                func_mode;      /**< Function Mode; refer to capi_function_mode_t                    */
    uint16_t                fec_term;       /**< fec term type; refer to capi_lane_fec_term_type_t               */
    uint16_t                mux_type;       /**< MUX mode; refer to capi_lane_mux_t                              */
    uint16_t                line_fec_type;  /**< Line side FEC config info; refer to capi_line_fec_type_t        */
    uint16_t                host_fec_type;  /**< Host side FEC config info; refer to capi_host_fec_type_t        */
    uint16_t                bh_br;          /**< Client side bud rate; refer to capi_bh_baud_rate_t              */
    uint16_t                lw_br;          /**< Line side bud rate; refer to capi_lw_baud_rate_t                */
    capi_lane_config_mod_t  line_lane;      /**< line side lane config                                           */
    capi_lane_config_mod_t  host_lane;      /**< host side lane config                                           */
    uint16_t                status;         /**< Port configuration status refer to capi_port_config_status_t    */
    uint16_t                pwd_status;     /**< Port power down status; refer to capi_port_power_down_status_t  */
} capi_config_info_t;                       /**< Configuration info type                                         */

#define PURELY_CAPI_SET_VDDM   1

#define CHIP_VOLTAGE_SET_FLAG    0x6001
#define CHIP_VOLTAGE_GET_STATUS  0x7000

/*extern uint8_t util_get_max_lanes(phy_info_t* phy_info_ptr);

#define MAX_CORES                   1 */ /*!< Max cores in the chip           */
#define MAX_PRTAD                   4   /*!< Max port_address supported      */
#define MAX_LW_LANES                4   /*!< Max LW lanes supported          */
#define MAX_BH_LANES                4   /*!< Max BH lanes supported          */
#define MAX_LANES(x)                util_get_max_lanes(x)
/*
#define NUM_OF_TRACE_POINT_PER_GENERAL 16
#define TRACE_BUFFER_BASE_GENERAL 0x2FA00
*/
#define MAX_PORT 4

/* #define INCREMENTAL_MDIO */
#define __HSIP_FUNC_ERR &(__hsip_func_err)
/**
 * The enumeration of lane command
 */
typedef enum lane_command_e {
    CAPI_LANE_CMD_SET_ALL_MODES = 0x0,      /*!< Set All Modes (Apply globally, Set lane, Blackhawk and line modes together) */
    CAPI_LANE_CMD_SET_PER_PLL_MODES = 0x1,  /*!< Set All Modes (Apply globally, apply on per PLL range, such as: lane0 & lane1; lane2 & lane3 */

    CAPI_LANE_CMD_SET_BLACKHAWK_MODE = 0x3, /*!< Set Blackhawk Mode (Apply only to Blackhawk Side) */
    CAPI_LANE_CMD_SET_LINE_MODE = 0x4,      /*!< Set Line Mode (Apply only to Line side) */

    CAPI_LANE_CMD_ASSERT_RESET_BLACKHAWK_LANE = 0x7,    /*!< Assert Reset Blackhawk Lane (Rx & Tx) */
    CAPI_LANE_CMD_RELEASE_RESET_BLACKHAWK_LANE = 0x8,   /*!< Release Reset Blackhawk Lane (Rx & Tx) */
    CAPI_LANE_CMD_ASSERT_RESET_LINE_LANE = 0x9,         /*!< Assert Reset Line Lane (Rx & Tx) */
    CAPI_LANE_CMD_RELEASE_RESET_LINE_LANE = 0xA,        /*!< Release Reset Line Lane (Rx & Tx) */
    CAPI_LANE_CMD_READ_STATUS_BLACKHAWK_LANE = 0xB,     /*!< Read Status Blackhawk Lane (LOS, CDR Lock, PLL Lock, SW State) */
    CAPI_LANE_CMD_READ_STATUS_LINE_LANE = 0xC,          /*!< Read Status Line Lane (LOS, CDR Lock, PLL Lock, SW State) */
    CAPI_LANE_CMD_SUSPEND_LINE_LANE_INGRESS = 0xD,      /*!<  */
    CAPI_LANE_CMD_RESUME_LINE_LANE_INGRESS = 0xE,       /*!<  */
    CAPI_LANE_CMD_SUSPEND_LINE_LANE_EGRESS = 0xF,       /*!<  */
    CAPI_LANE_CMD_RESUME_LINE_LANE_EGRESS = 0x10,       /*!<  */

    CAPI_LANE_CMD_SUSPEND_BH_LANE_INGRESS = 0x15,       /*!<  */
    CAPI_LANE_CMD_RESUME_BH_LANE_INGRESS = 0x16,        /*!<  */
    CAPI_LANE_CMD_SUSPEND_BH_LANE_EGRESS = 0x17,        /*!<  */
    CAPI_LANE_CMD_RESUME_BH_LANE_EGRESS = 0x18,         /*!<  */

    CAPI_LANE_CMD_FORCE_LW_BH_CDR_LOCKED = 0x19,
    CAPI_LANE_CMD_FORCE_LW_CDR_LOCKED = 0x20,
    CAPI_LANE_CMD_FORCE_BH_CDR_LOCKED = 0x21,

    CAPI_LANE_CMD_UNFORCE_LW_BH_CDR_LOCKED = 0x22,
    CAPI_LANE_CMD_UNFORCE_LW_CDR_LOCKED = 0x23,
    CAPI_LANE_CMD_UNFORCE_BH_CDR_LOCKED = 0x24,

    CAPI_LANE_CMD_GET_LW_STICKY_STATUS = 0x2E,

    CAPI_LANE_CMD_SET_PORTS_FUNCTION_MOKDE = 0x30,
    CAPI_LANE_CMD_ADD_NEW_PROTS = 0x31,
    CAPI_LANE_CMD_DROP_EXITING_PORTS = 0x32,
    CAPI_LANE_CMD_DROP_ALL_EXITING_PORTS = 0x33,
    CAPI_LANE_CMD_ASSERT_DATA_RESET_FOR_PORTS = 0x34,
    CAPI_LANE_CMD_DEASSERT_DATA_RESET_FORT_PORTS = 0x35,
    CAPI_LANE_CMD_ASSERT_POWER_DOWN_FOR_PORTS = 0x36,
    CAPI_LANE_CMD_DEASSERT_POWER_DOWN_FORT_PORTS = 0x37,
    CAPI_LANE_CMD_GET_PORT = 0x38,
    CAPI_LANE_CMD_ADD_NEW_PROTS_LPM = 0x39,

    CAPI_LANE_CMD_ADD_NEW_PORTS_EX = 0x3A,
} capi_lane_command_t;                              /*!< Lane Command type */

/*
 * A enumaratoin to represent register type
 */
typedef enum register_type_s {
    CAPI_CHIP_REF_CLOCK_CONFIG_REGISTER,        /**< Chip Reference Clock Configuration Register */
    CAPI_CHIP_TEMP_STATUS_REGISTER,             /**< Chip Temperture Status Register */
    CAPI_CHIP_VOLTAGE_STATUS_REGISTER,          /**< Chip Voltage Status Register */
    CAPI_LANE_MODE_REGISTER,                    /**< Lane Mode Register */
    CAPI_LANE_COMMAND_REGISTER,                 /**< Lane Command Register */
    CAPI_LANE_STATUS_REGISTER,                  /**< Lane Status Register */
    CAPI_CLIENT_LANE_MODE_REGISTER,             /**< Client Lain Mode Register */
    CAPI_LINE_WRAPPER_MODE_REGISTER             /**< Line Wrapper Mode Register */
} capi_register_t;                              /**< Register type */

/**
 * The union of chip reference clock configuration register
 */
typedef union chip_ref_clk_conf_reg_u {
    uint16_t content;                       /** Chip Reference Clock Configuration Register content */

    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint16_t reserved            : 10;  /** Reserved                       */
        uint16_t ref_clk_valid       : 1;   /** Rererence Clock Valid          */
        uint16_t ref_clock_freq_mode : 5;   /** Reference Clock Frequency Mode */
        #else
        uint16_t ref_clock_freq_mode : 5;   /** Reference Clock Frequency Mode */
        uint16_t ref_clk_valid       : 1;   /** Rererence Clock Valid          */
        uint16_t reserved            : 10;  /** Reserved                       */
        #endif
    } is;
} capi_chip_ref_clk_conf_reg_t;             /** Reference Clock Config Rerigster type */

/**
 * The enumeration of reference clock valid
 */
typedef enum ref_clk_valid_s {
    CAPI_REF_CLOCK_NOT_VALID,   /**< Reference clock not valid */
    CAPI_REF_CLOCK_VALID        /**< Reference clock valid */
} capi_ref_clk_valid_t;         /**< Reference Clock VAlid type*/

/**
 * The union of chip temperature status register
 */
typedef union chip_temp_status_reg_u {
    int16_t temperature;                         /**< Tempurture reported in celcius                */
                                                 /**< Normal Range  : -10C to 105C                  */
                                                 /**< Exterim Range : -40C to 125C  Not recommended */ 
} capi_temp_status_info_t;                       /**< Tempurture Status Information type            */

/**
 * The union of lane mode register
 */
typedef union lane_mode_reg_u {
    uint16_t content; /** Lane Mode Register content*/

    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint16_t client_loopback_alt_mode : 1;  /** Client Loopback Alt Mode */
        uint16_t function_mode            : 6;  /** Function Mode            */
        uint16_t fec_mode                 : 2;  /** FEC Mode                 */
        uint16_t rate_mode                : 7;  /** Rate Mode                */
        #else
        uint16_t rate_mode                : 7;  /** Rate Mode                */
        uint16_t fec_mode                 : 2;  /** FEC Mode                 */
        uint16_t function_mode            : 6;  /** Function Mode            */
        uint16_t client_loopback_alt_mode : 1;  /** Client Loopback Alt Mode */
        #endif
    } is;
} capi_lane_mode_reg_t;                         /** Lane Mode Register type */


/**
 * The enumeration of Black Hawk Loopback Alt Mode
 */
typedef enum bh_loopback_alt_mode_e {
    CAPI_BH_LOOPBACK_ALT_MODE_OFF,      /**< Blackhawk loopback ALT mode off */
    CAPI_BH_LOOPBACK_ALT_MODE_ON        /**< Blackhawk loopback ALT mode on */
} capi_bh_loopback_alt_mode_t;          /**< BH Loopback ALT Mode type */

/**
 * The union of lane command register
 */
typedef union lane_command_reg_u {
    uint16_t content; /** Lane Command Register content*/

    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint16_t reserved       : 7;    /** Reserved       */
        uint16_t command_change : 1;    /** command Change */
        uint16_t command        : 8;    /** Command        */
        #else
        uint16_t command        : 8;    /** Command        */
        uint16_t command_change : 1;    /** command Change */
        uint16_t reserved       : 7;    /** Reserved       */
        #endif
    } is;
} capi_lane_command_reg_t;              /** Lane Command Register type */


/**
 * The enumeration of command change
 */
typedef enum command_change_e {
    CAPI_COMMAND_NOT_CHANGED,   /**< Command Not Changed */
    CAPI_COMMAND_CHANGED        /**< Command Changed */
} capi_command_change_t;        /**< Command Change type */

/**
 * The union of lane status register
 */
typedef union lane_status_reg_u {
    uint16_t content;                       /** Lane Status Register content*/

    #ifdef CAPI_IS_BIG_ENDIAN
    struct {
        uint16_t command_accepted   : 1;    /** Command Accepted                     */
        uint16_t line_lock          : 1;    /** Line Lock                            */
        uint16_t error_code         : 6;    /** Error Code                           */
        uint16_t linktrn_error_code : 2;    /** Link Training Error Code             */
        uint16_t linktrn_done       : 1;    /** Successfully Completed Link Training */
        uint16_t cdr_lock           : 2;    /** CDR Lock                             */
        uint16_t adc_clip_detect    : 1;    /** Detected Clipping of ADC Signal      */
        uint16_t los                : 1;    /** Lose of Signal                       */
        uint16_t pll_lock           : 1;    /** PLL Lock                             */
    } lw;

    struct {
        uint16_t command_accepted   : 1;     /** Command Accepted                    */
        uint16_t line_lock          : 1;     /** Line Lock                           */
        uint16_t error_code         : 6;     /** Error Code                          */
        uint16_t reserved2          : 1;     /** Reserved                            */
        uint16_t pll_unlock_sticky  : 2;     /** PLL unlock sticky                   */
        uint16_t bh2bh_fifo_status  : 2;     /** Bh2bh fifo status                   */
        uint16_t cdr_lock           : 1;     /** CDR Lock                            */
        uint16_t los                : 1;     /** Lose of Signal                      */
        uint16_t pll_lock           : 1;     /** PLL Lock                            */
    } client;

    #else /*CAPI_IS_LITTLE_ENDIAN*/

    struct {
        uint16_t pll_lock           : 1;     /** PLL Lock                             */
        uint16_t los                : 1;     /** Lose of Signal                       */
        uint16_t adc_clip_detect    : 1;     /** Detected Clipping of ADC Signal      */
        uint16_t cdr_lock           : 2;     /** CDR Lock                             */
        uint16_t linktrn_done       : 1;     /** Successfully Completed Link Training */
        uint16_t linktrn_error_code : 2;     /** Link Training Error Code             */
        uint16_t error_code         : 6;     /** Error Code                           */
        uint16_t line_lock          : 1;     /** Line Lock                            */
        uint16_t command_accepted   : 1;     /** Command Accepted                     */
    } lw;

    struct {
        uint16_t pll_lock           : 1;     /** PLL Lock                             */
        uint16_t los                : 1;     /** Lose of Signal                       */
        uint16_t cdr_lock           : 1;     /** CDR Lock                             */
        uint16_t bh2bh_fifo_status  : 2;     /** Bh2bh fifo status                    */
        uint16_t pll_unlock_sticky  : 2;     /** PLL unlock sticky                    */
        uint16_t reserved2          : 1;     /** Reserved                             */
        uint16_t error_code         : 6;     /** Error Code                           */
        uint16_t line_lock          : 1;     /** Line Lock                            */
        uint16_t command_accepted   : 1;     /** Command Accepted                     */
    } client;
    #endif /*CAPI_IS_BIG_ENDIAN*/
} capi_lane_status_reg_t;                    /** Line Wrapper and Client Lane Status Register type */


typedef union chip_top_spi_program_command_t_u {
    uint32_t words;
    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint32_t reserved       :12;         /*!< reserved       */ 
        uint32_t block_id       :16;         /*!< block_id       */
        uint32_t buffer_id      : 1;         /*!< buffer_id      */         
        uint32_t error_status   : 2;         /*!< error_status   */       
        uint32_t command_change : 1;         /*!< command_change */
        #else /* LITTLE ENDIAN */        
        uint32_t command_change : 1;         /*!< command_change */        
        uint32_t error_status   : 2;         /*!< error_status   */ 
        uint32_t buffer_id      : 1;         /*!< buffer_id      */   
        uint32_t block_id       :16;         /*!< block_id       */
        uint32_t reserved       :12;         /*!< reserved       */
        #endif /*CAPI_IS_BIG_ENDIAN*/
    } fields;
} chip_top_spi_program_command_t;



/**
 * The enumeration of error code
 */
typedef enum capi_err_code_e {
    CAPI_NO_ERROR       /**< No Error Code */
} capi_err_code_t;      /**< Error Code type */

/**
 * The enumeration of command accepted
 */
typedef enum command_accepted_e {
    CAPI_COMMAND_NOT_ACCEPTED,  /**< Command Not Accepted */
    CAPI_COMMAND_ACCEPTED       /**< Command Accepted */
} capi_command_accepted_t;      /**< Command Accepted type */


/**
 * The union of BH lane mode register
 */

typedef union bh_lane_config_reg_u {
    uint16_t content; /**< BH Lane Mode Register content */

    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint16_t rx            : 1;    /**< Rx disable    */
        uint16_t tx            : 1;    /**< Tx disable    */
        uint16_t link_training : 1;    /**< Link training */
        uint16_t dfe           : 1;    /**< DFE           */
        uint16_t precoder      : 1;    /**< Precoder      */
        uint16_t ns_es         : 1;    /**< NS/ES         */
        uint16_t medium        : 2;    /**< Medium        */
        uint16_t lane_mode     : 2;    /**< Lane Mode     */
        uint16_t db_lost       : 6;    /**< DB Lost       */
        #else /*CAPI_IS_LITTLE_ENDIAN*/
        uint16_t db_lost       : 6;    /**< DB Lost       */
        uint16_t lane_mode     : 2;    /**< Lane Mode     */
        uint16_t medium        : 2;    /**< Medium        */
        uint16_t ns_es         : 1;    /**< NS/ES         */
        uint16_t precoder      : 1;    /**< Precoder      */
        uint16_t dfe           : 1;    /**< DFE           */
        uint16_t link_training : 1;    /**< Link training */
        uint16_t tx            : 1;    /**< Tx disable    */
        uint16_t rx            : 1;    /**< Rx disable    */
        #endif /*CAPI_IS_BIG_ENDIAN*/
    } is;
} bh_lane_config_reg_t;             /**< Black Hawk Lane Config Register type */

/**
 * The enumeration of BH DB lost
 */
typedef enum bh_db_lost_e {
    CAPI_BH_DB_LOST_0_9_VSR,        /**< DB lost 0-9 VSR */
    CAPI_BH_DB_LOST_10_19_MR,       /**< DB lost 10-19 MR */
    CAPI_BH_DB_LOST_20_29_LR        /**< DB lost 20-29 LR */
} capi_bh_db_lost_t;                /**< Black Hawk DB Lost type */


/**
 * The enumeration of BH medium
 */
typedef enum bh_medium_e {
    CAPI_BH_MEDIUM_PCB,             /**< PCB medium */
    CAPI_BH_MEDIUM_COPPER_WIRE,     /**< Copper wire medium */
    CAPI_BH_MEDIUM_OPTICAL          /**< Optical medium */
} capi_bh_medium_t;                 /**< BH Medium type */

/**
 * The enumeration of BH Tx
 */
typedef enum bh_tx_e {
    CAPI_BH_tx_ENABLE,      /**< Tx enable*/
    CAPI_BH_tx_DISABLE      /**< Tx disable*/
} capi_bh_tx_t;             /**< Tx type */

/**
 * The enumeration of BH Rx
 */
typedef enum bh_rx_e {
    CAPI_BH_rx_ENABLE,      /**< Rx enable*/
    CAPI_BH_rx_DISABLE      /**< Rx disable*/
} capi_bh_rx_t;             /**< Rx type */

/**
 * The union of line wrapper mode register
 */
typedef union lw_mode_reg_u {
    uint16_t content; /**< Line Wrapper Mode Register content */

    struct {
        #ifdef CAPI_IS_BIG_ENDIAN
        uint16_t optical_mode  : 1;     /**< Optical mode  */
        uint16_t phd_mode      : 3;     /**< Phd Mode      */
        uint16_t eq_tap_sel    : 2;     /**< EQ tap sel    */
        uint16_t eqpp          : 1;     /**< EQPP          */
        uint16_t nldet         : 1;     /**< NLDet         */
        uint16_t dfe           : 1;     /**< DFE           */
        uint16_t extended_mode : 1;     /**< Extended mode */
        uint16_t r_loop        : 1;     /**< R loop        */
        uint16_t g_loop        : 1;     /**< G loop        */
        uint16_t link_training : 3;     /**< Link training */
        uint16_t auto_neg      : 1;     /**< AutoCL73      */
        #else
        uint16_t auto_neg      : 1;     /**< AutoCL73      */
        uint16_t link_training : 3;     /**< Link training */
        uint16_t g_loop        : 1;     /**< G loop        */
        uint16_t r_loop        : 1;     /**< R loop        */
        uint16_t extended_mode : 1;     /**< Extended mode */
        uint16_t dfe           : 1;     /**< DFE           */
        uint16_t nldet         : 1;     /**< NLDet         */
        uint16_t eqpp          : 1;     /**< EQPP          */
        uint16_t eq_tap_sel    : 2;     /**< EQ tap sel    */
        uint16_t phd_mode      : 3;     /**< Phd Mode      */
        uint16_t optical_mode  : 1;     /**< Optical mode  */
        #endif
    } is;
} lw_mode_reg_t; /**< Line Wrapper mode Register type */

/**
 * The enumeration of LW optical mode
 */
typedef enum lw_optical_mode_e {
    CAPI_LW_OPTICAL_MODE,   /**< Optical mode */
    CAPI_LW_COPPER_MODE     /**< Copper mode */
} capi_lw_optical_mode_t;   /**< Optical Mode type */

/**
 * The enumeration of LW Phd mode
 */
typedef enum lw_phd_mode_e {
    CAPI_LW_PHD_MODE_PRE_CURSOR,            /**< Pre-cursor */
    CAPI_LW_PHD_MODE_MMSE,                  /**< MMSE */
    CAPI_LW_PHD_MODE_SYMMETRICAL,           /**< Sumetrical */
    CAPI_LW_PHD_MODE_SYMETRICAL_W_PRECODER  /**< Symetrical with precoder */
} capi_lw_phd_mode_t;                       /**< Phd Mode type */

/**
 * The enumeration of EQPP
 */
typedef enum lw_eqpp_e {
    CAPI_LW_EQPP_ENABLE, /**< EQPP enable*/
    CAPI_LW_EQPP_DISABLE /**< EQPP disable*/
} capi_lw_eqpp_t; /**< EQPP type */

/**
 * The enumeration of NLDet
 */
typedef enum lw_nldet_e {
    CAPI_LW_NLDET_ENABLE, /**< NLDet enable*/
    CAPI_LW_NLDET_DISABLE /**< NLDet disable*/
} capi_lw_nldet_t; /**< NLDet type */

/**
 * The enumeration of LW DFE
 */
typedef enum lw_dfe_e {
    CAPI_LW_DFE_ENABLE, /**< DFE enable*/
    CAPI_LW_DFE_DISABLE /**< DFE disable*/
} capi_lw_dfe_t; /**< DFE type */

/**
 * The enumeration of BH slicer mode
 */
typedef enum capi_bh_slicer_mode_e {
    CAPI_BH_SLICER_MODE_NS, /**< NS slicer mode */
    CAPI_BH_SLICER_MODE_ES /**< ES slicer mode */
} capi_bh_slicer_mode_t; /**< slicer mode type */

/**
 * The enumeration of BH precoder
 */
typedef enum bh_precoder_e {
    CAPI_BH_PRECODER_ENABLE, /**< Precoder enable */
    CAPI_BH_PRECODER_DISABLE /**< Precoder disable */
} capi_bh_precoder_t; /**< Precoder type */

/**
 * The enumeration of LW extended mode
 */
typedef enum lw_extended_e {
    CAPI_LW_EXTENDED_MODE_NO_OVERRIDE,      /**< No override */
    CAPI_LW_EXTENDED_MODE_SLICER,           /**< Slicer */
    CAPI_LW_EXTENDED_MODE_REGULAR_SLICER    /**< Regular sliver */
} capi_lw_extended_mode_t;                  /**< Extended Mode type */

/**
 * The enumeration of LW G loop
 */
typedef enum lw_g_loop_e {
    CAPI_LW_G_LOOP_ENABLE, /**< G loop enable */
    CAPI_LW_G_LOO_DISABLE /**< G loop disable */
} capi_lw_g_loop_t; /**< G Loop type */

/**
 * The enumeration of LW R loop
 */
typedef enum lw_r_loop_e {
    CAPI_LW_R_LOOP_ENABLE, /**< R loop enable */
    CAPI_LW_R_LOOP_DISABLE /**< R loop disable */
} capi_lw_r_loop_t; /**< R Loop type */

/**
 * The enumeration of LW link training
 */
typedef enum lw_link_training_e {
    CAPI_LW_LINK_TRAINING_NONE,                 /**< Link training none */
    CAPI_LW_LINK_TRAINING_CL73,                 /**< Link training CL73 */
    CAPI_LW_LINK_TRAINING_802_3CD_NRZ,          /**< Link training 802.3cd NRZ */
    CAPI_LW_LINK_TRAINING_802_3CD_PAM4,         /**< Link training 802.3cd PAM4 */
    CAPI_LW_LINK_TRAINING_802_3CD_PAM4_PRECODER /**< Link training 802.3cd PAM4 precoder */
} capi_lw_link_training_t;                      /**< Link Training type */

/**
 * The enumeration of LW CL73
 */
typedef enum lw_cl73_e {
    CAPI_BH_CL73_ENABLE,    /**< CL73 enable */
    CAPI_BH_CL73_DISABLE    /**< CL73 disable */
} capi_lw_cl73_t;           /**< CL73 type */

/**
 * The enumeration of data rates
 */
typedef enum data_rate_e {
    CAPI_1G_DATA_RATE,      /**< 1 Gbps data rate */
    CAPI_2G_DATA_RATE,      /**< 2 Gbps data rate */
    CAPI_10G_DATA_RATE,     /**< 25 Gbps data rate */
    CAPI_25G_DATA_RATE,     /**< 50 Gbps data rate */
    CAPI_50G_DATA_RATE,     /**< 100 Gbps data rate */
    CAPI_200G_DATA_RATE,    /**< 200 Gbps data rate */
    CAPI_400G_DATA_RATE,    /**< 400 Gbps data rate */
    CAPI_600G_DATA_RATE,    /**< 600 Gbps data rate */
    CAPI_800G_DATA_RATE,    /**< 800 Gbps data rate */
    CAPI_1000G_DATA_RATE,   /**< 1 Tbps data rate */
    CAPI_1200G_DATA_RATE,   /**< 1.2 Tbps data rate */
    CAPI_1600G_DATA_RATE,   /**< 1.6 Tbps data rate */
    CAPI_2000G_DATA_RATE    /**< 2 Tbps data rate */
} capi_data_rate_t;         /**< Data Rate type */

/**
 * The enumeration of firmware image type
 */
typedef enum capi_firmware_image_type_s {
    FIRMWARE_IMAGE_DEFAULT,         /**< default image */
    FIRMWARE_IMAGE_J                /**< image */
} capi_firmware_image_type_t;       /**< fimrware image type */

/**
 * A structure to represent each phy element of the phy list
 */
typedef struct capi_load_phy_list_s {
    uint32_t addr;                          /**< PHY address (PHYAD) used by this PHY */
    capi_firmware_image_type_t image_type;  /**< firmware image type for this PHY */
} capi_load_phy_info_t;                     /**< represent each phy element of the phy list */

/**
 * A structure to represent phy list
 */
typedef struct capi_firmware_load_phy_list_s {
    capi_load_phy_info_t* phy_info;         /**< pointer to the phy list*/
    uint32_t num_of_phy;                    /**< the number of phys in the phy list*/
} capi_firmware_load_phy_list_t;            /**< PHY list */

/**
 * The enumeration of Downloading Mechanism
 */
typedef enum download_mode_e {
    CAPI_DOWNLOAD_MODE_NONE,        /**< Download Mode None */
    CAPI_DOWNLOAD_MODE_MDIO_SRAM,   /**< Download via MDIO to SRAM */
    CAPI_DOWNLOAD_MODE_I2C_SRAM,    /**< Download via I2C to SRAM*/
    CAPI_DOWNLOAD_MODE_MDIO_EEPROM, /**< Download via MDIO to EEPRM*/
    CAPI_DOWNLOAD_MODE_I2C_EEPROM,  /**< Download via I2C to EEPRM*/
} capi_download_mode_t;             /**< Downloading Mode type */

/**
 * The enumeration of SPI Program mode
 */
typedef enum capi_spi_program_mode_e {
    CAPI_SPI_PROGRAM_STANDARD,    /**< stanadard */
    CAPI_SPI_PROGRAM_FAST,        /**< FAST*/
    
} capi_spi_program_mode_t;             /**< spi program mode */

/**
 * The enumeration of SRAM Program mode
 */
typedef enum capi_sram_program_mode_e {
    CAPI_SRAM_PROGRAM_STANDARD,    /**< stanadard */
    CAPI_SRAM_PROGRAM_FAST,        /**< FAST*/
    
} capi_sram_program_mode_t;             /**< sram program mode */
/**
 * A structure to represent the status
 */
typedef struct status_info_s {
    uint32_t version;           /**< Version */
    uint32_t sub_version;       /**< Sub Version */
    uint32_t crc_check;         /**< CRC check */
    boolean result;             /**< Download Result */
} capi_status_info_t;           /**< Download Status type */

/**
 * A structure to represent firmware image
 */
typedef struct capi_image_info_s {
    uint32_t* image_ptr;
    uint32_t  image_size;
} capi_image_info_t; /**< firmware info type */


typedef enum capi_default_mode_e {
    CAPI_NO_DEFAULT_MODE = 0,
    CAPI_DEFAULT_MODE_1_REPEATER = 1,  /*CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_NA    CHIP_LINE_FEC_TYPE_NA  CHIP_PORT_FEC_TERM_BYPASS  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_1_KP4_KP4  = 2,  /*CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_RS544    CHIP_LINE_FEC_TYPE_RS544  CHIP_PORT_FEC_DEC_ENC  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_5_PCS_KP4  = 3,  /*CHIP_MODE_4x25G_NRZ_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_PCS    CHIP_LINE_FEC_TYPE_RS544  CHIP_LANE_PCS_XENC  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_5_KR4_KP4  = 4,  /*CHIP_MODE_4x25G_NRZ_TO_1X106G_KP4PAM  CHIP_HOST_FEC_TYPE_RS528    CHIP_LINE_FEC_TYPE_RS544  CHIP_PORT_FEC_DEC_ENC  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_4_NRZ_NRZ_REPEATER  = 5,  /*CHIP_MODE_4x25G_KR4NRZ_TO_4x25G_KR4NRZ  CHIP_HOST_FEC_TYPE_NA    CHIP_HOST_FEC_TYPE_NA  CHIP_PORT_FEC_TERM_BYPASS  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_7_REPEATER = 6,  /*CHIP_MODE_8x53G_KP4PAM_TO_4X106G_KP4PAM  CHIP_HOST_FEC_TYPE_NA    CHIP_LINE_FEC_TYPE_NA  CHIP_PORT_FEC_TERM_BYPASS  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_7_KP4_KP4 = 7,  /*CHIP_MODE_8x53G_KP4PAM_TO_4X106G_KP4PAM  CHIP_HOST_FEC_TYPE_RS544    CHIP_HOST_FEC_TYPE_RS544  CHIP_PORT_FEC_DEC_ENC  CHIP_PORT_MUX_BIT_MUX*/
    CAPI_DEFAULT_MODE_MAX  = 8,
} capi_default_mode_t;
/**
 * The struct of static_config
 */
typedef struct phy_static_config_s {
    bool avs_enable;
    capi_default_mode_t chip_default_mode;
} phy_static_config_t;

/**
 * Thecapi_burst_write_mode
 */
typedef enum capi_burst_write_mode_e {
    CAPI_BURST_WRITE_MODE_I2C,      
    CAPI_BURST_WRITE_MODE_MDIO,   
    CAPI_BURST_WRITE_MODE_I2C_OPT,
} capi_burst_write_mode_t;         

/**
 * A structure to represent download information
 */
typedef struct download_info_s {
    capi_status_info_t status;                  /**< Download Status */
    capi_download_mode_t mode;                  /**< Download Mode */
    phy_static_config_t* phy_static_config_ptr; /**< static_config */
    capi_image_info_t image_info;
    capi_spi_program_mode_t spi_program_mode;     /**< 0: spi program standard mode 1: fast mode with I2c block write*/
    capi_burst_write_mode_t  burst_write_mode;
    capi_sram_program_mode_t sram_program_mode;     /**< 0: sram program standard mode 1: fast mode with I2c block write*/
} capi_download_info_t;                         /**< Download info type */


/**
 * The enumeration of access bus interface
 */
typedef enum capi_bus_intf_e {
    CAPI_ACCESS_MODE_MDIO = 0,
    CAPI_ACCESS_MODE_I2C
} capi_bus_intf_t;

typedef enum broadcast_enable_e {
    BROADCAST_DISABLE = 0,
    BROADCAST_ENABLE = 1,
} broadcast_enable_t;


typedef struct capi_access_intf_s {
    capi_bus_intf_t bus;
    broadcast_enable_t enable;
} capi_broadcast_intf_t;

/**
 * The enumeration of reset modes
 */
typedef enum reset_mode_e {
    CAPI_HARD_RESET_MODE,       /**< Hard reset mode */
    CAPI_SOFT_RESET_MODE,        /**< Soft reset mode */
    CAPI_RESET_BRING_UP_MODE    /**< reset bringup mode */
} capi_reset_mode_t;            /**< Reset mode type */

/**
 * The enumeration of interface modes
 */
typedef enum interface_mode_e {
    CAPI_CR_INTERFACE_TYPE,         /**< CR interface type */
    CAPI_KR_INTERFACE_TYPE,         /**< KR interface type */
    CAPI_XX_INTERFACE_TYPE          /**< Count reset type */
} capi_interface_t;                 /**< Interface type */

/**
 * The enumeration of interface mode
 */
typedef enum capi_interface_mode_e {
    CAPI_IEEE_INTERFACE_MODE,       /**< IEEE interface mode */
    CAPI_OTN_INTERFACE_MODE,        /**< OTN interface mode */
    CAPI_HIGIG_INTERFACE_MODE       /**< HIGIG interface mode */
} capi_interface_mode_t;            /**< Interface Mode type */


typedef struct capi_reg_info_s {
    uint32_t reg_address;       /**< Register Address */
    uint32_t content;           /**< Register Value */
} capi_reg_info_t;

/*GP interface interface*/
typedef union capi_lane_status_u {
    uint8_t bytes;

    #ifdef CAPI_IS_BIG_ENDIAN
    struct {
        uint8_t linktrn_err       : 2;   /**< (i) LW channel status: 1-link training error         */
        uint8_t linktrn_done      : 1;   /**< (i) LW channel status: 1-link training done          */
        uint8_t cdrlock           : 1;   /**< (i) LW channel status: 1-CDR locked                  */
        uint8_t tx_ready          : 1;   /**< (i) LW channel status: TX ready                      */
        uint8_t adcclip           : 1;   /**< (i) LW channel status: 1-Dectect the clipping of ADC */
        uint8_t los               : 1;   /**< (i) LW channel status: 1-LOS                         */
        uint8_t plllock           : 1;   /**< (i) LW channel status: 1-PLL locked                  */
    } lw;

    struct {
        uint8_t reserved          : 3;   /**< Reserved                                             */
        uint8_t bh2bh_fifo_status : 1;   /**< BH 2 BH FIFO Status                                  */
        uint8_t pll_unlock_sticky : 1;   /**< PLL Unlock Sticky                                    */
        uint8_t cdr_lock          : 1;   /**< (i) CLIENT channel status: 1-CDR locked              */
        uint8_t los               : 1;   /**< (i) CLIENT channel status: 1-LOS                     */
        uint8_t pll_lock          : 1;   /**< (i) CLIENT channel status: 1-PLL locked              */
    } client;

    #else /* CAPI_IS_LITTLE_ENDIAN */

    struct {
        uint8_t plllock           : 1;   /**< (i) LW channel status: 1-PLL locked                  */
        uint8_t los               : 1;   /**< (i) LW channel status: 1-LOS                         */
        uint8_t adcclip           : 1;   /**< (i) LW channel status: 1-Dectect the clipping of ADC */
        uint8_t tx_ready          : 1;   /**< (i) LW channel status: TX ready                      */
        uint8_t cdrlock           : 1;   /**< (i) LW channel status: 1-CDR locked                  */
        uint8_t linktrn_done      : 1;   /**< (i) LW channel status: 1-link training done          */
        uint8_t linktrn_err       : 2;   /**< (i) LW channel status: 1-link training error         */
    } lw;

    struct {
        uint8_t pll_lock          : 1;   /**< (i) CLIENT channel status: 1-PLL locked              */
        uint8_t los               : 1;   /**< (i) CLIENT channel status: 1-LOS                     */
        uint8_t cdr_lock          : 1;   /**< (i) CLIENT channel status: 1-CDR locked              */
        uint8_t pll_unlock_sticky : 1;   /**< BH 2 BH FIFO Status                                  */
        uint8_t bh2bh_fifo_status : 1;   /**< PLL Unlock Sticky                                    */
        uint8_t reserved          : 3;   /**< Reserved                                             */
    } client;
    #endif /* CAPI_IS_BIG_ENDIAN */
} capi_lane_status_t;

/**
 * The structure contain the chip level information
 */
typedef struct capi_hw_info_s {
    uint32_t chip_id;              /**< Identification                   */
    uint32_t chip_revision;        /**< Revision                         */
    uint32_t rev_id;               /**< Rev ID                           */
} capi_hw_info_t;                /**< Chip Information type            */

/**
 * Structure representing SW information
 */
typedef struct capi_sw_info_s {
    uint32_t fw_version;        /**< FW Version                          */
    uint32_t cmis_version;      /**< CMIS version                        */
    uint16_t capi_major_version; /**< cAPI major version                 */
    uint16_t capi_minor_version; /**< cAPI major version                 */
} capi_sw_info_t;               /**< Software information                */

/**
 * The structure contain the chip level information
 */
typedef struct capi_chip_info_s {

    union {
        uint32_t content;                                   /**< The command options              */

        struct {
#ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved                        : 30; /**< reserved bits                    */
            uint32_t hw_info                         :  1; /**< request for chip information     */
            uint32_t sw_info                         :  1; /**< request for firmware information */
#else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t sw_info                         :  1; /**< request for firmware information */
            uint32_t hw_info                         :  1; /**< request for chip information     */
            uint32_t reserved                        : 30; /**< reserved bits                    */
#endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } param;

    struct {
        capi_hw_info_t      hw_info;                       /**< chip information                 */
        capi_sw_info_t      sw_info;                       /**< firmware status                  */
    } value;
} capi_chip_info_t;                                        /**< Chip Information type            */

/**
 * The enumeration of core version
 */
typedef enum capi_core_version_e {
    CORE_VERSION_A0,        /**< A0 Version */
    CORE_VERSION_NONE
} capi_core_version_t;      /**< Core Version type */

/**
 * The structure of core info
 */
typedef struct capi_core_info_s {
    capi_core_version_t core_version;   /* core version */
    uint32_t serdes_id;                 /* serdes_id */
    uint32_t phy_id0;                   /* phy_id0 */
    uint32_t phy_id1;                   /* phy_id1 */
    char name[30];
} capi_core_info_t;                     /**< Core Info type */


/**
 * The structure of chip status information
 */
typedef struct capi_chip_status_info_s {
    union {
        uint8_t content;                         /**< The content                  */
        struct{
            uint8_t uc_ready      : 1;           /**< Micro Code Ready flag        */
            uint8_t avs_done      : 1;           /**< AVS Ready flag               */
            uint8_t download_done : 1;           /**< Download flag                */
            uint8_t spi_dev_ready : 1;           /**< SPI Device Ready flag        */
            uint8_t reserved      : 4;           /**< Reserved                     */
        } is;
    }param;

    struct{
        uint8_t uc_ready      : 1;               /**< Micro Code Ready value       */
        uint8_t avs_done      : 1;               /**< AVS Ready value              */
        uint8_t download_done : 1;               /**< Download value               */
        uint8_t spi_dev_ready : 1;               /**< SPI Device Ready value       */
        uint8_t reserved      : 4;               /**< Reserved                     */
    } value;
} capi_chip_status_info_t;                       /**< Chip Status Information type */


typedef enum client_rx_tx_e {
    CLIENT_RX,
    CLIENT_TX,
    CLIENT_MAX
} client_rx_tx_t;

typedef enum bh_rx_tx_e {
    LW_RX,
    LW_TX,
    LW_MAX
} lw_rx_tx_t;

/**
 * The enumeration of user command
 */
typedef enum mode_change_command_e {
    MODE_CHG_CMD_SET_ALL_MODES = 0x0,                   /**< Set All Modes (Apply globally, Set lane, Blackhawk and line modes together) */
    MODE_CHG_CMD_SET_PER_PLL_MODES = 0x1,               /**< Set All Modes (Apply globally, apply on per PLL range, such as: lane0 & lane1; lane2 & lane3*/
    MODE_CHG_CMD_SET_LANE_MODE = 0x2,                   /**< Set Lane Mode (Apply globally) */
    MODE_CHG_CMD_SET_BLACKHAWK_MODE = 0x3,              /**< Set Blackhawk Mode (Apply only to Blackhawk Side) */
    MODE_CHG_CMD_SET_LINE_MODE = 0x4,                   /**< Set Line Mode (Apply only to Line side) */

    MODE_CHG_CMD_ASSERT_GLOBAL_LANE_RESET = 0x5,        /**< Assert Global Lane Reset (Apply globally)  */
    MODE_CHG_CMD_RELEASE_GLOBAL_LANE_RESET = 0x6,       /**< Release Global Lane Reset (Apply globally) */
    MODE_CHG_CMD_ASSERT_RESET_BLACKHAWK_LANE = 0x7,     /**< Assert Reset Blackhawk Lane (Rx & Tx) */
    MODE_CHG_CMD_RELEASE_RESET_BLACKHAWK_LANE = 0x8,    /**< Release Reset Blackhawk Lane (Rx & Tx) */
    MODE_CHG_CMD_ASSERT_RESET_LINE_LANE = 0x9,          /**< Assert Reset Line Lane (Rx & Tx) */
    MODE_CHG_CMD_RELEASE_RESET_LINE_LANE = 0xA,         /**< Release Reset Line Lane (Rx & Tx) */

    MODE_CHG_CMD_READ_STATUS_BLACKHAWK_LANE = 0xB,      /**< Read Status Blackhawk Lane (LOS, CDR Lock, PLL Lock, SW State) */
    MODE_CHG_CMD_READ_STATUS_LINE_LANE = 0xC,           /**< Read Status Line Lane (LOS, CDR Lock, PLL Lock, SW State) */

    MODE_CHG_CMD_SUSPEND_LINE_LANE_INGRESS = 0xD,
    MODE_CHG_CMD_RESUME_LINE_LANE_INGRESS = 0xE,
    MODE_CHG_CMD_SUSPEND_LINE_LANE_EGRESS = 0xF,
    MODE_CHG_CMD_RESUME_LINE_LANE_EGRESS = 0x10,
    MODE_CHG_CMD_SUSPEND_DUAL_WRAPPER_INGRESS = 0x11,
    MODE_CHG_CMD_RESUME_DUAL_WRAPPER_INGRESS = 0x12,
    MODE_CHG_CMD_SUSPEND_DUAL_WRAPPER_EGRESS = 0x13,
    MODE_CHG_CMD_RESUME_DUAL_WRAPPER_EGRESS = 0x14,
    MODE_CHG_CMD_SUSPEND_BH_LANE_INGRESS = 0x15,
    MODE_CHG_CMD_RESUME_BH_LANE_INGRESS = 0x16,
    MODE_CHG_CMD_SUSPEND_BH_LANE_EGRESS = 0x17,
    MODE_CHG_CMD_RESUME_BH_LANE_EGRESS = 0x18,
    MODE_CHG_CMD_ENABLE_LW_BH_IGNORE_FAULT = 0x19,
    MODE_CHG_CMD_ENABLE_BH_IGNORE_FAULT = 0x20,
    MODE_CHG_CMD_ENABLE_LW_IGNORE_FAULT = 0x21,

    MODE_CHG_CMD_DISABLE_LW_BH_IGNORE_FAULT = 0x22,
    MODE_CHG_CMD_DISABLE_BH_IGNORE_FAULT = 0x23,
    MODE_CHG_CMD_DISABLE_LW_IGNORE_FAULT = 0x24,
    MODE_CHG_CMD_SET_JFEC_INGRESS_MODE = 0x25,
    MODE_CHG_CMD_SET_JFEC_EGRESS_MODE = 0x26,
    MODE_CHG_CMD_READ_JFEC_INGRESS_PRBS = 0x27,
    MODE_CHG_CMD_READ_JFEC_EGRESS_PRBS = 0x28,

    MODE_CHG_CMD_SET_KP4_CONFIG = 0x29,
    MODE_CHG_CMD_SET_JFEC_INGRESS_PRBS_GEN = 0x2A,
    MODE_CHG_CMD_SET_JFEC_EGRESS_PRBS_GEN = 0x2B,
    MODE_CHG_CMD_SET_JFEC_INGRESS_PRBS_MON = 0x2C,
    MODE_CHG_CMD_SET_JFEC_EGRESS_PRBS_MON = 0x2D,

    MODE_CHG_CMD_GET_LW_STICKY_STATUS = 0x2E,

    MODE_CHG_CMD_SUSPEND_FW = 0x2F,
    MODE_CHG_CMD_RESUME_FW = 0x30,

    MODE_CHG_CMD_SET_SFEC_CONFIG = 0x31,
    MODE_CHG_CMD_READ_SFEC_INGRESS_FRAME = 0x32,
    MODE_CHG_CMD_READ_SFEC_INGRESS_UNCORR_FRAME = 0x33,
    MODE_CHG_CMD_READ_SFEC_INGRESS_CORR0S = 0x34,
    MODE_CHG_CMD_READ_SFEC_INGRESS_CORR1S = 0x35,

    MODE_CHG_CMD_LAST_CMD
} mode_change_command_t;

/**
 * The enumeration of AVS Control
 */
typedef enum capi_avs_ctrl_e {
    CAPI_INTERNAL_AVS_CONTROL,      /**< Internal AVS Control */
    CAPI_EXTERNAL_AVS_CONTROL       /**< External Chip controlls the AVS */
} capi_avs_ctrl_t;                  /**< AVS Control Type */

/**
 * The enumeration of status
 */
typedef enum avs_result_e {
    CAPI_INIT_NOT_STARTED = 0,
    CAPI_INIT_DONE_SUCCESS = 1,     /**< 1 : init success */
    CAPI_INIT_ERROR = 2,            /**< 2 : int fail  */
} avs_result_t;                     /**<  periodical success faile */


/**
 * The enumeration of status
 */
typedef struct capi_avs_status_e {
    avs_result_t avs_result;       /**< AVS status result    */
    uint8_t      avs_enable;       /**< AVS Enabled/Disabled */
} capi_avs_status_t;               /**< AVS Status type      */

/**
 * The structure of AVS Voltage information
 */
typedef struct capi_avs_voltage_info_s {
    uint32_t voltage;               /**< AVS voltage controller    */
} capi_avs_voltage_info_t;          /**< AVS mode config info type */

/**
 * The enumeration of set volatge with or without fw
 */
typedef enum capi_set_voltage_with_fw_e {
    CAPI_SET_VOLTAGE_WITH_FIRMWARE = 0,     /**< Set Voltage with FW      */
    CAPI_SET_VOLTAGE_NO_FIRMWARE = 1        /**< Set Voltage No FW        */
} capi_set_voltage_with_fw_t;               /**< Set Voltage with FW type */


/**
 * The structure of fixed vddm mode configuration information
 */
typedef struct capi_vddm_voltage_config_info_s {
    capi_enable_t              fixed_voltage_enable;      /**< Fixed AVS voltage enable/disable     */
    uint16_t                   fixed_voltage_set;         /**< Fixed AVS voltage setting            */
    uint16_t                   fixed_voltage_read;        /**< Fixed AVS voltage reading            */
    capi_set_voltage_with_fw_t capi_set_voltage_with_fw;  /**< Set voltage with fw                  */ 
} capi_other_voltage_config_info_t;                       /**< Fixed vddm _voltage config info type */

/**
 * The union of chip voltage status register
 */
typedef union chip_voltage_status_reg_u {
    uint16_t voltage;                      /**< AVS Voltage                         */
} capi_voltage_status_info_t;              /**< AVS Voltage Status Information type */


/**< AVS mode config info type */
/**
 * The enumeration of AVS number of packages share AVS
 */
typedef enum capi_avs_pack_share_e {
    CAPI_1_PACKAGE_SHARE_AVS,   /**< 1 : Package share AVS */
    CAPI_2_PACKAGE_SHARE_AVS,   /**< 2 : Packages share AVS */
    CAPI_3_PACKAGE_SHARE_AVS,   /**< 3 : Packages share AVS */
    CAPI_4_PACKAGE_SHARE_AVS,   /**< 4 : Packages share AVS */
    CAPI_5_PACKAGE_SHARE_AVS,   /**< 5 : Packages share AVS */
    CAPI_6_PACKAGE_SHARE_AVS,   /**< 6 : Packages share AVS */
    CAPI_7_PACKAGE_SHARE_AVS,   /**< 7 : Packages share AVS */
    CAPI_8_PACKAGE_SHARE_AVS    /**< 8 : Packages share AVS */
} capi_avs_pack_share_t;        /**< AVS Control Type */

/**
 * The enumeration of AVS board DC margin
 */
typedef enum capi_board_dc_margin_e {
    CAPI_0_MV_MARGIN_ADDED_ON_TOP, /**<  0 :  0 mV margin added on top for board IR drop */
    CAPI_1_MV_MARGIN_ADDED_ON_TOP, /**<  1 :  5 mV margin added on top for board IR drop */
    CAPI_2_MV_MARGIN_ADDED_ON_TOP, /**<  2 : 10 mV margin added on top for board IR drop */
    CAPI_3_MV_MARGIN_ADDED_ON_TOP, /**<  3 : 15 mV margin added on top for board IR drop */
    CAPI_4_MV_MARGIN_ADDED_ON_TOP, /**<  4 : 20 mV margin added on top for board IR drop */
    CAPI_5_MV_MARGIN_ADDED_ON_TOP, /**<  5 : 25 mV margin added on top for board IR drop */
    CAPI_6_MV_MARGIN_ADDED_ON_TOP, /**<  6 : 30 mV margin added on top for board IR drop */
    CAPI_7_MV_MARGIN_ADDED_ON_TOP, /**<  7 : 35 mV margin added on top for board IR drop */
    CAPI_8_MV_MARGIN_ADDED_ON_TOP, /**<  8 : 40 mV margin added on top for board IR drop */
    CAPI_9_MV_MARGIN_ADDED_ON_TOP, /**<  9 : 45 mV margin added on top for board IR drop */
    CAPI_10_MV_MARGIN_ADDED_ON_TOP, /**< 10 : 50 mV margin added on top for board IR drop */
    CAPI_11_MV_MARGIN_ADDED_ON_TOP, /**< 11 : 55 mV margin added on top for board IR drop */
    CAPI_12_MV_MARGIN_ADDED_ON_TOP, /**< 12 : 60 mV margin added on top for board IR drop */
    CAPI_13_MV_MARGIN_ADDED_ON_TOP, /**< 13 : 65 mV margin added on top for board IR drop */
    CAPI_14_MV_MARGIN_ADDED_ON_TOP, /**< 14 : 70 mV margin added on top for board IR drop */
    CAPI_15_MV_MARGIN_ADDED_ON_TOP, /**< 15 : 75 mV margin added on top for board IR drop */
    CAPI_16_MV_MARGIN_ADDED_ON_TOP, /**< 16 : 80 mV margin added on top for board IR drop */
    CAPI_17_MV_MARGIN_ADDED_ON_TOP, /**< 17 : 85 mV margin added on top for board IR drop */
    CAPI_18_MV_MARGIN_ADDED_ON_TOP, /**< 18 : 90 mV margin added on top for board IR drop */
    CAPI_19_MV_MARGIN_ADDED_ON_TOP, /**< 19 : 95 mV margin added on top for board IR drop */
    CAPI_20_MV_MARGIN_ADDED_ON_TOP, /**< 20 : 100 mV margin added on top for board IR drop */
    CAPI_21_MV_MARGIN_ADDED_ON_TOP, /**< 21 : 105 mV margin added on top for board IR drop */
    CAPI_22_MV_MARGIN_ADDED_ON_TOP, /**< 22 : 110 mV margin added on top for board IR drop */
    CAPI_23_MV_MARGIN_ADDED_ON_TOP, /**< 23 : 115 mV margin added on top for board IR drop */
    CAPI_24_MV_MARGIN_ADDED_ON_TOP, /**< 24 : 120 mV margin added on top for board IR drop */
    CAPI_25_MV_MARGIN_ADDED_ON_TOP, /**< 25 : 125 mV margin added on top for board IR drop */
    CAPI_26_MV_MARGIN_ADDED_ON_TOP, /**< 26 : 130 mV margin added on top for board IR drop */
    CAPI_27_MV_MARGIN_ADDED_ON_TOP, /**< 27 : 135 mV margin added on top for board IR drop */
    CAPI_28_MV_MARGIN_ADDED_ON_TOP, /**< 28 : 140 mV margin added on top for board IR drop */
    CAPI_29_MV_MARGIN_ADDED_ON_TOP, /**< 29 : 145 mV margin added on top for board IR drop */
    CAPI_30_MV_MARGIN_ADDED_ON_TOP, /**< 30 : 150 mV margin added on top for board IR drop */
    CAPI_31_MV_MARGIN_ADDED_ON_TOP  /**< 31 : 155 mV margin added on top for board IR drop */
} capi_board_dc_margin_t;           /**< AVS Board DC Margin type */

/**
 * The enumeration of AVS number of packages share AVS
 */
typedef enum capi_voltage_ctrl_e {
    CAPI_VOLTAGE_INCREASE = -1,         /**< 1 : Increase voltage */
    CAPI_VOLTAGE_NO_CHANGE = 0,         /**< 2 : No change */
    CAPI_VOLTAGE_DECREASE = 1           /**< 3 : Decrease voltage */
} capi_voltage_ctrl_t;                  /**< Voltage Controller type */

/**< AVS regulator type */
/**
 * The enumeration of AVS regulator type
 */
typedef enum capi_type_of_regulator_e {
    CAPI_REGULATOR_4677,                /**< 0 : 4677 regulator */
    CAPI_REGULATOR_TPS546, /**<TI TPSM846 */
    CAPI_REGULATOR_7180,                /**< 2 : 7180 regulator */
    CAPI_REGULATOR_TPS544,              /**< 3 : TPS544 regulator */
    CAPI_REGULATOR_MAX77812,            /**< 4 : MAX77812 regulator */
    CAPI_REGULATOR_20730,               /**< 5 : 20730 regulator */
    CAPI_REGULATOR_4678,                /**< LTM4678*/
    CAPI_REGULATOR_INTERNAL,               /**<TI TPSM546 */   
} capi_type_of_regulator_t;             /**< AVS Control Type */

/**
 * The enumeration of avs disable
 */
typedef enum capi_avs_disable_type_e {
    CAPI_AVS_DISABLE_FIRMWARE_CONTROL = 0,              /*!< firmware set back volatge */
    CAPI_AVS_DISABLE_NO_FIRMWARE_CONTROL = 1            /*!<no firmware */
} capi_avs_disable_type_t;                              /*!< Enable/Disable type */

/**
 * The enumeration of external control step
 */
typedef enum capi_avs_external_ctrl_step_e {
    CAPI_AVS_EXTERNAL_CONTROL_STEP_1 = 0,       /*!< step 1*/
    CAPI_AVS_EXTERNAL_CONTROL_STEP_N = 1        /*!<step N */
} capi_avs_external_ctrl_step_t;                /*!< external control step */

/**
 * The structure of AVS mode configuration information
 */
typedef struct capi_avs_mode_config_info_s {
    capi_enable_t avs;                                  /**< AVS enable/disable */
    capi_avs_disable_type_t disable_type;               /**< AVS disable either no firmware control or firmware control*/
    capi_avs_ctrl_t avs_ctrl;                           /**< AVS control */
    capi_avs_pack_share_t package_share;                /**< AVS number of package share AVS */
    capi_board_dc_margin_t board_dc_margin;             /**< Board DC Margin */
    capi_type_of_regulator_t type_of_regulator;         /**< different type of regulator */
    uint8_t   i2c_slave_address[3];
    capi_avs_external_ctrl_step_t external_ctrl_step;
} capi_avs_mode_config_info_t;                          /**< AVS mode config info type */


/**
 * The struct of QSFP I2c slave address 
 */
typedef struct capi_i2c_slave_info_u {
    uint8_t i2c_address;                      /**< i2c_address                         */
} capi_i2c_info_t;              /**< i2c_address info */


typedef struct lane_txpi_override_s {
    uint8_t txpi_override_en;       /**< txpi_override_en */
    int16_t ppm;                    /**< ppm value */
} lane_txpi_override_t;             /**< LANE TXPI OVERRIDE structure */

/**
 * Interface Lane Status Enumeration
 */
typedef enum {
    INTF_LANE_ST_IDLE,                           /**< No port configured                                                  */
    INTF_LANE_ST_IN_PROGRESS,                    /**< Configuration is in progress                                        */
    INTF_LANE_ST_READY,                          /**< Port configured successfully and ready to run                       */
    INTF_LANE_ST_FAIL                            /**< Port configured but failed with either PLL_LOL or ADC_CAL_DONE_FAIL */
} capi_intf_lane_st_t;                           /**< Interface Lane Status type                                          */

typedef enum capi_phy_interface_e {
    CAPI_INTERFACE_Bypass = 0,
    CAPI_INTERFACE_SR,
    CAPI_INTERFACE_SR4,
    CAPI_INTERFACE_KX,
    CAPI_INTERFACE_KX4,
    CAPI_INTERFACE_KR,
    CAPI_INTERFACE_KR2,
    CAPI_INTERFACE_KR4,
    CAPI_INTERFACE_CX,
    CAPI_INTERFACE_CX2,
    CAPI_INTERFACE_CX4,
    CAPI_INTERFACE_CR,
    CAPI_INTERFACE_CR2,
    CAPI_INTERFACE_CR4,
    CAPI_INTERFACE_CR10,
    CAPI_INTERFACE_XFI,
    CAPI_INTERFACE_SFI,
    CAPI_INTERFACE_SFPDAC,
    CAPI_INTERFACE_XGMII,
    CAPI_INTERFACE_1000X,
    CAPI_INTERFACE_SGMII,
    CAPI_INTERFACE_XAUI,
    CAPI_INTERFACE_RXAUI,
    CAPI_INTERFACE_X2,
    CAPI_INTERFACE_XLAUI,
    CAPI_INTERFACE_XLAUI2,
    CAPI_INTERFACE_CAUI,
    CAPI_INTERFACE_QSGMII,
    CAPI_INTERFACE_LR4,
    CAPI_INTERFACE_LR,
    CAPI_INTERFACE_LR2,
    CAPI_INTERFACE_ER,
    CAPI_INTERFACE_ER2,
    CAPI_INTERFACE_ER4,
    CAPI_INTERFACE_SR2,
    CAPI_INTERFACE_SR10,
    CAPI_INTERFACE_CAUI4,
    CAPI_INTERFACE_VSR,
    CAPI_INTERFACE_LR10,
    CAPI_INTERFACE_KR10,
    CAPI_INTERFACE_CAUI4_C2C,
    CAPI_INTERFACE_CAUI4_C2M,
    CAPI_INTERFACE_ZR,
    CAPI_INTERFACE_LRM,
    CAPI_INTERFACE_XLPPI,
    CAPI_INTERFACE_OTN,
    CAPI_INTERFACE_Count
} capi_phy_interface_t;

/*********************************fec set/config  ***********************************/

/*********************************fec get info ***********************************/
#define FEC_FLN_NUM    8
/**
 * The structure for faults on KP4 / PCS
 */
typedef enum capi_link_sts_e {
    CAPI_LINK_STATUS_DOWN    = 0,
    CAPI_LINK_STATUS_UP
} capi_link_sts_t;

typedef struct cw_chip_port_info_s{
    uint8_t cw_chip_port_mask;
    uint8_t cw_chip_bh_lane_mask[4];
    uint8_t cw_chip_lw_lane_mask[4];
    uint16_t cw_chip_serdes_mask;
    uint16_t cw_chip_media_mask;
} cw_chip_port_info_t;

typedef struct cw_chip_port_info_for_mode_s{
    uint8_t cw_chip_mode_t;
    uint8_t cw_chip_port_mask;
    uint8_t cw_chip_bh_lane_mask[4];
    uint8_t cw_chip_lw_lane_mask[4];
} cw_chip_port_info_for_mode_t;

/**
 * Test pattern gen type
 */
typedef enum capi_test_pattern_type_e {
    SSPRQ           = 0,        /**< SSPRQ pattern gen   */
    QPRBS13         = 1,        /**< QPRBS13 pattern gen */
    SQUARE_WAVE     = 2,        /**< SQUARE WAVE pattern gen */
    PCS_SCRM_IDLE   = 3,        /**< PCS Scrambled IDLE pattern */
    KP4_PRBS        = 4,        /**< KP4 PRBS pattern */
    NO_TEST_PATTERN
} capi_test_pattern_type_t;


/**
 * Test pattern configuration structure
 */
typedef struct capi_test_pattern_cfg_s {
    capi_enable_t            enable;        /**< test pattern gen enable. 1 - enable and 0 - disable */
    capi_test_pattern_type_t type;          /**< test pattern type - SSPRQ/QPRBS13 */
    square_wave_pattern_t    sq_patt_mode;  /**< square wave pattern mode */
    uint8_t                  txpi_on;       /**< internal usage*/
} capi_test_pattern_cfg_t;

/*
 * Structure of phy power
 */
typedef struct capi_phy_power_s {
    capi_switch_t rx;      /**< receiver on/off/toggle control    */
    capi_switch_t tx;      /**< transmitter on/off/toggle control */
} capi_phy_power_t;

/**
 * The structure of fixed mode configuration information
 */
typedef struct capi_fixed_voltage_config_info_s {
    capi_enable_t fixed_voltage_enable; /**< enable/disable */    
    capi_type_of_regulator_t type_of_regulator; /**< different type of regulator */
    uint16_t fixed_voltage;   
    avs_result_t set_status;
    
} capi_fixed_voltage_config_info_t; /**< fixed_voltage config info type */


/**
 * The structure of chip voltage
 */
typedef struct capi_chip_voltage_config_info_s {  
   
    uint16_t voltage_dvdd;   
    uint16_t voltage_075pv; /*vddm */
    uint16_t voltage_09pv;  /*Avdd */
    
} capi_chip_voltage_config_info_t; /**< fixed_voltage config info type */

/**
 * The enumeration of FEC Type
 */
typedef union capi_fec_type_e {
   capi_line_fec_type_t line_fec;
   capi_host_fec_type_t host_fec;
} capi_fec_type_t;                      /**< FEC Count */

typedef enum capi_fec_mode_e {
    CAPI_FEC_INGRESS,
    CAPI_FEC_EGRESS,
    CAPI_FEC_LINE,
    CAPI_FEC_CLIENT,
    CAPI_FEC_MODE_MAX
} capi_fec_mode_t;


#define FEC_TOT_BITS_CORR_NUM 2
#define FEC_TOT_FRAMES_ERR_NUM 16

/**
 * The struct of fec status to support both Kp4 and jfec
 */
typedef struct fec_rx_err_cnt_s {
    uint64_t tot_frame_rev_cnt;
    uint64_t tot_frame_corr_cnt;
    uint64_t tot_frame_uncorr_cnt;
    uint64_t tot_symbols_corr_cnt;                          /*!< based on tot_frames_err_cnt[0..15] */
    uint64_t tot_bits_corr_cnt[FEC_TOT_BITS_CORR_NUM];      /*!< [0] for correctable 0's; [1] for correctable 1's */
    uint64_t tot_frames_err_cnt[FEC_TOT_FRAMES_ERR_NUM];    /*!< [n] for number of cw with correstable symbol error=n */
} capi_fec_rx_err_cnt_t;

typedef struct fec_rx_status_s {

    uint16_t igbox_clsn_sticky;      /*!< intf-gbox clsn per fec port - read followed by clear */
    uint16_t am_lolock_sticky;       /*!< am lol per fec lane - read followed by clear */
    uint16_t xdec_gbox_clsn_sticky;  /*!< xdec gearbox collison per fec port - read followed by clear */
    uint16_t xenc_gbox_clsn_sticky;  /*!< xenc gearbox collison per fec port - read followed by clear */
    uint16_t ogbox_clsn_sticky;      /*!< xdec gearbox collison per fec port - read followed by clear */    
    uint16_t fec_algn_lol_sticky;    /*!< alignment lol sticky per fec port - read followed by clear */
    uint16_t fec_algn_stat;          /*!< alignment lock live status */
} capi_fec_rx_status_t;

/**
 * The struct of CW KP4 FEC Error Counter
 */
typedef struct cw_kp4fec_err_cnt_s{
    uint64_t tot_frame_cnt;                              /**<  fec total frame count*/
    uint64_t tot_frame_uncorr_cnt;                       /**<  fec uncorrected frame count*/
    uint64_t tot_bits_corr_cnt[FEC_TOT_BITS_CORR_NUM];   /**<  fec corrected bit0 & bit1 count*/
    uint64_t tot_frames_err_cnt[FEC_TOT_FRAMES_ERR_NUM];        /**<  fec corrected frame count*/
} cw_kp4fec_err_cnt_t;
 

typedef struct capi_fec_ber_s {    
    float pre_fec_ber;              /*!< pre fec BER */
    float post_fec_ber;             /*!< post fec BER */
    float post_fec_ber_proj;        /*!< projected post fec BER */
} capi_fec_ber_t;

/**
 * The struct for dump status
 */
typedef struct fec_dump_status_u {
    capi_fec_rx_status_t      fec_sts;              /*!< output: fec status */
    capi_fec_rx_err_cnt_t     fec_a_err_cnt;        /*!< output: fec err counter struct - should latch/clear/update counters separately */
    capi_fec_rx_err_cnt_t     fec_b_err_cnt;        /*!< output: fec err counter struct - only for 200g / 400g */
    capi_function_mode_t      func_mode;            /*!< output: Function Mode      */
    capi_fec_type_t           fec_type;             /*!< output: FEC protocol type */
    capi_fec_mode_t           fec_mode;             /*!< input : Client / Line side selection */
    capi_fec_ber_t            fec_ber;            /*!< output: FEC BER */

} capi_fec_dump_status_t;

/**
 * The struct of CW KP4 FEC BER
 */
typedef struct cw_kp4fec_ber_s {
   double cur_ber; /**<  current FEC BER */
   double min_ber; /**<  minimum FEC BER */
   double max_ber; /**<  maximum FEC BER */
   double average_ber; /**<  average FEC BER */ 
   double total_ber; /**<  total FEC BER for average calculation*/

} cw_kp4fec_ber_t;

/**
 * The struct of CW KP4 FEC BER & Error COunter information
 */ 
typedef struct cw_kp4fec_ber_state_s {
    capi_fec_mode_t fec_mode;         /**<  input : Client / Line side selection*/
    bool clr_cnt;                     /**<  input : FALSE�don�t clear hardware FEC counter; TRUE�clear hardware FEC counter*/
    bool only_cur;                    /**<  input : 0�cAPI will return cur & min & max & average value; 1�cAPI will only return cur BER and error count*/
   
    uint32_t err_latch_times;         /**< input & output: capi retrieve_fec_ber calling times which will be used to calculate the average value; */
   
    cw_kp4fec_ber_t pre_fec_ber;      /**< input & output: pre FEC BER */
    cw_kp4fec_ber_t post_fec_ber;     /**< input & output: post FEC BER */ 
   
    cw_kp4fec_err_cnt_t cur_err;      /**< input & output: current FEC error count info during interval */
    cw_kp4fec_err_cnt_t min_err;      /**< input & output: minimum FEC error count info during interval */
    cw_kp4fec_err_cnt_t max_err;      /**< input & output: maximum FEC error count info during interval */ 
    cw_kp4fec_err_cnt_t average_err;  /**< input & output: average FEC error count info during interval */
    cw_kp4fec_err_cnt_t backup_err;   /**< input & output: backup last FEC error count info */
    cw_kp4fec_err_cnt_t total_err;    /**< input & output: total FEC error count info */

} capi_kp4fec_ber_state_t;


/**
 * The enumeration of lane data rate Type
 */
typedef enum capi_lane_data_rate_e {
    CAPI_LANE_DATA_RATE_NONE = 0,     /**< Line data rate: None */
    CAPI_LANE_DATA_RATE_1P25 = 1,     /**< Line data rate: 1.25G */
    CAPI_LANE_DATA_RATE_10P3125 = 2,  /**< Line data rate: 10.3125G  */
    CAPI_LANE_DATA_RATE_20P625 = 3,   /**< Line data rate: 20.625G */
    CAPI_LANE_DATA_RATE_25P78125 = 4, /**< Line data rate: 25.78125G  */
    CAPI_LANE_DATA_RATE_26P5625 = 5,  /**< Line data rate: 26.5625G */
    CAPI_LANE_DATA_RATE_28P125 = 7,   /**< Line data rate: 28.125G */
    CAPI_LANE_DATA_RATE_51P5625 = 8,  /**< Line data rate: 51.5625G */
    CAPI_LANE_DATA_RATE_53P125 = 9,   /**< Line data rate: 53.125 */
    CAPI_LANE_DATA_RATE_56P25 = 11,   /**< Line data rate: 56.25G */
    CAPI_LANE_DATA_RATE_MAX = 13
} capi_lane_data_rate_t;              /**< Chip Lane Data Rate type*/


/* Refer to cw_lw_pll_multiplier_t */
typedef enum capi_pll_ratio_list_e {
    CAPI_CW_LW_PLL_MULTIPLIER_170 = 0,   /**< PLL ratio 170 */
    CAPI_CW_LW_PLL_MULTIPLIER_165 = 1,   /**< PLL ratio 165 */
    CAPI_CW_LW_PLL_MULTIPLIER_168 = 2,   /**< PLL ratio 168 */
    CAPI_CW_LW_PLL_TYPE_MAX 
} capi_pll_ratio_type_t;


/*Refer to cw_bh_pll_multiplier_t */
typedef enum capi_bh_pll_ratio_list_e {
    CAPI_CW_BH_PLL_MULTIPLIER_170 = 0,   /*PLL ratio 170*/
    CAPI_CW_BH_PLL_MULTIPLIER_165 = 1,   /*PLL ratio 165*/
    CAPI_CW_BH_PLL_MULTIPLIER_160 = 2,   /*PLL ratio 160*/
    CAPI_CW_BH_PLL_TYPE_MAX
} capi_bh_pll_ratio_type_t;

typedef enum capi_egr_port_type_e {
    EGR_PAM4_RPTR =1,
    EGR_NRZ_RPTR =2,
    EGR_P2N_RGB_BM =3,
    EGR_N2P_FGB_BM =4,
    EGR_N2N_RGB_BM =5,
    EGR_N2N_FGB_BM =6,
    EGR_P2N_RGB_FEC =7,
    EGR_N2P_FGB_FEC =8,
    EGR_PAM4_HMUX =9,
    EGR_NRZ_HMUX =10,
    EGR_P2N_RGB_HMUX =11,
    EGR_N2N_RGB_HMUX =12,
} capi_egr_port_type_t;

typedef enum capi_igr_port_type_e {
    IGR_PAM4_RPTR =1,
    IGR_NRZ_RPTR =2,
    IGR_N2P_FGB_BM =3,
    IGR_P2N_RGB_BM =4,
    IGR_N2N_FGB_BM =5,
    IGR_N2N_RGB_BM =6,
    IGR_N2P_FGB_FEC =7,
    IGR_P2N_RGB_FEC =8,
    IGR_PAM4_RPTR_BCST =9,
    IGR_NRZ_RPTR_BCSM =10,
    IGR_N2P_FGB_FEC_BCST =11,
    IGR_N2N_FGB_BM_BCST =12,
    IGR_NRZ_RPTR_BCST =13/* ?? */
} capi_igr_port_type_t;


typedef enum capi_host_osr_e {
    CAPI_CW_BH_OSR_1 = 1
} capi_host_osr_t;


typedef enum capi_line_osr_e {
    CAPI_CW_LW_OSR_1 = 1,
    CAPI_CW_LW_OSR_2 = 2
} capi_line_osr_t;


typedef enum capi_q0q1_e {
    CW_QUAD_Q0,
    CW_QUAD_Q1
} capi_q0q1_t;


typedef enum capi_host_inst_e {
    CW_INST_HOST_0,
    CW_INST_HOST_1,
} capi_host_inst_t;

typedef enum capi_hostline_e {
    CW_INST_LINE = 0, 
    CW_INST_HOST = 1
} capi_hostline_t;


typedef enum capi_ch_e {
    CW_FEC_CORE_CH0A,
    CW_FEC_CORE_CH0B,
    CW_FEC_CORE_CH1A,
    CW_FEC_CORE_CH1B,
} capi_ch_t;

typedef enum capi_rm_ovrd_type_e {
     CAPI_RM_OVRD_NA = 0,  /*Rate mode overridden is not supported*/
     CAPI_RM_OVRD_NO = 1,  /*Rate mode overridden is not.*/
     CAPI_RM_OVRD_YES = 2,  /*Rate mode overridden .*/
} capi_rm_ovrd_type_t;


typedef enum return_stat_e {
   PORT_NO_MATCH = 0,
   PORT_MATCH_FOUND = 1,
   PORT_MATCH_MULTI = 2,
} return_stat_t;


/**
 * The structure to represent fec monitor info
 */
typedef struct capi_rptr_fec_st_s {
    uint8_t           fec_mode;           /*!< input : Client / Line side selection */
    uint8_t           is_enable;          /*!< input : enable or disable repeater FEC monitor */
    uint8_t           func_mode;          /*!< output: Function Mode      */
    uint8_t           fec_type;           /*!< output: FEC protocol type */
    uint16_t          port_idx;           /*!< input : port index */
} capi_rptr_fec_st_t;                     /*!< Port power down status type */


/**
 * The structure of line side link training status
 */
typedef struct lnktrn_status_s {
    uint8_t lnktrn_en;                   /**< link training enable/disable, refer to capi_enable_t */
    uint8_t lnktrn_frm_lck;              /**< link training frame lock */
    uint8_t lnktrn_recv_rdy;             /**< link training receive ready */
    uint8_t lnktrn_done;                 /**< link training link training done */
    uint8_t lnktrn_err;                  /**< link training link training error code, refer to capi_lnktrn_err_t */
} lnktrn_status_t;                       /**< Link Training Status type */

/**
 * The structure of lane status
 */
typedef struct lane_status_s{
    uint8_t cdr_lock;                    /**< CDR Lock;       1:locked       0:unlocked        */
    uint8_t los;                         /**< Lose of Signal; 1:Signal lost  0:Signal not lost */
    uint8_t tx_squelch;                  /**< tx_squelch      1:Squelched    0:Unsquelched     */
    uint8_t tx_mission;                  /**< tx_mission      1:In Mission   0:Not in Mission  */
    uint8_t rx_mission;                  /**< rx_mission      1:In Mission   0:Not in Mission  */
    uint8_t pll_lock;                    /**< PLL Lock;       1:locked       0:unlocked        */
} lane_status_t;                         /**< Lane status                                      */

/**
 * The structure of Diagnostics Status type
 */
typedef struct diag_status_s{
    uint8_t adc_sig_clipping;            /**< ADC Signal Clipping Detected, line side, refer to capi_adc_sig_clipping_t */
    uint8_t pll_unlock_sticky;           /**< PLL unlock sticky, client side, refer to capi_client_pll_unlock_sticky_t*/
    uint8_t bh2bh_fifo_status;           /**< Bh2bh fifo status, refer to capi_client_bh2bh_fifo_status_t  */
} diag_status_t;                         /**< Diagnostics Status type                                      */

/**
 * The structure of Core Wrapper status type
 */
typedef struct cw_status_s{
    uint16_t egress_sticky;              /**< Egress Sticky status                                    */
    uint16_t ingress_sticky;             /**< Ingress Sticky status                                   */
} cw_status_t;                           /**< Core Wrapper Status type                                */

/**
 * The structure of Sticky Loss status type
 */
typedef struct sticky_loss_s{
    uint8_t  electrical;                                 /**< Electrical Loss Sticky Status            */
    uint8_t  optical;                                    /**< Optical Loss Sticky Status   DSP only    */
    uint8_t  link;                                       /**< Loss of Link (i.e. lol) Sticky status    */
} sticky_loss_t;                                         /**< Sticky Loss type                         */

/**
 * The struct of lane information
 */
typedef struct capi_lane_info_s {
    union {
        uint16_t content;

        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint16_t reserved                      : 8;  /**< Reserved                                 */
            uint16_t lane_status                   : 1;  /**< Lane status (i.e. locks, tx-squelch)     */
            uint16_t lnktrn_status                 : 1;  /**< lnktrn status                            */
            uint16_t reserved2                     : 1;  /**< reserved                                 */
            uint16_t diag_status                   : 1;  /**< diag status                              */
            uint16_t cw_status                     : 1;  /**< CW : FIFO sticky                         */
            uint16_t sticky_loss_electrical        : 1;  /**< Electrical loss sticky:sticky_los_t       */
            uint16_t sticky_loss_optical           : 1;  /**< Optical loss sticky:sticky_los_t DSP only */
            uint16_t sticky_loss_link              : 1;  /**< Loss of Link (lol) sticky:sticky_los_t   */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint16_t sticky_loss_link              : 1;  /**< Loss of Link (lol) sticky:sticky_los_t   */
            uint16_t sticky_loss_optical           : 1;  /**< Optical loss sticky:sticky_los_t DSP only */
            uint16_t sticky_loss_electrical        : 1;  /**< Electrical loss sticky:sticky_los_t       */
            uint16_t cw_status                     : 1;  /**< CW status                                */
            uint16_t diag_status                   : 1;  /**< diag status                              */
            uint16_t reserved2                     : 1;  /** < reserved                                */
            uint16_t lnktrn_status                 : 1;  /**< lnktrn status                            */
            uint16_t lane_status                   : 1;  /**< Lane status (i.e. locks, tx-squelch)     */
            uint16_t reserved                      : 8;  /**< Reserved                                 */
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } param;

    union {
        lane_status_t          lane_status;              /**< Lane Status   (i.e. locks, tx-squelch)   */
        link_training_status_t lnktrn_status;            /**< Link Training Status                     */
        diag_status_t          diag_status;              /**< Diagnostic Status                        */
        cw_status_t            cw_status;                /**< CW Status                                */
        sticky_loss_t          sticky_loss;              /**< Sticky Loss status : sticky_loss_t       */
    } value;
} capi_lane_info_t;                                      /**< Lane Information type                    */


/**
 * CAPI EPDM Config Information enumeration
 */
typedef struct capi_epdm_prbs_cfg_s {
    uint8_t poly;                                                         /**< Poly Type                                 */
    uint8_t invert;                                                       /**< Invert                                    */
} capi_epdm_prbs_cfg_t;                                                   /**< EPDM PRBS Configuration type              */

/**
 * CAPI EPDM Mode Config Set Information enumeration
 */
typedef struct capi_epdm_mode_config_set_info_s {
    uint32_t lane_mask;                                                   /**< Lane Mask                                 */
    uint16_t lane_data_rate;                                              /**< Lane Data Rate                            */
    uint8_t  media_type;                                                  /**< Media type                                */
    uint8_t  lane_fec_type;                                               /**< Lane FEC Type                             */
    uint8_t  lane_modulation;                                             /**< Lane Modulation                           */
    bool     hitless_mux_mode;                                            /**< Hitless Mux Mode                          */
    bool     hybrid_port_mode;                                            /**< Hybrid port mode                          */
} capi_epdm_mode_cfg_set_info_t;                                          /**< EPDM Mode Config Set Information          */


/**
 * CAPI EPDM archive information structure
 */
typedef struct capi_epdm_archive_info_s {

    union {
        uint32_t                           content;                       /**< Flag Content                             */
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved              :24;                           /**< Reserved                                 */
            uint32_t sys_prbs_cfg_tx       : 1;                           /**< PRBS Configuration tx on sys             */
            uint32_t sys_prbs_cfg_rx       : 1;                           /**< PRBS Configuration rx on line            */
            uint32_t line_prbs_cfg_tx      : 1;                           /**< PRBS Configuration tx on line            */
            uint32_t line_prbs_cfg_rx      : 1;                           /**< PRBS Configuration rx on line            */
            uint32_t sys_intf_type         : 1;                           /**< system side interface type               */
            uint32_t line_intf_type        : 1;                           /**< line side interface type                 */
            uint32_t mode_config_info      : 1;                           /**< Mode Configuration Information           */
            uint32_t buffer                : 1;                           /**< Buffer for future use                    */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t buffer                : 1;                           /**< Buffer for future use                    */
            uint32_t mode_config_info      : 1;                           /**< Mode Configuration Information           */
            uint32_t line_intf_type        : 1;                           /**< line side interface type                 */
            uint32_t sys_intf_type         : 1;                           /**< system side interface type               */
            uint32_t line_prbs_cfg_rx      : 1;                           /**< PRBS Configuration rx on line            */
            uint32_t line_prbs_cfg_tx      : 1;                           /**< PRBS Configuration tx on line            */
            uint32_t sys_prbs_cfg_rx       : 1;                           /**< PRBS Configuration rx on sys             */
            uint32_t sys_prbs_cfg_tx       : 1;                           /**< PRBS Configuration tx on sys             */
            uint32_t reserved              :24;                           /**< Reserved                                 */
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } archive;

    /**< Archived Data */
    union {
        capi_epdm_prbs_cfg_t           sys_prbs_cfg_tx[MAX_BH_LANES];     /**< per sys lane prbs config for TX          */
        capi_epdm_prbs_cfg_t           sys_prbs_cfg_rx[MAX_BH_LANES];     /**< per sys lane prbs config for RX          */
        capi_epdm_prbs_cfg_t           line_prbs_cfg_tx[MAX_BH_LANES];    /**< per line lane prbs config for TX         */
        capi_epdm_prbs_cfg_t           line_prbs_cfg_rx[MAX_BH_LANES];    /**< per line lane prbs config for RX         */
        uint8_t                        sys_interface_type[MAX_BH_LANES];  /**< per lane interface type                  */
        uint8_t                        line_interface_type[MAX_BH_LANES]; /**< per lane interface type                  */
        capi_epdm_mode_cfg_set_info_t  mode_config_info;                  /**< per lane required for system side info   */
        uint8_t                        buffer[64];                        /**< buffer for tuture use                    */
    } value;                                                              /**< Content/Value of the archived parameter  */

} capi_epdm_archive_info_t;                                               /**< EPDM Archive Information type            */

/**
 * EPDM archive structure - FW only
 * Firmware uses this structure to store the EPDM data in the SRAM
 */
typedef struct fw_epdm_archive_storage_s {

    union {
        uint32_t                           content;                       /**< Flag Content                             */
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved              :24;                           /**< Reserved                                 */
            uint32_t buffer                : 1;                           /**< Buffer for future use                    */
            uint32_t mode_config_info      : 1;                           /**< Mode Configuration Information           */
            uint32_t line_intf_type        : 1;                           /**< line side interface type                 */
            uint32_t sys_intf_type         : 1;                           /**< system side interface type               */
            uint32_t line_prbs_cfg_rx      : 1;                           /**<  PRBS Configuration rx on line           */
            uint32_t line_prbs_cfg_tx      : 1;                           /**<  PRBS Configuration tx on line           */
            uint32_t sys_prbs_cfg_rx       : 1;                           /**<  PRBS Configuration rx on line           */
            uint32_t sys_prbs_cfg_tx       : 1;                           /**<  PRBS Configuration tx on sys            */
            #else /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t sys_prbs_cfg_tx       : 1;                           /**<  PRBS Configuration tx on sys            */
            uint32_t sys_prbs_cfg_rx       : 1;                           /**<  PRBS Configuration rx on line           */
            uint32_t line_prbs_cfg_tx      : 1;                           /**<  PRBS Configuration tx on line           */
            uint32_t line_prbs_cfg_rx      : 1;                           /**<  PRBS Configuration rx on line           */
            uint32_t sys_intf_type         : 1;                           /**< system side interface type               */
            uint32_t line_intf_type        : 1;                           /**< line side interface type                 */
            uint32_t mode_config_info      : 1;                           /**< Mode Configuration Information           */
            uint32_t buffer                : 1;                           /**< Buffer for future use                    */
            uint32_t reserved              :24;                           /**< Reserved                                 */
            #endif /*CAPI_IS_BIG_ENDIAN*/
        } is;
    } archive;

    /**< Archived Data */
    capi_epdm_prbs_cfg_t           sys_prbs_cfg_tx[MAX_BH_LANES];     /**< per sys lane prbs config for TX          */
    capi_epdm_prbs_cfg_t           sys_prbs_cfg_rx[MAX_BH_LANES];     /**< per sys lane prbs config for RX          */
    capi_epdm_prbs_cfg_t           line_prbs_cfg_tx[MAX_BH_LANES];    /**< per line lane prbs config for TX         */
    capi_epdm_prbs_cfg_t           line_prbs_cfg_rx[MAX_BH_LANES];    /**< per line lane prbs config for RX         */
    uint8_t                        sys_interface_type[MAX_BH_LANES];  /**< per lane interface type                  */
    uint8_t                        line_interface_type[MAX_BH_LANES]; /**< per lane interface type                  */
    capi_epdm_mode_cfg_set_info_t  mode_config_info;                    /**< per lane required for system side info   */
    uint8_t                        buffer[64];                          /**< buffer for tuture use                    */

} fw_epdm_archive_storage_t;                                                 /**< EPDM Archive Information type            */


/**
 * CAPI Archive Information type enumeration
 */
typedef enum capi_archive_type_e {
    CAPI_ARCHIVE_INFO_EPDM_TYPE = 1                                      /**< Archive Information Type EPDM            */
} capi_archive_type_t;                                                   /**< Archive Information type                 */

/**
 * CAPI archive information structure
 */
typedef struct capi_archive_info_s {

    uint32_t                            type;                            /**< Archive Information Type, refer capi_archive_type_e */

    union {
        capi_epdm_archive_info_t        epdm_archive_info;                /**< FW init params required during by FW     */
    } element;                                                            /**< Content/Value of the archived parameter  */

} capi_archive_info_t;                                                    /**< Archive Information type                 */


/**
 * The structure contains lw CMIS internal information
 */
typedef struct capi_cmis_info_s {
    int16_t  slicer_threshold[3];                     /**< FFE slicer threshold      */
    int16_t  slicer_target[4];                        /**< FFE slicer symbol value   */
    float    slicer_mean_value[4];                    /**< slicer mean value         */    
    float    slicer_sigma_sqr_value[4];               /**< slicer sigma square value */
    uint32_t slicer_p_value[4];                       /**< slicer P value            */
    int16_t  slicer_p_location[4];                    /**< slicer P location         */
    uint32_t slicer_v_value[3];                       /**< slicer V value            */
    int16_t  slicer_v_location[3];                    /**< slicer V location         */
} capi_cmis_info_t;                                   /**< Core Info type            */


/**
 * The structure contains DSP slicer historgram internal information
 */
typedef struct capi_shstgrm_info_s {
    uint32_t shist_sel;                               /**< slicer histogram traffic source type         */
    uint32_t shist_loc;                               /**< slicer histogram location            */
} capi_shstgrm_info_t;                                   /**< slicer histograme type            */

#define HW_MUTEX_INVALID 0xFFFFFFFF

typedef enum poll_feature_s {
    SNR_POLL = 1
} poll_feature_t;

/**
 * The structure for memory information
 */
typedef struct memory_info_s {
    uint32_t    ref_address;           /**< [In/Output] SRAM reference address               */
    uint32_t    ref_data_len;          /**< [In/Output] length of the data                   */
    uint32_t    hw_mutex;              /**< [In/Output] HW mutex for the memory region,      */
} memory_info_t;                    /**< Memory Information type                          */

/**
 * The structure for CMIS SNR LTP values
 */
typedef struct capi_cmis_snr_ltp_s {
    float cmis_snr_value;                    /**< CMIS SNR value */
    float cmis_ltp_value;                    /**< CMIS LTP value */
    float brcm_snr_value;                    /**< Broadcom SNR value */
} capi_cmis_snr_ltp_t;                     /**< CAPI CMIS SNR LTP Payload type                                       */

/**
 * The structure for fast data access feature, contains input poll configuration and the output payload
 */
typedef struct memory_data_info_s {
    memory_info_t    memory_info;                /**< [In/Output] Memory details, returned by FW                            */
    
    union {
        struct {
            uint32_t start_stop;                 /**< [Input]  1=start : 0=stop. Refer to capi_turn_onoff_t enum            */
            uint32_t poll_interval;              /**< [Input]  polling timer in milliseconds. please refer                  */
                                                 /**<          to the user programming guidelines for the max/min pooling   */
                                                 /**<          intervals for each particular feature                        */
        } command;

        capi_cmis_snr_ltp_t lane_data[MAX_LW_LANES];/**< [Output] Lane based retrieved data payload                            */

                                                 /**< [Output] Other feature payload shall be added here                    */
    } param;
} memory_data_info_t;                            /**< Fast access memory Information                                        */

/**
 * The struct definition for MPI configuration
 */
typedef struct dsp_mpi_cfg_info_s {
    uint8_t         mpi_enable;           /**<MPI check enable/disable [0, 1] 0-disable MPI, 1; 1-enable MPI */
    uint8_t         mpi_interval;         /**<MPI update interval (unit: seconds ) [0, 15] 0-FW default, 1s; 1~15 configurable value*/
    float           ratio_threshold;      /**<MPI indicator ratio threshold [1.00, 2.00] 0-FW default 1.30, */
} dsp_mpi_cfg_info_t;      /**< DSP MPI configuration information define                      */


/**
 * The struct definition for MPI state
 */
typedef struct dsp_mpi_st_info_s {
    uint8_t        read_clear;              /**<[input] 0-read only ; 1-read & clear */
    uint8_t        dust_indicator;          /**<MPI dust indicator state in last cycle */
    uint8_t        dust_indicator_sticky;   /**<MPI dust indicator sticky state. when read_clear is 1, after reporting sticky state, it will be cleared; */
    float          mpi_metric;              /**<MPI metric as: new_slope + 0.25*new_c0. */
    float          mpi_normalized_s[4];     /**<MPI Sorted Normalized standard deviation of level-0 */
    float          mpi_slope;               /**<MPI Slope of the linear fit of the four normalized variances */
    float          mpi_intercept;           /**<MPI Intercept of the linear fit of the four normalized variances */
} dsp_mpi_st_info_t;      /**< DSP MPI state information define                      */


/**
 * The struct definition for MPI configuration in steady state
 */
typedef struct dsp_mission_mpi_cfg_info_s {
    uint8_t        mpi_enable;          /**<steady state MPI check enable/disable [0, 1] 0-disable MPI, 1; 1-enable MPI, default is enabled */
    uint8_t        mpi_chk_th;          /**<steady state MPI check threshold, default is 11, with 9,10,11,12,13 and 14 as settable options */
    uint16_t       mpi_chk_cnt;         /**<steady state MPI check counte default is 512, settable option are 128, 256, 512, 1024 */
} dsp_mission_mpi_cfg_info_t;      /**< DSP MPI configuration information in steady state state define                      */

/**
 * The structure contains serdes diagnostic information
 */
typedef struct serdes_diag_info_s {
    uint8_t signal_detect;                      /**< signal detect status of the lane                      */
    uint8_t pmd_mode;                           /**< modulation type of the lane                           */
    uint8_t osr_mode;                           /**< osr mode of the lane                                  */
    uint8_t rx_lock;                            /**< Rx CDR lock status of the lane                        */
    
    int16_t rx_ppm;                             /**< Rx ppm value of the lane                              */
    int16_t tx_ppm;                             /**< Tx ppm value of the lane                              */
    uint32_t link_time;                         /**< Time it takes for link to establish (in milliseconds) */
    int16_t txfir_pre2;                         /**< TxFiR Pre2 value of the lane                          */
    int16_t txfir_pre;                          /**< TxFiR Pre1 value of the lane                          */   
    int16_t txfir_main;                         /**< TxFiR main value of the lane                          */
    int16_t txfir_post1;                        /**< TxFiR post1 value of the lane                         */  
    int16_t txfir_post2;                        /**< TxFiR post2 value of the lane                         */
    int16_t txfir_post3;                        /**< TxFiR post3 value of the lane                         */
    eye_margin_estimate_t eye_margin_estimate;  /**< Eye margins (left, right, upper and lower)            */

    uint8_t vga;                                /**< VGA value of the lane                                 */
    int8_t dc_offset;                           /**< DC offset value of the lane                           */
    int8_t pf_main;                             /**< PF main value of the lane                             */
    int8_t dfe1;                                /**< DFE1 value of the lane                                */
    int8_t dfe2;                                /**< DFE2 value of the lane                                */
    int8_t dfe3;                                /**< DFE3 value of the lane                                */
    int8_t dfe4;                                /**< DFE4 value of the lane                                */
    int8_t dfe5;                                /**< DFE5 value of the lane                                */
    int8_t dfe6;                                /**< DFE6 value of the lane                                */
    uint8_t padding[3];                         /**< Padding for structure to be 32 bit aligned            */

} serdes_diag_info_t; /**< Serdes diagnostic information type */

/**
 * The structure for Diag Command Request Information
 */
typedef struct capi_diag_command_request_info_s {
    uint16_t command_id;                  /**< Command identifier; refer to phy_command_id_t                  */

    union {
        memory_data_info_t          cmis_snr_ltp;         /**< CMIS SNR LTP                                   */
        dsp_mpi_cfg_info_t          mpi_cfg;              /**< MPI config information                         */
        dsp_mpi_st_info_t           mpi_st;               /**< MPI state information                          */
        dsp_mission_mpi_cfg_info_t  mission_mpi_cfg;      /**< steady state MPI config information            */
        dsp_mpi_st_info_t           mission_mpi_st;       /**< steady state MPI state information             */
        serdes_diag_info_t          serdes_diag_info;     /**< serdes diag info                               */       
    } type;                               /**< Payload/data type of the command Id                            */

} capi_diag_command_request_info_t;       /**< CAPI Diag Command Request Information                          */

/**
 * The structure of command information 
 */
typedef struct capi_command_info_s {
    uint32_t command_id;                                        /**< Command Identifier, refer phy_command_id_t */

    union {
        tx_driver_t                       tx_driver;                      /**< Tx Driver                             */
        txpi_override_t                   txpi_ovrd;                      /**< TxPI Override                         */
        capi_polarity_info_t              polarity_info;                  /**< Polarity Info                         */
        capi_lane_ctrl_info_t             lane_ctrl_info;                 /**< Lane Control Info                     */
        capi_config_info_t                config_info;                    /**< Config Info                           */
        capi_loopback_info_t              lpbk_ctrl_info;                 /**< Loopback Control Info                 */
        capi_prbs_info_t                  prbs_info;                      /**< PRBS Info for set/get PRBS info       */
        capi_prbs_status_t                prbs_status_info;               /**< PRBS Status Info                      */
        capi_prbs_err_inject_t            prbs_err_inj_info;              /**< PRBS Error Injection Info             */
        capi_diag_lane_status_t           diag_lane_status;               /**< Diag Lane Status                      */
        capi_rptr_fec_st_t                rptr_fec_st;                    /**< repeater mode FEC statistics          */
        capi_lane_info_t                  lane_info;                      /**< Lane info                             */
        capi_lane_config_info_t           lane_config_info;               /**< Lane Config Info                      */
        capi_archive_info_t               archive_info;                   /**< Archive info                          */
        uint8_t                           optrxlos_host_fast_tx_squelch;  /**< 0 : Disable; other values not valid   */
        capi_lane_cdr_mode_t              serdes_lane_cdr_mode;           /**< Serdes lane CDR mode                  */
        uint8_t                           serdes_lane_tuning_control;     /**< Serdes lane tuning control            */
        capi_cmis_info_t                  cmis_diag_info;                 /**< Usr Diag info                         */
        capi_power_info_t                 dsp_power_info;                 /**< DSP: Power Information                */
        capi_shstgrm_info_t               shistogram_info;                /**< DSP slicer histogram information      */

        capi_diag_command_request_info_t  diag_cmd_req_info;        /**< diag command request info             */
        uint8_t                     payload[499];                   /**< Payload                               */
    } type;                                                         /**< Payload/data type of the command Id   */

} capi_command_info_t;                                              /**< Command information type              */

/**
 * The enumeration of optical_or_electric_mode
 */
typedef enum capi_dsp_mode_type_e {
    CAPI_DSP_OPTICAL_MODE,    /**< Optical bench mode */
    CAPI_DSP_ELECTRIC_MODE    /**< Electric bench mode */
} capi_dsp_mode_type_t;            /**< bench mode type */

/**
 * The enumeration of Test Chip TX mode type
 */
typedef enum capi_dsp_tc_tx_type_e {
    CAPI_DSP_TC_TX_DIFFERENTIAL_MODE,    /**< Default: Differential mode */
    CAPI_DSP_TC_TX_SE_MODE               /**< Single End mode */
} capi_dsp_tc_tx_type_t;            /**< Test Chip TX mode type */


/**
 * User Configuration information structure
 */

/**
 * AVS Default Information structure
 */
typedef struct avs_default_info_s {    
    union {
           uint32_t       content;                   /**< Flag Content                           */
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved        :31;            /**< Reserved                               */  
            uint32_t avs_switch      : 1;            /**< AVS switch on/off                      */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t avs_switch      : 1;            /**< AVS switch on/off                      */
            uint32_t reserved        :30;            /**< Reserved                               */  
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } feature;
    struct {
        uint32_t avs_switch;                         /**< AVS switch; refer to capi_turn_onoff_t */
    } default_config;
} avs_default_info_t;


/**
 * Internal Regulator Default Information structure
 */
typedef struct int_reg_default_info_s {    
    union { 
           uint32_t       content;                      /**< Flag Content                                          */
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved           :31;            /**< Reserved                                              */  
            uint32_t int_reg_switch     : 1;            /**< Internal Regulator Switch  on/off                     */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t int_reg_switch     : 1;            /**< Internal Regulator Switch  on/off                     */
            uint32_t reserved           :31;            /**< Reserved                                              */  
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } feature;
    struct {
        uint32_t int_reg_switch;                        /**< Internal REgulator switch, refer to capi_turn_onoff_t */
    } default_conifg;
} int_reg_default_info_t;

/**
 * Chip Default Information structure
*/
typedef struct chip_default_info_s {
    union {
           uint32_t        content;                 /**< Content of the flags              */
        struct {
            #ifdef CAPI_IS_BIG_ENDIAN
            uint32_t reserved           :30;        /**< Reserved                          */  
            uint32_t internal_regulator : 1;        /**< Internal Regulator                */
            uint32_t avs                : 1;        /**< AVS module                        */
            #else  /* CAPI_IS_LITTLE_ENDIAN */
            uint32_t avs                : 1;        /**< AVS module                        */
            uint32_t internal_regulator : 1;        /**< Internal Regulator                */
            uint32_t reserved           :30;        /**< Reserved                          */  
            #endif /* CAPI_IS_BIG_ENDIAN */
        } is;
    } module;

    struct {
        avs_default_info_t          avs_default;    /**< AVS Default Configuration Setting */
        int_reg_default_info_t      int_regulator;  /**< Internal Regulator                */
    } config;
    
} capi_chip_default_info_t;                


return_result_t capi_set_chip_default_info(phy_info_t* phy_info_ptr, capi_chip_default_info_t* chip_default_info_ptr);
#ifdef __cplusplus
}
#endif

#endif /*CAPI_DEF_H*/

