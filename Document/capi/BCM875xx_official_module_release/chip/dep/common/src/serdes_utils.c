/**
 *
 * @file     serdes_utils.c 
 * @author   HSIP FW
 * @date     8/5/2020
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2021 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "type_defns.h"
#include "common_def.h"
#include "estoque2_regs.h"
#include "serdes_config.h"
#include "serdes_utils.h"

/**
 * @brief serdes_util_get_base_address(core_ip_t core_ip)
 *
 * Config  Based on the core IP it returns the serdes register offset
 *
 * @param  core_ip
 *
 * @return Base address offset
 */
uint32_t serdes_util_get_base_address(core_ip_t core_ip)
{
    uint32_t base_addr = SERDES_HOST_SIDE_ADDRESS; /*CORE_IP_HOST_SERDES*/

    if (core_ip == CORE_IP_MEDIA_SERDES) {
        base_addr = SERDES_MEDIA_SIDE_ADDRESS;
    }
    return(base_addr);
}
