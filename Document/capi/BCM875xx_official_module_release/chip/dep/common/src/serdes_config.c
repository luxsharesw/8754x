/**
 *
 * @file     serdes_config.c 
 * @author   HSIP FW
 * @date     8/5/2020
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2021 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#include "type_defns.h"
#include "estoque2_regs.h"
#include "serdes_config.h"

/**< Serdes base address per line id */
const ubaddr_t serdes_lane_bbaddr[SERDES_LANE_NUM_MAX] = { BLACKHAWK_LANE_REGS_CH0,    /**< Serdes Lane Channel 0 Register Address */
                                                           BLACKHAWK_LANE_REGS_CH1,    /**< Serdes Lane Channel 1 Register Address */
                                                           BLACKHAWK_LANE_REGS_CH2,    /**< Serdes Lane Channel 2 Register Address */
                                                           BLACKHAWK_LANE_REGS_CH3    /**< Serdes Lane Channel 3 Register Address */
                                                          };   
