/****************************************************************************
*
*     Copyright (c) 2016 Broadcom Limited
*           All Rights Reserved
*
*     No portions of this material may be reproduced in any form without the
*     written permission of:
*
*           Broadcom Limited 
*           1320 Ridder Park Dr.
*           San Jose, California 95131
*           United States
*
*     All information contained in this document is Broadcom Limited 
*     company private, proprietary, and trade secret.
*
****************************************************************************/
/**
 *        Common header for estoque register access 
 *
 * @file regs_common.h
 * @author firmware
 * @date 12-6-2015
 * @version 1.0
 * @brief This header file includes all register access needed header file

*/
#ifndef REGS_COMMON_H
#define REGS_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include "access.h"
#include "type_defns.h"
#include "estoque2_regs.h"
#include "estoque2_pp_regs.h"
#include "estoque2_fw_regs.h"
#include "fw_gp_reg_map.h"
#include "common_def.h"

#ifdef __cplusplus
}
#endif
#endif
