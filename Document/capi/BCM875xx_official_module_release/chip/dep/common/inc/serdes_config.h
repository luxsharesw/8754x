/**
 *
 * @file     serdes_config.h 
 * @author   HSIP FW
 * @date     8/5/2020
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2021 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */
#ifndef SERDES_CONFIG_H
#define SERDES_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * The Symbol Definitions
 */
#define SERDES_LANE_NUM_MAX               4                                  /**< Serdes Total Number of lanes: Barchetta2 = 8, Centenario = 8         */
#define SERDES_LANE_NUM_PER_CORE          4                                  /**< Serdes Nuber of Lanes per Core : Barchetta2 = 4, Centenario = 8      */
#define SERDES_HOST_SIDE_ADDRESS          HOST_BASE                          /**< Serdes Host Side Base Address                                        */ 
#define SERDES_MEDIA_SIDE_ADDRESS         MEDIA_BASE                         /**< Serdes Media Side Base Address                                       */ 

extern const ubaddr_t serdes_lane_bbaddr[SERDES_LANE_NUM_MAX];               /**< serdes base address per line id                                      */

#ifdef __cplusplus
}
#endif

#endif /* SERDES_CONFIG_H */

