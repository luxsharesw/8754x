/**
 *
 * @file chip_mode_def.h
 * @author  
 * @date     09/01/2018
 * @version 1.0
 *
 * $Copyright: Copyright 2018 Broadcom INC.
 * This program is the proprietary software of Broadcom INC
 * and/or its licensors, and may only be used, duplicated, modified
 * or distributed pursuant to the terms and conditions of a separate,
 * written license agreement executed between you and Broadcom
 * (an "Authorized License").  Except as set forth in an Authorized
 * License, Broadcom grants no license (express or implied), right
 * to use, or waiver of any kind with respect to the Software, and
 * Broadcom expressly reserves all rights in and to the Software
 * and all intellectual property rights therein.  IF YOU HAVE
 * NO AUTHORIZED LICENSE, THEN YOU HAVE NO RIGHT TO USE THIS SOFTWARE
 * IN ANY WAY, AND SHOULD IMMEDIATELY NOTIFY BROADCOM AND DISCONTINUE
 * ALL USE OF THE SOFTWARE.
 * 
 * Except as expressly set forth in the Authorized License,
 * 
 * 1.      This program, including its structure, sequence and organization,
 * constitutes the valuable trade secrets of Broadcom, and you shall use
 * all reasonable efforts to protect the confidentiality thereof,
 * and to use this information only in connection with your use of
 * Broadcom integrated circuit products.
 * 
 * 2.      TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS
 * PROVIDED "AS IS" AND WITH ALL FAULTS AND BROADCOM MAKES NO PROMISES,
 * REPRESENTATIONS OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY,
 * OR OTHERWISE, WITH RESPECT TO THE SOFTWARE.  BROADCOM SPECIFICALLY
 * DISCLAIMS ANY AND ALL IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY,
 * NONINFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF VIRUSES,
 * ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR
 * CORRESPONDENCE TO DESCRIPTION. YOU ASSUME THE ENTIRE RISK ARISING
 * OUT OF USE OR PERFORMANCE OF THE SOFTWARE.
 * 
 * 3.      TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 * BROADCOM OR ITS LICENSORS BE LIABLE FOR  CONSEQUENTIAL,
 * INCIDENTAL, SPECIAL, INDIRECT, OR EXEMPLARY DAMAGES WHATSOEVER
 * ARISING OUT OF OR IN ANY WAY RELATING TO YOUR USE OF OR INABILITY
 * TO USE THE SOFTWARE EVEN IF BROADCOM HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES; OR (ii) ANY AMOUNT IN EXCESS OF
 * THE AMOUNT ACTUALLY PAID FOR THE SOFTWARE ITSELF OR USD 1.00,
 * WHICHEVER IS GREATER. THESE LIMITATIONS SHALL APPLY NOTWITHSTANDING
 * ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.$
 *
 * @brief   this file includes cHAL function implementation.
 *
 * @section
 * 
 */
#ifndef CHIP_MODE_DEF_H
#define CHIP_MODE_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_PORT            4                          /**< Max number of ports                                            */
#define MAX_LW_LANES        4                          /**< Max number of lanes on the Line Wrapper side                   */
#define MAX_CLIENT_LANES    4                          /**< Max number of lanes on the Client side                         */

#define NEW_CMODE_NUM   4
#define NEW_CMODE_MIN   12
#define NEW_CMODE_MAX   (NEW_CMODE_MIN + NEW_CMODE_NUM)

typedef enum ref_clk_freq_e {
    CHIP_REF_CLK_156_25_MHZ,             /*!< Chip reference clock at 156.25 Mhz      */
    CHIP_REF_CLK_106_50_MHZ,             /*!< Chip reference clock at 106.25 Mhz      */
    CHIP_REF_CLK_122_88_MHZ,             /*!< Chip reference clock at 122.88 Mhz      */
    CHIP_REF_CLK_212_50_MHZ,             /*!< Chip reference clock at 212.50 Mhz      */
/*  CHIP_REF_CLK_166_015625_MHZ,*/       /*!< Chip reference clock at 166.015625 Mhz  */
/*  CHIP_REF_CLK_625_MHZ,*/              /*!< Chip reference clock at 625 Mhz         */
    CHIP_REF_CLK_MAX                     /*!< Enum size holder                        */
} ref_clk_freq_t;                        /*!< Reference Clock Frequency Types         */

/**
 * The enumeration of enabled txfir taps setting
 */
typedef enum txfir_taps_e {
    TXFIR_TAPS_NRZ_LP_3TAP,              /**< TxFIR taps: NRZ 3 tap                    */
    TXFIR_TAPS_NRZ_6TAP,                 /**< TxFIR taps: NRZ 6 tap                    */
    TXFIR_TAPS_PAM4_LP_3TAP,             /**< TxFIR taps: PAM4 3 tap                   */
    TXFIR_TAPS_PAM4_6TAP,                /**< TxFIR taps: PAM4 6 tap                   */
    TXFIR_TAPS_7TAP,                     /**< TxFIR taps: 7 tap  (DSP only)            */
    TXFIR_TAPS_12TAP,                    /**< TxFIR taps: 12 tap  (DSP only)            */
} txfir_taps_t;                          /**< TxFIR taps type                          */

/**
 * The structure of TxFIR info
 */
typedef struct txfir_info_s {
    uint8_t      numb_of_taps;          /**< Enabled TxFIR taps : refer to txfir_taps_t                */
    int16_t      tap[12];                /**< TxFIR tap[pre2, pre1, main, post1, post2, post3, post4]   */
} txfir_info_t;                         /**< TxFIR Info type                                           */

/*Chip mode enum */
enum cw_chip_mode_e {
    CHIP_MODES_NONE                 = 0,
    CHIP_MODE_4X53G_PAM4_4X53G_PAM4 = 1,
    CHIP_MODE_2X53G_PAM4_2X53G_PAM4 = 2,
    CHIP_MODE_4X25G_NRZ_4X25G_NRZ   = 3,
    CHIP_MODE_1X53G_PAM4_1X53G_PAM4 = 4,
    CHIP_MODE_4X26G_NRZ_2X53G_PAM4  = 5,
    CHIP_MODE_2X26G_NRZ_1X53G_PAM4  = 6,
    CHIP_MODE_2X25G_NRZ_1X50G_PAM4  = 7,
    CHIP_MODE_4X25G_NRZ_2X50G_PAM4  = 8,
    CHIP_MODE_1X10G_NRZ_1X10G_NRZ   = 9,
    CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1 = 10,
    CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1 = 11,
    CHIP_MODE_4X26G_NRZ_4X26G_NRZ = 12,
    CHIP_MODE_1X25G_NRZ_1X25G_NRZ = 13,
    CHIP_MODE_2X26G_NRZ_2X26G_NRZ = 14,
    CHIP_MODE_2X25G_NRZ_2X25G_NRZ = 15,
    CHIP_MODE_2X25G_NRZ_1X53G_PAM4 = 16,
    CHIP_MODE_4X25G_NRZ_2X53G_PAM4 = 17,
    CHIP_MODE_4X50G_PAM4_4X50G_PAM4 = 18,    
    CHIP_MODE_2X25G_NRZ_1X53G_PAM4_M1 = 19,
    CHIP_MODE_4X25G_NRZ_4X25G_PAM4    = 20,    
    CHIP_MODE_2X50G_PAM4_2X50G_PAM4   = 21,
    CHIP_MODE_1X50G_PAM4_1X50G_PAM4   = 22,
    CHIP_MODE_4X10G_NRZ_2X20G_NRZ     = 23,    
    CHIP_MODE_4X10G_NRZ_4X10G_NRZ     = 24,    
    CHIP_MODE_MAX_MODE,
};

typedef uint8_t cw_chip_mode_t;  /* force the enum to 1 bytes*/

/*chip port mode related defnition*/
typedef enum chip_bh_baud_rate_e {
    CHIP_BH_BR_53_125 = 0,
    CHIP_BH_BR_51_5625,
    CHIP_BH_BR_25_78125,
    CHIP_BH_BR_26_5625,
    CHIP_BH_BR_10_3125,
    CHIP_BH_BR_20_625,
    CHIP_BH_BR_1_25,    
    CHIP_BH_BR_MAX
} chip_bh_baud_rate_t;

typedef enum chip_modulation_e {
    CHIP_MOD_NRZ,
    CHIP_MOD_PAM4
} chip_modulation_t;

typedef enum chip_bh_osr_e {
    CHIP_BH_OSR_1 = 0,
    CHIP_BH_OSR_2 = 1,
    CHIP_BH_OSR_16_5 = 2,
    CHIP_BH_OSR_MAX = 3
} chip_bh_osr_t;


typedef enum chip_bh_pll_multiplier_e {
    CHIP_BH_PLL_MULTIPLIER_170,
    CHIP_BH_PLL_MULTIPLIER_165,
    CHIP_BH_PLL_MULTIPLIER_160,
    CHIP_BH_PLL_MULTIPLIER_132,
    CHIP_BH_PLL_MULTIPLIER_MAX
} chip_bh_pll_multiplier_t;


typedef enum chip_bh_vco_rate_e {
    CHIP_BH_VCO_26P5625,
    CHIP_BH_VCO_25P78125,
    CHIP_BH_VCO_20P625,
    CHIP_BH_VCO_MAX
} chip_bh_vco_rate_t ;

typedef struct chip_bh_config_s {
    uint16_t baud_rate  : 4;
    uint16_t modulation : 1;
    uint16_t osr        : 3;
    uint16_t multiplier : 4;
    uint16_t vco_rate   : 4;
    uint16_t lane_mask;
} chip_bh_config;

typedef enum cw_lw_baud_rate_e {
    CHIP_LW_BR_53_125 = 0,
    CHIP_LW_BR_51_5625,
    CHIP_LW_BR_25_78125,
    CHIP_LW_BR_26_5625,
    CHIP_LW_BR_10_3125,
    CHIP_LW_BR_20_625,
    CHIP_LW_BR_1_25,    
    CHIP_LW_BR_MAX
} cw_lw_baud_rate_t;

typedef enum cw_lw_osr_e {
    CW_LW_OSR_1 = 0,
    CW_LW_OSR_2 = 1,
    CW_LW_OSR_MAX
} cw_lw_osr_t;

typedef enum cw_lw_pll_multiplier_e {
    CW_LW_PLL_MULTIPLIER_170,
    CW_LW_PLL_MULTIPLIER_165,
    CW_LW_PLL_MULTIPLIER_160,
    CW_LW_PLL_MULTIPLIER_132,
    CW_LW_PLL_MULTIPLIER_MAX
} cw_lw_pll_multiplier_t;

typedef enum cw_lw_vco_rate_e {
    CHIP_LW_VCO_26P5625,
    CHIP_LW_VCO_25P78125,
    CHIP_LW_VCO_20P625,
    CHIP_LW_VCO_MAX
} cw_lw_vco_rate_t ;

typedef struct cw_lw_config_s {
    uint16_t br:4;
    uint16_t mod:1;
    uint16_t osr:3;
    uint16_t multiplier:4;
    uint16_t vco_rate:4;
    uint16_t lane_mask;
} cw_lw_config;

/**
 * The enumeration of chip function mode
 */
enum cw_function_mode_e {
    CHIP_MODE_NONE = 0,
    CHIP_MODE_400G = 1, /**< 400G mode */
    CHIP_MODE_200G = 2, /**< 200G mode */
    CHIP_MODE_100G = 3, /**< 100G mode */
    CHIP_MODE_50G = 4,  /**< 50G mode */
    CHIP_MODE_25G = 5,  /**< 25G mode */
    CHIP_MODE_10G = 6,  /**< 10G mode */
    CHIP_MODE_1G = 7,   /**< 1G mode */
    CHIP_MODE_20G = 8,  /**< 20G mode */
    CHIP_MODE_40G = 9  /**< 40G mode */
}; /**< Chip function mode type */
typedef uint8_t cw_function_mode_t;  /* force the enum to 1 bytes*/


enum cw_host_fec_type_e { 
    CHIP_HOST_FEC_TYPE_NA = 0, /**< No FEC Mode */
    CHIP_HOST_FEC_TYPE_RS528 = 1, /**< RS528 KR4*/
    CHIP_HOST_FEC_TYPE_RS544 = 2, /**< RS544 KP4 */
    CHIP_HOST_FEC_TYPE_PCS = 3, /**< PCS */
    CHIP_HOST_FEC_TYPE_MAX

}; /**< FEC Type definition */
typedef uint8_t cw_host_fec_type_t;  /* force the enum to 1 bytes */

enum cw_line_fec_type_e { 
    CHIP_LINE_FEC_TYPE_NA = 0, /**< No FEC Mode */
    CHIP_LINE_FEC_TYPE_RS528 = 1, /**< RS528 KR4*/
    CHIP_LINE_FEC_TYPE_RS544 = 2, /**< RS544 KP4 */
    CHIP_LINE_FEC_TYPE_PCS = 3, /**< PCS */
    CHIP_LINE_FEC_TYPE_MAX

}; /**< FEC Type definition */
typedef uint8_t cw_line_fec_type_t;  /* force the enum to 1 bytes*/

/**
 * The enumeration of lane mux Type
 */
enum cw_port_mux_e {
    CHIP_PORT_MUX_BIT_MUX = 0, /**< bit mux type */
    CHIP_PORT_MUX_SYMBOL_MUX = 1, /**< symbol mux type  */

    CHIP_PORT_MUX_MAX

}; /**< Chip lane mux type*/
typedef uint8_t cw_port_mux_t;  /* force the enum to 1 bytes*/

/**
 * The enumeration of lane FEC term Type
 */
enum cw_port_fec_term_type_e {
    CHIP_PORT_FEC_TERM_BYPASS = 0, /**< NO FEC */
    CHIP_PORT_FEC_DEC_FWD = 1, /**< FEC_DEC_FWD */
    CHIP_PORT_FEC_DEC_ENC = 2, /**< FEC_DEC_ENC */
    CHIP_LANE_PCS_XENC = 3, /**< PCS_XENC: host_fec_type =  CHIP_HOST_FEC_TYPE_PCS*/
    CHIP_LANE_FEC_DEC_XDEC_XENC_ENC = 4, /**< FEC_DEC_XDEC_XENC_ENC */
    CHIP_LANE_XENC_PCS = 5, 
    CHIP_LANE_FEC_TERM_MAX
}; /**< Chip lane line inner code type*/
typedef uint8_t cw_port_fec_term_type_t;  /* force the enum to 1 bytes*/


/*chip port mode entry definition*/
typedef struct cw_port_entry_s {
    cw_chip_mode_t          port_type;
    cw_function_mode_t      func_mode;
    cw_port_fec_term_type_t fec_term; 
    cw_host_fec_type_t      host_fec; 
    cw_line_fec_type_t      line_fec; 
    chip_bh_config          bh_cfg;
    chip_bh_config          lw_cfg;
    cw_port_mux_t           port_mux; 
} cw_port_entry_t;


typedef struct cw_port_entry_ex_s {
    cw_chip_mode_t          port_type;
    cw_function_mode_t      func_mode;
    cw_port_fec_term_type_t fec_term; 
    cw_host_fec_type_t      host_fec; 
    cw_line_fec_type_t      line_fec; 
    chip_bh_config          bh_cfg;
    chip_bh_config          lw_cfg;
    cw_port_mux_t           port_mux; 
   
    uint16_t                bh_lane_mask;
    uint16_t                lw_lane_mask;
} cw_port_entry_ex_t;

typedef cw_port_entry_t capi_port_entry_t;

#ifdef __cplusplus
}
#endif
#endif  /*CHIP_MODE_DEF_H*/
