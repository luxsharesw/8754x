/**
 *
 * @file     serdes_utils.h
 * @author   HSIP FW
 * @date     8/5/2019
 * @version  1.0
 *
 * @property 
 * $Copyright: (c) 2021 Broadcom.
 * Broadcom Proprietary and Confidential. All rights reserved.$
 *
 * @brief    brief description of the block source file, sequences/flow/etc.
 *
 * @section  description
 *
 */

#ifndef SERDES_UTILS_H
#define SERDES_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief serdes_util_get_base_address(core_ip_t core_ip)
 *
 * Config  Based on the core IP it returns the serdes register offset
 *
 * @param  core_ip
 *
 * @return Base address offset
 */
uint32_t serdes_util_get_base_address(core_ip_t core_ip);

#ifdef __cplusplus
}
#endif

#endif /* SERDES_UTILS_H */

