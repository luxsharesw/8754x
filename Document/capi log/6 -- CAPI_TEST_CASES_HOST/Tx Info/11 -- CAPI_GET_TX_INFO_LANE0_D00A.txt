*** Please choose from following test items ......


Please choose core IP:
0: Host Core IP
1: Media Core IP
2: BACK_TO_PREV_MENU
0
**  0 -- CAPI_PRBS_GEN
**  1 -- CAPI_PRBS_CHK
**  2 -- CAPI_GET_PRBS_INFO
**  3 -- CAPI_PRBS_DISABLE
**  4 -- CAPI_PRBS_CLEAR
**  5 -- CAPI_PRBS_INJECT_ERROR
**  6 -- CAPI_READ_REGISTER
**  7 -- CAPI_WRITE_REGISTER
**  8 -- CAPI_GET_POLARITY
**  9 -- CAPI_CHANGE_POLARITY
** 10 -- CAPI_SET_TX_INFO
** 11 -- CAPI_GET_TX_INFO
** 12 -- CAPI_SET_SQUELCH
** 13 -- CAPI_GET_TX_ELECTRIC_IDLE_INFO
** 14 -- CAPI_SET_LOOPBACK
** 15 -- CAPI_GET_LOOPBACK
** 16 -- CAPI_GET_LANE_STATUS
** 17 -- CAPI_SET_LANE_CTRL
** 18 -- CAPI_GET_LANE_CTRL
** 19 -- CAPI_LANE_RESET
** 20 -- CAPI_SET_RX_INFO
** 21 -- CAPI_GET_RX_INFO
** 22 -- CAPI_HOST_SERDES_SET_TX_SHARED_PTRN
** 23 -- CAPI_SET_OPTRXLOS_HOST_FAST_TX_SQUELCH
** 24 -- CAPI_TEST_SET_ELECTRICAL_OPTICAL
** 25 -- CAPI_GET_GPR_LANE_STATUS
** 26 -- CAPI_DIAG_GET_LANE_STATUS
** 27 -- CAPI_SET_DSP_POWER_INFO
** 28 -- CAPI_GET_DSP_POWER_INFO
** 29 -- CAPI_TEST_FAST_CMIS_SNR
** 30 -- CAPI_TEST_CONFIG_MPI
** 31 -- CAPI_TEST_GET_MPI_STATE
** 32 -- CAPI_TEST_SET_87541_TC_TX_TYPE
** 33 -- CAPI_GET_USR_DIAG
** 34 -- CAPI_GET_DSP_SLICER_HISTOGRAM
** 35 -- BACK_TO_PREV_MENU
11


Please choose channel ID:
0: all channels
1: channel 0
2: channel 1
3: channel 2
4: channel 3
5: user specified lane mask
1


 ***** Successfully get Host lane 0 config info! *****
Symbol swap: disable
TAP enum: 3
Pre2: 0
Pre1: -16
Main: 120
Post1: 0
Post2: 0
Post3: 0

*** Please choose from following test items ......