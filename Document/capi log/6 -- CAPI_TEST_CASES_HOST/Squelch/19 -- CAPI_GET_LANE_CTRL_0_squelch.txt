**  0 -- CAPI_PRBS_GEN
**  1 -- CAPI_PRBS_CHK
**  2 -- CAPI_GET_PRBS_INFO
**  3 -- CAPI_PRBS_DISABLE
**  4 -- CAPI_PRBS_CLEAR
**  5 -- CAPI_PRBS_INJECT_ERROR
**  6 -- CAPI_READ_REGISTER
**  7 -- CAPI_WRITE_REGISTER
**  8 -- CAPI_GET_POLARITY
**  9 -- CAPI_CHANGE_POLARITY
** 10 -- CAPI_SET_TX_INFO
** 11 -- CAPI_GET_TX_INFO
** 12 -- CAPI_SET_SQUELCH
** 13 -- CAPI_GET_TX_ELECTRIC_IDLE_INFO
** 14 -- CAPI_SET_LOOPBACK
** 15 -- CAPI_GET_LOOPBACK
** 16 -- CAPI_GET_CDR_PMD_INFO
** 17 -- CAPI_GET_LANE_STATUS
** 18 -- CAPI_SET_LANE_CTRL
** 19 -- CAPI_GET_LANE_CTRL
** 20 -- CAPI_LANE_RESET
** 21 -- CAPI_SET_RX_INFO
** 22 -- CAPI_GET_RX_INFO
** 23 -- CAPI_SERDES_SET_TX_SHARED_PTRN
** 24 -- CAPI_TEST_SET_ELECTRICAL_OPTICAL
** 25 -- CAPI_GET_GPR_LANE_STATUS
** 26 -- CAPI_SET_DSP_POWER_INFO
** 27 -- CAPI_GET_DSP_POWER_INFO
** 28 -- CAPI_GET_USR_DIAGNOSTIC (client host)
** 29 -- CAPI_GET_USR_DIAG (line media)
** 30 -- BACK_TO_PREV_MENU
19


Please choose channel ID:
0: all channels
1: channel 0
2: channel 1
3: channel 2
4: channel 3
5: user specified lane mask
0

**Choose all Host lanes 0
Please choose option for getting lane control ......
** 0 -- Squelch
** 1 -- Lane Reset
** 2 -- Traffic
** 3 -- Electric Idle
** 4 -- DataPath Power
** 5 -- Ignore Fault
** 6 -- Suspend/Resume
0


Choose lane direction: 0 - Ingress (Serdes Tx); 1 - Egress (Serdes Rx) 2 - Both
0



***** Successfully Get Host Lane 0 Control Info! *****
--- Host Lane Control Info ingress squelch = 1



***** Successfully Get Host Lane 1 Control Info! *****
--- Host Lane Control Info ingress squelch = 1



***** Successfully Get Host Lane 2 Control Info! *****
--- Host Lane Control Info ingress squelch = 1



***** Successfully Get Host Lane 3 Control Info! *****
--- Host Lane Control Info ingress squelch = 1

*** Please choose from following test items ......