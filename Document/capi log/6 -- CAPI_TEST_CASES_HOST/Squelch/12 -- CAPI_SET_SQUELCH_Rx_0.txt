**  0 -- CAPI_PRBS_GEN
**  1 -- CAPI_PRBS_CHK
**  2 -- CAPI_GET_PRBS_INFO
**  3 -- CAPI_PRBS_DISABLE
**  4 -- CAPI_PRBS_CLEAR
**  5 -- CAPI_PRBS_INJECT_ERROR
**  6 -- CAPI_READ_REGISTER
**  7 -- CAPI_WRITE_REGISTER
**  8 -- CAPI_GET_POLARITY
**  9 -- CAPI_CHANGE_POLARITY
** 10 -- CAPI_SET_TX_INFO
** 11 -- CAPI_GET_TX_INFO
** 12 -- CAPI_SET_SQUELCH
** 13 -- CAPI_GET_TX_ELECTRIC_IDLE_INFO
** 14 -- CAPI_SET_LOOPBACK
** 15 -- CAPI_GET_LOOPBACK
** 16 -- CAPI_GET_CDR_PMD_INFO
** 17 -- CAPI_GET_LANE_STATUS
** 18 -- CAPI_SET_LANE_CTRL
** 19 -- CAPI_GET_LANE_CTRL
** 20 -- CAPI_LANE_RESET
** 21 -- CAPI_SET_RX_INFO
** 22 -- CAPI_GET_RX_INFO
** 23 -- CAPI_SERDES_SET_TX_SHARED_PTRN
** 24 -- CAPI_TEST_SET_ELECTRICAL_OPTICAL
** 25 -- CAPI_GET_GPR_LANE_STATUS
** 26 -- CAPI_SET_DSP_POWER_INFO
** 27 -- CAPI_GET_DSP_POWER_INFO
** 28 -- CAPI_GET_USR_DIAGNOSTIC (client host)
** 29 -- CAPI_GET_USR_DIAG (line media)
** 30 -- BACK_TO_PREV_MENU
12


Please choose channel ID:
0: all channels
1: channel 0
2: channel 1
3: channel 2
4: channel 3
5: user specified lane mask
0

**Choose all Host lanes 0


Please choose: 0 -- Rx; 1 -- Tx)
0


Please Choose: 0 -- Unsquelch ; 1 -- Squelch
0


***** Successfully config serdes Tx Unsquelch! *****
Reading back the Squelch values ...


*****  Successfully read serdes squelch status on lane 0 ! *****


*****  Serdes lane 0 Rx Squelch : Off *****


*****  Serdes lane 0 Tx Squelch : Off *****


*****  Successfully read serdes squelch status on lane 1 ! *****


*****  Serdes lane 1 Rx Squelch : Off *****


*****  Serdes lane 1 Tx Squelch : Off *****


*****  Successfully read serdes squelch status on lane 2 ! *****


*****  Serdes lane 2 Rx Squelch : Off *****


*****  Serdes lane 2 Tx Squelch : Off *****


*****  Successfully read serdes squelch status on lane 3 ! *****


*****  Serdes lane 3 Rx Squelch : Off *****


*****  Serdes lane 3 Tx Squelch : Off *****

*** Please choose from following test items ......