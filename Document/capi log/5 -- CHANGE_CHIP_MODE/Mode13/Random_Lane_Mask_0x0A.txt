Chip ID: 0x87540

 Get the PORT 0 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 2 -- CAPI_MODE_200
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 0 -- CAPI_BH_BR_53_125
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 1 -- PAM4
** HOST lane_mask: 0xf
** LINE data_rate: 0 -- CAPI_LW_BR_53_125
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0xf

**************MODE CONFIG ITEM*************
***  1 -- MODE_1  - CHIP_MODE_4X53G_PAM4_4X53G_PAM4
***  2 -- MODE_2  - CHIP_MODE_2X53G_PAM4_2X53G_PAM4
***  3 -- MODE_3  - CHIP_MODE_4X25G_NRZ_4X25G_NRZ
***  4 -- MODE_4  - CHIP_MODE_1X53G_PAM4_1X53G_PAM4
***  5 -- MODE_5  - CHIP_MODE_4X26G_NRZ_2X53G_PAM4
***  6 -- MODE_6  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4
***  7 -- MODE_7  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4
***  8 -- MODE_8  - CHIP_MODE_4X25G_NRZ_2X50G_PAM4
***  9 -- MODE_9  - CHIP_MODE_1X10G_NRZ_1X10G_NRZ
*** 10 -- MODE_10  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1
*** 11 -- MODE_11  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1
*** 12 -- MODE_12  - CHIP_MODE_4X26G_NRZ_4X26G_NRZ
*** 13 -- MODE_13  - CHIP_MODE_1X25G_NRZ_1X25G_NRZ
*** 14 -- MODE_14  - CHIP_MODE_2X26G_NRZ_2X26G_NRZ
*** 15 -- MODE_15  - CHIP_MODE_2X25G_NRZ_2X25G_NRZ
*** 16 -- MODE_16  - CHIP_MODE_2X25G_NRZ_1X53G_PAM4
*** 17 -- MODE_17  - CHIP_MODE_4X25G_NRZ_2X53G_PAM4
*** 18 -- MODE_18  - CHIP_MODE_4X50G_PAM4_4X50G_PAM4
*** 19 -- MODE_19  - CHIP_MODE_2X25G_NRZ_1X53G_PAM4_M1

Please choose valid config mode:  13
13
***  0 -- 3.1 -- CHIP_MODE_1X25G_NRZ_1X25G_NRZ:Repeater Bit_Mux

Please choose valid config sub mode:  0
0
*** 0: port  0 --CAPI_MODE_25G  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0x1 NRZ CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0x1 NRZ CAPI_LINE_FEC_TYPE_NA
*** 1: port  1 --CAPI_MODE_25G  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0x2 NRZ CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0x2 NRZ CAPI_LINE_FEC_TYPE_NA
*** 2: port  2 --CAPI_MODE_25G  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0x4 NRZ CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0x4 NRZ CAPI_LINE_FEC_TYPE_NA
*** 3: port  3 --CAPI_MODE_25G  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0x8 NRZ CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0x8 NRZ CAPI_LINE_FEC_TYPE_NA
*** 4: all ports --CAPI_MODE_25G CAPI_LANE_FEC_TERM_BYPASS HOST lane 0xf NRZ CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0xf NRZ CAPI_LINE_FEC_TYPE_NA
*** 5: random lane mask

Please choose config port:  5
5

Please choose config HOST lane mask (hex):  0A
10

Please choose config LINE lane mask (hex):  0A
10

*** INFO--> 3.1 -- CHIP_MODE_1X25G_NRZ_1X25G_NRZ:Repeater Bit_Mux: Set Mode Config Success ***
** No Active PORT mode

 Get the PORT 1 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 5 -- CAPI_MODE_25G
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 2 -- CAPI_BH_BR_25_78125
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 0 -- NRZ
** HOST lane_mask: 0x2
** LINE data_rate: 2 -- CAPI_LW_BR_25_78125
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 0 -- NRZ
** LINE lane_mask: 0x2
** No Active PORT mode

 Get the PORT 3 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 5 -- CAPI_MODE_25G
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 2 -- CAPI_BH_BR_25_78125
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 0 -- NRZ
** HOST lane_mask: 0x8
** LINE data_rate: 2 -- CAPI_LW_BR_25_78125
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 0 -- NRZ
** LINE lane_mask: 0x8

** Command Done, wait for next command