Please choose your testing itmes ......
** 0 -- CHANGE_USB_ID
** 1 -- CHANGE_PHY_ID
** 2 -- FIRMWARE_DOWNLOAD
** 3 -- FIRMWARE_DOWNLOAD_STATUS
** 4 -- FIRMWARE_SPI_PROGRAM
** 5 -- CHANGE_CHIP_MODE
** 6 -- CAPI_TEST_CASES
** 7 -- FEC_ST_CASE
** 8 -- GET_CHIP_STATUS
** 9 -- GET_VOLTAGE_TEMPERATURE
** 10 -- CHIP_TEST_SOFT_RESET
** 11 -- CHIP_TEST_HARD_RESET
** 12 -- AVS_CONFIG
** 13 -- AVS_STATUS
** 14 -- INTERNAL_0p75v_TEST
** 15 -- INTERNAL_0p9v_TEST
** 99 -- Exit Test Case
5
Chip ID: 0x87540

 Get the PORT 0 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 2 -- CAPI_MODE_200
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 1 -- CAPI_BH_BR_51_5625
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 1 -- PAM4
** HOST lane_mask: 0xf
** LINE data_rate: 1 -- CAPI_LW_BR_51_5625
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0xf

**************MODE CONFIG ITEM*************
***  1 -- MODE_1  - CHIP_MODE_4X53G_PAM4_4X53G_PAM4
***  2 -- MODE_2  - CHIP_MODE_2X53G_PAM4_2X53G_PAM4
***  3 -- MODE_3  - CHIP_MODE_4X25G_NRZ_4X25G_NRZ
***  4 -- MODE_4  - CHIP_MODE_1X53G_PAM4_1X53G_PAM4
***  5 -- MODE_5  - CHIP_MODE_4X26G_NRZ_2X53G_PAM4
***  6 -- MODE_6  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4
***  7 -- MODE_7  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4
***  8 -- MODE_8  - CHIP_MODE_4X25G_NRZ_2X50G_PAM4
***  9 -- MODE_9  - CHIP_MODE_1X10G_NRZ_1X10G_NRZ
*** 10 -- MODE_10  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1
*** 11 -- MODE_11  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1
*** 12 -- MODE_12  - CHIP_MODE_4X26G_NRZ_4X26G_NRZ
*** 13 -- MODE_13  - CHIP_MODE_1X25G_NRZ_1X25G_NRZ
*** 14 -- MODE_14  - CHIP_MODE_2X26G_NRZ_2X26G_NRZ
*** 15 -- MODE_15  - CHIP_MODE_2X25G_NRZ_2X25G_NRZ
*** 16 -- MODE_16  - CHIP_MODE_2X25G_NRZ_1X53G_PAM4
*** 17 -- MODE_17  - CHIP_MODE_4X25G_NRZ_2X53G_PAM4
*** 18 -- MODE_18  - CHIP_MODE_4X50G_PAM4_4X50G_PAM4
*** 19 -- MODE_19  - CHIP_MODE_2X25G_NRZ_1X53G_PAM4_M1

Please choose valid config mode:  18
18
***  0 -- 18.1 -- CHIP_MODE_4X50G_PAM4_4X50G_PAM4:Repeater Bit_Mux

Please choose valid config sub mode:  0
0
*** 0: port  0 --CAPI_MODE_200  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0xf PAM4 CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0xf PAM4 CAPI_LINE_FEC_TYPE_NA
*** 1: all ports --CAPI_MODE_200 CAPI_LANE_FEC_TERM_BYPASS HOST lane 0xf PAM4 CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0xf PAM4 CAPI_LINE_FEC_TYPE_NA
*** 2: random lane mask

Please choose config port:  1
1

*** INFO--> 18.1 -- CHIP_MODE_4X50G_PAM4_4X50G_PAM4:Repeater Bit_Mux: Set Mode Config Success ***

 Get the PORT 0 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 2 -- CAPI_MODE_200
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 1 -- CAPI_BH_BR_51_5625
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 1 -- PAM4
** HOST lane_mask: 0xf
** LINE data_rate: 1 -- CAPI_LW_BR_51_5625
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0xf

** Command Done, wait for next command

請按任意鍵繼續 . . .