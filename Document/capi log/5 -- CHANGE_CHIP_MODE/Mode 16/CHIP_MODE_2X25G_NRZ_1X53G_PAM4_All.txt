**************MODE CONFIG ITEM*************
***  1 -- MODE_1  - CHIP_MODE_4X53G_PAM4_4X53G_PAM4
***  2 -- MODE_2  - CHIP_MODE_2X53G_PAM4_2X53G_PAM4
***  3 -- MODE_3  - CHIP_MODE_4X25G_NRZ_4X25G_NRZ
***  4 -- MODE_4  - CHIP_MODE_1X53G_PAM4_1X53G_PAM4
***  5 -- MODE_5  - CHIP_MODE_4X26G_NRZ_2X53G_PAM4
***  6 -- MODE_6  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4
***  7 -- MODE_7  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4
***  8 -- MODE_8  - CHIP_MODE_4X25G_NRZ_2X50G_PAM4
***  9 -- MODE_9  - CHIP_MODE_1X10G_NRZ_1X10G_NRZ
*** 10 -- MODE_10  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1
*** 11 -- MODE_11  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1
*** 12 -- MODE_12  - CHIP_MODE_4X26G_NRZ_4X26G_NRZ
*** 13 -- MODE_13  - CHIP_MODE_1X25G_NRZ_1X25G_NRZ
*** 14 -- MODE_14  - CHIP_MODE_2X26G_NRZ_2X26G_NRZ
*** 15 -- MODE_15  - CHIP_MODE_2X25G_NRZ_2X25G_NRZ
*** 16 -- MODE_16  - CHIP_MODE_2X25G_NRZ_1X53G_PAM4
*** 17 -- MODE_17  - CHIP_MODE_4X25G_NRZ_2X53G_PAM4

Please choose valid config mode:  16
16
***  0 -- 16.1 -- CHIP_MODE_2X25G_NRZ_1X53G_PAM4: PCS_XDEC PCS<=>KP4

Please choose valid config sub mode:  0
0
*** 0: port  0 --CAPI_MODE_50G  CAPI_LANE_PCS_XENC HOST lane 0x3 NRZ CAPI_HOST_FEC_TYPE_PCS <=> LINE lane 0x1 PAM4 CAPI_LINE_FEC_TYPE_RS544
*** 1: port  2 --CAPI_MODE_50G  CAPI_LANE_PCS_XENC HOST lane 0xc NRZ CAPI_HOST_FEC_TYPE_PCS <=> LINE lane 0x4 PAM4 CAPI_LINE_FEC_TYPE_RS544
*** 2: all ports --CAPI_MODE_50G CAPI_LANE_PCS_XENC HOST lane 0xf NRZ CAPI_HOST_FEC_TYPE_PCS <=> LINE lane 0x5 PAM4 CAPI_LINE_FEC_TYPE_RS544
*** 3: random lane mask

Please choose config port:  2
2

*** INFO--> 16.1 -- CHIP_MODE_2X25G_NRZ_1X53G_PAM4: PCS_XDEC PCS<=>KP4: Set Mode Config Success ***

 Get the PORT 0 config information

** Reference Clock: 0

** port low power: DOWN
** func_mode: 4 -- CAPI_MODE_50G
** fec_term: 3 -- CAPI_LANE_PCS_XENC
** HOST data_rate: 2 -- CAPI_BH_BR_25_78125
** HOST fec_type: 3 -- CAPI_HOST_FEC_TYPE_PCS
** HOST modulation: 0 -- NRZ
** HOST lane_mask: 0x3
** LINE data_rate: 0 -- CAPI_LW_BR_53_125
** LINE fec_type: 2 -- CAPI_LINE_FEC_TYPE_RS544
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0x1
** No Active PORT mode

 Get the PORT 2 config information

** Reference Clock: 0

** port low power: DOWN
** func_mode: 4 -- CAPI_MODE_50G
** fec_term: 3 -- CAPI_LANE_PCS_XENC
** HOST data_rate: 2 -- CAPI_BH_BR_25_78125
** HOST fec_type: 3 -- CAPI_HOST_FEC_TYPE_PCS
** HOST modulation: 0 -- NRZ
** HOST lane_mask: 0xc
** LINE data_rate: 0 -- CAPI_LW_BR_53_125
** LINE fec_type: 2 -- CAPI_LINE_FEC_TYPE_RS544
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0x4
** No Active PORT mode

** Command Done, wait for next command

請按任意鍵繼續 . . .