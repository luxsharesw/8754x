**************MODE CONFIG ITEM*************
***  1 -- MODE_1  - CHIP_MODE_4X53G_PAM4_4X53G_PAM4
***  2 -- MODE_2  - CHIP_MODE_2X53G_PAM4_2X53G_PAM4
***  3 -- MODE_3  - CHIP_MODE_4X25G_NRZ_4X25G_NRZ
***  4 -- MODE_4  - CHIP_MODE_1X53G_PAM4_1X53G_PAM4
***  5 -- MODE_5  - CHIP_MODE_4X26G_NRZ_2X53G_PAM4
***  6 -- MODE_6  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4
***  7 -- MODE_7  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4
***  8 -- MODE_8  - CHIP_MODE_4X25G_NRZ_2X50G_PAM4
***  9 -- MODE_10  - CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1
*** 10 -- MODE_11  - CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1
*** 11 -- MODE_12  - CHIP_MODE_4X26G_NRZ_4X26G_NRZ

Please choose valid config mode:  2
2
***  0 -- 2.1 -- CHIP_MODE_2X53G_PAM4_2X53G_PAM4:Repeater Bit_Mux
***  1 -- 2.2 -- CHIP_MODE_2X53G_PAM4_2X53G_PAM4:KP4<=>KP4 FEC_XDEC_XENC
***  2 -- 2.3 -- CHIP_MODE_2X53G_PAM4_2X53G_PAM4:KP4<=>KP4 FEC_DEC_FWD
***  3 -- 2.4 -- CHIP_MODE_2X53G_PAM4_2X53G_PAM4:KP4<=>KP4 FEC_DEC_ENC

Please choose valid config sub mode:  0
0
*** 0: port  0 --CAPI_MODE_100  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0x3 PAM4 CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0x3 PAM4 CAPI_LINE_FEC_TYPE_NA
*** 1: port  2 --CAPI_MODE_100  CAPI_LANE_FEC_TERM_BYPASS HOST lane 0xc PAM4 CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0xc PAM4 CAPI_LINE_FEC_TYPE_NA
*** 2: all ports --CAPI_MODE_100 CAPI_LANE_FEC_TERM_BYPASS HOST lane 0xf PAM4 CAPI_HOST_FEC_TYPE_NA <=> LINE lane 0xf PAM4 CAPI_LINE_FEC_TYPE_NA
*** 3: random lane mask

Please choose config port:  0
0

*** INFO--> 2.1 -- CHIP_MODE_2X53G_PAM4_2X53G_PAM4:Repeater Bit_Mux: Set Mode Config Success ***

 Get the PORT 0 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 3 -- CAPI_MODE_100
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 0 -- CAPI_BH_BR_53_125
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 1 -- PAM4
** HOST lane_mask: 0x3
** LINE data_rate: 0 -- CAPI_LW_BR_53_125
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0x3

 Get the PORT 2 config information

** Reference Clock: 0

** port low power: UP
** func_mode: 3 -- CAPI_MODE_100
** fec_term: 0 -- CAPI_LANE_FEC_TERM_BYPASS
** HOST data_rate: 0 -- CAPI_BH_BR_53_125
** HOST fec_type: 0 -- CAPI_HOST_FEC_TYPE_NA
** HOST modulation: 1 -- PAM4
** HOST lane_mask: 0xc
** LINE data_rate: 0 -- CAPI_LW_BR_53_125
** LINE fec_type: 0 -- CAPI_LINE_FEC_TYPE_NA
** LINE modulation: 1 -- PAM4
** LINE lane_mask: 0xc

** Command Done, wait for next command