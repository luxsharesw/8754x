'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'User Macro: 
'During macro recording, DON'T CLOSE OR EDIT FILE
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0A&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H56&
App.wrI2C &H50&, &H87&, &H01&
'App.WriteEst &H00&, &H00047000&, &H0156000A&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H14&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008014&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H0A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H56&
App.rdI2C &H50&, &H90& ' value=&H01&
'App.ReadEst &H00&, &H00047400&   ' data=&H0156000A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000080&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H01&
'App.ReadEst &H00&, &H00047408&   ' data=&H01000100&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0A&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H57&
App.wrI2C &H50&, &H87&, &H01&
'App.WriteEst &H00&, &H00047000&, &H0157000A&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
'App.WriteEst &H00&, &H00047008&, &H01000100&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
'App.WriteEst &H00&, &H0004700C&, &H01000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H14&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008014&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H0A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H57&
App.rdI2C &H50&, &H90& ' value=&H01&
'App.ReadEst &H00&, &H00047400&   ' data=&H0157000A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000080&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047408&   ' data=&H02000200&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0A&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H58&
App.wrI2C &H50&, &H87&, &H01&
'App.WriteEst &H00&, &H00047000&, &H0158000A&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047008&, &H02000200&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H0004700C&, &H02000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H14&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008014&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H0A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H58&
App.rdI2C &H50&, &H90& ' value=&H01&
'App.ReadEst &H00&, &H00047400&   ' data=&H0158000A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000080&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H04&
'App.ReadEst &H00&, &H00047408&   ' data=&H04000400&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0A&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H59&
App.wrI2C &H50&, &H87&, &H01&
'App.WriteEst &H00&, &H00047000&, &H0159000A&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H04&
'App.WriteEst &H00&, &H00047008&, &H04000400&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H04&
'App.WriteEst &H00&, &H0004700C&, &H04000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H14&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008014&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H0A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H59&
App.rdI2C &H50&, &H90& ' value=&H01&
'App.ReadEst &H00&, &H00047400&   ' data=&H0159000A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000080&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H08&
'App.ReadEst &H00&, &H00047408&   ' data=&H08000800&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
