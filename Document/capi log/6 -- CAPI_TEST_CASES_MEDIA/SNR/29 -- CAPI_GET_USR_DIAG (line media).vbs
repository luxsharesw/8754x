'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'User Macro: 
'During macro recording, DON'T CLOSE OR EDIT FILE
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H21&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58002158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &H21&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H9C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H580021B0&   ' data=&H00009C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H03&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000450&   ' data=&H00000003&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H07&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58000450&, &H00000007&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H93&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000608&   ' data=&H00000093&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HF9&
App.rdI2C &H50&, &H90& ' value=&HE7&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000604&   ' data=&H0000E7F9&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H21&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58002158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &H21&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H9C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H580021B0&   ' data=&H00009C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00010008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H07&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000450&   ' data=&H00000007&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58000450&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H93&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000608&   ' data=&H00000093&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HD7&
App.rdI2C &H50&, &H90& ' value=&H06&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000604&   ' data=&H000006D7&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58000450&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H58&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000608&   ' data=&H00000058&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H8F&
App.rdI2C &H50&, &H90& ' value=&HDC&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000604&   ' data=&H0000DC8F&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000450&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58000450&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H74&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000608&   ' data=&H00000074&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H9E&
App.rdI2C &H50&, &H90& ' value=&H07&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000604&   ' data=&H0000079E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800044C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800044C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000450&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H04&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58000450&, &H00000003&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H71&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000608&   ' data=&H00000071&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H06&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H72&
App.rdI2C &H50&, &H90& ' value=&H1D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58000604&   ' data=&H00001D72&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00020008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H78&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H03&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00030078&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047008&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004700C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047010&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047014&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047018&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004701C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047020&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047024&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047028&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004702C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047030&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047034&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047038&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004703C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047040&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047044&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047048&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004704C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047050&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047054&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047058&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004705C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047060&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047064&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047068&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004706C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047070&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047074&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047078&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H17&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008017&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H78&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H03&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00030078&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HC0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H0000FFC0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H40&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047408&   ' data=&HFFA00040&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H20&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H0020FFE0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000060&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA2&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047414&   ' data=&HFFFFFFA2&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE1&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047418&   ' data=&HFFFFFFE1&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H0000001D&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H5F&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H0000005F&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H21&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000021&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H16&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000016&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1B&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H0000001B&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H20&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047430&   ' data=&H00000020&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HCA&
App.rdI2C &H50&, &H90& ' value=&H2D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047434&   ' data=&H00002DCA&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HC7&
App.rdI2C &H50&, &H90& ' value=&H36&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047438&   ' data=&H000036C7&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HF5&
App.rdI2C &H50&, &H90& ' value=&H31&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004743C&   ' data=&H000031F5&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H4C&
App.rdI2C &H50&, &H90& ' value=&H2D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047440&   ' data=&H00002D4C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047444&   ' data=&HFFE0FFA0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1E&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047448&   ' data=&H0060001E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004744C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047454&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HB7&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HF6&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047458&   ' data=&HFFF6FFB7&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H34&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004745C&   ' data=&H00000034&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047460&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047464&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047468&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004746C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047470&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047474&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047478&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H61&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58006158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &H61&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H580061B0&   ' data=&H00001C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H03&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004450&   ' data=&H00000003&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H07&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58004450&, &H00000007&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H89&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004608&   ' data=&H00000089&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2D&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004604&   ' data=&H0000E02D&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H61&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58006158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &H61&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H580061B0&   ' data=&H00001C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00040008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H07&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004450&   ' data=&H00000007&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58004450&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H8A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004608&   ' data=&H0000008A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H7C&
App.rdI2C &H50&, &H90& ' value=&HA5&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004604&   ' data=&H0000A57C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58004450&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H4D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004608&   ' data=&H0000004D&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HED&
App.rdI2C &H50&, &H90& ' value=&H55&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004604&   ' data=&H000055ED&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004450&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58004450&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004608&   ' data=&H00000060&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H50&
App.rdI2C &H50&, &H90& ' value=&H7A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004604&   ' data=&H00007A50&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800444C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800444C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004450&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H44&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58004450&, &H00000003&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H5A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004608&   ' data=&H0000005A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H46&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H58&
App.rdI2C &H50&, &H90& ' value=&H6A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58004604&   ' data=&H00006A58&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00050008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H78&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00060078&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047008&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004700C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047010&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047014&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047018&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004701C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047020&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047024&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047028&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004702C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047030&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047034&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047038&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004703C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047040&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047044&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047048&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004704C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047050&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047054&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047058&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004705C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047060&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047064&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047068&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004706C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047070&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047074&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047078&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H17&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008017&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H78&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H06&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00060078&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HC0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H0000FFC0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H40&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047408&   ' data=&HFFA00040&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H20&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H0020FFE0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000060&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA1&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047414&   ' data=&HFFFFFFA1&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE1&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047418&   ' data=&HFFFFFFE1&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1E&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H0000001E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H61&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000061&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1F&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H0000001F&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H14&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000014&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H15&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000015&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H17&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047430&   ' data=&H00000017&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HD6&
App.rdI2C &H50&, &H90& ' value=&H2E&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047434&   ' data=&H00002ED6&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H29&
App.rdI2C &H50&, &H90& ' value=&H3A&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047438&   ' data=&H00003A29&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HC2&
App.rdI2C &H50&, &H90& ' value=&H38&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004743C&   ' data=&H000038C2&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HF3&
App.rdI2C &H50&, &H90& ' value=&H34&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047440&   ' data=&H000034F3&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047444&   ' data=&HFFE0FFA0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1E&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H61&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047448&   ' data=&H0061001E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004744C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047454&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HB8&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HF7&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047458&   ' data=&HFFF7FFB8&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H36&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004745C&   ' data=&H00000036&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047460&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047464&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047468&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004746C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047470&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047474&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047478&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &HA1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800A158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &HA1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800A1B0&   ' data=&H00001C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H03&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008450&   ' data=&H00000003&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H07&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58008450&, &H00000007&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H78&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008608&   ' data=&H00000078&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1A&
App.rdI2C &H50&, &H90& ' value=&H74&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008604&   ' data=&H0000741A&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &HA1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800A158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &HA1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800A1B0&   ' data=&H00001C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H07&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00070008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H07&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008450&   ' data=&H00000007&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58008450&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H79&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008608&   ' data=&H00000079&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HF8&
App.rdI2C &H50&, &H90& ' value=&HA7&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008604&   ' data=&H0000A7F8&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58008450&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H42&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008608&   ' data=&H00000042&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H7E&
App.rdI2C &H50&, &H90& ' value=&H9D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008604&   ' data=&H00009D7E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008450&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58008450&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H50&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008608&   ' data=&H00000050&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H13&
App.rdI2C &H50&, &H90& ' value=&H6D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008604&   ' data=&H00006D13&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800844C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800844C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008450&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H84&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H58008450&, &H00000003&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H57&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008608&   ' data=&H00000057&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H86&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HD5&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H58008604&   ' data=&H000002D5&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00080008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H78&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00090078&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047008&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004700C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047010&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047014&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047018&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004701C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047020&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047024&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047028&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004702C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047030&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047034&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047038&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004703C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047040&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047044&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047048&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004704C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047050&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047054&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047058&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004705C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047060&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047064&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047068&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004706C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047070&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047074&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047078&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H17&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008017&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H78&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H09&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00090078&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HC0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H0000FFC0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H40&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047408&   ' data=&HFFA00040&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H20&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H0020FFE0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000060&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA2&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047414&   ' data=&HFFFFFFA2&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047418&   ' data=&HFFFFFFE0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H0000001D&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H61&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000061&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1F&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H0000001F&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000010&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H12&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000012&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H14&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047430&   ' data=&H00000014&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HF7&
App.rdI2C &H50&, &H90& ' value=&H2F&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047434&   ' data=&H00002FF7&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA7&
App.rdI2C &H50&, &H90& ' value=&H40&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047438&   ' data=&H000040A7&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H3B&
App.rdI2C &H50&, &H90& ' value=&H3C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004743C&   ' data=&H00003C3B&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HB6&
App.rdI2C &H50&, &H90& ' value=&H39&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047440&   ' data=&H000039B6&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HDF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047444&   ' data=&HFFDFFFA0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1E&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H62&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047448&   ' data=&H0062001E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004744C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047454&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HB9&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HF4&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047458&   ' data=&HFFF4FFB9&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H32&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004745C&   ' data=&H00000032&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047460&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047464&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047468&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004746C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047470&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047474&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047478&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &HE1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800E158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &HE1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800E1B0&   ' data=&H00001C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H03&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C450&   ' data=&H00000003&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H07&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C450&, &H00000007&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H6B&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C608&   ' data=&H0000006B&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HDE&
App.rdI2C &H50&, &H90& ' value=&HB5&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C604&   ' data=&H0000B5DE&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &HE1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800E158&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H28&
App.rdI2C &H50&, &H90& ' value=&H10&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C400&   ' data=&H00001028&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HB0&
App.wrI2C &H50&, &H81&, &HE1&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H11&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800E1B0&   ' data=&H00001C11&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H0A&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H000A0008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000080&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H07&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C450&   ' data=&H00000007&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C450&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H69&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C608&   ' data=&H00000069&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H9C&
App.rdI2C &H50&, &H90& ' value=&HF9&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C604&   ' data=&H0000F99C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C450&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H40&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C608&   ' data=&H00000040&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H6B&
App.rdI2C &H50&, &H90& ' value=&H46&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C604&   ' data=&H0000466B&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C450&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C450&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H41&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C608&   ' data=&H00000041&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H44&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C604&   ' data=&H00001C44&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H33&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000033&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H23&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000023&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H23&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C44C&   ' data=&H00000023&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H33&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C44C&, &H00000033&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C450&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &HC4&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5800C450&, &H00000003&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H3D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C608&   ' data=&H0000003D&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &HC6&
App.wrI2C &H50&, &H82&, &H00&
App.wrI2C &H50&, &H83&, &H58&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HB1&
App.rdI2C &H50&, &H90& ' value=&HC0&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5800C604&   ' data=&H0000C0B1&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H0B&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H000B0008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H20&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000020&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008006&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H78&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H000C0078&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047008&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004700C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047010&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047014&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047018&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004701C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047020&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047024&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047028&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004702C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047030&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047034&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047038&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004703C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047040&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047044&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047048&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004704C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047050&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047054&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047058&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004705C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047060&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047064&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047068&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H0004706C&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047070&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047074&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047078&, &HCCCCCCCC&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H17&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008017&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H78&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0C&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H000C0078&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HC0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H0000FFC0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H40&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047408&   ' data=&HFFA00040&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H20&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H0020FFE0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000060&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA1&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047414&   ' data=&HFFFFFFA1&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE1&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047418&   ' data=&HFFFFFFE1&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1D&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H0000001D&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000060&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H16&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000016&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H0000000F&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H13&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000013&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H30&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H13&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047430&   ' data=&H00000013&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H34&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HE4&
App.rdI2C &H50&, &H90& ' value=&H37&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047434&   ' data=&H000037E4&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H38&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H55&
App.rdI2C &H50&, &H90& ' value=&H43&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047438&   ' data=&H00004355&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H3C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H78&
App.rdI2C &H50&, &H90& ' value=&H3C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004743C&   ' data=&H00003C78&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H40&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H3C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047440&   ' data=&H00003C0F&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H44&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HA0&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HE1&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047444&   ' data=&HFFE1FFA0&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H48&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1E&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047448&   ' data=&H0060001E&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H4C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004744C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H50&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047450&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H54&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047454&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H58&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&HB5&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&HF5&
App.rdI2C &H50&, &H90& ' value=&HFF&
'App.ReadEst &H00&, &H00047458&   ' data=&HFFF5FFB5&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H5C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H34&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004745C&   ' data=&H00000034&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H60&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047460&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H64&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047464&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H68&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047468&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H6C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004746C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H70&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047470&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047474&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047478&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
