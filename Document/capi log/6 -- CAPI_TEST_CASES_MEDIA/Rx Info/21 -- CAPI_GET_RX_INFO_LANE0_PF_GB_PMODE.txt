Please choose core IP:
0: Host Core IP
1: Media Core IP
2: BACK_TO_PREV_MENU
1
**  0 -- CAPI_PRBS_GEN
**  1 -- CAPI_PRBS_CHK
**  2 -- CAPI_GET_PRBS_INFO
**  3 -- CAPI_PRBS_DISABLE
**  4 -- CAPI_PRBS_CLEAR
**  5 -- CAPI_PRBS_INJECT_ERROR
**  6 -- CAPI_READ_REGISTER
**  7 -- CAPI_WRITE_REGISTER
**  8 -- CAPI_GET_POLARITY
**  9 -- CAPI_CHANGE_POLARITY
** 10 -- CAPI_SET_TX_INFO
** 11 -- CAPI_GET_TX_INFO
** 12 -- CAPI_SET_SQUELCH
** 13 -- CAPI_GET_TX_ELECTRIC_IDLE_INFO
** 14 -- CAPI_SET_LOOPBACK
** 15 -- CAPI_GET_LOOPBACK
** 16 -- CAPI_GET_LANE_STATUS
** 17 -- CAPI_SET_LANE_CTRL
** 18 -- CAPI_GET_LANE_CTRL
** 19 -- CAPI_LANE_RESET
** 20 -- CAPI_SET_RX_INFO
** 21 -- CAPI_GET_RX_INFO
** 22 -- CAPI_SERDES_SET_TX_SHARED_PTRN
** 23 -- CAPI_TEST_SET_ELECTRICAL_OPTICAL
** 24 -- CAPI_GET_GPR_LANE_STATUS
** 25 -- CAPI_SET_DSP_POWER_INFO
** 26 -- CAPI_GET_DSP_POWER_INFO
** 27 -- CAPI_GET_USR_DIAG
** 28 -- CAPI_GET_SERDES_DIAG
** 29 -- CAPI_GET_DSP_SLICER_HISTOGRAM
** 30 -- BACK_TO_PREV_MENU
21


Please choose channel ID:
0: all channels
1: channel 0
2: channel 1
3: channel 2
4: channel 3
5: user specified lane mask
1


***** Get Media Lane_0 RX_INFO Successful ! *****
--- Rx VGA = 46
--- Rx power_mode  = 3 low power
--- Rx media type   = 0
--- Rx pf value   = 10
--- Rx dsp_graycode    = 1
--- Rx dc_wander_mu    = 4
--- Rx nldet_en     = 1
--- Rx phase auto tune en  = 0
--- Rx dynamic phase auto tune en  = 0
--- Rx max_phase_bias_th  = 10
--- Rx min_phase_bias_th  = -10
--- Rx dynamic_auto_ph_tune_type  = 0
--- Rx dynamic_auto_ph_tune_shift  = 0
--- Rx dsp_oplos_ignore    = 0
--- Rx dsp_elos_ignore    = 0
--- Rx dsp_los_th_idx    = 1
--- Rx dsp_inlos_th_idx    = 0
--- Rx dsp_outlos_th_idx    = 0
--- Rx kp  = 6
--- Rx kf  = 4
--- Rx kp tracking = 5
--- Rx kf tracking = 1
--- Rx kp hlf stp  = 0
--- Rx kp tracking hlf stp  = 0
--- Rx dsp_gain_boost    = 20

*** Please choose from following test items ......