'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'User Macro: 
'During macro recording, DON'T CLOSE OR EDIT FILE
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HC4&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00C4002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HC4&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00C4002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HC5&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00C5002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HC5&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00C5002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HC6&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00C6002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HC6&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00C6002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HC7&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00C7002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HC7&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00C7002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HC8&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00C8002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HC8&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00C8002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HC9&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00C9002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HC9&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00C9002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HCA&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00CA002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HCA&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00CA002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HCB&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047000&, &H00CB002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0D&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800D&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H2C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HCB&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047400&   ' data=&H00CB002C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047420&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047424&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047428&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004742C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
