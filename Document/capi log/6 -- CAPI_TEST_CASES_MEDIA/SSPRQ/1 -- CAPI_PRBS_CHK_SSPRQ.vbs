'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'User Macro: 
'During macro recording, DON'T CLOSE OR EDIT FILE
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF0&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F0002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA74&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF1&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F1001C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00010000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA78&, &H00008001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00008009&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA7C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA80&   ' data=&H00008001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF1&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F1001C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00020000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA7C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA80&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF2&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F20018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF2&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F20018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF3&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F30018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF3&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F30018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF4&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F40018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF4&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F40018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF5&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F50018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF5&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F50018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF6&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F60018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF6&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F60018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF7&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F7002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA74&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF8&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F8001C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00020000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA78&, &H00008002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00008009&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA7C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA80&   ' data=&H00008002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF8&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F8001C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00020000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA7C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA80&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HF9&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02F90018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HF9&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02F90018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HFA&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02FA0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HFA&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02FA0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HFB&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02FB0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HFB&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02FB0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HFC&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02FC0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HFC&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02FC0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HFD&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02FD0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HFD&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02FD0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HFE&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02FE002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA74&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HFF&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02FF001C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00040000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA78&, &H00008004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00008009&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA7C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA80&   ' data=&H00008004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&HFF&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02FF001C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00020000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA7C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA80&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03000018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03000018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03010018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03010018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03020018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03020018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H03&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03030018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H03&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03030018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03040018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000004&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H04&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03040018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H0305002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA74&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H06&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H0306001C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00080000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA78&, &H00008008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00008009&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA7C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA80&   ' data=&H00008008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H06&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H0306001C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00020000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA7C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA80&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H07&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03070018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H07&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03070018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03080018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03080018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H03090018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H09&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H03090018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H0A&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H030A0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0A&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H030A0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H0B&
App.wrI2C &H50&, &H87&, &H03&
'App.WriteEst &H00&, &H00047000&, &H030B0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H08&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000008&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H08&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000008&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0B&
App.rdI2C &H50&, &H90& ' value=&H03&
'App.ReadEst &H00&, &H00047400&   ' data=&H030B0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H05&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000005&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
