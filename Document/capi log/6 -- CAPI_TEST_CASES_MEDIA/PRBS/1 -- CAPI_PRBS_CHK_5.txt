**  0 -- CAPI_PRBS_GEN
**  1 -- CAPI_PRBS_CHK
**  2 -- CAPI_GET_PRBS_INFO
**  3 -- CAPI_PRBS_DISABLE
**  4 -- CAPI_PRBS_CLEAR
**  5 -- CAPI_PRBS_INJECT_ERROR
**  6 -- CAPI_READ_REGISTER
**  7 -- CAPI_WRITE_REGISTER
**  8 -- CAPI_GET_POLARITY
**  9 -- CAPI_CHANGE_POLARITY
** 10 -- CAPI_SET_TX_INFO
** 11 -- CAPI_GET_TX_INFO
** 12 -- CAPI_SET_SQUELCH
** 13 -- CAPI_GET_TX_ELECTRIC_IDLE_INFO
** 14 -- CAPI_SET_LOOPBACK
** 15 -- CAPI_GET_LOOPBACK
** 16 -- CAPI_GET_CDR_PMD_INFO
** 17 -- CAPI_GET_LANE_STATUS
** 18 -- CAPI_SET_LANE_CTRL
** 19 -- CAPI_GET_LANE_CTRL
** 20 -- CAPI_LANE_RESET
** 21 -- CAPI_SET_RX_INFO
** 22 -- CAPI_GET_RX_INFO
** 23 -- CAPI_SERDES_SET_TX_SHARED_PTRN
** 24 -- CAPI_TEST_SET_ELECTRICAL_OPTICAL
** 25 -- CAPI_GET_GPR_LANE_STATUS
** 26 -- CAPI_SET_DSP_POWER_INFO
** 27 -- CAPI_GET_DSP_POWER_INFO
** 28 -- CAPI_GET_USR_DIAGNOSTIC (client host)
** 29 -- CAPI_GET_USR_DIAG (line media)
** 30 -- BACK_TO_PREV_MENU
1


Please choose channel ID:
0: all channels
1: channel 0
2: channel 1
3: channel 2
4: channel 3
5: user specified lane mask
0

**Choose all Media lanes 0

**Input: PRBS (0) or SSPRQ (1):0

** Re-start PRBS_PMON : 0 => No, 1 => Yes
1

** Please choose poly:
0: PRBS_7
1: PRBS_9
2 :PRBS_11
3: PRBS_15
4: PRBS_23
5: PRBS_31
6: PRBS_58
7: PRBS_49
8: PRBS_10
9: PRBS_20
10: PRBS_13
5

** Turn on Media PRBS mon on lane 0
start check Media PRBS_MON status
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0xe ber=5.270588e-010
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x10 ber=3.011765e-010
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x13 ber=2.384314e-010
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x13 ber=1.788235e-010
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x15 ber=1.581176e-010

** Turn on Media PRBS mon on lane 1
start check Media PRBS_MON status
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000

** Turn on Media PRBS mon on lane 2
start check Media PRBS_MON status
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x4 ber=1.505882e-010
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x5 ber=9.411764e-011
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x6 ber=7.529412e-011
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x6 ber=5.647059e-011
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x6 ber=4.517647e-011

** Turn on Media PRBS mon on lane 3
start check Media PRBS_MON status
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000
PRBS lock = 0x1,  loss_of_lock_sticky = 0x0, err_cnt = 0x0 ber=0.000000e+000

*** Please choose from following test items ......