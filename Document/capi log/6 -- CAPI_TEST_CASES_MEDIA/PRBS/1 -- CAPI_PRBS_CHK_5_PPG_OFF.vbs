'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'User Macro: 
'During macro recording, DON'T CLOSE OR EDIT FILE
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H57&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H0257002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1F&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H0000001F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA74&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H58&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H0258001C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00010000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA78&, &H00008001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00008009&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA7C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA80&   ' data=&H00008001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H58&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H0258001C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00020000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA7C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA80&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H59&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02590018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H59&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02590018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H5A&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H025A0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H5A&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H025A0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H5B&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H025B0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H5B&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H025B0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H5C&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H025C0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H5C&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H025C0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H5D&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H025D0018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H5D&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H025D0018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H5E&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H025E0002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCCCC00&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H5E&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H025E0002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H5F&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H025F0002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCC0200&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H60&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02600002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCC0200&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H60&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02600002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000100&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H61&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02610002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCC0101&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H61&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02610002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H2C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H62&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H0262002C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1F&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H0000001F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H20&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047020&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H24&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047024&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H28&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047028&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H2C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004702C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0C&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA74&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H1C&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H63&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H0263001C&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00010000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004701C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H78&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA78&, &H00008001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H09&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00008009&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA7C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA80&   ' data=&H00008001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H1C&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H63&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H0263001C&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00020000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H0F&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H000F0001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H1C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004741C&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H7C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA7C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H80&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA80&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H64&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02640018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H64&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02640018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H65&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02650018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H65&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02650018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H66&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02660018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H66&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02660018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H67&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02670018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H67&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02670018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H18&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H68&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02680018&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047004&, &H00000002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047008&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H0004700C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047010&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047014&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H00047018&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H0F&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H0000800F&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H18&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H68&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02680018&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H08&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047408&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H0C&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H0004740C&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H10&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047410&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H14&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047414&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H18&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047418&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H69&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H02690002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCCCC00&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H69&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H02690002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000100&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H6A&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H026A0002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCC0200&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H04&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008004&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H6B&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H026B0002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCC0200&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H6B&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H026B0002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA94&   ' data=&H00000000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H02&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H6C&
App.wrI2C &H50&, &H87&, &H02&
'App.WriteEst &H00&, &H00047000&, &H026C0002&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H70&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &HCC&
App.wrI2C &H50&, &H87&, &HCC&
'App.WriteEst &H00&, &H00047004&, &HCCCC0001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H98&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H01&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA98&, &H00000001&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H05&
App.wrI2C &H50&, &H87&, &H80&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00008005&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H80&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CA9C&   ' data=&H00008000&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H5201CAA0&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H00&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H02&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H6C&
App.rdI2C &H50&, &H90& ' value=&H02&
'App.ReadEst &H00&, &H00047400&   ' data=&H026C0002&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H04&
App.wrI2C &H50&, &H81&, &H74&
App.wrI2C &H50&, &H82&, &H04&
App.wrI2C &H50&, &H83&, &H00&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H01&
App.rdI2C &H50&, &H90& ' value=&H01&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
App.rdI2C &H50&, &H90& ' value=&H00&
'App.ReadEst &H00&, &H00047404&   ' data=&H00000001&
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H84&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA84&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H94&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA94&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H74&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA74&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &H9C&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CA9C&, &H00000000&   '
App.wrI2C &H50&, &H7F&, &HFF&
App.wrI2C &H50&, &H80&, &HA0&
App.wrI2C &H50&, &H81&, &HCA&
App.wrI2C &H50&, &H82&, &H01&
App.wrI2C &H50&, &H83&, &H52&
App.wrI2C &H50&, &H84&, &H04&
App.wrI2C &H50&, &H85&, &H00&
App.wrI2C &H50&, &H86&, &H03&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
App.wrI2C &H50&, &H87&, &H00&
'App.WriteEst &H00&, &H5201CAA0&, &H00000000&   '
